/**
 * 
 **/
package com.etouch.taf.core.driver.web;

import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.etouch.taf.core.TestBed;
import com.etouch.taf.core.exception.DriverException;
import com.etouch.taf.util.ConfigUtil;
import com.etouch.taf.webui.selenium.SeleniumDriver;

/**
 * Helps to build Firefox Driver firefox profile is not included yet.
 * @author eTouch
 */
public class FirefoxDriver extends WebDriver {

	/**
	 * Instantiates a new firefox driver.
	 * @param testBed the test bed
	 * @throws DriverException the driver exception
	 */
	public FirefoxDriver(TestBed testBed) throws DriverException {
		super(testBed);
	}

	/**
	 * Builds Firefox Driver according to the given configuration values in config.yml
	 * @throws DriverException the driver exception
	 */
	@Override
	public void buildDriver() throws DriverException {

		if (ConfigUtil.isLocalEnv(testBed.getTestBedName())) {
			// if tool is given devConfig.yml as Selenium, then create a
			// selenium FireFox driver
			if (ConfigUtil.isSelenium()) {
				driver = SeleniumDriver.buildFireFoxDriver();
			}
		} else if (ConfigUtil.isRemoteEnv(testBed.getTestBedName())) {

			if (ConfigUtil.isSelenium()) {

				FirefoxProfile ffProfile = new FirefoxProfile();
				ffProfile.setEnableNativeEvents(false);
				capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability(
						org.openqa.selenium.firefox.FirefoxDriver.PROFILE,
						testBed);
				driver=SeleniumDriver.buildRemoteDriver(capabilities);
			}
		} else if (ConfigUtil.isBrowserStackEnv(testBed.getTestBedName())) {
			capabilities = DesiredCapabilities.firefox();
			buildBrowserstackCapabilities();
		}
	}

	/**
	 * This method will give an instance of FirefoxProfile Uncomment the code
	 * according to which firefox profile you want to create.
	 * @return the firefox profile
	 */
	private FirefoxProfile getFirefoxProfile() {				
		return new FirefoxProfile();
	}

	/**
	 * (non-Javadoc)
	 * @see com.etouch.taf.core.driver.DriverBuilder#getDriver()
	 **/
	@Override
	public Object getDriver() throws DriverException {
		return driver;
	}
}