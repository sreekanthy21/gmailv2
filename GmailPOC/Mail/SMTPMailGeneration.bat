set DIRPATH=%1
set RECEIVER=%2
set SUBJECT=%3
set PRIORITY=%4
set BODY=%5

cd /d %DIRPATH%\MailAlert


MailAlert -r %RECEIVER% -s %SUBJECT% %PRIORITY% %BODY%
