package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Gmailify extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*public void ExecuteAllGmailify() throws Exception{
		verifyGmailifyFlow();
		verifyTryGmailifyFlow();
		verifyGmailifyPasswordFlow();
	}*/
	
	/**Testcase to verify the Gmailify Flow
	 * 
	 * @preCondition Check the imap account is not linked
	 * @throws Exception
	 * GMAT 85
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 107)
	public void verifyGmailifyFlow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyGmailifyFlow******************");
			Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");
			userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
			
		//	userAccount.verifyAccountIsLinked(driver, gmailifyFlowData.get("ImapEmail"));
			
			navDrawer.navigateSettingsPage(driver);
			//written by Venkat on 14/12/2018: start
			try{
				CommonFunctions.scrollsToTitle(driver, gmailifyFlowData.get("ImapEmail"));
			}catch(Exception e){
				throw new Exception("Email Account not present:"+gmailifyFlowData.get("ImapEmail"));
			}
			//written by Venkat on 14/12/2018: end
			
			userAccount.linkAccountGmailify(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("GmailAccount"), 
					gmailifyFlowData.get("GmailPassword"), gmailifyFlowData.get("ImapEmailPwd"));
			
			userAccount.verifyGmailifyAccount(driver, gmailifyFlowData.get("ImapEmail"));			
			
			log.info("*****************Completed testcase ::: verifyGmailifyFlow************");
			//driver.navigate().back();
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyGmailifyFlow");
		}
	}
	
	/**TestCase to verify the IMAP account is setup with Try Gmailify option
	 * Gmailify in Account Setup flow
	 * @preCondition Add an IMAP account
	 * @throws Exception
	 * GMAT 86
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 108)
	public void verifyTryGmailifyFlow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyTryGmailifyFlow******************");
			Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
			
			userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			
			 log.info("adding Other account using Other option");
			 userAccount.addingOtherAccountForGmailify(driver, tryGmailifyFlowData.get("IMAPEmail"),
						tryGmailifyFlowData.get("IMAPEmailPwd"));
			
			/*userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
					tryGmailifyFlowData.get("IMAPEmailPwd"));*/
			userAccount.tryGmailifySetUpIMAP(driver);
			//userAccount.verifyExchangeAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));			

			log.info("*****************Completed testcase ::: verifyTryGmailifyFlow************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "TryGmailifyFlow");
		}
	}
	
	/**TestCase to Gmailify flow for password providers
	 * Gmailify in Account Setup flow
	 * @preCondition Add an IMAP account
	 * @throws Exception
	 * GMAT 86
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 109)
	public void verifyGmailifyFlowPwdProviders()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyGmailifyFlowPwdProviders******************");
			Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
			Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");

			userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			
			 log.info("adding Other account using Other option");
			 userAccount.addingOtherAccountForGmailify(driver, tryGmailifyFlowData.get("IMAPEmail"),
						tryGmailifyFlowData.get("IMAPEmailPwd"));
			userAccount.completeSetupAccount(driver);
			/*userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
					tryGmailifyFlowData.get("IMAPEmailPwd"));*/
			driver.navigate().back();
			if(!CommonFunctions.isAccountAlreadySelected(tryGmailifyFlowData.get("IMAPEmail")))
			userAccount.switchMailAccount(driver, tryGmailifyFlowData.get("IMAPEmail"));
			userAccount.TryItOutGmailify(driver);
			userAccount.linkAccountGmailify(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("GmailAccount"), 
					gmailifyFlowData.get("GmailPassword"), gmailifyFlowData.get("ImapEmailPwd"));
			userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));			

			log.info("*****************Completed testcase ::: verifyGmailifyFlowPwdProviders************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyGmailifyFlowPwdProviders");
		}
	}
	
	@Test(priority = 110)
	public void verifyTheLabelListForGmailifiedAccount() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyTheLabelListForGmailifiedAccount******************");
			Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
			/*userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			
			userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
					tryGmailifyFlowData.get("IMAPEmailPwd"));
			userAccount.tryGmailifySetUpIMAP(driver);*/
			//userAccount.verifyExchangeAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));
			inbox.verifyTheLabelListForGmailifiedAccount(driver);
			log.info("*****************Completed testcase ::: verifyTheLabelListForGmailifiedAccount************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyTheLabelListForGmailifiedAccount");
		}
	}
	


	/**Testcase to verify the certificate transparency
	 * 
	 * @throws Exception
	 * GMAT 88
	 * @author Rahul
	 */
	@Test(enabled = true, priority = 111)
	public void certificateTransparency()  {
		driver = map.get(Thread.currentThread().getId());
		try {
		
			log.info("*****************Started testcase ::: certificateTransparency***************************");
			Thread.sleep(10000);
			inbox.verifyGmailAppInitState(driver);
			inbox.certificateTransparency(driver);
			
			log.info("*****************Completed testcase ::: certificateTransparency*************************");
     } catch (Throwable e) {
			
		}
	}
}
