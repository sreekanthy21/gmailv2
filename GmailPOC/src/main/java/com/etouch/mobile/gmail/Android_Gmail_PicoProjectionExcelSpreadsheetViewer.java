package com.etouch.mobile.gmail;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.MailDeletion;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_PicoProjectionExcelSpreadsheetViewer extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	
/**
* @TestCase  127: Verify MS Documents In App
* @Pre-condition : MS related apps should be installed. 
* @comment: Tried automating the installing part as well, but The problem is the time taken to
*                      download and install is more so in script it is not possible to wait for such long.
*  @author batchi
* @throws: Exception
* GMAT 102
*/
@Test(groups = {"P0"}, enabled=true, priority=102)
public void verifyDocumentsInApp() throws Exception{
	driver = map.get(Thread.currentThread().getId());
	try {
		log.info("***************** Started testcase ::: verifyDocumentsInApp ******************************");
		Map<String, String> verifyDocumentInAppData = commonPage
				.getTestData("verifyAttachmentsUponRotating");
		Map<String, String> verifyReplyFromNotificationData = commonPage
				.getTestData("verifyReplyFromNotification");
		userAccount.launchPlayStoreApp(driver);
		userAccount.verifyAppIsInstalledOrNot(driver,verifyDocumentInAppData.get("ExcelType") );
		userAccount.verifyAppIsInstalledOrNot(driver,verifyDocumentInAppData.get("DocsType") );
		userAccount.verifyAppIsInstalledOrNot(driver,verifyDocumentInAppData.get("SlidesType") );
		
		inbox.launchGmailApplication(driver);		
		/*userAccount.switchMailAccount(driver,verifyDocumentInAppData.get("FromMailIdData"));
		String toUser=  inbox.composeAsNewMail(driver,
				verifyDocumentInAppData.get("FromMailIdData"),
				verifyReplyFromNotificationData.get("CC"),
				verifyReplyFromNotificationData.get("BCC"),
				verifyDocumentInAppData.get("mailSubjectData"),
				verifyReplyFromNotificationData.get("ComposeBodyData"));
		attachment.addAttachmentFromDrive(driver,verifyDocumentInAppData.get("ExcelAttachmentName"));
		attachment.addAttachmentFromDrive(driver,verifyDocumentInAppData.get("DocsAttachmentName"));
		attachment.addAttachmentFromDrive(driver,verifyDocumentInAppData.get("SlidesAttachementName"));
		attachment.addAttachmentFromDrive(driver,verifyDocumentInAppData.get("PdfAttachmentName"));
		userAccount.tapOnSendForNewMail(); 
		userAccount.switchMailAccount(driver,verifyDocumentInAppData.get("FromMailIdData") );*/
		if(!CommonFunctions.isAccountAlreadySelected(verifyDocumentInAppData.get("FromMailIdData")))
		userAccount.switchMailAccount(driver, verifyDocumentInAppData.get("FromMailIdData"));
		inbox.openMailUsingSearch(driver, verifyDocumentInAppData.get("mailSubjectData"));
		
	    attachment.openingAttachments(driver,verifyDocumentInAppData.get("ExcelAttachmentName"));
		attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("ExcelAttachmentName"),
				                                               verifyDocumentInAppData.get("ExcelType"),
			                                                   verifyDocumentInAppData.get("OpenAttachmentSheetsOption"));
		attachment.openingAttachments(driver,verifyDocumentInAppData.get("DocsAttachmentName"));
		attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("DocsAttachmentName"),
                verifyDocumentInAppData.get("DocsType"),
                verifyDocumentInAppData.get("OpenAttachmentDocsOption"));
    	 attachment.openingAttachments(driver,verifyDocumentInAppData.get("SlidesAttachementName"));
		attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("SlidesAttachementName"),
                verifyDocumentInAppData.get("SlidesType"),
                verifyDocumentInAppData.get("OpenAttachmentSlidesOption"));

		log.info("*****************Completed testcase ::: verifyDocumentsInApp*************************");

	}catch (Throwable e) {
		  commonPage.catchBlock(e, driver, "verifyDocumentsInApp");
}
}
/**
* @TestCase 108 : Verify all attachments in Pico projector file preview
* @Pre-condition : Mail consists of MS document file, excel file, pdf file, slides file
*  @author batchi
* @throws: Exception
* GMAT 103
*/
@Test(groups = {"P0"}, enabled=true, priority=103)
public void verifyPicoProjectorFilePreview() throws Exception{
	driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyPicoProjectorFilePreview ******************************");
			Map<String, String> verifyPicoProjectorFilePreviewData = commonPage
					.getTestData("verifyAttachmentsUponRotating");
			driver.closeApp();
			//verify apps are already installed
			userAccount.launchPlayStoreApp(driver);
			userAccount.verifyAppIsInstalled(driver,verifyPicoProjectorFilePreviewData.get("ExcelType"));
			//On Android 6.0.1 docs app is installed by default and unable to uninstall
		//	userAccount.verifyAppIsInstalled(driver,verifyPicoProjectorFilePreviewData.get("DocsType"));
			userAccount.verifyAppIsInstalled(driver,verifyPicoProjectorFilePreviewData.get("SlidesType"));
			inbox.launchGmailApplication(driver);
			Map<String, String> verifyReplyFromNotificationData = commonPage
					.getTestData("verifyReplyFromNotification");
/*			userAccount.switchMailAccount(driver,verifyPicoProjectorFilePreviewData.get("FromMailIdData"));
			String toUser=  inbox.composeAsNewMail(driver,
					verifyPicoProjectorFilePreviewData.get("ToMailIdData"),
					verifyReplyFromNotificationData.get("CC"),
					verifyReplyFromNotificationData.get("BCC"),
					verifyPicoProjectorFilePreviewData.get("mailSubjectData"),
					verifyReplyFromNotificationData.get("ComposeBodyData"));
			attachment.addAttachmentFromDrive(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
			attachment.addAttachmentFromDrive(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
			attachment.addAttachmentFromDrive(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
			attachment.addAttachmentFromDrive(driver,verifyPicoProjectorFilePreviewData.get("PdfAttachmentName"));
			userAccount.tapOnSendForNewMail();*/ 
			if(!CommonFunctions.isAccountAlreadySelected(verifyPicoProjectorFilePreviewData.get("ToMailIdData")))
			userAccount.switchMailAccount(driver,verifyPicoProjectorFilePreviewData.get("ToMailIdData"));
			inbox.openMailUsingSearch(driver, verifyPicoProjectorFilePreviewData.get("mailSubjectData"));
			//all attachments in pico  view
			attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
			attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
		//	attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
		//  attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
			attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
			attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
			attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("PdfAttachmentName"));
			attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("PdfAttachmentName"));
			//uneditable mode
			attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
		//	attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
			attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
			//Add-to-drive option
			/*Need to verify the add to drive option is not currently available*/
			attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
			attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
			attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
			//able to scroll down in pico viewer
			attachment.verifyDocumentIsScrollable(driver, verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
		} catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyPicoProjectorFilePreview");
	}
		
}

/**
* @TestCase 94: Verify excel spreadsheet viewer
* @Pre-condition : Add eaitest103@gmail.com account to device (use this account until feature is launched 100% to production)
*  @author batchi
* @throws: Exception
*//*
@Test(groups = {"P1"}, enabled = true, priority = 2)
public void verifyExcelSpreadsheetViewer()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> verifyExcelSpreadsheetViewerData = commonPage
					.getTestData("verifyAttachmentsUponRotating");
			log.info("*****************Started testcase ::: verifyExcelSpreadsheetViewer*****************************");
			//MailDeletion.DeleteAllMailsMotoGOne();
			driver.closeApp();
			//verify apps are already installed
			userAccount.launchPlayStoreApp(driver);
			userAccount.verifyAppIsInstalledOrNot(driver,verifyExcelSpreadsheetViewerData.get("ExcelType"));
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyReplyFromNotificationData = commonPage
					.getTestData("verifyReplyFromNotification");
			userAccount.switchMailAccount(driver,verifyExcelSpreadsheetViewerData.get("ToMailIdData"));
			String toUser=  inbox.composeAsNewMail(driver,
					verifyExcelSpreadsheetViewerData.get("FromMailIdData"),
					verifyReplyFromNotificationData.get("CC"),
					verifyReplyFromNotificationData.get("BCC"),
					verifyExcelSpreadsheetViewerData.get("mailSubject"),
					verifyReplyFromNotificationData.get("ComposeBodyData"));
			attachment.addAttachmentFromDrive(driver,verifyExcelSpreadsheetViewerData.get("ExcelWithMultipleSheets"));
			userAccount.tapOnSendForNewMail(); 
			userAccount.switchMailAccount(driver,verifyExcelSpreadsheetViewerData.get("FromMailIdData") );
			inbox.openMailUsingSearch(driver, verifyExcelSpreadsheetViewerData.get("mailSubject"));
			attachment.openingAttachments(driver,verifyExcelSpreadsheetViewerData.get("ExcelWithMultipleSheets"));
			attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyExcelSpreadsheetViewerData.get("ExcelWithMultipleSheets"),
					verifyExcelSpreadsheetViewerData.get("ExcelType"),
					verifyExcelSpreadsheetViewerData.get("OpenAttachmentSheetsOption"));
			//navigate between sheets
			attachment.verifyNavigatiionBtwSheets(driver,verifyExcelSpreadsheetViewerData.get("ExcelWithMultipleSheets"));
			//long press on cell to copy the text
			navDrawer.longPressOnSpreadsheetCell(driver);
			log.info("*****************Completed testcase ::: verifyExcelSpreadsheetViewer*************************");
	}catch(Throwable e){
		commonPage.catchBlock(e, driver, "verifyExcelSpreadsheetViewer");
	}
}		
*/

}
