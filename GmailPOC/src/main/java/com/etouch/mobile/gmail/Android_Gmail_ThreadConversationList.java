package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_ThreadConversationList extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/**
	 * @TestCase 12 : Verify Archvie/Delete (CAB mode) muiltipe mails
	 * @Pre-condition :
	 * @throws: Exception
	 * GMAT 35
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 5)
	public void verifyArchiveAndDeletingMultipleMails() throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map <String, String> verifyArchiveAndDeletingMultipleMailsData = CommonPage.getTestData("verifyArchiveAndDeletingMultipleMails");
		
		
		String emailId = CommonFunctions.getNewUserAccount(verifyArchiveAndDeletingMultipleMailsData, "AccountOne", null);

		
		if(!CommonFunctions.isAccountAlreadySelected(emailId)) {
   			userAccount.switchMailAccount(driver, emailId);
   		}
		
		
		String mailSubject = verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMailsSubject");
		CommonFunctions.smtpMailGeneration(emailId,mailSubject, "SwipeDownToRefresh.html", null);
		
		Thread.sleep(8000);
	   	try{
	   		inbox.verifyGmailAppInitState(driver);
	   		log.info("**************Started Testcae :: Archive and Delete multiple mails*************");
	   		
	   		if(!CommonFunctions.isAccountAlreadySelected(emailId)) {
	   			userAccount.switchMailAccount(driver, emailId);
	   		}
	   		
	   		
	           Thread.sleep(2000);
	           
	           userAccount.navigateToGeneralSettings(driver);
	           CommonFunctions.navigateBackNTimesNew(driver, 2);
	           
	           navDrawer.navigateToPrimary(driver);
	           Thread.sleep(1000);
	           UserAccount.pullToReferesh(driver);
	           Thread.sleep(2000);
	           inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail1"));
	         //  Thread.sleep(1000);
	           inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail2"));
	          // Thread.sleep(1000);
	           inbox.verifyMailSelectionCount(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("MailSelectionCount"));
	           navDrawer.tapOnArchive(driver);
	           navDrawer.navigateToAllEmails(driver);

	           inbox.verifyMailIsPresent(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("ArchiveMail1"));
	           inbox.verifyMailIsPresent(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("ArchiveMail2"));
	          // Thread.sleep(1500);

	           inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	               .get("DeleteMail1"));
	          // Thread.sleep(1000);

	           inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	               .get("DeleteMail2"));

	           inbox.verifyMailSelectionCount(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("MailSelectionCount"));
	           navDrawer.tapOnDelete(driver);
	           navDrawer.navigateToBin(driver, verifyArchiveAndDeletingMultipleMailsData.get("BinOption"));

	           inbox.verifyMailIsPresent(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("DeleteMail1"));
	           inbox.verifyMailIsPresent(driver,
	               verifyArchiveAndDeletingMultipleMailsData
	               .get("DeleteMail2"));
	           driver.navigate().back();
	           
	           log.info("**************Completed Testcae :: Archive and Delete multiple mails*************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyArchiveAndDeletingMultipleMails");
		}
	}
	
	/**
	 * @TestCase 2 : Verify Swipe to Delete/archive and Undo
	 * @Pre-condition : Mail with subject "verifySwipeDeleteFunctionality" and
	 *                "verifySwipeArchiveFunctionality" should be available
	 *                under account one
	 * @throws: Exception
	 * GMAT 36
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 85)
	public void verifySwipeDeleteFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
	/*	
		 String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"verifySwipeDeleteFunctionality.bat";
		 log.info("Executing the bat file "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed the bat file to generate a mail");
	*/	 
		
		 Thread.sleep(10000);
		try {
			log.info("*****************Started testcase ::: verifySwipeDeleteFunctionality***********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyForwardingMailFunctionality = CommonPage.getTestData("verifySwipeDeleteFunctionality");
			

			String  emailId = CommonFunctions.getNewUserAccount(verifyForwardingMailFunctionality, "AccountOne", null);
			CommonFunctions.smtpMailGeneration(emailId,"verifySwipeDeleteFunctionality,verifySwipeArchiveFunctionality", "SwipeDownToRefresh.html", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);


			userAccount.setGmailDefaultActionToArchive(driver);
			inbox.pullToReferesh(driver);
			/*inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			*/
			inbox.swipeToDeleteOrArchiveMailNew(driver, verifyForwardingMailFunctionality.get("MailSubjectDelete"), "Delete");
			inbox.undoSwipeDeleteArchiveOperation(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			/*inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			*/
			inbox.swipeToDeleteOrArchiveMailNew(driver, verifyForwardingMailFunctionality.get("MailSubjectDelete"), "Delete");

			navDrawer.navigateToBin(driver, verifyForwardingMailFunctionality.get("BinOption"));
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			/*navDrawer.openPrimaryInbox(driver,
					verifyForwardingMailFunctionality.get("Accountthree"));
			*/
			driver.navigate().back();
			//navDrawer.navigateToPrimary(driver);
			/*inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			*/
			inbox.swipeToDeleteOrArchiveMailNew(driver, verifyForwardingMailFunctionality.get("MailSubjectArchive"), "Archive");

			inbox.undoSwipeDeleteArchiveOperation(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			/*inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			*/
			inbox.swipeToDeleteOrArchiveMailNew(driver, verifyForwardingMailFunctionality.get("MailSubjectArchive"), "Archive");

			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
//			userAccount.setGmailDefaultActionToDelete(driver);
			
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifySwipeDeleteFunctionality************");
		}
			catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySwipeDeleteFunctionality");
		}
	}
	
	/**
	 * @TestCase 14 : Verify Default and Priority Inbox list views and ads in
	 *           Promotions
	 * @Pre-condition : Ad should be available under Promotion mails for Account
	 *                one
	 * @throws: Exception
	 * GMAT 37
	 */
	@Test(groups = {"P2"}, enabled = false, priority = 84)
	public void verifyDefaultAndPriorityInboxListViews() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		boolean isPrimaryInboxEnabled;
		try {
			log.info("*****************Started testcase ::: verifyDefaultAndPriorityInboxListViews************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyDefaultAndPriorityInboxListViewsData = commonPage
					.getTestData("verifyDefaultAndPriorityInboxListViews");

			String user = verifyDefaultAndPriorityInboxListViewsData
					.get("AccountOne");
			
			if(!CommonFunctions.isAccountAlreadySelected(user))
			userAccount.switchMailAccount(driver, user);
			inbox.openMenuDrawer(driver);
			isPrimaryInboxEnabled = navDrawer
					.isPrimaryInboxDisplayed(driver);
			commonFunction.navigateBack(driver);
			userAccount.openUserAccountSetting(driver, user);
			navDrawer.verifyDefaultInbox(driver, user);
			navDrawer.verifyInboxCategories(driver);
			navDrawer.enableAllInboxCategories(driver, isPrimaryInboxEnabled);
			promotions.verifyPromotionsAdDisplayed(driver);
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyDefaultAndPriorityInboxListViews"),
							"Image comparison for verifyDefaultAndPriorityInboxListViews");

			commonFunction.navigateBack(driver);
			userAccount.openUserAccountSetting(driver, user);
			navDrawer.verifyPriorityInbox(driver, user);
			isPrimaryInboxEnabled = navDrawer
					.isPrimaryInboxDisplayed(driver);
			commonFunction.navigateBack(driver);
			userAccount.openUserAccountSetting(driver, user);
			navDrawer.verifyDefaultInbox(driver, user);
			navDrawer.verifyInboxCategories(driver);
			navDrawer.enableAllInboxCategories(driver, isPrimaryInboxEnabled);
			//softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyDefaultAndPriorityInboxListViews*************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyDefaultAndPriorityInboxListViews");
		}
	}
	
	/**
	 * @TestCase 8 : Verify Mail snippet in Thread list
	 * @Pre-condition : Mail with subject "verifyMailSnippetInThreadList" and
	 *                "VerifyReplyAllFunctionality" should be available under
	 *                Account two
	 * @throws: Exception
	 * GMAT 38
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 39)
	public void verifyMailSnippetThreadList() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyMailSnippetInThreadList******************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyMailSnippetInThreadListData = CommonPage.getTestData("verifyMailSnippetInThreadList");

			if(!CommonFunctions.isAccountAlreadySelected(verifyMailSnippetInThreadListData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifyMailSnippetInThreadListData.get("AccountOne"));
			inbox.searchForMail(driver,
					verifyMailSnippetInThreadListData.get("MailSnippetImage"));
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage("verifyMailSnippetThreadList"),
					"Image comparison for verifyMailSnippetInThreadList");
			
			commonFunction.navigateBack(driver);
						//softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyMailSnippetInThreadList********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyMailSnippetInThreadList");
		}
	}
	
	/**--Verifying Unsupported swipe actions
	 * 	   
	 * @throws Exception
	 */
		    @Test(priority = 98)
		    public void verifyUnsupportedSwipeActions()throws Exception{
	   			driver = map.get(Thread.currentThread().getId());
		    	try{
		   	        log.info("*****************Started testcase ::: verifyUnsupportedSwipeActions*************************");
		   	        Capabilities s=driver.getCapabilities();
					version = s.getVersion();
		   	        Map <String, String> swipeActions = CommonPage.getTestData("verifySwipeActions");
		   	        
		   	        String  emailId = CommonFunctions.getNewUserAccount(swipeActions, "AccountOne", null);
		   	        
		   	        if(!CommonFunctions.isAccountAlreadySelected(emailId))
		   	        	userAccount.switchMailAccount(driver,emailId);
		   	     
		   	        /*--Navigate to settings and set swipe action--*/
		   	        userAccount.navigateToGeneralSettings(driver);
		   	        CommonFunctions.scrollsToTitle(driver, "Swipe actions");
		   	        userAccount.ChangeSwipeAction(driver, "Move to");
		   	        /*--Navigate to System folders--*/
		   	        navDrawer.navigateToSystemFoldersFromSettings(driver, "Starred");
					inbox.verifySwipeMail(driver);
					navDrawer.navigateToSystemFoldersFromSettings(driver, "Important");
					inbox.verifySwipeMail(driver);
					navDrawer.navigateToSystemFoldersFromSettings(driver, "Sent");
					inbox.verifySwipeMail(driver);
					navDrawer.navigateToSystemFoldersFromSettings(driver, "All mail");
					inbox.verifySwipeMail(driver);
					
				/*	
					String path = System.getProperty("user.dir");
					String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"UnsupportedSwipeActionSearch.bat";
					log.info("Executing the bat file to generate and delete it "+command);
			        Process  p = new ProcessBuilder(command).start();
				*/
					
					CommonFunctions.smtpMailGeneration(emailId,"UnsupportedSwipeActionSearch", "verifyMailSnippets.html", null);
					
					navDrawer.navigateToSystemFoldersFromSettings(driver, "Primary");
					userAccount.searchForMail(driver, "UnsupportedSwipeActionSearch");
					CommonPage.swipeLeftToRight(driver, swipeActions.get("SwipeSearchMail"));
					driver.navigate().back();
					driver.navigate().back();
			        
					userAccount.navigateToGeneralSettings(driver);
		   	        CommonFunctions.scrollsToTitle(driver, "Swipe actions");
		   	        userAccount.ChangeSwipeAction(driver, "Archive");
		   	        navDrawer.navigateToSystemFoldersFromSettings(driver, "Spam");
					inbox.verifySwipeMail(driver);
		   	 
		   	 }catch(Exception e){
		   		  commonPage.catchBlock(e, driver, "verifyUnsupportedSwipeActions");
		   	 }
		    }
		    /**--verifyDeletedMessagesInCV
		     * Author Santosh
			 * 	   
			 * @throws Exception
			 */
			
		    public void verifyDeletedMessagesInCV() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				 String path = System.getProperty("user.dir");
				 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"DeletedMessages.bat";
				 log.info("Executing the bat file "+command);
				 Process p = new ProcessBuilder(command).start();
				 log.info("Executed the bat file to generate a mail");
				 Thread.sleep(8000);
				   Map < String, String > DeletedMessagesInCV = commonPage.getTestData("DeletedMessagesInCV");
			   	try{
			   		inbox.verifyGmailAppInitState(driver);
			   		log.info("**************Started Testcae :: Deleted Messages In CV*************");
			           userAccount.switchMailAccount(driver, DeletedMessagesInCV.get("AccountOne"));
			           Thread.sleep(2000);
			           userAccount.pullToReferesh(driver);
			           inbox.selectMail(driver, DeletedMessagesInCV
				               .get("DeleteMessage"));
			           Thread.sleep(1500);
			           CommonFunctions.searchAndClickByXpath(driver, DeletedMessagesInCV.get("MailToolBarDeleteIcon"));
						Thread.sleep(1000);
						 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"DeletedMessages1.bat";
						 log.info("Executing the bat file "+command);
						 p = new ProcessBuilder(command).start();
						 log.info("Executed the bat file to generate a mail");
						 Thread.sleep(1000);
						 userAccount.pullToReferesh(driver);
						 Thread.sleep(1000);
						 if(CommonFunctions.getSizeOfElements(driver, DeletedMessagesInCV
					               .get("DeleteMessageSelect"))!=0){
								CommonFunctions.searchAndClickByXpath(driver, DeletedMessagesInCV
							               .get("DeleteMessageSelect"));
							}else{
								userAccount.pullToReferesh(driver);
								CommonFunctions.searchAndClickByXpath(driver, DeletedMessagesInCV
							               .get("DeleteMessageSelect"));
								log.info("Attachment found and clicked");
							}
						 Thread.sleep(1000);
						 CommonFunctions.searchAndClickByXpath(driver, DeletedMessagesInCV
					               .get("ShowButton"));
						 Thread.sleep(1000);
						 CommonFunctions.searchAndClickByXpath(driver, DeletedMessagesInCV
					               .get("ThisMessageDeleted"));
						 CommonFunctions.scrollDown(driver, 2);

			           driver.navigate().back();
			           
			           log.info("**************Completed Testcae :: Deleted Messages In CV*************");
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver,
							"verifyDeletedMessagesInCV");
				}
			}
		    /**--verify  undo operations in TL view
		     * Author Bindu
			 * 	   
			 * @throws Exception
			 */
			
		    public void verifyUndoInTL() throws Exception {
		    	 Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
				driver = map.get(Thread.currentThread().getId());
				 String path = System.getProperty("user.dir");
				 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert"+java.io.File.separator+"UndoOperations.bat";
				 log.info("Executing the bat file "+command);
				 Process p = new ProcessBuilder(command).start();
				 log.info("Executed the bat file to generate a mail");
				 Thread.sleep(8000);
				
			   	try{
			   		
			   		log.info("**************Started Testcase :: Verifying undo operations in TL view *************");
			   		userAccount.switchMailAccount(driver, undoData.get("account1"));
//			   		inbox.verifyMailPresent(driver,undoData.get("undoSubject") );
			//Verifying Archive undo option   		
				commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   	inbox.verifyUndoArchive(driver);
			//Verifying Delete undo option   
			   	commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   	inbox.verifyUndoDelete(driver);
			//Verifying Mute undo option      	
			   		commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   		inbox.verifyUndoMute(driver);
		    //Verifying Report Spam undo option   	   		
			   		commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   		inbox.verifyUndoReportSpam(driver);
			//Verifying Move To undo option     		
			   		commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   		inbox.verifyUndoMoveTo(driver);
			//Verifying Snooze undo option     		
			   		commonFunction.searchAndClickByXpath(driver, undoData.get("SelectMailImage"));
			   		inbox.verifyUndoSnooze(driver);
			   		
			   		log.info("*********** Ended Verifying undo operations in TL view  ************");	
			   	} catch (Throwable e) {
					commonPage.catchBlock(e, driver,
							"verifyUndoInTL");
				}
}
		    
		    /**--verify  undo operations in CV view
		     * Author Bindu
			 * 	   
			 * @throws Exception
			 */
			
		    public void verifyUndoInCV() throws Exception {
		    	 Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
				driver = map.get(Thread.currentThread().getId());
			   	try{			   		
			   		log.info("**************Started Testcase :: Verifying undo operations in CV view *************");
			   		Thread.sleep(2000);
			//Verifying Archive undo option   		
			   	inbox.openMail(driver,undoData.get("undoSubject") );
			   	inbox.verifyUndoArchive(driver);
			//Verifying Delete undo option   
			   	inbox.openMail(driver,undoData.get("undoSubject") );
			   	inbox.verifyUndoDelete(driver);
			//Verifying Mute undo option      	
			   	inbox.openMail(driver,undoData.get("undoSubject") );
			   		inbox.verifyUndoMute(driver);
		    //Verifying Report Spam undo option   	   		
			   		inbox.openMail(driver,undoData.get("undoSubject") );
			   		inbox.verifyUndoReportSpam(driver);
			//Verifying Move To undo option     		
			   		inbox.openMail(driver,undoData.get("undoSubject") );
			   		inbox.verifyUndoMoveTo(driver);
			//Verifying Snooze undo option     		
			   		inbox.openMail(driver,undoData.get("undoSubject") );
			   		inbox.verifyUndoSnooze(driver);
			   		
			   		log.info("*********** Ended Verifying undo operations in CV view  ************");	
			   	} catch (Throwable e) {
					commonPage.catchBlock(e, driver,
							"verifyUndoInCV");
				}
}
		    /**--verify  label mutation
		     * Author Bindu
			 * 	   
			 * @throws Exception
			 */
			
		    public void verifyLabelMutation() throws Exception {
		    	Map < String, String > ChangeLabelMutationData = commonPage.getTestData("changeLabels");
				driver = map.get(Thread.currentThread().getId());
			   	try{			   		
			   		log.info("************** Started Testcase :: Verifying Label mutation *************");
			   		Thread.sleep(2000);		    
			   		String path = System.getProperty("user.dir");
					 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert"+java.io.File.separator+"LabelMutation.bat";
					 log.info("Executing the bat file "+command);
					 Process p = new ProcessBuilder(command).start();
					 log.info("Executed the bat file to generate a mail");
					 Thread.sleep(8000);
			   		userAccount.switchMailAccount(driver,ChangeLabelMutationData.get("AccountOne"));
			   		conversationView.verifyLabelMutationTL(driver);
			   		conversationView.verifyLabelMutationCV(driver);
			   		
			   		
			   		log.info("************** Ended Testcase :: Verifying Label mutation *************");
			   		
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver,
							"verifyUndoInCV");
				}
}   
}

