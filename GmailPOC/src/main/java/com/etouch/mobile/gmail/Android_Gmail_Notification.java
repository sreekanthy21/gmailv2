package com.etouch.mobile.gmail;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Notification extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
    AppiumDriver driver = null;
	Process p;
	HtmlEmailSender mail = new HtmlEmailSender();
	sendMailMultipleEmails mailmultiple = new sendMailMultipleEmails();

    
/**
* @TestCase 68 : Verify all elements in the notification
* @Pre-condition : 1. Gmail app installed
                   2. Gmail account is added to app
                   3. Single Message sent from another account/device
*  @author batchi
* @throws: Exception
*/
	@Test(enabled=true, priority=86)
	public void verifyNotificationElements() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyNotificationElements ******************************");
			SoftAssert softAssert = new SoftAssert();
			Map<String, String> verifyNotificationElementsData = CommonPage.getTestData("verifyAttachmentsUponRotating");

			inbox.verifyGmailAppInitState(driver);

			String  emailId = CommonFunctions.getNewUserAccount(verifyNotificationElementsData, "ToMailIdData", null);
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			//userAccount.switchMailAccount(driver,verifyNotificationElementsData.get("ToMailIdData"));

			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingNotificationElements", " ");
			String[] adbroot = new String[]{"adb", "root"};
			p = new ProcessBuilder(adbroot).start();
			log.info("device rooted");
			Thread.sleep(2000);
			
			String[] adbclearnotify = new String[]{"adb", "shell", "service", "call", "notification", "1"};
			p = new ProcessBuilder(adbclearnotify).start();
			userAccount.openUserAccountSetting(driver, verifyNotificationElementsData.get("FromMailIdData"));
			userAccount.EnableSyncOnOFf(driver);;
			Thread.sleep(4000);
			//commonFunction.switchWifiOnAndOff();
			((AndroidDriver)driver).openNotifications();
			/*commonFunction.switchOnAirPlaneMode();
			Thread.sleep(1000);
			 commonFunction.switchOffAirPlaneMode();*/

			Thread.sleep(2000);
			CommonPage.verifyNotificationSenderName(driver);
			CommonPage.verifyNotificationTime(driver);
			CommonPage.verifyNotificationMailSubject(driver);
			softAssert.assertTrue(CommonPage.getScreenshotAndCompareImage("verifyNotificationElements"),
					"Image comparison for verifyNotificationElements");
			CommonPage.clearAllNotifications(driver);
			driver.navigate().back();
			driver.navigate().back();

		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyNotificationElements");
		}	
	}
    
    /**
	* @TestCase 70 : Verify notification off for specific account
	* @Pre-condition : 1. Gmail app installed
                       2. Gmail account is added to app
					   3. Notification option turned off (unchecked in settings) for selected account 
                       4. Message sent from another account/device to account with disabled notification option

	*  @author batchi
	* @throws: Exception
	* comment: turning off notifications for etouchmotogone@gmail.com account
	*/
	@Test(enabled=true, priority=87)
	public void verifyNotificationOffForSpecificAccount() throws Exception{

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyNotificationOffForSpecificAccount ******************************");
			Map<String, String> verifyNotificationOffForSpecificAccountData = CommonPage.getTestData("NotificationSettingOff");
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyNotificationOffForSpecificAccountData, "User2", null);
			
			//userAccount.switchMailAccount(driver,verifyNotificationOffForSpecificAccountData.get("User1"));
			userAccount.settingNotificationOff(driver,verifyNotificationOffForSpecificAccountData.get("User1"));
			
			//userAccount.switchMailAccount(driver,verifyNotificationOffForSpecificAccountData.get("User2"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			Thread.sleep(2000);
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingMailWithNotificationsOff", " ");
			Thread.sleep(800);
			navDrawer.openUserAccountSetting(driver, verifyNotificationOffForSpecificAccountData.get("User1"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(2000);
			((AndroidDriver)driver).openNotifications();
			commonPage.verifyNoNotifications(driver, "TestingMailWithNotificationsOff");
			driver.navigate().back();
			userAccount.settingNotificationOn(driver,verifyNotificationOffForSpecificAccountData.get("User1"));			
			driver.navigate().back();
			driver.navigate().back();

            }
		     catch (Throwable e) {
		    	 
			  commonPage.catchBlock(e, driver, "verifyNotificationOffForSpecificAccount");
	     }
	}
	/**
	* @TestCase 69 : Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
	* @Pre-condition : 1. Gmail app installed
                       2. Gmail account is added to app
                       3. Single Message sent from another account/device
    *  @author batchi
	* @throws: Exception
	*/
	@Test(enabled=true, priority=88)
	public void verifyNotificationReplyAndArchive() throws Exception{

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyNotificationReplyAndArchive ******************************");
			Map<String, String> verifyNotificationReplyAndArchiveData = CommonPage.getTestData("verifyReplyFromNotification");
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyNotificationReplyAndArchiveData, "GIGAccount", null);
			
			//Settings
			//navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			navDrawer.openUserAccountSetting(driver,emailId);
			CommonFunctions.searchAndClickByXpath(driver, verifyNotificationReplyAndArchiveData.get("NotificationsSettings"));
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "All");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Default reply action");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Reply");
			driver.navigate().back();
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "General settings");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Default notification action");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Archive");
			driver.navigate().back();
			driver.navigate().back();
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver,verifyNotificationReplyAndArchiveData.get("ToFieldData") );
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingGmailMailOpenfromNotificationFunctionality", " ");
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.verifyPopUp(driver, null);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			CommonFunctions.verifyPopUp(driver, null);
			driver.navigate().back();

			Thread.sleep(3000);
			((AndroidDriver)driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"));
			commonPage.verifyOpenFromNotification(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"));
			commonPage.navigateUntilHamburgerIsPresent(driver);

			//Sending mail to click Reply button from notification
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver,verifyNotificationReplyAndArchiveData.get("ToFieldData") );
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingGmailMailReplyfromNotificationFunctionality", " ");
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			driver.navigate().back();

			Thread.sleep(3500);
			((AndroidDriver)driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectData"));
			commonPage.verifyReplyButtonFromNotification(driver);
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			driver.navigate().back();
			driver.navigate().back();
			//Sending mail to click on Delete button from notification
			//inbox.verifyGmailAppInitState(driver);
			inbox.launchGmailApplication(driver);
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver,verifyNotificationReplyAndArchiveData.get("ToFieldData") );
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingGmailMailArchivefromNotificationFunctionality", " ");
			//navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			navDrawer.openUserAccountSetting(driver, emailId);
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			driver.navigate().back();

			Thread.sleep(3500);
			((AndroidDriver)driver).openNotifications();
			/*commonFunction.switchOnAirPlaneMode();
			Thread.sleep(5000);
			commonFunction.switchOffAirPlaneMode();*/
			commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForArchive"));
			commonPage.verifyArchiveFromNotification(driver);
			//driver.swipe(0, 0, 720, 1184, 1000);
			driver.navigate().back();
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "General settings");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Default notification action");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Delete");
			driver.navigate().back();
			driver.navigate().back(); //Nexus 6P  added by Venkat on 31/01/2019
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Default reply action");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Reply all");
			driver.navigate().back();
			driver.navigate().back();

			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver,verifyNotificationReplyAndArchiveData.get("ToFieldData") );
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingGmailMailDeletefromNotificationFunctionality", " ");
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			driver.navigate().back();

			Thread.sleep(3500);
			((AndroidDriver)driver).openNotifications();
			/*commonFunction.switchOnAirPlaneMode();
			Thread.sleep(5000);
			commonFunction.switchOffAirPlaneMode();*/
			commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForDelete"));
			commonPage.verifyDeleteFromNotification(driver);
			driver.navigate().back();
			String[] strarray=new String[]{"gm.gig1.auto@gmail.com","gm.gig2.auto@gmail.com"};
			mailmultiple.sendFromGMail(strarray, "TestingGmailMailReplyAllfromNotificationFunctionality", " ");
			driver.navigate().back();// Nexus 6P  // added by Venkat on 31/01/2019
			//navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("GIGAccount"));
			navDrawer.openUserAccountSetting(driver, emailId);
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			driver.navigate().back();

			Thread.sleep(3500);
			((AndroidDriver)driver).openNotifications();
			/*commonFunction.switchOnAirPlaneMode();
			Thread.sleep(5000);
			commonFunction.switchOffAirPlaneMode();*/
			commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForReplyAll"));
			commonPage.verifyReplyAllFromNotification(driver);
			driver.navigate().back();
			driver.navigate().back();
			inbox.launchGmailApplication(driver);
			log.info("*****************Completed testcase ::: verifyNotificationReplyAndArchive ***************************");

		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyNotificationReplyAndArchive");
		}
	}

	
	/**
	* Notification levels should be set to "Important Only"
	* 
	*  @author batchi
	* @throws: Exception
	*/
		@Test(enabled=true, priority=1)
		public void verifyNotificationImportantOnly() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try {
				log.info("***************** Started testcase ::: verifyNotificationImportantOnly ******************************");
				SoftAssert softAssert = new SoftAssert();
				Map<String, String> verifyReplyFromNotificationData = CommonPage.getTestData("verifyReplyFromNotification");
				Map<String, String> InboxTypes = CommonPage.getTestData("verifyInboxTypes");
				
				Map<String, String> SetNotification = CommonPage.getTestData("verifyNotificationSections");

				inbox.verifyGmailAppInitState(driver);
				
				String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFromNotificationData, "GIGAccount", null);

				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);

				navDrawer.openUserAccountSetting(driver, emailId);

				//userAccount.switchMailAccount(driver,verifyReplyFromNotificationData.get("GIGAccount"));
				//navDrawer.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("GIGAccount"));
				
				inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
				inbox.setNotificationLevel(driver, SetNotification.get("Important section only"));
				inbox.selectInboxType(driver, InboxTypes.get("Unreadfirst"));
				inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
				inbox.verifyNotificationSection(driver, SetNotification.get("Notify for inbox sections"), "Important section only");
			/*	
				String path = System.getProperty("user.dir");
				 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ImpUnImpMails.bat";
				 log.info("Executing the bat file for imp and unimp mails "+command);
				// Process p = new ProcessBuilder(command).start();
				 Runtime.getRuntime().exec(command);
				 log.info("Executed Bat file");
			*/
				
				String mailSubject = "ImpMail";
				CommonFunctions.smtpMailGeneration(emailId,mailSubject, "High", "-i");
				mailSubject = "UnImpMail";
				CommonFunctions.smtpMailGeneration(emailId,mailSubject, "UnImpMail", null);
				
				Thread.sleep(4000);
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				CommonFunctions.verifyPopUp(driver, null);
				Thread.sleep(5000);
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				CommonFunctions.verifyPopUp(driver, null);
				Thread.sleep(5000); 
				((AndroidDriver)driver).openNotifications();
				inbox.verifyMailNotification(driver);
				log.info("***************** Completed testcase ::: verifyNotificationImportantOnly ******************************");
				inbox.ClearNotification(driver);
				navDrawer.navigateUntilHamburgerIsPresent(driver);
				
			} catch (Throwable e) {
				  commonPage.catchBlock(e, driver, "verifyNotificationImportantOnly");
		}	
	}	
	

		/**
		*  Notification levels "None" when Important notification level to "important only"
		* 
		*  @author batchi
		* @throws: Exception
		*/
			@Test(enabled=true, priority=1)
			public void verifyNotificationNone() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				try {
					log.info("***************** Started testcase ::: verifyNotificationNone ******************************");
					SoftAssert softAssert = new SoftAssert();
					
					Map<String, String> verifyReplyFromNotificationData = CommonPage.getTestData("verifyReplyFromNotification");
					Map<String, String> InboxTypes = CommonPage.getTestData("verifyInboxTypes");
					
					Map<String, String> SetNotification = CommonPage.getTestData("verifyNotificationSections");

					String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFromNotificationData, "GIGAccount", null);
					
					inbox.verifyGmailAppInitState(driver);
					//userAccount.switchMailAccount(driver,verifyReplyFromNotificationData.get("GIGAccount"));// enabled by Venkat on 14/12/2018
					//navDrawer.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("GIGAccount"));
					
					if(!CommonFunctions.isAccountAlreadySelected(emailId))
						userAccount.switchMailAccount(driver, emailId);
					navDrawer.openUserAccountSetting(driver, emailId);

					inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
					inbox.setNotificationLevel(driver, SetNotification.get("Important section only"));
					inbox.selectInboxType(driver, InboxTypes.get("Unreadfirst"));
					inbox.setNotificationMain(driver, "None");
					inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
					inbox.verifyNotificationMain(driver, SetNotification.get("Notifications"), "None");
				/*	
					String path = System.getProperty("user.dir");
					 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ImpUnImpMails.bat";
					 log.info("Executing the bat file for imp and unimp mails "+command);
					// Process p = new ProcessBuilder(command).start();
					 Runtime.getRuntime().exec(command);
					 log.info("Executed Bat file");
				*/
					
					String mailSubject = "ImpMail";
					CommonFunctions.smtpMailGeneration(emailId,mailSubject, "High", "-i");
					mailSubject = "UnImpMail";
					CommonFunctions.smtpMailGeneration(emailId,mailSubject, "UnImpMail", null);

					
					 Thread.sleep(4000);
					CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
					Thread.sleep(1000);
					CommonFunctions.verifyPopUp(driver, null);
					CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
					Thread.sleep(1000);
					CommonFunctions.verifyPopUp(driver, null);
					((AndroidDriver)driver).openNotifications();
					inbox.verifyMailNotificationNone(driver);
					driver.navigate().back();	
					log.info("***************** Completed testcase ::: verifyNotificationNone ******************************");
					navDrawer.navigateUntilHamburgerIsPresent(driver);

				} catch (Throwable e) {
					  commonPage.catchBlock(e, driver, "verifyNotificationNone");
			}	
		}
			
			
			/**
			* Notification levels should be set to "All mail"
			* 
			*  @author batchi
			* @throws: Exception
			*/
				@Test(enabled=true, priority=1)
				public void verifyNotificationAllMail() throws Exception{
					driver = map.get(Thread.currentThread().getId());
					try {
						log.info("***************** Started testcase ::: verifyNotificationAllMail ******************************");
						SoftAssert softAssert = new SoftAssert();
						Map<String, String> verifyReplyFromNotificationData = CommonPage.getTestData("verifyReplyFromNotification");
						Map<String, String> InboxTypes = CommonPage.getTestData("verifyInboxTypes");
						
						Map<String, String> SetNotification = CommonPage.getTestData("verifyNotificationSections");

						String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFromNotificationData, "GIGAccount", null);
						
						inbox.verifyGmailAppInitState(driver);
						//userAccount.switchMailAccount(driver,verifyReplyFromNotificationData.get("GIGAccount")); //// enabled by Venkat on 14/12/2018
						//navDrawer.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("GIGAccount"));
						
						if(!CommonFunctions.isAccountAlreadySelected(emailId))
							userAccount.switchMailAccount(driver, emailId);
						navDrawer.openUserAccountSetting(driver, emailId);

						inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
						inbox.setNotificationMain(driver, "None");
						inbox.selectInboxType(driver, InboxTypes.get("Unreadfirst"));
						Thread.sleep(2600);
						inbox.setNotificationMain(driver, "All");
						Thread.sleep(2600);
						inbox.selectInboxType(driver, InboxTypes.get("Importantfirst"));
						Thread.sleep(2600);
						inbox.verifyNotificationSection(driver, SetNotification.get("Notify for inbox sections"), "All new mail");
						Thread.sleep(2600);
					/*	
						String path = System.getProperty("user.dir");
						 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ImpUnImpMails.bat";
						 log.info("Executing the bat file for imp and unimp mails "+command);
						// Process p = new ProcessBuilder(command).start();
						 Runtime.getRuntime().exec(command);
						 log.info("Executed Bat file");
					*/	 
						String mailSubject = "ImpMail";
						CommonFunctions.smtpMailGeneration(emailId,mailSubject, "High", "-i");
						mailSubject = "UnImpMail";
						CommonFunctions.smtpMailGeneration(emailId,mailSubject, "SwipeDownToRefresh.html", null);

						 Thread.sleep(1000);
						CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
						Thread.sleep(4000);
						CommonFunctions.verifyPopUp(driver, null);
						CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
						Thread.sleep(4000);
						CommonFunctions.verifyPopUp(driver, null);
						((AndroidDriver)driver).openNotifications();
						inbox.verifyMailNotificationAll(driver);
						driver.navigate().back();
						Thread.sleep(2000);
						driver.navigate().back();
						driver.navigate().back();
						
						log.info("***************** Completed testcase ::: verifyNotificationAllMail ******************************");
						navDrawer.navigateUntilHamburgerIsPresent(driver);

					} catch (Throwable e) {
						  commonPage.catchBlock(e, driver, "verifyNotificationAllMail");
				}	
			}

				
				/**
				* Notification level "None" when Important notification level to "All mail"
				* 
				*  @author batchi
				* @throws: Exception
				*/
					@Test(enabled=true, priority=2)
					public void verifyImpNotificationAllMail() throws Exception{
						driver = map.get(Thread.currentThread().getId());
						try {
							log.info("***************** Started testcase ::: verifyImpNotificationAllMail ******************************");
							SoftAssert softAssert = new SoftAssert();
							Map<String, String> verifyReplyFromNotificationData = CommonPage.getTestData("verifyReplyFromNotification");
							Map<String, String> InboxTypes = CommonPage.getTestData("verifyInboxTypes");
							
							Map<String, String> SetNotification = CommonPage.getTestData("verifyNotificationSections");

							String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFromNotificationData, "GIGAccount", null);
							
							inbox.verifyGmailAppInitState(driver);
							//userAccount.switchMailAccount(driver,verifyReplyFromNotificationData.get("GIGAccount")); // enabled by Venkat on 14/12/2018
							//navDrawer.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("GIGAccount"));
							
							if(!CommonFunctions.isAccountAlreadySelected(emailId))
								userAccount.switchMailAccount(driver, emailId);
							navDrawer.openUserAccountSetting(driver, emailId);

							inbox.selectInboxType(driver, "Important first");
							inbox.setNotificationLevel(driver, SetNotification.get("All new mail"));
							inbox.selectInboxType(driver, "Unread first");
							inbox.setNotificationMain(driver, "None");
							inbox.selectInboxType(driver, "Important first");
							inbox.verifyNotificationMain(driver, SetNotification.get("Notifications"), "None");
						/*
							String path = System.getProperty("user.dir");
							 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ImpUnImpMails.bat";
							 log.info("Executing the bat file for imp and unimp mails "+command);
							// Process p = new ProcessBuilder(command).start();
							 Runtime.getRuntime().exec(command);
							 log.info("Executed Bat file");
						*/
							
							
							String mailSubject = "ImpMail";
							CommonFunctions.smtpMailGeneration(emailId,mailSubject, "High", "-i");
							mailSubject = "UnImpMail";
							CommonFunctions.smtpMailGeneration(emailId,mailSubject, "SwipeDownToRefresh.html", null);

							Thread.sleep(10000);
							CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
							CommonFunctions.verifyPopUp(driver, null);
							Thread.sleep(4000);
							CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
							CommonFunctions.verifyPopUp(driver, null);
							Thread.sleep(6000);
							((AndroidDriver)driver).openNotifications();
							inbox.verifyMailNotificationNone(driver);
							driver.navigate().back();
							driver.navigate().back();

							driver.navigate().back();
							log.info("***************** Completed testcase ::: verifyImpNotificationAllMail ******************************");
							navDrawer.navigateUntilHamburgerIsPresent(driver);

						} catch (Throwable e) {
							  commonPage.catchBlock(e, driver, "verifyImpNotificationAllMail");
					}	
				}

	
}

