package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_AllInbox extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	HtmlEmailSender mail = new HtmlEmailSender();
	
/*	public void ExecuteAllInbox()throws Exception{
		verifyAllInboxBasicFunctionality();
	}
*/	
	/**
	 * @TestCase 4 : Verify All inbox basic functionality
	 * @Pre-condition : Mail with subject "InboxOne" should be available under
	 *                account one. Mail with subject "Inboxtwo" should be
	 *                available under account two. Mail with subject
	 *                "Inboxthree" should be available under account three. Mail
	 *                with subject "InboxOne" should be available under account
	 *                three.
	 * GMAT 39
	 * @throws: Exception
	 */
	@Test(groups= {"P1"}, enabled = true, priority = 99)
	public void verifyAllInboxBasicFunctionality() throws Exception {

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAllInboxBasicFunctionality***************");
			log.info("*****************Started testcase ::: verifyAllInboxBasicFunctionality***************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyAllInboxbasicFunctionalityData = commonPage
					.getTestData("verifyAllInboxbasicFunctionality");
			//Added By Phaneendra
			mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "etouchtestone@outlook.com", "InboxOneOutlook", "Test");
			mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "InboxTwoGig1", "Test");
			mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "gm.gig2.auto@gmail.com", "InboxThreeGig2", "Test");
			mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "gm.gig3.auto@gmail.com", "InboxFourGig3", "Test");
			//boolean rediffAcctAdded = userAccount.verifyGmailAccountIsPresent(verifyAllInboxbasicFunctionalityData.get("RediffID"));// Commented by Venkat on 7/12/2018
			Set<String> listOfConfiguredMailIds = userAccount.getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018
			boolean rediffAcctAdded = (listOfConfiguredMailIds.isEmpty()) ? true : listOfConfiguredMailIds.contains(verifyAllInboxbasicFunctionalityData.get("RediffID"));// added by Venkat on 7/12/2018	
			log.info("In Android_Gmail_AllInbox.verifyAllInboxBasicFunctionality rediffAcctAdded:"+rediffAcctAdded+" RediffID"+verifyAllInboxbasicFunctionalityData.get("RediffID"));
			if(!rediffAcctAdded){
			/*userAccount.addRediffAccount(driver,
					verifyAllInboxbasicFunctionalityData.get("RediffID"),
					verifyAllInboxbasicFunctionalityData.get("Password"));*/
				CommonFunctions.scrollToCellByTitle(driver, "Add another account");
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Add another account']");
				userAccount.addOutlookHotmailLiveAcct(driver, verifyAllInboxbasicFunctionalityData.get("RediffID"),
						verifyAllInboxbasicFunctionalityData.get("Password"));
				
			}
			Thread.sleep(1200);
			inbox.pullToReferesh(driver);
			Thread.sleep(1200);
			inbox.pullToReferesh(driver);
			/*	String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"verifyAllInboxBasicFunctionality.bat";
			log.info("Executing the bat file to generate a mail and move to other folder "+command);
			Process p = new ProcessBuilder(command).start();
			Thread.sleep(10000);*/
			if(!CommonFunctions.isAccountAlreadySelected(verifyAllInboxbasicFunctionalityData.get("RediffID")))
			userAccount.switchMailAccount(driver, verifyAllInboxbasicFunctionalityData.get("RediffID"));
			navDrawer.navigateToAllInbox(driver);
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject3"));
		
			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyOne"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);

			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyTwo"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
			
			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyThree"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject3"));
			commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);
			inbox.selectMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			inbox.selectMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			Thread.sleep(2000);
			inbox.verifyMailSelectionCount(driver,
					verifyAllInboxbasicFunctionalityData.get("SelectionCount"));
			commonFunction.navigateBack(driver);
			userAccount.setGmailDefaultActionToArchive(driver);

			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			userAccount.setGmailDefaultActionToDelete(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			inbox.openMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject4"));
			inbox.swipeToNextMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject4"));
			commonFunction.navigateBack(driver);
		//	userAccount.removeAccountPOP3(driver, verifyAllInboxbasicFunctionalityData.get("RediffID"));
			log.info("*****************Completed testcase ::: verifyAllInboxBasicFunctionality************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAllInboxBasicFunctionality");		
}
	}

}
