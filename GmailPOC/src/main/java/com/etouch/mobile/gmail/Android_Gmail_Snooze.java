package com.etouch.mobile.gmail;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })

public class Android_Gmail_Snooze extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	public static String MailSubject;
	public static String MailSubject1;
	public static String emailSubject;
	
	/**
	* @TestCase: Evoke Snooze from CV and threadlist
	* 
	*/
	@Test(groups = {"P0"}, enabled=true, priority=62)
	public void EvokeSnoozeCVTL() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		try {
			log.info("***************** Started testcase ::: EvokeSnoozeCVTL ******************************");
			inbox.verifyGmailAppInitState(driver);
			inbox.multiSelectMailNew(driver);
			inbox.viewMoreOptionsCV(driver);
			userAccount.SnoozeGrid(driver);
			CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[contains(@text,'Later today')]");
			inbox.tapOnFirstMail(driver);
			String[] x = UserAccount.getSubjectOfMail(driver, MailSubject, emailSubject);
			for(int i=0; i<=x.length; i++){
				MailSubject = x[0].replace("Inbox", " ");
				MailSubject1 = MailSubject.replace(" .", "");
				log.info(MailSubject1);
				emailSubject = x[1];
			}
			inbox.viewMoreOptionsCV(driver);
			userAccount.SnoozeGrid(driver);
			userAccount.SnoozeMail(driver, emailSubject);
			log.info("Email Subject is:"+emailSubject);
			CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[contains(@text,'Undo')]");
			Thread.sleep(3000);
			userAccount.verifyMailIsPresent(driver, MailSubject1.trim());
			Thread.sleep(3000);
			inbox.tapOnFirstMail(driver);
			x = UserAccount.getSubjectOfMail(driver, MailSubject, emailSubject);
			for(int i=0; i<=x.length; i++){
				MailSubject = x[0].replace("Inbox", " ");
				MailSubject1 = MailSubject.replace(" .", "");
				log.info(MailSubject1);
				emailSubject = x[1];
			}
			inbox.viewMoreOptionsCV(driver);
			userAccount.SnoozeGrid(driver);
			userAccount.SnoozeMail(driver, emailSubject);
			navDrawer.navigateToSnooze(driver);
			userAccount.verifyMailIsPresent(driver, MailSubject1.trim());
			log.info("***************** Completed testcase ::: EvokeSnoozeCVTL ******************MailSubject1.trim():"+MailSubject1.trim());
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "EvokeSnoozeCVTL");
			//e.printStackTrace();
	}
	}
	
	/**Searching for is:snoozed
	 * 
	 * For this test case to run, first execute above test case "EvokeSnoozeCVTL()"  
	 */
	@Test(groups = {"P0"}, enabled=true, priority=63)
	public void SearchSnoozedMail() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		try {
			log.info("***************** Started testcase ::: SearchSnoozedMail **********************MailSubject1:"+MailSubject1);
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToPrimary(driver);
			//userAccount.verifyMailIsPresent(driver, (MailSubject1 != null ? MailSubject1.trim() : MailSubject1)); //Commented by Venkat on 22/11/2018
			userAccount.verifyMailNotPresent(driver, (MailSubject1 != null ? MailSubject1.trim() : MailSubject1));//added by Venkat on 22/11/2018
			inbox.searchForMail(driver, "is:snoozed");
			
			userAccount.verifyMailIsPresent(driver, MailSubject1.trim());
			log.info("***************** Completed testcase ::: SearchSnoozedMail ******************************");
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "SearchSnoozedMail");
			//e.printStackTrace();
	}
	}
	
	
	/**Unsnooze a mail
	 * 
	 * For this test case to run, first execute above test case "EvokeSnoozeCVTL()"  
	 */
	@Test(groups = {"P0"}, enabled=true, priority=64)
	public void UnsnoozeMail() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		try {
			log.info("***************** Started testcase ::: UnsnoozeMail ******************************");
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToSnooze(driver);
			//userAccount.verifyMailIsPresent(driver, MailSubject1);
			//userAccount.UnSnoozeMail(driver, MailSubject1); //Commented by Venkat on 22/11/2018
			userAccount.UnSnoozeMail(driver, ( MailSubject1 != null) ? MailSubject1.trim() : MailSubject1);//added by Venkat on 22/11/2018
			navDrawer.navigateToPrimary(driver);
			userAccount.verifyMailIsPresent(driver, MailSubject1.trim());
			log.info("***************** Completed testcase ::: UnsnoozeMail ******************************");
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "UnsnoozeMail");
			//e.printStackTrace();
	}
	}
	
	
	
	/**Remembering custom last snoozed date and time in snooze dialog
	 * 
	 */
	@Test(groups = {"P0"}, enabled=true, priority=65)
	public void RemCustomeLastSnoozeDateTime() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		try {
			log.info("***************** Started testcase ::: RemCustomeLastSnoozeDateTime ******************************");
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToPrimary(driver);
			inbox.tapOnFirstMail(driver);
			inbox.viewMoreOptionsCV(driver);
			userAccount.SnoozeGrid(driver);
			userAccount.SetCustomSnooze(driver);
			//userAccount.RadialTimeNumber(driver);
			/*navDrawer.navigateToSnooze(driver);
			userAccount.verifyMailIsPresent(driver, MailSubject);*/
			Thread.sleep(1200);
			inbox.tapOnFirstMail(driver);
			inbox.viewMoreOptionsCV(driver);
			userAccount.SnoozeGrid(driver);
			userAccount.verifyRememberLastSnooze(driver);
			driver.navigate().back();
			driver.navigate().back();
			log.info("***************** Completed testcase ::: RemCustomeLastSnoozeDateTime ******************************");
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "RemCustomeLastSnoozeDateTime");
			//e.printStackTrace();
	}
	}
	
}
