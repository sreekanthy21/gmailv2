package com.etouch.mobile.gmail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.common.MailManager;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = {
    "GmailAppData=testData"
})

public class PerformanceTest extends BaseTest {
    Date timeStamp = new Date();
    static Log log = LogUtil.getLog(GmailTest.class);
    AppiumDriver driver = null;
    SoftAssert softAssert = new SoftAssert();

    @Test
    public void performanceTest() throws Exception {
        driver = map.get(Thread.currentThread().getId());
        Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
        Map < String, String > composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
        Map < String, String > markMailReadUnread = commonPage.getTestData("markMailReadUnread");
        Map < String, String > mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
        Map < String, String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
        Map < String, String > verifyReplyFromNotificationData = commonPage.getTestData("verifyReplyFromNotification");
        Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
        Map < String, String > verifyMailConversationView = new HashMap < String, String > ();
        verifyMailConversationView = commonPage.getTestData("verifyMailConversationView");
        Map < String, String > verifyHelpAndFeedbackSectionData = CommonPage.getTestData("verifyHelpAndFeedbackSection");
        Map < String, String > verifyReplyFunctionalityData = commonPage.getTestData("verifyReplyAllFunctionality");
        Map < String, String > verifyForwardingMailFunctionalityData = commonPage.getTestData("verifyForwardingMailFunctionality");
        Map < String, String > verifyArchiveAndDeletingMultipleMailsData = commonPage.getTestData("verifyArchiveAndDeletingMultipleMails");
        Map < String, String > verifyForwardingMailFunctionality = commonPage.getTestData("verifySwipeDeleteFunctionality");
        Map < String, String > verifyOpenAndCloseDrawerData = commonPage.getTestData("SystemLabels");
        Map < String, String > accountDrawerData = commonPage.getTestData("accountDrawer");
		Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage.getTestData("verifyTwoAdsDisplayInPromotionSocial");
		Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");
		Map<String, String> verifyMultipleLinesInBodyData1 = commonPage.getTestData("verifyAttachmentFunctionality");
		Map<String, String> verifyMultipleLinesInBodyData = commonPage.getTestData("verifyReplyFromNotification");
		Map<String, String> verifyDocumentInAppData = commonPage.getTestData("verifyAttachmentsUponRotating");
		Map<String, String> verifyPicoProjectorFilePreviewData = commonPage.getTestData("verifyAttachmentsUponRotating");
		Map<String, String> verifyMailSnippetInThreadListData = commonPage.getTestData("verifyMailSnippetInThreadList");
		Map<String, String> richTextFormattingFeatures = commonPage.getTestData("richTextFormattingFeatures");	
		Map<String, String> deleteMailCheckTrashData = commonPage.getTestData("deleteMailCheckTrash");
		Map<String, String> verifyDeleteLuncherShortcutData = commonPage.getTestData("verifyLauncherShortcut");
		Map<String, String> verifyNotificationOffForSpecificAccountData = commonPage.getTestData("NotificationSettingOff");
		Map<String, String> verifyAdvertismentData = commonPage.getTestData("verifyAdvertisementFunctionality");
		/**---Install/Upgrade-Downgrade, Add new gmail account, compose and send mail with CC,BCC, 
         *    Verify attachment on rotating, compose with attachments, compose and save as draft, multiple attachments from drive
         *    edit draft and send mail, verify conversation view, smart reply suggestions, after replying
         *    helpandfeedback, mark read/unread, starmail, change label, move mail to other folder, markmailimportant,
         *    sync for google accounts, swipe down, search functionality, verify richtext format, trash/spam
         *  
         */
        Process p;
        
	     /* log.info("*****************Started testcase ::: verifyAddingNewGmailAccount*****************************");

	        boolean isGmailWelComePageDisplayed = userAccount
	            .isGmailWelComePageDisplayed(driver);
	        if (isGmailWelComePageDisplayed) {
	            userAccount.addNewAccountFromGmailHomePage(driver);
	        }else{
	        	log.info("**********Started testcase ::: Removing account********************");
	        	userAccount.removeAccount(driver, verifyAddingNewGmailAccountData.get("UserList"), "");
	        	log.info("**********Completed testcase ::: Removing account********************");
	       // userAccount.launchGmailApplication(driver);
	        }
	       
	        String expectedAccountTypeList = verifyAddingNewGmailAccountData.get("ExpectedAccountTypeList");
	        userAccount.addNewMultipleGmailAccount(driver,
	            verifyAddingNewGmailAccountData.get("UserList"),
	            verifyAddingNewGmailAccountData.get("Password"),
	            isGmailWelComePageDisplayed, expectedAccountTypeList);
	        driver.navigate().back();
	        userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
	        log.info("**********Started testcase :: verify test account sync for google account************");
	        inbox.verifyContentPresent(driver);
	        log.info("**********Completed testcase :: verify test account sync for google account************");
	        log.info("*****************Completed testcase ::: verifyAddingNewGmailAccount*************************");
 
	       log.info("***************Started testcase :: Navigate to settings ***********");
	        inbox.navigateToSettings(driver);
	        inbox.navigateUntilHamburgerIsPresent(driver);
	       */ log.info("***************Started testcase :: Verify general app settings ***********");
	        String path4 = System.getProperty("user.dir");
			String command4 = path4+java.io.File.separator+"Mail"+java.io.File.separator+"GeneralSettings.bat";
			Process p4 = new ProcessBuilder(command4).start();
			log.info("Executed the bat file to generate a mail");
			Thread.sleep(6000);
			inbox.pullToReferesh(driver);
	        userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
	        userAccount.navigateToGeneralSettings(driver);
	        userAccount.verifyGeneralSettingsOptions(driver);
	        log.info("***************Completed testcase :: Verify general app settings***********");
	        log.info("***************Completed testcase :: Navigate to settings ***********");
	       driver.navigate().back();
	    		driver.navigate().back();
	        log.info("******************************Started testcase :: Gmail Account Settings****************** ");
	        userAccount.navigateToSettings(driver);
	        
	        userAccount.verifyAccountSettings(driver);
	       // userAccount.navigateToSettings(driver);
	        
	        inbox.turnOffAccountSync(driver);
	        log.info("******************************Started testcase :: Notifications Off in Account Settings****************** ");
	        userAccount.settingNotificationOff(driver, verifyNotificationOffForSpecificAccountData.get("User1"));
	    	String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"TestingNotificationsOff.bat";
			p = new ProcessBuilder(command).start();
			log.info("Executed the bat file to generate a mail");
			Thread.sleep(2000);
			((AndroidDriver)driver).openNotifications();
			commonPage.verifyAvailabilityOfNotifications(driver);
			
			log.info("***************Completed testcase :: Notifications Off in  Account Settings***********");
	        log.info("***************Completed testcase :: Gmail Account Settings***********");

	        
	       log.info("***************** Started testcase ::: ApkUpgrade/Downgrade ******************************");
	        Android_Gmail_ApkInstallUninstall apk = new Android_Gmail_ApkInstallUninstall();
	        apk.verifyDowngradeOfGmailApp();
	       // driver.navigate().back();
	       // driver.navigate().back();
	        apk.verifyUpgradeOfGmailApp();
	        log.info("***************** Completed testcase ::: ApkUpgrade/Downgrade ******************************");

	        log.info("*********************Started Testcase :: Open and close navigation drawer**********");
	        inbox.openMenuDrawer(driver);
	        commonPage.swipeRightToLeft(driver, verifyOpenAndCloseDrawerData.get("LabelViewID"));
	        inbox.openMenuDrawer(driver);
	        log.info("As menu drawer is clickable, the swipe function worked to close the menu drawer");
	        commonPage.hamburgerDrawerLableView(driver);
	        commonPage.hamburgerDrawerAccountView(driver);
	        inbox.openMenuDrawer(driver);
	        commonPage.verifyAccountView(driver);
	        commonPage.swipeRightToLeft(driver, verifyOpenAndCloseDrawerData.get("LabelViewID"));
	        log.info("**************Completed Testcae :: Open and close navigation drawer*************");
	       
	        log.info("**********Started testcase :: Verifying the Account Drawer**********");
			inbox.openMenuDrawer(driver);
			
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Social"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Promotions"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Primary"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Starred"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Trash"));
	        log.info("**********Completed testcase :: Verifying the Account Drawer**********");

	       log.info("*****************Started testcase ::: verifyComposeAndSendMail***************************");
		       Thread.sleep(5000);
		        userAccount.switchMailAccount(driver,
		            verifyComposeAndSendMailData.get("AccountOne"));

		        //log.info("Drats mail are deleting");
		        //MailManager.DeleteAllMailsDrafts();
		        //log.info("Drats mail are deleted");
		        log.info("***************** Started testcase ::: verifyMultipleLinesInBodyWithAttchament ******************************");
		     // Multiple Lines  in compose body added by bindu
				String composeInMultipleLines=verifyMultipleLinesInBodyData.get("ComposeBodyData1").replaceAll(",", "\n");;
				//inbox.verifyGoogleDriveAppInitState(driver);
				String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
				//attachment.addFileToDrive(driver, fileName);
		        Thread.sleep(2000);
		      //  inbox.launchGmailApplication(driver);
		        log.info("Drats mail are deleting");
		        MailManager.DeleteAllMailsDrafts();
		        log.info("Drats mail are deleted");
		        String fromUser = inbox.composeAsNewMail(driver,
		            verifyComposeAndSendMailData.get("ToFieldData"),
		            verifyComposeAndSendMailData.get("CCFieldData"), "",
		            verifyComposeAndSendMailData.get("SubjectData"),
		            composeInMultipleLines);
		        
		        log.info("*****************Completed testcase ::: verifyComposeAndSendMail***************************");

		        log.info("***************** Started testcase ::: verifyAttachmentsUponRotating ******************************");
		        
		        //inbox.openDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		        attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		        attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		        attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	           // Added new attachment to verify File sharing options while sending
		        //attachment.addAttachmentFromDrive(driver, fileName);
	            commonPage.orientationOfScreen(driver);
		        Thread.sleep(2000);
		        commonFunction.hideKeyboard();
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
		        commonFunction.scrollUp(driver, 2);
		        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
		        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
		        log.info("***************** Completed testcase ::: verifyAttachmentsUponRotating ******************************");
	            driver.navigate().back();
		         inbox.openMenuDrawer(driver);
		        navDrawer.navigateToDrafts(driver);
		        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
		        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
				commonFunction.scrollDown(driver, 1);
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
		        log.info("----------------Verified attachments in drafts--------------------");
		        //Check below code --Phani, edited: No need of the below code -- Bindu//
				        // driver.navigate().back();
				        // userAccount.switchMailAccount(driver,fromUser);
					   // inbox.openMenuDrawer(driver);
				       // inbox.scrollDown(1);
				       // inbox.verifyMailInSentBox(driver,verifyComposeAndSendMailData.get("SubjectData"));
				       
		        log.info("CLickEditDraft");
		        commonPage.scrollUp(2);
		        inbox.clickEditDraft(driver);
		        userAccount.tapOnSend(driver);
		       // userAccount.verifySharingOptionPopUpIsPresent();
			//	inbox.tapOnSendSharingAlert(driver);
				inbox.verifyDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        Thread.sleep(4000);
		        driver.navigate().back();
		        inbox.verifyMailInSentBox(driver,
		            verifyComposeAndSendMailData.get("SubjectData"));
		        userAccount.switchMailAccount(driver,
		            verifyComposeAndSendMailData.get("ToFieldData"));
		        navDrawer.navigateToPrimary(driver);
		        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
		        log.info("-------------------Verifying the attachements-----------------------------");
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
		     
		      
		        log.info("******************Started testcase ::verifyDocumentsInApp*************");
		        Process p7;
		        String[] googleSheetsInstall = new String[]{"adb","install", "E:/Gmail-POC-GmailTest_Automation/GmailPOC/Drive_Apk/Sheets.apk"};
			    p7 = new ProcessBuilder(googleSheetsInstall).start();
			    log.info("install  Sheets apk");
			    String[] googleSlidesInstall = new String[]{"adb","install", "E:/Gmail-POC-GmailTest_Automation/GmailPOC/Drive_Apk/Slides.apk"};
			    p7 = new ProcessBuilder(googleSlidesInstall).start();
			    log.info("install  Slides apk");
			    Thread.sleep(3000);
			    //inbox.launchGmailApplication(driver);
			    //inbox.navigateUntilHamburgerIsPresent(driver);
			    userAccount.switchMailAccount(driver, verifyDocumentInAppData.get("ToMailIdData"));
				inbox.openMailUsingSearch(driver, verifyDocumentInAppData.get("mailSubjectData")); 
				Thread.sleep(2000);
				if(commonFunction.getSizeOfElements(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"))==0){
					commonPage.scrollDown(1);
					log.info("Scrolled down to click on attachment");
				}
			    attachment.openingAttachments(driver,verifyDocumentInAppData.get("ExcelAttachmentName"));
				attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("ExcelAttachmentName"),
						                                               verifyDocumentInAppData.get("ExcelType"),
					                                                   verifyDocumentInAppData.get("OpenAttachmentSheetsOption"));
				attachment.openingAttachments(driver,verifyDocumentInAppData.get("DocsAttachmentName"));
				attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("DocsAttachmentName"),
		                verifyDocumentInAppData.get("DocsType"),
		                verifyDocumentInAppData.get("OpenAttachmentDocsOption"));
		    	 attachment.openingAttachments(driver,verifyDocumentInAppData.get("SlidesAttachementName"));
				attachment.verifyOpenAttachmentInRespectiveApps(driver,verifyDocumentInAppData.get("SlidesAttachementName"),
		                verifyDocumentInAppData.get("SlidesType"),
		                verifyDocumentInAppData.get("OpenAttachmentSlidesOption"));

				log.info("*****************Completed testcase ::: verifyDocumentsInApp*************************");
				
			 
			    log.info("***************** Started testcase ::: verifyPicoProjectorFilePreview ******************************");
				Thread.sleep(2000);
				String[] googleSheetsUnInstall = new String[]{"adb","uninstall", "com.google.android.apps.docs.editors.sheets"};
			    p = new ProcessBuilder(googleSheetsUnInstall).start();
			    log.info("uninstall  Sheets apk");
			    String[] googleSlidesUnInstall = new String[]{"adb","uninstall", "com.google.android.apps.docs.editors.slides"};
			    p = new ProcessBuilder(googleSlidesUnInstall).start();
			    log.info("uninstall  Slides apk");
			    
			    //userAccount.switchMailAccount(driver,verifyPicoProjectorFilePreviewData.get("ToMailIdData"));
				//inbox.openMailUsingSearch(driver, verifyPicoProjectorFilePreviewData.get("mailSubjectData"));
				//all attachments in pico  view
				//Thread.sleep(2000);
				if(commonFunction.getSizeOfElements(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"))==0){
					commonPage.scrollDown(2);
					log.info("Scrolled down to click on attachment");
				}
			    attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
				attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
				//Commented all docs related steps as docs app cannot be uninstalled -- Bindu
  			   //	attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
			   //  attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
				attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
				attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
				attachment.openingAttachments(driver,verifyPicoProjectorFilePreviewData.get("PdfAttachmentName"));
				attachment.verifyOpenAttachmentInPicoView(driver,verifyPicoProjectorFilePreviewData.get("PdfAttachmentName"));
				//uneditable mode
				attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
			//	attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
				attachment.verifyAttachmentInUneditableMode(driver,verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
				//Add-to-drive option
				//Need to verify the add to drive option is not currently available
				attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("ExcelAttachmentName"));
				//attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("DocsAttachmentName"));
				attachment.verifyAddToDriveOption(driver, verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
				//able to scroll down in pico viewer
				attachment.verifyDocumentIsScrollable(driver, verifyPicoProjectorFilePreviewData.get("SlidesAttachementName"));
				log.info("***************** Completed testcase ::: verifyPicoProjectorFilePreview ******************************");
	       
	      log.info("*****************Started testcase ::: verifyMailSnippetInThreadList******************");
	      
	      //inbox.searchForMail(driver,
	    	//	  verifyPicoProjectorFilePreviewData.get("mailSubjectData"));
			inbox.openMail(driver, verifyPicoProjectorFilePreviewData.get("mailSubjectData"));
	      softAssert.assertTrue(commonPage.getScreenshotAndCompareImage("verifyMailSnippetInThreadList"),
					"Image comparison for verifyMailSnippetInThreadList");
	      driver.navigate().back();
			inbox.openMail(driver,verifyPicoProjectorFilePreviewData.get("mailSubjectData"));
			conversationView.verifyRecipientDetails(driver,verifyMailSnippetInThreadListData.get("alertTitle"));
			Thread.sleep(3000);
			softAssert.assertTrue(commonPage.getScreenshotAndCompareImage("verifyMailSnippetInThreadListMailView"),
							"Image comparison for verifyMailSnippetInThreadListMailView");
		   commonFunction.navigateBack(driver);
		  
			log.info("*****************Completed testcase ::: verifyMailSnippetInThreadList********************");
			
		  log.info("******************Started testcase :: Reply/Reply All functionality*************");
	      inbox.openMail(driver, verifyComposeAndSendMailData.get("SubjectData"));   
	      inbox.composeMailAsReply(
	            driver,
	            verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"),
	            verifyReplyFunctionalityData.get("AccountOne"));
	        inbox.insertInLineText(driver,
	            verifyReplyFunctionalityData.get("InLineData"));
	        inbox.tapOnSend(driver);
	        driver.navigate().back();

	        log.info("Verifying Mail is sent");
	        inbox.verifyMailInSentBox(driver,
	            verifyComposeAndSendMailData.get("SubjectData"));
	        Thread.sleep(2000);
	        userAccount.switchMailAccount(driver,
	            verifyReplyFunctionalityData.get("AccountOne"));
	        inbox.openMail(driver,
	            verifyComposeAndSendMailData.get("SubjectData"));

	        inbox.verifyReplyInlineTextIsPresent(driver, verifyReplyFunctionalityData
	            .get("InLineDataVerify"));
	        inbox.composeMailAsReplyAll(driver,
	            verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"));
	        inbox.tapOnSend(driver);
	        commonFunction.navigateBack(driver);

	        inbox.verifyMailInSentBox(driver,
	            verifyComposeAndSendMailData.get("SubjectData"));
		      log.info("******************Completed testcase :: Reply/Reply All functionality*************");

	        log.info("******************Started testcase ::Super collapse functionality*************");
	        inbox.openMail(driver,
		            verifyComposeAndSendMailData.get("SubjectData"));
            inbox.composeMailAsReplyAll(driver,
		            verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"));
		        inbox.tapOnSend(driver);
		    commonFunction.navigateBack(driver);
		    inbox.openMail(driver,
			            verifyComposeAndSendMailData.get("SubjectData"));
		        
	        conversationView.verifySuperCollapseCountOfMail(driver);
	        log.info("******************Completed testcase ::Super collapse functionality*************");
	      
	        
	        //The below TC cannot be included in this flow as the mail should consist of Table -- Bindu
	        log.info("******************Started testcase ::Zooming out tables for CV functionality*************");
	        commonFunction.scrollTo(driver, "Forward");
	        inbox.tapOnShowQuotedText(driver);
			//commonFunction.scrollTo(driver, "Forward");
			commonPage.scrollDown(1);
	        softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyZoomingOutTablesForConversationView"),
							"Image comparison for verifyZoomingOutTablesForConversationView");
			log.info("******************Completed testcase ::Zooming out tables for CV functionality*************");
	        //Thread.sleep(3000);
	        
	        log.info("******************Started testcase :: Forward functionality*************");
	        String path6 = System.getProperty("user.dir");
			String command6 = path6+java.io.File.separator+"Mail"+java.io.File.separator+"ForwardHTML.bat";
			Process p5 = new ProcessBuilder(command6).start();
			log.info("Executed the bat file to generate a mail");
			inbox.launchGmailApplication(driver);
	        userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne") );
	        inbox.openMail(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	        // edited the subject and placed above this comment -- Bindu
			        //inbox.openMail(driver,verifyComposeAndSendMailData.get("SubjectData"));
			        
	        inbox.composeMailAsFowarding(driver,
	            verifyForwardingMailFunctionalityData.get("AccountThree"),
	            "", "", verifyForwardingMailFunctionalityData
	            .get("ComposeBodyData"));
	        log.info("******************Started testcase :: Forward functionality HTML mail*************");
	        inbox.forwardAnHtmlEmail(driver);
	        log.info("******************Completed testcase :: Forward functionality HTML mail*************"); 
	        inbox.composeMailAsFowarding(driver,verifyForwardingMailFunctionalityData.get("AccountThree"),
		            "", "", verifyForwardingMailFunctionalityData.get("ComposeBodyData"));
	        
	        //Added attachements, check again whether required or not//
	        attachment
	            .addAttachmentFromDrive(driver,
	                verifyForwardingMailFunctionalityData
	                .get("AttachmentName"));
	        inbox.insertInLineText(driver,
	            verifyForwardingMailFunctionalityData.get("InLineData"));
	        inbox.tapOnSend(driver);
	        commonFunction.navigateBack(driver);
	        inbox.verifyMailInSentBox(driver,
	        		composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	        //inbox.openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	        userAccount.switchMailAccount(driver,
	            verifyForwardingMailFunctionalityData.get("AccountThree"));
	        inbox.openMail(driver,
	        		composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	        
	       attachment
	            .verifyAttachmentIsAddedSuccessfully(driver,
	                verifyForwardingMailFunctionalityData
	                .get("AttachmentName"));
	        log.info("******************Completed testcase :: Forward functionality*************");
	       driver.navigate().back();
	         
		
		    log.info("*****************Started testcase ::: verifyArchiveMail*****************************");
		    inbox.archiveMail(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"), null);
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"));
			log.info("*****************Completed testcase ::: verifyArchiveMail*************************");
			
	        log.info("*****************Started testcase ::: deleteMailCheckTrash*****************************");
	        inbox.deleteMailPrimary(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"), null);
			inbox.verifySnackBarDeleted(driver);
			navDrawer.navigateToTrashFolder(driver);
			inbox.verifyMailTrashFolder(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
			log.info("*****************Completed testcase ::: deleteMailCheckTrash*************************");
			
	        log.info("**************Started Testcae :: Archive and Delete multiple mails*************");
	        userAccount.switchMailAccount(driver, verifyReplyFunctionalityData.get("AccountOne"));
	        String path1 = System.getProperty("user.dir");
	        String command1 = path1 + java.io.File.separator + "Mail" + java.io.File.separator + "ArchiveMailsTestOne.bat";
	        log.info("Executing the bat file " + command1);
	        Process p1 = new ProcessBuilder(command1).start();
	        log.info("Executed the bat file to generate a mail");
	        Thread.sleep(5000);
	        userAccount.navigateToGeneralSettings(driver);
	        inbox.checkSenderImageCheckBox(driver);
	        commonFunction.navigateBack(driver);
	        commonFunction.navigateBack(driver);
	        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	            .get("ArchiveMail1"));
	        Thread.sleep(1000);
	        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	            .get("ArchiveMail2"));
	        Thread.sleep(1000);
	        inbox.verifyMailSelectionCount(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("MailSelectionCount"));
	        navDrawer.tapOnArchive(driver);
	        navDrawer.navigateToAllEmails(driver);

	        inbox.verifyMailIsPresent(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("ArchiveMail1"));
	        inbox.verifyMailIsPresent(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("ArchiveMail2"));

	        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	            .get("DeleteMail1"));
	        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
	            .get("DeleteMail2"));

	        inbox.verifyMailSelectionCount(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("MailSelectionCount"));
	        navDrawer.tapOnDelete(driver);
	        navDrawer.navigateToBin(driver, verifyArchiveAndDeletingMultipleMailsData.get("BinOption"));

	        inbox.verifyMailIsPresent(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("DeleteMail1"));
	        inbox.verifyMailIsPresent(driver,
	            verifyArchiveAndDeletingMultipleMailsData
	            .get("DeleteMail2"));
	        log.info("**************Completed Testcae :: Archive and Delete multiple mails*************");
//Verifying this in general app settings
//	        log.info("*****************Started testcase ::: Swipe to Archive and Delete***************");
//	        driver = map.get(Thread.currentThread().getId());
//	        String path2 = System.getProperty("user.dir");
//	        String command2 = path1 + java.io.File.separator + "Mail" + java.io.File.separator + "verifySwipeDeleteFunctionality.bat";
//	        log.info("Executing the bat file " + command1);
//	        Process p2 = new ProcessBuilder(command1).start();
//	        log.info("Executed the bat file to generate a mail");
//
//	        userAccount.navigateToPrimary(driver);
//	        userAccount.setGmailDefaultActionToArchive(driver);
//	        inbox.swipeToDeleteOrArchiveMail(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectArchive"));
//	        inbox.undoSwipeDeleteArchiveOperation(driver);
//	        inbox.verifyMailIsPresent(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectArchive"));
//	        inbox.swipeToDeleteOrArchiveMail(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectArchive"));
//	        navDrawer.navigateToAllEmails(driver);
//	        inbox.verifyMailIsPresent(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectArchive"));
//	        userAccount.setGmailDefaultActionToDelete(driver);
//	        navDrawer.openPrimaryInbox(driver,
//	            verifyForwardingMailFunctionality.get("Accountthree"));
//	        inbox.swipeToDeleteOrArchiveMail(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectDelete"));
//	        inbox.undoSwipeDeleteArchiveOperation(driver);
//	        inbox.verifyMailIsPresent(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectDelete"));
//	        inbox.swipeToDeleteOrArchiveMail(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectDelete"));
//	        navDrawer.navigateToBin(driver, verifyForwardingMailFunctionality.get("BinOption"));
//	        inbox.verifyMailIsPresent(driver,
//	            verifyForwardingMailFunctionality.get("MailSubjectDelete"));
//	        //commonFunction.navigateBack(driver);
//	        log.info("*****************Completed testcase ::: Swipe to Archive and Delete************");

	        
	        log.info("*****************Started testcase ::: verifyMailConversationView***************");
	        userAccount.switchMailAccount(driver,
	            verifyMailConversationView.get("AccountFour"));
	        inbox.searchForMail(driver,
	            verifyMailConversationView.get("MailSubject"));
	        inbox.openMail(driver,
	            verifyMailConversationView.get("MailSubject"));
	        commonFunction.scrollDown(driver, 1);
	        inbox.tapOnShowQuotedText(driver);
	        inbox.tapOnHideQuotedText(driver);
	        commonFunction.scrollUp(driver, 1);

	        conversationView.verifyRecipientDetails(driver,
	            verifyMailConversationView.get("alertTitle"));
	        conversationView.actionBarConversationView(driver);
	        commonFunction.navigateBack(driver);
	        attachment.openAndValidateAttachment(driver,
	            verifyMailConversationView.get("AttachmentName"));
	        // Device get hang while opening chrome browser
	      
	        //Removed here as we are verify in chrome custom tabs feature
	          //inbox.openLinkInChrome(driver,verifyMailConversationView.get("LinkText"), verifyMailConversationView.get("InAppBrowserMoreOptionsOpenInChrome"));
	        // commonFunction.navigateBack(driver);
	        Thread.sleep(3000);
	        conversationView.tapOnRecipientSummary(driver);
	        conversationView.tapOnEmailSnippet(driver);

	        conversationView.actionBarConversationView(driver);
	        commonFunction.navigateBack(driver);
	        inbox.verifySubjectOfMail(driver,
	            verifyMailConversationView.get("MailSubject"));
	        commonFunction.navigateBack(driver);
	        commonFunction.navigateBack(driver);
	        commonFunction.navigateBack(driver);
	        log.info("*****************Completed testcase ::: verifyMailConversationView********************");

	       Map < String, String > smartReplySuggestionsData = commonPage.getTestData("smartReplySuggestions");
	        log.info("***************** Started testcase ::: verifySmartReplySuggestions ******************************");

	        //userAccount.verifyAccountsExists(driver, smartReplySuggestionsData.get("Account"), smartReplySuggestionsData.get("AccountPassword"));
	        userAccount.switchMailAccount(driver, smartReplySuggestionsData.get("Account"));
	      //  navDrawer.openPrimaryInbox(driver, smartReplySuggestionsData.get("Account"));
	        navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account"));
	        userAccount.verifySmartReplyEnabled(driver, true);
	        userAccount.deleteMailExistPrimary(driver, smartReplySuggestionsData.get("EmailSubject"));
	        inbox.verifyMailSmartReplySuggestions(driver, smartReplySuggestionsData.get("EmailSubject"));
	        userAccount.verifySmartReplySuggestionsNotDisplayed(driver);
	        log.info("*****************Completed testcase ::: verifySmartReplySuggestions*************************");
	        driver.navigate().back();


	        log.info("***************** Started testcase ::: verifyTrashandSpamFolders ******************************");
	        userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
	        //Ensure all items in Trash folder are deleted
	        log.info("************Verifying the Trash folder functionality*************");
	        navDrawer.navigateToTrashFolder(driver);
	        userAccount.verifyTrashFolderForMails(driver);
	        inbox.emptyTrashOrSpamNow(driver, "Trash");
	        log.info("*********Verified all mails are deleted from Trash folder**************");

	        //Ensure all items in Spam folder are deleted
	        log.info("*********Verifying the Spam folder functionality***********");
	        navDrawer.navigateToSpamFolder(driver);
	        userAccount.verifySpamFolderForMails(driver, null);
	        inbox.emptyTrashOrSpamNow(driver, "Spam");
	        log.info("*********Verified all mails are deleted from Spam folder**************");

	        log.info("*****************Completed testcase ::: verifyTrashandSpamFolders*************************");
			
			       log.info("***************** Started testcase ::: RichTextFormatting ******************************");
	        userAccount.navigateToPrimary(driver);
	        inbox.richTextFormattingFeatures(driver);
	        log.info("*****************Completed testcase ::: RichTextFormatting*************************");
	        driver.navigate().back();
	        driver.navigate().back();
	        log.info("***************** Started testcase ::: MailReadUnread ******************************");
	        driver.navigate().back();
	       navDrawer.navigateToPrimary(driver);

	        Thread.sleep(3000);
	        CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("SelectMailImage"));
	        Thread.sleep(1000);
	        if (CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("ReadIcon")) != 0) {
	            log.info("Mail is unread");
	            CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("ReadIcon"));
	            Thread.sleep(1000);
	            if (CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("UnReadIcon")) != 0) {
	                log.info("Mail is read");
	            }
	        } else if (CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("UnReadIcon")) != 0) {
	            log.info("Mail is read");
	            CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("UnReadIcon"));
	            Thread.sleep(1000);
	            if (CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("ReadIcon")) != 0) {
	                log.info("Mail is Unread");
	            }
	        }
	        	CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("SelectedMailSenderImage")); 
	        log.info("*****************Completed testcase ::: MailReadUnread*************************");

	        
	        log.info("***************** Started testcase ::: StarMail ******************************");
	        inbox.verifyStarredMail(driver);
	        log.info("*****************Completed testcase ::: StarMail*************************");

	        
	        log.info("***************** Started testcase ::: ChangeLabels ******************************");
	        // Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

	        // inbox.verifyGmailAppInitState(driver);
	        userAccount.verifyAccountsExists(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"),
	            composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AcctPwd"));
	        userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"));
	       
		inbox.ChangeLabel(driver);
	        log.info("*****************Completed testcase ::: ChangeLabels*************************");

	        log.info("***************** Started testcase ::: MoveMailToOtherFolder ******************************");
	     //   userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
	      //  navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
	       navDrawer.navigateToPrimary(driver);
	        
	        inbox.moveMailToFolder(driver, mailMoveToOtherFolderData.get("EmailSubject"), mailMoveToOtherFolderData.get("FolderName"), null);
	        navDrawer.navigateToSocial(driver);
	        inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubject"));
	        log.info("*****************Completed testcase ::: MoveMailToOtherFolder*************************");

	        
	        log.info("***************** Started testcase ::: verifyMarkAsImportantInCV ******************************");
	        String command3 = "E:\\Gmail-POC-GmailTest_Automation\\GmailPOC\\Mail\\MarkMailImportant.bat";
	        Process p3 = new ProcessBuilder(command3).start();
	        log.info("Executed Bat file");
	      //  Thread.sleep(4000);
	        //userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
	      //  navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
	        navDrawer.navigateToPrimary(driver);        
	        inbox.verifyMarkAsImportant(driver, mailMoveToOtherFolderData.get("MailImportant"));
	        log.info("*****************Completed testcase ::: verifyMarkAsImportantInCV*************************");
	        driver.navigate().back();
	        driver.navigate().back();
	       
	        log.info("****************************Started testcase :: Ads displayed in Promotions/Social************************");
	       // inbox.verifyTwoAdsDisplayInPromotionSocial(driver);
	        //CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			inbox.openMenuDrawer(driver);
	        CommonFunctions.searchAndClickByXpath(driver, verifyTwoAdsDisplayInPromotionSocial.get("promotion"));
			log.info("Clicked on Promtotions");
			CommonFunctions.isElementByXpathDisplayed(driver, verifyTwoAdsDisplayInPromotionSocial.get("verifyad1"));
			log.info("Ad is displayed promotion folder");
			CommonFunctions.searchAndClickByXpath(driver, verifyTwoAdsDisplayInPromotionSocial.get("verifyad1"));		
			CommonFunctions.isElementByXpathDisplayed(driver, verifyTwoAdsDisplayInPromotionSocial.get("Verifyad1displayed"));
			log.info("Ad is dispalyed in promotions");
		    log.info("***************Completed testcase ::: Ads displayed in Promotions/Social*****************************"); 
		      log.info("*****************Started testcase ::: verifyExpandAndControlAds***************************");
        promotions.verifyControlAds(driver, "PromotionsInboxOption");
        log.info("*****************Completed  testcase ::: verifyExpandAndControlAds***************************");
       
        log.info("*****************Started testcase ::: verifyPromotionsTabRedignforADS**************");
        driver.navigate().back();
        promotions.verifyPromotionsAdDisplayed(driver);
        softAssert.assertTrue(commonPage.getScreenshotAndCompareImage("verifyPromotionsTabRedignforADS"),"Image comparison for verifyPromotionsTabRedignforADS");
        log.info("*****************Completed  testcase ::: verifyPromotionsTabRedignforADS**************");
        
        log.info("*****************Started testcase ::: verifyAdvertisementFunctionality**************");
	    log.info("Verify Ad functionality for Promotions Inbox option");
		promotions.verifyAdForward(driver,verifyAdvertismentData.get("AccountOne"),"PromotionsInboxOption",
				verifyAdvertismentData.get("ToFieldData"),
				verifyAdvertismentData.get("ComposeBodyData"));
		promotions.verifyAdDelete(driver, "PromotionsInboxOption");

		softAssert.assertTrue(promotions.verifyAd_StarAd(driver,
						"PromotionsInboxOption",
						"verifyAdvertisementFunctionalityPromotions"),
						"Image comparision failed for verifyAdvertisementFunctionalityPromotions");
		log.info("*****************Completed  testcase ::: verifyAdvertisementFunctionality**************");   
		    driver.navigate().back();
		

	        log.info("***************** Started testcase ::: Search functionality ******************************");
	        Android_Gmail_Search search = new Android_Gmail_Search();
	        search.verifySearchFunctionality();
	        log.info("*****************Completed testcase ::: Search functionality*************************");

	        log.info("***************** Started testcase ::: Report spam for imap account ******************************");
			 inbox.validateReportSpamForIMAPAccounts(driver);
	        log.info("*****************Completed testcase ::: Report spam for imap account*************************");
			 driver.navigate().back();

	        
	        log.info("***************** Started testcase ::: Help and feedback ******************************");
	        Android_Gmail_HelpAndFeedback helpFeedback = new Android_Gmail_HelpAndFeedback();
	        helpFeedback.verifyHelpAndFeedBackTab();
	        userAccount.ClickToSendFeedback(driver);
	        userAccount.verifyforEditingAndSentFeedback(driver, verifyHelpAndFeedbackSectionData.get("feedbackText"));
	       // Thread.sleep(4000);
	       // softAssert
	       //     .assertTrue(
	       //         commonPage
	       //         .getScreenshotAndCompareImage("verifyHelpAndFeedbackSection"),
	       //         "Image comparison for verifyHelpAndFeedbackSection");

	        //softAssert.assertAll();
	        Thread.sleep(3000);
	        //	commonFunction.navigateBack(driver);
	        driver.navigate().back();
	        driver.navigate().back();
	       log.info("*****************Completed testcase ::: Help and feedback************");
	log.info("***************************Started testcase :: Spam features**************************");
	  Android_Gmail_SpamFeatures spamfeature = new Android_Gmail_SpamFeatures();
	  inbox.launchGmailApplication(driver);
	  spamfeature.removeUnsyncableLablesSpam();
	  spamfeature.verifyBlockAndUnblockSenderWorks();
	  log.info("***************************Completed testcase :: Spam features**************************");
	  driver.navigate().back();  
	  
	  
	  log.info("***************************Started testcase :: verifyEnableDisableChromeTabs**************************");
		Android_Gmail_ChromeCustom_Tabs ccTabs= new Android_Gmail_ChromeCustom_Tabs();
		ccTabs.verifyEnableDisableChromeTabs();	
		log.info("*****************Completed testcase ::: verifyEnableDisableChromeTabs*************************");
		
		log.info("******************Started testcase ::: verifyGmailifyFlow******************");
		
		userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
		
		userAccount.verifyAccountIsLinked(driver, gmailifyFlowData.get("ImapEmail"));
		
		userAccount.linkAccountGmailify(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("GmailAccount"), 
				gmailifyFlowData.get("GmailPassword"), gmailifyFlowData.get("ImapEmailPwd"));
		
		userAccount.verifyGmailifyAccount(driver, gmailifyFlowData.get("ImapEmail"));
				
		log.info("*****************Completed testcase ::: verifyGmailifyFlow************");
		
		
		log.info("******************Started testcase ::: verifyTryGmailifyFlow******************");
		Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
		userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
		
		userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
				tryGmailifyFlowData.get("IMAPEmailPwd"));
		userAccount.tryGmailifySetUpIMAP(driver);
		//userAccount.verifyExchangeAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
		userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));			
       //Label list for Gmailified account
		log.info("*****************Completed testcase ::: verifyTryGmailifyFlow************");

		log.info("******************Started testcase ::: verifyTheLabelListForGmailifiedAccount******************");
		inbox.verifyTheLabelListForGmailifiedAccount(driver);
		log.info("*****************Completed testcase ::: verifyTheLabelListForGmailifiedAccount************");
		
		log.info("*****************Started testcase ::: certificateTransparency***************************");
		inbox.certificateTransparency(driver);
		log.info("*****************Completed testcase ::: certificateTransparency*************************");
		
		
		log.info("******************Started testcase ::: vacation responser and signature on/off*****************");
		Android_Gmail_Settings settings = new Android_Gmail_Settings();
		settings.enableDisableVacationResponder();
		settings.enableVacationResponderWithSendToMyContactsOption();
		settings.verifySignatureOnAndOff();
	//	settings.notificationSettingsForSystemLabels();
		log.info("*****************Completed testcase ::: vacation responser and signature on/off************");
		
		

		log.info("******************Started testcase ::: RSVP*****************");
		Android_Gmail_RSVP rsvp = new Android_Gmail_RSVP();
		rsvp.verifyNewRSVPFormatDisplayedForCalendarInvites();
		log.info("******************Started testcase ::: RSVP*****************");

		
	log.info("******************Started testcase ::: SMIME_TLS_DKIM_SPF*****************");
	Android_Gmail_SMIME_TLS_DKIM_SPF mime = new Android_Gmail_SMIME_TLS_DKIM_SPF();
	mime.verifyInboundAndOutbound();
	driver.navigate().back();
	driver.navigate().back();
	mime.verifyIconsAreUpdated();
	mime.verifyAllPossibleCasesForSMIME();
	log.info("*****************Completed testcase ::: SMIME_TLS_DKIM_SPF************");
	

    
    log.info("******************Started testcase ::: Discard Drafts *****************");
    Android_Gmail_Draft drafts = new Android_Gmail_Draft();
	drafts.verifyDiscardDraft();
	log.info("*****************Completed testcase ::: Discard Drafts ************");
	
	log.info("******************Started testcase ::: Conversation view *****************");
	Android_Gmail_ConversationView conversationView= new Android_Gmail_ConversationView();
    conversationView.verifyZoomingOutTablesForConversationView();
    conversationView.verifyCalendarPromotionInConversation();
    conversationView.verifySmartReplySuggestionsAfterDiscarding();
    conversationView.verifyAttachments();
    log.info("*****************Completed testcase ::: Conversation view ************");
    
    log.info("******************Started testcase ::: Mail Actions  *****************");
    Android_Gmail_MailAction mailAction = new Android_Gmail_MailAction();
    mailAction.reportSpam();
    mailAction.verifyUserIsAbleToMuteAConversation();
    mailAction.verifyMailSavedAsPDFPrint();
    log.info("*****************Completed testcase ::: Mail Actions ************");
  
    
    log.info("******************Started testcase ::: Gmailify  *****************");
    Android_Gmail_Gmailify gmailify= new Android_Gmail_Gmailify();
  //  gmailify.verifyGmailifyPasswordFlow();
    log.info("*****************Completed testcase ::: Gmailify ************");
    
    
	log.info("******************Started testcase ::: Notifications*****************");
	Android_Gmail_Notification notifications = new Android_Gmail_Notification();
	notifications.verifyNotificationReplyAndArchive();
	log.info("*****************Completed testcase ::: Notifications************");
	log.info("******************Started testcase ::: LauncherShortCut*****************");
	Android_Gmail_LauncherShortCut launcher = new Android_Gmail_LauncherShortCut();
	launcher.verifyLauncherShortcut();
	launcher.verifyCreateLauncherShortcut();
	launcher.verifyDeleteLuncherShortcut();
	launcher.verifyReenabledShortcut();
	
	log.info("*****************Completed testcase ::: LauncherShortCut************");
	
	log.info("******************Started testcase ::: Android N *****************");
	Android_Gmail_AndroidNMultiWindow androidN = new Android_Gmail_AndroidNMultiWindow();
	androidN.verifyMailSendMultiWindow();
	androidN.verifyMultipleMailsNotificationsReplyAndArchive();
	androidN.verifyArchieveDeleteAndMoveToFromMultiWindow();
	androidN.verifyMailSendMultiWindowInLandscapeMode();
	
	log.info("*****************Completed testcase ::: Android N ************");
    }
}