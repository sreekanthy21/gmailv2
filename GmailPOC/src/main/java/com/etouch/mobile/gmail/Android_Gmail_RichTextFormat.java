package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_RichTextFormat extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
/*	public void ExecuteAllRichTextFormat()throws Exception{
		richTextFormattingFeatures();
	}*/
	
	/**
	 * @TestCase 9 :Rich text formatting features /**
		 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * GMAT 76
	 * @author Rahul
	 */	 
	@Test(groups = {"P0"}, enabled = true, priority = 46)
   public void richTextFormattingFeatures() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: richTextFormattingFeatures*************************");
			inbox.verifyGmailAppInitState(driver);
			inbox.richTextFormattingFeatures(driver);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			   log.info("*****************Completed testcase ::: richTextFormattingFeatures*****************************");	
} catch (Throwable e) {
					}
	}
	
	
}
