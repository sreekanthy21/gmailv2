package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_HelpAndFeedback extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void ExecuteAllHelpAndFeedback()throws Exception{
		verifyHelpAndFeedBackTab();
		verifyHelpAndFeedbackSection();
	}
*/	
	/**Testcase to verify the Help and Feedback tab is opened and displayed Help page
	 * 
	 * @preCondition Gmail app installed
					 Gmail account is added to app
	 * @author Phaneendra
	 * GMAT 42
	 */
	@Test(groups = {"P1"}, enabled = true, priority=6)
	public void verifyHelpAndFeedBackTab() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			log.info("******************Started testcase ::: verifyHelpAndFeedBackTab******************");
			
			Map<String, String> verifyHelpAndFeedbackSectionData = CommonPage
					.getTestData("verifyHelpAndFeedbackSection");
		//	userAccount.switchMailAccount(driver, verifyHelpAndFeedbackSectionData.get("AccountTwo"));
			userAccount.openMenuDrawer(driver);
			userAccount.openHelpAndFeedbackSection(driver);
			userAccount.verifyHelpDisplayed(driver,verifyHelpAndFeedbackSectionData.get("HelpText"));
			userAccount.verifyAllFeedbackOptions(driver);
			log.info("*****************Completed testcase ::: verifyHelpAndFeedBackTab************");

		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyHelpAndFeedBackTab");
		}
	}
	
	
	/**Testcase to verify the feedback functionality
	 * 
	 * @throws Exception
	 * GMAT 43
	 * @author Phaneendra
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 7)
	public void verifyHelpAndFeedbackSection() throws Exception {
		try {
			SoftAssert softAssert = new SoftAssert();
			driver = map.get(Thread.currentThread().getId());
			//inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyHelpAndFeedbackSection******************");
			Map<String, String> verifyHelpAndFeedbackSectionData = CommonPage
					.getTestData("verifyHelpAndFeedbackSection");
			/*	userAccount.switchMailAccount(driver, verifyHelpAndFeedbackSectionData.get("AccountTwo"));
			userAccount.openMenuDrawer(driver);
			userAccount.openHelpAndFeedbackSection(driver);*/
			userAccount.openMenuDrawer(driver); // added by Venkat on 20-Nov-2018
			userAccount.openHelpAndFeedbackSection(driver); // added by Venkat on 20-Nov-2018
			userAccount.verifyHelpDisplayed(driver,verifyHelpAndFeedbackSectionData.get("HelpText"));
			userAccount.ClickToSendFeedback(driver);
			userAccount.verifyforEditingAndSentFeedback(driver,verifyHelpAndFeedbackSectionData.get("feedbackText"));
			/*Thread.sleep(4000);
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage("verifyHelpAndFeedbackSection"),
					"Image comparison for verifyHelpAndFeedbackSection");

			softAssert.assertAll();
			Thread.sleep(3000);*/
		//	commonFunction.navigateBack(driver);
			
			log.info("*****************Completed testcase ::: verifyHelpAndFeedbackSection************");
			} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyHelpAndFeedbackSection");
		}
	}

}
