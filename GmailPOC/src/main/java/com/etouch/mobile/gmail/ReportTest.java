package com.etouch.mobile.gmail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.springframework.aop.ThrowsAdvice;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.common.MailManager;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = {
    "GmailAppData=testData"
})

public class ReportTest extends BaseTest {
    Date timeStamp = new Date();
    static Log log = LogUtil.getLog(GmailTest.class);
    AppiumDriver driver = null;
    SoftAssert softAssert = new SoftAssert();
    boolean isGmailWelComePageDisplayed;
    boolean flag;
    String path = System.getProperty("user.dir");
    Process p;
    
    Android_Gmail_Compose compose = new Android_Gmail_Compose();
    Android_Gmail_ConversationView convView = new Android_Gmail_ConversationView();



 @Test(priority = 1)
 public void verifyRemoveAccount()throws Exception{
	 try{
			driver = map.get(Thread.currentThread().getId());

		    Map < String, String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
	        Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

		 log.info("*****************Started testcase ::: RemoveAccount*****************************");

		 isGmailWelComePageDisplayed = userAccount
	            .isGmailWelComePageDisplayed(driver);
	        if (isGmailWelComePageDisplayed) {
	            userAccount.addNewAccountFromGmailHomePage(driver);
	        } else {
	            userAccount.removeAccount(driver, verifyAddingNewGmailAccountData.get("UserList"), "");
	            Thread.sleep(4000);
	           // userAccount.launchGmailGoApplication(driver);
	        }
			 log.info("*****************Completed testcase ::: RemoveAccount*****************************");

	 }catch(Exception e){
	 commonPage.catchBlock(e, driver, "verifyAddingNewAccountGmail");
 }
 }
 
 @Test(priority = 2)
 public void verifyAddingNewAccountGmail()throws Exception{
	 try{
			driver = map.get(Thread.currentThread().getId());
	        log.info("*****************Started testcase ::: verifyAddingNewGmailAccount*************************");

		    Map < String, String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
	        Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

		 String expectedAccountTypeList = verifyAddingNewGmailAccountData.get("ExpectedAccountTypeList");
	        userAccount.addNewMultipleGmailAccount(driver,
	            verifyAddingNewGmailAccountData.get("UserList"),
	            verifyAddingNewGmailAccountData.get("Password"),
	            isGmailWelComePageDisplayed, expectedAccountTypeList);
	        driver.navigate().back();
	        
	        if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("AccountOne")))
	        userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
	        log.info("*****************Completed testcase ::: verifyAddingNewGmailAccount*************************");
	 }catch(Exception e){
		  commonPage.catchBlock(e, driver, "verifyAddingNewAccountGmail");
	 }
 }

 @Test(priority = 3)
  public void verifySyncAccount()throws Exception{
	  try{
			driver = map.get(Thread.currentThread().getId());
			 log.info("***************Started testcase :: Turn Off sync ***********");

			 inbox.verifyContentPresent(driver);
			 log.info("***************Started testcase :: Turn Off sync ***********");

	  }catch(Exception e){
		  commonPage.catchBlock(e, driver, "verifySyncAccount");
	  }
  }  
 
 @Test(priority = 4)
 public void verifyNavigateSettings()throws Exception{
		driver = map.get(Thread.currentThread().getId());
	 try{
		 log.info("***************Started testcase :: Navigate to settings ***********");

	        inbox.navigateToSettings(driver);
	        inbox.navigateUntilHamburgerIsPresent(driver);
	        log.info("***************Completed testcase :: Navigate to settings ***********");
	 }catch(Exception e){
		 commonPage.catchBlock(e, driver, "verifyNavigateSettings");
	 }
 }
    
@Test(priority = 5)
public void verifyGeneralAppSettings()throws Exception{
	driver = map.get(Thread.currentThread().getId());
	try{
        Map < String, String > gmailifyFlowData = commonPage.getTestData("gmailifyFlow");

		log.info("***************Started testcase :: Verify general app settings ***********");
    
        String command4 = path + java.io.File.separator + "Mail" + java.io.File.separator + "GeneralSettingsGig.bat";
        p = new ProcessBuilder(command4).start();
        log.info("Executed the bat file to generate a mail");
        inbox.pullToReferesh(driver);
        
        userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
        userAccount.navigateToGeneralSettings(driver);
        userAccount.verifyGeneralSettingsOptions(driver);
        log.info("***************Completed testcase :: Verify general app settings***********");
        driver.navigate().back();
	}catch(Exception e){
		 commonPage.catchBlock(e, driver, "verifyGeneralAppSettings");
	}
}

@Test(priority = 6)
public void verifyAccountSpecificSettings()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
		log.info("******************************Started testcase :: Gmail Account Settings****************** ");
        
       // driver.navigate().back();
		inbox.launchGmailApplicationLite(driver);
		userAccount.navigateToSettings(driver);

        userAccount.verifyAccountSettings(driver);
        // userAccount.navigateToSettings(driver);

        log.info("***************Completed testcase :: Gmail Account Settings***********");
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyAccountSpecificSettings");
	}
}


Android_Gmail_ApkInstallUninstall apk = new Android_Gmail_ApkInstallUninstall();

@Test(priority = 7)
public void verifyDowngradeApplication()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
		 log.info("***************** Started testcase ::: Downgrade ******************************");
	       inbox.verifyGmailAppInitState(driver);
		 	apk.verifyDowngradeOfGmailGoApp();
	        // driver.navigate().back();
	        // driver.navigate().back();
	        log.info("***************** Completed testcase ::: Downgrade ******************************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyDowngradeApplication");
	}
}

@Test(priority = 8)
public void verifyUpgradeApplication()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
        log.info("***************** Started testcase ::: ApkUpgrade ******************************");

		apk.verifyUpgradeOfGmailGoApp();
        log.info("***************** Completed testcase ::: ApkUpgrade ******************************");
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyUpgradeApplication");
	}
}

@Test(priority = 9)
public void verifyDiscardDraft()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
		log.info("******************Started testcase ::: Discard Drafts *****************");
        Android_Gmail_Draft drafts = new Android_Gmail_Draft();
        drafts.verifyDiscardDraft();
        log.info("*****************Completed testcase ::: Discard Drafts ************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyDiscardDraft");
	}
}

@Test(priority = 10)
public void verifyArchiveDeleteMultipleMails()throws Exception{
    Map < String, String > verifyArchiveAndDeletingMultipleMailsData = commonPage.getTestData("verifyArchiveAndDeletingMultipleMails");
    Map < String, String > verifyReplyFunctionalityData = commonPage.getTestData("verifyReplyAllFunctionality");
	driver = map.get(Thread.currentThread().getId());

	try{
		inbox.verifyGmailAppInitState(driver);
		log.info("**************Started Testcae :: Archive and Delete multiple mails*************");
		
		if(!CommonFunctions.isAccountAlreadySelected(verifyReplyFunctionalityData.get("AccountOne")))
        userAccount.switchMailAccount(driver, verifyReplyFunctionalityData.get("AccountOne"));
        String path1 = System.getProperty("user.dir");
        String command1 = path1 + java.io.File.separator + "Mail" + java.io.File.separator + "ArchiveMailsTestOneGig.bat";
        log.info("Executing the bat file " + command1);
        Process p1 = new ProcessBuilder(command1).start();
        log.info("Executed the bat file to generate a mail");
        Thread.sleep(5000);
        userAccount.navigateToGeneralSettings(driver);
        inbox.checkSenderImageCheckBox(driver);
        commonFunction.navigateBack(driver);
        commonFunction.navigateBack(driver);
        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
            .get("ArchiveMail1"));
        Thread.sleep(1000);
        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
            .get("ArchiveMail2"));
        Thread.sleep(1000);
        inbox.verifyMailSelectionCount(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("MailSelectionCount"));
        navDrawer.tapOnArchive(driver);
        navDrawer.navigateToAllEmails(driver);

        inbox.verifyMailIsPresent(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("ArchiveMail1"));
        inbox.verifyMailIsPresent(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("ArchiveMail2"));

        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
            .get("DeleteMail1"));
        inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
            .get("DeleteMail2"));

        inbox.verifyMailSelectionCount(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("MailSelectionCount"));
        navDrawer.tapOnDelete(driver);
        navDrawer.navigateToBin(driver, verifyArchiveAndDeletingMultipleMailsData.get("BinOption"));

        inbox.verifyMailIsPresent(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("DeleteMail1"));
        inbox.verifyMailIsPresent(driver,
            verifyArchiveAndDeletingMultipleMailsData
            .get("DeleteMail2"));
        log.info("**************Completed Testcae :: Archive and Delete multiple mails*************");
		
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyArchieveDeleteMultipleMails");
	}
}

@Test(priority = 11)
public void verifySmartReplySuggestions()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
		 Map < String, String > smartReplySuggestionsData = commonPage.getTestData("smartReplySuggestions");
	        log.info("***************** Started testcase ::: verifySmartReplySuggestions ******************************");

	        //userAccount.verifyAccountsExists(driver, smartReplySuggestionsData.get("Account"), smartReplySuggestionsData.get("AccountPassword"));
	        
	        if(!CommonFunctions.isAccountAlreadySelected(smartReplySuggestionsData.get("Account")))
	        	userAccount.switchMailAccount(driver, smartReplySuggestionsData.get("Account"));
	      //  navDrawer.openPrimaryInbox(driver, smartReplySuggestionsData.get("Account"));
	        navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account"));
	        userAccount.verifySmartReplyEnabled(driver, true);
	        userAccount.deleteMailExistPrimary(driver, smartReplySuggestionsData.get("EmailSubject"));
	        inbox.verifyMailSmartReplySuggestions(driver, smartReplySuggestionsData.get("EmailSubject"));
	        log.info("*****************Completed testcase ::: verifySmartReplySuggestions*************************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifySmartReplySuggestions");
	}
}

@Test(priority = 12)
public void verifySmartReplyAfterReplying()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
        log.info("***************** Started testcase ::: verifySmartReplySuggestionsAfterReply ******************************");

        userAccount.verifySmartReplySuggestionsNotDisplayed(driver);
        log.info("***************** Completed testcase ::: verifySmartReplySuggestionsAfterReply ******************************");
        driver.navigate().back();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifySmartReplyAfterReplying");
	}
}
Android_Gmail_ConversationView conversationView = new Android_Gmail_ConversationView();

@Test(priority = 13)
public void verifySmartReplyAfterDiscard()throws Exception{
	driver = map.get(Thread.currentThread().getId());

	try{
        conversationView.verifySmartReplySuggestionsAfterDiscarding();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifySmartReplyAfterDiscard");
	}
}

@Test(priority = 14)
public void verifyComposeCCBCCSendMail()throws Exception{
	try{
		compose.verifyComposeAndSendMail();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyComposeCCBCCSendMail");
	}
}

@Test(priority = 15)
public void verifyComposeMailAttachmentsSaveDraft()throws Exception{
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
    Map < String, String > verifyMultipleLinesInBodyData = commonPage.getTestData("verifyReplyFromNotification");
    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
	driver = map.get(Thread.currentThread().getId());

	try{
		
		String fromUser = inbox.composeAsNewMail(driver,
	            verifyComposeAndSendMailData.get("ToFieldData"),
	            verifyComposeAndSendMailData.get("CCFieldData"), " ",
	            verifyComposeAndSendMailData.get("SubjectData1"),
	            verifyComposeAndSendMailData.get("ComposeBodyData"));

	        log.info("***************** Started testcase ::: verifyComposeMailAttachmentsSaveDraft ******************************");

	        //inbox.openDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	      //  attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        Thread.sleep(2000);
	        commonFunction.hideKeyboard();
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        commonFunction.scrollUp(driver, 2);
	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	        log.info("***************** Completed testcase ::: verifyComposeMailAttachmentsSaveDraft ******************************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyComposeMailAttachmentsSaveDraft");
	}
}


@Test(priority = 16)
public void verifyMailSavedAsDraft()throws Exception{
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");

    driver = map.get(Thread.currentThread().getId());
	try{
			log.info("***************** Started testcase ::: verifyMailSavedAsDraft ******************************");
			inbox.saveMailAsDraft(driver);
	        log.info("Saved as draft from menu options");
	        inbox.openMenuDrawer(driver);
	        navDrawer.navigateToDrafts(driver);
	        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	        commonFunction.scrollDown(driver, 1);
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        log.info("----------------Verified attachments in drafts--------------------");
	        log.info("***************** Completed testcase ::: verifyMailSavedAsDraft ******************************");
	        driver.navigate().back();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyMailSavedAsDraft");
	}
}

@Test(priority = 17)
public void verifyComposeMailWithAttachmentsAndSend()throws Exception{
	driver = map.get(Thread.currentThread().getId());
	// MailManager.DeleteAllMailsInbox();
	Map < String,
	String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
	Map < String,
	String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
	try {
		userAccount.verifyAccountsExists(driver, verifyAddingNewGmailAccountData.get("GIGAccountsExists"), verifyAddingNewGmailAccountData.get("GIGPassword"));
		if(!CommonFunctions.isAccountAlreadySelected(verifyAddingNewGmailAccountData.get("GIGAccount")))
			userAccount.switchMailAccount(driver, verifyAddingNewGmailAccountData.get("GIGAccount"));
		inbox.composeAndSendMailForGmailAccountWithAttachmentsGIG(driver);
		//compose.composeAndSendMailForGmailAccountWithAttachments();	
		driver.navigate().back();
		
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyComposeMailWithAttachmentsAndSend");
	}
}

@Test(priority = 18)
public void verifyAttachmentsOnRotation()throws Exception{
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
    Map < String, String > verifyMultipleLinesInBodyData = commonPage.getTestData("verifyReplyFromNotification");
    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
	driver = map.get(Thread.currentThread().getId());

	try{
        String composeInMultipleLines = verifyMultipleLinesInBodyData.get("ComposeBodyData1").replaceAll(",", "\n");;

		String fromUser = inbox.composeAsNewMail(driver,
	            verifyComposeAndSendMailData.get("ToFieldData"),
	            verifyComposeAndSendMailData.get("CCFieldData"), "",
	            verifyComposeAndSendMailData.get("SubjectData1"),
	            composeInMultipleLines);

	        log.info("***************** Started testcase ::: verifyAttachmentsUponRotating ******************************");

	        //inbox.openDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	      //  attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        // Added new attachment to verify File sharing options while sending
	      //  attachment.addAttachmentFromDrive(driver, fileName);
	        commonPage.orientationOfScreen(driver);
	        Thread.sleep(2000);
	        commonFunction.hideKeyboard();
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        commonFunction.scrollUp(driver, 2);
	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	        log.info("***************** Completed testcase ::: verifyAttachmentsUponRotating ******************************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyAttachmentsOnRotation");
	}
}

@Test(priority = 19)
public void verifyMultipleLinesAttachments()throws Exception{
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");

    driver = map.get(Thread.currentThread().getId());
	try{
			driver.navigate().back();
	       // userAccount.
	        log.info("***************** Started testcase ::: verifyMultipleLinesAttachments ******************************");

	        log.info("Navigated back as mail is saved as draft");
	        inbox.openMenuDrawer(driver);
	        navDrawer.navigateToDrafts(driver);
	        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	        commonFunction.scrollDown(driver, 1);
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	        log.info("----------------Verified attachments in drafts--------------------");
	        log.info("***************** Completed testcase ::: verifyMultipleLinesAttachments ******************************");
	
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyMultipleLinesAttachments");
	}
}

@Test(priority = 20)
public void verifyEditDraftSendMail()throws Exception{
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");

    driver = map.get(Thread.currentThread().getId());
	try{
        log.info("***************** Started testcase ::: verifyEditDraftSendMail ******************************");

		log.info("CLickEditDraft");
        commonPage.scrollUp(2);
        inbox.clickEditDraft(driver);
        userAccount.tapOnSend(driver);
        Thread.sleep(4000);
        driver.navigate().back();
        userAccount.verifyDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData1"));
        inbox.verifyMailInSentBox(driver,
            verifyComposeAndSendMailData.get("SubjectData1"));
        log.info("***************** Completed testcase ::: verifyEditDraftSendMail ******************************");

	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyEditDraftSendMail");
	}
}

Android_Gmail_MailAction mailAction = new Android_Gmail_MailAction();
@Test(priority = 21)
public void verifyArchiveMail()throws Exception{
    driver = map.get(Thread.currentThread().getId());
    Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

	try{
		if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
		mailAction.verifyArchiveMail();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyArchiveMail");
	}
}

@Test(priority = 22)
public void verifyStarMail()throws Exception{
    driver = map.get(Thread.currentThread().getId());

	try{

		mailAction.verifyMailMarkedAsStar();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyStarMail");
	}
}

@Test(priority = 23)
public void verifyMailReadUnread()throws Exception{
	try{
		mailAction.markMailReadUnread();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyMailReadUnread");
	}
}

@Test(priority = 24)
public void verifyDeleteMailTrash()throws Exception{
	try{
		mailAction.deleteMailCheckTrash();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyDeleteMailTrash");
	}
}

@Test(priority = 25)
public void verifyEmptyTrashSpamFunctionality()throws Exception{
	try{
		mailAction.verifyTrashandSpamFolders();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyEmptyTrashSpamFunctionality");
	}
}

@Test(priority = 26)
public void verifyReplyNotification()throws Exception{
    driver = map.get(Thread.currentThread().getId());

	try{
		compose.verifyReplyFromNotificationGmailGo();
		driver.navigate().back();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyReplyNotification");
	}
}


@Test(priority = 27)
public void verifyZoomingOutTables()throws Exception{
    driver = map.get(Thread.currentThread().getId());
	try{
		
	    convView.verifyZoomingOutTablesForConversationView();
		
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyZoomingOutTables");
	}
}

@Test(priority = 28)
public void verifyReplyReplyAll()throws Exception{
    driver = map.get(Thread.currentThread().getId());

	try{
			convView.verifyReplyAllFunctionality();
	}catch(Exception e){
		commonPage.catchBlock(e, driver, "verifyReplyReplyAll");
	}
}
}