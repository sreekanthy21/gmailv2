package com.etouch.mobile.gmail;

public interface TraveAndPurchases {
	
	public static final String TRAVEL = "Travel";
	public static final String PURCHASES = "Purchases";
	public static final String UPDATES = "Updates";
	public static final String OK = "OK";
	public static final String CANCEL = "Cancel";
	public static final String CHECKED = "checked";
	public static final String TEXT = "text";
	public static final String CHECK_IN = "Check in";
	public static final String ALL_TRIPS = "All trips";
	public static final String HOTELS_BOOKING_CONFIRMATION = "Hotels.com booking confirmation";
	public static final String VIEW_OR_EDIT_IN_TRIPS = "View/edit in Trips";
	public static final String MODIFY_RESERVATION = "Modify reservation";
	public static final String CHANGE_LABELS = "Change labels";
	public static final String MOVE_TO = "Move to";
	public static final String FLIGHT_RESERVATION= "flight_reservation";
	public static final String PARCEL_DELIVERY= "parcel_delivery";
	public static final String ALL_TRIPS_LOCATOR = "//android.view.View[contains(@text,'All trips')]";
	public static final String ANDROID_WIDGET_TEXTVIEW= "//android.widget.TextView[contains(@text,'%s')]";
	public static final String OK_OR_CANCEL= "//android.widget.Button[contains(@text,'%s')]";
	public static final String NAVIGATE_UP = "//android.widget.ImageButton[@content-desc='Navigate up']";
	public static final String SUBJECT_FOLDER_VIEW = "//android.widget.TextView[@resource-id='com.google.android.gm:id/subject_and_folder_view']";
	public static final String MORE_OPTIONS = "//android.view.ViewGroup[@resource-id='com.google.android.gm:id/mail_toolbar']//android.widget.ImageView[@content-desc='More options']";
	public static final String LABELS_LOCATOR = "//android.widget.ListView[@resource-id='com.google.android.gm:id/folder_list_view']//android.widget.CheckedTextView[@resource-id='com.google.android.gm:id/checkbox'] | //android.widget.ListView[@resource-id='com.google.android.gm:id/folder_list_view']//android.widget.TextView[@resource-id='com.google.android.gm:id/folder_name']";
}
