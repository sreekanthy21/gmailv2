package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.InboxFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Draft extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*public void ExecuteAllDrafts()throws Exception{
		composeMailAndSaveAsDraft();
		composeMailWithMultipleAttachmentsAndAnsertedDriveChips();
		verifyEditDraftAndSendMail();
		verifyDiscardDraft();
	}
	*/
	
	/**
	 * @TestCase : Compose mail and save as draft
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
.    * @author Rahul kulkarni
	 * @throws: Exception
	 * GMAT 17
	 * 
	 */	
	@Test(groups={"P1"}, enabled = true, priority = 47)
		public void composeMailAndSaveAsDraft() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> composeMailAndSaveAsDraft = CommonPage.getTestData("composeMailAndSaveAsDraft");
			try{
				log.info("*****************Started testcase ::: composeMailAndSaveAsDraft*****************************");
				inbox.verifyGmailAppInitState(driver);
				
				String emailId = CommonFunctions.getNewUserAccount(composeMailAndSaveAsDraft, "ToFieldDatadraft", null);

				
				//userAccount.switchMailAccount(driver, composeMailAndSaveAsDraft.get("ToFieldDatadraft"));
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
				
				inbox.composeMailAndSaveAsDraft(driver);
				log.info("*****************Ended testcase ::: composeMailAndSaveAsDraft*****************************");
				}catch(Throwable e){
				commonPage.catchBlock(e, driver, "composeMailAndSaveAsDraft");
			}
		}
	
	/**
	 * @TestCase GMAT 18 : composeMailWithMultipleAttachmentsAndAnsertedDriveChips
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
.    *Author Rahul kulkarni
	 * @throws: Exception
	 */
	   @Test(groups={"P1"}, enabled = true, priority = 71)
		public void composeMailWithMultipleAttachmentsAndAnsertedDriveChips() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
			     Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
			        Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
				/*log.info("*****************Started testcase ::: composeMailWithMultipleAttachmentsAndAnsertedDriveChips*****************************");
				
				inbox.verifyGmailAppInitState(driver);
			    inbox.composeMailWithMultipleAttachmentsAndAnsertedDriveChips(driver);
			    log.info("*****************Completed testcase ::: composeMailWithMultipleAttachmentsAndAnsertedDriveChips*****************************");   
			    driver.navigate().back();*/
		   		String fromUser = inbox.composeAsNewMail(driver,
	    	            verifyComposeAndSendMailData.get("ToFieldData"),
	    	            verifyComposeAndSendMailData.get("CCFieldData"), " ",
	    	            verifyComposeAndSendMailData.get("SubjectData"),
	    	            verifyComposeAndSendMailData.get("ComposeBodyData"));
	   		log.info("***************** Started testcase ::: verifyAttachmentsUponRotating ******************************");
	           attachment.addAttachementFromDriveAccount(driver, "Appium.jpg", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	           attachment.addAttachementFromDriveAccount(driver, "Appiumnew.png", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	   		   Thread.sleep(1200);
	   		   driver.navigate().back();
	   		   inbox.openMenuDrawer(driver);
	   		   CommonFunctions.scrollTo(driver, "Drafts"); // added by Venkat on 26/12/2018
    	       navDrawer.navigateToDrafts(driver);
    	       userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData"));
    	       userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appium.jpg");
	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appiumnew.png");
	           driver.navigate().back();
	           driver.navigate().back();
	           log.info("***************** Completed testcase ::: verifyAttachmentsUponRotating ******************************");
	           driver.navigate().back();
	    			//Previous CODE
	    			/*log.info("***************** Started testcase ::: verifyMailSavedAsDraft ******************************");
	    			inbox.saveMailAsDraft(driver);
	    	        log.info("Saved as draft from menu options");
	    	        inbox.openMenuDrawer(driver);
	    	        navDrawer.navigateToDrafts(driver);
	    	        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	    	        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	    	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	    	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	    	        commonFunction.scrollDown(driver, 1);
	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	    	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
			           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
			           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));log.info("----------------Verified attachments in drafts--------------------");
	    	        log.info("***************** Completed testcase ::: verifyMailSavedAsDraft ******************************");
	    	        driver.navigate().back();*/
			}catch(Throwable e)
			{
				commonPage.catchBlock(e, driver, "composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			}
		}
	
	/** Testcase for edit draft and send mail
	 * 
	 * GMAT 19
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 48)
	public void verifyEditDraftAndSendMail() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("*****************Started testcase ::: verifyEditDraftAndSendMail*****************************");
			
			Map<String, String>verifyEditDraftAndSendMailData = CommonPage.getTestData("verifyEditDraftAndSendMail");
			String subject = verifyEditDraftAndSendMailData.get("SubjectData");

			//userAccount.switchMailAccount(driver, verifyEditDraftAndSendMailData.get("ToEmail"));
			/*--Verifying the drafts option and click on it--*/
//			if(CommonFunctions.getSizeOfElements(driver, verifyEditDraftAndSendMailData.get("Drafts"))!=0){
//				log.info("Checking for saved draft from this method verifyComposeMailSavedAsDraft");
//			}
//			else{
//			inbox.openMenuDrawer(driver);
//			//inbox.scrollDown(1);
//			navDrawer.navigateToDrafts(driver);
			Thread.sleep(2000);
			
			String fromUser=inbox.composeAsNewMail(driver,
					verifyEditDraftAndSendMailData.get("To"),
					"", "",
					verifyEditDraftAndSendMailData.get("SubjectData"),
					verifyEditDraftAndSendMailData.get("ComposeBodyData"));
			
			navDrawer.navigateBackTo(driver);
	//Start: Creating draft with Attachments -- @author: batchi		
			String fromUser1=inbox.composeAsNewMail(driver,
					verifyEditDraftAndSendMailData.get("To"),
					"", "",
					verifyEditDraftAndSendMailData.get("EditDraftWithAttachmentSubject"),
					verifyEditDraftAndSendMailData.get("ComposeBodyData"));
			attachment.addAttachementFromDriveAccount(driver, verifyEditDraftAndSendMailData.get("AttachmentJPG"),verifyEditDraftAndSendMailData.get("eTouchOne"));
			//attachment.addAttachementFromRoot(driver, verifyEditDraftAndSendMailData.get("AttachmentJPG") );
			navDrawer.navigateBackTo(driver);
	// End: Creating draft with Attachments -- @author: batchi			

			Thread.sleep(2000);
	// Started: code commented as this is no longer useful -- @author: batchi	
			/*	if(CommonFunctions.getSizeOfElements(driver, verifyEditDraftAndSendMailData.get("Drafts"))!=0){
				log.info("Checking for saved draft from this method verifyComposeMailSavedAsDraft");
			}
			else{
			
			inbox.openMenuDrawer(driver);
			//inbox.scrollDown(1); // enabled on 02/05/2019
			try {
				userAccount.scrollTo("Drafts");
			} catch (Exception e) {
				inbox.scrollDown(1); 
			}}*/
	// End: code commented as this is no longer useful -- @author: batchi			
			
		inbox.openMenuDrawer(driver);	
		Thread.sleep(2000);
		//navDrawer.navigateToDrafts(driver);
		CommonFunctions.searchAndClickByXpath(driver, verifyEditDraftAndSendMailData.get("Drafts"));
			Thread.sleep(2000);
			inbox.openDraftMail(driver, subject);
			inbox.tapOnSendAndNavigateBack(driver);
			inbox.verifyDraftMail(driver, subject);
	// Started: Edit and send draft verification -- @author: batchi		
			Thread.sleep(2000);
			inbox.openDraftMail(driver, verifyEditDraftAndSendMailData.get("EditDraftWithAttachmentSubject"));
			inbox.tapOnSendAndNavigateBack(driver);
			inbox.verifyDraftMail(driver,verifyEditDraftAndSendMailData.get("EditDraftWithAttachmentSubject") );
	// End : Edit and send draft verification -- @author: batchi			
			
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyEditDraftAndSendMail************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyEditDraftAndSendMail");
		}
	}
	
	/**
	* @TestCase GMAT 20 : Discard Draft
	* @Pre-condition : 1. Gmail app installed
	                   2. Gmail account is added to app
	    			   3. Mail was created and put into Draft
	* @throws: Exception
	* @author batchi
	*/
	
	
	@Test(groups = {"P2"}, enabled=true, priority=4)
	public void verifyDiscardDraft()throws Exception{

		driver = map.get(Thread.currentThread().getId());
		try {
			inbox.verifyGmailAppInitState(driver);

			Map<String, String> verifyDiscarDraftData = CommonPage.getTestData("verifyReplyFromNotification");

			String fromFieldData = CommonFunctions.getNewUserAccount(verifyDiscarDraftData, "FromFieldData", null);

			if(!CommonFunctions.isAccountAlreadySelected(fromFieldData)) {
				userAccount.switchMailAccount(driver, fromFieldData);
			}
			
			String discardMsgFromDraftTL = verifyDiscarDraftData.get("mailSubjectTL");
			String discardMsgFromDraftCV = verifyDiscarDraftData.get("mailSubjectCV");
			String discardMsgFromDraftEditView = verifyDiscarDraftData.get("mailSubjectEditView");
			
			CommonFunctions.enterDataForDraftMail(driver, discardMsgFromDraftTL);
			CommonFunctions.enterDataForDraftMail(driver, discardMsgFromDraftCV);
			CommonFunctions.enterDataForDraftMail(driver, discardMsgFromDraftEditView);
			
			Thread.sleep(2000);
			
			InboxFunctions.openMenuDrawer(driver);
			CommonFunctions.scrollsToTitle(driver, "Drafts");

			CommonFunctions.discardDraftFromPage(driver, discardMsgFromDraftTL, "TL");
			CommonFunctions.discardDraftFromPage(driver, discardMsgFromDraftCV, "CV");
			CommonFunctions.discardDraftFromPage(driver, discardMsgFromDraftEditView, "EditView");
			
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyDiscarDraft");
		}
	}

/*	
	public static void discardDraftFromPage(AppiumDriver driver, String text, String pageType) throws Exception {
		try {
			CommonFunctions.isElementByXpathDisplayed(driver, "//android.view.ViewGroup[contains(@text,'"+text+"')]");
		} catch (Exception e) {
			log.info("Scrolling down for mail :"+text);
			CommonFunctions.scrollDown(driver,1);
		}
		if("TL".equalsIgnoreCase(pageType)) {
			CommonFunctions.searchAndClickByXpath(driver, "//android.view.ViewGroup[contains(@text,'"+text+"')]//android.widget.ImageView[@index='0']");
		}else {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'"+text+"')]");
		}

		if("EditView".equalsIgnoreCase(pageType)) {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/edit_draft'] | //android.widget.ImageView[@content-desc='Edit']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@content-desc='More options']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Discard']");
		}else {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.gm:id/discard_drafts'] | //android.widget.TextView[@content-desc='Discard drafts']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.Button[@text='OK']");
		}
		Thread.sleep(2000);
		
		CommonFunctions.scrollUp(driver,1);
		

	}
	
	public static void enterDataForDraftMail(AppiumDriver driver, String text) throws Exception {

		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageButton[@content-desc='Compose'] | //android.widget.ImageButton[@resource-id='com.google.android.gm:id/compose_button']");

		CommonFunctions.searchAndSendKeysByXpath(driver,"//android.widget.EditText[@text='Subject']",text);
		try {
			CommonFunctions.searchAndSendKeysByXpathAndADB(driver,"//android.widget.EditText[@text='Compose email']",text);
		} catch (Exception e) {
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			
			try{
				Process p;
				 String[] adbInput = new String[]{"adb", "-s", deviceudid, "shell", "input", "text ", "'",text,"'"};
				 p = new ProcessBuilder(adbInput).start();
				    log.info("Sent keys using adb input");
				    Thread.sleep(2000);
				    p.destroy();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageButton[@content-desc='Navigate up']");

	}
	
*/	
	@Test(groups = {"P2"}, enabled=true, priority=4)
	public void verifyDiscardDraft1()throws Exception{

		driver = map.get(Thread.currentThread().getId());
		try {
			inbox.verifyGmailAppInitState(driver);

			Map<String, String> verifyDiscarDraftData = CommonPage.getTestData("verifyReplyFromNotification");

			//String fromFieldData = verifyDiscarDraftData.get("FromFieldData");
			//fromFieldData = CommonFunctions.getNewUserAccount(verifyDiscarDraftData, fromFieldData);

			String fromFieldData = CommonFunctions.getNewUserAccount(verifyDiscarDraftData, "FromFieldData", null);

			if(!CommonFunctions.isAccountAlreadySelected(fromFieldData))
            	userAccount.switchMailAccount(driver, fromFieldData);
            
			String toUser = inbox.composeAsNewMailWithOut(driver,verifyDiscarDraftData.get("mailSubjectEditView"));
          
			InboxFunctions.navigateBack(driver);
			
			inbox.composeAsNewMailWithOut(driver, verifyDiscarDraftData.get("mailSubjectCV"));
			InboxFunctions.navigateBack(driver);
			
			inbox.composeAsNewMailWithOut(driver, 
							verifyDiscarDraftData.get("mailSubjectTL"));
			InboxFunctions.navigateBack(driver);
			InboxFunctions.openMenuDrawer(driver);
			commonFunction.scrollDown(driver,1);
			CommonFunctions.scrollsToTitle(driver, "Drafts");
			CommonPage.openDraftsInEditView(driver,verifyDiscarDraftData.get("mailSubjectEditView"));
			CommonPage.openDraftsInCVView(driver,verifyDiscarDraftData.get("mailSubjectCV"));
			driver.navigate().back(); // added by Venkat on 25/04/2019
			CommonPage.openDraftsInTLView(driver,verifyDiscarDraftData.get("mailSubjectTL")); //  ****Have to Implement as I'm unable to locate the Sender Image Icon******
			driver.navigate().back();
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyDiscarDraft");
	}
	}
	
	/**
	 * @TestCase sanity IMAP : IMAP Accounts - Verify create, edit and send draft
	 * @Pre-condition :
	 * GMAT 
	 * @throws: Exception
	 * @author batchi
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyEditAndSendDraft() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = CommonPage.getTestData("IMAPAccountDetails");

		//userAccount.switchMailAccount(driver, verifySendMailIMAPData.get("OutlookEmailAddress"));
		
		String  emailId = CommonFunctions.getNewUserAccount(verifySendMailIMAPData, "OutlookEmailAddress", "IMAP");

		if(!CommonFunctions.isAccountAlreadySelected(emailId))
			userAccount.switchMailAccount(driver,emailId);

		
		String fromUser = inbox.composeAsNewMail(driver,
				verifySendMailIMAPData.get("ToFieldData"),
				verifySendMailIMAPData.get(" "), " ",
				verifySendMailIMAPData.get("SubjectDataForDraft"),
				verifySendMailIMAPData.get("ComposeBodyData"));
		driver.navigate().back();
		//driver.navigate().back();
		navDrawer.openMenuDrawer(driver);
		//navDrawer.navigateToDrafts(driver);// commented by Venkat on 7/2/2019 
		CommonFunctions.scrollsToTitle(driver, "Drafts"); // added by Venkat on 7/2/2019 
		CommonFunctions.searchAndClickByXpath(driver, verifySendMailIMAPData.get("DraftMailTL"));
		CommonFunctions.searchAndClickByXpath(driver, verifySendMailIMAPData.get("editdraftIcon"));
//		driver.hideKeyboard();
		CommonFunctions.searchAndSendKeysByXpathNoClear(driver, verifySendMailIMAPData.get("DraftEditComposeText"), 
							"  " +verifySendMailIMAPData.get("EditComposeBody"));
		CommonFunctions.searchAndClickByXpath(driver, verifySendMailIMAPData.get("SendButton"));
		Thread.sleep(2000);
		inbox.pullToReferesh(driver);
		CommonFunctions.isElementByXpathNotDisplayed(driver,verifySendMailIMAPData.get("DraftMailTL") );
		Thread.sleep(2000);
		driver.navigate().back(); // added by Venkat on 7/2/2019 for Nexus6P device
		navDrawer.navigateToSentBox(driver);
		Thread.sleep(12000);
		inbox.pullToReferesh(driver);
		Thread.sleep(5000);
		inbox.pullToReferesh(driver);
		Thread.sleep(5000);
		commonFunction.isElementByXpathDisplayed(driver, verifySendMailIMAPData.get("DraftMailTL"));
//		CommonFunctions.isElementByXpathDisplayed(driver, verifySendMailIMAPData.get("UnReadIcon"));
		
	
		
	
	}
	/**
	 * @TestCase GMAT 18 : compose draft and discard with attachments
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
.    * @author batchi
	 * @throws: Exception
	 */
	   @Test(groups={"P1"}, enabled = true, priority = 71)
		public void discardDraftWithAttachments() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
			     Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
			        Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
				String fromUser = inbox.composeAsNewMail(driver,
	    	            verifyComposeAndSendMailData.get("ToFieldData")," ", " ",
	    	            verifyAttachmentsUponRotatingData.get("DiscardDraftWithAttachmentSubject"),
	    	            verifyComposeAndSendMailData.get("ComposeBodyData"));
	   		log.info("***************** Started testcase ::: Verify discarding draft with attachments ******************************");
	           attachment.addAttachementFromDriveAccount(driver, "Appium.jpg", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	           attachment.addAttachementFromDriveAccount(driver, "Appiumnew.png", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	           attachment.addAttachmentFromDrive(driver, "Appium");
	   		   Thread.sleep(1200);
	   		   driver.navigate().back();
	   		   inbox.openMenuDrawer(driver);
    	       navDrawer.navigateToDrafts(driver);
    	       Thread.sleep(1200);
    	       userAccount.pullToReferesh(driver);
    	       userAccount.verifyMailIsPresent(driver, verifyAttachmentsUponRotatingData.get("DiscardDraftWithAttachmentSubject"));
    	       userAccount.openMail(driver, verifyAttachmentsUponRotatingData.get("DiscardDraftWithAttachmentSubject"));
	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appium.jpg");
	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appiumnew.png");
	           driver.navigate().back();
	           commonPage.openDraftsInEditView(driver, verifyAttachmentsUponRotatingData.get("DiscardDraftWithAttachmentSubject"));
	           
	           driver.navigate().back();
	           driver.navigate().back();
	           log.info("***************** Completed testcase ::: Verify discarding draft with attachments ******************************");
	           driver.navigate().back();
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifyDiscardDraftWithAttachments");
		}
	   }	
}
