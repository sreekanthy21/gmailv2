package com.etouch.mobile.gmail;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;
import com.etouch.taf.util.SendMail;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Spoofing extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	HtmlEmailSender mail = new HtmlEmailSender();
	
	Process p;

	
	/*@Test
	public void ExecuteAllAccounts() throws Exception{
		verifyAddingNewGmailAccount();
		verifyAddingOtherAccounts();
		verifyRemoveAccount();
	}*/
	
	public void SendMailBySite() {  
		  
		  String host="smtp.gmail.com";  
		  final String user="gm.gig1.auto@gmail.com";
		  final String password="mobileautomation"; 
          String port = "587";
		    
		 String to="caribou.dnis.receiver@gmail.com";
		  
		   //Get the session object  
		   Properties props = new Properties();  
		   props.put("mail.smtp.host", host);
		   props.put("mail.smtp.port", port);
		   props.put("mail.smtp.auth", "true");
		   props.put("mail.smtp.starttls.enable", "true");
		     
		   Session session = Session.getDefaultInstance(props,  
		    new javax.mail.Authenticator() {  
		      protected PasswordAuthentication getPasswordAuthentication() {  
		    return new PasswordAuthentication(user,password);  
		      }  
		    });  
		  
		   //Compose the message  
		    try {  
		     MimeMessage message = new MimeMessage(session);  
		     message.setFrom(new InternetAddress(user));  
		     message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
		     message.setSubject("TestSpoofingName");  
		     message.setText("This is simple program of sending email using JavaMail API");  
		       
		    //send the message  
		     Transport.send(message);  
		  
		     System.out.println("message sent successfully...");  
		   
		     } catch (MessagingException e) {e.printStackTrace();}  
		   
		}  
	
	public void sendMailBatFile() throws InterruptedException, IOException{
		String path = System.getProperty("user.dir");
        String command = path + java.io.File.separator + "Mail" + java.io.File.separator + "caribou.bat";
        log.info("Executing the bat file " + command);
        p = new ProcessBuilder(command).start();
        log.info("Executed the bat file to generate a mail");
        Thread.sleep(5000);
	}
	
	
	/**-- Verify display name for suspicious sender 
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 66)
	public void verifyDisplayNameSuspiciousSender() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		String[] addAccountTwo = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "caribou.dnis.receiver@gmail.com", "-e", "password", "BigBangInF()RK", "-e", "action", "add", "-e", "sync", "true",
	    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
	    p = new ProcessBuilder(addAccountTwo).start();
	    log.info("Installing account caribou.dnis.receiver with google account manager apk");
	    Thread.sleep(10000);
	    
	    //sendMailBatFile();
	    
		CommonFunctions.smtpMailGeneration("caribou.dnis.receiver@gmail.com","TestSpoofingEmail", "ArchiveMail.html", null);

	    
		try {
			log.info("********Started testcase ::: verifyDisplayNameSuspiciousSender*******************************");
			
			Map<String, String> verifyspoofing = CommonPage.getTestData("verifyReportNotSuspiciousOption");
			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyspoofing.get("caribouAccount")))
			userAccount.switchMailAccount(driver, verifyspoofing.get("caribouAccount"));
		    
		    userAccount.pullToReferesh(driver);
		    commonPage.waitForElementToBePresentByXpath(verifyspoofing.get("TestSpoofingSubjectEmail"));
			log.info("Long email displayed properly for suspicious/new user");
		    
			SendMailBySite();
			//userAccount.switchMailAccount(driver, verifyspoofing.get("Account"));
			/*inbox.composeAndSendMail(driver, verifyspoofing.get("caribouAccount"), "", "", verifyspoofing.get("TestSpoofingNameSubject"),
					"");
			*/Thread.sleep(3500);
			inbox.pullToReferesh(driver);
		    //userAccount.switchMailAccount(driver, verifyspoofing.get("caribouAccount"));
		    Thread.sleep(1200);
		    userAccount.pullToReferesh(driver);
		    commonPage.waitForElementToBePresentByXpath(verifyspoofing.get("TestSpoofingSubjectName"));
			log.info("Long Name displayed properly for non suspicious user");
			
			log.info("*****************Completed testcase ::: verifyDisplayNameSuspiciousSender*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyDisplayNameSuspiciousSender");
		}
	}
	

	/**--Verify display name on notification
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 68)
	public void verifyDisplayNameNotification() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("********Started testcase ::: verifyDisplayNameNotification*******************************");
			Map<String, String> verifyspoofing = CommonPage
					.getTestData("verifyReportNotSuspiciousOption");
			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyspoofing.get("Account")))
			userAccount.switchMailAccount(driver, verifyspoofing.get("Account"));
			
			String[] adbroot = new String[]{"adb", "root"};
			p = new ProcessBuilder(adbroot).start();
			log.info("device rooted");
			Thread.sleep(2000);
			
			String[] adbclearnotify = new String[]{"adb", "shell", "service", "call", "notification", "1"};
			p = new ProcessBuilder(adbclearnotify).start();
			//sendMailBatFile();
			//CommonFunctions.smtpMailGeneration("caribou.dnis.receiver@gmail.com","TestSpoofingEmail", "ArchiveMail.html", null);
			mail.sendHtmlEmail("gm.gig3.auto@gmail.com", "mobileautomation", "caribou.dnis.receiver@gmail.com", "TestSpoofingEmail", " ");
			
			//userAccount.navigateToSettings(driver);
			userAccount.openUserAccountSetting(driver, verifyspoofing.get("caribouAccount"));
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			CommonFunctions.verifyPopUp(driver, null); // added by Venkat on 01/03/2019
			Thread.sleep(2000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			CommonFunctions.verifyPopUp(driver, null); // added by Venkat on 01/03/2019
			Thread.sleep(5000);
			/*driver.navigate().back();
			driver.navigate().back();*/
			String[] adbswipenotifybar = new String[]{"adb", "shell", "service", "call", "statusbar", "1"};
			//String[] adbswipenotifybar = new String[]{"adb", "shell", "input", "swipe 0 0 0 300"};
			p = new ProcessBuilder(adbswipenotifybar).start();
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, verifyspoofing.get("NotificationBar"))){
				log.info("Some Emails displayed as notifications");
				if(CommonFunctions.isElementByXpathDisplayed(driver, verifyspoofing.get("NotificationEmailSpoof"))){
					log.info("Email address displayed for the spoofing mail");
				}
			}else{
				driver.navigate().back();
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				Thread.sleep(5000);
				p = new ProcessBuilder(adbswipenotifybar).start();
				CommonPage.waitForElementToBePresentByXpath(verifyspoofing.get("NotificationBar"));
				CommonPage.waitForElementToBePresentByXpath(verifyspoofing.get("NotificationEmailSpoof"));
				log.info("Notification spoofing verified");
			}
			// added by Venkat on 19/09/2019 : start
			CommonFunctions.searchAndClickByXpath(driver, verifyspoofing.get("NotificationEmailSpoof"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			if(CommonFunctions.isElementByXpathDisplayed(driver, verifyspoofing.get("NotificationEmailSpoof"))){
				log.info("Email address displayed for the spoofing mail");
			}else {
				throw new Exception("Display name not displayed properly in CV");
			}
			// added by Venkat on 19/09/2019 : end
			driver.navigate().back();
			driver.navigate().back();
			
			log.info("********Completed testcase ::: verifyDisplayNameNotification*******************************");
			}catch(Exception e){
				commonPage.catchBlock(e, driver, "verifyDisplayNameNotification");
			}
		}
	
	/**--Verify display name as email address
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 67)
	public void verifyDisplayNameEmailAddress()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("********Started testcase ::: verifyDisplayNameNotification*******************************");
			Map<String, String> verifyspoofing = commonPage
					.getTestData("verifyReportNotSuspiciousOption");
			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyspoofing.get("AccountFour")))
			userAccount.switchMailAccount(driver, verifyspoofing.get("AccountFour"));
			inbox.composeAndSendMail(driver, verifyspoofing.get("caribouAccount"), "", "", verifyspoofing.get("FourSubject"),
					"");			
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyspoofing.get("caribouAccount")))
			userAccount.switchMailAccount(driver, verifyspoofing.get("caribouAccount"));
			Thread.sleep(3000);
			userAccount.pullToReferesh(driver);
			userAccount.pullToReferesh(driver);
			commonPage.waitForElementToBePresentByXpath(verifyspoofing.get("FourSubjectXpath"));
			CommonFunctions.searchAndClickByXpath(driver, verifyspoofing.get("FourSubjectXpath"));
			CommonFunctions.searchAndClickByXpath(driver, verifyspoofing.get("ViewRecipientDetails"));
			Thread.sleep(600);
			commonPage.waitForElementToBePresentByXpath(verifyspoofing.get("DetailFromEmail"));
			String spoofmail = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifyspoofing.get("verifySpoofingMail"));
			if(spoofmail.equalsIgnoreCase(verifyspoofing.get("verifySpoofingMailText"))){
				log.info("Verified spoofing email address");
			}else{
				throw new Exception("Spoofing email address not displayed");
			}
			
			driver.navigate().back();
			
			log.info("********Completed testcase ::: verifyDisplayNameNotification*******************************");
			
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyDisplayNameEmailAddress");
		}
	}
	
	}
