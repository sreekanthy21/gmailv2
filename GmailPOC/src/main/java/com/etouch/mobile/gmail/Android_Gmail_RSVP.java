package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_RSVP extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	public void ExecuteAllRSVP()throws Exception{
		verifyNewRSVPFormatDisplayedForCalendarInvites();
	}
	
	/**
	 * @TestCase 10 :Verify New RSVP format displayed for Calendar invites**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * GMAT 77
	 * @throws: Exception
	 */	 
	@Test(groups = {"P0"}, enabled = true, priority = 96)
   public void verifyNewRSVPFormatDisplayedForCalendarInvites() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			  Map<String, String> verifyNewRSVPFormatDisplayedForCalendarInvites = commonPage.getTestData("verifyNewRSVPFormatDisplayedForCalendarInvites");
			  log.info("*****************Started testcase ::: verifyNewRSVPFormatDisplayedForCalendarInvites*************************");
			    
			 /* inbox.verifyNewRSVPFormatDisplayedForCalendarInvites(driver);
			  inbox.launchGmailApp(driver);
			 */ 
			  inbox.SendRsvpMail();
			  inbox.verifyNewRSVPFormatDisplayedForCalendarInvitesInEmail(driver);
			  
			  log.info("*****************Completed testcase ::: verifyNewRSVPFormatDisplayedForCalendarInvites*****************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyNewRSVPFormatDisplayedForCalendarInvites");
					}
	}
}
