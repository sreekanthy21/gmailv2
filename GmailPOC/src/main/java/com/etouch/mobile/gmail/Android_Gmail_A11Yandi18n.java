package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_A11Yandi18n extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	String sub;
	
	/*@Test
	public void ExecuteAllA11Yandi18n() throws Exception{
		verifyi18NCJK();
	}
	*/
	/**Testcase to verify the i18N language for Chinese, Japanese and Korean
	 * 
	 * @preCondition Set device language to any specified Chinese(中文)/Japanese/Korean(한국어)
	 * Japanese is not available in S5 v6.0.1
	 * GMAT 115
	 *@throws Exception
	 *@author Phaneendra
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 119)
	public void verifyi18NCJK()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> LocaleChinese = commonPage.getTestData("localLanguageChinese");
			//userAccount.verifyAccountsExists(driver, LocaleChinese.get("Account"), LocaleChinese.get("AccountPassword"));
			log.info("***************** Started testcase ::: verifyi18NCJK ******************************");
			userAccount.setLanguageADB(driver, "Chinese");
			
			commonPage.launchGmailApp(driver);
			
			inbox.verifyComposeLocale(driver, LocaleChinese.get("ChineseComposeTitle"), LocaleChinese.get("ChineseFromAddress"), LocaleChinese.get("ChineseToAddress"),
					LocaleChinese.get("ChineseSubjectAddress"), LocaleChinese.get("ChineseComposeText"));
			inbox.verifySearchLocale(driver, LocaleChinese.get("ChineseSearchFieldText"));
			inbox.verifyMailOtherOptionsCV(driver, LocaleChinese.get("ChineseOverFlowButton"));
			inbox.verifyMailOtherOptionsTL(driver, LocaleChinese.get("ChineseMailSenderImage"), LocaleChinese.get("ChineseOverFlowButton"));
			inbox.verifyMailArchiveDelete(driver, LocaleChinese.get("ChineseArchivedMail"), LocaleChinese.get("ChineseUndo"), LocaleChinese.get("ChineseDeletedMail"));
			inbox.verifyLeftNavigationLocale(driver, LocaleChinese.get("ChinesePrimaryLabel"), LocaleChinese.get("ChineseSocialLabel"), LocaleChinese.get("ChinesePromotionsLabel"),
					LocaleChinese.get("ChineseUpdatesLabel"), LocaleChinese.get("ChineseForumsLabel"));
			inbox.verifySettings(driver, LocaleChinese.get("Account"), LocaleChinese.get("ChineseSettingsOptions"), 
					LocaleChinese.get("ChineseSettingsOptions"));
			userAccount.setLanguageADB(driver, "English");

			//userAccount.setLanguageToLocale(driver, LocaleChinese.get("EnglishLanguage"));
			log.info("*****************Completed testcase ::: verifyi18NCJK*************************");
		}catch(Throwable e){
			userAccount.setLanguageADB(driver, "English");
			commonPage.catchBlock(e, driver, "verifyi18NCJK");
		}
	}
	
	
	@Test(groups = {"P2"}, enabled = true, priority = 120)
	public void verifyi18FSGR()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> LocaleFrench = commonPage.getTestData("localLanguageFrench");
			log.info("***************** Started testcase ::: verifyi18FSGR ******************************");
			userAccount.setLanguageADB(driver, "French");
			
			commonPage.launchGmailApp(driver);
			
			inbox.verifyComposeLocale(driver, LocaleFrench.get("FrenchComposeTitle"), LocaleFrench.get("FrenchFromAddress"), LocaleFrench.get("FrenchToAddress"),
					LocaleFrench.get("FrenchSubjectAddress"), LocaleFrench.get("FrenchComposeText"));
			inbox.verifySearchLocale(driver, LocaleFrench.get("FrenchSearchFieldText"));
			inbox.verifyMailOtherOptionsCV(driver, LocaleFrench.get("FrenchOverFlowButton"));
			inbox.verifyMailOtherOptionsTL(driver, LocaleFrench.get("FrenchMailSenderImage"), LocaleFrench.get("FrenchOverFlowButton"));
			inbox.verifyMailArchiveDelete(driver, LocaleFrench.get("FrenchArchivedMail"), LocaleFrench.get("FrenchUndo"), LocaleFrench.get("FrenchDeletedMail"));
			inbox.verifyLeftNavigationLocale(driver, LocaleFrench.get("FrenchPrimaryLabel"), LocaleFrench.get("FrenchSocialLabel"), LocaleFrench.get("FrenchPromotionsLabel"),
					LocaleFrench.get("FrenchUpdatesLabel"), LocaleFrench.get("FrenchForumsLabel"));
			inbox.verifySettings(driver, LocaleFrench.get("Account"), LocaleFrench.get("FrenchSettingsOptions"), 
					LocaleFrench.get("FrenchGeneralSettings"));
			userAccount.setLanguageADB(driver, "English");

			//	userAccount.setLanguageToLocale(driver, LocaleFrench.get("EnglishLanguage"));
			log.info("*****************Completed testcase ::: verifyi18FSGR*************************");
		}catch(Throwable e){
			userAccount.setLanguageADB(driver, "English");
			commonPage.catchBlock(e, driver, "verifyi18FSGR");
		}
	}
	
	@Test(groups = {"P2"}, enabled = true, priority = 121)
	public void verifyi18HA()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> LocaleHebrew = commonPage.getTestData("localLanguageHebrew");
			log.info("***************** Started testcase ::: verifyi18HA ******************************");
			userAccount.setLanguageADB(driver, "Hebrew");
			
			commonPage.launchGmailApp(driver);
			
			inbox.verifyComposeLocale(driver, LocaleHebrew.get("HebrewComposeTitle"), LocaleHebrew.get("HebrewFromAddress"), LocaleHebrew.get("HebrewToAddress"),
					LocaleHebrew.get("HebrewSubjectAddress"), LocaleHebrew.get("HebrewComposeText"));
			inbox.verifySearchLocale(driver, LocaleHebrew.get("HebrewSearchFieldText"));
			inbox.verifyMailOtherOptionsCV(driver, LocaleHebrew.get("HebrewOverFlowButton"));
			inbox.verifyMailOtherOptionsTL(driver, LocaleHebrew.get("HebrewMailSenderImage"), LocaleHebrew.get("HebrewOverFlowButton"));
			inbox.verifyMailArchiveDelete(driver, LocaleHebrew.get("HebrewArchivedMail"), LocaleHebrew.get("HebrewUndo"), LocaleHebrew.get("HebrewDeletedMail"));
			inbox.verifyLeftNavigationLocale(driver, LocaleHebrew.get("HebrewPrimaryLabel"), LocaleHebrew.get("HebrewSocialLabel"), LocaleHebrew.get("HebrewPromotionsLabel"),
					LocaleHebrew.get("HebrewUpdatesLabel"), LocaleHebrew.get("HebrewForumsLabel"));
			inbox.verifySettings(driver, LocaleHebrew.get("Account"), LocaleHebrew.get("HebrewSettingsOptions"), 
					LocaleHebrew.get("HebrewGeneralSettings"));
			userAccount.setLanguageADB(driver, "English");

			//	userAccount.setLanguageToLocale(driver, LocaleFrench.get("EnglishLanguage"));
			log.info("*****************Completed testcase ::: verifyi18HA*************************");
		}catch(Throwable e){
			userAccount.setLanguageADB(driver, "English");
			commonPage.catchBlock(e, driver, "verifyi18HA");
		}
	}
	/**Verify i18n input data and input methods
	 * Below test case verifyOutputMethods and this case are linked
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 122)
	public void verifyInputMethods()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> LocaleHebrew = CommonPage.getTestData("localLanguageHebrew");
			log.info("***************** Started testcase ::: verifyInputMethods ******************************");
			userAccount.setLanguageADB(driver, "Hebrew");
			commonPage.launchGmailApp(driver);
			//userAccount.switchMailAccount(driver, LocaleHebrew.get("Account"));
			
			String  emailId = CommonFunctions.getNewUserAccount(LocaleHebrew, "Account", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			sub = inbox.verifyComposeLocaleMail(driver, LocaleHebrew.get("HebrewComposeTitle"), LocaleHebrew.get("HebrewFromAddress"), LocaleHebrew.get("HebrewToAddress"),
					LocaleHebrew.get("HebrewSubjectAddress"), LocaleHebrew.get("HebrewComposeText"));
			log.info(sub);
			}catch(Throwable e){
				userAccount.setLanguageADB(driver, "English");
				commonPage.catchBlock(e, driver, "verifyInputMethods");

			}
		
		}
	
	/**
	 * Verify i18n output data
	 * Above test case verifyInputMethods and this case are linked
	
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 123)
	public void verifyOutputMethods()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> LocaleHebrew = CommonPage.getTestData("localLanguageHebrew");
			log.info("***************** Started testcase ::: verifyOutputMethods ******************************");
			//userAccount.setLanguageADB(driver, "Hebrew");
			//commonPage.launchGmailApp(driver);
			
			String  emailId = CommonFunctions.getNewUserAccount(LocaleHebrew, "Account1", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			//userAccount.switchMailAccount(driver, LocaleHebrew.get("Account1"));
			inbox.pullToReferesh(driver);
			Thread.sleep(1200);
			inbox.pullToReferesh(driver);
			userAccount.verifyMailIsPresent(driver, sub);
			userAccount.setLanguageADB(driver, "English");

			}catch(Throwable e){
				userAccount.setLanguageADB(driver, "English");
				commonPage.catchBlock(e, driver, "verifyOutputMethods");

			}
		
		}
}
