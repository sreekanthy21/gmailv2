package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_ChromeCustom_Tabs extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	/*
	@Test
	public void ExecuteAllChromeCustomTabs() throws Exception{
		verifyEnableDisableChromeTabs();
	}
	*/
	/**Testcase to verify Enable/Disable Chrome Custom Tabs
	 * 
	 * @throws Exception
	 * GMAT 105
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 105)
	public void verifyEnableDisableChromeTabs()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> enableDisableChromeCustomData = commonPage.getTestData("enableDisableChromeCustom");
			log.info("*****************Started testcase ::: verifyEnableDisableChromeTabs*****************************");
			
			String  emailId = CommonFunctions.getNewUserAccount(enableDisableChromeCustomData, "Account", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
			
			//userAccount.switchMailAccount(driver, enableDisableChromeCustomData.get("Account")); // enabled by Venkat on 18/12/2018
			
			log.info("***********Verifying the link is opened in Gmail App when the option is Checked**************");
			userAccount.navigateToGeneralSettings(driver);
			userAccount.openWebLinksGmailOption(driver, true);
			//navDrawer.navigateToPrimary(driver);
			CommonFunctions.smtpMailGeneration(emailId,"ChromeCustomWeblinks", "SupportGoogle.html", null);
			inbox.verifyLinkOpenedInGmailApporBrowser(driver, enableDisableChromeCustomData.get("ChromeCustomSubject"), enableDisableChromeCustomData.get("GmailApp"));
			log.info("***********Verifying the link is opened in browser instead of Gmail app when the option is Unchecked**************");
			userAccount.navigateToGeneralSettings(driver);
			userAccount.openWebLinksGmailOption(driver, false);
			CommonFunctions.smtpMailGeneration(emailId,"ChromeCustomWeblinks", "SupportGoogle.html", null);
			inbox.verifyLinkOpenedInGmailApporBrowser(driver, enableDisableChromeCustomData.get("ChromeCustomSubject"), enableDisableChromeCustomData.get("Browser"));
			log.info("*****************Completed testcase ::: verifyEnableDisableChromeTabs*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyEnableDisableChromeTabs");
		}
	}

}
