package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Compose extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	static AppiumDriver driver = null;
	public static String MailSubject;
	public static String MailSubject1;
	public static String emailSubject;
	String[] adbroot;
	String[] adbAirplaneModeOnOff;
	String[] adbBroadcastIntent;
	Process p;
	HtmlEmailSender mail = new HtmlEmailSender();
	/*public void ExecuteAllCompose()throws Exception{
		verifyComposeAndSendMail();
		verifyAttachmentsUponRotating();
		composeAndSendMailForGmailAccountWithAttachments();
		verifyReplyFromNotification();
		verifyAttachmentSharingOptions();
	}*/
	
	/**
	 * @TestCase 3 : Verify Compose and send mail with CC,BCC fields
	 * @Pre-condition : At least 1 account should be there in Gmail. Account is
	 *                already added in testcase1.
	 * @throws: Exception
	 * GMAT 7
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 27)
	public void verifyComposeAndSendMail() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyComposeAndSendMail***************************");

			inbox.verifyGmailAppInitState(driver);
		//	inbox.launchGmailApplication(driver);

			Map<String, String> verifyComposeAndSendMailData = CommonPage.getTestData("verifyComposeAndSendMail");

			String emailId = CommonFunctions.getNewUserAccount(verifyComposeAndSendMailData, "AccountOne", null);
			
		
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);

			//Thread.sleep(2000);
			String fromUser=inbox.composeAndSendNewMail(driver,
					verifyComposeAndSendMailData.get("ToFieldData"),
					verifyComposeAndSendMailData.get("CCFieldData"), 
					verifyComposeAndSendMailData.get("BccFieldData"),
					verifyComposeAndSendMailData.get("SubjectData1"),
					verifyComposeAndSendMailData.get("ComposeBodyData"));
		/*	userAccount.switchMailAccount(driver,fromUser);
			
			inbox.scrollDown(1);
		*/	inbox.verifyMailInSentBox(driver,
					verifyComposeAndSendMailData.get("SubjectData1"));
		if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("ToFieldData")))
			userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("ToFieldData"));
			//userAccount.navigateToPrimary(driver);
			inbox.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));
			if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("CCFieldData")))
			userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("CCFieldData"));
			inbox.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));

			log.info("*****************Completed testcase ::: verifyComposeAndSendMail***************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyComposeAndSendMail");
		//	e.printStackTrace();
		}
	}
	/**
	* @TestCase GAT 10 : Verify attachments are not lost upon rotation and able to scroll the page
	* @Pre-condition : 1. Device should have all types of attachments
	  @author batchi
	* @throws: Exception
	* GMAT 8
	*/
	@Test(groups = {"P0"}, enabled=true, priority=70)
	public void verifyAttachmentsUponRotating()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		   Map < String, String > verifyComposeAndSendMailData = CommonPage.getTestData("verifyComposeAndSendMail");
	        Map < String, String > verifyMultipleLinesInBodyData = CommonPage.getTestData("verifyReplyFromNotification");
	        Map < String, String > verifyAttachmentsUponRotatingData = CommonPage.getTestData("verifyAttachmentsUponRotating");
	    	driver = map.get(Thread.currentThread().getId());
	    	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
	    	try{
	    		inbox.verifyGmailAppInitState(driver);
	    		
	    		String emailId = CommonFunctions.getNewUserAccount(verifyComposeAndSendMailData, "AccountOne", null);

	    		//userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
	    		if(!CommonFunctions.isAccountAlreadySelected(emailId))
	    			userAccount.switchMailAccount(driver, emailId);

	    		 String composeInMultipleLines = verifyMultipleLinesInBodyData.get("ComposeBodyData1").replaceAll(",", "\n");
	    		 	//String fromUser = inbox.composeAsNewMailWithOnlyTo(driver, verifyComposeAndSendMailData.get("ToFieldData"), "Rotation");
	    			String fromUser = inbox.composeAsNewMail(driver,
	    		            verifyComposeAndSendMailData.get("ToFieldData"),"","","",""
	    		            );

		   		log.info("***************** Started testcase ::: verifyAttachmentsUponRotating ******************************");
		           //log.info("Drats mail are deleting");
		        //   MailManager.DeleteAllMailsDrafts();
		        //   log.info("Drats mail are deleted");
		           //String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
		           //inbox.openDraftMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
		         /*  attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		           attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		           attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		           attachment.addAttachmentFromDriveMultiple(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
		         */ // attachment.addAttachmentFromDrive(driver, fileName);
		         // attachment.addAttachementFromDriveAccount(driver, "Appium.jpg", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
		         // attachment.addAttachementFromDriveAccount(driver, "Appiumnew.png", verifyAttachmentsUponRotatingData.get("FromMailIdData"));

		         attachment.addAttachementFromDriveAccount(driver, "Appium.jpg", emailId);
		         attachment.addAttachementFromDriveAccount(driver, "Appiumnew.png", emailId);

		   			Thread.sleep(1200);
		   		   commonPage.orientationOfScreen(driver);
		           commonFunction.hideKeyboard();
		          /* attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
		           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
		           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
		           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
		          */ 
		           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appium.jpg");
		           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appiumnew.png");
		           
		           commonFunction.scrollUp(driver, 2);
		           commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
		           log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
		           log.info("***************** Completed testcase ::: verifyAttachmentsUponRotating ******************************");
		           driver.navigate().back();
		           
	}catch (Throwable e) {
		commonPage.catchBlock(e, driver, "verifyAttachmentsUponRotating");
	}
	}
	
	/**
	 * @TestCase 8 : composeAndSendMailForGmailAccountWithAttachments	 *  
	 * @throws: Exception
	 * GMAT 10
	 */
	    @Test(groups = {"P1"}, enabled = true, priority = 45)
    	public void composeAndSendMailForGmailAccountWithAttachments() throws Exception{
		driver = map.get(Thread.currentThread().getId());
    	Map <String,String> verifyAddingNewGmailAccountData = CommonPage.getTestData("verifyAddingNewGmailAccount");
    	//inbox.OpenDownloadsApp(driver);
    	Process p;
		 /*String[] imgpng = new String[]{"adb", "push", "E:/Gmail-POC-GmailTest_Automation/GmailPOC/AttachmentFiles/ImagePNG.png", "/sdcard/DCIM/Camera"};
		    p = new ProcessBuilder(imgpng).start();
		    log.info("Copying png file to Camera");
		    Thread.sleep(800);
		    String[] imgjpg = new String[]{"adb", "push", "E:/Gmail-POC-GmailTest_Automation/GmailPOC/AttachmentFiles/ImageJPG.jpg", "/sdcard/Pictures"};
		    p = new ProcessBuilder(imgjpg).start();
		    log.info("Copying png file to Pictures");
		    Thread.sleep(800);
		    String[] openPhotos = new String[]{"adb", "shell", "am", "start", "-n", "com.google.android.apps.photos/.home.HomeActivity"};
		    p = new ProcessBuilder(openPhotos).start();
		    log.info("Opening photos");
		    Thread.sleep(3000);
		    String[] openGmail = new String[]{"adb", "shell", "am", "start", "-n", "com.google.android.gm/com.google.android.gm.ConversationListActivityGmail"};
		    p = new ProcessBuilder(openGmail).start();
		    log.info("Opening gmail");*/
		try{
			log.info("*****************Started testcase ::: composeAndSendMailForGmailAccountWithAttachments*****************************");
				inbox.verifyGmailAppInitState(driver);
	    		//userAccount.verifyAccountsExists(driver, verifyAddingNewGmailAccountData.get("AccountToExists"), verifyAddingNewGmailAccountData.get("Password"));
				
				String emailId = CommonFunctions.getNewUserAccount(verifyAddingNewGmailAccountData, "UserList", null);
				
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
	    		inbox.composeAndSendMailForGmailAccountWithAttachments(driver);
		    log.info("*****************Completed testcase ::: composeAndSendMailForGmailAccountWithAttachments*****************************");   
		    driver.navigate().back();
		}catch(Throwable e)
		{
			commonPage.catchBlock(e, driver, "verifyEditDraftAndSendMail");
		}
	}
	    
	    @Test(groups={"P1"}, enabled = true, priority = 113)
	 		public void MultipleLinesAttachments() throws Exception{
	 			driver = map.get(Thread.currentThread().getId());
	 			try{
	 			     Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
	 			        Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");
	 			       
	 				/*log.info("*****************Started testcase ::: composeMailWithMultipleAttachmentsAndAnsertedDriveChips*****************************");
	 				
	 				inbox.verifyGmailAppInitState(driver);
	 			    inbox.composeMailWithMultipleAttachmentsAndAnsertedDriveChips(driver);
	 			    log.info("*****************Completed testcase ::: composeMailWithMultipleAttachmentsAndAnsertedDriveChips*****************************");   
	 			    driver.navigate().back();*/
	 			      String fromUser=inbox.composeAsNewMail(driver,
	 	    	            verifyComposeAndSendMailData.get("ToFieldData"),
	 	    	            verifyComposeAndSendMailData.get("CCFieldData"), "",
	 	    	            verifyComposeAndSendMailData.get("SubjectData"),
	 	    	            verifyComposeAndSendMailData.get("ComposeMultipleData"));
	 		   
	 	   		log.info("***************** Started testcase ::: verifyAttachmentsUponRotating ******************************");
	 	           attachment.addAttachementFromDriveAccount(driver, "Appium.jpg", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	 	           attachment.addAttachementFromDriveAccount(driver, "Appiumnew.png", verifyAttachmentsUponRotatingData.get("FromMailIdData"));
	 	   		   Thread.sleep(1200);
	 	   		   driver.navigate().back();
	 	   		   inbox.openMenuDrawer(driver);
	     	       navDrawer.navigateToDrafts(driver);
	     	       userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData"));
	     	       userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData"));
	 	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appium.jpg");
	 	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appiumnew.png");
	 	           driver.navigate().back();
	 	           driver.navigate().back();
	 	           log.info("***************** Completed testcase ::: verifyAttachmentsUponRotating ******************************");
	 	           driver.navigate().back();
	 	    			//Previous CODE
	 	    			/*log.info("***************** Started testcase ::: verifyMailSavedAsDraft ******************************");
	 	    			inbox.saveMailAsDraft(driver);
	 	    	        log.info("Saved as draft from menu options");
	 	    	        inbox.openMenuDrawer(driver);
	 	    	        navDrawer.navigateToDrafts(driver);
	 	    	        userAccount.verifyMailIsPresent(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	 	    	        userAccount.openMail(driver, verifyComposeAndSendMailData.get("SubjectData1"));
	 	    	        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
	 	    	        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
	 	    	        commonFunction.scrollDown(driver, 1);
	 	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
	 	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
	 	    	       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
	 	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
	 	    	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ExcelAttachmentName"));
	 			           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("DocsAttachmentName"));
	 			           attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));log.info("----------------Verified attachments in drafts--------------------");
	 	    	        log.info("***************** Completed testcase ::: verifyMailSavedAsDraft ******************************");
	 	    	        driver.navigate().back();*/
	 			}catch(Throwable e)
	 			{
	 				commonPage.catchBlock(e, driver, "composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	 			}
	 		}
	
	    /**
		 * @TestCase GAT 13 : Reply from Notification
		 * @Pre-condition : Send and receive emails 
		   @author batchi
		 * @throws: Exception
		 * GMAT 11
		 */
		@Test (groups = {"P1"}, enabled= true, priority= 73)
		public void verifyReplyFromNotification() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try {
				log.info("***************** Started testcase ::: verifyReplyFromNotification ******************************");

				inbox.verifyGmailAppInitState(driver);
				Map<String, String> verifyReplyFromNotificationData = CommonPage.getTestData("verifyReplyFromNotification");
//				String toUser = inbox.composeAsNewMailWithOnlyTo(driver, verifyReplyFromNotificationData.get("ToFieldData"), 
//						verifyReplyFromNotificationData.get("SubjectData"));
				
				String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFromNotificationData, "GIGAccount", null);
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
				//userAccount.switchMailAccount(driver, verifyReplyFromNotificationData.get("GIGAccount"));
// Added body content by Bindu on 05/08				
	   		 	mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "gm.gig2.auto@gmail.com", "TestingGmailMailReplyfromNotificationFunctionality", verifyReplyFromNotificationData.get("ComposeBodyData1") );

				userAccount.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("ToFieldData"));
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail"); 
				Thread.sleep(5000);
				
				((AndroidDriver)driver).openNotifications();
				Thread.sleep(3000);
				
					commonPage.verifySubjectIsMatching(driver,verifyReplyFromNotificationData.get("SubjectData"));
					commonPage.verifyReplyButtonFromNotification(driver);
					Map<String, String> verifyReplyFromNotificationData1 = commonPage
							.getTestData("verifyAttachmentsUponRotating");
					attachment.addAttachementFromDriveAccount(driver, verifyReplyFromNotificationData1.get("ImageJpgAttachmentName"), 
							verifyReplyFromNotificationData.get("ToFieldData"));
					//commonPage.hideKeyboard();
					Thread.sleep(1500);
					commonFunction.orientationOfScreen(driver);
				    Thread.sleep(2000);
					commonPage.hideKeyboard();
					attachment.verifyAttachmentIsAddedSuccessfully(driver,verifyReplyFromNotificationData1.get("ImageJpgAttachmentName"));
					
	//Added by Bindu on 0608
					CommonFunctions.searchAndClickByXpath(driver, verifyReplyFromNotificationData.get("InlineButton"));
					Thread.sleep(2000);	
					commonPage.scrollDown(3);
					Thread.sleep(2000);
					String scrollDownVerification = CommonFunctions.searchAndGetTextOnElementByXpath(driver,verifyReplyFromNotificationData.get("AttachmentTitle"));
					if(scrollDownVerification.equals(verifyReplyFromNotificationData.get("ImageTitleName"))){
						log.info("********** Scrolling down is successful ***********");
					}else{
						throw new Exception("!!!!!!!!!! Scrolling is not performed !!!!!!!!!");
					}
					

					Thread.sleep(2000);		
					commonPage.scrollUp(3);
					
					String scrollUpVerification = CommonFunctions.searchAndGetTextOnElementByXpath(driver,verifyReplyFromNotificationData.get("FromAccountName"));
					if(scrollUpVerification.equals(verifyReplyFromNotificationData.get("ToFieldData"))){
						log.info("********** Scrolling up is successful ***********");
					}else{
						throw new Exception("!!!!!!!!!! Scrolling is not performed !!!!!!!!!");
					}
	// Ended by Bindu 06/08				
					driver.navigate().back();
//		    		inbox.launchGmailApplication(driver);
				log.info("*****************Completed testcase ::: verifyReplyFromNotification ***************************");
				} catch (Throwable e) {
					userAccount.launchGmailApplication(driver);
					commonPage.catchBlock(e, driver, "verifyReplyFromNotification");
				}
	}	

		
		 /**
		 * @TestCase GAT 13 : Reply from Notification
		 * @Pre-condition : Send and receive emails 
		   @author batchi
		 * @throws: Exception
		 * GMAT 11
		 */
		@Test (groups = {"P1"}, enabled= true)
		public void verifyReplyFromNotificationGmailGo() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try {
				log.info("***************** Started testcase ::: verifyReplyFromNotification ******************************");

				inbox.verifyGmailAppInitState(driver);
				Map<String, String> verifyReplyFromNotificationData = commonPage
						.getTestData("verifyReplyFromNotification");
			    Map < String, String > verifyAttachmentsUponRotatingData = commonPage.getTestData("verifyAttachmentsUponRotating");

				String toUser=  inbox.composeAndSendNewMail(driver,
						verifyReplyFromNotificationData.get("ToFieldData"),
						verifyReplyFromNotificationData.get("CC"),
						verifyReplyFromNotificationData.get("BCC"),
						verifyReplyFromNotificationData.get("SubjectData"),
						verifyReplyFromNotificationData.get("ComposeBodyData"));
				Thread.sleep(2000);
		    	
				//open settings to sync gmail
				//navDrawer.navigateToSettings(driver);
				userAccount.openUserAccountSetting(driver, verifyReplyFromNotificationData.get("ToFieldData"));
				userAccount.EnableSyncOnOFf(driver);
				
				((AndroidDriver)driver).openNotifications();
				Thread.sleep(3000);
				
					commonPage.verifySubjectIsMatching(driver,verifyReplyFromNotificationData.get("SubjectData"));
					commonPage.verifyReplyButtonFromNotification(driver);
					Map<String, String> verifyReplyFromNotificationData1 = commonPage
							.getTestData("verifyAttachmentsUponRotating");
					 attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
				        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
				      //  attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
				        attachment.addAttachmentFromDriveMultipleGmailGo(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
				        // Added new attachment to verify File sharing options while sending
				      //  attachment.addAttachmentFromDrive(driver, fileName);
				        commonPage.orientationOfScreen(driver);
				        Thread.sleep(2000);
				        commonFunction.hideKeyboard();
				        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("ImageJpgAttachmentName"));
				        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("GettingStarted"));
				       // attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("SlidesAttachementName"));
				        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyAttachmentsUponRotatingData.get("PdfAttachmentName"));
				        commonFunction.scrollUp(driver, 2);
				        commonFunction.isElementByXpathDisplayed(driver, verifyAttachmentsUponRotatingData.get("ComposeBody"));
				        log.info("(((((((((((((((((((((( Verified Body of the Mail))))))))))))))))))))))))))))");
				log.info("*****************Completed testcase ::: verifyReplyFromNotification ***************************");
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "verifyReplyFromNotification");
				}
	}	

		
	    /**
		 * @TestCase 9 : Verify Compose and send mail for Gmail account with Insert From Drive sharing options 
		 * @Pre-condition : Gmail app installed
	 						Gmail account is added to app
	 						Drive installed 
							Documents yearlier not shared
		 * GMAT 13
		 * @throws: Exception
		 *  @author Phaneendra
		 */
		@Test(groups = {"P2"}, enabled=true, priority = 69)
		public void verifyAttachmentSharingOptions() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyAttachmentSharingOptions******************");
				//driver.closeApp();
				Map<String, String> verifyEditDraftAndSendMailData = CommonPage.getTestData("verifyEditDraftAndSendMail");
				
				//--Open GoogleDrive--//
			//	inbox.verifyGoogleDriveAppInitState(driver);
				
			/*	String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
				attachment.addFileToDrive(driver, fileName, verifyEditDraftAndSendMailData.get("eTouchOne"));
			*/	
			//	inbox.launchGmailApplication(driver);
			//	userAccount.getListOfAddedAccounts(driver);
				
				String emailId = CommonFunctions.getNewUserAccount(verifyEditDraftAndSendMailData, "eTouchOne", null);

				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
				//userAccount.switchMailAccount(driver, verifyEditDraftAndSendMailData.get("eTouchOne"));
				String fromUser=inbox.composeAsNewMail(driver,
						verifyEditDraftAndSendMailData.get("ToEmail"), "", "", 
						verifyEditDraftAndSendMailData.get("Subject"), 
						verifyEditDraftAndSendMailData.get("Subject"));
				attachment.addAttachmentFromDrive(driver, "Sharing Options");
				inbox.tapOnSend(driver);
				userAccount.verifySharingOptionPopUpIsPresent();
				//inbox.tapOnSendSharingAlert(driver);
				driver.navigate().back();
				driver.navigate().back();
				
				log.info("*****************Completed testcase ::: verifyAttachmentSharingOptions************");
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyAttachmentSharingOptions");
			}
		}
		
		
		/**-- Reply to a message in airplane mode
		 * 
		 * @throws Exception
		 */
		@Test(priority = 74)
		public void verifyReplyMailTrashSame() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
				Map<String, String> verifyReplyTrash = CommonPage.getTestData("verifyReplyMailTrashSame");
				
				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyReplyMailTrashSame******************");
				
				String  emailId = CommonFunctions.getNewUserAccount(verifyReplyTrash, "FromAccount", null);
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);
				//userAccount.switchMailAccount(driver, verifyReplyTrash.get("FromAccount"));
				inbox.tapOnFirstMail(driver);
				String[] x = userAccount.getSubjectOfMail(driver, MailSubject, emailSubject);
				for(int i=0; i<=x.length; i++){
					MailSubject = x[0].replace("Inbox", "");
					MailSubject1 = MailSubject.replace(" .", "");
					log.info("AGC MailSubject1 :"+MailSubject1);
					emailSubject = x[1];
				}
				Thread.sleep(1200);
				inbox.scrollDown(4);
				Thread.sleep(1000);
				CommonFunctions.scrollsToTitle(driver, "Reply");
				adbroot = new String[]{"adb", "root"};
			    p = new ProcessBuilder(adbroot).start();
			    log.info("rooting device");
			    Thread.sleep(2000);
			    
			    adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "1"};
			    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
			    log.info("Airplane mode settings to On");
			    Thread.sleep(2000);
			    
			    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
			    p = new ProcessBuilder(adbBroadcastIntent).start();
			    log.info("Airplane mode settings to On");
			    Thread.sleep(2000);
				CommonFunctions.searchAndSendKeysByXpath(driver, verifyReplyTrash.get("ComposeBody"), "Text");
				inbox.tapOnSend(driver);
				CommonFunctions.isElementByXpathDisplayed(driver, verifyReplyTrash.get("Delete"));
				CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("Delete"));
				Thread.sleep(1000);
				if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("OKBtn"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("OKBtn"));
					Thread.sleep(1000);
					if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("BackBtn"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("BackBtn"));
					}
				}
					navDrawer.navigateToTrashFolder(driver);
					log.info("MailSubject1.trim():"+MailSubject1.trim());
					MailSubject1 = MailSubject1.substring(0, MailSubject1.indexOf(" ")); // added by Venkat on 07/06/2019
					userAccount.verifyMailIsPresent(driver, MailSubject1.trim());
					adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "0"};
				    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
				    log.info("Airplane mode settings to Off");
				    Thread.sleep(2000);
				    
				    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
				    p = new ProcessBuilder(adbBroadcastIntent).start();
				    log.info("Airplane mode broadcast to Off");
				    Thread.sleep(10000);
				    
				    log.info("*****************Completed testcase ::: verifyReplyMailTrashSame************");
			}catch(Throwable e){
				adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "0"};
			    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
			    log.info("Airplane mode settings to Off");
			    Thread.sleep(2000);
			    
			    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
			    p = new ProcessBuilder(adbBroadcastIntent).start();
			    log.info("Airplane mode broadcast to Off");
			    Thread.sleep(10000);
				commonPage.catchBlock(e, driver, "verifyReplyMailTrashSame");
			}
		}		
		
		
		/**-- Reply-to for Add another email address without reply-to
		 * 
		 * @throws Exception
		 */
		@Test(priority = 75)
		public void verifyReplyToWithout() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			String[] addtesthyd3 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd3@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd3).start();
		    log.info("Installing account gm.test.hyd3 google account manager apk");
		    Thread.sleep(10000);
		    
		    String[] addtesthyd4 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd4@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd4).start();
		    log.info("Installing account gm.test.hyd4 google account manager apk");
		    Thread.sleep(10000);
		    
		    String[] addtesthyd6 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd6@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd6).start();
		    log.info("Installing account gm.test.hyd6 google account manager apk");
		    Thread.sleep(10000);
			
		    String[] addtesthyd7 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd7@gmail.com", "-e", "password", "Carib0u!@", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd7).start();
		    log.info("Installing account gm.test.hyd7 google account manager apk");
		    Thread.sleep(10000);
		    try{
				Map<String, String> verifyReplyTrash = commonPage.getTestData("verifyReplyMailTrashSame");
				
				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyReplyToWithout******************");
				if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd3")))
				userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd3"));
				CommonFunctions.isElementByXpathDisplayed(driver, verifyReplyTrash.get("ComposeBtn"));
				CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("ComposeBtn"));
				Thread.sleep(1500);
				CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("FromAddChange"));
				Thread.sleep(800);
				if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd3Custom"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd3Custom"));
				}else{
					inbox.scrollUp(1);
					Thread.sleep(800);
					if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd3Custom"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd3Custom"));
						}else{
							inbox.scrollDown(1);
							Thread.sleep(800);
							if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd3Custom"))!=0){
								CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd3Custom"));
								}
						}
				}
				//CommonFunctions.scrollsToTitle(driver, Title);
				Thread.sleep(1200);
			    CommonFunctions.searchAndSendKeysByXpath(driver, verifyReplyTrash.get("ToAddress"), verifyReplyTrash.get("FromAccount"));
			    Thread.sleep(800);
			    CommonFunctions.searchAndSendKeysByXpath(driver, verifyReplyTrash.get("Subject"), "Testing GmHyd4");
			    inbox.tapOnSend(driver);
			    Thread.sleep(1000);
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("FromAccount")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("FromAccount"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailIsPresent(driver, "Testing GmHyd4");
			    userAccount.openMail(driver, "Testing GmHyd4");
			    Thread.sleep(1200);
			    CommonFunctions.scrollsToTitle(driver, "Reply");
			    inbox.tapOnSend(driver);
			    driver.navigate().back();
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd6")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd6"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailIsPresent(driver, "Testing GmHyd4");
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd4")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd4"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailNotPresent(driver, "Testing GmHyd4");
				log.info("*****************Completed testcase ::: verifyReplyToWithout************");
				
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyReplyToWithout");
			}
			}
		
		
		/**-- Reply-to for Add another email address with reply-to
		 * 
		 * @throws Exception
		 */
		@Test(priority = 76)
		public void verifyReplyTo() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			/*String[] addtesthyd3 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd3@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.android.providers.settings/ .GET_ACCOUNTS"};
		    p = new ProcessBuilder(addtesthyd3).start();
		    log.info("Installing account gm.test.hyd3 google account manager apk");
		    Thread.sleep(10000);
		    
		    String[] addtesthyd4_add = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd4@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd4_add).start();
		    log.info("Installing account gm.test.hyd4 google account manager apk");
		    Thread.sleep(10000);
		    
		    String[] addtesthyd6_add = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd6@gmail.com", "-e", "password", "Carib0u!", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd6_add).start();
		    log.info("Installing account gm.test.hyd6 google account manager apk");
		    Thread.sleep(10000);
			
		    String[] addtesthyd7_add = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd7@gmail.com", "-e", "password", "Carib0u!@", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addtesthyd7_add).start();
		    log.info("Installing account gm.test.hyd7 google account manager apk");
		    Thread.sleep(10000);
		    */
			try{
				Map<String, String> verifyReplyTrash = commonPage.getTestData("verifyReplyMailTrashSame");
				
				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyReplyTo******************");
				if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd7")))
				userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd7"));
				CommonFunctions.isElementByXpathDisplayed(driver, verifyReplyTrash.get("ComposeBtn"));
				CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("ComposeBtn"));
				Thread.sleep(1500);
				CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("FromAddChange"));
				Thread.sleep(800);
				if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd7Custom"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd7Custom"));
				}else{
					inbox.scrollUp(1);
					Thread.sleep(800);
					if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd7Custom"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd7Custom"));
						}else{
							inbox.scrollDown(1);
							Thread.sleep(800);
							if(CommonFunctions.getSizeOfElements(driver, verifyReplyTrash.get("GmTestHyd7Custom"))!=0){
								CommonFunctions.searchAndClickByXpath(driver, verifyReplyTrash.get("GmTestHyd7Custom"));
								}
						}
				}
				//CommonFunctions.scrollsToTitle(driver, Title);
				Thread.sleep(1200);
			    CommonFunctions.searchAndSendKeysByXpath(driver, verifyReplyTrash.get("ToAddress"), verifyReplyTrash.get("FromAccountGig2"));
			    Thread.sleep(800);
			    CommonFunctions.searchAndSendKeysByXpath(driver, verifyReplyTrash.get("Subject"), "Testing GmHyd7");
			    inbox.tapOnSend(driver);
			    Thread.sleep(1000);
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("FromAccountGig2")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("FromAccountGig2"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailIsPresent(driver, "Testing GmHyd7");
			    userAccount.openMail(driver, "Testing GmHyd7");
			    Thread.sleep(1200);
			    CommonFunctions.scrollsToTitle(driver, "Reply");
			    inbox.tapOnSend(driver);
			    driver.navigate().back();
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd4")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd4"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailIsPresent(driver, "Testing GmHyd7");
			    Thread.sleep(2000);
			    if(!CommonFunctions.isAccountAlreadySelected(verifyReplyTrash.get("GmTestHyd6")))
			    userAccount.switchMailAccount(driver, verifyReplyTrash.get("GmTestHyd6"));
			    inbox.pullToReferesh(driver);
			    Thread.sleep(2000);
			    inbox.pullToReferesh(driver);
			    userAccount.verifyMailNotPresent(driver, "Testing GmHyd7");
				log.info("*****************Completed testcase ::: verifyReplyTo************");
				/**Removing accounts**/
				String[] addtesthyd3 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd3@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd3).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd4 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd4@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd4).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd6 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd6@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd6).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd7 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd7@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd7).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    Thread.sleep(10000);
			    userAccount.launchGmailApplication(driver);
			}catch(Throwable e){
				/**Removing accounts**/
				String[] addtesthyd3 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd3@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd3).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd4 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd4@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd4).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd6 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd6@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd6).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    String[] addtesthyd7 = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.test.hyd7@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addtesthyd7).start();
			    log.info("Removing account gm.test.hyd3 google account manager apk");
			    Thread.sleep(10000);
			    userAccount.launchGmailApplication(driver);
				commonPage.catchBlock(e, driver, "verifyReplyTo");
			}
			}
		
		/**
		 *Forward HTML Mail
		 *  
		 */
		@Test(groups = {"P2"}, enabled=true, priority = 72)
		public void verifyForwardHTML() throws Exception{
			driver = map.get(Thread.currentThread().getId());
/*			
			String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ForwardHTML.bat";
			log.info("Executing the bat file to generate a forward HTML "+command);
			Process p = new ProcessBuilder(command).start();
			
			Thread.sleep(25000);
*/			
			try{
				Map<String, String> verifyComposeAndSendMailData = CommonPage.getTestData("verifyComposeAndSendMail");
				
				//String emailId = verifyComposeAndSendMailData.get("AccountOne");
				String emailId = CommonFunctions.getNewUserAccount(verifyComposeAndSendMailData, "AccountOne", null);

				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyForwardHTML******************");
				//userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
				
				String mailSubject = verifyComposeAndSendMailData.get("ForwrdHTMLMail");
				CommonFunctions.smtpMailGeneration(emailId,mailSubject, "forward.html", null);

				userAccount.openMail(driver, mailSubject);
				//userAccount.openMail(driver, verifyComposeAndSendMailData.get("ForwrdHTMLMail"));
				CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("Forward"));
				inbox.hideKeyboard();
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("QuotedText"));
				CommonFunctions.searchAndSendKeysByXpath(driver, verifyComposeAndSendMailData.get("ToField"), "gm.gig2.auto@gmail.com");
				userAccount.tapOnSend(driver);
				Thread.sleep(1000);
				driver.navigate().back();
				Thread.sleep(1200);
				if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("ToFieldData")))
				userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("ToFieldData"));
				inbox.pullToReferesh(driver);
				Thread.sleep(1200);
				inbox.pullToReferesh(driver);
				//userAccount.openMail(driver, verifyComposeAndSendMailData.get("ForwrdHTMLMail"));
				userAccount.openMail(driver, mailSubject);
				CommonFunctions.isElementByXpathDisplayed(driver, "//android.view.View[@text='Forwarding HTML']");
				driver.navigate().back();
				log.info("*****************Completed testcase ::: verifyForwardHTML************");
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyForwardHTML");
			}
		}	
		/**-- Smart compose
		 * @author batchi
		 * @throws Exception
		 */
		@Test(priority = 76)
		public void verifySmartCompose() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> verifySmartComposeData = commonPage
					.getTestData("SmartCompose");
			String fromUser=inbox.composeAsNewMail(driver,
					verifySmartComposeData.get("ToEmail"), "", "", 
					verifySmartComposeData.get("Subject"), 
					verifySmartComposeData.get("Body"));
			Thread.sleep(2000);
			
//		CommonFunctions.searchAndClickByXpath(driver, verifySmartComposeData.get("SmartCompose") );
		
			
			
		}
		/**
		 * -- Save to Photos
		 * 
		 * @author Santosh
		 * @throws Exception
		 */

		public void saveToPhotos() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> verifyAddingNewGmailAccountData = commonPage
					.getTestData("verifyAddingNewGmailAccount");
			// inbox.OpenDownloadsApp(driver);
			Process p;

			try {
				log.info("*****************Started testcase ::: verifySaveToPhotos*****************************");
				inbox.verifyGmailAppInitState(driver);
				userAccount.switchMailAccount(driver,
						verifyAddingNewGmailAccountData.get("UserList"));
				inbox.saveToPhotos(driver);
				log.info("*****************Completed testcase ::: verifySaveToPhotos*****************************");
				driver.navigate().back();
			} catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifySaveToPhotos");
			}
		}

		/**
		 * -- offline Mail Sending
		 * 
		 * @author batchi
		 * @throws Exception
		 */

		public void offlineMailSending() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> offlineMailSendingData = commonPage.getTestData("OfflineMailSending");

			try {
				log.info("*****************Started testcase ::: offlineMailSending *****************************");
				inbox.verifyGmailAppInitState(driver);
				userAccount.switchMailAccount(driver,offlineMailSendingData.get("account1"));
				log.info("Turning OFF WIFI");
				Runtime.getRuntime().exec("adb shell svc wifi disable");
				Thread.sleep(3000);
				inbox.composeAndSendWithAttachments(driver,offlineMailSendingData.get("mailwithattchmentSubject"));
				inbox.composeAndSendMail(driver, offlineMailSendingData.get("account2"),"" ,"",
						offlineMailSendingData.get("mailwithoutattchmentSubject"), offlineMailSendingData.get("mailwithoutattchmentSubject"));
				inbox.verifyOfflineToastAndOutboxTip(driver);
				log.info("Turning ON WIFI");
				Runtime.getRuntime().exec("adb shell svc wifi enable");
				Thread.sleep(6000);
				inbox.verifyMailSentAfterOnline(driver);			
				log.info("*****************Ended testcase ::: offlineMailSending *****************************");

			} catch (Throwable e) {
				commonPage.catchBlock(e, driver, "offlineMailSending");
			}
		}
		
		/**
		 * -- attach Multiple Files in compose
		 * 
		 * @author batchi
		 * @throws Exception
		 */

		public void attachDeleteAttachments() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> attachMultipleFilesData = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

			try {
				log.info("*****************Started testcase ::: attachDeleteAttachments *****************************");
				inbox.verifyGmailAppInitState(driver);
				userAccount.switchMailAccount(driver,attachMultipleFilesData.get("etouchTestTwoAccount"));
				inbox.addingMultipleFiles(driver);
				inbox.deletingMultipleFiles(driver);
				log.info("*****************Ended testcase ::: attachDeleteAttachments *****************************");

			} catch (Throwable e) {
				commonPage.catchBlock(e, driver, "attachDeleteAttachments");
			}
		}
	    /**
			 * @TestCase  : verifyPromptToEnterPassword	 *  
			 * @throws: Exception
			 * GMAT
			 */
			    @Test(groups = {"P1"}, enabled = true, priority = 00)
		    	public void PromptToEnterPassword() throws Exception{
				driver = map.get(Thread.currentThread().getId());
		    	Map < String,
		    	String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");

				try{
					log.info("*****************Started testcase ::: verifyPromptToEnterPassword*****************************");
						inbox.verifyGmailAppInitState(driver);
			    		inbox.PromptToEnterPassword(driver);
				    log.info("*****************Completed testcase ::: verifyPromptToEnterPassword*****************************");   
				    driver.navigate().back();
				}catch(Throwable e)
				{
					commonPage.catchBlock(e, driver, "verifyPromptToEnterPassword");
				}
			}


		/**
		 * -- verify Schedule send from Compose, Reply all and Forward pages
		 * 
		 * @author batchi
		 * @throws Exception
		 */

		public void verifyScheduleSend() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

			try {
				log.info("*****************Started testcase ::: verifyScheduleSend *****************************");
				userAccount.switchMailAccount(driver, composeAndSendMailData.get("AccountOne"));
			// Scheduled email from Compose
				log.info("Verifying schedule send from Compose");
				String fromUser= inbox.composeAsNewMail(driver,
						composeAndSendMailData.get("ToFieldData"),
						composeAndSendMailData.get("CCFieldData"),"",
						composeAndSendMailData.get("ScheduleSendSubject"),composeAndSendMailData.get("ScheduleSendSubject"));
				
				String emailScheduledTime = inbox.verifyScheduleSendAfterSomeMins(driver, composeAndSendMailData.get("ScheduleSendSubject"),4);
				navDrawer.navigateToScheduled(driver);
				Thread.sleep(2000);
				inbox.verifyMailPresent(driver,composeAndSendMailData.get("ScheduleSendSubject") );
				navDrawer.navigateBack(driver);
				inbox.afterScheduledTime(driver, emailScheduledTime, composeAndSendMailData.get("ToFieldData"), composeAndSendMailData.get("ScheduleSendSubject"));
			//  Scheduled email from Reply all
				log.info("Verifying schedule send from Reply all");
				inbox.openMail(driver,composeAndSendMailData.get("ScheduleSendSubject") );
				CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("ReplyAllButton"));
				String emailScheduledTimeReplyAll = inbox.verifyScheduleSendAfterSomeMins(driver, composeAndSendMailData.get("ScheduleSendSubject"),4);
				navDrawer.navigateUntilHamburgerIsPresent(driver);
				navDrawer.navigateToScheduled(driver);
				Thread.sleep(2000);
				inbox.verifyMailPresent(driver,composeAndSendMailData.get("ScheduleSendSubject") );
				navDrawer.navigateBack(driver);
				inbox.afterScheduledTime(driver, emailScheduledTimeReplyAll, composeAndSendMailData.get("CCFieldData"), composeAndSendMailData.get("ScheduleSendSubject"));
				userAccount.switchMailAccount(driver, composeAndSendMailData.get("AccountOne"));
				inbox.verifyMailPresentNoOpen(driver,composeAndSendMailData.get("ScheduleSendSubject") );
			//  Scheduled email from Forward
				log.info("Verifying schedule send from forward");	
				inbox.openMail(driver,composeAndSendMailData.get("ScheduleSendSubject") );
				CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("ForwardButton"));
				CommonFunctions.searchAndSendKeysByXpath(driver,composeAndSendMailData.get("ToFieldXpath") , composeAndSendMailData.get("BccFieldData"));
			    String emailScheduledTimeForward = inbox.verifyScheduleSendAfterSomeMins(driver, composeAndSendMailData.get("ScheduleSendSubject"),4);
			    navDrawer.navigateUntilHamburgerIsPresent(driver);
			    navDrawer.navigateToScheduled(driver);
				Thread.sleep(2000);
				inbox.verifyMailPresent(driver,composeAndSendMailData.get("ScheduleSendSubject") );
				navDrawer.navigateBack(driver);
				inbox.afterScheduledTime(driver, emailScheduledTimeForward, composeAndSendMailData.get("BccFieldData"), composeAndSendMailData.get("ScheduleSendSubject"));
				
				log.info("*****************Ended testcase ::: verifyScheduleSend *****************************");
			} catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifyScheduleSend");
			}
		}
		/**
		 * -- verify Schedule send cancel  option 
		 * 
		 * @author batchi
		 * @throws Exception
		 */

		public void verifyScheduleSendCancel() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

			try {
				log.info("*****************Started testcase ::: verifyScheduleSend *****************************");
				userAccount.switchMailAccount(driver, composeAndSendMailData.get("AccountOne"));
			//Compose Canceling scheduled mail from TL view
				log.info("Verifying schedule send from Compose");
				String fromUser= inbox.composeAsNewMail(driver,
						composeAndSendMailData.get("ToFieldData"),
						composeAndSendMailData.get("CCFieldData"),"",
						composeAndSendMailData.get("CancelFromTLSubject"),composeAndSendMailData.get("CancelFromTLSubject"));
				
				inbox.verifyScheduleSendAfterSomeMins(driver,composeAndSendMailData.get("CancelFromTLSubject"),4);
				navDrawer.navigateToScheduled(driver);
				inbox.selectMail(driver,composeAndSendMailData.get("CancelFromTLSubject"));
				inbox.verifyCancelScheduleSend(driver, composeAndSendMailData.get("CancelFromTLSubject"));
			//Compose Canceling scheduled email from CV view
				navDrawer.openMenuDrawer(driver);
				navDrawer.navigateToDrafts(driver);
				inbox.openMail(driver,composeAndSendMailData.get("CancelFromTLSubject"));
				CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("EditMail"));
				inbox.verifyScheduleSendAfterSomeMins(driver,composeAndSendMailData.get("CancelFromTLSubject"),4);
				navDrawer.navigateToScheduled(driver);
			    inbox.openMail(driver,composeAndSendMailData.get("CancelFromTLSubject"));
			    inbox.verifyCancelScheduleSend(driver, composeAndSendMailData.get("CancelFromTLSubject"));
			    commonPage.openDraftsInEditView(driver, composeAndSendMailData.get("CancelFromTLSubject"));
			    driver.navigate().back();
			    navDrawer.navigateToPrimary(driver);
			// Reply all  Canceling scheduled email from TL view
			    String mailSubjectReplyAllTL = inbox.selectScheduleSendOption(driver, composeAndSendMailData.get("ReplyAllButton"), composeAndSendMailData.get("ScheduleSendTomorrowMorning"));
			    navDrawer.navigateToScheduled(driver);
			    inbox.selectMail(driver,mailSubjectReplyAllTL);
			    inbox.verifyCancelScheduleSend(driver, mailSubjectReplyAllTL);
			    commonPage.openDraftsInEditView(driver, mailSubjectReplyAllTL);
			    
			// Reply all  Canceling scheduled email from CV view
			    String mailSubjectReplyAllCV = inbox.selectScheduleSendOption(driver, composeAndSendMailData.get("ReplyAllButton"), composeAndSendMailData.get("ScheduleSendTomorrowAfternoon"));
			    inbox.openMail(driver,mailSubjectReplyAllCV);
			    inbox.verifyCancelScheduleSend(driver, mailSubjectReplyAllCV);
			    commonPage.openDraftsInEditView(driver, mailSubjectReplyAllCV);
			 // Forward Canceling scheduled email from TL view
			    String mailSubjectForwardTL = inbox.selectScheduleSendOption(driver, composeAndSendMailData.get("ForwardButton"), composeAndSendMailData.get("ScheduleSendMMondayMorning"));
			    navDrawer.navigateToScheduled(driver);
			    inbox.selectMail(driver,mailSubjectForwardTL);
			    inbox.verifyCancelScheduleSend(driver, mailSubjectForwardTL);
			    commonPage.openDraftsInEditView(driver, mailSubjectForwardTL);
			// Forward  Canceling scheduled email from CV view
			    String mailSubjectForwardCV = inbox.selectScheduleSendOption(driver, composeAndSendMailData.get("ForwardButton"), composeAndSendMailData.get("ScheduleSendTomorrowAfternoon"));
			    inbox.openMail(driver,mailSubjectForwardCV);
			    inbox.verifyCancelScheduleSend(driver, mailSubjectForwardCV);    
			    
			log.info("*****************Ended testcase ::: verifyScheduleSend *****************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyScheduleSend");
		}
		}	
		
	}
