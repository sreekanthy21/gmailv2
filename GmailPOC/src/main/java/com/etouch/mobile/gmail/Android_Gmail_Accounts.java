package com.etouch.mobile.gmail;

import java.awt.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Accounts extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	boolean isGmailWelComePageDisplayed;
	boolean flag;
	
	/*@Test
	public void ExecuteAllAccounts() throws Exception{
		verifyAddingNewGmailAccount();
		verifyAddingOtherAccounts();
		verifyRemoveAccount();
	}*/
	
	/**
	 * @TestCase 1 : Add new Gmail account(Consumer account) to app
	 * @Pre-condition : Gmail App should be installed in testing device.
	 * GMAT 4
	 * @throws: Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 1)
	public void verifyAddingNewGmailAccount() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAddingNewGmailAccount*****************************");
			//inbox.verifyGmailAppInitState(driver);
			Map < String, String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");

	   	        String expectedAccountTypeList = verifyAddingNewGmailAccountData.get("ExpectedAccountTypeList");
	   	        isGmailWelComePageDisplayed = userAccount.isGmailWelComePageDisplayed(driver);
	   	        if (isGmailWelComePageDisplayed) {
	   	            userAccount.addNewAccountFromGmailHomePage(driver);
	   	        } 

	   	        //userAccount.addNewMultipleGmailAccount(driver,verifyAddingNewGmailAccountData.get("UserList"),verifyAddingNewGmailAccountData.get("Password"),isGmailWelComePageDisplayed, expectedAccountTypeList);
	   	        userAccount.addNewMultipleGmailAccountNew(driver,verifyAddingNewGmailAccountData.get("UserList"),verifyAddingNewGmailAccountData.get("Password"),isGmailWelComePageDisplayed, expectedAccountTypeList);
	   	        //driver.navigate().back();
	   	       // userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("AccountOne"));
			log.info("*****************Completed testcase ::: verifyAddingNewGmailAccount*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAddingNewGmailAccount");
		}
	}
	
	/** Testcase to verify adding IMAP , POP . Exchange accounts in app
	 * Outlook/Hotmail/Yahoo/Other
	 * 
	 * GAT5
	 * @preCondition: Latest Gmail and Exchange apks installed 
	 * @throws Exception
	 * GMAT 5
	 * @author Phaneendra
	 */
	@Test(groups = {"P1"}, enabled=true, priority = 93)
	public void verifyAddingOtherAccounts()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("******************Started testcase ::: verifyAddingOtherAccounts******************");
		
			Map<String, String> getEmailAddressData = commonPage.getTestData("getEmailAddress");
			
			inbox.verifyGmailAppInitState(driver);
		//	commonFunction.switchOnAirPlaneMode();
//			userAccount.removeAccountIMAPPOP3(driver, 
//					getEmailAddressData.get("OutlookEmailAddress"), getEmailAddressData.get("YahooEmail"),
//					getEmailAddressData.get("ExchangeEmailAddress"), getEmailAddressData.get("OtherEmailAddress"), 
//					getEmailAddressData.get("OtherEmailAddress1"));
		//	commonFunction.switchOffAirPlaneMode();
			//--Adding Outlook,Hotmail, and Live Account--//
			log.info("**********Adding Outlook/Hotmail/Live Account*********");
			inbox.launchGmailApplication(driver);
			
			//inbox.openMenuDrawer(driver);// Commmented by Venkat on 16/11/2018
			//userAccount.clickOnAccountSwitcher(driver);// Commmented by Venkat on 16/11/2018
			
			if (CommonFunctions.getSizeOfElements(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']") != 0) {
				userAccount.clickOnAccountSwitcher(driver);
			}else{
				inbox.openMenuDrawer(driver);
			}
			userAccount.addOutlookHotmailLiveAcct(driver, getEmailAddressData.get("OutlookEmailAddress"),getEmailAddressData.get("EmailPwdMicrosoft"));
			
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OutlookEmailAddress")); 
			
			//--Adding Yahoo Account--//
			log.info("**********Adding Yahoo Account*********");
			userAccount.clickAddAccount(driver);
			userAccount.addYahooAccount(driver, getEmailAddressData.get("YahooEmail"),
					getEmailAddressData.get("YahooPassword"));
			//userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("YahooEmail")); //Commented by Venkat on 02/01/2019
			// added by Venkat on 02/01/2019
			try {
				userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("YahooEmail"));
			} catch (Exception e) {
			}
			
			//--Adding Exchange Account--//
			log.info("*************Adding Exchange and Office 365 Account***********");
			userAccount.clickAddAccount(driver);
			userAccount.addExchangeAccount(driver, getEmailAddressData.get("ExchangeEmailAddress"),
					getEmailAddressData.get("ExchangePassword"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("ExchangeEmailAddress"));
		
			String imap = "IMAP", pop3 = "POP3";
			//--Adding Other Account--//
		
			log.info("*************Adding gmx Account using Other option***********");
			userAccount.clickAddAccount(driver);
			userAccount.addingOtherAccount(driver, getEmailAddressData.get("OtherAccount"),
					getEmailAddressData.get("OtherAccountPwd"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OtherAccount"));
			
			
			//--Adding Other Account using IMAP setting--//
			/*
			log.info("***************Adding Other Account using IMAP settings*************");
			userAccount.clickAddAccount(driver);
			userAccount.addOtherAccount(driver, imap, getEmailAddressData.get("OtherEmailAddress"),
					getEmailAddressData.get("OtherPassword"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OtherEmailAddress"));*/
			/*//--Adding the Other Account using POP3 settings--//
			log.info("***************Adding account using POP3 Settings*******************");
			userAccount.clickAddAccount(driver);
			userAccount.addOtherAccount(driver, pop3, getEmailAddressData.get("OtherEmailAddress1"),
					getEmailAddressData.get("OtherPassword1"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OtherEmailAddress1"));*/
			//--Verifying the account are added with server settings--//
			userAccount.manageAccount(driver);
			//userAccount.verifyServerAccount(driver);
			log.info("*****************Completed testcase ::: verifyAddingOtherAccounts************");
			driver.navigate().back();
			driver.navigate().back();
		}catch(Throwable e){
			
			commonPage.catchBlock(e, driver, "verifyAddingOtherAccounts");
		}
	}

	public void verifyAddingOtherAccountsNew()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{

			Map<String, String> emailAddressData = CommonPage.getTestData("getEmailAddress");
			inbox.verifyGmailAppInitState(driver);
			String outlookEmailId = emailAddressData.get("OutlookEmailAddress");
			String outlookPwd = emailAddressData.get("EmailPwdMicrosoft");
			String yahooEmailId = emailAddressData.get("YahooEmail");
			String yahooPwd= emailAddressData.get("YahooPassword");
			String exchangeEmailId = emailAddressData.get("ExchangeEmailAddress");
			String otherEmailId = emailAddressData.get("OtherEmailAddress");
			String otherEmailId1 = emailAddressData.get("OtherEmailAddress1");
			
			//userAccount.removeAccountIMAPPOP3New(driver, outlookEmailId, yahooEmailId,exchangeEmailId, otherEmailId, otherEmailId1);
			//inbox.launchGmailApplication(driver);
			Thread.sleep(1000);
			userAccount.goToAccountsListPage(driver);
			CommonFunctions.scrollTo(driver, "Add another account");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Add another account']");
			userAccount.addOutlookHotmailLiveAcctNew(driver, outlookEmailId,outlookPwd);

			CommonFunctions.scrollTo(driver, "Add another account");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Add another account']");
			userAccount.addYahooAccount(driver,yahooEmailId,yahooPwd);
			userAccount.verifyExchangeAccountAdded(driver, yahooEmailId);

		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyAddingOtherAccounts");
		}
	}
	
	/**
	 * @TestCase 18 : Manage Accounts - Verify Remove account
	 * @Pre-condition :
	 * GMAT 6
	 * @throws: Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 6)
	public void verifyRemoveAccount() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyRemoveAccount******************************");
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyManageAccountData = commonPage
					.getTestData("verifyRemoveAccount");
			userAccount.removeAccount(driver,
					verifyManageAccountData.get("MailID"), verifyManageAccountData.get("RemoveAccountMoreOptions"));
			// userAccount.removeAllAccounts();
			log.info("*****************Completed testcase ::: verifyRemoveAccount******************************");
			 String[] addAccountTwo = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig1.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    Process p = new ProcessBuilder(addAccountTwo).start();
			    log.info("Installing account gm.gig1.auto with google account manager apk");
			    Thread.sleep(5000);
		} catch (Throwable e) {
			 String[] addAccountTwo = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig1.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    Process p = new ProcessBuilder(addAccountTwo).start();
			    log.info("Installing account gm.gig1.auto with google account manager apk");
			    Thread.sleep(5000);
			commonPage.catchBlock(e, driver, "verifyRemoveAccount");
		}
	}
	
	/**
	 * @TestCase Google apps in drawer
	 * @Pre-condition :
	 * GMAT 6
	 * @throws: Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 8)
	  public void GoogleAppsInDrawer(AppiumDriver driver)throws Exception{
			driver = map.get(Thread.currentThread().getId());
		  try{
	    		 log.info("----------Started testcase Google apps in drawer----------");
	    		 Map < String, String > appsInDrawerData = commonPage.getTestData("AppsInDrawer");
	   //Added by Bindu on 30/07
	    		 log.info("Launching Settings to Disable the Calendar and Contact apps");
	    		 commonPage.launchSettingsAppAndDisableApp(driver,appsInDrawerData.get("CalendarAppSearchKeyword"));     		 
	    		 commonPage.launchSettingsAppAndDisableApp(driver,appsInDrawerData.get("ContactsAppSearchKeyword") ); 	
	    		//Launching Gmail app
	    		 UserAccount.launchGmailApp(driver);
	    		 inbox.openMenuDrawer(driver);
	    		 CommonFunctions.scrollsToTitle(driver, "Calendar");
	    		 CommonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("PlaystoreAppLaunched").replace("AppName", appsInDrawerData.get("CalendarAppSearchKeyword")));
	    		 log.info("Verifying apps uninstalled redirects to Playstore or not");
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("EnableButton"))!=0){
	    			 log.info("*********** Playstore app  launched with respective app ***********");
	    			 CommonFunctions.searchAndClickByXpath(driver,appsInDrawerData.get("EnableButton") );
	    			 CommonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("PlayStoreOpenButton"));
	    			 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("PlaystoreUpdateButton"))!=0){
	    				 CommonFunctions.searchAndClickByXpath(driver, appsInDrawerData.get("PlaystoreUpdateButton"));
	    				 commonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("PlaystoreUninstallButton"));
	    			 }
	    		 }else{
	    			 throw new Exception("!!!!!!! Playstore app is not launched !!!!!!!!!!");
	    		 }
	    		 driver.navigate().back();
	 			 inbox.openMenuDrawer(driver);
	 			 Thread.sleep(800);
	    		 CommonFunctions.scrollsToTitle(driver, "Contacts");
	    		 CommonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("PlaystoreAppLaunched").replace("AppName", appsInDrawerData.get("ContactsAppSearchKeyword")));
	    		 log.info("Verifying apps uninstalled redirects to Playstore or not");
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("EnableButton"))!=0){
	    			 log.info("*********** Playstore app  launched with respective app ***********");
	    			 CommonFunctions.searchAndClickByXpath(driver,appsInDrawerData.get("EnableButton") );
	    			 CommonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("PlayStoreOpenButton"));
	    		 }else{
	    			 throw new Exception("!!!!!!! Playstore app is not launched !!!!!!!!!!");
	    		 }
	    		 driver.navigate().back();
	    		 
	   //Ended by Bindu on 31/07 		  		  
	    		 inbox.openMenuDrawer(driver);
	    		 CommonFunctions.scrollsToTitle(driver, "Calendar");
	   //Added by Bindu on 31/07
	    		 log.info("Verifying app launch is properly launched or not");
	    		 Thread.sleep(5000);
	    		
//	    		CommonFunctions.isElementByXpathDisplayed(driver, appsInDrawerData.get("CalendarAppJumpToTodayButton"));
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("NextPageButton"))!=0){
	    			 for(int i=0;i<5;i++){
	    				 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("NextPageButton"))!=0)
	    					 CommonFunctions.searchAndClickByXpath(driver,appsInDrawerData.get("NextPageButton") );
	    			 }
	    		 
	    			 CommonFunctions.searchAndClickByXpath(driver, appsInDrawerData.get("GotItButton"));
	    			 
	    		 }
	    		 commonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("CalendarAppJumpToTodayButton"));
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("CalendarAppJumpToTodayButton"))!=0){
    				 log.info("************ The Calendar app is launched properly ***************");
    			 }else{
    				 throw new Exception("!!!!!!!!!! The calendar app is not launched !!!!!!!!!!!!");
    			 }
	    		 Thread.sleep(1500);
	    		
	   //Ended by Bindu on 31/07 		 
	    		 
	    		 Thread.sleep(4000);
	    		 driver.navigate().back();
	 			 inbox.openMenuDrawer(driver);
	 			 Thread.sleep(800);
	    		 CommonFunctions.scrollsToTitle(driver, "Contacts");
	    		 Thread.sleep(2000);
	//Added by Bindu  on 05/08  
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("AllowButton"))!=0){
	    			 for(int i=0;i<5;i++){
	    				 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("AllowButton"))!=0)
	    					 CommonFunctions.searchAndClickByXpath(driver,appsInDrawerData.get("AllowButton") );
	    			 }
	    		 }
	    		 commonPage.waitForElementToBePresentByXpath(appsInDrawerData.get("ContactsAppVerificationLocator"));
	    		 
	    		 if(CommonFunctions.getSizeOfElements(driver, appsInDrawerData.get("ContactsAppVerificationLocator"))!=0){
	    			 log.info("********* Contacts app is launched properly *************");
	    		 }else{
	    			 throw new Exception("!!!!!!! Contacts app is not launched properly !!!!!!!!!!");
	    		 }
	//Ended by Bindu on 05/08    		 
	    		 Thread.sleep(3500);
	    		 driver.navigate().back();
	    		 log.info("----------Completed testcase Google apps in drawer----------");
	    	 }catch(Exception e){
	    		 e.printStackTrace();
	    	 }
	     }
	/**
	 * @TestCase sanity IMAP : IMAP Accounts - Verify navigating to IMAP and send an email
	 * @Pre-condition :
	 * GMAT 
	 * @throws: Exception
	 * @author batchi
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyEmailSendIMap() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = CommonPage.getTestData("IMAPAccountDetails");

		//userAccount.switchMailAccount(driver, verifySendMailIMAPData.get("OutlookEmailAddress"));
		String  emailId = CommonFunctions.getNewUserAccount(verifySendMailIMAPData, "OutlookEmailAddress", "IMAP");

		if(!CommonFunctions.isAccountAlreadySelected(emailId))
			userAccount.switchMailAccount(driver,emailId);
		
		String fromUser = inbox.composeAsNewMail(driver,
				verifySendMailIMAPData.get("ToFieldData"),
				verifySendMailIMAPData.get(" "), " ",
				verifySendMailIMAPData.get("SubjectData"),
				verifySendMailIMAPData.get("ComposeBodyData"));
		
		inbox.tapOnSendForNewMail();
		boolean isMailFound = true;
		try {
			inbox.verifymailInOutbox(driver);
		}catch(Exception e) {
			isMailFound = false;
		}
		//Thread.sleep(15000);
		Thread.sleep(5000);
		try {
			conversationView.verifyEmailInSent(driver);		
		} catch (Exception e) {
			if(!isMailFound) {
				throw new Exception("No mail found either in Outbox or Sent folder. Please check..");
			}
		}
		
	}
	/**
	 * @TestCase exploratory : verify Remove all accounts
	 * @Pre-condition :
	 * GMAT 
	 * @throws: Exception
	 * @author batchi
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyRemoveAllAccountsDoesnotCrash() throws Exception {
		log.info(" ************ Started TestCase: verifyRemoveAllAccountsDoesnotCrash  ************");
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > removeAllAccountsData = commonPage.getTestData("verifyLauncherShortcut");
		Process p1;
		String[] accountManageApk = new String[] {"adb", "install",  "-r", "-d", "path" + java.io.File.separator + "Gmail_Apk" + java.io.File.separator + "GoogleAccountManagerApp.apk"};
		 p1 = new ProcessBuilder(accountManageApk).start();
		 Thread.sleep(2000);
		 log.info("Installed Google Acocunt Manager Apk");
		CommonFunctions.searchAndClickByXpath(driver, removeAllAccountsData.get("AccountIconNew"));
		ArrayList<String> al1= new ArrayList<String>();
		java.util.List<WebElement> AccountsList = driver.findElementsByXPath(removeAllAccountsData.get("SwitcherAccountsCommon"));
		log.info("Account list size:  " +AccountsList.size());
		Iterator<WebElement> iters = AccountsList.iterator();
		while (iters.hasNext()){
			WebElement row = iters.next();
			log.info(row.getText());
			userAccount.verifyRemoveMultipleAccounts(driver,row.getText());
			/*String value= row.getText();
			al1.add(value);
			log.info(al1);*/
			Thread.sleep(2000);
		}
//		userAccount.launchGmailApplication(driver);
		commonPage.waitForElementToBePresentByXpath(removeAllAccountsData.get("WelcomeToGmail"));
		if(CommonFunctions.getSizeOfElements(driver,removeAllAccountsData.get("WelcomeToGmail") )!=0){
			log.info(" @@@@@@@@@@ Welcome to Gmail page is displayed @@@@@@@@@@@@ ");
		}else{
			throw new  Exception("!!!!!!!!!! Welcome to Gmail page is not displayed !!!!!!!!!!!! ");
		}
	   	   userAccount.verifyAlertToAddAccount(driver);
	   log.info(" ************ Ended TestCase: verifyRemoveAllAccountsDoesnotCrash  ************");
		
	}
	
}
