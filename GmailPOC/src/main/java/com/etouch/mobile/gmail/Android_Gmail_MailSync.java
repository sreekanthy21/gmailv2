package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_MailSync extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void ExecuteAllMailSync() throws Exception{
		testAccountSyncWorksForGoogleAccounts();
		verifySwipeDownToRefreshWorks();
		turnOffAccountSync();
		
	   }*/
	
	/**
	 * @TestCase 17 :Turn off account sync /**
	 * @TestCase 3 : Turn off account sync
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 57
	 * @throws Exception 
	 */	 
	@Test(groups = {"P0"}, enabled = true, priority = 2)
	public void testAccountSyncWorksForGoogleAccounts() throws Exception  {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: testAccountSyncWorksForGoogleAccounts***************************");
			Thread.sleep(10000);
			inbox.verifyGmailAppInitState(driver);
			 inbox.verifyContentPresent(driver);
  			 inbox.verifyPeriodicSync(driver);
			log.info("*****************Completed testcase ::: testAccountSyncWorksForGoogleAccounts*************************");
     } catch (Throwable e) {
			commonPage.catchBlock(e, driver, "testAccountSyncWorksForGoogleAccounts");

		}
	}

	/**
	 * @throws Exception 
	 * @TestCase 7 : Test Account sync works for non Google accounts
	 *  
	 * @throws: Exception
	 */
	
	@Test(groups = {"P0"}, enabled = true, priority = 95)
	public void testAccountSyncWorksFornonGoogleAccounts() throws Exception  {
		driver = map.get(Thread.currentThread().getId());
		try {
			
			Map<String, String> ValidateReportSpamForIMAPAccounts = CommonPage.getTestData("ValidateReportSpamForIMAPAccounts");

			log.info("*****************Started testcase ::: testAccountSyncWorksForGoogleAccounts***************************");
			Thread.sleep(10000);
			inbox.verifyGmailAppInitState(driver);
			inbox.testAccountSyncWorksFornonGoogleAccounts(driver);
			driver.closeApp();
			inbox.verifyGmailAppInitState(driver);
			userAccount.removeAccountNonGmail(driver,ValidateReportSpamForIMAPAccounts.get("ImapAccount"), ValidateReportSpamForIMAPAccounts.get("AccountOne"));
			log.info("*****************Completed testcase ::: testAccountSyncWorksForGoogleAccounts*************************");
     } catch (Exception e) {
			commonPage.catchBlock(e, driver, "testAccountSyncWorksFornonGoogleAccounts");

		}
	}
	
	/**Testcase to verify the swipe down to refresh is working
	 * 
	 * @preCondition Add Gmail account to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 59
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 38)
	public void verifySwipeDownToRefreshWorks()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> swipeDownToRefreshData = CommonPage.getTestData("swipeDownToRefresh");
			log.info("*****************Started testcase ::: verifySwipeWorks*****************************");
			
			//userAccount.verifyAccountsExists(driver, swipeDownToRefreshData.get("Account"), swipeDownToRefreshData.get("AccountPassword"));
			//userAccount.switchMailAccount(driver, swipeDownToRefreshData.get("Account"));
			//driver.navigate().back();
			//navDrawer.navigateToPrimary(driver);
			
			String emailId = CommonFunctions.getNewUserAccount(swipeDownToRefreshData, "Account", null);

			String emailSubject = swipeDownToRefreshData.get("EmailSubject");
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId); 
					
			CommonFunctions.smtpMailGeneration(emailId, emailSubject, "SwipeDownToRefresh.html", null);

			//inbox.verifySwipeDownToRefresh(driver, swipeDownToRefreshData.get("TestSyncOff"));// Commented by Venkat on 14/11/2018
			inbox.verifySwipeDownToRefresh(driver, emailSubject); // Modified by Venkat on 14/11/2018

			if(CommonFunctions.getSizeOfElements(driver, swipeDownToRefreshData.get("AccountSyncOFF"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, swipeDownToRefreshData.get("AccountSyncOFF"));
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			}
			log.info("*****************Completed testcase ::: verifySwipeWorks*************************");
			driver.navigate().back();
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySwipeWorks");
		}
	}
	
			/**
			 * @TestCase 17 :Turn off account sync /**
			 * @TestCase 3 : Turn off account sync
			 * @Pre-condition : Gmail build 6.0+
			 * @throws: Exception
			 * @author Rahul
			 * GMAT 60
			 */	 
			@Test(groups = {"P2"}, enabled = true, priority = 37)
		   public void turnOffAccountSync() throws Exception {
				
				driver = map.get(Thread.currentThread().getId());
				try {
					log.info("*****************Started testcase ::: turnOffAccountSync*************************");
					/*Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage.getTestData("testAccountSyncWorksForGoogleAccounts");
			        Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			        Map<String, String>ValidateReportSpamForIMAPAccounts= commonPage.getTestData("ValidateReportSpamForIMAPAccounts");
			        Map<String, String> richTextFormattingFeatures = commonPage.getTestData("richTextFormattingFeatures");*/
			        inbox.verifyGmailAppInitState(driver);
					//userAccount.navigateToPrimary(driver);
			        inbox.turnOffAccountSync(driver);
					
					log.info("*****************Completed testcase ::: turnOffAccountSync*****************************");
				} catch (Throwable e) {
							}
			}
	
}
