package com.etouch.mobile.gmail;

import java.util.HashSet;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class GmailTest2 extends GmailTest {
	static Log log = LogUtil.getLog(GmailTest.class);

	AppiumDriver driver = null;

	@Test(enabled = true, priority = 21)
	public void verifyHelpAndFeedbackSection() throws Exception {
		try {
			SoftAssert softAssert = new SoftAssert();
			driver = map.get(Thread.currentThread().getId());
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyHelpAndFeedbackSectionData = CommonPage
					.getTestData("verifyHelpAndFeedbackSection");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyHelpAndFeedbackSectionData.get("AccountTwo")))
			userAccount.switchMailAccount(driver, verifyHelpAndFeedbackSectionData.get("AccountTwo"));
			userAccount.openMenuDrawer(driver);
			userAccount.openHelpAndFeedbackSection(driver);
			userAccount.verifyHelpDisplayed(driver,verifyHelpAndFeedbackSectionData.get("HelpText"));
			userAccount.ClickToSendFeedback(driver);
			userAccount.verifyforEditingAndSentFeedback(driver,verifyHelpAndFeedbackSectionData.get("feedbackText"));
			Thread.sleep(3000);
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage("verifyHelpAndFeedbackSection"),
					"Image comparison for verifyHelpAndFeedbackSection");

			softAssert.assertAll();
			Thread.sleep(3000);
			commonFunction.navigateBack(driver);
			} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyHelpAndFeedbackSection");

		}

	}
	
	/**
	* @TestCase  : Verify Search Functionality
	* @Pre-condition : 1. Gmail app installed 2. Gmail account is added to app
	* @throws: Exception
	*/
	
	@Test(enabled = true, priority = 22)
	public void verifySearchFunctionality() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			log.info("*****************Started testcase ::: verifySearchFunctionality*****************************");
			SoftAssert softAssert = new SoftAssert();
			
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifySearchFunctionalityData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifySearchFunctionalityData.get("AccountOne"));
			
			softAssert.assertTrue(commonFunction.isElementByIdDisplayed(driver, verifySearchFunctionalityData.get("SearchIconID")));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementById(driver, verifySearchFunctionalityData.get("SearchMailID")), verifySearchFunctionalityData.get("SearchMailText"));
			try{
				driver.hideKeyboard();
				driver.getKeyboard();
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "Keyboard not opening for verifySearchFunctionality");
			}
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityData.get("SearchMailID"), verifySearchFunctionalityData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			commonFunction.navigateBackToMenuDrawer(driver);
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementByXpath(driver, verifySearchFunctionalityData.get("suggestionTextPath")), verifySearchFunctionalityData.get("suggestionSearchText"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifySearchFunctionality*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionality");
		}

		
	}
	@Test(enabled = true, priority = 23)
	public void verifyGmailMailAction() throws Exception {
		try {
			SoftAssert softAssert = new SoftAssert();
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> verifyGmailMailActionData = CommonPage
					.getTestData("verifyGmailMailAction");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyGmailMailActionData.get("AccountThree")))
			userAccount.switchMailAccount(driver, verifyGmailMailActionData.get("AccountThree"));
			
			String mailSubject=verifyGmailMailActionData.get("MailSubject");
			inbox.openMail(driver,mailSubject);
			inbox.verifyForUnreadMail(driver);
			inbox.openMail(driver,mailSubject);
			inbox.verifyMuteConversation(driver);
			inbox.openMail(driver,mailSubject);
			softAssert
			.assertTrue(inbox.verifyForStarredMail(driver,
					"verifyGmailMailActionStar"),
					"Image comparision failed for verifyGmailMailAction");
			
			inbox.verifyChangeLabelForMail(driver);
			inbox.openMail(driver,
					verifyGmailMailActionData.get("MailSubject"));
			inbox.verifyMovingMailtoOtherFolder(driver);
			inbox.openMail(driver,mailSubject);
			inbox.verifyImportantAndUnImportantMail(driver);
		//	inbox.openMail(driver,mailSubject);
			inbox.verifyPrintingOfMail(driver);
			inbox.verifyReportSpamMail(driver);
			inbox.verifyEmptySpam(driver);
			softAssert.assertAll();
			commonFunction.navigateBack(driver);
			} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyGmailMailAction");
		}
	}
	
	@Test(enabled = true, priority = 24)
	 public void verifySettingsOption() throws Exception {
	  driver = map.get(Thread.currentThread().getId());
	  try {
	   log.info("*****************Started testcase ::: verifySettingsOption******************************");
	   inbox.verifyGmailAppInitState(driver);
	   Map<String, String> verifySettingsOptionData = commonPage
	     .getTestData("verifySettingsOption");
	   HashSet<String> settingOptions = userAccount.getListOfAddedAccounts(driver);
	   settingOptions.add(verifySettingsOptionData.get("Option1"));
	   settingOptions.add(verifySettingsOptionData.get("Option2"));
	   userAccount.navigateToSettings(driver);
	   userAccount.verifySettingsOptions(driver,settingOptions);
	   commonFunction.navigateBack(driver);
	   commonFunction.navigateBack(driver);
	   commonFunction.navigateBack(driver);
	   log.info("*****************Completed testcase ::: verifySettingsOption******************************");
	  } catch (Throwable e) {
	   commonPage.catchBlock(e, driver, "verifySettingsOption");
	  }
	 }
	
	/**
	* @TestCase  : Offline search is able to display cached search results for Gmail and IMAP accounts and able to perform actions
	* @Pre-condition : While online perform search operations for some search term for few minutes (GMail as well as IMAP Accouts)
	* @throws: Exception
	*/
	@Test
	public void verifySearchFunctionalityOffline() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			log.info("*****************Started testcase ::: verifySearchFunctionalityOffline*****************************");
			SoftAssert softAssert = new SoftAssert();
			
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifySearchFunctionalityOfflineData = commonPage.getTestData("verifySearchFunctionalityOffline");
			
			commonFunction.switchOnAirPlaneMode();
			
			//Offline search for G-mail account
			if(!CommonFunctions.isAccountAlreadySelected(verifySearchFunctionalityOfflineData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityOfflineData.get("SearchIconID"));
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityOfflineData.get("SearchMailID"), verifySearchFunctionalityOfflineData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			commonFunction.navigateBackToMenuDrawer(driver);

			//Offline search for IMAP account
			if(!CommonFunctions.isAccountAlreadySelected(verifySearchFunctionalityOfflineData.get("IMAPAccountID")))
			userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("IMAPAccountID"));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityOfflineData.get("SearchIconID"));
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityOfflineData.get("SearchMailID"), verifySearchFunctionalityOfflineData.get("SearchKey2"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			commonFunction.navigateBackToMenuDrawer(driver);
			if(!CommonFunctions.isAccountAlreadySelected(verifySearchFunctionalityOfflineData.get("AccountOne")))
				userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));
			
			//Perform archive operation
			userAccount.setGmailDefaultActionToArchive(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,verifySearchFunctionalityOfflineData.get("ArchiveMailSubject"));
			
			//Perform delete operation
			userAccount.setGmailDefaultActionToDelete(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,verifySearchFunctionalityOfflineData.get("DeleteMailSubject"));
			
			//Move mail to test label
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			navDrawer.moveToLabel(driver, verifySearchFunctionalityOfflineData.get("TestLabelPath"));
			
			commonFunction.switchOffAirPlaneMode();
			
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("ArchiveMailSubject"));
			
			navDrawer.navigateToBin(driver, verifySearchFunctionalityOfflineData.get("BinOption"));
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("DeleteMailSubject"));
			
			navDrawer.navigateToLabel(driver, verifySearchFunctionalityOfflineData.get("TestLabelPath"));
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifySearchFunctionalityOffline*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionalityOffline");
		}

	}

}
