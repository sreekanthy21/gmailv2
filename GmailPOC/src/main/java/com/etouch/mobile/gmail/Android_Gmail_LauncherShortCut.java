package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_LauncherShortCut extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void ExecuteAllLauncherShortCut() throws Exception{
		verifyLauncherShortcut();
		verifyCreateLauncherShortcut();
	}*/
	
	/**Testcase to verify Launcher short cut Gmail app for compose
	 * 
	 * @preCondition Device should be Android N
	 * @throws Exception
	 * GMAT 94
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 130)
	public void verifyLauncherShortcut()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> launcherShortcut = commonPage.getTestData("LauncherShortCut");

		try{
			log.info("*****************Started testcase ::: verifyLauncherShortcut*****************************");
			driver.closeApp();
			driver.navigate().back();
			navDrawer.navigateToApps(driver);
			navDrawer.navigateToComposeShortCut(driver);
			inbox.tapOnComposeShortCut(driver);

			driver.closeApp();
			log.info("App closed");
			navDrawer.navigateToComposeShortCut(driver);
			navDrawer.createShortCutCompose(driver);
			inbox.openComposeShortCut(driver, launcherShortcut.get("From"));
			//driver.navigate().back();
			navDrawer.navigateBackTo(driver);
			log.info("*****************Completed testcase ::: verifyLauncherShortcut*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyLauncherShortcut");
		}
	}
	/**
	* @TestCase 95: Verify Create and Launch account shortcut
	* @Pre-condition : - Add few accounts (@gmail,@google,IMAP,POP,Exchange,Dasher)
                       - Launcher Shortcuts works for N (7.0.1 and above)

	*  @author batchi
	*  GMAT 95
	* @throws: Exception
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 131)
	public void verifyCreateLauncherShortcut()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> verifyLauncherShortcutData = commonPage
					.getTestData("verifyLauncherShortcut");
			Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");
			inbox.verifyGmailAppInitState(driver);
			log.info("*****************Started testcase ::: verifyCreateLauncherShortcut*****************************");
			userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
			//verify 1 static and 3 dynamic account shortcuts are displayed
			//navDrawer.gettingListFromMenuDrawer(driver,verifyLauncherShortcutData.get("ComposeElement"));
			if(!CommonFunctions.isAccountAlreadySelected(gmailifyFlowData.get("ImapEmail")))
			userAccount.switchMailAccount(driver, gmailifyFlowData.get("ImapEmail"));
			if(!CommonFunctions.isAccountAlreadySelected(verifyLauncherShortcutData.get("Account1")))
			userAccount.switchMailAccount(driver, verifyLauncherShortcutData.get("Account1"));
			if(!CommonFunctions.isAccountAlreadySelected( verifyLauncherShortcutData.get("Account2")))
			userAccount.switchMailAccount(driver, verifyLauncherShortcutData.get("Account2"));
			driver.closeApp();
			navDrawer.navigateToApps(driver);
			navDrawer.navigateToComposeShortCut(driver);
			navDrawer.verifyElementsOnLongPressOfGmail(driver, verifyLauncherShortcutData.get("ComposeElement"));
			
			//verify Shortcuts can be dragged out of app popup panel to Homescreen
			driver.closeApp();
			navDrawer.navigateToComposeShortCut(driver);
		    navDrawer.createShortCutComposeMail(driver);
			
			navDrawer.verifyAfterDataIsCleared(driver);
		
			log.info("*****************Completed testcase ::: verifyCreateLauncherShortcut*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyCreateLauncherShortcut");
		}
	}
	/**
	* @TestCase 94: Verify Delete Launch account shortcut
	* @Pre-condition : - Add few accounts (@gmail,@google,IMAP,POP,Exchange,Dasher)
	                   - Launcher Shortcuts works for N (7.0.1 and above)
	*  @author batchi
	* @throws: Exception
	*/
	@Test(enabled = true, priority = 132)
		public void verifyDeleteLuncherShortcut()throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
				Map<String, String> verifyDeleteLuncherShortcutData = commonPage
						.getTestData("verifyLauncherShortcut");
				log.info("*****************Started testcase ::: verifyDeleteLuncherShortcut*****************************");
				//verify 1 static and 3 dynamic account shortcuts are displayed
				//navDrawer.gettingListFromMenuDrawer(driver,verifyLauncherShortcutData.get("ComposeElement"));
				driver.closeApp();
				//navDrawer.navigateToApps(driver);
				navDrawer.navigateToComposeShortCut(driver);
				navDrawer.createShortCutComposeMail(driver);
				navDrawer.launchSettingsAndRemoveAccounts(driver,
						verifyDeleteLuncherShortcutData.get("AccountTypeGoogle"),
						verifyDeleteLuncherShortcutData.get("Account2"),
						verifyDeleteLuncherShortcutData.get("SearchSettingsApp"),
						verifyDeleteLuncherShortcutData.get("SearchAccountsFeature"));
				commonPage.verifyDeleteShortcutBeforeDisabled(driver, verifyDeleteLuncherShortcutData.get("Account2"));
				commonPage.verifyDeleteShortcutAfterDisabled(driver, verifyDeleteLuncherShortcutData.get("Account2"));
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyDeleteLuncherShortcut");
			}
		}		
	/**
	* @TestCase 94: Verify re-enabled deleted shortcut
	* @Pre-condition : - Add few accounts (@gmail,@google,IMAP,POP,Exchange,Dasher)
	                   - Launcher Shortcuts works for N (7.0.1 and above)
	*  @author batchi
	* @throws: Exception
	*/
	@Test(enabled = true, priority = 133)
		public void verifyReenabledShortcut()throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try{
				Map<String, String> verifyDeleteLuncherShortcutData = commonPage
						.getTestData("verifyLauncherShortcut");
				log.info("*****************Started testcase ::: verifyReenabledShortcut*****************************");
				//verify 1 static and 3 dynamic account shortcuts are displayed
				//navDrawer.gettingListFromMenuDrawer(driver,verifyLauncherShortcutData.get("ComposeElement"));
			/*	driver.closeApp();
				//navDrawer.navigateToApps(driver);
				navDrawer.navigateToComposeShortCut(driver);
				navDrawer.createShortCutComposeMail(driver);
				navDrawer.launchSettingsAndRemoveAccounts(driver,
						verifyDeleteLuncherShortcutData.get("AccountTypeGoogle"),
						verifyDeleteLuncherShortcutData.get("Account2"),
						verifyDeleteLuncherShortcutData.get("SearchSettingsApp"),
						verifyDeleteLuncherShortcutData.get("SearchAccountsFeature"));
				commonPage.verifyDeleteShortcutBeforeDisabled(driver, verifyDeleteLuncherShortcutData.get("Account2"));
				commonPage.verifyDeleteShortcutAfterDisabled(driver, verifyDeleteLuncherShortcutData.get("Account2"));
			 */ navDrawer.launchSettingsAndAddAccount(driver, 
					 verifyDeleteLuncherShortcutData.get("Account2"), 
					 verifyDeleteLuncherShortcutData.get("PwdField"), 
					 verifyDeleteLuncherShortcutData.get("SearchSettingsApp"),
					 verifyDeleteLuncherShortcutData.get("SearchAccountsFeature"));
			 commonPage.verifyReenabledShortcutIcon(driver,verifyDeleteLuncherShortcutData.get("Account2") );

			 log.info("*****************Ended testcase ::: verifyReenabledShortcut*****************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyReenabledShortcut");
		}
	}	
	
	/**
	*  @author Venkat
	**/
	@Test(enabled = true, priority = 134)
	public void verifyIMAPLauncherShortcut()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		
		Map<String, String> launcherShortcutData = CommonPage.getTestData("verifyLauncherShortcut");
		
		String gigAcc = launcherShortcutData.get("Account1");
		String outlookAcc = launcherShortcutData.get("IMAPAccount");
		
		String gigAccXpaht = launcherShortcutData.get("GIGAccountXPath");
		String outlookAccXpath = launcherShortcutData.get("IMAPAccountXPath");
		if(!CommonFunctions.isAccountAlreadySelected(gigAcc))
		userAccount.switchMailAccount(driver, gigAcc);
		if(!CommonFunctions.isAccountAlreadySelected(outlookAcc))
		userAccount.switchMailAccount(driver, outlookAcc);
		Thread.sleep(4000);
		driver.closeApp();

		for(int i=1; i<=4; i++) {
			String xPath = null;
			if(i == 2) {
				xPath = gigAccXpaht;
			}else if(i == 4) {
				xPath = outlookAccXpath;
			}
			navDrawer.pressAndHoldForShortcut(driver,xPath);
			Thread.sleep(1000);
		}
		
		for(int i=1; i<=4; i++) {
			String xPath = "", accName = "";
			if(i%2 == 0) {
				xPath = gigAccXpaht;
				accName = gigAcc;
			}else {
				xPath = outlookAccXpath;
				accName = outlookAcc;
			}
			CommonFunctions.searchAndClickByXpath(driver, xPath);
			Thread.sleep(5000);
			String currentAccName = userAccount.getSelectedAccountName(driver);
			if(!currentAccName.equalsIgnoreCase(accName)) {
				throw new Exception("Account:"+accName+" not matched with Selected account :"+currentAccName);	
			}
			log.info(accName+" account has been verified successfully...");
			driver.closeApp();
		}
	}
	
}
