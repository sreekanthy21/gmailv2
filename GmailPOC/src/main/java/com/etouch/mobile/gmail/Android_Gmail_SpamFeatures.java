package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_SpamFeatures extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void ExecuteAllSpamFeatures() throws Exception{
		removeUnsyncableLablesSpam();
		verifyBlockAndUnblockSenderWorks();
	}*/
	/**
	 * @TestCase 16 verifyBlockAndUnblockSenderWorks**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 */	
	@Test(groups = {"P1"}, enabled = true, priority = 40)
	   public void verifyBlockAndUnblockSenderWorks() throws Exception {
			
			driver = map.get(Thread.currentThread().getId());
			try {
				 log.info("*****************Started testcase ::: verifyBlockAndUnblockSenderWorks*************************");
				/* Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
			     Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		         Map<String, String>VerifyBlockAndUnblockSenderWorks = commonPage.getTestData("VerifyBlockAndUnblockSenderWorks");
				*/
				 inbox.verifyGmailAppInitState(driver);
				 inbox.verifyBlockAndUnblockSenderWorks(driver);
			
				 log.info("*****************Completed testcase ::: verifyBlockAndUnblockSenderWorks*****************************");
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifyBlockAndUnblockSenderWorks");	
			}
}
	
	/**
	 * @TestCase   :REMOVE_UNSYNCABLE_LABELS SPAM
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
.    *Author Rahul kulkarni
	 * @throws: Exception
	 * GMAT 83
	 */	
   @Test(groups = {"P1"}, enabled = true, priority = 41)
   public void removeUnsyncableLablesSpam() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
				log.info("*****************Started testcase ::: remove_Unsyncable_Lables_Spam*************************");
				inbox.verifyGmailAppInitState(driver);
				inbox.removeUnsyncableLablesSpam(driver);
			    log.info("*****************Completed testcase ::: remove_Unsyncable_Lables_Spam*****************************");
			} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "removeUnsyncableLablesSpam");
			}
	}
   
   /**
	 * @TestCase 5 :verifyMailWithUnauthenticatedAvatarAndWarning

	 * @Pre-condition :verifyMailWithUnauthenticatedAvatarAndWarning

	 *  some time period

	 * @throws: Exception
	 */
	
	@Test(enabled = true, priority = 106)
	public void verifyMailWithUnauthenticatedAvatarAndWarning() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try{
			Process p;
			 String[] addAccountOne = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "eaitest101@gmail.com", "-e", "password", "eaitest123", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountOne).start();
			    log.info("Installing account eaitest101 with google account manager apk");
			    Thread.sleep(10000);
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyMailWithUnauthenticatedAvatarAndWarning******************");
			
			inbox.verifyMailWithUnauthenticatedAvatarAndWarning(driver);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyMailWithUnauthenticatedAvatarAndWarning************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "forwardAnHtmlEmail");
		}
	}

	/**
	* @TestCase GMAT 82: Verify Report Spam with mute option
	* @Pre-condition : 
    *  @author batchi
	*  Comment: Unable to verify the Mute option when the open mail using search method is used. Instead, I have used open mail method 
	* @throws: Exception
	*/
	@Test(enabled = true, priority = 43)
	public void verifyReportSpamMuteOption()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> verifyReportSpamMuteOptionData = CommonPage.getTestData("verifyReportSpamAndMuteOption");
			log.info("*****************Started testcase ::: verifyReportSpamMuteOption*****************************");
			inbox.verifyGmailAppInitState(driver);
			
			String emailId = CommonFunctions.getNewUserAccount(verifyReportSpamMuteOptionData, "SwitchAccount3", null);

		    //userAccount.switchMailAccount(driver, verifyReportSpamMuteOptionData.get("SwitchAccount3"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
		    // driver = map.get(Thread.currentThread().getId());
			
			String mailSubject = verifyReportSpamMuteOptionData.get("MailSubject");
			String newSubject = mailSubject+","+mailSubject+","+mailSubject+","+mailSubject;
			CommonFunctions.smtpMailGeneration(emailId,newSubject, "Test1", null);
			UserAccount.pullToReferesh(driver);
			Thread.sleep(2000);
/*			
		    for(int i=0;i<=3;i++){
		    
		    String path = System.getProperty("user.dir");
	        String command = path + java.io.File.separator + "Mail" + java.io.File.separator + "verifyReportSpamAndMute.bat";
	        log.info("Executing the bat file " + command);
	        Process p = new ProcessBuilder(command).start();
	        log.info("Executed the bat file to generate a mail");
	        Thread.sleep(3000);
	        userAccount.pullToReferesh(driver);
	        
	      }
*/		    
	      Thread.sleep(1000);
		//commonPage.verifyMuteOption(driver,verifyReportSpamMuteOptionData.get("MailSubject") );
	       CommonPage.verifyMuteOption(driver,mailSubject);
			driver.navigate().back();
			driver.navigate().back();
			
			log.info("*****************Completed testcase ::: verifyReportSpamMuteOption*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyReportSpamMuteOption");
		}
	}
   
	   /**
		 * @TestCase 5 :verifyTheLabelListForGmailifiedAccount
		 * @Pre-condition :Enable Vacation responder from Settings -> Account
		 *  ""A"" -> Vacation Responder with some message and subject for 
		 *  some time period

		 * @throws: Exception
		 */
		
		@Test(enabled = true, priority = 1)
		public void verifyTheLabelListForGmailifiedAccount() throws Exception {
			
			
			driver = map.get(Thread.currentThread().getId());
			try{
				inbox.verifyGmailAppInitState(driver);
				log.info("******************Started testcase ::: verifyTheLabelListForGmailifiedAccount******************");
				Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
				userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
				
				userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
						tryGmailifyFlowData.get("IMAPEmailPwd"));
				userAccount.tryGmailifySetUpIMAP(driver);
				//userAccount.verifyExchangeAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
				userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));
				inbox.verifyTheLabelListForGmailifiedAccount(driver);
				log.info("*****************Completed testcase ::: verifyTheLabelListForGmailifiedAccount************");
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyTheLabelListForGmailifiedAccount");
			}
		}

}
