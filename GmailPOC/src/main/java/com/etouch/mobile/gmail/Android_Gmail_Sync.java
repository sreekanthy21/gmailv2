package com.etouch.mobile.gmail;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Sync extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	Process p;
	String[] adbroot;
	String[] adbAirplaneModeOnOff;
	String[] adbBroadcastIntent;
	
	
	/**-- Ensure that syncing all changes functions properly without errors 
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 54)
	public void verifySyncingWithoutErrors() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
		Map<String, String> verifyAutoSync = commonPage
					.getTestData("AutoSync");

		String[] remSapiAcct = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig4.testing@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", 
				"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		
	    p = new ProcessBuilder(remSapiAcct).start();
	    log.info("Installing account gm.gig4.testing google account manager apk");
		try {
			log.info("********Started testcase ::: verifySyncingWithoutErrors*******************************");
			
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToManageAccounts(driver);
			
			userAccount.verifyTurningOffOnSync(driver, false);
			driver.navigate().back();
			String[] addSapiAcct = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig4.testing@gmail.com", "-e", "password", "mobiletesting", "-e", "action", "add", "-e", "sync", "true",
		    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(addSapiAcct).start();
		    log.info("Installing account gm.gig4.testing google account manager apk");
		    Thread.sleep(10000);
    		
		    if(!CommonFunctions.isAccountAlreadySelected(verifyAutoSync.get("SyncAccountSapi")))
			userAccount.switchMailAccount(driver, verifyAutoSync.get("SyncAccountSapi"));
		    
			userAccount.verifyAutoSyncMsg(driver);
			userAccount.verifyTurningOffOnSync(driver, true);
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("SyncNow"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("SyncNow"));
					
			}
//			userAccount.pullToReferesh(driver);
			userAccount.launchGmailApp(driver);
			userAccount.verifyThreadListLoaded(driver);
						
			log.info("*****************Completed testcase ::: verifySyncingWithoutErrors*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySyncingWithoutErrors");
		}
	}
	
	/**-- Able to create a draft from reply to message in inbox.. 
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 55)
	public void verifyAbleToCreateDraftFromReply() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyAbleToCreateDraftFromReply*******************************");
			String replyText = "ReplyAndDraftTest";
			inbox.verifyGmailAppInitState(driver);
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);
			CommonFunctions.smtpMailGeneration(emailId,replyText, "ArchiveMail.html", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
			
			String mailSubjectLocator = verifyAutoSync.get("MailSubjectLocator");
			
			mailSubjectLocator = mailSubjectLocator.replaceAll("SubjectHere", replyText);
			String attachmentTile = verifyAutoSync.get("AttachmentTile");
			String discardDrafts = verifyAutoSync.get("DiscardDrafts");
			
			Thread.sleep(3000);
			CommonPage.pullToReferesh(driver);
			CommonPage.pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, mailSubjectLocator);
			inbox.replyMessage(driver, replyText);
			userAccount.openMenuDrawerNew(driver);
			CommonFunctions.scrollsToTitle(driver, "Drafts");
			CommonFunctions.searchAndClickByXpath(driver, mailSubjectLocator);
			mailSubjectLocator = mailSubjectLocator.replaceAll("content-desc", "text");
			String bodyText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, mailSubjectLocator);
			if(!replyText.equalsIgnoreCase(bodyText)) {
				throw new Exception("Body text not matched!.");
			}
			CommonFunctions.searchAndClickByXpath(driver, attachmentTile);
			driver.navigate().back();
			CommonFunctions.searchAndClickByXpath(driver, discardDrafts);
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyAbleToCreateDraftFromReply*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAbleToCreateDraftFromReply");
		}
	}

	/**-- With Auto-Sync off, verify that tips are inserted in expected locations
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 56)
	public void verifyAutoSyncOffTipsInserted() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyAutoSyncOffTipsInserted*******************************");
			inbox.verifyGmailAppInitState(driver);
			
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);

			
			//navDrawer.EnablePrimaryInbox(driver, verifyAutoSync.get("eTouchAccountOne"));
			//userAccount.switchMailAccount(driver, verifyAutoSync.get("eTouchAccountOne")); // Added/Shifted by Venkat on 28/11/2018 to here from bottom 111111

			navDrawer.EnablePrimaryInbox(driver, emailId);
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId); // Added/Shifted by Venkat on 28/11/2018 to here from bottom 111111

			navDrawer.navigateToManageAccounts(driver);
			userAccount.verifyTurningOffOnSync(driver, false);

			driver.navigate().back();
			log.info("App is being restated. So please wait....");
			UserAccount.launchGmailApp(driver); //added by Bindu on 11/12
			
			//userAccount.switchMailAccount(driver, verifyAutoSync.get("eTouchAccountOne"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
			
			inbox.verifyAutoSyncTips(driver);
			//userAccount.openUserAccountSetting(driver, verifyAutoSync.get("eTouchAccountOne"));
			userAccount.openUserAccountSetting(driver, emailId);
			
			CommonFunctions.scrollToCellByTitleVerify(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			
			CommonFunctions.scrollsToTitle(driver, "Manage labels");
			CommonFunctions.scrollsToTitle(driver, "Primary");
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();

			log.info("*****************Completed testcase ::: verifyAutoSyncOffTipsInserted*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAutoSyncOffTipsInserted");
		}
	}

	/**-- Turning on Auto-Sync from Conversation List message
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 57)
	public void verifyAutoSyncConversationList() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: AGS.sverifyAutoSyncConversationList*******************************");
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToManageAccounts(driver);
			userAccount.verifyTurningOffOnSync(driver, false);
			log.info("Before going to scroll up..");
			// added by Venkat on 22-Nov-2018 : start
			int cnt = 0;
			while(cnt++ <= 2){
				//if(CommonFunctions.getSizeOfElements(driver, "//android.support.design.chip.Chip[@resource-id='com.google.android.gm:id/my_account'] | //android.support.design.chip.Chip[contains(@text,'Manage your Google Account')]") <= 0){
				if(CommonFunctions.getSizeOfElements(driver, "//android.support.design.chip.Chip[@resource-id='com.google.android.gm:id/my_account'] | //android.support.design.chip.Chip[contains(@text,'Manage your Google Account')] |android.widget.TextView[@resource-id='com.google.android.gm:id/account_display_name'] | //android.widget.ImageView[@resource-id='com.google.android.gm:id/avatar']") <= 0){
					CommonPage.scrollUp(1);
					continue;
				}
				break;
			}
			Thread.sleep(1000);
			// added by Venkat on 22-Nov-2018: end
			log.info("After finding the selected account..");
			
			driver.navigate().back();
			
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);

			
			//userAccount.switchMailAccount(driver, verifyAutoSync.get("eTouchAccountOne"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
			userAccount.verifyAutoSyncMsg(driver);
			userAccount.tapOnTurnOffSync(driver);
			inbox.pullToReferesh(driver);
			
			/*navDrawer.navigateToManageAccounts(driver);
			userAccount.verifyTurningOffOnSync(driver, false);
			driver.navigate().back();
			Thread.sleep(3000);
			userAccount.pullToReferesh(driver);
			userAccount.verifyAutoSyncMsg(driver);
			userAccount.swipeMailToDeleteOrArchive(driver, verifyAutoSync.get("AutoSyncText"));
			CommonFunctions.isElementByXpathNotDisplayed(driver, verifyAutoSync.get("AutoSyncMsg"));
			*/
			log.info("*****************Completed testcase ::: verifyAutoSyncConversationList*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAutoSyncConversationList");
		}
	}

	/**-- Turning on Auto-Sync from Account Settings
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 58)
	public void verifyAutoSyncAccountSettings() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyAutoSyncAccountSettings*******************************");
			
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToManageAccounts(driver);
			userAccount.verifyTurningOffOnSync(driver, false);
			driver.navigate().back();
			
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);

			//userAccount.openUserAccountSetting(driver, verifyAutoSync.get("eTouchAccountOne"));
			userAccount.openUserAccountSetting(driver, emailId);
			
			CommonFunctions.scrollToCellByTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			CommonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncAlert"));
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("Cancel"));
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("Phone"));
			log.info("Detected device type as Phone");
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("TurnOn"));
			CommonFunctions.isElementByXpathNotDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			driver.navigate().back();
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyAutoSyncAccountSettings*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAutoSyncAccountSettings");
		}
	}

	/**--Turn on Auto-sync dialog should detect device type
	 * 
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 59)
	public void verifyAutoSyncDetectDeviceType() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		 try {
				log.info("********Started testcase ::: verifyAutoSyncDetectDeviceType*******************************");
				
				inbox.verifyGmailAppInitState(driver);
				navDrawer.navigateToManageAccounts(driver);
				userAccount.verifyTurningOffOnSync(driver, false);
				driver.navigate().back();
				Thread.sleep(1000);
				userAccount.verifyAutoSyncMsg(driver);
				CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("Phone"));
				log.info("Detected device type as Phone");
				driver.navigate().back();
				log.info("*****************Completed testcase ::: verifyAutoSyncDetectDeviceType*************************");
		 }catch(Throwable e){
			 commonPage.catchBlock(e, driver, "verifyAutoSyncDetectDeviceType");
		 }
				
	}


	/**-- Turning on Auto-Sync from Label Settings
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 60)
	public void verifyAutoSyncLabelSettings() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyAutoSyncLabelSettings*******************************");
			
			inbox.verifyGmailAppInitState(driver);
			navDrawer.navigateToManageAccounts(driver);
			userAccount.verifyTurningOffOnSync(driver, false);
			driver.navigate().back();
			
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);

			//userAccount.openUserAccountSetting(driver, verifyAutoSync.get("eTouchAccountOne"));
				userAccount.openUserAccountSetting(driver, emailId);

			try {
				CommonFunctions.scrollsToTitle(driver, "Manage labels");
			} catch (Exception e) {
			}
			CommonFunctions.scrollsToTitle(driver, "Primary");
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			CommonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncAlert"));
			/*CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("Cancel"));
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			*/CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("TurnOn"));
			CommonFunctions.isElementByXpathNotDisplayed(driver, verifyAutoSync.get("AutoSyncMsgContains"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();

			log.info("*****************Completed testcase ::: verifyAutoSyncLabelSettings*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAutoSyncLabelSettings");
		}
	}
	
	
	/**-- Verify Unsent in Outbox error message
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 61)
	public void verifyUnsentOutboxErrorMessage() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map<String, String> verifyAutoSync = CommonPage.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyUnsentOutboxErrorMessage*******************************");
			
			String emailId = CommonFunctions.getNewUserAccount(verifyAutoSync, "eTouchAccountOne", null);

			
			inbox.verifyGmailAppInitState(driver);
			//userAccount.switchMailAccount(driver, verifyAutoSync.get("eTouchAccountOne"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);
			
			adbroot = new String[]{"adb", "root"};
		    p = new ProcessBuilder(adbroot).start();
		    log.info("rooting device");
		    Thread.sleep(2000);
		    
		    adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "1"};
		    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
		    log.info("Airplane mode settings to On");
		    Thread.sleep(2000);
		    
		    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
		    p = new ProcessBuilder(adbBroadcastIntent).start();
		    log.info("Airplane mode settings to On");
		    Thread.sleep(2000);
		    
    		inbox.composeAsNewMailWithOnlyTo(driver, verifyAutoSync.get("Gig2Auto"), verifyAutoSync.get("UnsentError"));
    		//inbox.tapOnSend(driver);
    		navDrawer.navigateToSentBox(driver);
    		Thread.sleep(1200);
    		CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("UnsentOutboxError"));
    		log.info("Unsent in outbox displayed");
    		CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("UnsentOutboxError"));
    		CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("OutBoxOption"));
    		log.info("Navigated to Outbox");
    		navDrawer.navigateToPrimary(driver);
    		Thread.sleep(1200);
    		userAccount.swipeText(driver, verifyAutoSync.get("UnsentOutbox"));
    		navDrawer.navigateToSentBox(driver);
    		Thread.sleep(1200);
    		CommonFunctions.isElementByXpathNotDisplayed(driver, verifyAutoSync.get("UnsentOutboxError"));
    		log.info("Tip error message not displayed");
    		inbox.composeAsNewMailWithOnlyTo(driver, verifyAutoSync.get("Gig1Auto"), verifyAutoSync.get("UnsentError"));
    		//inbox.tapOnSend(driver);
    		CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("UnsentOutboxError"));
    		log.info("Unsent in outbox displayed");
    		String tipText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifyAutoSync.get("TipText"));
    		if(tipText.contains("2")){
    			log.info("Tip displayed with unsent error message with 2 mails in Outbox");
    		}else{
    			throw new Exception("Tip message is not displayed with 2 mails in Outbox");
    		}
			
			log.info("*****************Completed testcase ::: verifyUnsentOutboxErrorMessage*************************");
			adbroot = new String[]{"adb", "root"};
		    p = new ProcessBuilder(adbroot).start();
		    log.info("rooting device");
		    Thread.sleep(2000);
		    
		    adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "0"};
		    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
		    log.info("Airplane mode settings to Off");
		    Thread.sleep(2000);
		    
		    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
		    p = new ProcessBuilder(adbBroadcastIntent).start();
		    log.info("Airplane mode broadcast to Off");
		    Thread.sleep(2000);
		    
		} catch (Throwable e) {
			 adbAirplaneModeOnOff = new String[]{"adb", "shell", "settings", "put", "global", "airplane_mode_on", "0"};
			    p = new ProcessBuilder(adbAirplaneModeOnOff).start();
			    log.info("Airplane mode settings to Off");
			    Thread.sleep(2000);
			    
			    adbBroadcastIntent = new String[]{"adb", "shell", "am", "broadcast", "-a", "android.intent.action.AIRPLANE_MODE"};
			    p = new ProcessBuilder(adbBroadcastIntent).start();
			    log.info("Airplane mode broadcast to Off");
			    Thread.sleep(2000);
			commonPage.catchBlock(e, driver, "verifyUnsentOutboxErrorMessage");
		}
	}

	/**-- Verify Labels sync for IMAP account
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 61)
	public void verifyIMAPLabelSync() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
    	version = s.getVersion();
    	Map < String, String > verifyIMAPLAbelsData = commonPage.getTestData("IMAPAccountDetails");
    	try {
			log.info("********Started testcase ::: Verify Labels sync for IMAP*******************************");
			//userAccount.switchMailAccount(driver, verifyIMAPLAbelsData.get("OutlookEmailAddress"));
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyIMAPLAbelsData, "OutlookEmailAddress", "IMAP");

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			inbox.verifyStarredIMAPSync(driver);
			
			inbox.verifyUnreadIMAPSync(driver);
			inbox.verifyArchiveSysLabelIMAPSync(driver);
			
			
			
		
		} catch (Throwable e) {

			
		}
	}
}	
