package com.etouch.mobile.gmail;

import java.util.List;

import org.apache.commons.logging.Log;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import static com.etouch.mobile.gmail.TraveAndPurchases.*;

public class Android_Gmail_TravelAndPurchases extends BaseTest{
	AppiumDriver driver = null;
	private boolean isAccNowSwitched = false;
	static Log log = LogUtil.getLog(Android_Gmail_TravelAndPurchases.class);
	
	
	public void verifyTripLegsShouldHaveAllTheRelatedToTheTrip() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		openMailInLabel(driver, "gmobileqa19@gmail.com", TRAVEL, FLIGHT_RESERVATION);
		
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(CommonPage.getScreenshotAndCompareImage("verifyTripLegsShouldHaveAllTheRelatedToTheTrip"),
				"Image comparison for verifyTripLegsShouldHaveAllTheRelatedToTheTrip");
	}
	
	public void openMailInLabel(AppiumDriver driver, String emailId, String label, String mailSubject) throws Exception {
		
		if(!isAccNowSwitched && !CommonFunctions.isAccountAlreadySelected(emailId)) {
			isAccNowSwitched = true;
			userAccount.switchMailAccount(driver, emailId);
		}
		CommonFunctions.findMailInSpecificFolder(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW, mailSubject));

	}
	
	public void verifyChangeLabelShouldNotShowTravelAndPurchase(String label, String mailsubject) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		openMailInLabel(driver, "gmobileqa19@gmail.com", UPDATES, mailsubject);
		List<WebElement> elemsList = getLabelsList(driver, label);
		for(WebElement elem: elemsList) {
			String eachLabel = elem.getAttribute(TEXT).trim();
			log.info("Label is :"+eachLabel);
			Thread.sleep(600);
			if(eachLabel == null || eachLabel.equalsIgnoreCase(TRAVEL) || eachLabel.equalsIgnoreCase(PURCHASES)) {
				throw new Exception("Mail Should not have the "+label+". But it contains the Travel/Purchases.");
			}
		}
		
		log.info("******* Travel/Purchases not displayed. ******* in labe:"+label);
		
		Thread.sleep(4000);
		CommonFunctions.searchAndClickByXpath(driver, String.format(OK_OR_CANCEL,CANCEL));
		CommonFunctions.searchAndClickByXpath(driver, NAVIGATE_UP);
		

	}
	
	public void verifyChangeLabelForTravelAndPurchase(String label, String mailsubject) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		openMailInLabel(driver, "gmobileqa19@gmail.com", label, mailsubject);
		String appliedLabel = applyLabel(driver, label);
		
		String subjectAndLabelsText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, SUBJECT_FOLDER_VIEW);
		if(subjectAndLabelsText == null || !subjectAndLabelsText.contains(appliedLabel)) {
			throw new Exception("Mail Does not has the "+label+". But should contains the label.");
		}
		log.info("******* "+label+" Not displayed. ******* in attached labes:"+subjectAndLabelsText);
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, NAVIGATE_UP);
	}
	
	public String applyLabel(AppiumDriver driver, String label) throws Exception {
		String appliedLabel = "";
		List<WebElement> elemsList = getLabelsList(driver, CHANGE_LABELS);
		for(WebElement elem: elemsList) {
			if(Boolean.valueOf(elem.getAttribute(CHECKED))){
				continue;
			}
			appliedLabel = elem.getAttribute(TEXT);
			elem.click();
			CommonFunctions.searchAndClickByXpath(driver, String.format(OK_OR_CANCEL,OK));
			break;
		}
		return appliedLabel;
	}
	
	private static List<WebElement> getLabelsList(AppiumDriver driver, String labelType) throws Exception {
		
		CommonFunctions.searchAndClickByXpath(driver, MORE_OPTIONS);
		CommonFunctions.searchAndClickByXpath(driver, String.format(ANDROID_WIDGET_TEXTVIEW, labelType));
		List<WebElement> elemsList = driver.findElementsByXPath(LABELS_LOCATOR);
		return elemsList;
	}

	public void verifyViewEditTripsLinksShouldTakeToTripsWebview(String label, String mailSubject) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		openMailInLabel(driver, "gmobileqa19@gmail.com", label, mailSubject);
		
		CommonFunctions.clickOrScrollAndClick(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW,CHECK_IN));
		Thread.sleep(6000);
		driver.navigate().back();
		CommonFunctions.clickOrScrollAndClick(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW,VIEW_OR_EDIT_IN_TRIPS));
		Thread.sleep(6000);
		String subjectAndLabelsText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, ALL_TRIPS_LOCATOR);
		
		if(subjectAndLabelsText!= null && !subjectAndLabelsText.contains(ALL_TRIPS)) {
			throw new Exception("All trips Not found in View/edit in Trips Page.");
		}
		driver.navigate().back();
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, NAVIGATE_UP);

		CommonFunctions.clickOrScrollAndClick(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW,HOTELS_BOOKING_CONFIRMATION));
		Thread.sleep(2000);
		CommonFunctions.clickOrScrollAndClick(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW,MODIFY_RESERVATION));
		Thread.sleep(5000);
		driver.navigate().back();
		try {
			CommonFunctions.clickOrScrollAndClick(driver, label, String.format(ANDROID_WIDGET_TEXTVIEW,VIEW_OR_EDIT_IN_TRIPS));
			Thread.sleep(5000);
			subjectAndLabelsText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, ALL_TRIPS_LOCATOR);
			Thread.sleep(5000);
			if(subjectAndLabelsText!= null && !subjectAndLabelsText.contains(ALL_TRIPS)) {
				throw new Exception("All trips Not found in View/edit in Trips Page.");
			}
		} catch (Exception e) {
			log.error("&&&&&&& ------ View/edit in Trips not visible OR ALL_TRIPS Lable not visible. ------ &&&&&&&");
		}
		
	}
	
	public void verifyMailInRelatedLabel(String label, String mailSubject) throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		openMailInLabel(driver, "gmobileqa19@gmail.com", label, mailSubject);
		
		Thread.sleep(1000);
		CommonFunctions.searchAndClickByXpath(driver, NAVIGATE_UP);
		Thread.sleep(1000);
		CommonFunctions.findMailInSpecificFolder(driver, UPDATES, String.format(ANDROID_WIDGET_TEXTVIEW,mailSubject));
		
		String subjectAndLabelsText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, SUBJECT_FOLDER_VIEW);
		
		if(subjectAndLabelsText!= null && subjectAndLabelsText.contains(label)) {
			throw new Exception("Mail has the "+label+". But should not contains the label.");
		}
		
	}
}
