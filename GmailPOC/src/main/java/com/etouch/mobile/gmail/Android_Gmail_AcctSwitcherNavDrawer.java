package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_AcctSwitcherNavDrawer extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	static AppiumDriver driver = null;
	
/*	@Test
	public void ExecuteAllAccoutSwitcherNavigationDawer() throws Exception{
		verifyOpenAndCloseDrawer();
		verifyAccountSwitcherAndDrawer();
	}
*/	
	/**
	 * @TestCase : Open And Close Navigation Drawer
	 * @Pre-condition : 1. Gmail app installed
	                    2. Gmail account is added to app
	 	@author batchi

	 * @throws: Exception
	 * GMAT 40
	 */
	@Test(groups = {"P1"}, enabled=true, priority=21)
	public void verifyOpenAndCloseDrawer()throws Exception{
		
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyOpenAndCloseDrawer ******************************");
			Map<String, String> verifyOpenAndCloseDrawerData = CommonPage.getTestData("SystemLabels");
			//inbox.launchGmailApplication(driver);
			inbox.verifyGmailAppInitState(driver);
			commonPage.waitForHambergerMenuToBePresent();
			inbox.openMenuDrawer(driver);
			commonPage.swipeRightToLeft(driver,verifyOpenAndCloseDrawerData.get("LabelViewID"));
			inbox.openMenuDrawer(driver);
			log.info("As menu drawer is clickable, the swipe function worked to close the menu drawer");
			commonPage.hamburgerDrawerLableView(driver);
			commonPage.hamburgerDrawerAccountView(driver);
			inbox.openMenuDrawer(driver);
			//commonPage.verifyAccountView(driver);
			commonPage.swipeRightToLeft(driver,verifyOpenAndCloseDrawerData.get("LabelViewID"));
			
			log.info("***************** Completed testcase ::: verifyOpenAndCloseDrawer ******************************");
		
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyOpenAndCloseDrawer");
		}
	}
	/**Testcase to verify the account switcher and Drawer
	 *   
	 * @preCondition: Gmail app installed
 		Multiple Gmail accounts added to app
		Add 1 or more IMAP/POP3 accounts
		Add 1 Exchange account
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 41
	 */
	@Test(groups = {"P1"}, enabled=true, priority = 22)
	public void verifyAccountSwitcherAndDrawer()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			//inbox.launchGmailApplication(driver);
			log.info("******************Started testcase ::: verifyAccountSwitcherAndDrawer******************");
			Map<String, String> accountDrawer = CommonPage.getTestData("accountDrawer");
			Map<String, String> accountDrawerData = CommonPage.getTestData("getEmailAddress");
			
			String primayAccount = CommonFunctions.getNewUserAccount(accountDrawerData, "OtherEmailAddress1", null);

			log.info("**********Verifying the Account Switcher**********");
			//inbox.openMenuDrawer(driver);
			
			//userAccount.verifyAccountsExists(driver, getEmailAddressData.get("OtherEmailAddress1"), getEmailAddressData.get("OtherPassword1"));
			
			if(!CommonFunctions.isAccountAlreadySelected(primayAccount))
				userAccount.switchMailAccount(driver, primayAccount);
			//inbox.openMenuDrawer(driver);
			//userAccount.verifyAddAccountManageAccount(driver);
			
			log.info("**********Verifying the Account Drawer**********");
			inbox.openMenuDrawer(driver);
			navDrawer.navigateToSystemFolders(driver, accountDrawer.get("Social"));
			navDrawer.navigateToSystemFolders(driver, accountDrawer.get("Promotions"));
			navDrawer.navigateToSystemFolders(driver, accountDrawer.get("Primary"));
			navDrawer.navigateToSystemFolders(driver, accountDrawer.get("Starred"));
			userAccount.scrollDown(1); // added on 06/05/2019
			navDrawer.navigateToSystemFolders(driver, accountDrawer.get("Trash"));
			
			log.info("*****************Completed testcase ::: verifyAccountSwitcherAndDrawer************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyAccountSwitcherAndDrawer");
		}
	}
	
	/**
	 * @TestCase exploratory : verify Account switcher on boarding message
	 * @Pre-condition :
	 * GMAT 
	 * @throws: Exception
	 * @author batchi
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyAccountSwitchOnboardingMsg() throws Exception {
		log.info(" ************ Started TestCase: verifyAccountSwitchOnboardingMsg  ************");
		driver = map.get(Thread.currentThread().getId());
		log.info("Clearing app data");
		Process p1;
		String[] clear = new String[] {"adb" ,"shell", "pm", "clear", "com.google.android.gm"};
		p1 = new ProcessBuilder(clear).start();
		Thread.sleep(2000);
		driver.launchApp();	
		navDrawer.onboardingMsg(driver);
		log.info(" ************ Ended TestCase: verifyAccountSwitchOnboardingMsg  ************");
			
		}
	
}
