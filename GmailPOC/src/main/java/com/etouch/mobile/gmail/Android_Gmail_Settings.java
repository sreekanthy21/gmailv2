package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.InboxFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Settings extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	HtmlEmailSender mail = new HtmlEmailSender();
    String path = System.getProperty("user.dir");
    Process p;

	
	/**
	 * @TestCase 5 : Enable/Disable Vacation Responder
	 * @Pre-condition :Enable Vacation responder from Settings -> Account
	 *  ""A"" -> Vacation Responder with some message and subject for 
	 *  some time period

	 * @throws: Exception
	 *//*
	
	@Test(groups = {"P0"}, enabled = true, priority = 67)
	public void enableDisableVacationResponder() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			Map<String, String> enableDisableVacationResponder = commonPage
					.getTestData("enableDisableVacationResponder");

			log.info("*****************Started testcase ::: enableDisableVacation Responder***************************");
			inbox.verifyGmailAppInitState(driver);
		//	userAccount.verifyAccountsExists(driver, enableDisableVacationResponder.get("Accounts"), enableDisableVacationResponder.get("AccountsPwd"));
			userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("ToFieldVR"));

			inbox.enableDisableVacationResponder(driver);
			Thread.sleep(2500); 
			log.info("Composing the mail ");
			String fromUser=inbox.composeAndSendNewMail(driver,
		    		  enableDisableVacationResponder.get("ToFieldVR"),
		    		  enableDisableVacationResponder.get("CcFieldVR"), " ",
		    		  enableDisableVacationResponder.get("SubjectVR"),
		    		  enableDisableVacationResponder.get("ComposeBodyVR"));	
			log.info("Sent Mail from Account B to Account A");
			inbox.verifyEnableDisableVacationResponderMailIsPresent(driver);
			
			String fromUser1=inbox.composeAndSendNewMail(driver,
		    		  enableDisableVacationResponder.get("ToFieldVR"),
		    		  enableDisableVacationResponder.get("CcFieldVR"), " ",
		    		  enableDisableVacationResponder.get("SubjectVR"),
		    		  enableDisableVacationResponder.get("ComposeBodyVR"));
			log.info("Sent Mail from Account C to Account A");
			
			inbox.verifyEnableDisableVacationResponderMailIsNotPresent(driver);

			log.info("*****************Completed testcase ::: enableDisableVacation Responder*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "enableDisableVacationResponder");
		}
	}
*/
	
	/**
	 * @TestCase 5 : Enable/Disable Vacation Responder
	 * @Pre-condition :Enable Vacation responder from Settings -> Account
	 *  ""A"" -> Vacation Responder with some message and subject for 
	 *  some time period

	 * @throws: Exception
	 */
	
	@Test(groups = {"P0"}, enabled = true, priority = 102)
	public void enableDisableVacationResponder() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			Map<String, String> enableDisableVacationResponder = CommonPage.getTestData("enableDisableVacationResponder");

			log.info("*****************Started testcase ::: enableDisableVacation Responder***************************");
			inbox.verifyGmailAppInitState(driver);
		//	userAccount.verifyAccountsExists(driver, enableDisableVacationResponder.get("Accounts"), enableDisableVacationResponder.get("AccountsPwd"));
			
			//userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("ToFieldVR"));

			String  emailId = CommonFunctions.getNewUserAccount(enableDisableVacationResponder, "ToFieldVR", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			inbox.enableDisableVacationResponder(driver);
			log.info("Composing the mail ");
			//userAccount.composeAsNewMailWithOnlyTo(driver, enableDisableVacationResponder.get("ToFieldVR"), enableDisableVacationResponder.get("SubjectVR"));
			//userAccount.tapOnSend(driver);
			//mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "Testing Vacation Responder Email", " ");
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", emailId, "Testing Vacation Responder Email", " ");
			log.info("Sent Mail from Account B to Account A");
			inbox.verifyEnableDisableVacationResponderMailIsPresent(driver);
	       // userAccount.composeAsNewMailWithOnlyTo(driver, enableDisableVacationResponder.get("ToFieldVR"), enableDisableVacationResponder.get("SubjectVR"));
			//userAccount.tapOnSend(driver);
			//mail.sendHtmlEmail("gm.gig3.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "Testing Vacation Responder Email", " ");
			mail.sendHtmlEmail("gm.gig3.auto@gmail.com", "mobileautomation", emailId, "Testing Vacation Responder Email", " ");
			log.info("Sent Mail from Account C to Account A");
			inbox.verifyEnableDisableVacationResponderMailIsNotPresent(driver);
			log.info("*****************Completed testcase ::: enableDisableVacation Responder*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "enableDisableVacationResponder");
		}
	}
	
	/**Testcase to verify the signature set to On/Off and send emails.
	 *  
	 * @throws Exception
	 * @preCondition Atleast 3 accounts should exists
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 104)
	public void verifySignatureOnAndOff()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);

			log.info("*****************Started testcase ::: verifySignatureOnAndOff*****************************");
			Map<String, String> verifySignatureOnAndOffData = CommonPage.getTestData("verifySignatureOnAndOff");

			//Verify the accounts are exists in the app
		//	userAccount.verifyAccountsExists(driver, verifySignatureOnAndOffData.get("EmailAccounts"), verifySignatureOnAndOffData.get("EmailPassword"));
			//userAccount.switchMailAccount(driver, verifySignatureOnAndOffData.get("AccountA"));
			
			String  emailId = CommonFunctions.getNewUserAccount(verifySignatureOnAndOffData, "AccountA", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			//Set Signature On/Off for the accounts
			log.info("Setting signature for the accounts");
			userAccount.setSignatureOnOff(driver, verifySignatureOnAndOffData.get("SignatureTextA"), verifySignatureOnAndOffData.get("SignatureTextC"),
					verifySignatureOnAndOffData.get("GeneralSettings"), verifySignatureOnAndOffData.get("AddAccount"), 
					verifySignatureOnAndOffData.get("AccountB"), emailId, verifySignatureOnAndOffData.get("AccountC"));
			log.info("Verifying signature in compose");
			inbox.verifySignatureInCompose(driver, emailId, verifySignatureOnAndOffData.get("AccountB"), 
					verifySignatureOnAndOffData.get("AccountC"), verifySignatureOnAndOffData.get("SignatureTextA"), verifySignatureOnAndOffData.get("SignatureTextC"));
			log.info("Sending email and verifying");
			inbox.sendEmailAndVerify(driver, emailId, verifySignatureOnAndOffData.get("AccountC"), 
					verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			
			userAccount.verifyEmailNoSignature(driver, emailId, verifySignatureOnAndOffData.get("AccountC"), 
					verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			 inbox.sendEmailAndVerifyNoSignature(driver, emailId, verifySignatureOnAndOffData.get("AccountC"), 
					 verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			navDrawer.navigateUntilHamburgerIsPresent(driver);
			 log.info("*****************Completed testcase ::: verifySignatureOnAndOff*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySignatureOnAndOff");
		}
	}
	
	/**
	 * @TestCase 10 :navigateToSettings**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 */	 
	@Test(groups = {"P1"}, enabled = true, priority = 3)
   public void navigateToSettings() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			
			log.info("*****************Started testcase ::: navigateToSettings*************************");
			 inbox.verifyGmailAppInitState(driver);
			 inbox.navigateToSettings(driver);
	  	     inbox.navigateUntilHamburgerIsPresent(driver);
			 log.info("*****************Completed testcase ::: navigateToSettings*****************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "navigateToSettings");
		}
}
	
	/**
	 * @TestCase  57: Verify Mark As Important In CV
	* @Pre-condition : Mark  Important should be enabled in Settings of Inbox for the account from Web
	*  @author batchi
	* @throws: Exception
	* comment: Using etouchtestfive@gmail.com account as the precondition can't be automated.
	* GMAT 70
	*/
	@Test(groups = {"P1"}, enabled=true, priority=100)
	 public void verifyGeneralSettings() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try {
		          Map < String, String > gmailifyFlowData = CommonPage.getTestData("gmailifyFlow");
		       
		  		log.info("***************Started testcase :: Verify general app settings ***********");
		      /*
		          String command4 = path + java.io.File.separator + "Mail" + java.io.File.separator + "GeneralSettingsGig.bat";
		          p = new ProcessBuilder(command4).start();
		          log.info("Executed the bat file to generate a mail");
		          //Thread.sleep(60000);
		        */
		  		
		  		String  emailId = CommonFunctions.getNewUserAccount(gmailifyFlowData, "GmailAccount", null);

				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);
		  		
					CommonFunctions.smtpMailGeneration(emailId,"TestFour,TestThree,TestTwo,TestOne,Testing,Swipe Archive And Delete Functionality,TestLink", "Test", null);
		          inbox.pullToReferesh(driver);
		         // userAccount.switchMailAccount(driver, gmailifyFlowData.get("GmailAccount"));
		         // userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd")); // // Commented by Venkat on 23/11/2018
		          //driver.navigate().back();
		          userAccount.navigateToGeneralSettings(driver);

		          userAccount.verifyGeneralSettingsOptions(driver);
		          log.info("***************Completed testcase :: Verify general app settings***********");
		          driver.navigate().back();
		        //  driver.navigate().back();

			}catch (Throwable e) {
				  commonPage.catchBlock(e, driver, "verifyGeneralSettingsOptions");
		}
		}
	
	/**
	 * @TestCase 14 verifyGmailAccountSpecificSettings**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 */	 
	@Test(groups = {"P1"}, enabled = true, priority = 101)
	   public void verifyGmailAccountSpecificSettings() throws Exception {
			
			driver = map.get(Thread.currentThread().getId());
			try {
		          Map < String, String > acctSelect = CommonPage.getTestData("deleteMailCheckTrash");

					  		log.info("******************************Started testcase :: Gmail Account Settings****************** ");
					  			inbox.verifyGmailAppInitState(driver);
					          //driver.navigate().back();
					  			
					  			String  emailId = CommonFunctions.getNewUserAccount(acctSelect, "Account", null);

					  			if(!CommonFunctions.isAccountAlreadySelected(emailId))
					  				userAccount.switchMailAccount(driver,emailId);
					  			
						          //userAccount.switchMailAccount(driver, acctSelect.get("Account"));
					  			
					  			userAccount.navigateToSettings(driver);

					          userAccount.verifyAccountSettings(driver);
					          // userAccount.navigateToSettings(driver);
					          inbox.verifySoundVibrateImagesDaysofmaailtosyncDowmloadAttachments(driver);
					          log.info("***************Completed testcase :: Gmail Account Settings***********");
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifyGmailAccountSpecificSettings");
			}
}
	
	
	
	/**
	 * Enable/Disable Smart Reply
	 */	 
	@Test(groups = {"P1"}, enabled = true, priority = 26)
	   public void verifySmartReplyEnableDisable() throws Exception {
			
			driver = map.get(Thread.currentThread().getId());
			try {
		   		 Map <String, String> smartReplySuggestionsData = CommonPage.getTestData("smartReplySuggestions");
		   		 
		   		String emailId = CommonFunctions.getNewUserAccount(smartReplySuggestionsData, "Account2", null);
			    //userAccount.switchMailAccount(driver, smartReplySuggestionsData.get("Account2"));
		   		if(!CommonFunctions.isAccountAlreadySelected(emailId))
		   			userAccount.switchMailAccount(driver, emailId);
			    
			    inbox.openMail(driver, smartReplySuggestionsData.get("EmailSubject"));
			    CommonFunctions.scrollToCellByTitle(driver, "Forward");
			    Thread.sleep(1000);
			    if(CommonFunctions.getSizeOfElements(driver, smartReplySuggestionsData.get("SmartReplySuggestions"))!=0){
			    	log.info("Smart reply suggestions displayed");
			    }else{
			    	throw new Exception("Smart reply suggestions are not displayed for this mail, please check again");
			    }
			    driver.navigate().back();
			    //userAccount.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account2"));
			    userAccount.openUserAccountSetting(driver, emailId);
			    CommonFunctions.scrollsToTitle(driver, "Smart Reply");
			    driver.navigate().back();
			    driver.navigate().back();
			    inbox.openMail(driver, smartReplySuggestionsData.get("EmailSubject"));
			    Thread.sleep(1000);
			    CommonFunctions.scrollToCellByTitle(driver, "Forward");
			    Thread.sleep(1000);
			    if(CommonFunctions.getSizeOfElements(driver, smartReplySuggestionsData.get("SmartReplySuggestions"))!=0){
			    	throw new Exception("Smart reply suggestions are not displayed for this mail, please check again");
			    }else{
			    	log.info("Smart reply suggestions are not displayed");
			    }
			    driver.navigate().back();
			    //userAccount.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account2"));
			    userAccount.openUserAccountSetting(driver, emailId);
			    CommonFunctions.scrollsToTitle(driver, "Smart Reply");
			    driver.navigate().back();
			    driver.navigate().back();
			    
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifySmartReplyEnableDisable");
			}
}
	
		
	/**
	 * @TestCase 6 : enableVacationResponderWithSendToMyContactsOption
	 * @Pre-condition :Enable Vacation responder from Settings -> Account
	 *  ""A"" -> Vacation Responder with some message and subject for 
	 *  some time period

	 * @throws: Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 103)
	public void enableVacationResponderWithSendToMyContactsOption() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			Map<String, String> enableDisableVacationResponder = CommonPage.getTestData("enableDisableVacationResponder");

			log.info("*****************Started testcase ::: enableVacationResponderWithSendToMyContactsOption***************************");
			inbox.verifyGmailAppInitState(driver);
			//userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("ToFieldVR"));
			String  emailId = CommonFunctions.getNewUserAccount(enableDisableVacationResponder, "ToFieldVR", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			inbox.enableVacationResponderWithSendToMyContactsOption(driver);
			//userAccount.composeAsNewMailWithOnlyTo(driver, enableDisableVacationResponder.get("ToFieldVR"), enableDisableVacationResponder.get("SubjectVR"));
			//userAccount.tapOnSend(driver);
			//mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "Testing Vacation Responder Email", " ");
			mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", emailId, "Testing Vacation Responder Email", " ");
			
			log.info("Sent Mail from Account B which is in contacts of Account A");
			inbox.enableVacationResponderWithSendToMyContactsOptionMailIsPresent(driver);
			log.info("Composing the mail ");
			//userAccount.composeAsNewMailWithOnlyTo(driver, enableDisableVacationResponder.get("ToFieldVR"), enableDisableVacationResponder.get("SubjectVR"));
			//userAccount.tapOnSend(driver);
			//mail.sendHtmlEmail("gm.gig4.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "Testing Vacation Responder Email", " ");
			mail.sendHtmlEmail("gm.gig4.auto@gmail.com", "mobileautomation", emailId, "Testing Vacation Responder Email", " ");
			log.info("Sent Mail from Account gig4 to Account A");
			inbox.enableVacationResponderWithSendToMyContactsOptionMailIsNotPresent(driver);
			log.info("*****************Completed testcase ::: enableVacationResponderWithSendToMyContactsOption*************************");
			
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "enableDisableVacationResponder");
		}
	}
	

	/**
	* @TestCase: Swipe actions in General Settings
      @author Phaneendra
	* @throws: Exception
	*/
	@Test(groups = {"P0"}, priority=105)
	public void verifySwipeActions() throws Exception{
		
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifySwipeActions ******************************");
			
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> SwipeActions = CommonPage.getTestData("verifySwipeActions");
			
			String  emailId = CommonFunctions.getNewUserAccount(SwipeActions, "AccountOne", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			//userAccount.switchMailAccount(driver, SwipeActions.get("AccountOne"));
			userAccount.navigateToGeneralSettings(driver);
			Thread.sleep(1200);
			CommonFunctions.searchAndClickByXpath(driver, SwipeActions.get("SwipeActions"));
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, SwipeActions.get("SwipeChangeButton"));
			//Right Swipe
			CommonFunctions.searchAndClickByXpath(driver, SwipeActions.get("RightSwipe"));
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, SwipeActions.get("UseRightSwipeFor"));
			Thread.sleep(1000);
			userAccount.verifySwipeActionsPresent(driver, SwipeActions.get("Archive"),SwipeActions.get("Delete"),SwipeActions.get("MarkReadUnread"),
					SwipeActions.get("Move to"),SwipeActions.get("Snooze"),SwipeActions.get("None"));
			CommonFunctions.searchAndClickByXpath(driver, SwipeActions.get("Cancel"));
			Thread.sleep(800);
			//Left Swipe
			CommonFunctions.searchAndClickByXpath(driver, SwipeActions.get("LeftSwipe"));
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, SwipeActions.get("UseLeftSwipeFor"));
			Thread.sleep(1000);
			userAccount.verifySwipeActionsPresent(driver, SwipeActions.get("Archive"),SwipeActions.get("Delete"),SwipeActions.get("MarkReadUnread"),
					SwipeActions.get("Move to"),SwipeActions.get("Snooze"),SwipeActions.get("None"));
			CommonFunctions.searchAndClickByXpath(driver, SwipeActions.get("Cancel"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();

		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySwipeActions");
		}
	}
	@Test(groups = {"P0"}, priority=106)
	public void verifyLeftSwipeActions(String whichDirection) throws Exception {
		verifySwipeActions(whichDirection);
	}
	
	@Test(groups = {"P0"}, priority=107)
	public void verifyRightSwipeActions(String whichDirection) throws Exception {
		verifySwipeActions(whichDirection);
	}
	
	public void verifySwipeActions(String whichDirection) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> swipeActionsData = CommonPage.getTestData("verifySwipeActions");
		
		String emailId = CommonFunctions.getNewUserAccount(swipeActionsData, "AccountOne", null);
		String mailSubject = swipeActionsData.get("SwipeActionsMailSubject");
		
		if(!CommonFunctions.isAccountAlreadySelected(emailId))
			userAccount.switchMailAccount(driver, emailId);
		
		userAccount.goToSwipteActions(driver, whichDirection, swipeActionsData);
		Thread.sleep(600);
		List<WebElement> actionElemsList = driver.findElements(By.xpath(swipeActionsData.get("SwipeActionElements")));
		if(!actionElemsList.isEmpty()){
			//userAccount.mailSend("SwipeActions.bat");
			CommonFunctions.smtpMailGeneration(emailId,mailSubject, "None.html", null);
		}else{
			throw new Exception("Unable to click on Swipe Actions events..");
		}
		String failedActions = "", actionText = "";
		boolean isFirstTime = true, pullToRefresh = false;
		for(WebElement actionElement: actionElemsList){
			try {
				if(!isFirstTime){
					userAccount.goToSwipteActions(driver, whichDirection, swipeActionsData);
				}
				isFirstTime = false;
				actionText = actionElement.getText();
				String actionLableText = actionText+"ActionTest";
				System.out.println("actionLableText :"+actionLableText);
				if(!actionElement.isSelected()){
					actionElement.click();
				}
				userAccount.navigeteNTimes(driver, 3);
				if(!pullToRefresh){
					Thread.sleep(1000);
					CommonPage.pullToReferesh(driver);
					pullToRefresh = true;
				}
				String actionLableMailXpath = inbox.findMailInPrimaryFolder(driver, actionLableText);
				System.out.println("Mail is in Primay folder :"+actionLableMailXpath);
				Thread.sleep(1000);
				if(actionLableMailXpath == null){
					throw new Exception("Mail is not displayed due to sync issue or slow network connectivity for "+actionLableText);
				}
				userAccount.horizontalSwipe(driver, actionLableMailXpath, whichDirection);
				Thread.sleep(1000);
				
				String xPath = null; 
				if(actionText.contains("Move")){
					xPath = swipeActionsData.get("MoveXPath");
				} else if(actionText.contains("Snooze")){
					xPath = swipeActionsData.get("SnoozeXPath");
				}
				
				if(null != xPath){
					CommonFunctions.searchAndClickByXpath(driver, xPath);
				}
				
				Thread.sleep(1000);
				userAccount.findSwippedMail(driver, actionText, actionLableMailXpath, whichDirection);
				log.info(actionLableText+" is success.");
			} catch (Exception e) {
				failedActions = actionText+","+failedActions;
			}
		}
		if(!failedActions.isEmpty()){
			throw new Exception("Some swipe actions :"+failedActions+" are failed.");
		}
	}
	/**
	 * @TestCase  : Verify Reply/Reply All setting in settings
	* @Pre-condition :
	*  @author Santosh
	* @throws: Exception
	* comment:
	* GMAT
	*/
	@Test(groups = {"P1"}, enabled=true, priority=00)
	 public void verifyReplyAndReplyAllFromSettings() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			try {
		  		log.info("***************Started testcase :: Verify Reply/Reply All setting in settings ***********");
		      
		          inbox.verifyReplyAndReplyAllFromSettings(driver);

		          log.info("***************Completed testcase :: Verify Reply/Reply All setting in settings***********");
		          driver.navigate().back();
		        
			}catch (Throwable e) {
				  commonPage.catchBlock(e, driver, "verifyReplyAndReplyAllFromSettings");
		}
		}	


	public void verifyChangingFromCustomFieldAndMailFromNogGoogle() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> exchangeComposeMail = CommonPage.getTestData("ExchangeComposeMail");
		
		String mailSubject =  exchangeComposeMail.get("MailSubject");
		String toAccount =  exchangeComposeMail.get("ToAccount");
		String customFromAcc =  exchangeComposeMail.get("customFromAcc");
		String mailBody =  exchangeComposeMail.get("mailBody");
		String accountSpinner =  exchangeComposeMail.get("accountSpinner");
		String customFromAccXPath =  exchangeComposeMail.get("customFromAccXPath");
		
		String exchangeAccount = CommonFunctions.getNewUserAccount(exchangeComposeMail, "exchangeAccount", "exchange");

		if(!CommonFunctions.isAccountAlreadySelected(exchangeAccount))
			userAccount.switchMailAccount(driver, exchangeAccount);

		inbox.composeAsNewMail(driver,toAccount,null, null, mailSubject, mailBody);
		customFromAccXPath = customFromAccXPath.replace("EmailHere", customFromAcc);
		CommonFunctions.changeCustomFrom(driver, accountSpinner, customFromAccXPath);
		Thread.sleep(3000);
		inbox.tapOnSendForNewMail();
		Thread.sleep(3000);
		
		if(!CommonFunctions.isAccountAlreadySelected(toAccount))
			userAccount.switchMailAccount(driver, toAccount);
		Thread.sleep(2000);
		inbox.findMailInPrimaryFolder(driver, mailSubject);
		log.info("********** Mail found successfully with the subject "+mailSubject);
	}


	public void verifyChangingFromCustomFieldAndRotation() throws Exception {
		try {
			
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> exchangeComposeMail = CommonPage.getTestData("ExchangeComposeMail");
			
			String mailSubject =  exchangeComposeMail.get("MailSubject");
			String exchangeAccount =  exchangeComposeMail.get("exchangeAccount");
			String mailBody =  exchangeComposeMail.get("mailBody");
			String accountSpinner =  exchangeComposeMail.get("accountSpinner");
			String attachFilePath =  exchangeComposeMail.get("AttachFilePath");
			String drivePath =  exchangeComposeMail.get("DrivePath");
			String customFromAccXPath =  exchangeComposeMail.get("customFromAccXPath");
			String filePath =  exchangeComposeMail.get("FilePath");
			
			String gig1Account = CommonFunctions.getNewUserAccount(exchangeComposeMail, "customFromAcc", null);
			String gig2Account =  exchangeComposeMail.get("ToAccount");

			if(!CommonFunctions.isAccountAlreadySelected(gig1Account))
				userAccount.switchMailAccount(driver, gig1Account);

			inbox.composeAsNewMail(driver,gig2Account,null, null, mailSubject, mailBody);
			
			driver.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
			Thread.sleep(3000);

			customFromAccXPath = customFromAccXPath.replace("EmailHere", gig2Account);
			driver.navigate();
			CommonFunctions.scrollUp(driver, 1);
			Thread.sleep(2000);
			CommonFunctions.changeCustomFrom(driver, accountSpinner, customFromAccXPath);
			
			driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
			Thread.sleep(2000);
			CommonFunctions.scrollUp(driver, 1);
			customFromAccXPath = "";
			customFromAccXPath = exchangeComposeMail.get("customFromAccXPath").replace("EmailHere", exchangeAccount);
			CommonFunctions.changeCustomFrom(driver, accountSpinner, customFromAccXPath);
			
			Thread.sleep(1000);
			driver.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
			Thread.sleep(3000);
			driver.navigate();

			CommonFunctions.scrollUp(driver, 1);
			
			CommonFunctions.searchAndClickByXpath(driver, attachFilePath);
			CommonFunctions.searchAndClickByXpath(driver, attachFilePath);
			
		/*	
			if(CommonFunctions.getSizeOfElements(driver, drivePath)>=0){
				CommonFunctions.searchAndClickByXpath(driver, drivePath);
			}
			if(CommonFunctions.getSizeOfElements(driver, drivePath)>=0){
				CommonFunctions.searchAndClickByXpath(driver, drivePath);
			}
			if(CommonFunctions.getSizeOfElements(driver, gig1Account)>=0){
				CommonFunctions.scrollsToTitle(driver, gig1Account);
			}
		*/	
			int count = 0;
			while(count++ <3) {
				try {
						CommonFunctions.searchAndClickByXpath(driver, filePath);	
					break;
				} catch (Exception e) {
					InboxFunctions.scrollDown(1);
				}
			}
			Thread.sleep(2000);
			driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
			Thread.sleep(2000);
			
			customFromAccXPath = "";
			customFromAccXPath = exchangeComposeMail.get("customFromAccXPath").replace("EmailHere", gig1Account);
			CommonFunctions.changeCustomFrom(driver, accountSpinner, customFromAccXPath);
			
			inbox.tapOnSendForNewMail();
			Thread.sleep(2000);
			userAccount.goToSpecificLable(driver, "Sent");
			inbox.findMailInPrimaryFolder(driver, mailSubject);
			
			CommonFunctions.verifyAttachmentExistance(driver, mailSubject, "Appium.jpg");

			Thread.sleep(2000);
			driver.navigate().back();
			log.info("Attachment find succeffully in sent folder");
			
			if(!CommonFunctions.isAccountAlreadySelected(gig2Account))
				userAccount.switchMailAccount(driver, gig2Account);
			
			inbox.findMailInPrimaryFolder(driver, mailSubject);
			Thread.sleep(2000);
			InboxFunctions.pullToReferesh(driver);
			CommonFunctions.verifyAttachmentExistance(driver, mailSubject, "Appium.jpg");
			
			log.info("Attachment find succeffully in Primary folder");

			log.info("********** Mail found successfully with the subject "+mailSubject);
			
		} catch (Exception e) {
			throw new Exception("Exception "+e.getMessage());
		}finally {
			Thread.sleep(1000);
			driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
		}

	}

}
