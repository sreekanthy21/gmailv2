package com.etouch.mobile.gmail;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.lang.UsesSunHttpServer;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.common.GetDataInHashMap;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class GmailTest extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/**
	 * @TestCase 1 : Add new Gmail account(Consumer account) to app
	 * @Pre-condition : Gmail App should be installed in testing device.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 1)
	public void verifyAddingNewGmailAccount() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAddingNewGmailAccount*****************************");
		//	inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyAddingNewGmailAccountData = commonPage
					.getTestData("verifyAddingNewGmailAccount");
			boolean isGmailWelComePageDisplayed = userAccount
					.isGmailWelComePageDisplayed(driver);
			if (isGmailWelComePageDisplayed) {
				userAccount.addNewAccountFromGmailHomePage(driver);
			}
			String expectedAccountTypeList = verifyAddingNewGmailAccountData.get("ExpectedAccountTypeList");
			userAccount.addNewMultipleGmailAccount(driver,
					verifyAddingNewGmailAccountData.get("UserList"),
					verifyAddingNewGmailAccountData.get("Password"),
					isGmailWelComePageDisplayed,expectedAccountTypeList);
			log.info("*****************Completed testcase ::: verifyAddingNewGmailAccount*************************");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * @TestCase 2 : Verify Swipe to Delete/archive and Undo
	 * @Pre-condition : Mail with subject "verifySwipeDeleteFunctionality" and
	 *                "verifySwipeArchiveFunctionality" should be available
	 *                under account one
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 4)
	public void verifySwipeDeleteFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifySwipeDeleteFunctionality***********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyForwardingMailFunctionality = commonPage
					.getTestData("verifySwipeDeleteFunctionality");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyForwardingMailFunctionality.get("AccountOne")))
			userAccount.switchMailAccount(driver,
					verifyForwardingMailFunctionality.get("AccountOne"));
			userAccount.setGmailDefaultActionToArchive(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			inbox.undoSwipeDeleteArchiveOperation(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectArchive"));
			userAccount.setGmailDefaultActionToDelete(driver);
			navDrawer.openPrimaryInbox(driver,
					verifyForwardingMailFunctionality.get("AccountOne"));
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			inbox.undoSwipeDeleteArchiveOperation(driver);
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			navDrawer.navigateToBin(driver, verifyForwardingMailFunctionality.get("BinOption"));
			inbox.verifyMailIsPresent(driver,
					verifyForwardingMailFunctionality.get("MailSubjectDelete"));
			//commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifySwipeDeleteFunctionality************");
		}
			catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySwipeDeleteFunctionality");
		}
	}

	/**
	 * @TestCase 3 : Verify Compose and send mail with CC,BCC fields
	 * @Pre-condition : At least 1 account should be there in Gmail. Account is
	 *                already added in testcase1.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 2)
	public void verifyComposeAndSendMail() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyComposeAndSendMail***************************");

			inbox.verifyGmailAppInitState(driver);

			Map<String, String> verifyComposeAndSendMailData = commonPage
					.getTestData("verifyComposeAndSendMail");
			//Thread.sleep(5000);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyComposeAndSendMailData.get("AccountOne")))
			userAccount.switchMailAccount(driver,
					verifyComposeAndSendMailData.get("AccountOne"));

			//Thread.sleep(2000);
			String fromUser=inbox.composeAndSendNewMail(driver,
					verifyComposeAndSendMailData.get("ToFieldData"),
					verifyComposeAndSendMailData.get("CCFieldData"), "",
					verifyComposeAndSendMailData.get("SubjectData"),
					verifyComposeAndSendMailData.get("ComposeBodyData"));
			
			if(!CommonFunctions.isAccountAlreadySelected(fromUser))
			userAccount.switchMailAccount(driver,fromUser);
			
			inbox.scrollDown(1);
			inbox.verifyMailInSentBox(driver,
					verifyComposeAndSendMailData.get("SubjectData"));
			log.info("*****************Completed testcase ::: verifyComposeAndSendMail***************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyComposeAndSendMail");
		}
	}

	/**
	 * @TestCase 4 : Verify All inbox basic functionality
	 * @Pre-condition : Mail with subject "InboxOne" should be available under
	 *                account one. Mail with subject "Inboxtwo" should be
	 *                available under account two. Mail with subject
	 *                "Inboxthree" should be available under account three. Mail
	 *                with subject "InboxOne" should be available under account
	 *                three.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 18)
	public void verifyAllInboxBasicFunctionality() throws Exception {

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAllInboxBasicFunctionality***************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyAllInboxbasicFunctionalityData = commonPage
					.getTestData("verifyAllInboxbasicFunctionality");

			userAccount.addRediffAccount(driver,
					verifyAllInboxbasicFunctionalityData.get("RediffID"),
					verifyAllInboxbasicFunctionalityData.get("Password"));
			navDrawer.navigateToAllInbox(driver);
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject3"));
			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyOne"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);

			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyTwo"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
			inbox.searchForMail(driver,
					verifyAllInboxbasicFunctionalityData.get("SearchKeyThree"));
			inbox.verifyMailIsPresent(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject3"));
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
		/*	inbox.selectMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			inbox.selectMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			
			inbox.verifyMailSelectionCount(driver,
					verifyAllInboxbasicFunctionalityData.get("SelectionCount"));
			commonFunction.navigateBack(driver);*/
			userAccount.setGmailDefaultActionToArchive(driver);

			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject1"));
			userAccount.setGmailDefaultActionToDelete(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject2"));
			/*inbox.openMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject4"));*/
			inbox.swipeToNextMail(driver,
					verifyAllInboxbasicFunctionalityData.get("MailSubject4"));
			commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyAllInboxBasicFunctionality************");
		} catch (Throwable e) {
			commonPage
					.catchBlock(e, driver, "verifyAllInboxBasicFunctionality");
		}
	}

	/**
	 * @TestCase 6 : Verify Forward mail functionality
	 * @Pre-condition : Mail with subject "verifyForwardingMailFunctionality"
	 *                should be available under Account Two.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 6)
	public void verifyForwardingMailFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyForwardingMailFunctionality***************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyForwardingMailFunctionalityData = commonPage
					.getTestData("verifyForwardingMailFunctionality");

			if(!CommonFunctions.isAccountAlreadySelected(verifyForwardingMailFunctionalityData.get("AccountTwo")))
			userAccount.switchMailAccount(driver,
					verifyForwardingMailFunctionalityData.get("AccountTwo"));

			inbox.searchForMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));

			inbox.openMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			inbox.composeMailAsFowarding(driver,
					verifyForwardingMailFunctionalityData.get("AccountThree"),
					"", "", verifyForwardingMailFunctionalityData
							.get("ComposeBodyData"));
			attachment
					.addAttachmentFromDrive(driver,
							verifyForwardingMailFunctionalityData
									.get("AttachmentName"));
			inbox.insertInLineText(driver,
					verifyForwardingMailFunctionalityData.get("InLineData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);

			inbox.verifyMailInSentBox(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyForwardingMailFunctionalityData.get("AccountThree")))
			userAccount.switchMailAccount(driver,
					verifyForwardingMailFunctionalityData.get("AccountThree"));

			inbox.searchForMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			inbox.openMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));

			// Instead of testing the inline data, we should test the link
			// inbox.verifyInlineTextIsPresent(verifyForwardingMailFunctionalityData.get("InLineData"));
			// inbox.verifyReplyInlineTextIsPresent(verifyForwardingMailFunctionalityData.get("ComposeBodyData"));
			commonFunction.scrollUp(driver, 1);
			attachment
					.verifyAttachmentIsAddedSuccessfully(driver,
							verifyForwardingMailFunctionalityData
									.get("AttachmentName"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyForwardingMailFunctionality**********");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyForwardingMailFunctionality");
		}
	}

	/**
	 * @TestCase 7 : Verify Reply/Reply All mail
	 * @Pre-condition : Mail with subject "verifyReplyAllFunctionality" should
	 *                be available under Account Two.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 3)
	public void verifyReplyAllFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyReplyAllFunctionality**********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyReplyFunctionalityData = commonPage
					.getTestData("verifyReplyAllFunctionality");

			if(!CommonFunctions.isAccountAlreadySelected(verifyReplyFunctionalityData.get("AccountTwo")))
			userAccount.switchMailAccount(driver,
					verifyReplyFunctionalityData.get("AccountTwo"));

			inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));

			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			inbox.composeMailAsReply(
					driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"),
					verifyReplyFunctionalityData.get("AccountOne"));
			inbox.insertInLineText(driver,
					verifyReplyFunctionalityData.get("InLineData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			//inbox.openMenuDrawer(driver);
			
			//Thread.sleep(7000);
			//inbox.scrollDown(1);
			log.info("Verifying Mail is sent");
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(2000);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyReplyFunctionalityData.get("AccountOne")))
			userAccount.switchMailAccount(driver,
					verifyReplyFunctionalityData.get("AccountOne"));
			inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));

			inbox.verifyReplyInlineTextIsPresent(driver, verifyReplyFunctionalityData
					.get("InLineDataVerify"));
			inbox.composeMailAsReplyAll(driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);

			inbox.scrollDown(1);
			Thread.sleep(7000);
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(3000);
			log.info("*****************Completed testcase ::: verifyReplyAllFunctionality**********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyReplyAllFunctionality");
		}
	}

	/**
	 * @TestCase 8 : Verify Mail snippet in Thread list
	 * @Pre-condition : Mail with subject "verifyMailSnippetInThreadList" and
	 *                "VerifyReplyAllFunctionality" should be available under
	 *                Account two
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 8)
	public void verifyMailSnippetInThreadList() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyMailSnippetInThreadList******************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyMailSnippetInThreadListData = commonPage
					.getTestData("verifyMailSnippetInThreadList");

			String user = verifyMailSnippetInThreadListData.get("AccountFour");
			
			if(!CommonFunctions.isAccountAlreadySelected(user))
			userAccount.switchMailAccount(driver, user);
			navDrawer.openPrimaryInbox(driver, user);
		
			inbox.searchForMail(driver,
					verifyMailSnippetInThreadListData.get("MailSubject"));
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage("verifyMailSnippetInThreadList"),
					"Image comparison for verifyMailSnippetInThreadList");
			inbox.openMail(driver,
					verifyMailSnippetInThreadListData.get("MailSubject"));
			
			conversationView.verifyRecipientDetails(driver,
					verifyMailSnippetInThreadListData.get("alertTitle"));
			Thread.sleep(3000);
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyMailSnippetInThreadListMailView"),
							"Image comparison for verifyMailSnippetInThreadListMailView");
			
	
			commonFunction.navigateBack(driver);
			
			//GetDataInHashMap.getScreenShot(driver,"verifyMailSnippetInThreadList");
			//commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);
			/*softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage2("verifyMailSnippetInThreadList"),
					"Image comparison for verifyMailSnippetInThreadList");
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage2("verifyMailSnippetInThreadListMailView"),
					"Image comparison for verifyMailSnippetInThreadListMailView");*/
		

			/*
			 * inbox.openAnotherMailUsingSearch(
			 * verifyMailSnippetInThreadListData.get("MailSubjectForReplyAll"));
			 * inbox.openFirstMail(verifyMailSnippetInThreadListData.get(
			 * "MailSubjectForReplyAll"));
			 * conversationView.verifySuperCollapseCountOfMail();
			 * commonFunction.navigateBack(driver);
			 */
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyMailSnippetInThreadList********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyMailSnippetInThreadList");
		}
	}

	/**
	 * @TestCase 9 : Verify Super collpase for multiple thread mails
	 * @Pre-condition : Mail with subject
	 *                "verifyZoomingOutTablesForConversationView" should be
	 *                available under Account Two with upto 4 threads.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 9)
	public void verifySuperCollapseForMultiThreadedMail() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifySuperCollapseForMultiThreadedMail*********");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifySuperCollapseForMultiThreadedMailData = commonPage
					.getTestData("verifySuperCollapseForMultiThreadedMail");
			String mailSubject = verifySuperCollapseForMultiThreadedMailData
					.get("MailSubject").toString();
			String user = verifySuperCollapseForMultiThreadedMailData
					.get("AccountTwo");
			
			if(!CommonFunctions.isAccountAlreadySelected(user))
			userAccount.switchMailAccount(driver, user);
			navDrawer.openPrimaryInbox(driver, user);
			Thread.sleep(2000);
			inbox.searchForMail(driver, mailSubject);
			inbox.openMail(driver, mailSubject);
			Thread.sleep(2000);

			commonFunction.navigateBack(driver);
			// For Smasung additional navigate back needed for displaying super
			// collapse count
			inbox.openMail(driver, mailSubject);

			Thread.sleep(2000);
			commonFunction.navigateBack(driver);
			Thread.sleep(2000);

			inbox.openMail(driver, mailSubject);
			conversationView.verifySuperCollapseCountOfMail(driver);
			Thread.sleep(4000);
			inbox.composeMailAsReply(
					driver,
					verifySuperCollapseForMultiThreadedMailData.get("TestData"),
					null);
			commonFunction.navigateBack(driver);
			Thread.sleep(4000);
			// softAssert.assertTrue(
			// commonPage.getScreenshotAndCompareImage("verifySuperCollapseForMultiThreadedMailDraft"),
			// "Image comparison for
			// verifySuperCollapseForMultiThreadedMailDraft");

			inbox.tapOnFirstMail(driver);
			commonFunction.scrollUp(driver, 1);
			inbox.tapOnInMailReplyIcon(driver);
			inbox.composeOpenedMailBody(driver, "", "", "", "",
					verifySuperCollapseForMultiThreadedMailData.get("TestData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			inbox.openMail(driver, verifySuperCollapseForMultiThreadedMailData
					.get("MailSubject"));
			Thread.sleep(3000);
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifySuperCollapseForMultiThreadedMail"),
							"Image comparison for verifySuperCollapseForMultiThreadedMail");
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifySuperCollapseForMultiThreadedMail***********");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifySuperCollapseForMultiThreadedMail");
		}
		Thread.sleep(10000);
	}

	/**
	 * @TestCase 10 : Verify Verify Conversation view for mail / all thread
	 * @Pre-condition : Mail with subject "verifyForwardingMailFunctionality"
	 *                should be available under Account Two
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 10)
	public void verifyMailConversationView() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyMailConversationView***************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyMailConversationView = new HashMap<String, String>();
			verifyMailConversationView = commonPage
					.getTestData("verifyMailConversationView");

			if(!CommonFunctions.isAccountAlreadySelected(verifyMailConversationView.get("AccountFour")))
			userAccount.switchMailAccount(driver,
					verifyMailConversationView.get("AccountFour"));
			inbox.searchForMail(driver,
					verifyMailConversationView.get("MailSubject"));
			inbox.openMail(driver,
					verifyMailConversationView.get("MailSubject"));
			commonFunction.scrollDown(driver, 1);
			inbox.tapOnShowQuotedText(driver);
			inbox.tapOnHideQuotedText(driver);
			commonFunction.scrollUp(driver, 1);

			conversationView.verifyRecipientDetails(driver,
					verifyMailConversationView.get("alertTitle"));
			conversationView.actionBarConversationView(driver);
			commonFunction.navigateBack(driver);
			attachment.openAndValidateAttachment(driver,
					verifyMailConversationView.get("AttachmentName"));
			// Device get hang while opening chrome browser
			inbox.openLinkInChrome(driver,
					verifyMailConversationView.get("LinkText"), verifyMailConversationView.get("InAppBrowserMoreOptionsOpenInChrome"));
			// commonFunction.navigateBack(driver);
			Thread.sleep(3000);
			conversationView.tapOnRecipientSummary(driver);
			conversationView.tapOnEmailSnippet(driver);

			conversationView.actionBarConversationView(driver);
			commonFunction.navigateBack(driver);
			inbox.verifySubjectOfMail(driver,
					verifyMailConversationView.get("MailSubject"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyMailConversationView********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyMailConversationView");
			log.info("Stop Time :" + System.currentTimeMillis());
		}
	}

	/**
	 * @TestCase 11 : Verify Open various types of attachments and drive chips
	 *           for mail
	 * @Pre-condition : Mail with subject "Verifying Gmail Attachments Types"
	 *                should be available under Account One
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 11)
	public void verifyAttachments() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAttachments***************************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyAttachments = commonPage
					.getTestData("verifyAttachments");

			if(!CommonFunctions.isAccountAlreadySelected(verifyAttachments.get("AccountFour")))
			userAccount.switchMailAccount(driver,
					verifyAttachments.get("AccountFour"));
			inbox.searchForMail(driver, verifyAttachments.get("MailSubject"));
			inbox.openMail(driver, verifyAttachments.get("MailSubject"));
			commonFunction.scrollDown(driver, 2);

			/*attachment.openAndValidateAttachment1(driver,
					verifyAttachments.get("MyDriveGmailExcelFileName"));
			*/// attachment.openAndValidateAttachment1(verifyAttachments.get("MyDriveGmailSlidesFileName"));
			attachment.openAndValidateEMLFileAttachment(driver,
					verifyAttachments.get("MyDriveEMLFileName"));
			attachment.scrollDown(1);
			attachment.openAndValidateAttachment1(driver,
					verifyAttachments.get("MyDrivePDFFileName"));
			// attachment.openAndValidateAttachment1(verifyAttachments.get("MyDriveImageFileName"));
			attachment.navigateBack(driver);
			attachment.navigateBack(driver);
			attachment.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyAttachments*********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAttachments");
		}
	}

	/**
	 * @TestCase 12 : Verify Archvie/Delete (CAB mode) muiltipe mails
	 * @Pre-condition :
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 12)
	public void verifyArchiveAndDeletingMultipleMails() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyArchiveAndDeletingMultipleMails********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyArchiveAndDeletingMultipleMailsData = commonPage
					.getTestData("verifyArchiveAndDeletingMultipleMails");

			if(!CommonFunctions.isAccountAlreadySelected(verifyArchiveAndDeletingMultipleMailsData.get("AccountThree")))
			userAccount.switchMailAccount(driver,verifyArchiveAndDeletingMultipleMailsData.get("AccountThree"));
			userAccount.navigateToGeneralSettings(driver);
			inbox.checkSenderImageCheckBox(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
					.get("ArchiveMail1"));
			Thread.sleep(1000);
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
					.get("ArchiveMail2"));
			Thread.sleep(1000);
			inbox.verifyMailSelectionCount(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("MailSelectionCount"));
			// System.out.println("First Time selection count");
			navDrawer.tapOnArchive(driver);
			navDrawer.navigateToAllEmails(driver);

			inbox.verifyMailIsPresent(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("ArchiveMail1"));
			inbox.verifyMailIsPresent(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("ArchiveMail2"));
			// userAccount.switchMailAccount(driver,
			// verifyArchiveAndDeletingMultipleMailsData.get("AccountTwo"));
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
					.get("DeleteMail1"));
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData
					.get("DeleteMail2"));
			// inbox.selectMail(verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail3"));
			// System.out.println("Second Time selection count");
			inbox.verifyMailSelectionCount(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("MailSelectionCount"));
			navDrawer.tapOnDelete(driver);
			//navDrawer.navigateToBin(driver);
			navDrawer.navigateToBin(driver, verifyArchiveAndDeletingMultipleMailsData.get("BinOption"));
			
			inbox.verifyMailIsPresent(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("DeleteMail1"));
			inbox.verifyMailIsPresent(driver,
					verifyArchiveAndDeletingMultipleMailsData
							.get("DeleteMail2"));
			// inbox.verifyMailIsPresent(driver,verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail3"));
			// commonFunction.navigateBack(driver);
			// commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyArchiveAndDeletingMultipleMails************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyArchiveAndDeletingMultipleMails");
		}
	}

	/**
	 * @TestCase 13 : Verify Promo Tab Redesign ads TL View
	 * @Pre-condition : Ad should be available under Promotion mails for account
	 *                two
	 * @throws: Exception
	 */
	@Test(enabled = false, priority = 13)
	public void verifyPromotionsTabRedesignForADS() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyPromotionsTabRedignforADS**************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyPromotionsTabRedignforADSData = commonPage
					.getTestData("verifyPromotionsTabRedesignForADS");

			String user = verifyPromotionsTabRedignforADSData.get("AccountOne");
			
			if(!CommonFunctions.isAccountAlreadySelected(user))
			userAccount.switchMailAccount(driver, user);
			inbox.openMenuDrawer(driver);
			boolean isPromotionsInboxEnabled = navDrawer
					.isPromotionsInboxDisplayed(driver);
			if (!isPromotionsInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver, user);
				navDrawer.verifyDefaultInbox(driver, user);
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
			}

			promotions.verifyPromotionsAdDisplayed(driver);
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyPromotionsTabRedignforADS"),
							"Image comparison for verifyPromotionsTabRedignforADS");
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyPromotionsTabRedignforADS*****************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPromotionsTabRedignforADS");
		}
	}

	/**
	 * @TestCase 14 : Verify Default and Priority Inbox list views and ads in
	 *           Promotions
	 * @Pre-condition : Ad should be available under Promotion mails for Account
	 *                one
	 * @throws: Exception
	 */
	@Test(enabled = false, priority = 14)
	public void verifyDefaultAndPriorityInboxListViews() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyDefaultAndPriorityInboxListViews************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyDefaultAndPriorityInboxListViewsData = commonPage
					.getTestData("verifyDefaultAndPriorityInboxListViews");

			String user = verifyDefaultAndPriorityInboxListViewsData
					.get("AccountOne");
			
			if(!CommonFunctions.isAccountAlreadySelected(user))
			userAccount.switchMailAccount(driver, user);
			inbox.openMenuDrawer(driver);
			boolean isPrimaryInboxEnabled = navDrawer
					.isPrimaryInboxDisplayed(driver);
			commonFunction.navigateBack(driver);
			userAccount.openUserAccountSetting(driver, user);
			navDrawer.verifyDefaultInbox(driver, user);
			navDrawer.verifyInboxCategories(driver);
			navDrawer.enableAllInboxCategories(driver, isPrimaryInboxEnabled);
			promotions.verifyPromotionsAdDisplayed(driver);
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyDefaultAndPriorityInboxListViews"),
							"Image comparison for verifyDefaultAndPriorityInboxListViews");

			commonFunction.navigateBack(driver);
			userAccount.openUserAccountSetting(driver, user);
			navDrawer.verifyPriorityInbox(driver, user);
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyDefaultAndPriorityInboxListViews*************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyDefaultAndPriorityInboxListViews");
		}
	}

	
	/**
	 * @TestCase 16 : Verify Able to expand Ad info and Control Ads
	 * @Pre-condition : Ad should be available under Promotion and Social tabs
	 *                for Account one.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 15)
	public void verifyExpandAndControlAds() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyExpandAndControlAds***************************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyDefaultAndPriorityInboxListViewsData = commonPage
					.getTestData("verifyDefaultAndPriorityInboxListViews");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyDefaultAndPriorityInboxListViewsData.get("AccountOne")))
			userAccount.switchMailAccount(driver,verifyDefaultAndPriorityInboxListViewsData.get("AccountOne"));
			inbox.openMenuDrawer(driver);

			boolean isPromotionsInboxEnabled = inbox.isInboxDisplayed(driver,
					"PromotionsInboxOption");
			boolean isSocialInboxEnabled = inbox.isInboxDisplayed(driver,
					"SocialInboxOption");

			if (!isPromotionsInboxEnabled && !isSocialInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver,
						verifyDefaultAndPriorityInboxListViewsData
								.get("AccountOne"));
				navDrawer.verifyDefaultInbox(driver,
						verifyDefaultAndPriorityInboxListViewsData
								.get("AccountOne"));
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
				commonFunction.navigateBack(driver);
			} else {
				commonFunction.navigateBack(driver);
			}
			promotions.verifyControlAds(driver, "PromotionsInboxOption");
			//commented since Social ad not displayed.
			//promotions.verifyControlAds(driver, "SocialInboxOption");
			log.info("*****************Completed testcase ::: verifyExpandAndControlAds***************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyExpandAndControlAds");
		}
	}

	
	/**
	 * @TestCase 15 : Verify user is able to Delete, Forward and Star Ad
	 * @Pre-condition : Ad should be available under Promotion and Social tabs
	 *                for Account one.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 16)
	public void verifyAdvertisementFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAdvertisementFunctionality**************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);

			Map<String, String> verifyAdvertismentData = commonPage
					.getTestData("verifyAdvertisementFunctionality");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyAdvertismentData.get("AccountOne")))
			userAccount.switchMailAccount(driver,verifyAdvertismentData.get("AccountOne"));
			inbox.openMenuDrawer(driver);
			boolean isPromotionsInboxEnabled = inbox.isInboxDisplayed(driver,
					"PromotionsInboxOption");
			boolean isSocialInboxEnabled = inbox.isInboxDisplayed(driver,
					"SocialInboxOption");
			if (!isPromotionsInboxEnabled && !isSocialInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver,
						verifyAdvertismentData.get("AccountOne"));
				navDrawer.verifyDefaultInbox(driver,
						verifyAdvertismentData.get("AccountOne"));
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
				commonFunction.navigateBack(driver);
			} else {
				commonFunction.navigateBack(driver);
			}
			log.info("Verify Ad functionality for Promotions Inbox option");
			promotions.verifyAdForward(driver,
					verifyAdvertismentData.get("AccountOne"),
					"PromotionsInboxOption",
					verifyAdvertismentData.get("ToFieldData"),
					verifyAdvertismentData.get("ComposeBodyData"));
			promotions.verifyAdDelete(driver, "PromotionsInboxOption");

			softAssert
					.assertTrue(promotions.verifyAd_StarAd(driver,
							"PromotionsInboxOption",
							"verifyAdvertisementFunctionalityPromotions"),
							"Image comparision failed for verifyAdvertisementFunctionalityPromotions");
/* Commented since social ad not displayed
			log.info("Verify Ad functionality for Social Inbox option");
			promotions.verifyAdForward(driver,
					verifyAdvertismentData.get("AccountOne"),
					"SocialInboxOption",
					verifyAdvertismentData.get("ToFieldData"),
					verifyAdvertismentData.get("ComposeBodyData"));
			promotions.verifyAdDelete(driver, "SocialInboxOption");
			softAssert
					.assertTrue(promotions.verifyAd_StarAd(driver,
							"SocialInboxOption",
							"verifyAdvertisementFunctionalitySocial"),
							"Image comparision failed for verifyAdvertisementFunctionalitySocial");
			*/
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyAdvertisementFunctionality*******************");
		} catch (Throwable e) {
			commonPage
					.catchBlock(e, driver, "verifyAdvertisementFunctionality");
		}
	}
	/**
	 * @TestCase 17 : Verify Verify calendar promotion in the conversation if
	 *           the calendar app is not installed
	 * @Pre-condition : Calendar Invitation mail should be available with
	 *                account two
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 17)
	public void verifyCalendarPromotionInConversation() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyCalendarPromotionInConversation******************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyCalendarPromotionInConversationData = commonPage
					.getTestData("verifyCalendarPromotionInConversation");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyCalendarPromotionInConversationData.get("AccountFour")))
			userAccount.switchMailAccount(driver,verifyCalendarPromotionInConversationData.get("AccountFour"));

			inbox.openMailUsingSearch(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));
			inbox.openFirstMail(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));
			calendar.verifyCalendarDetails(driver,
					verifyCalendarPromotionInConversationData
							.get("CalendarMessage"),
					verifyCalendarPromotionInConversationData
							.get("DownloadCalendarText"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyCalendarPromotionInConversation***********");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyCalendarPromotionInConversation");
		}
	}
	
	/**
	 * @TestCase 5 : Verify Zooming Out Tables for Conversation View
	 * @Pre-condition : Mail with subject
	 *                "verifyZoomingOutTablesForConversationView" should be
	 *                available under Account Two.
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 19)
	public void verifyZoomingOutTablesForConversationView() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyZoomingOutTablesForConversationView********");
			SoftAssert softAssert = new SoftAssert();
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyZoomingOutTablesForConversationViewData = commonPage
					.getTestData("verifyZoomingOutTablesForConversationView1");

			if(!CommonFunctions.isAccountAlreadySelected(verifyZoomingOutTablesForConversationViewData.get("AccountTwo")))
			userAccount.switchMailAccount(driver,
					verifyZoomingOutTablesForConversationViewData.get("AccountTwo"));
			inbox.searchForMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.openMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsFowarding(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("AccountThree"), "", "",
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSendAndNavigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyZoomingOutTablesForConversationViewData.get("AccountThree")))
			userAccount.switchMailAccount(driver,
					verifyZoomingOutTablesForConversationViewData.get("AccountThree"));
			inbox.searchForMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.openMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsReplyAll(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyZoomingOutTablesForConversationViewData.get("AccountTwo")))
			userAccount.switchMailAccount(driver,verifyZoomingOutTablesForConversationViewData.get("AccountTwo"));
			inbox.searchForMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.openMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsReplyAll(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSend(driver);
			inbox.tapOnShowQuotedText(driver);
			commonFunction.scrollTo(driver, "Forward");
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyZoomingOutTablesForConversationView"),
							"Image comparison for verifyZoomingOutTablesForConversationView");
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("Completion Time :" + System.currentTimeMillis());
			log.info("*****************Completed testcase ::: verifyZoomingOutTablesForConversationView************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyZoomingOutTablesForConversationView");
		}
	}

	/**
	 * @TestCase 18 : Manage Accounts - Verify Remove account
	 * @Pre-condition :
	 * @throws: Exception
	 */
	@Test(enabled = true, priority = 100)
	public void verifyRemoveAccount() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyRemoveAccount******************************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyManageAccountData = commonPage
					.getTestData("verifyRemoveAccount");
			userAccount.removeAccount(driver,
					verifyManageAccountData.get("MailID"), verifyManageAccountData.get("RemoveAccountMoreOptions"));
			// userAccount.removeAllAccounts();

			log.info("*****************Completed testcase ::: verifyRemoveAccount******************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyRemoveAccount");
		}
	}
	
	
	/** Testcase for edit draft and send mail
	 * 
	 * GAT18
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 19)
	public void verifyEditDraftAndSendMail() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("*****************Started testcase ::: verifyEditDraftAndSendMail*****************************");
			
			Map<String, String>verifyEditDraftAndSendMailData = commonPage.getTestData("verifyEditDraftAndSendMail");
			
			/*--Verifying the drafts option and click on it--*/
			inbox.openMenuDrawer(driver);
			//inbox.scrollDown(1);
			navDrawer.navigateToDrafts(driver);
			
			String fromUser=inbox.composeAsNewMail(driver,
					verifyEditDraftAndSendMailData.get("To"),
					verifyEditDraftAndSendMailData.get("CC"), "",
					verifyEditDraftAndSendMailData.get("SubjectData"),
					verifyEditDraftAndSendMailData.get("ComposeBodyData"));
			
			navDrawer.navigateBackTo(driver);
			
			String subject = verifyEditDraftAndSendMailData.get("SubjectData");
			inbox.openDraftMail(driver, subject);
			inbox.tapOnSendAndNavigateBack(driver);
			
			inbox.verifyDraftMail(driver, subject);
			log.info("*****************Completed testcase ::: verifyEditDraftAndSendMail************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyEditDraftAndSendMail");
		}
	}
	
	
	/**
	 * @TestCase 9 : Verify Compose and send mail for Gmail account with Insert From Drive sharing options 
	 * @Pre-condition : Gmail app installed
 						Gmail account is added to app
 						Drive installed 
						Documents yearlier not shared
	 * GAT9
	 * @throws: Exception
	 *  @author Phaneendra
	 */
	@Test(enabled=true, priority = 20)
	public void verifyAttachmentSharingOptions() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyAttachmentSharingOptions******************");
			
			Map<String, String> verifyEditDraftAndSendMailData = commonPage.getTestData("verifyEditDraftAndSendMail");
			Map<String, String> verifyForwardingMailFunctionalityData = commonPage.getTestData("verifyForwardingMailFunctionality");
			
			//--Open GoogleDrive--//
			inbox.verifyGoogleDriveAppInitState(driver);
			
			String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
			attachment.addFileToDrive(driver, fileName, verifyEditDraftAndSendMailData.get("eTouchOne"));
			
			inbox.launchGmailApplication(driver);
		//	userAccount.getListOfAddedAccounts(driver);
			String fromUser=inbox.composeAsNewMail(driver,
					verifyEditDraftAndSendMailData.get("ToEmail"), "", "", 
					verifyEditDraftAndSendMailData.get("SubjectData"), 
					verifyEditDraftAndSendMailData.get("ComposeBodyData"));
			attachment.addAttachmentFromDrive(driver, fileName);
			inbox.tapOnSend(driver);
			userAccount.verifySharingOptionPopUpIsPresent();
			inbox.tapOnSendSharingAlert(driver);
			log.info("*****************Completed testcase ::: verifyAttachmentSharingOptions************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyAttachmentSharingOptions");
		}
	}
	
	
	
	/** Testcase to verify adding IMAP , POP . Exchange accounts in app
	 * Outlook/Hotmail/Yahoo/Other
	 * 
	 * GAT5
	 * @preCondition: Latest Gmail and Exchange apks installed 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled=true, priority = 21)
	public void verifyAddingOtherAccounts()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			//inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyAddingOtherAccounts******************");
			
			Map<String, String> verifyAddingNewGmailAccountData = commonPage
					.getTestData("verifyAddingNewGmailAccount");	
			Map<String, String> getEmailAddressData = commonPage.getTestData("getEmailAddress");
			
			userAccount.removeAllAccountsNew();
			String expectedAccountTypeList = verifyAddingNewGmailAccountData.get("ExpectedAccountTypeList");
			userAccount.verifyAccountTypes(driver, expectedAccountTypeList);
		
		//	inbox.verifyGmailAppInitState(driver);
		//	inbox.openMenuDrawer(driver);
		//	userAccount.clickOnAccountSwitcher(driver);
			//remove accounts if already exist
			//--Adding Outlook,Hotmail, and Live Account--//
			log.info("**********Adding Outlook/Hotmail/Live Account*********");
			userAccount.addOutlookHotmailLiveAcct(driver, getEmailAddressData.get("OutlookEmailAddress"),
					getEmailAddressData.get("EmailPwdMicrosoft"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OutlookEmailAddress"));
			
			//--Adding Yahoo Account--//
			log.info("**********Adding Yahoo Account*********");
			userAccount.clickAddAccount(driver);
			userAccount.addYahooAccount(driver, getEmailAddressData.get("YahooEmail"),
					getEmailAddressData.get("YahooPassword"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("YahooEmail"));
			
			//--Adding Exchange Account--//
			log.info("*************Adding Exchange and Office 365 Account***********");
			userAccount.clickAddAccount(driver);
			userAccount.addExchangeAccount(driver, getEmailAddressData.get("ExchangeEmailAddress"),
					getEmailAddressData.get("ExchangePassword"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("ExchangeEmailAddress"));
			
			String imap = "IMAP", pop3 = "POP3";
			//--Adding Other Account using IMAP setting--//
			log.info("***************Adding Other Account using IMAP settings*************");
			userAccount.clickAddAccount(driver);
			userAccount.addOtherAccount(driver, imap, getEmailAddressData.get("OtherEmailAddress"),
					getEmailAddressData.get("OtherPassword"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OtherEmailAddress"));
			
			//--Adding the Other Account using POP3 settings--//
			log.info("***************Adding account using POP3 Settings*******************");
			userAccount.clickAddAccount(driver);
			userAccount.addOtherAccount(driver, pop3, getEmailAddressData.get("OtherEmailAddress1"),
					getEmailAddressData.get("OtherPassword1"));
			userAccount.verifyExchangeAccountAdded(driver, getEmailAddressData.get("OtherEmailAddress1"));
			
			//--Verifying the account are added with server settings--//
			userAccount.manageAccount(driver);
			//userAccount.verifyServerAccount(driver);
			log.info("*****************Completed testcase ::: verifyAddingOtherAccounts************");

		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyAddingOtherAccounts");
			
		}
	}
	
	/**Testcase to verify the account switcher and Drawer
	 * GAT42
	 *  
	 * @preCondition: Gmail app installed
 		Multiple Gmail accounts added to app
		Add 1 or more IMAP/POP3 accounts
		Add 1 Exchange account
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled=true, priority=22)
	public void verifyAccountSwitcherAndDrawer()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyAccountSwitcherAndDrawer******************");
			Map<String, String> getEmailAddressData = commonPage.getTestData("getEmailAddress");
			Map<String, String> accountDrawerData = commonPage.getTestData("accountDrawer");
			
			log.info("**********Verifying the Account Switcher**********");
			inbox.openMenuDrawer(driver);
			userAccount.verifyAccountsExists(driver, getEmailAddressData.get("OtherEmailAddress1"), getEmailAddressData.get("OtherPassword1"));
			userAccount.verifyAddAccountManageAccount(driver);
			
			log.info("**********Verifying the Account Drawer**********");
			inbox.openMenuDrawer(driver);
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Social"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Promotions"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Primary"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Starred"));
			navDrawer.navigateToSystemFolders(driver, accountDrawerData.get("Trash"));
			
			log.info("*****************Completed testcase ::: verifyAccountSwitcherAndDrawer************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyAccountSwitcherAndDrawer");
		}
	}
	
	/**Testcase to verify the Gmailify Flow
	 * 
	 * @preCondition Add Imap account
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 23)
	public void verifyGmailifyFlow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyGmailifyFlow******************");
			Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");
			userAccount.verifyIMAPAccountAdded(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("ImapEmailPwd"));
			
			userAccount.verifyAccountIsLinked(driver, gmailifyFlowData.get("ImapEmail"));
			
			userAccount.linkAccountGmailify(driver, gmailifyFlowData.get("ImapEmail"), gmailifyFlowData.get("GmailAccount"), 
					gmailifyFlowData.get("GmailPassword"), gmailifyFlowData.get("ImapEmailPwd"));
			
			userAccount.verifyGmailifyAccount(driver, gmailifyFlowData.get("ImapEmail"));			
			
			log.info("*****************Completed testcase ::: verifyGmailifyFlow************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyGmailifyFlow");
		}
	}
	
	/**TestCase to verify the IMAP account is setup with Try Gmailify option
	 * Gmailify in Account Setup flow
	 * @preCondition Add an IMAP account
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 24)
	public void verifyTryGmailifyFlow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyTryGmailifyFlow******************");
			Map<String, String> tryGmailifyFlowData = commonPage.getTestData("TryGmailifySetUpFlow");
			userAccount.verifyTryGmailifyAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			
			userAccount.setUpAccount(driver, tryGmailifyFlowData.get("IMAPEmail"),
					tryGmailifyFlowData.get("IMAPEmailPwd"));
			userAccount.tryGmailifySetUpIMAP(driver);
			//userAccount.verifyExchangeAccountAdded(driver, tryGmailifyFlowData.get("IMAPEmail"));
			userAccount.verifyGmailifyAccount(driver, tryGmailifyFlowData.get("ImapEmail"));			

			log.info("*****************Completed testcase ::: verifyTryGmailifyFlow************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "TryGmailifyFlow");
		}
	}
	
	/**Testcase to verify the Gmailify password change flow
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority=25)
	public void verifyGmailifyPasswordFlow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try{
			inbox.verifyGmailAppInit(driver);
			log.info("******************Started testcase ::: verifyGmailifyPasswordFlow******************");
			Map<String, String> gmailifyFlowData = commonPage.getTestData("gmailifyFlow");
			
			userAccount.verifyGmailifyAccount(driver, gmailifyFlowData.get("ImapEmail"));
			
			userAccount.changePasswordFromNotification(driver, gmailifyFlowData.get("OutlookNewPassword"));
			log.info("******************Completed testcase ::: verifyGmailifyPasswordFlow******************");

		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyGmailifyPasswordFlow");
		}
	}
	
	
	/**Testcase to verify the Help and Feedback tab is opened and displayed Help page
	 * 
	 * @preCondition Gmail app installed
					 Gmail account is added to app
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority=26)
	public void verifyHelpAndFeedBackTab() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			log.info("******************Started testcase ::: verifyHelpAndFeedBackTab******************");
			
			Map<String, String> verifyHelpAndFeedbackSectionData = CommonPage
					.getTestData("verifyHelpAndFeedbackSection");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyHelpAndFeedbackSectionData.get("AccountTwo")))
			userAccount.switchMailAccount(driver, verifyHelpAndFeedbackSectionData.get("AccountTwo"));
			userAccount.openMenuDrawer(driver);
			userAccount.openHelpAndFeedbackSection(driver);
			userAccount.verifyHelpDisplayed(driver,verifyHelpAndFeedbackSectionData.get("HelpText"));
			
			log.info("*****************Completed testcase ::: verifyHelpAndFeedBackTab************");

		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyHelpAndFeedBackTab");
		}
	}
	
	
	/**Testcase to verify the feedback functionality
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 27)
	public void verifyHelpAndFeedbackSection() throws Exception {
		try {
			SoftAssert softAssert = new SoftAssert();
			driver = map.get(Thread.currentThread().getId());
			inbox.verifyGmailAppInitState(driver);
			log.info("******************Started testcase ::: verifyHelpAndFeedbackSection******************");
			Map<String, String> verifyHelpAndFeedbackSectionData = CommonPage
					.getTestData("verifyHelpAndFeedbackSection");
			
			if(!CommonFunctions.isAccountAlreadySelected( verifyHelpAndFeedbackSectionData.get("AccountTwo")))
			userAccount.switchMailAccount(driver, verifyHelpAndFeedbackSectionData.get("AccountTwo"));
			userAccount.openMenuDrawer(driver);
			userAccount.openHelpAndFeedbackSection(driver);
			userAccount.verifyHelpDisplayed(driver,verifyHelpAndFeedbackSectionData.get("HelpText"));
			userAccount.ClickToSendFeedback(driver);
			userAccount.verifyforEditingAndSentFeedback(driver,verifyHelpAndFeedbackSectionData.get("feedbackText"));
			Thread.sleep(3000);
			softAssert
			.assertTrue(
					commonPage
							.getScreenshotAndCompareImage("verifyHelpAndFeedbackSection"),
					"Image comparison for verifyHelpAndFeedbackSection");

			softAssert.assertAll();
			Thread.sleep(3000);
		//	commonFunction.navigateBack(driver);
			
			log.info("*****************Completed testcase ::: verifyHelpAndFeedbackSection************");
			} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyHelpAndFeedbackSection");
		}
	}
	
	/**
	* @TestCase  : Verify Search Functionality
	* @Pre-condition : 1. Gmail app installed 2. Gmail account is added to app
	* @throws: Exception
	*/
	@Test(enabled = true, priority = 28)
	public void verifySearchFunctionality() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			inbox.verifyGmailAppInitState(driver);

			log.info("*****************Started testcase ::: verifySearchFunctionality*****************************");
			SoftAssert softAssert = new SoftAssert();
			Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
			
			if(!CommonFunctions.isAccountAlreadySelected(verifySearchFunctionalityData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifySearchFunctionalityData.get("AccountOne"));
			softAssert.assertTrue(commonFunction.isElementByIdDisplayed(driver, verifySearchFunctionalityData.get("SearchIconID")));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementById(driver, verifySearchFunctionalityData.get("SearchMailID")), verifySearchFunctionalityData.get("SearchMailText"));
			try{
				driver.hideKeyboard();
				driver.getKeyboard();
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "Keyboard not opening for verifySearchFunctionality");
			}
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityData.get("SearchMailID"), verifySearchFunctionalityData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			commonFunction.navigateBackToMenuDrawer(driver);
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementByXpath(driver, verifySearchFunctionalityData.get("suggestionTextPath")), verifySearchFunctionalityData.get("suggestionSearchText"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			
			log.info("*****************Completed testcase ::: verifySearchFunctionality*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionality");
		}
	}

	
	/**Testcase for verifying the trash and spam folders by deleting the mails
	 * 
	 * @preCondition There should exist few or more mails in Trash and Spam
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 29)
	public void verifyTrashandSpamFolders()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);
			log.info("*****************Started testcase ::: verifyTrashandSpamFolders*****************************");
			Map<String, String> verifyAddingNewGmailAccountData = CommonPage.getTestData("verifyAddingNewGmailAccount");
			//Ensure all items in Trash folder are deleted
			
			String primaryAccountNew = CommonFunctions.getNewUserAccount(verifyAddingNewGmailAccountData, "UserList", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(primaryAccountNew))
				userAccount.switchMailAccount(driver, primaryAccountNew); 

			
			log.info("************Verifying the Trash folder functionality*************");
			navDrawer.navigateToTrashFolder(driver);
			userAccount.verifyTrashFolderForMails(driver);
			inbox.emptyTrashOrSpamNow(driver, "Trash");
			log.info("*********Verified all mails are deleted from Trash folder**************");

			//Ensure all items in Spam folder are deleted
			log.info("*********Verifying the Spam folder functionality***********");
			navDrawer.navigateToSpamFolder(driver);
			userAccount.verifySpamFolderForMails(driver, primaryAccountNew);
			inbox.emptyTrashOrSpamNow(driver,"Spam");
			log.info("*********Verified all mails are deleted from Spam folder**************");

			log.info("*****************Completed testcase ::: verifyTrashandSpamFolders*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyTrashandSpamFolders");
		}
	}

	/**Testcase to verify the signature set to On/Off and send emails.
	 *  
	 * @throws Exception
	 * @preCondition Atleast 3 accounts should exists
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 30)
	public void verifySignatureOnAndOff()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInitState(driver);

			log.info("*****************Started testcase ::: verifySignatureOnAndOff*****************************");
			Map<String, String> verifySignatureOnAndOffData = commonPage.getTestData("verifySignatureOnAndOff");

			//Verify the accounts are exists in the app
			userAccount.verifyAccountsExists(driver, verifySignatureOnAndOffData.get("EmailAccounts"), verifySignatureOnAndOffData.get("EmailPassword"));
			
			//Set Signature On/Off for the accounts
			userAccount.setSignatureOnOff(driver, verifySignatureOnAndOffData.get("SignatureTextA"), verifySignatureOnAndOffData.get("SignatureTextC"),
					verifySignatureOnAndOffData.get("GeneralSettings"), verifySignatureOnAndOffData.get("AddAccount"), 
					verifySignatureOnAndOffData.get("AccountB"), verifySignatureOnAndOffData.get("AccountA"), verifySignatureOnAndOffData.get("AccountC"));
			
			inbox.verifySignatureInCompose(driver, verifySignatureOnAndOffData.get("AccountA"), verifySignatureOnAndOffData.get("AccountB"), 
					verifySignatureOnAndOffData.get("AccountC"), verifySignatureOnAndOffData.get("SignatureTextA"), verifySignatureOnAndOffData.get("SignatureTextC"));
			
			inbox.sendEmailAndVerify(driver, verifySignatureOnAndOffData.get("AccountA"), verifySignatureOnAndOffData.get("AccountC"), 
					verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			
			userAccount.verifyEmailNoSignature(driver, verifySignatureOnAndOffData.get("AccountA"), verifySignatureOnAndOffData.get("AccountC"), 
					verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			 inbox.sendEmailAndVerifyNoSignature(driver, verifySignatureOnAndOffData.get("AccountA"), verifySignatureOnAndOffData.get("AccountC"), 
					 verifySignatureOnAndOffData.get("SubjectText"), verifySignatureOnAndOffData.get("ComposeText"), verifySignatureOnAndOffData.get("SignatureTextA"));
			log.info("*****************Completed testcase ::: verifySignatureOnAndOff*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySignatureOnAndOff");
		}
	}
	
	/**Testcase to Delete mail and check in Trash
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 51
	 */
	@Test(enabled = true, priority = 51)
	public void deleteMailCheckTrash() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> deleteMailCheckTrashData = commonPage.getTestData("deleteMailCheckTrash");
			log.info("*****************Started testcase ::: deleteMailCheckTrash*****************************");
			
			if(!CommonFunctions.isAccountAlreadySelected(deleteMailCheckTrashData.get("Account")))
			userAccount.switchMailAccount(driver, deleteMailCheckTrashData.get("Account"));
			navDrawer.openPrimaryInbox(driver, deleteMailCheckTrashData.get("Account"));
			
			inbox.deleteMailPrimary(driver, deleteMailCheckTrashData.get("MailSubject"), null);
			inbox.verifySnackBarDeleted(driver);
			
			navDrawer.navigateToTrashFolder(driver);
			inbox.verifyMailTrashFolder(driver, deleteMailCheckTrashData.get("MailSubject"));
			log.info("*****************Completed testcase ::: deleteMailCheckTrash*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "deleteMailCheckTrash");
		}
		
	}
	
	/**Testcase to verify Enable/Disable Chrome Custom Tabs
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 32)
	public void verifyEnableDisableChromeTabs()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> enableDisableChromeCustomData = commonPage.getTestData("enableDisableChromeCustom");
			log.info("*****************Started testcase ::: verifyEnableDisableChromeTabs*****************************");
			
			if(!CommonFunctions.isAccountAlreadySelected(enableDisableChromeCustomData.get("Account")))
			userAccount.switchMailAccount(driver, enableDisableChromeCustomData.get("Account"));

			log.info("***********Verifying the link is opened in Gmail App when the option is Checked**************");
			userAccount.navigateToGeneralSettings(driver);
			userAccount.openWebLinksGmailOption(driver, true);
			inbox.verifyLinkOpenedInGmailApporBrowser(driver, enableDisableChromeCustomData.get("ChromeCustomSubject"), enableDisableChromeCustomData.get("GmailApp"));
			
			log.info("***********Verifying the link is opened in browser instead of Gmail app when the option is Unchecked**************");
			userAccount.navigateToGeneralSettings(driver);
			userAccount.openWebLinksGmailOption(driver, false);
			inbox.verifyLinkOpenedInGmailApporBrowser(driver, enableDisableChromeCustomData.get("ChromeCustomSubject"), enableDisableChromeCustomData.get("Browser"));
			log.info("*****************Completed testcase ::: verifyEnableDisableChromeTabs*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyEnableDisableChromeTabs");

		}
	}
	
	
	/**Testcase to verify the swipe down to refresh is working
	 * 
	 * @preCondition Add Gmail account to app
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 33)
	public void verifySwipeDownToRefreshWorks()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> swipeDownToRefreshData = commonPage.getTestData("swipeDownToRefresh");
			log.info("*****************Started testcase ::: verifySwipeWorks*****************************");
			
			userAccount.verifyAccountsExists(driver, swipeDownToRefreshData.get("Account"), swipeDownToRefreshData.get("AccountPassword"));
			driver.navigate().back();
			navDrawer.navigateToPrimary(driver);
			inbox.verifySwipeDownToRefresh(driver, swipeDownToRefreshData.get("EmailSubject"));
				
			log.info("*****************Completed testcase ::: verifySwipeWorks*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySwipeWorks");
		}
	}
	
	/**Testcase to Move mail to other folder
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 48
	 */
	@Test(enabled = true, priority = 48)
	public void verifyMailMoveToOtherFolder()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("*****************Started testcase ::: verifyMailMoveToOtherFolder*****************************");
			
			String emailId = mailMoveToOtherFolderData.get("Account");
			emailId = CommonFunctions.getNewUserAccount(mailMoveToOtherFolderData, emailId, null);

			
			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.moveMailToFolder(driver, mailMoveToOtherFolderData.get("EmailSubject"), mailMoveToOtherFolderData.get("FolderName"), emailId);
			navDrawer.navigateToSocial(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubject"));
			
			log.info("*****************Completed testcase ::: verifyMailMoveToOtherFolder*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailMoveToOtherFolder");
		}
	}
	
	/**Testcase to Archive mail
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 35)
	public void verifyArchiveMail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("*****************Started testcase ::: verifyArchiveMail*****************************");
			
			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.archiveMail(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"), null);
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"));
			
			log.info("*****************Completed testcase ::: verifyArchiveMail*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyArchiveMail");
		}
	}
	
	/**Testcase to Print/Print All mail and save as PDF
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 36)
	public void verifyMailSavedAsPDFPrint()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			Map<String, String> mailSaveAsPdfData = commonPage.getTestData("mailSaveAsPdf"); 
			log.info("*****************Started testcase ::: verifyMailSavedAsPDF*****************************");
			
			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.verifyMailSaveAsPDF(driver, mailSaveAsPdfData.get("FileName"), mailSaveAsPdfData.get("Kind"));
			
			log.info("*****************Completed testcase ::: verifyMailSavedAsPDF*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailSavedAsPDF");
		}
	}

	/**
	* @TestCase 68 : Verify all elements in the notification
	* @Pre-condition : 1. Gmail app installed
                       2. Gmail account is added to app
                       3. Single Message sent from another account/device
	*  @author batchi/Phaneendra
	* @throws: Exception
	*/
	@Test(enabled=true, priority=37)
	public void verifyMarkAsImportantInCV() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyMarkAsImportantInCV ******************************");
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			inbox.verifyGmailAppInitState(driver);
			
			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			
			inbox.verifyMarkAsImportant(driver, mailMoveToOtherFolderData.get("MailImportant"));
			log.info("*****************Completed testcase ::: verifyMarkAsImportantInCV*************************");
		}catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyMarkAsImportantInCV");
	}
	}
	
	/**Testcase to verify when the mail is marked as star
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 38)
	public void verifyMailMarkedAsStar()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("***************** Started testcase ::: verifyMailMarkedAsStar ******************************");

			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			
			inbox.verifyStarredMail(driver);
			log.info("*****************Completed testcase ::: verifyMailMarkedAsStar*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailMarkedAsStar");
		}
	}
	
	/**Testcase to verify the smart reply suggestions
	 * 
	 * @preCondition Mail should have smart reply suggestions
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 39)
	public void verifySmartReplySuggestions()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> smartReplySuggestionsData = commonPage.getTestData("smartReplySuggestions");
			log.info("***************** Started testcase ::: verifySmartReplySuggestions ******************************");

			userAccount.verifyAccountsExists(driver, smartReplySuggestionsData.get("Account"), smartReplySuggestionsData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, smartReplySuggestionsData.get("Account"));
			 
		    navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account"));
		    userAccount.verifySmartReplyEnabled(driver, true);
		    
		    inbox.verifyMailSmartReplySuggestions(driver, smartReplySuggestionsData.get("EmailSubject"));
			log.info("*****************Completed testcase ::: verifySmartReplySuggestions*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySmartReplySuggestions");
		}
	}
	
	/**Testcase to verify the i18N language for Chinese, Japanese and Korean
	 * 
	 * @preCondition Set device language to any specified Chinese(中文)/Japanese/Korean(한국어)
	 * Japanese is not available in S5 v6.0.1
	 * @throws Exception
	 *@author Phaneendra
	 */
	@Test(enabled = true, priority = 40)
	public void verifyi18NCJK()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> LocaleChinese = commonPage.getTestData("localLanguageChinese");
			userAccount.verifyAccountsExists(driver, LocaleChinese.get("Account"), LocaleChinese.get("AccountPassword"));
			log.info("***************** Started testcase ::: verifyi18NCJK ******************************");
			userAccount.setLanguageToLocale(driver, LocaleChinese.get("ChineseLanguage"));
			commonPage.launchGmailApp(driver);
			
			inbox.verifyComposeLocale(driver, LocaleChinese.get("ChineseComposeTitle"), LocaleChinese.get("ChineseFromAddress"), LocaleChinese.get("ChineseToAddress"),
					LocaleChinese.get("ChineseSubjectAddress"), LocaleChinese.get("ChineseComposeText"));
			inbox.verifySearchLocale(driver, LocaleChinese.get("ChineseSearchFieldText"));
			/*inbox.verifyMailOtherOptionsCV(driver, LocaleChinese.get("ChineseOverFlowButton"));
			inbox.verifyMailOtherOptionsTL(driver);
			inbox.verifyMailArchiveDelete(driver);
			inbox.verifyLeftNavigationLocale(driver);
			inbox.verifySettings(driver, LocaleChinese.get("Account"));
			*/
			log.info("*****************Completed testcase ::: verifyi18NCJK*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyi18NCJK");
		}
	}
	
	/*@Test(enabled = true, priority = 72)
	public void verifyLauncherShortcut()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> LocaleChinese = commonPage.getTestData("localLanguageChinese");

			log.info("***************** Started testcase ::: verifyLauncherShortcut ******************************");
			//userAccount.verifyAccountsExists(driver, LocaleChinese.get("Account"), LocaleChinese.get("AccountPassword"));
			driver.closeApp();
			driver.findElement(By.xpath("//android.widget.TextView[@text='Google']")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			WebElement e = driver.findElement(By.xpath("//android.widget.TextView[@text='Gmail']"));
			TouchAction t = new TouchAction(driver);
			t.longPress(e).perform();
			
			
			
			
			
			log.info("*****************Completed testcase ::: verifyLauncherShortcut*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyLauncherShortcut");
		}
	}
*/
	



}