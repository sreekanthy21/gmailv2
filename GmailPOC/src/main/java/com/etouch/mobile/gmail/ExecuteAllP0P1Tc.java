package com.etouch.mobile.gmail;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class ExecuteAllP0P1Tc extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	Android_Gmail_ApkInstallUninstall apkIntstallUninstall = new Android_Gmail_ApkInstallUninstall();
	Android_Gmail_Accounts accounts = new Android_Gmail_Accounts();
	Android_Gmail_Draft draft = new Android_Gmail_Draft();
	Android_Gmail_ConversationView conversationView = new Android_Gmail_ConversationView();
	Android_Gmail_ThreadConversationList threadConvList = new Android_Gmail_ThreadConversationList();
	Android_Gmail_AllInbox allInbox = new Android_Gmail_AllInbox();
	Android_Gmail_AcctSwitcherNavDrawer acctSwitNavDrawer = new Android_Gmail_AcctSwitcherNavDrawer();
	Android_Gmail_HelpAndFeedback helpAndfeedback = new Android_Gmail_HelpAndFeedback();
	Android_Gmail_MailAction mailAction = new Android_Gmail_MailAction();
	Android_Gmail_MailSync mailSync= new Android_Gmail_MailSync();
	Android_Gmail_Search search= new Android_Gmail_Search();
	Android_Gmail_Settings settings= new Android_Gmail_Settings();
	Android_Gmail_RichTextFormat richText= new Android_Gmail_RichTextFormat();
	Android_Gmail_RSVP rsvp= new Android_Gmail_RSVP();
	Android_Gmail_SpamFeatures spamfeature = new Android_Gmail_SpamFeatures();
	Android_Gmail_Gmailify gmailify = new Android_Gmail_Gmailify();
	Android_Gmail_LauncherShortCut launcherShortCut= new Android_Gmail_LauncherShortCut();
	Android_Gmail_PicoProjectionExcelSpreadsheetViewer ppexsv= new Android_Gmail_PicoProjectionExcelSpreadsheetViewer();
	Android_Gmail_ChromeCustom_Tabs ccTabs= new Android_Gmail_ChromeCustom_Tabs();
	Android_Gmail_AndroidNMultiWindow androidN= new Android_Gmail_AndroidNMultiWindow();
	Android_Gmail_A11Yandi18n a11Yi18N= new Android_Gmail_A11Yandi18n();
	Android_Gmail_All_Ads_Related allAdsRelated = new Android_Gmail_All_Ads_Related();
	Android_Gmail_Compose compose = new Android_Gmail_Compose();

	
	/**--------------------Accounts--------------------------**/
	/*@Test(priority = 1)
	public void verifyAddingNewGmailAccount()throws Exception{
		accounts.verifyAddingNewGmailAccount();
	}*/
	
	/**------------------Help and Feedback-------------------**/
	@Test(priority = 2)
	public void verifyHelpAndFeedBackTab()throws Exception{
		helpAndfeedback.verifyHelpAndFeedBackTab();
	}
	@Test(priority = 3)
	public void verifyHelpAndFeedbackSection()throws Exception{
		helpAndfeedback.verifyHelpAndFeedbackSection();
	}
	
	/*@Test(priority = 2)
	public void verifyAddingOtherAccounts()throws Exception{
		accounts.verifyAddingOtherAccounts();
	}
	*//**----------------Apk Install/Uninstall-----------------**//*
	@Test(priority = 3)
	public void verifyDowngradeOfGmailApp()throws Exception{
		apkIntstallUninstall.verifyDowngradeOfGmailApp();
	}*/
	@Test(priority = 4)
	public void verifyUpgradeOfGmailApp()throws Exception{
		apkIntstallUninstall.verifyUpgradeOfGmailApp();
	}
	/**-----------------------Compose------------------------**/
	@Test(priority = 5)
	public void verifyComposeAndSendMail()throws Exception{
		compose.verifyComposeAndSendMail();
	}
	@Test(priority = 6)
	public void verifyAttachmentsUponRotating()throws Exception{
		compose.verifyAttachmentsUponRotating();
	}
	@Test(priority = 7)
	public void composeAndSendMailForGmailAccountWithAttachments()throws Exception{
		compose.composeAndSendMailForGmailAccountWithAttachments();
	}
	@Test(priority = 8)
	public void verifyReplyFromNotification()throws Exception{
		compose.verifyReplyFromNotification();
	}
	/**-------------------------Drafts-----------------------**/
	@Test(priority = 9)
	public void composeMailAndSaveAsDraft()throws Exception{
		draft.composeMailAndSaveAsDraft();
	}
	@Test(priority = 10)
	public void composeMailWithMultipleAttachmentsAndAnsertedDriveChips()throws Exception{
		draft.composeMailWithMultipleAttachmentsAndAnsertedDriveChips();
	}
	@Test(priority = 11)
	public void verifyEditDraftAndSendMail()throws Exception{
		draft.verifyEditDraftAndSendMail();
	}
	/**----------------Conversation View---------------------**/
	@Test(priority = 12)
	public void verifyMailConversationView()throws Exception{
		conversationView.verifyMailConversationView();
	}
	@Test(priority = 13)
	public void verifySmartReplySuggestions()throws Exception{
		conversationView.verifySmartReplySuggestions();
	}
	@Test(priority = 14)
	public void verifyReplyAllFunctionality()throws Exception{
		conversationView.verifyReplyAllFunctionality();
	}
	@Test(priority = 15)
	public void verifyForwardingMailFunctionality()throws Exception{
		conversationView.verifyForwardingMailFunctionality();
	}
	@Test(priority = 16)
	public void verifyCalendarPromotionInConversation()throws Exception{
		conversationView.verifyCalendarPromotionInConversation();
	}
	@Test(priority = 17)
	public void verifySmartReplySuggestionsAfterReplying()throws Exception{
		conversationView.verifySmartReplySuggestionsAfterReplying();
	}
	/**----------------Thread Conversation List--------------**/
	@Test(priority = 18)
	public void verifyArchiveAndDeletingMultipleMails()throws Exception{
		threadConvList.verifyArchiveAndDeletingMultipleMails();
	}
	@Test(priority = 19)
	public void verifySwipeDeleteFunctionality()throws Exception{
		threadConvList.verifySwipeDeleteFunctionality();
	}
	/**------------------------All Inbox---------------------**/
	@Test(priority = 20)
	public void verifyAllInboxBasicFunctionality()throws Exception{
		allInbox.verifyAllInboxBasicFunctionality();
	}
	/**-------Account Switcher and Navigation Drawer---------**/
	@Test(priority = 21)
	public void verifyAccountSwitcherAndDrawer()throws Exception{
		acctSwitNavDrawer.verifyAccountSwitcherAndDrawer();
	}
	@Test(priority = 22)
	public void verifyOpenAndCloseDrawer()throws Exception{
		acctSwitNavDrawer.verifyOpenAndCloseDrawer();
	}
	
	/**-------------Android_Gmail_MailAction-----------------**/
	@Test(priority = 25)
	public void verifyTrashandSpamFolders()throws Exception{
		mailAction.verifyTrashandSpamFolders();
	}
	@Test(priority = 26)
	public void markMailReadUnread()throws Exception{
		mailAction.markMailReadUnread();
	}
	@Test(priority = 27)
	public void verifyMailMarkedAsStar()throws Exception{
		mailAction.verifyMailMarkedAsStar();
	}
	@Test(priority = 28)
	public void changeLabels()throws Exception{
		mailAction.changeLabels();
	}
	@Test(priority = 29)
	public void verifyMailMoveToOtherFolder()throws Exception{
		mailAction.verifyMailMoveToOtherFolder();
	}
	@Test(priority = 30)
	public void verifyMarkAsImportantInCV()throws Exception{
		mailAction.verifyMarkAsImportantInCV();
	}
	@Test(priority = 31)
	public void validateReportSpamForIMAPAccounts()throws Exception{
		mailAction.validateReportSpamForIMAPAccounts();
	}
	/**---------------Android_Gmail_MailSync-----------------**/
	@Test(priority = 32)
	public void testAccountSyncWorksForGoogleAccounts()throws Exception{
		mailSync.testAccountSyncWorksForGoogleAccounts();
	}
	@Test(priority = 33)
	public void testAccountSyncWorksFornonGoogleAccounts()throws Exception{
		mailSync.testAccountSyncWorksFornonGoogleAccounts();
	}
	@Test(priority = 34)
	public void verifySwipeDownToRefreshWorks()throws Exception{
		mailSync.verifySwipeDownToRefreshWorks();
	}
	/**---------------------Search---------------------------**/
	@Test(priority = 35)
	public void verifySearchFunctionality()throws Exception{
		search.verifySearchFunctionality();
	}
	@Test(priority = 36)
	public void verifySearchFunctionalityOffline()throws Exception{
		search.verifySearchFunctionalityOffline();
	}
	/**---------------------Settings-------------------------**/
	@Test(priority = 37)
	public void enableDisableVacationResponder()throws Exception{
		settings.enableDisableVacationResponder();
	}
	@Test(priority = 38)
	public void verifySignatureOnAndOff()throws Exception{
		settings.verifySignatureOnAndOff();
	}
	@Test(priority = 39)
	public void navigateToSettings()throws Exception{
		settings.navigateToSettings();
	}
	@Test(priority = 40)
	public void verifyGeneralSettings()throws Exception{
		settings.verifyGeneralSettings();
	}
	@Test(priority = 41)
	public void verifyGmailAccountSpecificSettings()throws Exception{
		settings.verifyGmailAccountSpecificSettings();
	}
	/**------------------Rich Text Format--------------------**/
	@Test(priority = 42)
	public void richTextFormattingFeatures()throws Exception{
		richText.richTextFormattingFeatures();
	}
	/**-----------------------RSVP---------------------------**/
	@Test(priority = 43)
	public void verifyNewRSVPFormatDisplayedForCalendarInvites()throws Exception{
		rsvp.verifyNewRSVPFormatDisplayedForCalendarInvites();
	}
	/**---------------------SpamFeatures---------------------**/
	@Test(priority = 44)
	public void verifyBlockAndUnblockSenderWorks()throws Exception{
		spamfeature.verifyBlockAndUnblockSenderWorks();
	}
	@Test(priority = 45)
	public void removeUnsyncableLablesSpam()throws Exception{
		spamfeature.removeUnsyncableLablesSpam();
	}
	/**---------------------Gmailify---------------------**/
	@Test(priority = 46)
	public void verifyGmailifyFlow()throws Exception{
		gmailify.verifyGmailifyFlow();
	}
	@Test(priority = 47)
	public void verifyTryGmailifyFlow()throws Exception{
		gmailify.verifyTryGmailifyFlow();
	}
	/**------------------LauncherShortCut--------------------**/
	@Test(priority = 48)
	public void verifyLauncherShortcut()throws Exception{
		launcherShortCut.verifyLauncherShortcut();
	}
	@Test(priority = 49)
	public void verifyCreateLauncherShortcut()throws Exception{
		launcherShortCut.verifyCreateLauncherShortcut();
	}
	/**--------PicoProjectionExcelSpreadSheetViewer----------**/
	@Test(priority = 55)
	public void verifyDocumentsInApp()throws Exception{
		ppexsv.verifyDocumentsInApp();
	}
	@Test(priority = 54)
	public void verifyPicoProjectorFilePreview()throws Exception{
		ppexsv.verifyPicoProjectorFilePreview();
	}
	/**----------------ChromeCustomTabs----------------------**/
	@Test(priority = 50)
	public void verifyEnableDisableChromeTabs()throws Exception{
		ccTabs.verifyEnableDisableChromeTabs();
	}
	/**----------------Android N----------------------**/
	@Test(priority = 51)
	public void verifyMailSendMultiWindow()throws Exception{
		androidN.verifyMailSendMultiWindow();
	}
	@Test(priority = 52)
	public void verifyMultipleMailsNotificationsReplyAndArchive()throws Exception{
		androidN.verifyMultipleMailsNotificationsReplyAndArchive();
	}
	/**----------------All Ads Related----------------------**/
	@Test(priority = 53)
	public void verifyTwoAdsDisplayInPromotionSocial()throws Exception{
		allAdsRelated.verifyTwoAdsDisplayInPromotionSocial();
	}


}
