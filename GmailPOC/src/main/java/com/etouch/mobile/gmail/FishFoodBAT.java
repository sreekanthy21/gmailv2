package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class FishFoodBAT extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/**Testcase to verify whether app is able to open
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 1)
	public void launchApp()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("***********************Started testcase launchApp*****************************");
			inbox.verifyGmailAppInit(driver);
			log.info("App launched");
			inbox.verifyThreadListLoaded(driver);
			log.info("***********************Completed testcase launchApp*****************************");
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("App is unable to open " +e.getLocalizedMessage());
		}
	}
	
	/**Testcase to verify account is added and GIG is set to ON
	 * 
	 * @throws Exception
	 */
	@Test(enabled = true, priority = 2)
	public void addAccountGIG()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			inbox.verifyGmailAppInit(driver);
			Map<String, String> addGigAccount = commonPage.getTestData("GIGAccount");
			log.info("App launched");
			userAccount.verifyAccountsExists(driver, addGigAccount.get("Account"), addGigAccount.get("AcctPassword"));
			userAccount.verifyAccountsExists(driver, addGigAccount.get("Account2"), addGigAccount.get("AcctPassword"));
			
			if(!CommonFunctions.isAccountAlreadySelected(addGigAccount.get("Account")))
			userAccount.switchMailAccount(driver, addGigAccount.get("Account"));
			
			userAccount.changeToGigAccount(driver);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to find "+e.getLocalizedMessage());
		}
	}
	
	/**Testcase to verify notification received and able to read it
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	@Test(enabled = true, priority = 3)
	public void verifyNotificationEmail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
			try {
				log.info("***************** Started testcase ::: verifyNotificationReplyAndArchive ******************************");
				Map<String, String> verifyNotificationReplyAndArchiveData = commonPage
						.getTestData("verifyReplyFromNotification");
			//	userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
				Thread.sleep(3000);
				//Sending mail to open mail from notification
				userAccount.clearNotification(driver);
				
				if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("FromFieldData")))
				userAccount.switchMailAccount(driver,verifyNotificationReplyAndArchiveData.get("FromFieldData") );
				inbox.composeAndSendNewMail(driver,
						verifyNotificationReplyAndArchiveData.get("ToFieldData"),
						verifyNotificationReplyAndArchiveData.get("CC"),
						verifyNotificationReplyAndArchiveData.get("BCC"),
						verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"),
						verifyNotificationReplyAndArchiveData.get("ComposeBodyData"));
				Thread.sleep(30000);
				((AndroidDriver)driver).openNotifications();
				//commonFunction.switchOnAirPlaneMode();
				Thread.sleep(5000);
				//commonFunction.switchOffAirPlaneMode();
				 commonPage.verifySubjectIsMatching(driver,verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"));
				//commonPage.verifyOpenFromNotification(driver);
				driver.navigate().back();
			}catch(Exception e){
				
			}
	}
	
	/**
	 * @TestCase 7 : Verify Reply/Reply All mail
	 * @Pre-condition : Mail with subject "verifyReplyAllFunctionality" should
	 *                be available under Account Two.
	 * @throws: Exception
	 * GMAT 23
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 4)
	public void verifyReplyAllFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		 String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ReplyAll.bat";
		 log.info("Executing the bat file to generate a mail for reply all functionality "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed the bat file to generate a mail");
		try {
			log.info("*****************Started testcase ::: verifyReplyAllFunctionality**********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyReplyFunctionalityData = commonPage
					.getTestData("verifyReplyAllFunctionality");

			if(!CommonFunctions.isAccountAlreadySelected(verifyReplyFunctionalityData.get("AccountTwo")))
			userAccount.switchMailAccount(driver,verifyReplyFunctionalityData.get("AccountTwo"));

			inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));

			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			inbox.composeMailAsReply(
					driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"),
					verifyReplyFunctionalityData.get("AccountOne"));
			inbox.insertInLineText(driver,
					verifyReplyFunctionalityData.get("InLineData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			//inbox.openMenuDrawer(driver);
			
			//Thread.sleep(7000);
			//inbox.scrollDown(1);
			log.info("Verifying Mail is sent");
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(2000);
			userAccount.switchMailAccount(driver,verifyReplyFunctionalityData.get("AccountOne"));
			inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));

			inbox.verifyReplyInlineTextIsPresent(driver, verifyReplyFunctionalityData
					.get("InLineDataVerify"));
			inbox.composeMailAsReplyAll(driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);

			inbox.scrollDown(1);
			Thread.sleep(7000);
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(3000);
			log.info("*****************Completed testcase ::: verifyReplyAllFunctionality**********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyReplyAllFunctionality");
		}
	}

	
	/**
	 * @TestCase 8 : composeAndSendMailForGmailAccountWithAttachments	 *  
	 * @throws: Exception
	 * GMAT 10
	 */
	    @Test(groups = {"P1"}, enabled = true, priority = 5)
    	public void composeAndSendMailForGmailAccountWithAttachments() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("*****************Started testcase ::: composeAndSendMailForGmailAccountWithAttachments*****************************");
			 Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			inbox.verifyGmailAppInitState(driver);
		    inbox.composeAndSendMailForGmailAccountWithAttachments(driver);
		    
		    log.info("*****************Completed testcase ::: composeAndSendMailForGmailAccountWithAttachments*****************************");   
		    driver.navigate().back();
		}catch(Throwable e)
		{
			commonPage.catchBlock(e, driver, "verifyEditDraftAndSendMail");
		}
	}
	
	/**Testcase for Search functionality
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 6)
	public void verifySearchFunctionality() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			inbox.verifyGmailAppInitState(driver);

			log.info("*****************Started testcase ::: verifySearchFunctionality*****************************");
			SoftAssert softAssert = new SoftAssert();
			Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
			
			if(!CommonFunctions.isAccountAlreadySelected( verifySearchFunctionalityData.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifySearchFunctionalityData.get("AccountOne"));
			softAssert.assertTrue(commonFunction.isElementByIdDisplayed(driver, verifySearchFunctionalityData.get("SearchIconID")));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementById(driver, verifySearchFunctionalityData.get("SearchMailID")), verifySearchFunctionalityData.get("SearchMailText"));
			try{
				driver.hideKeyboard();
				driver.getKeyboard();
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "Keyboard not opening for verifySearchFunctionality");
			}
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityData.get("SearchMailID"), verifySearchFunctionalityData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityData.get("MailSubject"));
		//	commonFunction.navigateBackToMenuDrawer(driver);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementByXpath(driver, verifySearchFunctionalityData.get("suggestionTextPath")), verifySearchFunctionalityData.get("suggestionSearchText"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			
			log.info("*****************Completed testcase ::: verifySearchFunctionality*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionality");
		}
	}
	
	/**Testcase to Archive mail
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 52
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 7)
	public void verifyArchiveMail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("*****************Started testcase ::: verifyArchiveMail*****************************");
			
			userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			
			if(!CommonFunctions.isAccountAlreadySelected( mailMoveToOtherFolderData.get("Account")))
			userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.archiveMail(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"), null);
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"));
			
			log.info("*****************Completed testcase ::: verifyArchiveMail*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyArchiveMail");
		}
	}
	
	
}
