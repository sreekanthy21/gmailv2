package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_MailAction extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	static String path = System.getProperty("user.dir");
	String command;
	/*public void ExecuteAllMailAction()throws Exception{
		verifyTrashandSpamFolders();
		markMailReadUnread();
		verifyMailMarkedAsStar();
		changeLabels();
		verifyMailMoveToOtherFolder();
		verifyMarkAsImportantInCV();
		validateReportSpamForIMAPAccounts();
		deleteMailCheckTrash();
		verifyArchiveMail();
		verifyMailUnStar();
		reportSpam();
		verifyMailSavedAsPDFPrint();
		verifyUserIsAbleToMuteAConversation();
	}*/
	
	/**Testcase for verifying the trash and spam folders by deleting the mails
	 * 
	 * @preCondition There should exist few or more mails in Trash and Spam
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 44
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 14)
	public void verifyTrashandSpamFolders()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> mailMoveToOtherFolderData = CommonPage.getTestData("mailMoveToOtherFolder");
			inbox.verifyGmailAppInitState(driver);
			log.info("*****************Started testcase ::: verifyTrashandSpamFolders*****************************");
			//userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			//userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
			//Ensure all items in Trash folder are deleted
			
			String primaryAccountNew = CommonFunctions.getNewUserAccount(mailMoveToOtherFolderData, "Account", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(primaryAccountNew))
				userAccount.switchMailAccount(driver, primaryAccountNew); 

			
			log.info("************Verifying the Trash folder functionality*************");
			navDrawer.navigateToTrashFolder(driver);
			CommonFunctions.smtpMailGeneration(primaryAccountNew, "TestingDeleteMail", "ZoomOut.html", "");
			userAccount.verifyTrashFolderForMails(driver);
			inbox.emptyTrashOrSpamNow(driver, "Trash");
			log.info("*********Verified all mails are deleted from Trash folder**************");

			//Ensure all items in Spam folder are deleted
			log.info("*********Verifying the Spam folder functionality***********");
			navDrawer.navigateToSpamFolder(driver);
			userAccount.verifySpamFolderForMails(driver, primaryAccountNew);
			inbox.emptyTrashOrSpamNow(driver,"Spam");
			log.info("*********Verified all mails are deleted from Spam folder**************");
			driver.navigate().back();
			
			
			log.info("*****************Completed testcase ::: verifyTrashandSpamFolders*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyTrashandSpamFolders");
		}
	}

	/**
	 * @TestCase 18 :markMailReadUnread /**
	 * @TestCase 3 : markMailReadUnread
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 45
	 */	 
	@Test(groups = {"P1"}, enabled = true, priority = 12)
    public void markMailReadUnread() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: markMailReadUnread*************************");
			
			inbox.verifyGmailAppInitState(driver);
			inbox.markMailReadUnread(driver);
			
			log.info("*****************Completed testcase ::: markMailReadUnread*****************************");
			} catch (Throwable e) {
				commonPage.catchBlock(e, driver, "markMailReadUnread");			
			}
	}
	

	/**Testcase to verify when the mail is marked as star
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 46
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 10)
	public void verifyMailMarkedAsStar()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("***************** Started testcase ::: verifyMailMarkedAsStar ******************************");

			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			
			inbox.verifyStarredMail(driver);
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyMailMarkedAsStar*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailMarkedAsStar");
		}
	}

	/**
	 * @TestCase 19 :unStarMail /**
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 53
	 */	
	@Test(groups = {"P1"}, enabled = true, priority = 11)
	public void verifyMailUnStar() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase :::  unStarMail*************************");
			//Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");

			inbox.pullToReferesh(driver);
			//inbox.verifyGmailAppInitState(driver);
			//navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.verifyUnStarredMail(driver);
			
			log.info("*****************Completed testcase :::  unStarMail*****************************");
	} catch (Throwable e) {
		commonPage.catchBlock(e, driver, "unStarMail");	
					}
	}

	/**
	 * @TestCase 13 changeLabels**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 47
	 */	 
	@Test(groups = {"P1"}, enabled = true, priority = 15)
    public void changeLabels() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			 log.info("*****************Started testcase ::: changeLabels*************************");
			 Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = CommonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			 Map<String, String> mailMoveToOtherFolderData = CommonPage.getTestData("mailMoveToOtherFolder");

			//String emailId = mailMoveToOtherFolderData.get("Account");
			String emailId = CommonFunctions.getNewUserAccount(composeMailWithMultipleAttachmentsAndAnsertedDriveChips, "ToFieldAttachments", null);
			
			 inbox.verifyGmailAppInitState(driver);
			 if(!CommonFunctions.isAccountAlreadySelected(emailId))
			 userAccount.switchMailAccount(driver, emailId); 
			/*
			 * command =
			 * path+java.io.File.separator+"Mail"+java.io.File.separator+"ChangeLabel.bat";
			 * Process p = new ProcessBuilder(command).start();
			 * log.info("Executed Bat file");
			 */			 
			 Thread.sleep(4000);
				navDrawer.openPrimaryInbox(driver, emailId);

			/* userAccount.verifyAccountsExists(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"), 
					 composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AcctPwd"));*/
			 inbox.changeLabels(driver);
			// inbox.changeLabelsWeb(driver);
			 log.info("*****************Completed testcase ::: changeLabels*****************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "changeLabels");
		}
	}
	
	/**Testcase to Move mail to other folder
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 48
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 16)
	public void verifyMailMoveToOtherFolder()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = CommonPage.getTestData("mailMoveToOtherFolder");
			log.info("*****************Started testcase ::: verifyMailMoveToOtherFolder*****************************");
			
			String emailId = CommonFunctions.getNewUserAccount(mailMoveToOtherFolderData, "Account", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId); 
			navDrawer.openPrimaryInbox(driver, emailId);
			inbox.moveMailToFolder(driver, mailMoveToOtherFolderData.get("EmailSubject"), mailMoveToOtherFolderData.get("FolderName"), emailId);
			navDrawer.navigateToSocial(driver);
			inbox.searchForMail(driver, mailMoveToOtherFolderData.get("EmailSubject")); // Added by Venkat on 10/10/2019
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubject"));
			inbox.moveMailToPrimary(driver, mailMoveToOtherFolderData.get("EmailSubject"), mailMoveToOtherFolderData.get("PrimaryFolder"));

			log.info("*****************Completed testcase ::: verifyMailMoveToOtherFolder*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailMoveToOtherFolder");
		}
	}
	
	/**
	* @TestCase 68 : Verify all elements in the notification
	* @Pre-condition : 1. Gmail app installed
                       2. Gmail account is added to app
                       3. Single Message sent from another account/device
	*  @author batchi/Phaneendra
	* @throws: Exception
	* GMAT 49
	*/
	@Test(groups = {"P1"}, enabled=true, priority=17)
	public void verifyMarkAsImportantInCV() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			 log.info("***************** Started testcase ::: verifyMarkAsImportantInCV ******************************");
			 Map<String, String> mailMoveToOtherFolderData = CommonPage.getTestData("mailMoveToOtherFolder");
			 inbox.verifyGmailAppInitState(driver);
			/*
			 * command = path+java.io.File.separator+"Mail"+java.io.File.separator+
			 * "MarkMailImportant.bat"; Process p = new ProcessBuilder(command).start();
			 * log.info("Executed Bat file");
			 */			
				String emailId = CommonFunctions.getNewUserAccount(mailMoveToOtherFolderData, "Account", null);
				String emailSubject = mailMoveToOtherFolderData.get("MailImportant");
				CommonFunctions.smtpMailGeneration(emailId, emailSubject, "verifyMailSnippets.html", null);


			 Thread.sleep(4000);
			 //userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			 navDrawer.openPrimaryInbox(driver,emailId);
			
			 inbox.verifyMarkAsImportant(driver, mailMoveToOtherFolderData.get("MailImportant"));
			 driver.navigate().back();
			 log.info("*****************Completed testcase ::: verifyMarkAsImportantInCV*************************");
			}catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyMarkAsImportantInCV");
	}
	}
	
	/**
	 * @TestCase 12 validateReportSpamForIMAPAccounts**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 50
	 */	 
   @Test(groups = {"P1"}, enabled = true, priority = 94)
   public void validateReportSpamForIMAPAccounts() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
		   	 log.info("*****************Started testcase ::: validateReportSpamForIMAPAccounts*************************");
			 
		   	 inbox.verifyGmailAppInitState(driver);
			 inbox.validateReportSpamForIMAPAccounts(driver);
			 
			 log.info("*****************Completed testcase ::: validateReportSpamForIMAPAccounts*****************************"); 
			 driver.navigate().back();
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "validateReportSpamForIMAPAccounts");
		}
   }
   
   /**Testcase to Delete mail and check in Trash
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 51
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 13)
	public void deleteMailCheckTrash() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> deleteMailCheckTrashData = commonPage.getTestData("deleteMailCheckTrash");
			log.info("*****************Started testcase ::: deleteMailCheckTrash*****************************");
			
		//	userAccount.switchMailAccount(driver, deleteMailCheckTrashData.get("Account"));
			
			//String accountOne = deleteMailCheckTrashData.get("Account");
			String accountOne = CommonFunctions.getNewUserAccount(deleteMailCheckTrashData, "Account", null);
			if(!CommonFunctions.isAccountAlreadySelected(accountOne))
				userAccount.switchMailAccount(driver, accountOne);
			navDrawer.openPrimaryInbox(driver, accountOne);
			
			inbox.deleteMailPrimary(driver, deleteMailCheckTrashData.get("MailSubject"), accountOne);
			inbox.verifySnackBarDeleted(driver);
			
			navDrawer.navigateToTrashFolder(driver);
			inbox.verifyMailTrashFolder(driver, deleteMailCheckTrashData.get("MailSubject"));
			log.info("*****************Completed testcase ::: deleteMailCheckTrash*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "deleteMailCheckTrash");
		}
	}
   
   /**Testcase to Archive mail
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 52
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 9)
	public void verifyArchiveMail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = CommonPage.getTestData("mailMoveToOtherFolder");
			
			//String primaryAccountNew = mailMoveToOtherFolderData.get("Account");
			String primaryAccountNew = CommonFunctions.getNewUserAccount(mailMoveToOtherFolderData, "Account", null);
			
			log.info("*****************Started testcase ::: verifyArchiveMail*****************************");
			if(!CommonFunctions.isAccountAlreadySelected(primaryAccountNew))
			userAccount.switchMailAccount(driver,primaryAccountNew); // enabled by Venkat on 15/02/2019
			navDrawer.openPrimaryInbox(driver,primaryAccountNew);
			inbox.archiveMail(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"), primaryAccountNew);
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"));
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyArchiveMail*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyArchiveMail");
		}
	}

	
   /**
	 * @TestCase 11 Report Spam**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 54
	 */	 
	@Test(groups = {"P2"}, enabled = true, priority = 18)
    public void reportSpam() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			 log.info("*****************Started testcase ::: reportSpam*************************");
			 
			 inbox.verifyGmailAppInitState(driver);
			 /*
			 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ReportSpamGmail.bat";
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file");
			 */
			 Thread.sleep(4000);
			 inbox.reportSpam(driver);
			 
			 log.info("*****************Completed testcase ::: reportSpam*****************************");
			 driver.navigate().back();
			 driver.navigate().back();

		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "reportSpam");
		}	
	}
	
	/**Testcase to Print/Print All mail and save as PDF
	 * 
	 * @preCondition Gmail account is added to app
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 55
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 19)
	public void verifyMailSavedAsPDFPrint()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			Map<String, String> mailSaveAsPdfData = commonPage.getTestData("mailSaveAsPdf"); 
			log.info("*****************Started testcase ::: verifyMailSavedAsPDF*****************************");
			
		//	userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
			inbox.verifyMailSaveAsPDF(driver, mailSaveAsPdfData.get("FileName"), mailSaveAsPdfData.get("Kind"));
			
			log.info("*****************Completed testcase ::: verifyMailSavedAsPDF*************************");
			driver.navigate().back();
			/*driver.navigate().back();
			driver.navigate().back();*/
			
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailSavedAsPDF");
		}
	}

	/**
	 * @TestCase 16 verifyUserIsAbleToMuteAConversation**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 * @author Rahul
	 * GMAT 56
	 */	
	   @Test(groups = {"P2"}, enabled = true, priority = 20)
	   public void verifyUserIsAbleToMuteAConversation() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			try {
				log.info("*****************Started testcase ::: verifyUserIsAbleToMuteAConversation*************************");
				
				inbox.verifyGmailAppInitState(driver);
		        inbox.verifyUserIsAbleToMuteAConversation(driver);
		    
				log.info("*****************Completed testcase ::: verifyUserIsAbleToMuteAConversation*****************************");
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "verifyUserIsAbleToMuteAConversation");			
			}
			}
	   /**
		 * @TestCase 16 verify able to moveTo IMAP from  TL  view**
		 *
		 * @Pre-condition : Gmail build 6.0+
		 * @throws: Exception
		 * @author batchi
		 * GMAT 56
		 */	
		   @Test(groups = {"P2"}, enabled = true, priority = 20)
		   public void verifyIMAPMailActionsTL() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
				try {
					log.info("*****************Started testcase ::: verify able to move to from TL view*************************");
				/*	
					String path = System.getProperty("user.dir");
					String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"IMAPMailActionsTL.bat";
				    log.info("Executing the bat file for imp and unimp mails "+command);
					// Process p = new ProcessBuilder(command).start();
					Runtime.getRuntime().exec(command);
					log.info("Executed Bat file");
				*/
					String  emailId = CommonFunctions.getNewUserAccount(verifySendMailIMAPData, "OutlookEmailAddress", "IMAP");
					CommonFunctions.smtpMailGeneration(emailId,"VerifyMailActionsTL", "Test", null);
					Thread.sleep(3000);
					if(!CommonFunctions.isAccountAlreadySelected(emailId))
						userAccount.switchMailAccount(driver,emailId);

					//Thread.sleep(10000);////commented by Venkat on 15/02/2013
					//userAccount.switchMailAccount(driver, verifySendMailIMAPData.get("OutlookEmailAddress"));
					UserAccount.pullToReferesh(driver); //added by Venkat on 15/02/2013
					Thread.sleep(3000);//added by Venkat on 15/02/2013
					inbox.verifyIMAPMoveToDelTLview(driver);
					inbox.verifyIMAPMarkReadAddStarTLview(driver);
					
					log.info("*****************Completed testcase ::: verify able to move to from TL view*****************************");
				}
				catch (Throwable e) {
					commonPage.catchBlock(e, driver, "verifyUserIsAbleToMuteAConversation");			
				}
			}
		   /**
			 * @TestCase 13 Verify able to edit IMAP account settings			 *
			 * @Pre-condition :  IMAP account is added
			 * @throws: Exception
			 * @author Santosh
			 * GMAT
			 */	 
			@Test(groups = {"P1"}, enabled = true, priority = 00)
		    public void verifyAbleToEditIMAPAccountSettings() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				try {
					 log.info("*****************Started testcase ::: verifyAbleToEditIMAPAccountSettings*************************");
					 Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
					 inbox.verifyGmailAppInitState(driver);
					
					 inbox.AbleToEditIMAPAccountSettings(driver);
					 log.info("*****************Completed testcase ::: verifyAbleToEditIMAPAccountSettings*****************************");
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "verifyAbleToEditIMAPAccountSettings");
				}
			}
			/**
			 * @TestCase  Label notification is enabled but Inbox sync is disabled
   		 * @Pre-condition : Gmail build 6.0+
			 * @throws: Exception
			 * @author Santosh
			 * GMAT
			 */	 
			@Test(groups = {"P1"}, enabled = true, priority = 15)
		    public void labelNotificationEnabledAndInboxSyncDisabled() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				try {
					 log.info("*****************Started testcase ::: labelNotificationEnabledAndInboxSyncDisabled*************************");
					 Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
					 inbox.verifyGmailAppInitState(driver);
					 userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
					 inbox.labelNotificationEnabledAndInboxSyncDisabled(driver);
					 log.info("*****************Completed testcase ::: labelNotificationEnabledAndInboxSyncDisabled*****************************");
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "labelNotificationEnabledAndInboxSyncDisabled");
				}
			}
			/**
			 * @TestCase 13 Manage labels and enable label notifications
			 * @Pre-condition : Gmail build 6.0+
			 * @throws: Exception
			 * @author Santosh
			 * GMAT 47
			 */	 
			@Test(groups = {"P1"}, enabled = true, priority = 00)
		    public void manageLabelsAndEnableLabelNotifications() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				try {
					 log.info("*****************Started testcase ::: manageLabelsAndEnableLabelNotifications*************************");
					 Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
					 inbox.verifyGmailAppInitState(driver);
					 userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
					 Thread.sleep(1000);
					 inbox.manageLabelsAndEnableLabelNotifications(driver);
					 log.info("*****************Completed testcase ::: manageLabelsAndEnableLabelNotifications*****************************");
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "manageLabelsAndEnableLabelNotifications");
				}
			}
			  /**Testcase to Verify able to archive and mute from search(Gmail only)
			 *
			 * @preCondition Gmail account is added to app,Remove Mails with Subject "muteFromSearch"
			 * @throws Exception
			 * @author Santosh
			 * GMAT
			 */
			@Test(groups = {"P2"}, enabled = true, priority = 0)
			public void verifyAbleToArchiveAndMuteFromSearch()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				
				try{
					Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
					log.info("*****************Started testcase ::: verifyAbleToArchiveAndMuteFromSearch*****************************");
					
					//inbox.AbleToArchiveAndMuteFromSearch(driver, mailMoveToOtherFolderData.get("archiveFromSearch"));
					//navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("Account"));
					inbox.verifyUserIsAbleToMuteFromSearch(driver,mailMoveToOtherFolderData.get("muteFromSearch"));
				
					driver.navigate().back();
					log.info("*****************Completed testcase ::: verifyAbleToArchiveAndMuteFromSearch*************************");
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyAbleToArchiveAndMuteFromSearch");
				}
			}
/** @TestCase Undo delete/archive when auto-advance is set to older.
 * @throws: Exception
* @author batchi
 */	
@Test(groups = {"P0"}, enabled = true, priority = 6)
public void undoDeleteArchiveAutoAdvanceOlder() throws Exception {
	driver = map.get(Thread.currentThread().getId());
	try{
  Map < String, String > undoDeleteData = commonPage.getTestData("VerifyUndo");
//  String  gigMailId = CommonFunctions.getNewUserAccount(undoDeleteData, "account1", null);
//  userAccount.switchMailAccount(driver, gigMailId);
  log.info("*****************Started testcase ::: undoDeleteArchiveAutoAdvanceOlder *****************************");
  commonPage.launchGmailApp(driver);
  log.info("Performing Delete action and undo");
  inbox.undoRestoresAutoAdvanceOlder(driver,undoDeleteData.get("MailToolBarDeleteIcon"),"Delete");
  Thread.sleep(2000);
  log.info("Performing Archive action and undo");
  inbox.undoRestoresAutoAdvanceOlder(driver,undoDeleteData.get("MailToolBarArchiveIcon"),"Archive");
  log.info("*****************Completed testcase ::: undoDeleteArchiveAutoAdvanceOlder *************************");
  }catch(Throwable e){
		commonPage.catchBlock(e, driver, "undoDeleteArchiveAutoAdvanceOlder");
	}
}
}
