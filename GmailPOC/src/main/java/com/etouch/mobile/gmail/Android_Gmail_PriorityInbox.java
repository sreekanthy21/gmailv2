package com.etouch.mobile.gmail;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_PriorityInbox extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	Process p;
    String path = System.getProperty("user.dir");

	
	public void addAccounts()throws Exception{
		String[] launchOldApk = new String[]{"adb","install", "-r","-d" ,path+java.io.File.separator+"Gmail_Apk"+java.io.File.separator+"GoogleAccountManagerApp.apk"};
	    p = new ProcessBuilder(launchOldApk).start();
	    log.info("Installing the google account manager apk");
	    Thread.sleep(10000);
		
	    String[] addgig1auto = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig1.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync", "true",
	    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
	    p = new ProcessBuilder(addgig1auto).start();
	    log.info("Installing account gm.gig1.auto with google account manager apk");
	    Thread.sleep(10000);
		
	    String[] addgig2auto = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "gm.gig2.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync", "true",
	    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
	    p = new ProcessBuilder(addgig2auto).start();
	    log.info("Installing account gm.gig2.auto with google account manager apk");
	    Thread.sleep(10000);
	}
	
	public void NavigateAcctSettings(){
		try{
			
		}catch(Exception e){
			throw e;
		}
	}
	
	/**-- Verify Inbox Type settings in Account settings
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 49)
	public void verifyInboxTypesSettings() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
		 String primayAccount = null;
		try {
			
			log.info("********Started testcase ::: verifyInboxSettings*******************************");
			//addAccounts();
			inbox.verifyGmailAppInitState(driver);
			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto"))) {
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			//}
			primayAccount = verifyPI.get("Gig4_Auto");
			if(!CommonFunctions.isAccountAlreadySelected(primayAccount)) {
				userAccount.switchMailAccount(driver,primayAccount);
			}

			userAccount.openUserAccountSetting(driver,primayAccount);
			
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("Importantfirst"));
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				UserAccount.openMenuDrawer(driver);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Inbox"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Important"));
				userAccount.clickOrScrollAndClick(driver, "Important", verifyPI.get("Important"));
			}
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Important"))!=0){
				log.info("Important Page title has been displayed");
			}else {
				throw new Exception("Important Page title has NOT been displayed");
			}
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifyInboxSettings*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyInboxSettings");
		}
	}
	
	/**-- Selected PI option is remembered
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 50)
	public void verifyPIOptionRemembered() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
		 String primayAccount = null;
		try {
			log.info("********Started testcase ::: verifyPIOptionRemembered*******************************");

			inbox.verifyGmailAppInitState(driver);

			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto"))) {
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			//}
			
			primayAccount = verifyPI.get("Gig4_Auto");
			if(!CommonFunctions.isAccountAlreadySelected(primayAccount)) {
				userAccount.switchMailAccount(driver,primayAccount);
			}

			userAccount.openUserAccountSetting(driver,primayAccount);
			//userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("Importantfirst"));
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				log.info("Inbox Title displayed");
				UserAccount.openMenuDrawer(driver);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Inbox"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Important"));
			}
			CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("Important"));
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Important"))!=0){
				log.info("Important Title displayed");
				driver.closeApp();
				driver.launchApp();
				inbox.verifyGmailAppInit(driver);
				if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Important"))!=0){
					log.info("Last remembered setting displayed as Important");
				}else{
					throw new Exception("Last remembered option is not displayed");
				}
			}
			
			log.info("*****************Completed testcase ::: verifyPIOptionRemembered*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPIOptionRemembered");
		}
	}

	/**-- Selected Priority Inbox is remembered after switching accounts
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 51)
	public void verifyPIOptionRememberedSwitching() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
		 String primayAccount = null;
		try {
			log.info("********Started testcase ::: verifyPIOptionRememberedSwitching*******************************");

			inbox.verifyGmailAppInitState(driver);
			
			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto"))) {
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			//}
			//userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			
			primayAccount = verifyPI.get("Gig4_Auto");
			if(!CommonFunctions.isAccountAlreadySelected(primayAccount)) {
				userAccount.switchMailAccount(driver,primayAccount);
			}
			userAccount.openUserAccountSetting(driver, primayAccount);
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("PriorityInbox"));
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				log.info("Inbox Title displayed");
				UserAccount.openMenuDrawer(driver);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Inbox"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("ImportantUnread"));
				//CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Starred"));
				userAccount.clickOrScrollAndClick(driver, "Starred", verifyPI.get("Starred"));
				driver.navigate().back();
			}
			UserAccount.openMenuDrawer(driver);
			userAccount.clickOrScrollAndClick(driver, "Important and unread",verifyPI.get("ImportantUnread"));
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed");
			}else{
				throw new Exception("Important and unread title is not displayed");
			}
			UserAccount.openMenuDrawer(driver);
			CommonFunctions.scrollsToTitle(driver, "All inboxes");
			Thread.sleep(15000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("AllInboxes"))!=0){
				log.info("All inboxes Title displayed");
			}else{
				throw new Exception("All inboxes title is not displayed");
			}
			driver.closeApp();
			driver.launchApp();
			inbox.verifyGmailAppInit(driver);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed");
			}else{
				throw new Exception("Important and unread title is not displayed");
			}
			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig2Auto"))) {
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig2Auto"));
			//}
			userAccount.switchMailAccount(driver, verifyPI.get("Gig3_Auto"));
			
			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto"))){
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			//}
			userAccount.switchMailAccount(driver,primayAccount);
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed after switching account");
			}else{
				throw new Exception("Important and unread Title is not displayed after switching account");
			}
			log.info("*****************Completed testcase ::: verifyPIOptionRememberedSwitching*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPIOptionRememberedSwitching");
		}
	}
	
	/**-- Verify Exit app by using device back key after selecting any of PI sections
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 52)
	public void verifyExitAppPISection() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
		 String primayAccount = null;
		try {
			log.info("********Started testcase ::: verifyExitAppPISection*******************************");

			inbox.verifyGmailAppInitState(driver);
			
			//if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto"))) {
				//userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			//}
			//userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			
			primayAccount = verifyPI.get("Gig4_Auto");
			if(!CommonFunctions.isAccountAlreadySelected(primayAccount)) {
				userAccount.switchMailAccount(driver,primayAccount);
			}
			userAccount.openUserAccountSetting(driver, primayAccount);
			
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("PriorityInbox"));
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				log.info("Inbox Title displayed");
				UserAccount.openMenuDrawer(driver);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Inbox"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("ImportantUnread"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Starred"));
			}
			CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("ImportantUnread"));
			Thread.sleep(5000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed");
			}else{
				throw new Exception("Important and unread title is not displayed");
			}
			driver.navigate().back();
			Thread.sleep(1000);
			driver.navigate().back();
			driver.launchApp();
			inbox.verifyGmailAppInitState(driver);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				log.info("Inbox Title displayed");
			}else{
				throw new Exception("Inbox setion is not opened after exiting app");
			}
			log.info("*****************Completed testcase ::: verifyExitAppPISection*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyExitAppPISection");
		}
	}
 
	/**-- Selected Priority Inbox is remembered after opening from notification
	 * 
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 53)
	public void verifyPIAfterNotification() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = commonPage
					.getTestData("AutoSync");
		try {
			log.info("********Started testcase ::: verifyPIAfterNotification*******************************");

			inbox.verifyGmailAppInitState(driver);
			if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto")))
			userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			userAccount.pullToReferesh(driver);//added by Venkat on 15/11/2018
			userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("PriorityInbox"));
			driver.navigate().back();
			driver.navigate().back();
			Thread.sleep(6000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("Inbox"))!=0){
				log.info("Inbox Title displayed");
				UserAccount.openMenuDrawer(driver);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Inbox"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("ImportantUnread"));
				CommonPage.waitForElementToBePresentByXpath(verifyPI.get("Starred"));
			}
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("ImportantUnread"));
			//Thread.sleep(3000);
			Thread.sleep(5000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed");
			}else{
				userAccount.openMenuDrawer(driver);
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("ImportantUnread"));
				Thread.sleep(5000);
				if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
					log.info("Important and unread Title displayed");
				}else{
					throw new Exception("Important and unread title is not displayed");
				}
			}
			/*
			String command4 = path + java.io.File.separator + "Mail" + java.io.File.separator + "NotificationGig2.bat";
	        p = new ProcessBuilder(command4).start();
	        log.info("Executed the bat file to generate a mail");
	        */
			
			CommonFunctions.smtpMailGeneration("gm.gig2.auto@gmail.com", "NotificationGig2", "verifyMailSnippets.html", null);
	        userAccount.openUserAccountSetting(driver, verifyPI.get("Gig2Auto"));
	        CommonFunctions.scrollTo(driver, "Images");
	        CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("SyncGmail"));
	        Thread.sleep(1000);
	        CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("SyncGmail"));
	        
//	        CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
	        Thread.sleep(6000);
	        String[] checkNotification;
	        checkNotification = new String[]{"adb", "shell", "service", "call", "statusbar", "1"};
		    p = new ProcessBuilder(checkNotification).start();
((AndroidDriver)driver).openNotifications();
		    log.info("Status bar expanded");
		    Thread.sleep(6000);
		    if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("NotificationGig2"))!=0){
		    	log.info("Notification received and tapping on it");
		    }else{
		    	log.info("Notification not received and syncing the gmail again");
		    	checkNotification = new String[]{"adb", "shell", "service", "call", "statusbar", "2"};
		    	Thread.sleep(800);
		    	driver.navigate().back();
		    	Thread.sleep(600);
//		    	CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
//		        CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
		    
		    	CommonFunctions.scrollTo(driver, "Images");
		        CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("SyncGmail"));
		        Thread.sleep(1000);
		        CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("SyncGmail"));
		        
		    	Thread.sleep(6000);
		        checkNotification = new String[]{"adb", "shell", "service", "call", "statusbar", "1"};
			    p = new ProcessBuilder(checkNotification).start();
			    log.info("Status bar expanded");
			    Thread.sleep(1500);
		    }
	    	CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("NotificationGig2"));
	    	//Thread.sleep(800);
	    	Thread.sleep(1500);
	    	driver.navigate().back();
	    	if(!CommonFunctions.isAccountAlreadySelected(verifyPI.get("Gig1Auto")))
	    	userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
	    	Thread.sleep(1500);
	    	if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
				log.info("Important and unread Title displayed");
			}else{
				throw new Exception("Important and unread title is not displayed");
			}
	        
	        
			log.info("*****************Completed testcase ::: verifyPIAfterNotification*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPIAfterNotification");
		}
	}

	/**
	*  @author Venkat
	**/

	@Test(groups = {"P0"}, enabled = true, priority = 52)
	public void switchingAccountsPI() throws Exception {
		try {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
			log.info("********Started testcase ::: switchingAccountsPIExploratory*******************************");

			String primayAcc = verifyPI.get("Gig1Auto");
			String secondaryAcc = verifyPI.get("Gig2Auto");
			String unreadXPath =  verifyPI.get("UnreadXPath");
			String inboxXPath =  verifyPI.get("Inbox");
			
			CommonFunctions.performAction(driver, primayAcc);
			if(!CommonFunctions.isAccountAlreadySelected(secondaryAcc))
			userAccount.switchMailAccount(driver, secondaryAcc);
			Thread.sleep(1000);
			
			if(!CommonFunctions.isAccountAlreadySelected(primayAcc))
			userAccount.switchMailAccount(driver, primayAcc);
			Thread.sleep(1000);

			CommonFunctions.verifyLable(driver, "UNREAD", unreadXPath);
			userAccount.navigateBackNTimesNew(driver, 1);
			CommonFunctions.verifyLable(driver, "INBOX", inboxXPath);
			
			String currentAccName = userAccount.getSelectedAccountName(driver);
			if(!currentAccName.equalsIgnoreCase(primayAcc)) {
				throw new Exception("Primary account:"+primayAcc+" not matched with current account :"+currentAccName);	
			}
			log.info("Account has been verified successfully...");
			Thread.sleep(1000);
			userAccount.navigateBackNTimesNew(driver, 1);

			log.info("*****************Completed testcase ::: switchingAccountsPIExploratory*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPIOptionRememberedSwitching");
		}
	}
		/**
	*  @author Venkat
	**/

	@Test(groups = {"P0"}, enabled = true, priority = 53)
	public void switchingAccountsAfterChangePITypes() throws Exception {

		try {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> autoSyncData = CommonPage.getTestData("AutoSync");
			Map<String, String> priorityInboxData = CommonPage.getTestData("PriorityInbox");
			log.info("********Started testcase ::: switchingAccountsAfterChangePITypes*******************************");

			String primayAcc = autoSyncData.get("Gig1Auto");
			String secondaryAcc = autoSyncData.get("Gig2Auto");
			String unreadXPath =  autoSyncData.get("UnreadXPath");
			String importantXPath =  autoSyncData.get("Important");
			String inboxTypeXPath = priorityInboxData.get("InboxType");
			String importantFirstXPath = priorityInboxData.get("ImportantFirst");
			
			CommonFunctions.performAction(driver, primayAcc);
			CommonFunctions.verifyLable(driver, "UNREAD", unreadXPath);
			log.info("UNREAD inbox type displayed properly..");
			
			if(!CommonFunctions.isAccountAlreadySelected(secondaryAcc))
			userAccount.switchMailAccount(driver, secondaryAcc); // #3
			Thread.sleep(1000);

			userAccount.openUserAccountSettingNew(driver, primayAcc);
			userAccount.clickOrScrollAndClick(driver, "Inbox type",inboxTypeXPath);
			Thread.sleep(200);

			CommonFunctions.searchAndClickByXpath(driver, importantFirstXPath);
			userAccount.navigateBackNTimesNew(driver, 2);
			Thread.sleep(1000);
			
			if(!CommonFunctions.isAccountAlreadySelected(primayAcc))
			userAccount.switchMailAccount(driver, primayAcc);
			userAccount.openMenuDrawerNew(driver);
			userAccount.clickOrScrollAndClick(driver, "Unread",importantXPath);
			
			CommonFunctions.verifyLable(driver, "IMPORTANT", importantXPath);
		
			log.info("*****************Completed testcase ::: switchingAccountsAfterChangePITypes*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPIOptionRememberedSwitching");
		}
	}

	public void piNotificationWhenDisabled() throws Exception {
		try {
			driver = map.get(Thread.currentThread().getId());
			Map<String, String> verifyPI = CommonPage.getTestData("AutoSync");
			Map<String, String> priorityInboxData = CommonPage.getTestData("PriorityInbox");
			String priorityInbox = priorityInboxData.get("PriorityInboxXPath");
			String turnOffXPath = priorityInboxData.get("TurnOff");
			String notificationNoneXPath = priorityInboxData.get("NotificationNone");
			String notificationsXPath = priorityInboxData.get("NotificationsXPath");
			
			log.info("********Started testcase ::: piNotificationWhenDisabled*******************************");

			String primayAcc = verifyPI.get("Gig1Auto");
			CommonFunctions.priorityInboxChange(driver, primayAcc, priorityInbox);
			userAccount.clickOrScrollAndClick(driver, "Notifications",notificationsXPath);
			Thread.sleep(200);
			CommonFunctions.searchAndClickByXpath(driver, notificationNoneXPath);
			try {
				driver.findElement(By.xpath(turnOffXPath)).click();
			} catch (Exception e) {
			}
			
			Thread.sleep(1000);
			userAccount.navigateBackNTimesNew(driver, 2);
			try {
				driver.runAppInBackground(12);
			} catch (Exception e) {
			}
			try {
				((AndroidDriver)driver).openNotifications();
				CommonPage.clearAllNotifications(driver);
			} catch (Exception e) {
				log.info("No notifications exist to clear..");
			}
			userAccount.mailSend("piNotification.bat");
			Thread.sleep(5000);
			((AndroidDriver)driver).openNotifications();
			Thread.sleep(1000);
			UserAccount.pullToReferesh(driver);
			Thread.sleep(5000);
			String actionLableMailXpath = inbox.findMailInPrimaryFolder(driver, "PINotification1");
			Thread.sleep(1000);
			actionLableMailXpath = inbox.findMailInPrimaryFolder(driver, "PINotification2");
			Thread.sleep(1000);
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "piNotificationWhenDisabled");
		}
	}
	/**-- Verify PI notification when Enabled
	 *
	 * @throws Exception
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 53)
	public void verifyPINotificationWhenEnabled() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		 Map<String, String> verifyPI = commonPage
					.getTestData("AutoSync");
		 Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		try {
			log.info("********Started testcase ::: verifyPINotificationWhenEnabled*******************************");

			inbox.verifyGmailAppInitState(driver);
			userAccount.switchMailAccount(driver, verifyPI.get("Gig1Auto"));
			userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			inbox.verifyInboxTypes(driver);
			CommonFunctions.scrollsToTitle(driver, verifyPI.get("PriorityInbox"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("NotifyForInboxSections"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Importantsectiononly"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxImportantSection"));
			Thread.sleep(800);
			labelSyncConditions();
			Thread.sleep(800);
			//Added if condition and else script as the existing code is not working for all devices  @author batchi
			if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Vibratecheckbox"))!=0){
				 String checkVibrateNoti=driver.findElement(By.xpath(navigateToSettings.get("Vibratecheckbox"))).getAttribute("checked");
				 if(checkVibrateNoti.equalsIgnoreCase("true"))
				{
					 log.info("Vibrate Notifications are enabled");
			    }else {
			    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("Vibratecheckbox"));
			    	 log.info("Vibrate Notification checked...");
			    }
			}else{
				driver.navigate().back();
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("ManageNotification"));
				String vibr = CommonFunctions.searchAndGetTextOnElementByXpath(driver, navigateToSettings.get("VibrateOptionID"));
				if(navigateToSettings.get("VibrateOptionText").equals(vibr)){
					String vibToggle = CommonFunctions.searchAndGetTextOnElementByXpath(driver, navigateToSettings.get("VibrateSwitch"));
					if(navigateToSettings.get("VibrateSwitchStatus").equals(vibToggle)){
						log.info("Vibrate toggle is OFF");
						CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("VibrateOptionID"));
					}else{
						log.info("Vibrate toggle is already ON");
					}
				}else{
					throw new  Exception("Vibrate option is not available");
				}
			}
		   	driver.navigate().back();
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Managelables"));
		    Thread.sleep(500);
		    CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelInbox"));
		    Thread.sleep(800);
		    labelSyncConditions();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			// need to send two mails//PINotificationEnabled
		    userAccount.switchMailAccount(driver, navigateToSettings.get("AccountOne"));
			String command = path + java.io.File.separator + "Mail" + java.io.File.separator +"MailAlert"+ java.io.File.separator + "PINotificationEnabled.bat";
		        Runtime.getRuntime().exec(command);
		        log.info("Executed the bat file to generate a mail"+command);
			Thread.sleep(6000);
			userAccount.openMenuDrawer(driver);
			navDrawer.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(600);
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("AcceptOk"));
			Thread.sleep(1000);
			CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
			Thread.sleep(5000);
			driver.navigate().back();
			Thread.sleep(2000);
			((AndroidDriver)driver).openNotifications();
		     Thread.sleep(3000);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("PiNotificationVerification"));
			 Thread.sleep(1000);
			 driver.navigate().back();
           // Verify the active folder is Important and unread or not --Bindu
			 Thread.sleep(600);
			// userAccount.openMenuDrawer(driver);
			 if(CommonFunctions.getSizeOfElements(driver, verifyPI.get("ImportantUnread"))!=0){
					log.info("Important and unread Title verified");
				}else{
					throw new Exception("Important and unread title is not verified");
				}
			 Thread.sleep(1000);
			 driver.navigate().back();
			 userAccount.openUserAccountSetting(driver, verifyPI.get("Gig1Auto"));
			 Thread.sleep(1000);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("NotifyForInboxSections"));
			 Thread.sleep(800);
			 String checkAllNewMail=driver.findElement(By.xpath(navigateToSettings.get("AllNewMail"))).getAttribute("checked");
			 if(checkAllNewMail.equalsIgnoreCase("true"))
			{
				 log.info("Vibrate Notifications are enabled");
		    }else {
		    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("AllNewMail"));
		    	 log.info("Vibrate Notification checked...");
		    }
			 Thread.sleep(800);
			 driver.navigate().back();
			 driver.navigate().back();
			 userAccount.switchMailAccount(driver, navigateToSettings.get("AccountOne"));
			 String command2 = path + java.io.File.separator + "Mail" + java.io.File.separator + "MailAlert"  + java.io.File.separator + "PINotificationEnabled.bat";
			 p = new ProcessBuilder(command2).start();
			 log.info("Executed the bat file to generate a mail"+command2);
			 Thread.sleep(6000);
				userAccount.openMenuDrawer(driver);
				navDrawer.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
				Thread.sleep(800);
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("AcceptOk"));
				Thread.sleep(1000);
				CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				Thread.sleep(2000);
				driver.navigate().back();
				Thread.sleep(2000);
				((AndroidDriver)driver).openNotifications();
			     inbox.waitForElementToBePresentByXpath(navigateToSettings.get("PiNotificationVerification"));
			     if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("PiNotificationVerification"))!=0){
						log.info("Important mail notification verified");
					}else{
						throw new Exception("Important mail notification not verifieded");
					}
			     Thread.sleep(500);
			     if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("VerifyUnImpMail"))!=0){
						log.info("UnImportant mail notification verified");
					}else{
						throw new Exception("UnImportant mail notification not verifieded");
					}
			     CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("PiNotificationVerification"));
			     Thread.sleep(1000);
			     if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("InboxTitle"))!=0){
						log.info("InboxTitle verified");
					}else{
						throw new Exception("InboxTitle not verifieded");
					}
			     if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("InboxVerifyUnImpMail"))!=0){
						log.info("InboxVerifyUnImpMail present");
					}else{
						throw new Exception("InboxVerifyUnImpMail not present");
					}
			     userAccount.openMenuDrawer(driver);
			     Thread.sleep(1000);
			     CommonFunctions.searchAndClickByXpath(driver, verifyPI.get("ImportantUnread"));
			     Thread.sleep(1000);
			     if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("ImpAndUnreadMail"))!=0){
						log.info("ImpAndUnreadMail present");
					}else{
						throw new Exception("ImpAndUnreadMail not present");
					}
			     Thread.sleep(800);
			    
			log.info("*****************Completed testcase ::: verifyPINotificationWhenEnabled*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPINotificationWhenEnabled");
		}
	}
	public void labelSyncConditions() throws Exception {
		 Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		try {
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSync"));
			 log.info("selected LabelSync Messages...");
			 Thread.sleep(500);
			 String checkSyncLabel=driver.findElement(By.xpath(navigateToSettings.get("SyncTestLabel"))).getAttribute("checked");
			 if(checkSyncLabel.equalsIgnoreCase("true"))
			{
				 driver.navigate().back();
				 log.info("Notifications are enabled");
		    }else {
		    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("SyncTestLabel"));
		    	 driver.navigate().back();
		    	 log.info("clicked on SyncTestLabel...");
		    }
			 Thread.sleep(1000);
			 String checkLabelNoti=driver.findElement(By.xpath(navigateToSettings.get("LabelNotificationCheck"))).getAttribute("checked");
			 if(checkLabelNoti.equalsIgnoreCase("true"))
			{
				 log.info("Notifications are enabled");
		    }else {
		    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("LabelNotificationCheck"));
		    	 log.info("LabelNotification checked...");
		    }
			
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "labelSyncConditions");
		}
	}

}
