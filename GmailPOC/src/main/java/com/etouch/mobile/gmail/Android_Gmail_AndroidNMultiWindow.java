package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_AndroidNMultiWindow extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void AndroidNMultiWindow() throws Exception{
		verifyMailSendMultiWindow();
		verifyMultipleMailsNotificationsReplyAndArchive();
		verifyDragAndDropFilesDownloads();
	}*/
	
	/**Testcase to verify Launcher short cut Gmail app for compose
	 * 
	 * @preCondition Device should be Android N
	 * @throws Exception
	 * GMAT 106
	 * @author Phaneendra
	 */
	@Test(groups = {"P0"},enabled = true, priority = 124)
	public void verifyMailSendMultiWindow()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> verifyComposeAndSendMailData = CommonPage.getTestData("verifyComposeAndSendMail");
			
			log.info("*****************Started testcase ::: verifyMailSendMultiWindow*****************************");
			inbox.verifyGmailAppInitState(driver);
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyComposeAndSendMailData, "AccountOne", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			inbox.enableMultiWindow(driver);	
			navDrawer.selectMultiWindow(driver);
			driver.navigate().back();
			Thread.sleep(1000);
			inbox.composeNewMail(driver,
					verifyComposeAndSendMailData.get("ToFieldData"),
					verifyComposeAndSendMailData.get("CCFieldData"), verifyComposeAndSendMailData.get("BccFieldData"),
					verifyComposeAndSendMailData.get("SubjectData"),
					verifyComposeAndSendMailData.get("ComposeBodyData"));
						
			CommonFunctions.addAttachmentFromDriver(driver,verifyComposeAndSendMailData, "Appium.jpg");
			Thread.sleep(3000);
			CommonFunctions.addAttachmentFromDriver(driver,verifyComposeAndSendMailData, "AppiumTutorial.pdf");
			inbox.tapOnSend(driver);
			
			Thread.sleep(3000);
			log.info("*****************Completed testcase ::: verifyMailSendMultiWindow*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailSendMultiWindow");		
}
	}	
	
	/**
	* @TestCase 107 : Verify multiple mails Notifications actions - View mail - Reply / reply all / Delete or archive mail
	* @Pre-condition : 1. Gmail app installed
                       2. Gmail account is added to app
                       3. Single Message sent from another account/device
    *  @author batchi
	* @throws: Exception
	*/
	@Test(groups = {"P0"}, enabled=true, priority=127)
	public void verifyMultipleMailsNotificationsReplyAndArchive() throws Exception{

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyMultipleMailsNotificationsReplyAndArchive ******************************");
			Map<String, String> verifyMultipleMailsNotificationsReplyAndArchiveData = CommonPage.getTestData("verifyReplyFromNotification");
			Map<String, String> verifyDeleteLuncherShortcutData = CommonPage.getTestData("verifyLauncherShortcut");
			driver.closeApp();
			log.info("Relaunching the app....");
			//Sending mail to open mail from notification
			//userAccount.switchMailAccount(driver, verifyMultipleMailsNotificationsReplyAndArchiveData.get("FromFieldData"));
		/*	
			String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"NestedNotifications.bat";
			Process p = new ProcessBuilder(command).start();
			log.info("Executed the bat file to generate a mail");
			Thread.sleep(10000);
		*/	
			String  emailId = CommonFunctions.getNewUserAccount(verifyDeleteLuncherShortcutData, "Account1", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			CommonFunctions.smtpMailGeneration(emailId,"TestingGmailMailArchivefromNotificationFunctionality,TestingGmailMailOpenfromNotificationFunctionality,TestingGmailMailDeletefromNotificationFunctionality,TestingGmailMailReplyfromNotificationFunctionality,", "Test", null);
			
			//New code added by Bindu to receieve notifications
			commonPage.syncNowFromSettingsAppToGetNotifications(driver, verifyDeleteLuncherShortcutData.get("SearchAccountsFeature"), verifyDeleteLuncherShortcutData.get("AccountTypeGoogle"), verifyDeleteLuncherShortcutData.get("Account1"));
			Thread.sleep(2000);
			((AndroidDriver)driver).openNotifications();
			
			// commonPage.pullToReferesh(driver);
			commonPage.swipeDown(driver);
			commonPage.verifySubjectIsMatching(driver,verifyMultipleMailsNotificationsReplyAndArchiveData.get("SubjectData"));
			commonPage.swipeDown(driver);
			commonPage.verifyReplyButtonFromNotification(driver);
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			((AndroidDriver)driver).openNotifications();
			//commonPage.pullToReferesh(driver);
			commonPage.swipeDown(driver);
			commonPage.verifySubjectIsMatching(driver,verifyMultipleMailsNotificationsReplyAndArchiveData.get("SubjectDataForArchive"));
			commonPage.swipeDown(driver);
			commonPage.swipeDown(driver);
			commonPage.verifyArchiveFromNotification(driver);
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			((AndroidDriver)driver).openNotifications();
			
			// commonPage.pullToReferesh(driver);
			 commonPage.swipeDown(driver);
			 commonPage.verifySubjectIsMatching(driver,verifyMultipleMailsNotificationsReplyAndArchiveData.get("SubjectDataForOpen"));
			 //commonPage.swipeDown(driver);
			 commonPage.verifyOpenFromNotification(driver,verifyMultipleMailsNotificationsReplyAndArchiveData.get("SubjectDataForOpen"));
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			 inbox.launchGmailApplication(driver);
			}catch (Throwable e) {
				driver.navigate().back();
				userAccount.launchGmailApplication(driver);
				commonPage.catchBlock(e, driver, "verifyMailSendMultiWindow");		
	}
}
	
	/**Testcase to compose mail with drag and drop files from downloads app to gmail 
	 * 
	 * @preCondition Device should be Android N
	 * @throws Exception
	 * GMAT 108
	 * @author Phaneendra
	 */
	//Drag and drop is not happening properly and hence making this TC as false
	/*@Test(groups = {"P0"}, enabled = false, priority = 108)
	public void verifyDragAndDropFilesDownloads()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> verifyComposeAndSendMailData = commonPage
					.getTestData("verifyComposeAndSendMail");
			
			log.info("*****************Started testcase ::: verifyDragAndDropFilesDownloads*****************************");
			inbox.openDownloadsApp(driver);
			inbox.launchGmailApplication(driver);	
			inbox.enableMultiWindow(driver);	
			
			userAccount.adjustMultiWindow(driver);
			navDrawer.selectGmailWindowCompose(driver);
			navDrawer.selectDownloadsApp(driver);
			userAccount.selectFilesDownloads(driver);
			Thread.sleep(6000);
			log.info("*****************Completed testcase ::: verifyDragAndDropFilesDownloads*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyDragAndDropFilesDownloads");		
}
	}	*/
	/**
	* @TestCase 69 : verify Delete, archive and Move To from multi window
	* @Pre-condition : Activate multi window mode by long press on overview button or press and hold app title bar in recent apps screen
	*  @author batchi
	*  GMAT 109
	* @throws: Exception
	*/
	@Test(enabled=true, priority=125)
	public void verifyArchieveDeleteAndMoveToFromMultiWindow() throws Exception{
		driver = map.get(Thread.currentThread().getId());
			try {
				log.info("***************** Started testcase ::: verifyArchieveDeleteAndMoveToFromMultiWindow ******************************");
/*				
				String path = System.getProperty("user.dir");
				String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"verifyArchiveDeleteAndMoveTo.bat";
				log.info("Executing the bat file to generate a mail and move to other folder "+command);
				Process p = new ProcessBuilder(command).start();
*/

				commonPage.pullToReferesh(driver);
				Map<String, String> verifyArchieveDeleteAndMoveToFromMultiWindowData = CommonPage.getTestData("verifyLauncherShortcut");
				//inbox.launchGmailApp(driver);
				
				String  emailId = CommonFunctions.getNewUserAccount(verifyArchieveDeleteAndMoveToFromMultiWindowData, "Account1", null);
				CommonFunctions.smtpMailGeneration(emailId,"verifyArchiveFromCV,verifyDeleteFromCV,verifyMoveToFromCV,verifyArchiveFromTL,verifyDeleteFromTL,verifyMoveToFromTL", "ArchiveMail.html", null);
				Thread.sleep(10000);
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);
				
				//userAccount.switchMailAccount(driver, verifyArchieveDeleteAndMoveToFromMultiWindowData.get("Account1"));
		        /*Process p;
				String [] longpressOverview = new String[]{"adb", "shell", "input", "touchscreen","swipe", "844 1857 844 1857 2000"};
	            p = new ProcessBuilder(longpressOverview).start();*/
				/**Uncomment this below line if your execution this case individually**/
				//	inbox.enableMultiWindow(driver);	
	    		navDrawer.selectMultiWindow(driver);
	            Thread.sleep(2000);
	            inbox.verifyDeleteArchiveAndMoveToFromTL(driver,
	            		verifyArchieveDeleteAndMoveToFromMultiWindowData.get("SubjectForArchiveInTL"));
	            Thread.sleep(2000);
	            inbox.verifyDeleteArchiveAndMoveToFromCV(driver, 
	            		verifyArchieveDeleteAndMoveToFromMultiWindowData.get("SubjectForArchiveInCV"),
	            		verifyArchieveDeleteAndMoveToFromMultiWindowData.get("SubjectForDeleteInCV"),
	            		verifyArchieveDeleteAndMoveToFromMultiWindowData.get("SubjectForMoveToInCV"));
	            log.info("***************** Ended testcase ::: verifyArchieveDeleteAndMoveToFromMultiWindow ******************************");
			}catch (Throwable e) {
				  commonPage.catchBlock(e, driver, "verifyArchieveDeleteAndMoveToFromMultiWindow");
		}
	}

	/**Testcase to verify Launcher short cut Gmail app for compose in Landscape mode and  also perform Delete Archive and MoveTo options
	 * @preCondition Device should be Android N
	 * @throws Exception
	 * GMAT 110
	 * @author batchi
	 */
	@Test(enabled = true, priority = 126)
	public void verifyMailSendMultiWindowInLandscapeMode()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("*****************Started testcase ::: verifyMailSendMultiWindowInLandscapeMode*****************************");
			Map<String, String> verifyComposeAndSendMailData = CommonPage.getTestData("verifyLauncherShortcut");
			inbox.verifyGmailAppInitState(driver);
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyComposeAndSendMailData, "Account1", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			//userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("Account1"));
			/**Uncomment this below line if your execution this case individually**/
			//inbox.enableMultiWindow(driver);	
			navDrawer.selectMultiWindow(driver);
			Thread.sleep(2000);
			driver.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
			if("Nexus 6P".equals(deviceName)){
				driver.navigate().back();
			}
			Thread.sleep(1000);
			/*String fromUser=inbox.composeAndSendNewMail(driver,
					verifyComposeAndSendMailData.get("Account3"),
					verifyComposeAndSendMailData.get("CCFieldData"), "",
					verifyComposeAndSendMailData.get("SubjectData"),
					verifyComposeAndSendMailData.get("ComposeBodyData"));
			
		*/	/*
			String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"verifyArchiveDeleteAndMoveTo.bat";
			log.info("Executing the bat file to generate a mail and move to other folder "+command);
			Process p = new ProcessBuilder(command).start();
			Thread.sleep(35000);
			*/
		//	userAccount.switchMailAccount(driver, verifyComposeAndSendMailData.get("Account2"));
			CommonFunctions.smtpMailGeneration(emailId,"verifyArchiveFromCV,verifyDeleteFromCV,verifyMoveToFromCV,verifyArchiveFromTL,verifyDeleteFromTL,verifyMoveToFromTL", "ArchiveMail.html", null);
			Thread.sleep(9000);
			commonPage.pullToReferesh(driver);
			inbox.verifyDeleteArchiveAndMoveToFromTL(driver,
	        		verifyComposeAndSendMailData.get("SubjectForTL"));
	        Thread.sleep(2000);
	        inbox.verifyDeleteArchiveAndMoveToFromCV(driver, 
	        		verifyComposeAndSendMailData.get("SubjectForArchiveInCV"),
	        		verifyComposeAndSendMailData.get("SubjectForDeleteInCV"),
	        		verifyComposeAndSendMailData.get("SubjectForMoveToInCV"));
	        Thread.sleep(2000);
			driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
						
			log.info("*****************Completed testcase ::: verifyMailSendMultiWindowInLandscapeMode*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifyMailSendMultiWindowInLandscapeMode");	
			driver.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
	}
	}		
			
}
