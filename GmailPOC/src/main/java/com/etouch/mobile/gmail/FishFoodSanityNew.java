package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.MailManager;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;
import com.gargoylesoftware.htmlunit.javascript.host.file.File;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;@Test(groups = "GmailApplication")@IExcelDataFiles(excelDataFiles = {
	"GmailAppData=testData"
})
public class FishFoodSanityNew extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;

	Android_Gmail_Compose compose = new Android_Gmail_Compose();
	Android_Gmail_ThreadConversationList threadConvList = new Android_Gmail_ThreadConversationList();
	Android_Gmail_MailAction mailAction = new Android_Gmail_MailAction();
	Android_Gmail_Notification notification = new Android_Gmail_Notification();

	/**Start of sanity testcases which covers below scenerios :
	 * - Compose and send mail with CC, BCC fields with attachments
	 * - Archive and Delete mail from TL and CV
	 * - Delete, Archive from Notification
	 * - Reply/Reply All from Notification
	 * - Verify search works
	 * - Verify Drawer displays properly and able to switch accounts
	 * - Send and Receive mails from and to dasher accounts
	 * - Verify General settings and Account level Settings are showing properly
	 * 
	 * @author Phaneendra
	 */

    String path = System.getProperty("user.dir");
    
    String attachfiles = path + java.io.File.separator + "AttachmentFiles" + java.io.File.separator;
	
	
	/**--Add accounts--**/
	/*@Test(priority=1) 
    public void addAccountsInternal()throws Exception{
	    	try{

	    		log.info(attachfiles);
	    		
	    		
	    		*//**--Pushing the files to android device--**//*
	    		String[] img = new String[]{"adb","push", ""};
	    		//log.info(img);
	    		
	    		log.info("Installing the google account manager apk internally to add other accounts");
			    Process p;
			    ProcessBuilder pb; 
				String[] launchOldApk = new String[]{"adb","install", "-r","-d" ,"E:/Gmail-POC-GmailTest_Automation/GmailPOC/Gmail_Apk/GoogleAccountManagerApp.apk"};
			    p = new ProcessBuilder(launchOldApk).start();
			    log.info("Installing the google account manager apk");
			    Thread.sleep(10000);
	    		
			    *//**---Adding etouch account one--**//*
			    String[] addAccountOne = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "etouchtestone@gmail.com", "-e", "password", "eTouch@123", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountOne).start();
			    log.info("Installing etouch account one with google account manager apk");
			    Thread.sleep(10000);
			    
			    *//**---Adding etouch account two--**//*
			    String[] addAccountTwo = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "etouchtesttwo@gmail.com", "-e", "password", "eTouch@123", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountTwo).start();
			    log.info("Installing etouch account two with google account manager apk");
			    Thread.sleep(10000);
			    
			    *//**---Adding etouch account three--**//*
			    String[] addAccountThree = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "etouchtestthree@gmail.com", "-e", "password", "eTouch@123", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountThree).start();
			    log.info("Installing etouch account three with google account manager apk");
			    Thread.sleep(10000);
			    
			    *//**---Adding etouch account four--**//*
			    String[] addAccountFour = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "etouchtestfour@gmail.com", "-e", "password", "eTouch@123", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountFour).start();
			    log.info("Installing etouch account four with google account manager apk");
			    Thread.sleep(10000);
	    		
			    *//**---Adding the dasher account--**//*
	    		String[] addDasherAccount = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "bigtopdasher1@cariboupremier.bigr.name", "-e", "password", "d0n0ev1l", "-e", "action", "add", "-e", "sync", "true",
			    		"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
			    p = new ProcessBuilder(addAccountFour).start();
			    log.info("Installing dahser account with google account manager apk");
			    Thread.sleep(10000);
	    	
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    }
	
*/	
	/**--Testcase include :: Compose and send mail with CC, BCC with the attachments
	 * 					   Verify Drawer displays properly and able to switch accounts
	 * (Limitation with verifying the user profile picture along with background image if exists, currently no support for this in this framework)
	 * --**//*
	@Test(priority = 2, enabled = true)
	public void ComposeMailWithAttachmentsandAccountSwitcher() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		// MailManager.DeleteAllMailsInbox();
		Map < String,
		String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
		Map < String,
		String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
		try {
			log.info("********Verifying the accounts that are exists**********************");
			userAccount.verifyAccountsExists(driver, verifyAddingNewGmailAccountData.get("AccountToExists"), verifyAddingNewGmailAccountData.get("Password"));
			log.info("********Switching to account******");
			userAccount.switchMailAccount(driver, verifyAddingNewGmailAccountData.get("UserList"));
			//inbox.composeAndSendMailForGmailAccountWithAttachmentsGIG(driver);
			
			
			
			
			
			compose.composeAndSendMailForGmailAccountWithAttachments();	
			//driver.navigate().back();
		} catch(Exception e) {
			e.printStackTrace();
			commonPage.catchBlockDefect(e, driver, "ComposeMailWithAttachmentsandAccountSwitcher");
		}
	}

*/	/**--Testcase include :: Archive and Delete mail from TL and CV--**/
	@Test(priority = 3, enabled = true)
	public void ArchiveDelete() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		Map < String,
		String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");
		Map < String,
		String > verifyArchiveAndDeletingMultipleMailsData = commonPage.getTestData("verifyArchiveAndDeletingMultipleMails");
		Map < String,
		String > deleteMailCheckTrashData = commonPage.getTestData("deleteMailCheckTrash");

		if(!CommonFunctions.isAccountAlreadySelected(verifyAddingNewGmailAccountData.get("UserList")))
			userAccount.switchMailAccount(driver, verifyAddingNewGmailAccountData.get("UserList"));
		String path = System.getProperty("user.dir");
		String command = path + java.io.File.separator + "Mail" + java.io.File.separator + "ArchiveMails.bat";
		log.info("Executing the bat file " + command);
		//Process p = new ProcessBuilder(command).start();
		Runtime.getRuntime().exec(command);
		log.info("Executed the bat file to generate a mail");
		Thread.sleep(10000);
		try {
			log.info("*****************Started testcase ::: verifyArchiveAndDeletingMultipleMails********************");
			inbox.verifyGmailAppInitState(driver);
			userAccount.navigateToGeneralSettings(driver);
			inbox.checkSenderImageCheckBox(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail1"));
			Thread.sleep(1000);
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail2"));
			Thread.sleep(1000);
			inbox.verifyMailSelectionCount(driver, verifyArchiveAndDeletingMultipleMailsData.get("MailSelectionCount"));
			// System.out.println("First Time selection count");
			navDrawer.tapOnArchive(driver);
			navDrawer.navigateToAllEmails(driver);

			inbox.verifyMailIsPresent(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail1"));
			inbox.verifyMailIsPresent(driver, verifyArchiveAndDeletingMultipleMailsData.get("ArchiveMail2"));
			// userAccount.switchMailAccount(driver,
			// verifyArchiveAndDeletingMultipleMailsData.get("AccountTwo"));
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail1"));
			inbox.selectMail(driver, verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail2"));
			// inbox.selectMail(verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail3"));
			// System.out.println("Second Time selection count");
			inbox.verifyMailSelectionCount(driver, verifyArchiveAndDeletingMultipleMailsData.get("MailSelectionCount"));
			navDrawer.tapOnDelete(driver);
			//navDrawer.navigateToBin(driver);
			navDrawer.navigateToBin(driver, verifyArchiveAndDeletingMultipleMailsData.get("BinOption"));

			inbox.verifyMailIsPresent(driver, verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail1"));
			inbox.verifyMailIsPresent(driver, verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail2"));
			// inbox.verifyMailIsPresent(driver,verifyArchiveAndDeletingMultipleMailsData.get("DeleteMail3"));
			// commonFunction.navigateBack(driver);
			// commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyArchiveAndDeletingMultipleMails************");

			log.info("*****************Started testcase ::: deleteMailCheckTrash*****************************");

			//	userAccount.switchMailAccount(driver, deleteMailCheckTrashData.get("Account"));
			navDrawer.openPrimaryInbox(driver, deleteMailCheckTrashData.get("GIGAccount"));

			inbox.deleteMailPrimary(driver, deleteMailCheckTrashData.get("MailSubject"), null);
			inbox.verifySnackBarDeleted(driver);

			navDrawer.navigateToTrashFolder(driver);
			inbox.verifyMailTrashFolder(driver, deleteMailCheckTrashData.get("MailSubject"));
			log.info("*****************Completed testcase ::: deleteMailCheckTrash*************************");

			Map < String,
			String > mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
			log.info("*****************Started testcase ::: verifyArchiveMail*****************************");

			//	userAccount.verifyAccountsExists(driver, mailMoveToOtherFolderData.get("Account"), mailMoveToOtherFolderData.get("AccountPassword"));
			//	userAccount.switchMailAccount(driver, mailMoveToOtherFolderData.get("Account"));
			navDrawer.openPrimaryInbox(driver, mailMoveToOtherFolderData.get("GIGAccount"));
			inbox.archiveMail(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"), null);
			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, mailMoveToOtherFolderData.get("EmailSubjectArchive"));

			log.info("*****************Completed testcase ::: verifyArchiveMail*************************");
		} catch(Exception e) {
			e.printStackTrace();
			commonPage.catchBlockDefect(e, driver, "ArchiveDelete");
		}
	}

	/**--Testcase include :: Verify Search Works--**/
	@Test(priority = 4, enabled = true)
	public void SearchWorks() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		SoftAssert softAssert = new SoftAssert();
		Map < String,
		String > verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		try {
			//	userAccount.switchMailAccount(driver, verifySearchFunctionalityData.get("GIGAccount"));
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementById(driver, verifySearchFunctionalityData.get("SearchMailID")), verifySearchFunctionalityData.get("SearchMailText"));
			try {
				driver.hideKeyboard();
				driver.getKeyboard();
			} catch(Throwable e) {
				commonPage.catchBlockDefect(e, driver, "Keyboard not found");
			}
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityData.get("SearchMailID"), verifySearchFunctionalityData.get("SearchKeyAttachments"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityData.get("MailSubjectAttachments"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityData.get("MailSubjectAttachments"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			commonFunction.searchAndClickById(driver, verifySearchFunctionalityData.get("SearchIconID"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementByXpath(driver, verifySearchFunctionalityData.get("suggestionTextPathattachments")), verifySearchFunctionalityData.get("suggestionSearchTextattachments"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();

		} catch(Exception e) {
			e.printStackTrace();
			commonPage.catchBlockDefect(e, driver, "SearchWorks");
		}
	}

	/**--Testcase include :: Verify General settings and Account level Settings are showing properly--**/
	@Test(priority = 5, enabled = true)
	public void verifyGeneralAccountLevelSettings() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			inbox.verifyAccountSettingsDisplayed(driver);
			driver.navigate().back();
			Thread.sleep(800);
			userAccount.NavigateGeneralSettingsFromSettings(driver);
			userAccount.verifyGeneralSettingsDisplayed(driver);
			driver.navigate().back();
			driver.navigate().back();
		} catch(Exception e) {
			commonPage.catchBlockDefect(e, driver, "verifyGeneralAccountLevelSettings");
		}
	}
	
	
	/**--Testcase include :: Verify mail open from notification, reply/reply all, archive and delete from notification--**/
	@Test(priority = 6, enabled = true)
	public void verifyNotificationOptions() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map <String, String> addAttachments = commonPage.getTestData("verifyAttachmentsUponRotating");
		String fromUser;
		try {
			//	notification.verifyNotificationReplyAndArchive();
			log.info("***************** Started testcase ::: verifyNotificationReplyAndArchive ******************************");
			Map < String,
			String > verifyNotificationReplyAndArchiveData = commonPage.getTestData("verifyReplyFromNotification");
			//Sending mail to open mail from notification
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
						
			fromUser = inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"), 
					"", "", 
					verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"), "");
			
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			userAccount.EnableSyncOnOFf(driver);
			//	Runtime.getRuntime().exec("adb shell am start -n com.android.settings/.accounts.AddAccountSettings");
			driver.navigate().back();
			Thread.sleep(5000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"));
			commonPage.verifyOpenFromNotification(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForOpen"));
			commonPage.navigateUntilHamburgerIsPresent(driver);

			//Sending mail to click Reply button from notification
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
			log.info("***********Verifying for reply notification**********");
			userAccount.setDefaultReplyActionToReply(driver);
			
			fromUser = inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"), 
					"", "", 
					verifyNotificationReplyAndArchiveData.get("SubjectData"), "");
			
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			userAccount.EnableSyncOnOFf(driver);
			driver.navigate().back();
			Thread.sleep(5000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectData"));
			log.info("Clicking on reply button");
			commonPage.verifyReplyButtonFromNotification(driver);
			Thread.sleep(1500);
			log.info("Adding attachments");
			attachment.addAttachement(driver, addAttachments.get("ImageJpgAttachmentName"));
			attachment.addAttachement(driver, addAttachments.get("GettingStarted"));
			attachment.addAttachement(driver, addAttachments.get("GmailTestData"));
			inbox.tapOnSend(driver);
			
			commonPage.navigateUntilHamburgerIsPresent(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
			userAccount.pullToReferesh(driver);
			userAccount.openMail(driver, verifyNotificationReplyAndArchiveData.get("SubjectData"));
			attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("ImageJpgAttachmentName"));
			attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("GettingStarted"));
			attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("GmailTestData"));

			driver.navigate().back();
			log.info("***********Verifying for reply notification completed**********");

			//Sending mail to click on Delete button from notification
			inbox.verifyGmailAppInitState(driver);
			log.info("***********Verifying for delete from notification**********");

			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
			userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
			log.info("***********Verifying for delete notification**********");

			userAccount.setGmailDefaultActionToDelete(driver);
			inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"), 
					verifyNotificationReplyAndArchiveData.get("CC"), 
					verifyNotificationReplyAndArchiveData.get("BCC"), 
					verifyNotificationReplyAndArchiveData.get("SubjectDataForDelete"), 
					verifyNotificationReplyAndArchiveData.get("ComposeBodyData"));
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			userAccount.EnableSyncOnOFf(driver);
			driver.navigate().back();
			Thread.sleep(8000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForDelete"));
			commonPage.verifyDeleteFromNotification(driver);
			Runtime.getRuntime().exec("adb shell service call statusbar 2");
			Thread.sleep(1500);
			driver.navigate().back();
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("FromFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			//navDrawer.navigateToPrimary(driver);
			navDrawer.navigateToTrashFolder(driver);
			userAccount.verifyMailIsPresent(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForDelete"));
			//driver.swipe(0, 0, 720, 1184, 1000);
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			//Sending mail to click on Archive button from notification
			log.info("***********Verifying for delete from notification completed**********");

			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
			log.info("***********Verifying for archive from notification**********");

			userAccount.setGmailDefaultActionToArchive(driver);
			inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"), 
					verifyNotificationReplyAndArchiveData.get("CC"), 
					verifyNotificationReplyAndArchiveData.get("BCC"), 
					verifyNotificationReplyAndArchiveData.get("SubjectDataForArchive"), 
					verifyNotificationReplyAndArchiveData.get("ComposeBodyData"));
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			userAccount.EnableSyncOnOFf(driver);
			driver.navigate().back();
			Thread.sleep(8000);
			((AndroidDriver) driver).openNotifications();
			/*commonFunction.switchOnAirPlaneMode();
			Thread.sleep(5000);
			commonFunction.switchOffAirPlaneMode();*/
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForArchive"));
			commonPage.verifyArchiveFromNotification(driver);
			Runtime.getRuntime().exec("adb shell service call statusbar 2");
			Thread.sleep(1500);
			driver.navigate().back();
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("FromFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			//navDrawer.navigateToPrimary(driver);
			navDrawer.navigateToAllEmails(driver);
			userAccount.verifyMailIsPresent(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForArchive"));
			//driver.swipe(0, 0, 720, 1184, 1000);
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			//Sending Mail to click on Reply All from Notification
			log.info("***********Verifying for archive from notification completed**********");

			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
				userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
			log.info("***********Verifying for reply all from notification**********");

			userAccount.setDefaultReplyActionToReplyAll(driver);
			inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"), 
					verifyNotificationReplyAndArchiveData.get("BccFieldData"), 
					verifyNotificationReplyAndArchiveData.get("BCC"), 
					verifyNotificationReplyAndArchiveData.get("SubjectDataForReplyAll"), 
					verifyNotificationReplyAndArchiveData.get("ComposeBodyData"));
			navDrawer.openUserAccountSetting(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			userAccount.EnableSyncOnOFf(driver);
			driver.navigate().back();
			Thread.sleep(8000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForReplyAll"));
			commonPage.verifyReplyAllFromNotification(driver);
			Thread.sleep(1000);
			
			String toText = driver.findElement(By.xpath("//android.widget.MultiAutoCompleteTextView[@resource-id='com.google.android.gm:id/to']")).getText();
			String ccText = driver.findElement(By.xpath("//android.widget.MultiAutoCompleteTextView[@resource-id='com.google.android.gm:id/cc']")).getText();
			
			if(toText.length()>1 && ccText.length()>1){
				log.info("Replying all the recipients");
				attachment.addAttachement(driver, addAttachments.get("ImageJpgAttachmentName"));
				attachment.addAttachement(driver, addAttachments.get("GettingStarted"));
				attachment.addAttachement(driver, addAttachments.get("GmailTestData"));
				inbox.tapOnSend(driver);
				
				commonPage.navigateUntilHamburgerIsPresent(driver);
				
				if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("ToFieldData")))
					userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"));
				userAccount.pullToReferesh(driver);
				userAccount.openMail(driver, verifyNotificationReplyAndArchiveData.get("SubjectData"));
				attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("ImageJpgAttachmentName"));
				attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("GettingStarted"));
				attachment.verifyAttachmentIsAddedSuccessfully(driver, addAttachments.get("GmailTestData"));

				driver.navigate().back();
			}else{
				throw new Exception("One of the field in compose is not filled with recipients");
			}
			
			//commonPage.navigateUntilHamburgerIsPresent(driver);
			//Sending Mail to Swipe to dismiss from Notification
			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyNotificationReplyAndArchiveData.get("FromFieldData")))
			userAccount.switchMailAccount(driver, verifyNotificationReplyAndArchiveData.get("FromFieldData"));
			inbox.composeAndSendNewMail(driver, verifyNotificationReplyAndArchiveData.get("ToFieldData"), verifyNotificationReplyAndArchiveData.get("CC"), verifyNotificationReplyAndArchiveData.get("BCC"), verifyNotificationReplyAndArchiveData.get("SubjectDataForDismiss"), verifyNotificationReplyAndArchiveData.get("ComposeBodyData"));
			Thread.sleep(8000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyNotificationReplyAndArchiveData.get("SubjectDataForDismiss"));
			//to dismiss the notification
			commonPage.swipeRightToLeft(driver, verifyNotificationReplyAndArchiveData.get("SwipeElementNotificationViewPath"));
			log.info("*****************Completed testcase ::: verifyNotificationReplyAndArchive ***************************");
		} catch(Exception e) {
			e.printStackTrace();
			commonPage.catchBlockDefect(e, driver, "verifyNotificationOptions");

		}
	}

	@Test(priority = 7, enabled = true)
	public void verifyDasherAccountEmail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			
			Map < String,
			String > verifyDasherAccount = commonPage.getTestData("dasherAccount");
	        Map < String, String > verifyComposeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

			log.info("***************** Started testcase ::: verifyDasherAccountEmail ******************************");
			userAccount.verifyAccountsExists(driver, verifyDasherAccount.get("dasherAccountName"), verifyDasherAccount.get("dasherPwd"));
			Thread.sleep(3000);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyDasherAccount.get("ToFieldEtouch")))
				userAccount.switchMailAccount(driver, verifyDasherAccount.get("ToFieldEtouch"));
			
			String fromUser = inbox.composeAsNewMail(driver,verifyDasherAccount.get("dasherAccountName"),"","",verifyDasherAccount.get("SubjectData"), "");
	       
			attachment.addAttachement(driver, verifyDasherAccount.get("Appium"));
			log.info("Appium attahment added");
			attachment.addAttachement(driver, verifyDasherAccount.get("GettingStarted"));
			log.info("Getting started attahment added");
			driver.navigate().back();
			inbox.openMenuDrawer(driver);
			navDrawer.navigateToDrafts(driver);
			userAccount.openMail(driver, verifyDasherAccount.get("SubjectData"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyDasherAccount.get("Appium"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyDasherAccount.get("GettingStarted"));
	        log.info("attachements added successful");

	        log.info("CLickEditDraft");
	        commonPage.scrollUp(2);
	        inbox.clickEditDraft(driver);
	        userAccount.tapOnSend(driver);
	        log.info("****Mail Sent*****");
	        driver.navigate().back();
	        navDrawer.openUserAccountSetting(driver, verifyDasherAccount.get("dasherAccountName"));
			userAccount.EnableSyncOnOFf(driver);
			driver.navigate().back();
			Thread.sleep(5000);
			((AndroidDriver) driver).openNotifications();
			commonPage.verifySubjectIsMatching(driver, verifyDasherAccount.get("SubjectData"));
			
	      //  userAccount.switchMailAccount(driver, verifyDasherAccount.get("ToFieldData"));
	        userAccount.openMail(driver, verifyDasherAccount.get("SubjectData"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyDasherAccount.get("Appium"));
	        attachment.verifyAttachmentIsAddedSuccessfully(driver, verifyDasherAccount.get("GettingStarted"));
	       // inbox.composeMailAsReply(driver, verifyDasherAccount.get("ReplyText"), "");
	       // userAccount.tapOnSend(driver);
	        
	        //For notification code has to develop
	        driver.navigate().back();
			log.info("***************** Completed testcase ::: verifyDasherAccountEmail ******************************");

		}catch(Exception e){
			commonPage.catchBlockDefect(e, driver, "verifyDasherAccountEmail");

		}
	}

}