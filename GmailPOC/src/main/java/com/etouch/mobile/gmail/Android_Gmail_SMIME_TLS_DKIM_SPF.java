package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_SMIME_TLS_DKIM_SPF extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;

	/**
	 * @TestCase 100 : Verify inbound (CV) and outbound (Compose)
	 * @Pre-condition : 1. Add TLS flag enabled account to gmail
	 *                "user01.andro@gmail.com"
	 * @author batchi
	 * @throws: Exception comment: Used etouchqaone@gmail.com as TLS flagged
	 *          account and for time being used Monster.com which has padlock
	 *          icon.
	 */
	@Test(enabled = true, priority = 115)
	public void verifyInboundAndOutbound() throws Exception {

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyInboundAndOutbound ******************************");
			Map<String, String> verifyInboundAndOutboundData = commonPage
					.getTestData("verifyIconsAreUpdated");

			inbox.verifyGmailAppInitState(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyInboundAndOutboundData.get("ToFieldData")))
			userAccount.switchMailAccount(driver,verifyInboundAndOutboundData.get("ToFieldData"));
			
			inbox.openMailUsingSearch(driver,
					verifyInboundAndOutboundData.get("InboundSubject"));
			Thread.sleep(1500);
			inbox.inBound(driver);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			inbox.composeMailWithOnlyTo(driver, verifyInboundAndOutboundData.get("Account3"));
			inbox.outBound(driver,
					verifyInboundAndOutboundData.get("Account3"));
			driver.navigate().back();
			driver.navigate().back();
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyInboundAndOutbound");
		}
	}

	/**
	 * @TestCase GMAT 91 : verify Icons Are Updated
	 * @Pre-condition : Sign into below account
	 *                dasher.01@gau.pintoqa.work/BigBangInF()RK
	 * @author batchi
	 * @throws: Exception
	 **/
	@Test(enabled = true, priority = 116)
	public void verifyIconsAreUpdated() throws Exception {

		driver = map.get(Thread.currentThread().getId());
		try {
			
				log.info("***************** Started testcase ::: verifyIconsAreUpdated ******************************");
				Map<String, String> verifyIconsAreUpdatedData = commonPage
						.getTestData("verifyIconsAreUpdated");
				inbox.verifyGmailAppInitState(driver);
				
				if(!CommonFunctions.isAccountAlreadySelected(verifyIconsAreUpdatedData.get("Account1")))
				userAccount.switchMailAccount(driver,
						verifyIconsAreUpdatedData.get("Account1"));
				/*--Method to verify smime1 account is added with device policy-*/
			//	userAccount.verifyDevicePolicy(driver);
				
				commonPage.verifyGreenPadlock(driver,
						verifyIconsAreUpdatedData.get("GreenPadAccount"));
				commonPage.verifyViewDetailsInToast(driver,
						verifyIconsAreUpdatedData.get("GreenIconText"),
						verifyIconsAreUpdatedData.get("Account1"),
						verifyIconsAreUpdatedData.get("EnhancedEncryptionRecipient"));
				driver.navigate().back();
			//	commonFunction.navigateBackToMenuDrawer(driver);
			//	inbox.launchGmailApplication(driver);
				commonPage.verifyGrayPadlock(driver,
						verifyIconsAreUpdatedData.get("Account2"));
				commonPage.verifyViewDetailsInToast(driver,
						verifyIconsAreUpdatedData.get("GrayIconText"),
						verifyIconsAreUpdatedData.get("Account2"),
						verifyIconsAreUpdatedData.get("StandardEncryptionRecipient"));
			
				driver.navigate().back();
			//	commonFunction.navigateBackToMenuDrawer(driver);
			//	inbox.launchGmailApplication(driver);
				commonPage.verifyRedPadlock(driver,
						verifyIconsAreUpdatedData.get("Account3"));
				commonPage.verifyViewDetailsInToast(driver,
						verifyIconsAreUpdatedData.get("RedIconText"),
						verifyIconsAreUpdatedData.get("Account3"),
						verifyIconsAreUpdatedData.get("NoEncryptionRecipient"));
				driver.navigate().back();
				//driver.navigate().back();
				log.info("***************** Completed testcase ::: verifyIconsAreUpdated ******************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyIconsAreUpdated");
		}
	}

	/**
	 * @TestCase GMAT 92 : Verify All Possible Cases For SMIME
	 * @Pre-condition :
	 * @author batchi
	 * @throws: Exception
	 **/
	@Test(enabled = true, priority = 1)
	public void verifyAllPossibleCasesForSMIME() throws Exception {

		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyAllPossibleCasesForSMIME ******************************");
			Map<String, String> verifyAllPossibleCasesForSMIMEData = commonPage
					.getTestData("verifyIconsAreUpdated");
			inbox.verifyGmailAppInitState(driver);
			//userAccount.verifyAccountsExists(driver, verifyAllPossibleCasesForSMIMEData.get("Account1"), verifyAllPossibleCasesForSMIMEData.get("DasherPwd"));
			//userAccount.verifyAccountsExists(driver, verifyAllPossibleCasesForSMIMEData.get("Account2"), verifyAllPossibleCasesForSMIMEData.get("Account2Pwd"));
			//userAccount.verifyAccountsExists(driver, verifyAllPossibleCasesForSMIMEData.get("Account3"), verifyAllPossibleCasesForSMIMEData.get("Account3Pwd"));
			//userAccount.addYahooAccount(driver, verifyAllPossibleCasesForSMIMEData.get("Account3"), verifyAllPossibleCasesForSMIMEData.get("Account3Pwd"));
			//if(!CommonFunctions.isAccountAlreadySelected(verifyAllPossibleCasesForSMIMEData.get("Account1")))
				
				if(!CommonFunctions.isAccountAlreadySelected(verifyAllPossibleCasesForSMIMEData.get("Account1")))
			userAccount.switchMailAccount(driver,
					verifyAllPossibleCasesForSMIMEData.get("Account1"));
		/*	inbox.composeAndSendNewMail(driver,
					verifyAllPossibleCasesForSMIMEData.get("ToFieldData"),
					verifyAllPossibleCasesForSMIMEData.get("CC"),
					verifyAllPossibleCasesForSMIMEData.get("BCC"),
					verifyAllPossibleCasesForSMIMEData
							.get("GreenPadlockSubject"),
					verifyAllPossibleCasesForSMIMEData.get("ComposeBody"));
		*/	navDrawer.navigateToSentBox(driver);
			userAccount.openMailUsingSearch(driver, verifyAllPossibleCasesForSMIMEData.get("GreenPadlockSubject"));
			CommonFunctions.searchAndClickByXpath(driver, verifyAllPossibleCasesForSMIMEData.get("ViewDetails"));
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAllPossibleCasesForSMIMEData.get("VerifiedEmailAdd"));
		/*userAccount.switchMailAccount(driver,
			 verifyAllPossibleCasesForSMIMEData.get("Account3") );
		inbox.composeAndSendNewMail(driver,
			  verifyAllPossibleCasesForSMIMEData.get("ToFieldData"),
			  verifyAllPossibleCasesForSMIMEData.get("CC"),
			  verifyAllPossibleCasesForSMIMEData.get("BCC"),
			  verifyAllPossibleCasesForSMIMEData.get("RedPadlockSubject"),
			  verifyAllPossibleCasesForSMIMEData.get("ComposeBody"));
		userAccount
					.verifyGmailAccountIsPresent(verifyAllPossibleCasesForSMIMEData
							.get("ToFieldData"));
		userAccount.switchMailAccount(driver,
					verifyAllPossibleCasesForSMIMEData.get("ToFieldData"));

		commonPage.verifyDetailsFromRespectiveAccount(driver,
					verifyAllPossibleCasesForSMIMEData
							.get("GreenPadlockSubject"),
					verifyAllPossibleCasesForSMIMEData
							.get("EnhancedEncryptionMessage"));
		log.info("****************Navigating back to menu drawer********************");
			commonFunction.navigateBackToMenuDrawer(driver);
			commonPage.verifyGreenPadlock(driver,
					verifyAllPossibleCasesForSMIMEData.get("Account1"));
			commonPage.verifyPadlockToast(driver,
					verifyAllPossibleCasesForSMIMEData
							.get("GreenPadlockToastText"));
			commonPage.verifyViewDetailsInToast(driver,
					verifyAllPossibleCasesForSMIMEData.get("GreenIconText"),
					verifyAllPossibleCasesForSMIMEData.get("Account1"),
					"EnhancedEncryptionRecipient");
			commonFunction.navigateBackToMenuDrawer(driver);

			inbox.verifyGmailAppInitState(driver);
			// As per Aravind's suggestion Grey is not required
			
			 * commonPage.verifyDetailsFromRespectiveAccount(driver,
			 * verifyAllPossibleCasesForSMIMEData.get("GrayPadlockSubject"),
			 * verifyAllPossibleCasesForSMIMEData
			 * .get("StandardEncryptionMessage"));
			 * commonFunction.navigateBackToMenuDrawer(driver);
			 
			commonPage.verifyGrayPadlock(driver,
					verifyAllPossibleCasesForSMIMEData.get("Account2"));
			commonPage.verifyPadlockToast(driver,
					verifyAllPossibleCasesForSMIMEData
							.get("GrayPadlockToastText"));
			commonPage.verifyViewDetailsInToast(driver,
					verifyAllPossibleCasesForSMIMEData.get("GrayIconText"),
					verifyAllPossibleCasesForSMIMEData.get("Account2"),
					"StandardEncryptionRecipient");
			commonFunction.navigateBackToMenuDrawer(driver);

			inbox.verifyGmailAppInitState(driver);
			
			 * commonPage.verifyDetailsFromRespectiveAccount(driver,
			 * verifyAllPossibleCasesForSMIMEData.get("RedPadlockSubject"),
			 * verifyAllPossibleCasesForSMIMEData.get("NoEncryptionMessage"));
			 * commonFunction.navigateBackToMenuDrawer(driver);
			 
			commonPage.verifyRedPadlock(driver,
					verifyAllPossibleCasesForSMIMEData.get("Account3"));
			commonPage.verifyPadlockToast(driver,
					verifyAllPossibleCasesForSMIMEData
							.get("RedPadlockToastText"));
			commonPage.verifyViewDetailsInToast(driver,
					verifyAllPossibleCasesForSMIMEData.get("RedIconText"),
					verifyAllPossibleCasesForSMIMEData.get("Account3"),
					"NoEncryptionRecipient");*/
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAllPossibleCasesForSMIME");
		}
	}
	
	/**
	 * @TestCase 16 OODwarningForOutsideTheDomainReplies**
	 *
	 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 */	
	
	@Test(enabled = true, priority = 114)
	   public void OODwarningForOutsideTheDomainReplies() throws Exception {
			
			driver = map.get(Thread.currentThread().getId());
			try {
				log.info("*****************Started testcase ::: OOD warning*************************");
				Map<String, String> verifySmime = commonPage
						.getTestData("OODwarningForOutsideTheDomainReplies");

				 inbox.verifyGmailAppInitState(driver);
				 
				 if(!CommonFunctions.isAccountAlreadySelected(verifySmime.get("SmimeAccount")))
				 userAccount.switchMailAccount(driver, verifySmime.get("SmimeAccount"));
				 inbox.openMailUsingSearch(driver, verifySmime.get("MailWithOODWarning"));
				 inbox.verifyOODwarning(driver);
				 /*inbox.addAccountOODwarningForOutsideTheDomainReplies(driver);
				 inbox.sendMailToaddAccountOODwarningForOutsideTheDomainReplies(driver);
				 inbox.verifyOODwarningForOutsideTheDomainReplies(driver);
			 */
				// driver.navigate().back();
				 driver.navigate().back();
				 driver.navigate().back();
				 driver.navigate().back();
				 log.info("*****************Completed testcase ::: OOD warning*****************************");
			}
			catch (Throwable e) {
				commonPage.catchBlock(e, driver, "OODwarningForOutsideTheDomainReplies");
			}
}
}
