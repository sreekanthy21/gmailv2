package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_ApkInstallUninstall extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void ExecuteAllApkInstallUninstall() throws Exception{
		verifyDowngradeOfGmailApp();
		verifyUpgradeOfGmailApp();
	}*/
	
	/**
	* @TestCase  5: Verify downgrade of gmail App
	* @Pre-condition : Previous APK should be available 
	*  @author batchi
	*  GMAT 2
	* @throws: Exception
	*/
	@Test(groups = {"P0"}, enabled=true, priority=128)
	public void verifyDowngradeOfGmailApp() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyDowngradeOfGmailApp ******************************");
			inbox.verifyGmailAppInitState(driver);
			userAccount.verifyGmailAppVersionDowngrade(driver);
			log.info("***************** Completed testcase ::: verifyDowngradeOfGmailApp ******************************");
			//driver.navigate().back();
			//driver.navigate().back();
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyDowngradeOfGmailApp");
			//e.printStackTrace();
	}
	}
	
	/**Testcase for Apk Install/Upgrade
	 * 
	 * @throws Exception
	 * GMAT 1
	 * @author Bindu
	 */
	@Test(groups = {"P0"}, enabled=true, priority=129)
	public void verifyUpgradeOfGmailApp() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyUpgradeOfGmailApp ******************************");
			Map<String, String> verifyUpgradeOfGmailAppData = commonPage
					.getTestData("verifySettingsOption1");
			inbox.verifyGmailAppInitState(driver);
			//inbox.addingSignature(driver);
			userAccount.verifyGmailAppVersionUpgrade(driver);
			if(!CommonFunctions.isAccountAlreadySelected(verifyUpgradeOfGmailAppData.get("SwitchAccountId")))
			userAccount.switchMailAccount(driver,verifyUpgradeOfGmailAppData.get("SwitchAccountId") );
			//inbox.verifySignature(driver, verifyUpgradeOfGmailAppData.get("Signature"));
			log.info("***************** Completed testcase ::: verifyUpgradeOfGmailApp ******************************");
			driver.navigate().back();
		}catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyUpgradeOfGmailApp");
	}
}

	
	/**
	* @TestCase  5: Verify downgrade of gmail App
	* @Pre-condition : Previous APK should be available 
	*  @author batchi
	*  GMAT 2
	* @throws: Exception
	*/
	@Test(groups = {"P0"}, enabled=true, priority=1)
	public void verifyDowngradeOfGmailGoApp() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyDowngradeOfGmailApp ******************************");
			inbox.verifyGmailAppInitState(driver);
			userAccount.verifyGmailAppVersionDowngradeGmailGo(driver);
			log.info("***************** Completed testcase ::: verifyDowngradeOfGmailApp ******************************");
			//driver.navigate().back();
			//driver.navigate().back();
		}catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyDowngradeOfGmailApp");
			//e.printStackTrace();
	}
	}

	/**Testcase for Apk Install/Upgrade
	 * 
	 * @throws Exception
	 * GMAT 1
	 * @author Bindu
	 */
	@Test(groups = {"P0"}, enabled=true, priority=2)
	public void verifyUpgradeOfGmailGoApp() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("***************** Started testcase ::: verifyUpgradeOfGmailApp ******************************");
			Map<String, String> verifyUpgradeOfGmailAppData = commonPage
					.getTestData("verifySettingsOption1");
			inbox.verifyGmailAppInitState(driver);
			inbox.addingSignature(driver);
			userAccount.verifyGmailAppVersionUpgradeGmailGo(driver);
			if(!CommonFunctions.isAccountAlreadySelected(verifyUpgradeOfGmailAppData.get("SwitchAccountId")))
			userAccount.switchMailAccount(driver,verifyUpgradeOfGmailAppData.get("SwitchAccountId") );
			inbox.verifySignature(driver, verifyUpgradeOfGmailAppData.get("signature"));
			log.info("***************** Completed testcase ::: verifyUpgradeOfGmailApp ******************************");
			driver.navigate().back();
		}catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyUpgradeOfGmailApp");
	}
}


}
