package com.etouch.mobile.gmail;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import com.etouch.mobile.gmail.HtmlEmailSender;

import static com.etouch.mobile.gmail.TraveAndPurchases.*;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = {
    "GmailAppData=testData"
})

public class RegressionLegacyDF extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	SoftAssert softAssert = new SoftAssert();
	boolean isGmailWelComePageDisplayed;
	boolean flag;
	String path = System.getProperty("user.dir");
	Process p;
	Android_Gmail_Search search = new Android_Gmail_Search();
	Android_Gmail_Accounts accounts = new Android_Gmail_Accounts();
	Android_Gmail_ChromeCustom_Tabs enabledisableChrome = new Android_Gmail_ChromeCustom_Tabs();
	Android_Gmail_Compose compose = new Android_Gmail_Compose();
	Android_Gmail_MailSync mailSync= new Android_Gmail_MailSync();
	Android_Gmail_Notification notifications = new Android_Gmail_Notification();
	Android_Gmail_ThreadConversationList tConv = new Android_Gmail_ThreadConversationList();
	Android_Gmail_ConversationView convView = new Android_Gmail_ConversationView();
	Android_Gmail_AndroidNMultiWindow androidN = new Android_Gmail_AndroidNMultiWindow();
	Android_Gmail_AcctSwitcherNavDrawer navSwitcherDrawer = new Android_Gmail_AcctSwitcherNavDrawer();
	Android_Gmail_HelpAndFeedback helpFeedback = new Android_Gmail_HelpAndFeedback();
	Android_Gmail_MailAction mailAction = new Android_Gmail_MailAction();
	Android_Gmail_All_Ads_Related ads = new Android_Gmail_All_Ads_Related();
	Android_Gmail_Settings settings = new Android_Gmail_Settings();
	Android_Gmail_Gmailify gmailify = new Android_Gmail_Gmailify();
	Android_Gmail_RichTextFormat richText = new Android_Gmail_RichTextFormat();
	Android_Gmail_RSVP rsvp = new Android_Gmail_RSVP();
	Android_Gmail_SpamFeatures spamFeatures = new Android_Gmail_SpamFeatures();
	String Filepath = System.getProperty("user.dir"); 
	Android_Gmail_Spoofing spoofing = new Android_Gmail_Spoofing();	
	Android_Gmail_Draft drafts = new Android_Gmail_Draft();
	Android_Gmail_Sync sync = new Android_Gmail_Sync();
	Android_Gmail_A11Yandi18n a11y = new Android_Gmail_A11Yandi18n();
	Android_Gmail_PriorityInbox pi = new Android_Gmail_PriorityInbox();
	Android_Gmail_LauncherShortCut launch = new Android_Gmail_LauncherShortCut();
	Android_Gmail_Snooze snooze = new Android_Gmail_Snooze();
	Android_Gmail_ApkInstallUninstall apk = new Android_Gmail_ApkInstallUninstall();
	Android_Gmail_SMIME_TLS_DKIM_SPF smime = new Android_Gmail_SMIME_TLS_DKIM_SPF();
	Android_Gmail_AllInbox allinbox = new Android_Gmail_AllInbox();
	HtmlEmailSender mail = new HtmlEmailSender();
	Android_Gmail_TravelAndPurchases travelAndExpenses = new Android_Gmail_TravelAndPurchases();

	/**--Add new Gmail account(Consumer account) to app
	 * @Pre condition - If this test case is executing individually delete only gm.gig1.auto@gmail.com account	   
	 * @throws Exception
	 */
	@Test(priority = 1)
	public void verifyAddingNewAccountGmail()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();
		try{
			log.info("*****************Started testcase ::: verifyAddingNewGmailAccount*************************");
			accounts.verifyAddingNewGmailAccount();
			log.info("*****************Completed testcase ::: verifyAddingNewGmailAccount*************************");
			//  addAccountsInternal();
		}catch(Exception e){
			UserAccount.launchGmailApp(driver);
			commonPage.catchBlock(e, driver, "verifyAddingNewAccountGmail");
		}
	}

	    
	    /**--TC: Test Account sync works for Google accounts 
	     * 	   
	     * @throws Exception
	     */
	    @Test(priority = 2)
	    public void verifySyncAccount()throws Exception{
	    	driver = map.get(Thread.currentThread().getId());
	    	UserAccount.launchGmailApp(driver);
  			Capabilities s=driver.getCapabilities();
	    	version = s.getVersion();
 	    	try{
	  			mailSync.testAccountSyncWorksForGoogleAccounts();
 	    	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
	  		  commonPage.catchBlock(e, driver, "verifySyncAccount");
	  	  }
	    }  


	    /**-- Navigate to settings
	     * 
	     * @throws Exception
	     */
	    @Test(priority = 3)
		   public void verifyNavigateSettings()throws Exception{
		  		driver = map.get(Thread.currentThread().getId());
		  		UserAccount.launchGmailApp(driver);
		  		Capabilities s=driver.getCapabilities();
		    	version = s.getVersion();
		  	 try{
		  		 userAccount.addAccountsInternal();
		  		 log.info("***************Started testcase :: Navigate to settings ***********");
		  		 settings.navigateToSettings();
		  	     log.info("***************Completed testcase :: Navigate to settings ***********");
		  	 }catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		  		 commonPage.catchBlock(e, driver, "verifyNavigateSettings");
		  	 }
		   }
	    
	    /**-- Discard Draft
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 4)
		   public void verifyDiscardDraft()throws Exception{
		   	driver = map.get(Thread.currentThread().getId());
		   	UserAccount.launchGmailApp(driver);
		   	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
		   	try{
		   		log.info("******************Started testcase ::: Discard Drafts *****************");
		        drafts.verifyDiscardDraft();
		        log.info("*****************Completed testcase ::: Discard Drafts ************");

		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifyDiscardDraft");
		   	}
		   }
		   
		   /**-- Archive and Delete multiple mails
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 5)
		   public void verifyArchiveDeleteMultipleMails()throws Exception{
			 	driver = map.get(Thread.currentThread().getId());
			 	UserAccount.launchGmailApp(driver);
			   	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
		   		tConv.verifyArchiveAndDeletingMultipleMails();
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifyArchieveDeleteMultipleMails");
		   	}
		   }
		   
		    /**-- Help Functionality
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 6)
			public void verifyHelpFunctionality()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
						helpFeedback.verifyHelpAndFeedBackTab();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyHelpFunctionality");
				}
			}

		    /**-- Feedback works
		     * 
		     * @throws Exception
		     */
			@Test(priority = 7, enabled = true)
			public void verifyFeebackWorks()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					helpFeedback.verifyHelpAndFeedbackSection();
					driver.navigate().back();
					driver.navigate().back();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyFeebackWorks");
				}
			}

			/**-- Google Apps in Drawer
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 8)
		    public void verifyGoogleAppsinDrawer()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		        Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		accounts.GoogleAppsInDrawer(driver);
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyGoogleAppsinDrawer");
		    	}
		    }
			
			
		    /**-- Archive mail(Gmail only)
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 9)
		    public void verifyArchiveMail()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		        Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.verifyArchiveMail();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyArchiveMail");
		    	}
		    }

		    /**-- Star mail
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 10)
		    public void verifyStarMail()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		        Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.verifyMailMarkedAsStar();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyStarMail");
		    	}
		    }
		    
		    /**-- Unstar mail
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 11)
			public void verifyUnstarMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					mailAction.verifyMailUnStar();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyUnstar");
				}
			}
		    
		    /**-- Mark mail read / unread
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 12)
		    public void verifyMailReadUnread()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.markMailReadUnread();
		    	//	driver.navigate().back();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyMailReadUnread");
		    	}
		    }

		    /**-- Delete Mail and check in Trash
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 13)
		    public void verifyDeleteMail()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.deleteMailCheckTrash();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyDeleteMailTrash");
		    	}
		    }

		    /**-- All mails in Trash/Spam gets deleted when "Empty Trash Now/Empty Spam Now"
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 14)
		    public void verifyEmptyTrashSpamFunctionality()throws Exception{
		        driver = map.get(Thread.currentThread().getId());
		        UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.verifyTrashandSpamFolders();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyEmptyTrashSpamFunctionality");
		    	}
		    }
		    
		    /**-- Change Labels
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 15)
			public void verifyChangeLabels()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					mailAction.changeLabels();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyChangeLabels");
				}
			}

		    /**-- Move mail to other folder
		     * 
		     * @throws Exception
		     */
			@Test(priority = 16)
			public void verifyMailMoveToFolder()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					mailAction.verifyMailMoveToOtherFolder();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyMoveMailToFolder");
				}
			}

			/**-- Mark Mail important/not important
			 * 
			 * @throws Exception
			 */
			@Test(priority = 17)
			public void verifyMailMarkImportantUnImp()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					mailAction.verifyMarkAsImportantInCV();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyMailMarkImportant");
				}
			}

			/**-- Report Spam
			 * 
			 * @throws Exception
			 */
			@Test(priority = 18)
			public void verifyReportSpamGmailAcct()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					mailAction.reportSpam();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyReportSpamGmail");
				}
			}
			
			/**-- Print / Print all (multiple thread)
			 * 
			 * @throws Exception
			 */
			@Test(priority = 19)
		    public void verifyPrintFunctionality()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
		    		 mailAction.verifyMailSavedAsPDFPrint();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyTwoAdsPromotionsAndSocial");
		    	}
		    }

			/**-- Able to Mute a conversation
			 * 
			 * @throws Exception
			 */
		    @Test(priority = 20)
		    public void verifyUserAbleToMute()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.verifyUserIsAbleToMuteAConversation();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyTwoAdsPromotionsAndSocial");
		    	}
		    }
		    
		    /**-- Open Close Navigation Drawer
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 21)
			public void verifyOpenCloseNavDrawer()throws Exception{
			driver = map.get(Thread.currentThread().getId());
			UserAccount.launchGmailApp(driver);
			Capabilities s=driver.getCapabilities();
			version = s.getVersion();
			try{
					navSwitcherDrawer.verifyOpenAndCloseDrawer();
				
			}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
				commonPage.catchBlock(e, driver, "verifyOpenCloseNavigationDrawer");
			}
		}

		/**-- Account Switcher Drawer
		 *     
		 * @throws Exception
		 */
		@Test(priority = 22)
		public void verifyAccountSwitcherDrawer()throws Exception{
			driver = map.get(Thread.currentThread().getId());
			UserAccount.launchGmailApp(driver);
			Capabilities s=driver.getCapabilities();
			version = s.getVersion();
			try{
				navSwitcherDrawer.verifyAccountSwitcherAndDrawer();
			}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
				commonPage.catchBlock(e, driver, "verifyAccountSwitcherDrawer");
			}
		}
		
		/**-- Smart reply suggestions
		 * 
		 * @throws Exception
		 */
		@Test(priority = 23)
		   public void verifySmartReplySuggestions()throws Exception{
		   	driver = map.get(Thread.currentThread().getId());
		   	UserAccount.launchGmailApp(driver);
		   	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
		   	try{
		   		convView.verifySmartReplySuggestions();
		   	       // driver.navigate().back();
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifySmartReplySuggestions");
		   	}
		   }
		   
			/**-- Smart reply suggestions after discard
			 * 
			 * @throws Exception
			 */
		   @Test(priority = 24)
		   public void verifySmartReplyAfterDiscard()throws Exception{
		   	driver = map.get(Thread.currentThread().getId());
		   	UserAccount.launchGmailApp(driver);
		   	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
		   	try{
		           convView.verifySmartReplySuggestionsAfterDiscarding();
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifySmartReplyAfterDiscard");
		   	}
		   }
		   
		   /**-- Smart reply suggestions after replying
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 25)
		   public void verifySmartReplyAfterReplying()throws Exception{
		   	driver = map.get(Thread.currentThread().getId());
		   	UserAccount.launchGmailApp(driver);
		   	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
		   	try{
		           log.info("***************** Started testcase ::: verifySmartReplySuggestionsAfterReply ******************************");
		           convView.verifySmartReplySuggestionsAfterReplying();
		           
		          // UserAccount.verifySmartReplySuggestionsNotDisplayed(driver);
		           log.info("***************** Completed testcase ::: verifySmartReplySuggestionsAfterReply ******************************");
		           //driver.navigate().back();
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifySmartReplyAfterReplying");
	   	}
		   }
		   
		   
		   /**-- Enable/Disable Smart Reply
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 26)
		   public void verifySmartReplyEnableDisable()throws Exception{
		   	driver = map.get(Thread.currentThread().getId());
		   	UserAccount.launchGmailApp(driver);
		   	Capabilities s=driver.getCapabilities();
			version = s.getVersion();
		   	try{
		           log.info("***************** Started testcase ::: verifySmartReplyEnableDisable ******************************");
		           settings.verifySmartReplyEnableDisable();
		           log.info("***************** Completed testcase ::: verifySmartReplyEnableDisable ******************************");
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifySmartReplyEnableDisable");
		   	}
		   }
		   
		   /**-- Compose and send email with CC,BCC fields
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 27)
		   public void verifyComposeEMail()throws Exception{
		    	driver = map.get(Thread.currentThread().getId());
		    	UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			   try{
		   			compose.verifyComposeAndSendMail();
		   	}catch(Exception e){
	    		UserAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifyComposeCCBCCSendMail");
		}
		   }

		
		   /**-- Verifying two ads displayed
		    * 
		    * @throws Exception
		    */
		   @Test(priority = 28)
			public void verifyTwoAds()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					ads.verifyTwoAdsDisplayInPromotionSocial();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyTwoAdsPromotionsAndSocial");
				}
			}
			
		   /**-- Expand ad i icon
		    * 
		    * @throws Exception
		    */
			@Test(priority = 29)
			public void verifyExpandAdInfo()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					ads.verifyExpandAndControlAds();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyExpandAdInfoControlAds");
				}
			}
			
			/**-- Verify promo tab design
			 * 
			 * @throws Exception
			 */
			@Test(priority = 30)
			public void verifyPromoTabDesign()throws Exception{
				UserAccount.launchGmailApp(driver);
				driver = map.get(Thread.currentThread().getId());
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					ads.verifyPromotionsTabRedesignForADS();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyExpandAdInfoControlAds");
				}
			}
			
			/**-- Delete, Forward, Star ad
			 * 
			 * @throws Exception
			 */
			@Test(priority = 31)
			public void verifyDeleteForwardStarAd()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					ads.verifyAdvertisementFunctionality();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyDeleteForwardStarAds");
				}
			}
			
			/**-- Safe links V1
			 * 
			 * @throws Exception
			 */
			@Test(priority = 32)
			public void verifySafeLinksV1()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					convView.verifySafeLinksFunctionalityv1();
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySafeLinksV1");
				}
			}
			
			/**-- Safe links V2
			 * 
			 * @throws Exception
			 */
			@Test(priority = 33)
			public void verifySafeLinksV2()throws Exception{
				 driver = map.get(Thread.currentThread().getId());
				 UserAccount.launchGmailApp(driver);
				 Capabilities s=driver.getCapabilities();
				 version = s.getVersion();	
				try{
					convView.verifySafeLinksFunctionalityv2();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySafeLinksV2");
				}
			}
			
			/**-- Verify Suspicious banner
			 * 
			 * @throws Exception
			 */
			@Test(priority = 34)
			   public void verifyReportNotSuspiciousBanner() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						inbox.SendMail(driver);
						log.info("*****************Started testcase ::: verifySuspiciousBanner*************************");
						inbox.verifyGmailAppInitState(driver);
						inbox.verifySuspiciousBannerDisplay(driver);
					    log.info("*****************Completed testcase ::: verifySuspiciousBanner******************************");	
					} catch (Exception e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyReportNotSuspiciousBanner");
					}
				}
		   
			/**-- Verify Suspicious banner
			 * 
			 * @throws Exception
			 */
			@Test(priority = 35)
			   public void verifyReportNotSuspiciousOption() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						log.info("*****************Started testcase ::: verifyReportNotSuspiciousOption*************************");
						//inbox.verifyGmailAppInitState(driver);
						inbox.verifyReportNotSuspiciousOption(driver);
					    log.info("*****************Completed testcase ::: verifyReportNotSuspiciousOption******************************");	
					} catch (Exception e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyReportNotSuspiciousOption");
					}
				}
			
			/**--Verify Conversation mail
			 * 
			 * @throws Exception
			 */
			@Test(priority = 36)
		    public void verifyMailConversationView()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
			    	Capabilities s=driver.getCapabilities();
					version = s.getVersion();
			try{
					convView.verifyMailConversationView();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    	commonPage.catchBlock(e, driver, "verifyMailConversationView");
		    }
		    }
			
			/**-- Verify turn off sync
			 * 
			 * @throws Exception
			 */
		    @Test(priority = 37)
		    public void verifyTurnOffSync()throws Exception{
		    	driver = map.get(Thread.currentThread().getId());
		    	UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    	mailSync.turnOffAccountSync();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyTwoAdsPromotionsAndSocial");
		    	}
		    }
		    
		    /**-- verify swipe down refresh
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 38)
		    public void verifySwipeDownWorks()throws Exception{
		    	UserAccount.launchGmailApp(driver);
			driver = map.get(Thread.currentThread().getId());
			UserAccount.launchGmailApp(driver);
			Capabilities s=driver.getCapabilities();
			version = s.getVersion();	
			try{
		    	mailSync.verifySwipeDownToRefreshWorks();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyTwoAdsPromotionsAndSocial");
		    	}
		    }

		    /**-- verify Mail snippet
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 39)
			public void VerifyMailSnippetTL()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					tConv.verifyMailSnippetThreadList();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyMailSnippetInThreadList");
				}
			}	
		    
		    /**-- Block and unblock sender messages
		     * 
		     * @throws Exception
		     */
		    @Test(priority = 40)
			public void verifyBlockUnblockSender()throws Exception{
		    	driver = map.get(Thread.currentThread().getId());
		    	UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
		    	try{
					spamFeatures.verifyBlockAndUnblockSenderWorks();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyBlockUnblockSender");
				}
			}
		   
		    /**-- verify remove unsyncable labels spam
		     * 
		     * @throws Exception
		     */
			@Test(priority = 41)
			public void verifyRemoveUnsyncableLabelsSpam()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					spamFeatures.removeUnsyncableLablesSpam();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyBlockUnblockSender");
				}
			}
			
			/**--Search Functionality Gmail
			 *  
			 * @throws Exception
			 */
			@Test(priority = 42)
			public void verifySearchFunctionalityGmail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					search.verifySearchFunctionality();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySearchFunctionality");
				}
			}
			
			/**--Verify Report Spam with mute option
			 * 
			 * @throws Exception
			 */
			@Test(priority = 43)
			public void verifyReportSpamMuteOption()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					spamFeatures.verifyReportSpamMuteOption();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyReportSpamMuteOption");
				}
			}
			
			/**--SuperCollapse
			 * 
			 */
			@Test(priority = 44)
			public void verifySuperCollapse()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
				convView.verifySuperCollapseForMultiThreadedMail();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySuperCollapse");
				}
			}
			
			/**--verify compose mail with attachments
			 * 
			 * @throws Exception
			 */
			@Test(priority = 45)
		    public void verifyComposeMailWithAttachments()throws Exception{
		    	driver = map.get(Thread.currentThread().getId());
		    	UserAccount.launchGmailApp(driver);
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	// MailManager.DeleteAllMailsInbox();
			    	try {
		    		compose.composeAndSendMailForGmailAccountWithAttachments();
		    	}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyComposeMailWithAttachmentsAndSend");
		    	}
		    }
			
			/**Verify Rich Text formatting
			 * 
			 * @throws Exception
			 */
			@Test(priority = 46)
			public void verifyRichTextFormatting()throws Exception {
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					richText.richTextFormattingFeatures();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyRichTextFormatting");
				}
			}
			
			/**--Verify compose mail saved as draft
			 * 
			 * @throws Exception
			 */
			@Test(priority = 47)
			public void verifyComposeMailSavedAsDraft()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					drafts.composeMailAndSaveAsDraft();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyComposeMailSavedAsDraft");
				}
			}
			
			/**--Verify mail sent from a drafted mail
			 * 
			 * @throws Exception
			 */
			@Test(priority = 48)
			public void verifyEditDraftMailAndSendMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					drafts.verifyEditDraftAndSendMail();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyEditDraftMailAndSendMail");
				}
			}
			
			/**--Inbox Type settings in Account settings
			 * 
			 */
			@Test(priority=49)
			public void verifyInboxTypes()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.verifyInboxTypesSettings();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyInboxTypes");
				}
			}
			
			/**--PI option remembered
			 * 
			 */
			@Test(priority=50)
			public void verifyPIOptionRemember()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.verifyPIOptionRemembered();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyPIOptionRemember");
				}
			}
			
			/**--PI option remembered after switching
			 * 
			 */
			@Test(priority=51)
			public void verifyPIOptionRememberSwitching()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.verifyPIOptionRememberedSwitching();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyPIOptionRememberSwitching");
				}
			}
			
			/**--Exit app PI
			 * 
			 */
			@Test(priority=52)
			public void verifyExitAppPI()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.verifyExitAppPISection();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyExitAppPI");
				}
			}
			
			/**--PI option after notification
			 * 
			 */
			@Test(priority=53)
			public void verifyPIAfterNotification()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.verifyPIAfterNotification();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyPIAfterNotification");
				}
			}
			
			/**
			 * Ensure that syncing all changes functions properly without errors
			 * @throws Exception
			 */
			@Test(priority=54)
			public void verifySyncingWithoutErrors()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifySyncingWithoutErrors();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySyncingWithoutErrors");
				}
			}
			
			/**
			 * Able to create a draft from reply to message in inbox.
			 * @throws Exception
			 */
			@Test(priority=55)
			public void verifyAbleToCreateDraftFromReply()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAbleToCreateDraftFromReply();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAbleToCreateDraftFromReply");
				}
			}
			
			/**
			 * Auto-Sync off, verify that tips are inserted in expected locations
			 * @throws Exception
			 */
			@Test(priority=56)
			public void verifyAutoSyncOffTipsInserted()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAutoSyncOffTipsInserted();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAutoSyncOffTipsInserted");
				}
			}
			
			/**
			 * Turning on Auto-Sync from Conversation List message
			 * @throws Exception
			 */
			@Test(priority=57)
			public void verifyAutoSyncConversationList()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAutoSyncConversationList();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAutoSyncConversationList");
				}
			}
			
			/**
			 * Turning on Auto-Sync from Account Settings
			 * @throws Exception
			 */
			@Test(priority=58)
			public void verifyAutoSyncAccountSettings()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAutoSyncAccountSettings();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAutoSyncAccountSettings");
				}
			}
			
			/**
			 * Turn on Auto-Sync dialog should detect device type
			 * @throws Exception
			 */
			@Test(priority=59)
			public void verifyAutoSyncDetectDeviceType()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAutoSyncDetectDeviceType();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAutoSyncDetectDeviceType");
				}
			}
			
			/**
			 * Turning on Auto-Sync from Label Settings
			 * @throws Exception
			 */
			@Test(priority=60)
			public void verifyAutoSyncLabelSettings()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyAutoSyncLabelSettings();
				}catch(Exception e){
					UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyAutoSyncLabelSettings");
				}
			}
			
			/**
			 * Verify Unsent in Outbox error message
			 * @throws Exception
			 */
			@Test(priority=61)
			public void verifyUnsentOutboxErrorMessage()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					sync.verifyUnsentOutboxErrorMessage();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyUnsentOutboxErrorMessage");
				}
			}
			
			/**
			 * Evoke Snooze from CV and threadlist
			 * @throws Exception
			 */
			@Test(priority=62)
			public void EvokeSnoozeCVTL()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					snooze.EvokeSnoozeCVTL();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "EvokeSnoozeCVTL");
				}
			}
			
			/**
			 * UnSnooze a snoozed message that was set to expire in +5 mins.
			 * @throws Exception
			 */
			@Test(priority=63)
			public void SearchSnoozedMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					snooze.SearchSnoozedMail();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "SearchSnoozedMail");
				}
			}
			
			/**
			 * Searching for is:snoozed
			 * @throws Exception
			 */
			@Test(priority=64)
			public void UnsnoozeMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					snooze.UnsnoozeMail();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "UnsnoozeMail");
				}
			}
			
			/**
			 *Remembering custom last snoozed date and time in snooze dialog
			 * @throws Exception
			 */
			@Test(priority=65)
			public void RemCustomeLastSnoozeDateTime()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					snooze.RemCustomeLastSnoozeDateTime();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "RemCustomeLastSnoozeDateTime");
				}
			}
			
			/**
			 *Verify display name for suspicious sender
			 * @throws Exception
			 */
			@Test(priority=66)
			public void verifyDisplayNameSuspiciousSender()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					spoofing.verifyDisplayNameSuspiciousSender();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyDisplayNameSuspiciousSender");
				}
			}
			
			/**
			 *Verify display name as email address
			 * @throws Exception
			 */
			@Test(priority=67)
			public void verifyDisplayNameEmailAddress()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					spoofing.verifyDisplayNameEmailAddress();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyDisplayNameEmailAddress");
				}
			}
			
			/**
			 *Verify display name on notification
			 * @throws Exception
			 */
			@Test(priority=68)
			public void verifyDisplayNameNotification()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					spoofing.verifyDisplayNameNotification();
					String[] caribou = new String[]{"adb", "shell", "am", "instrument", "-e", "account", "caribou.dnis.receiver@gmail.com", "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
				    p = new ProcessBuilder(caribou).start();
				    p.destroy();
				    log.info("Removing account caribou google account manager apk");
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyDisplayNameNotification");
				}
			}
			
			
			/**
			 *Compose and send email from Gmail account with "Insert From Drive "
			 * @throws Exception
			 */
			@Test(priority = 69)
			public void verifyAttachmentSharingOptions() throws Exception{
			driver = map.get(Thread.currentThread().getId());
			UserAccount.launchGmailApp(driver);
			Capabilities s=driver.getCapabilities();
			version = s.getVersion();
			try{
				compose.verifyAttachmentSharingOptions();
			}catch(Throwable e){
	    		UserAccount.launchGmailApp(driver);
				commonPage.catchBlock(e, driver, "verifyAttachmentSharingOptions");
			}
		}
			
			/**
			 *Attachments are not lost at rotation and able to scroll the page
			 * @throws Exception
			 */
			   @Test(priority = 70)
			    public void verifyAttachmentsOnRotation()throws Exception{
				   driver = map.get(Thread.currentThread().getId());
				   UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
				   try{
			    	 compose.verifyAttachmentsUponRotating();
			    	}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyAttachmentsOnRotation");
			    	}
			    }
			   
				/**
				 *Compose mail with multiple attachments and inserted drive chips , save as draft
				 * @throws Exception
				 */
			    @Test(priority = 71)
			    public void verifyMailSavedAsDraft()throws Exception{
			        driver = map.get(Thread.currentThread().getId());
			        UserAccount.launchGmailApp(driver);
			        Capabilities s=driver.getCapabilities();
					version = s.getVersion();
			    	try{
			    		drafts.composeMailWithMultipleAttachmentsAndAnsertedDriveChips();
			    	}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyMailSavedAsDraft");
			    	}
			    }
			
			   /**
				 *Forward an html email
				 * @throws Exception
				 */
				   @Test(priority = 72)
				    public void verifyForwardHTML()throws Exception{
					   driver = map.get(Thread.currentThread().getId());
					   UserAccount.launchGmailApp(driver);
						Capabilities s=driver.getCapabilities();
						version = s.getVersion();
					   try{
				    	 compose.verifyForwardHTML();
				    	}catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyForwardHTML");
				    	}
				    }
				   
				   /**
					 *Reply from Notification
					 * @throws Exception
					 */
				   @Test(priority = 73)
				    public void verifyReplyNotification()throws Exception{
				        driver = map.get(Thread.currentThread().getId());
				        UserAccount.launchGmailApp(driver);
				        Capabilities s=driver.getCapabilities();
						version = s.getVersion();
				    	try{
				    		compose.verifyReplyFromNotification();
				    	}catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyReplyNotification");
				    	}
				    }
				   
				   
				/**-- Reply to a message in airplane mode and trash the same
				 * 
				 * @throws Exception
				 */
				@Test(priority = 74)
				 public void verifyReplyMailTrashSame()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
						try{
							compose.verifyReplyMailTrashSame();
				   	 }catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyReplyMailTrashSame");
				    }
				    }	
				
				/**--Reply-to for Add another email address without reply-to
				 * 
				 * @throws Exception
				 */
				@Test(priority = 75)
				 public void verifyReplyToWithout()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
						try{
							compose.verifyReplyToWithout();
				   	 }catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyReplyToWithout");
				    }
				   }	
					
				/**--Forward mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 76)
				 public void verifyReplyTo()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
						try{
							compose.verifyReplyTo();
				   	 }catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyReplyTo");
				    }
				    }	
				
				/**--Reply/Reply All
				 * 
				 * @throws Exception
				 */
				@Test(priority = 77)
				public void verifyReplyReplyAllFunctionality()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						convView.verifyReplyAllFunctionality();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyReplyReplyAll");
					}
				}
				
				/**-Forward All mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 78)
				public void verifyForwardFunctionality()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						convView.verifyForwardingMailFunctionality();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyForwardFunctionality");
					}
				}
				
				/**--Open various types of attachments and drive chips for mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 79)
				public void verifyAttachments() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try {
						convView.verifyAttachments();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyAttachments");
					}
				}
				
				/**--Zooming Out Tables for Conversation View
				 * 
				 * @throws Exception
				 */
				@Test(priority = 80)
				public void verifyZoomingOutTables()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
					    convView.verifyZoomingOutTablesForConversationView();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyZoomingOutTables");
					}
				}
				
				/**--Verify calendar promotion in the conversation if the calendar app is not installed
				 * 
				 * @throws Exception
				 */
				@Test(priority = 81)
				public void verifyCalendarPromotionInConversation() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						convView.verifyCalendarPromotionInConversation();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver,
								"verifyCalendarPromotionInConversation");
					}
				}
				
				/**--Verify calendar promotion in the conversation if the calendar app is not used at all.
				 * 
				 * @throws Exception
				 */
				@Test(priority = 82)
				public void verifyCalendarPromotionInConversationAppNotUsed() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						convView.verifyCalendarPromotionInConversationAppNotUsed();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver,
								"verifyCalendarPromotionInConversationAppNotUsed");
					}
				}
				
				/**--Test Expanded Style Support
				 * 
				 * @throws Exception
				 */
				@Test(priority = 83)
				public void verifyStyleSupport() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						convView.verifyStyleSupport();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver,
								"verifyStyleSupport");
					}
				}
				
				/**--Verify Default and Priority Inbox list views and ads in Promotions
				 * 
				 * @throws Exception
				 */
				//Check this case for image verification for once    
				@Test(priority = 84)
				public void verifyDefaultPriorityInboxListViewsAndAdsPromo()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						tConv.verifyDefaultAndPriorityInboxListViews();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyDefaultPriorityInboxListViewsAndAdsPromo");
					}
				}
				
				/**--Swipe to Delete/archive and Undo
				 * 
				 * @throws Exception
				 */
				@Test(priority = 85)
				public void verifySwipeDeleteFunctionality()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						tConv.verifySwipeDeleteFunctionality();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifySwipeDeleteFunctionality");
					}
				}
				
				
				/**--Notification for Single and multiple message
				 * 
				 * @throws Exception
				 */
				@Test(priority = 86)
			    public void verifyNotificationsForSingleMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationElements();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationsForSingleMail");
			    	}
			    }
				
				/**--Notification turned off for sepecific account
				 * 
				 * @throws Exception
				 */
				@Test(priority = 87)
			    public void verifyNotificationOffForSpecificAccount()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationOffForSpecificAccount();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationOffForSpecificAccount");
			    	}
			    }
				
				/**--Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 88)
			    public void verifyNotificationReplyAndArchive()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationReplyAndArchive();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationReplyAndArchive");
			    	}
			    }
				
				/**--Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 89)
			    public void verifyNotificationImportantOnly()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationImportantOnly();
				}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationImportantOnly");
			    	}
			    }
				
				/**--Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 90)
			    public void verifyNotificationNone()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationNone();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationNone");
			    	}
			    }
				
				/**--Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 91)
			    public void verifyNotificationAllMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyNotificationAllMail();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyNotificationAllMail");
			    	}
			    }
				
				/**--Verify Notification actions - View mail - Reply / reply all / Delete or archive mail
				 * 
				 * @throws Exception
				 */
				@Test(priority = 92)
			    public void verifyImpNotificationAllMail()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
			    		notifications.verifyImpNotificationAllMail();
			}catch(Exception e){
	    				UserAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyImpNotificationAllMail");
			    	}
			    }
				
				/**--Add IMAP/POP/Exchange account to app
				 * 
				 * @throws Exception
				 */
				@Test(priority = 93)
				public void verifyAddingIMAPPOPAccounts()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						accounts.verifyAddingOtherAccounts();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyAddingIMAPPOPAccounts");
					}
				}
				
				/**--Report spam for imap account
				 * 
				 * @throws Exception
				 */
				@Test(priority = 94)
				public void verifyReportSpamIMAPAccount()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						mailAction.validateReportSpamForIMAPAccounts();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyReportSpamIMAPAccount");
					}
				}
				
				/**--Test Account sync works for Non Gmail accounts - IMAP / POP / Exchange
				 * 
				 * @throws Exception
				 */
				@Test(priority = 95)
				public void testAccountSyncWorksFornonGoogleAccounts() throws Exception  {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						mailSync.testAccountSyncWorksFornonGoogleAccounts();
			     } catch (Exception e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "testAccountSyncWorksFornonGoogleAccounts");

					}
				}
				
				/**--Verify New RSVP format displayed for Calendar invites
				 * 
				 * @throws Exception
				 */
				@Test(priority = 96)
				public void verifyRSVPDisplayed()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						rsvp.verifyNewRSVPFormatDisplayedForCalendarInvites();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyRSVPDisplayed");
					}
				}
				
				/**
				 * Search Functionality offline
				 * 
				 */
				@Test(priority = 97)
				public void verifySearchFunctionalityOfflineGmailIMAP()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						search.verifySearchFunctionalityOffline();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifySearchFunctionalityOffline");
					}
				}
				
				
				/**
				 * Unsupported swipe actions
				 * 
				 */
				@Test(priority = 98)
				public void verifyUnsupportedSwipeActions()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						tConv.verifyUnsupportedSwipeActions();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyUnsupportedSwipeActions");
					}
				}
				
				/**
				 * Verify All inbox basic functionality
				 * 
				 */
				@Test(priority = 99)
				public void verifyAllInboxBasicFunctionality() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try {
							allinbox.verifyAllInboxBasicFunctionality();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyAllInboxBasicFunctionality");		
			}
				}
				
				  /**-- General Settings
				   *   
				   * @throws Exception
				   */
				  @Test(priority = 100)
				  public void verifyGeneralAppSettings()throws Exception{
					  driver = map.get(Thread.currentThread().getId());
					  UserAccount.launchGmailApp(driver);
						Capabilities s=driver.getCapabilities();
						version = s.getVersion();
				  	try{
				  			settings.verifyGeneralSettings();
				  	}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
				  		 commonPage.catchBlock(e, driver, "verifyGeneralAppSettings");
				  	}
				  }
				
				  /**-- Account Settings
				   * 
				   * @throws Exception
				   */
				  @Test(priority = 101)
				  public void verifyAccountSpecificSettings()throws Exception{
				  	driver = map.get(Thread.currentThread().getId());
				  	UserAccount.launchGmailApp(driver);
				  	Capabilities s=driver.getCapabilities();
					version = s.getVersion();
				  	try{
				  		settings.verifyGmailAccountSpecificSettings();
				  	}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
				  		commonPage.catchBlock(e, driver, "verifyAccountSpecificSettings");
				  	}
				  }
				
				/**
				 * Enable/Disable Vacation Responder
				 * 
				 */
				@Test(priority = 102)
				public void enableDisableVacationResponder() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try {
							settings.enableDisableVacationResponder();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "enableDisableVacationResponder");		
			}
				}
				
				/**
				 * Enable Vacation Responder with "Send to my Contacts" option
				 * 
				 */
				@Test(priority = 103)
				public void enableVacationResponderWithSendToMyContactsOption() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try {
							settings.enableVacationResponderWithSendToMyContactsOption();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "enableVacationResponderWithSendToMyContactsOption");		
			}
				}
				
				/**
				 * Verify Signature On/Off
				 * 
				 */
				@Test(priority = 104)
				public void verifySignatureOnOff()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						settings.verifySignatureOnAndOff();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifySignatureOnOff");
					}
				}
				
				/**
				 *Swipe actions in General Settings
				 * 
				 */
				@Test(priority = 105)
				public void verifySwipeActions()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						settings.verifySwipeActions();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifySwipeActions");
					}
				}
				
				/**-- verify avatar and warning
				 * 
				 * @throws Exception
				 */
				@Test(priority = 106)
				public void verifyUnAuthAvatarWarning()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						spamFeatures.verifyMailWithUnauthenticatedAvatarAndWarning();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifySendEmailMultiWindow");
					}
				}
				
				/**-- Verify Gmailify flow
				 * 
				 * @throws Exception
				 */
				@Test(priority = 107)
				public void verifyGmailifyFlow()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						gmailify.verifyGmailifyFlow();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyGmailifyFlow");
					}
				}
				
				/**-- Gmailify in Account Setup flow
				 * 
				 * @throws Exception
				 */
				@Test(priority = 108)
				public void verifyTryGmailifyFlow()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						gmailify.verifyTryGmailifyFlow();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyTryGmailifyFlow");
					}
				}
				
				
				/**-- Gmailify flow for Password Providers
				 * 
				 * @throws Exception
				 */
				@Test(priority = 109)
				public void verifyGmailifyFlowPwdProviders()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						gmailify.verifyGmailifyFlowPwdProviders();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyGmailifyFlowPwdProviders");
					}
				}
				
				/**-- Label list for Gmailified account
				 * 
				 * @throws Exception
				 */
				@Test(priority = 110)
				public void verifyTheLabelListForGmailifiedAccount() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						gmailify.verifyTheLabelListForGmailifiedAccount();
					}catch(Throwable e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyTheLabelListForGmailifiedAccount");
					}
				}
				
				/**-- Certificate Transparency (IMAP/POP)
				 * 
				 * @throws Exception
				 */
				@Test(priority = 111)
				public void certificateTransparency() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						gmailify.certificateTransparency();
					}catch(Throwable e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "certificateTransparency");
					}
				}
				
				/**-- Enable/Disable Chrome Custom Tabs
				 * 
				 * @throws Exception
				 */
				@Test(priority = 112)
				public void verifyEnableDisableChromeCustomTabs()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						enabledisableChrome.verifyEnableDisableChromeTabs();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyEnableDisableChromeCustomTabs");
					}
				}
				
				/**-- Multiple lines of text with attachments
				 * 
				 * @throws Exception
				 */
				@Test(priority = 113)
				public void MultipleLinesAttachments()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try{
						compose.MultipleLinesAttachments();
					}catch(Exception e){
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "MultipleLinesAttachments");
					}
				}
				
				
				/**--"OOD warning" for outside the domain replies
				 * 
				 * This test case is developed to verify only OOD warning, please add the statement if required based on device
				 * @throws Exception
				 */
				@Test(priority = 114)
				   public void OODwarningForOutsideTheDomainReplies() throws Exception {
						driver = map.get(Thread.currentThread().getId());
						UserAccount.launchGmailApp(driver);
						Capabilities s=driver.getCapabilities();
						version = s.getVersion();	
					
						try {
							smime.OODwarningForOutsideTheDomainReplies();
						}
						catch (Throwable e) {
				    		UserAccount.launchGmailApp(driver);
							commonPage.catchBlock(e, driver, "OODwarningForOutsideTheDomainReplies");
						}
			}
				
				/**Verify Inbound and Outbound S/MIME
				 * 
				 * @throws Exception
				 */
				@Test(priority = 115)
				public void verifyInboundAndOutbound() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
						smime.verifyInboundAndOutbound();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyInboundAndOutbound");
					}
				}
				
				/**Verify icons are updated accordingly for S/MIME support
				 * 
				 * PreCondition: smime1@superpudu.com account should be added manually using device policy settings
				 * 
				 * Included view details(from verifyAllPossibleCasesForSMIME) also in this test case to save time
				 * 
				 * @throws Exception
				 */
				@Test(priority = 116)
				public void verifyIconsAreUpdated() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
					try {
							smime.verifyIconsAreUpdated();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyIconsAreUpdated");
					}
				}
				
				
				/**Verify all possible cases for S/MIME
				 * view details toast is verified in above case verifyIconsAreUpdated
				 * PreCondition: smime1@superpudu.com account should be added manually using device policy settings
				 * 
				 * @throws Exception
				 */
				@Test(priority = 117)
				public void verifyAllPossibleCasesForSMIME() throws Exception {
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
				try {
						smime.verifyAllPossibleCasesForSMIME();
					} catch (Throwable e) {
			    		UserAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyAllPossibleCasesForSMIME");
					}
				}

				/**-- Manage Accounts - Verify Remove account
				 * 
				 * @throws Exception
				 */
				@Test(priority = 118)
				 public void verifyRemoveAccount()throws Exception{
					driver = map.get(Thread.currentThread().getId());
					UserAccount.launchGmailApp(driver);
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();	
						try{
							accounts.verifyRemoveAccount();
				   	 }catch(Exception e){
				    		UserAccount.launchGmailApp(driver);
				    		commonPage.catchBlock(e, driver, "verifyRemoveAccount");
				    }
				    }
			
			/**
			 * Verify i18n for at least one language from CJK (Chinese, Japanese or Korean)
			 * @throws Exception
			 */
			@Test(priority = 119)
			public void verifyi18NCJK()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					//inbox.verifyGmailAppInit(driver);
					a11y.verifyi18NCJK();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyi18NCJK");
				}
			}
			
			/**
			 *
			 *Verify i18n for at least one language from French, Spanish, German, Russian	
			 * @throws Exception
			 */
			@Test(priority = 120)
			public void verifyi18FSGR()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					//inbox.verifyGmailAppInit(driver);
					a11y.verifyi18FSGR();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyi18FSGR");
				}
			}
			
			/**
			 * Verify i18n for at least one language from Bidi (Hebrew, Arabic)
			 * @throws Exception
			 */
			@Test(priority = 121)
			public void verifyi18HA()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					//inbox.verifyGmailAppInit(driver);
					a11y.verifyi18HA();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyi18HA");
				}
			}
			
			/**
			 *Verify i18n input data and input methods
			 * @throws Exception
			 */
			@Test(priority = 122)
			public void inputMethod_Arabic() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try {
					log.info("*****************Started testcase :::Verify target language is added in keyboard & input methods in device settings**************************");
					inbox.verifyGmailAppInitState(driver);
					/*inbox.inputMethod_Arabic(driver);
					inbox.inputMethod_Korean(driver);*/
					a11y.verifyInputMethods();
				} catch (Throwable e) {
					commonPage.catchBlock(e, driver, "Verify target language is added in keyboard & input methods in device settingsl");
				}
			}
			
			
			/**
			 * Verify i18n output data
			 * @throws Exception
			 */
			@Test(priority = 123, enabled = true)
			public void verifyOutputMethods()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					a11y.verifyOutputMethods();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyOutputMethods");
				}
			}
				
			/**
			 * Send email in multi window mode
			 * @throws Exception
			 */
			@Test(priority = 124, enabled = true)
			public void verifyMailSendMultiWindow()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					androidN.verifyMailSendMultiWindow();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyMailSendMultiWindow");
				}
			}
			
			/**
			 * Perform Archive/Delete/Move To, etc operations from TL View and CV
			 * @throws Exception
			 */
			@Test(priority = 125, enabled = true)
			public void verifyArchieveDeleteAndMoveToFromMultiWindow()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					androidN.verifyArchieveDeleteAndMoveToFromMultiWindow();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyArchieveDeleteAndMoveToFromMultiWindow");
				}
			}
			
			/**
			 * Verify multi window mode works as expected in Landscape mode
			 * @throws Exception
			 */
			@Test(priority = 126, enabled = true)
			public void verifyMailSendMultiWindowInLandscapeMode()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					androidN.verifyMailSendMultiWindowInLandscapeMode();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyMailSendMultiWindowInLandscapeMode");
				}
			}
			
			/**
			 * Nested notifications on Android N and perform Reply and Delete/Archive actions
			 * @throws Exception
			 */
			@Test(priority = 127, enabled = true)
			public void verifyMultipleMailsNotificationsReplyAndArchive()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					androidN.verifyMultipleMailsNotificationsReplyAndArchive();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyMultipleMailsNotificationsReplyAndArchive");
				}
			}
			
			/**
			 * Downgrade to Previous build and accounts re-syncs
			 * @throws Exception
			 */
			@Test(priority = 128)
			public void verifyDowngradeApplication()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					 log.info("***************** Started testcase ::: Downgrade ******************************");
					 	apk.verifyDowngradeOfGmailApp();
				        log.info("***************** Completed testcase ::: Downgrade ******************************");

				}catch(Exception e){
					commonPage.catchBlock(e, driver, "verifyDowngradeApplication");
				}
			}

			/**
			 * Verify APK installation/upgrade
			 * @throws Exception
			 */
			@Test(priority = 129)
			public void verifyUpgradeApplication()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
			        log.info("***************** Started testcase ::: ApkUpgrade ******************************");
					apk.verifyUpgradeOfGmailApp();
			        log.info("***************** Completed testcase ::: ApkUpgrade ******************************");
				}catch(Exception e){
					commonPage.catchBlock(e, driver, "verifyUpgradeApplication");
				}
			}
			
			/**
			 * Verify the launcher short cut
			 * @throws Exception
			 */
			@Test(priority = 130, enabled = true)
			public void verifyLauncherShortcut()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					launch.verifyLauncherShortcut();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyLauncherShortcut");
				}
			}
			
			/**
			 * Create and launch account shortcuts
			 * @throws Exception
			 */
			@Test(priority = 131, enabled = true)
			public void verifyCreateLauncherShortcut()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
				launch.verifyCreateLauncherShortcut();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyCreateLauncherShortcut");
				}
			}
			
			/**
			 * Deleted Account Shortcut
			 * @throws Exception
			 */
			@Test(priority = 132, enabled = true)
			public void verifyDeleteLuncherShortcut()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					launch.verifyDeleteLuncherShortcut();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyDeleteLuncherShortcut");
				}
			}		

			/**
			 * Re-enable the Disabled Shortcut
			 * @throws Exception
			 */
			@Test(priority = 133, enabled = true)
			public void verifyReenabledShortcut()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					launch.verifyReenabledShortcut();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyReenabledShortcut");
				}
			}			
	
			/**
			 * verify view task link
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 134, enabled = true)
			public void verifyTaskOption()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					navDrawer.navigateTOTasksApp(driver);
					conversationView.verifyTasks(driver);
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyTaskOption");
		}
			
			}

			
			/**--Send an email from IMAP
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 135)
			public void verifySendMailIMAP() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
//					accounts.verifyAddingOtherAccounts();
					accounts.verifyEmailSendIMap();
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySendMailIMAP");
				}
			}
			
			/**--Receive an email from IMAP
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 136)
			public void verifyReceiveEmailIMAP() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					
					convView.verifyEmailReceiveIMap();
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyReceiveEmailIMAP");
				}
			}
			/**--Receive an email from IMAP
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 137)
			public void verifyEditAndSendDraftIMAP() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					drafts.verifyEditAndSendDraft();						
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyEditAndSendDraftIMAP");
				}
			}	
			
			/**-- All labels sync for IMAP
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 138)
			public void verifyLabelSyncIMAP() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
						sync.verifyIMAPLabelSync();					
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyLabelSyncIMAP");
				}
			}	
			
			/**-- Mail Actions in TL  view for IMAP
			 * @author batchi
			 * @throws Exception
			 */
			@Test(priority = 139)
			public void verifyTLMailActionsIMAP() throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
						mailAction.verifyIMAPMailActionsTL();					
					
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyLabelSyncIMAP");
				}
			}	
			
			/**
			 * Search Functionality IMAP
			 * @author batchi
			 */
			@Test(priority = 140)
			public void verifySearchIMAP()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					search.verifySearchFunctionalityIMAP();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySearchFunctionalityOffline");
				}
			}
			

			@Test(priority = 141, enabled = true)
			public void verifyLeftSwipeActions()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					settings.verifyLeftSwipeActions("Left");
				}catch(Exception e){
					UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyLeftSwipeActions");
				}
			}
			@Test(priority = 142, enabled = true)
			public void verifyRightSwipeActions()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();	
				try{
					settings.verifyLeftSwipeActions("Right");
				}catch(Exception e){
					UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyLeftSwipeActions");
				}
			}
			
			//Exploratory // Needs to change the priority value as per the exact order
			/**
			 * @author Venkat
			 */
			@Test(priority=1000)
			public void switchingAccountsPI()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.switchingAccountsPI();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "switchingAccountsPIExploratory");
				}
			}

			/**
			 * @author Venkat
			 */
			@Test(priority=1001)
			public void switchingAccountsAfterChangePITypesExploratory()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.switchingAccountsAfterChangePITypes();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "switchingAccountsAfterChangePITypesExploratory");
				}
			}

			/**
			 * @author Venkat
			 */
			@Test(priority = 1002, enabled = true)
			public void verifyIMAPLauncherShortcut()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					launch.verifyIMAPLauncherShortcut();
				}catch(Throwable e){
					commonPage.catchBlock(e, driver, "verifyIMAPLauncherShortcut");
				}
			}

			/**
			 * @author Venkat
			 */
			@Test(priority=1111)
			public void verifyPINotificationWhenDisabled()throws Exception{
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					pi.piNotificationWhenDisabled();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "switchingAccountsPIExploratory");
				}
			}
		
			/**
			 * @author Venkat
			 * @throws Exception 
			 */
			@Test(priority=20190516) //Pending or Not completed
			public void verifySignatureOnChangingCustomFromField() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					settings.verifySignatureOnChangingCustomFromField();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySignatureOnChangingCustomFromField");
				}
				
			}

			/**
			 * @author Venkat
			 */
			@Test(priority=20190517)
			public void verifyChangingFromCustomField() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					settings.verifyChangingFromCustomFieldAndMailFromNogGoogle();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifySignatureOnChangingCustomFromField");
				}
				
			}


			/**
			 * @author Venkat
			 */
			@Test(priority=20190521)
			public void verifyChangingFromCustomFieldAndRotation() throws Exception {
				driver = map.get(Thread.currentThread().getId());
				UserAccount.launchGmailApp(driver);
				Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					settings.verifyChangingFromCustomFieldAndRotation();
				}catch(Exception e){
		    		UserAccount.launchGmailApp(driver);
					commonPage.catchBlock(e, driver, "verifyChangingFromCustomFieldAndRotation");
				}
				
			}

			/**
			 * Deleted Messages In CV
			 * @throws Exception
			 * Author: Santosh
			 */
		    @Test(priority = 135)
		    
			public void verifyDeletedMessagesInCV()throws Exception{
			 	driver = map.get(Thread.currentThread().getId());
			   	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
		   		tConv.verifyDeletedMessagesInCV();
		   	}catch(Exception e){
	    		userAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifyDeletedMessagesInCV");
		   	}
		   }
		    /**
			 * Save to Photos
			 * @throws Exception
			 * Author: Santosh
			 */
		    @Test(priority = 135)
		    
		    public void verifySaveToPhotos()throws Exception{
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	// MailManager.DeleteAllMailsInbox();
			    	try {
		    		compose.saveToPhotos();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifySaveToPhotos");
		    	}
		    }
		    /**
			 * Exploratory: Discard Draft with attachments
			 * @throws Exception
			 */
		    @Test(priority = 135)
		    public void verifyDiscardDraftWithAttachments()throws Exception{
		    	userAccount.launchGmailApp(driver);
		        driver = map.get(Thread.currentThread().getId());
		        Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		drafts.discardDraftWithAttachments();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyMailSavedAsDraft");
		    	}
		    }
		    /**
			 *Exploratory: Verify account is in focus
			 * @throws Exception
			 * Author: Santosh
			 */
		    @Test(priority = 1)
		    
		    public void verifyAccountInFocus()throws Exception{
			 	driver = map.get(Thread.currentThread().getId());
			   	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
				try{
					userAccount.verifyAccountInFocus(driver);
		   	}catch(Exception e){
	    		userAccount.launchGmailApp(driver);
		   		commonPage.catchBlock(e, driver, "verifyAccountInFocus");
		   	}
		   }
		    /**
			 *Exploratory: Verify photoviewer functionality
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyPhotoViewerFunctionality()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		convView.verifyPhotoViewerOptions();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyPhotoViewerFunctionality");
		    	}
		    }

		    /**
			 *Exploratory: Verify photoviewer Inline image functionality
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyPhotoViewerInlineFunctionality()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		convView.verifyPhotoViewerInlineImageOptions();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyPhotoViewerInlineFunctionality");
		    	}
		    }
		    
		    /**
			 *Exploratory: Verify Smart Profile options functionality
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifySmartProfile()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		convView.verifySmartProfileOptions();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifySmartProfile");
		    	}
		    }    
		    
		    /**
			 *Exploratory: Verify recent label option functionality
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyRecentLabel()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		navDrawer.verifyRecentLabel(driver);
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyRecentLabel");
		    	}
		    }    
		    /**
			 *Exploratory: Verify undo operations
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyUndoOperations()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		tConv.verifyUndoInTL();
		    		tConv.verifyUndoInCV();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyUndoOperations");
		    	}
		    }    
		    /**
			 *Exploratory: verify able to send mails while offline
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifySendMailsWhileOffline()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
		    		   compose.offlineMailSending();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifySendMailsWhileOffline");
		    	}
		    } 
		    
		    /**
			 *Exploratory: verify change label mutation
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyChangeLabelMutations()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		tConv.verifyLabelMutation();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyChangeLabelMutations");
		    	}
		    }    
		    /**
			 *Exploratory: verifyAttach and Delete inserted Attachments in compose 
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyAttachDeleteAttachments()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		
			    		compose.attachDeleteAttachments();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyAttachDeleteAttachments");
		    	}
		    }    
		   
		    
		    /**
			 *Exploratory: verify Various  Operations From Search
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyVariousOperationsFromSearch()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		search.verifyVariousSearchOperations();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyVariousOperationsFromSearch");
		    	}
		    }    
		       
		    /**
			 *Exploratory: verify remove all accounts
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void verifyRemoveAllAcountsNoCrash()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		accounts.verifyRemoveAllAccountsDoesnotCrash();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyVariousOperationsFromSearch");
		    	}
		    }    
		    
		    /**
			 * Schedule Send
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void scheduleSend()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		compose.verifyScheduleSend();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyVariousOperationsFromSearch");
		    	}
		    }    
		    /**
			 * Cancel Schedule Send
			 * @throws Exception
			 * @author batchi
			 */
		    @Test(priority = 1)
		    public void scheduleSendCancel()throws Exception{
		    	userAccount.launchGmailApp(driver);
		    	driver = map.get(Thread.currentThread().getId());
		    	Capabilities s=driver.getCapabilities();
				version = s.getVersion();
			    	try {
			    		compose.verifyScheduleSendCancel();
			    		
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "verifyVariousOperationsFromSearch");
		    	}
		    }    
		    
			  /**-- Reply/Reply All setting in settings
			   *   @author: Santosh
			   * @throws Exception
			   */
			  @Test(priority = 00)
			  public void verifyReplyAndReplyAllFromSettings()throws Exception{
				  userAccount.launchGmailApp(driver);
				  driver = map.get(Thread.currentThread().getId());
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
			  	try{
			  			settings.verifyReplyAndReplyAllFromSettings();
			  	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
			  		 commonPage.catchBlock(e, driver, "verifyReplyAndReplyAllFromSettings");
			  	}
			  }
			  
			  /**-- Exploratory: Verify able to edit IMAP account settings
			     *@author Santosh
			     * @throws Exception
			     */
			    @Test(priority = 00)
				public void verifyAbleToEditIMAPAccountSettings()throws Exception{
			    	userAccount.launchGmailApp(driver);
					driver = map.get(Thread.currentThread().getId());
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						mailAction.verifyAbleToEditIMAPAccountSettings();
					}catch(Exception e){
			    		userAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyAbleToEditIMAPAccountSettings");
					}
				}
			   /**--Exploratory: Verify PI notification when Enabled
				   *   @author Santosh
				   * @throws Exception
				   */
				@Test(priority=00)
				public void verifyPINotificationWhenEnabled()throws Exception{
					userAccount.launchGmailApp(driver);
					driver = map.get(Thread.currentThread().getId());
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						pi.verifyPINotificationWhenEnabled();
					}catch(Exception e){
			    		userAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyPINotificationWhenEnabled");
					}
				}
			    /**--Exploratory: Manage labels and enable label notifications
			     *@author Santosh
			     * @throws Exception
			     */
			    @Test(priority = 15)
				public void verifyLabelNotificationEnabledAndInboxSyncDisabled()throws Exception{
			    	userAccount.launchGmailApp(driver);
					driver = map.get(Thread.currentThread().getId());
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						mailAction.labelNotificationEnabledAndInboxSyncDisabled();
					}catch(Exception e){
			    		userAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyLabelNotificationEnabledAndInboxSyncDisabled");
					}
				}
			    /**-- Exploratory: Manage labels and enable label notifications
			     *@author Santosh
			     * @throws Exception
			     */
			    @Test(priority = 00)
				public void verifyManageLabelsAndEnableLabelNotifications()throws Exception{
			    	userAccount.launchGmailApp(driver);
					driver = map.get(Thread.currentThread().getId());
					Capabilities s=driver.getCapabilities();
					version = s.getVersion();
					try{
						mailAction.manageLabelsAndEnableLabelNotifications();
					}catch(Exception e){
			    		userAccount.launchGmailApp(driver);
						commonPage.catchBlock(e, driver, "verifyManageLabelsAndEnableLabelNotifications");
					}
				}
			    /**--Exploratory: Verify prompt to enter password
			     *@author Santosh
			     * @throws Exception
			     */
			 @Test(priority = 00)
			    public void verifyPromptToEnterPassword()throws Exception{
				 userAccount.launchGmailApp(driver);
				 driver = map.get(Thread.currentThread().getId());
			    	Capabilities s=driver.getCapabilities();
					version = s.getVersion();
				    	try {
			    		compose.PromptToEnterPassword();
			    	}catch(Exception e){
			    		userAccount.launchGmailApp(driver);
			    		commonPage.catchBlock(e, driver, "verifyPromptToEnterPassword");
			    	}
			    }		
			/**-- Exploratory: Verify able to archive and mute from search(Gmail only)
		     *@author Santosh
		     * @throws Exception
		     */
		    @Test(priority = 0)
		    public void VerifyAbleToArchiveAndMuteFromSearch()throws Exception{
		    	userAccount.launchGmailApp(driver);
		        driver = map.get(Thread.currentThread().getId());
		        Capabilities s=driver.getCapabilities();
				version = s.getVersion();
		    	try{
		    		mailAction.verifyAbleToArchiveAndMuteFromSearch();
		    	}catch(Exception e){
		    		userAccount.launchGmailApp(driver);
		    		commonPage.catchBlock(e, driver, "VerifyAbleToArchiveAndMuteFromSearch");
		    	}
		    }
		    /** -- Exploratory:  Selecting labels from Search by side swiping drawer
		     * @author batchi
		     * @throws Exception
		     */
		   		    @Test(priority = 0)
		   		    public void SelectingLabelFromSearch()throws Exception{
		   		    	userAccount.launchGmailApp(driver);
		   		        driver = map.get(Thread.currentThread().getId());
		   		        Capabilities s=driver.getCapabilities();
		   				version = s.getVersion();
		   		    	try{
		   		   
		   		    		search.verifySelectLableFromSearch();
		   		    	}catch(Exception e){
		   		    		userAccount.launchGmailApp(driver);
		   		    		commonPage.catchBlock(e, driver, "SelectingLabelFromSearch");
		   		    	}
		   		    }
		   		 /** -- Exploratory:  Verifying the Onboarding message of an Account switcher
				     * @author batchi
				     * @throws Exception
				     */
				   		    @Test(priority = 0)
				   		    public void accountSwitchOnboard()throws Exception{
				   		    	userAccount.launchGmailApp(driver);
				   		        driver = map.get(Thread.currentThread().getId());
				   		        Capabilities s=driver.getCapabilities();
				   				version = s.getVersion();
				   		    	try{
				   		    		navSwitcherDrawer.verifyAccountSwitchOnboardingMsg();	
				   		    	}catch(Exception e){
				   		    		userAccount.launchGmailApp(driver);
				   		    		commonPage.catchBlock(e, driver, "accountSwitchOnboard");
				   		    	}
				   		    }
	/** -- Exploratory:  verifying the save to drive option from Drafts
	 * @author batchi
	 * @throws Exception
	*/
	@Test(priority = 0)
	public void saveToDriveFromDrafts()throws Exception{
	driver = map.get(Thread.currentThread().getId());
	userAccount.launchGmailApp(driver);
	Capabilities s=driver.getCapabilities();
	version = s.getVersion();
	try{
		conversationView.saveToDriveFromDrafts(driver);
	}catch(Exception e){
	userAccount.launchGmailApp(driver);
	commonPage.catchBlock(e, driver, "saveToDriveFromDrafts");
	 }
	}
   /** -- Exploratory:  Search by clearing and selecting any other recent searches
    * @author batchi
   * @throws Exception
  */
  @Test(priority = 0)
  public void clearSearchSelectFromRecentSearch()throws Exception{
   driver = map.get(Thread.currentThread().getId());
   Capabilities s=driver.getCapabilities();
   version = s.getVersion();
       	try{
   		   		search.clearSearchAndSelectFromRecentSearch();    			
      	}catch(Exception e){
    		userAccount.launchGmailApp(driver);
      		commonPage.catchBlock(e, driver, "clearSearchSelectFromRecentSearch");
   		   		    	}
   		   		    }
  /** -- Exploratory:  Undo delete/archive when auto-advance is set to older.
	 * @author batchi
	 * @throws Exception
	*/
	@Test(priority = 0)
	public void undoDeleteAutoAdvanceOlderRestoreEmail()throws Exception{
	driver = map.get(Thread.currentThread().getId());
	Capabilities s=driver.getCapabilities();
	version = s.getVersion();
	try{
		mailAction.undoDeleteArchiveAutoAdvanceOlder();
	}catch(Exception e){
	userAccount.launchGmailApp(driver);
	commonPage.catchBlock(e, driver, "undoDeleteAutoAdvanceOlderRestoreEmail");
	 }
	}
/** -- Exploratory:  Verify  Search from AGSA
	 * @author batchi
	 * @throws Exception
	*/
	@Test(priority = 0)
	public void verifySearchFromAGSA()throws Exception{
	driver = map.get(Thread.currentThread().getId());
	Capabilities s=driver.getCapabilities();
	version = s.getVersion();
	try{
		search.verifyEmailFromPersonal();
	}catch(Exception e){
	userAccount.launchGmailApp(driver);
	commonPage.catchBlock(e, driver, "undoDeleteAutoAdvanceOlderRestoreEmail");
	 }
	}

	// Manual suite
	

	/**
	 * @author Venkat
	 */
	@Test(priority = 20190717, enabled = true)
	public void verifyScheduleSendMessage()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			snooze.verifyScheduledSendMessage();
		}catch(Exception e){
			UserAccount.launchGmailApp(driver);
			commonPage.catchBlock(e, driver, "verifyLeftSwipeActions");
		}
	}

	// Travel And Purchases
	
	/**
	 * @author Venkat
	 * 1.37.1 Verify mails related to Purchases
	 */
	@Test(priority = 143, enabled = true)
	public void verifyMailRelatedToPurchases()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			travelAndExpenses.verifyMailInRelatedLabel(PURCHASES,PARCEL_DELIVERY);
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyMailRelatedToPurchases");
		}
	}
	
	/**
	 * @author Venkat
	 * 1.37.2 Verify mails related to Travel
	 */
	@Test(priority = 144, enabled = true)
	public void verifyMailRelatedToTravel()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			travelAndExpenses.verifyMailInRelatedLabel(TRAVEL,FLIGHT_RESERVATION);
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyMailRelatedToTravel");
		}
	}

	
	/**
	 * @author Venkat
	 * 1.37.3 Trip leg / legs should have all the details related to the Trip
	 */
	@Test(priority = 145, enabled = true)
	public void verifyTripLegsShouldHaveAllTheRelatedToTheTrip()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			//travelAndExpenses.verifyMailInRelatedLabel("Travel","flight_reservation");
			travelAndExpenses.verifyTripLegsShouldHaveAllTheRelatedToTheTrip();
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyTripLegsShouldHaveAllTheRelatedToTheTrip");
		}
	}
	
	
	/**
	 * @author Venkat
	 * 1.37.4 View / Edit Trips links should take the user to Trips Webview
	 */
	@Test(priority = 146, enabled = true)
	public void verifyViewEditTripsLinksShouldTakeToTripsWebview()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			travelAndExpenses.verifyViewEditTripsLinksShouldTakeToTripsWebview(TRAVEL,FLIGHT_RESERVATION);
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyViewEditTripsLinksShouldTakeToTripsWebview");
		}
	}

	/**
	 * @author Venkat
	 * 1.37.6 Change Labels for Travel and Purchase mails
	 */
	@Test(priority = 147, enabled = true)
	public void verifyChangeLabelForTravelAndPurchase()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			travelAndExpenses.verifyChangeLabelForTravelAndPurchase(TRAVEL,FLIGHT_RESERVATION);
			travelAndExpenses.verifyChangeLabelForTravelAndPurchase(PURCHASES,PARCEL_DELIVERY);
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyChangeLabelForTravelAndPurchase");
		}
	}

	/**
	 * @author Venkat
	 * 1.37.7 Move to / Change Labels list should not show Travel / Purchase label
	 */
	@Test(priority = 148, enabled = true)
	public void verifyChangeLabelShouldNotShowTravelAndPurchase()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		Capabilities s=driver.getCapabilities();
		version = s.getVersion();	
		try{
			travelAndExpenses.verifyChangeLabelShouldNotShowTravelAndPurchase(MOVE_TO,FLIGHT_RESERVATION);
			travelAndExpenses.verifyChangeLabelShouldNotShowTravelAndPurchase(CHANGE_LABELS,PARCEL_DELIVERY);
		}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyChangeLabelForTravelAndPurchase");
		}
	}

}