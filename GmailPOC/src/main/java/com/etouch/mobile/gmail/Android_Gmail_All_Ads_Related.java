package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_All_Ads_Related extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*@Test
	public void  ExecuteAllAll_Ads_Related_Cases() throws Exception{
		verifyTwoAdsDisplayInPromotionSocial();
		verifyExpandAndControlAds();
		verifyPromotionsTabRedesignForADS();
		verifyAdvertisementFunctionality();
}*/

	/**
	 * @TestCase GAT29  : Verify two ads display in Promotion/Social
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
.    * @author Rahul kulkarni
	 * @throws: Exception
	 * GMAT 127
	 */	
	@Test(groups = {"P0"}, enabled = true, priority = 28)
		public void verifyTwoAdsDisplayInPromotionSocial() throws Exception{
		Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage.getTestData("verifyTwoAdsDisplayInPromotionSocial");

			driver = map.get(Thread.currentThread().getId());
			try{
				log.info("*****************Started testcase ::: verifyTwoAdsDisplayInPromotionSocial*****************************");
     			inbox.verifyGmailAppInitState(driver);
     			if(!CommonFunctions.isAccountAlreadySelected(verifyTwoAdsDisplayInPromotionSocial.get("twoadsaccount")))
     			userAccount.switchMailAccount(driver, verifyTwoAdsDisplayInPromotionSocial.get("twoadsaccount"));
			    inbox.verifyTwoAdsDisplayInPromotionSocial(driver);
			    log.info("***************Completed testcase ::: verifyTwoAdsDisplayInPromotionSocial*****************************");    
			    inbox.scrollUp(1);
			    driver.navigate().back();
			}catch(Throwable e){
				commonPage.catchBlock(e, driver, "verifyTwoAdsDisplayInPromotionSocial");
			}
	}
	
	
	/**
	 * @TestCase 16 : Verify Able to expand Ad info and Control Ads
	 * @Pre-condition : Ad should be available under Promotion and Social tabs
	 *                for Account one.
	 * @throws: Exception
	 * GMAT 132
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 29)
	public void verifyExpandAndControlAds() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyExpandAndControlAds***************************");

			inbox.verifyGmailAppInitState(driver);
			/*Map<String, String> verifyDefaultAndPriorityInboxListViewsData = commonPage
					.getTestData("verifyDefaultAndPriorityInboxListViews");
			Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage
					.getTestData("verifyTwoAdsDisplayInPromotionSocial");
*/
/*			userAccount.switchMailAccount(driver,
					verifyTwoAdsDisplayInPromotionSocial
							.get("twoadsaccount"));
*//*			inbox.openMenuDrawer(driver);

			boolean isPromotionsInboxEnabled = inbox.isInboxDisplayed(driver,
					"PromotionsInboxOption");
			boolean isSocialInboxEnabled = inbox.isInboxDisplayed(driver,
					"SocialInboxOption");

			if (!isPromotionsInboxEnabled && !isSocialInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver,
						verifyDefaultAndPriorityInboxListViewsData
								.get("AccountOne"));
				navDrawer.verifyDefaultInbox(driver,
						verifyDefaultAndPriorityInboxListViewsData
								.get("AccountOne"));
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
				commonFunction.navigateBack(driver);
			} else {
				commonFunction.navigateBack(driver);
			}
*/			log.info("Verifying control ads");
			promotions.verifyControlAds(driver, "PromotionsInboxOption");
			//commented since Social ad not displayed.
			//promotions.verifyControlAds(driver, "SocialInboxOption");
			log.info("*****************Completed testcase ::: verifyExpandAndControlAds***************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyExpandAndControlAds");
		}
	}


	/**
	 * @TestCase 15 : Verify user is able to Delete, Forward and Star Ad
	 * @Pre-condition : Ad should be available under Promotion and Social tabs
	 *                for Account one.
	 * @throws: Exception
	 * GMAT 133
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 31)
	public void verifyAdvertisementFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAdvertisementFunctionality**************");
			SoftAssert softAssert = new SoftAssert();

			inbox.verifyGmailAppInitState(driver);

			Map<String, String> verifyAdvertismentData = commonPage
					.getTestData("verifyAdvertisementFunctionality");
			//userAccount.switchMailAccount(driver,	verifyAdvertismentData.get("twoadsaccount"));
			/*inbox.openMenuDrawer(driver);
			boolean isPromotionsInboxEnabled = inbox.isInboxDisplayed(driver,
					"PromotionsInboxOption");
			boolean isSocialInboxEnabled = inbox.isInboxDisplayed(driver,
					"SocialInboxOption");
			if (!isPromotionsInboxEnabled && !isSocialInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver,
						verifyAdvertismentData.get("AccountOne"));
				navDrawer.verifyDefaultInbox(driver,
						verifyAdvertismentData.get("AccountOne"));
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
				commonFunction.navigateBack(driver);
			} else {
				commonFunction.navigateBack(driver);
			}
*/			log.info("Verify Ad functionality for Promotions Inbox option");
			promotions.verifyAdForward(driver,
					verifyAdvertismentData.get("AccountOne"),
					"PromotionsInboxOption",
					verifyAdvertismentData.get("ToFieldData"),
					verifyAdvertismentData.get("ComposeBodyData"));
			promotions.verifyAdDelete(driver, "PromotionsInboxOption");
			inbox.pullToReferesh(driver);
			softAssert
					.assertTrue(promotions.verifyAd_StarAd(driver,
							"PromotionsInboxOption",
							"verifyAdvertisementFunctionalityPromotions"),
							"Image comparision failed for verifyAdvertisementFunctionalityPromotions");
			/*//Commented since social ad not displayed
			log.info("Verify Ad functionality for Social Inbox option");
			promotions.verifyAdForward(driver,
					verifyAdvertismentData.get("AccountOne"),
					"SocialInboxOption",
					verifyAdvertismentData.get("ToFieldData"),
					verifyAdvertismentData.get("ComposeBodyData"));
			promotions.verifyAdDelete(driver, "SocialInboxOption");
			softAssert
					.assertTrue(promotions.verifyAd_StarAd(driver,
							"SocialInboxOption",
							"verifyAdvertisementFunctionalitySocial"),
							"Image comparision failed for verifyAdvertisementFunctionalitySocial");
			*/
			softAssert.assertAll();
			navDrawer.navigateUntilHamburgerIsPresent(driver);
			log.info("*****************Completed testcase ::: verifyAdvertisementFunctionality*******************");
		} catch (Throwable e) {
			commonPage
					.catchBlock(e, driver, "verifyAdvertisementFunctionality");
		}
	}

	/**
	 * @TestCase 13 : Verify Promo Tab Redesign ads TL View
	 * @Pre-condition : Ad should be available under Promotion mails for account
	 *                two
	 * @throws: Exception
	 * GMAT 134
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 30)
	public void verifyPromotionsTabRedesignForADS() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyPromotionsTabRedignforADS**************");
			SoftAssert softAssert = new SoftAssert();

		//	inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyPromotionsTabRedignforADSData = commonPage
					.getTestData("verifyPromotionsTabRedesignForADS");
			/*Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage
					.getTestData("verifyTwoAdsDisplayInPromotionSocial");*/
			String user = verifyPromotionsTabRedignforADSData.get("AccountOne");
		/*	userAccount.switchMailAccount(driver,
					verifyTwoAdsDisplayInPromotionSocial
							.get("twoadsaccount"));
		*/	
			Thread.sleep(1000);
			if(CommonFunctions.getSizeOfElements(driver, verifyPromotionsTabRedignforADSData.get("ForwardAd"))==0){
				
			inbox.openMenuDrawer(driver);
			boolean isPromotionsInboxEnabled = navDrawer
					.isPromotionsInboxDisplayed(driver);
			if (!isPromotionsInboxEnabled) {
				commonFunction.navigateBack(driver);
				userAccount.openUserAccountSetting(driver, user);
				navDrawer.verifyDefaultInbox(driver, user);
				navDrawer
						.enablePromotionAndSocialCategoriesfromInboxCategories(
								driver, isPromotionsInboxEnabled);
			}
			promotions.verifyPromotionsAdDisplayed(driver);
			
			}
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyPromotionsTabRedignforADS"),
							"Image comparison for verifyPromotionsTabRedignforADS");
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifyPromotionsTabRedignforADS*****************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyPromotionsTabRedignforADS");
		}
	}
	

}
