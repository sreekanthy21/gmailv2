package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_Search extends BaseTest {
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	
	/*public void ExecuteAllSearch()throws Exception{
		verifySearchFunctionality();
		verifySearchFunctionalityOffline();
	}*/
	
	/**
	* @TestCase  : Verify Search Functionality
	* @Pre-condition : 1. Gmail app installed 2. Gmail account is added to app
	* @throws: Exception
	* GMAT 64
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 42)
	public void verifySearchFunctionality() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			inbox.verifyGmailAppInitState(driver);

			log.info("*****************Started testcase ::: verifySearchFunctionality*****************************");
			SoftAssert softAssert = new SoftAssert();
			Map<String, String> verifySearchFunctionalityData = CommonPage.getTestData("verifySearchFunctionality");

			String accountOne = CommonFunctions.getNewUserAccount(verifySearchFunctionalityData, "AccountOne", null);

			if(!CommonFunctions.isAccountAlreadySelected(accountOne))
				userAccount.switchMailAccount(driver, accountOne);
			
			CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
			//softAssert.assertTrue(commonFunction.isElementByXpathDisplayed(driver, verifySearchFunctionalityData.get("SearchTextXpath")));
			//commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"));
			
			
			Thread.sleep(1000);
			//added by Venkat on 7/2/2019 for Go Device : starts
			try {
				if(testBedName.contains("Go")){
					CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.gm.lite:id/search']");
				}
			} catch (Exception e) {
			}
			//added by Venkat on 7/2/2019 for Go Device : end
			commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), verifySearchFunctionalityData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(800);
			CommonFunctions.isElementByXpathDisplayed(driver, verifySearchFunctionalityData.get("DidYouMean"));
			CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("DidYouMean"));
			Thread.sleep(2000);
			CommonFunctions.isElementByXpathDisplayed(driver, verifySearchFunctionalityData.get("ResultsText"));
			driver.navigate().back();
		
			/*inbox.openMail(driver, verifySearchFunctionalityData.get("MailSubject"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityData.get("MailSubject"));
		//	commonFunction.navigateBackToMenuDrawer(driver);
			
			commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
			softAssert.assertEquals(commonFunction.searchAndGetTextOnElementByXpath(driver, verifySearchFunctionalityData.get("suggestionTextPath")), verifySearchFunctionalityData.get("suggestionSearchText"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			softAssert.assertAll();
			*/
			log.info("*****************Completed testcase ::: verifySearchFunctionality*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionality");
		}
	}
	
	/**
	* @TestCase  : Offline search is able to display cached search results for Gmail and IMAP accounts and able to perform actions
	* @Pre-condition : While online perform search operations for some search term for few minutes (GMail as well as IMAP Accouts)
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void verifySearchFunctionalityOffline() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			Map<String, String> verifySearchFunctionalityOfflineData = CommonPage.getTestData("verifySearchFunctionalityOffline");

			log.info("*****************Started testcase ::: verifySearchFunctionalityOffline*****************************");
			SoftAssert softAssert = new SoftAssert();
			inbox.verifyGmailAppInitState(driver);
			/*	
				String path = System.getProperty("user.dir");
				 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SearchFunctionality.bat";
				 log.info("Executing the bat file to generate and delete it "+command);
				 Process p = new ProcessBuilder(command).start();
				 log.info("Executed Bat file");
			*/
			
			
			//Online Search for Gmail account
		//	userAccount.verifyAccountsExists(driver, verifySearchFunctionalityOfflineData.get("AccountOne"), verifySearchFunctionalityOfflineData.get("Password"));
			//userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));

			String  gigMailId = CommonFunctions.getNewUserAccount(verifySearchFunctionalityOfflineData, "AccountOne", null);
			CommonFunctions.smtpMailGeneration(gigMailId,"verifySearchFunctionalityRead", "verifyMailSnippets.html", null);
			if(!CommonFunctions.isAccountAlreadySelected(gigMailId))
				userAccount.switchMailAccount(driver,gigMailId);

			CommonFunctions.smtpMailGeneration(gigMailId,"InboxThree", "verifyMailSnippets.html", null);

			 Thread.sleep(5000);
			 CommonPage.pullToReferesh(driver);

			//inbox.sendMailWithSubject(driver);
			for(int i=1; i<=4; i++){
			commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityOfflineData.get("SearchIconIDXpath"));
			commonFunction.searchAndSendKeysByXpathNoClear(driver, verifySearchFunctionalityOfflineData.get("SearchMailXpath"), verifySearchFunctionalityOfflineData.get("MailSubject1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			//inbox.openMailWithSearchTerm(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			//inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			userAccount.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			driver.navigate().back();
		//	commonFunction.navigateBackToMenuDrawer(driver);
			log.info("Searched for the "+i+ " time");

			}
			//Online search for IMAP account
			userAccount.verifyIMAPAccountAdded(driver, verifySearchFunctionalityOfflineData.get("IMAPAccountID"), verifySearchFunctionalityOfflineData.get("IMAPAccountPwd"));
			String IMapMailId = verifySearchFunctionalityOfflineData.get("IMAPAccountID");
			CommonFunctions.smtpMailGeneration(IMapMailId,"InboxThree", "verifyMailSnippets.html", null);
			if(!CommonFunctions.isAccountAlreadySelected(IMapMailId))
			userAccount.switchMailAccount(driver, IMapMailId);
			for(int i=1; i<=4; i++){
				commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityOfflineData.get("SearchIconIDXpath"));
				commonFunction.searchAndSendKeysByXpathNoClear(driver, verifySearchFunctionalityOfflineData.get("SearchMailXpath"), verifySearchFunctionalityOfflineData.get("MailSubject2"));
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				//inbox.openMailWithSearchTerm(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
				//inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
				userAccount.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
				driver.navigate().back();
				
				
			/*commonFunction.searchAndClickById(driver, verifySearchFunctionalityOfflineData.get("SearchIconID"));
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityOfflineData.get("SearchMailID"), verifySearchFunctionalityOfflineData.get("SearchKey2"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			commonFunction.navigateBackToMenuDrawer(driver);*/
			log.info("Searched for the "+i+ " time");
			}
			//userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));
			if(!CommonFunctions.isAccountAlreadySelected(gigMailId))
			userAccount.switchMailAccount(driver, gigMailId);
			
			CommonFunctions.smtpMailGeneration(gigMailId,"Search Archive Mail", "verifyMailSnippets.html", null);
			Thread.sleep(5000);
			//inbox.sendMailArchive(driver);
			//inbox.sendDeleteMail(driver);
			CommonFunctions.smtpMailGeneration(gigMailId,"Search Delete Mail", "verifyMailSnippets.html", null);
			Thread.sleep(5000);
			
			commonFunction.turnOffWifi(driver);
			//Offline search for G-mail account
			inbox.launchGmailApplication(driver);
			
			if(!CommonFunctions.isAccountAlreadySelected(gigMailId))
				userAccount.switchMailAccount(driver, gigMailId);
			//userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));
			
			/*commonFunction.searchAndClickById(driver, verifySearchFunctionalityOfflineData.get("SearchIconID"));
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityOfflineData.get("SearchMailID"), verifySearchFunctionalityOfflineData.get("SearchKey1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			commonFunction.navigateBackToMenuDrawer(driver);*/
			commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityOfflineData.get("SearchIconIDXpath"));
			commonFunction.searchAndSendKeysByXpathNoClear(driver, verifySearchFunctionalityOfflineData.get("SearchMailXpath"), verifySearchFunctionalityOfflineData.get("MailSubject1"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			//inbox.openMailWithSearchTerm(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			//inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			userAccount.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			driver.navigate().back();
			
			//Offline search for IMAP account
			if(!CommonFunctions.isAccountAlreadySelected(IMapMailId))
				userAccount.switchMailAccount(driver,IMapMailId);
			//userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("IMAPAccountID"));
			/*commonFunction.searchAndClickById(driver, verifySearchFunctionalityOfflineData.get("SearchIconID"));
			commonFunction.searchAndSendKeysByID(driver, verifySearchFunctionalityOfflineData.get("SearchMailID"), verifySearchFunctionalityOfflineData.get("SearchKey2"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			commonFunction.navigateBackToMenuDrawer(driver);*/
			commonFunction.searchAndClickByXpath(driver, verifySearchFunctionalityOfflineData.get("SearchIconIDXpath"));
			commonFunction.searchAndSendKeysByXpathNoClear(driver, verifySearchFunctionalityOfflineData.get("SearchMailXpath"), verifySearchFunctionalityOfflineData.get("MailSubject2"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			//inbox.openMailWithSearchTerm(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			//inbox.verifySubjectOfMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			userAccount.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject2"));
			driver.navigate().back();
			
			if(!CommonFunctions.isAccountAlreadySelected(gigMailId))
			userAccount.switchMailAccount(driver, gigMailId);
			//userAccount.switchMailAccount(driver, verifySearchFunctionalityOfflineData.get("AccountOne"));
			//Perform archive operation
			userAccount.setGmailDefaultActionToArchive(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,verifySearchFunctionalityOfflineData.get("ArchiveMailSubject"));
			
			//Perform delete operation
			userAccount.setGmailDefaultActionToDelete(driver);
			inbox.swipeToDeleteOrArchiveMail(driver,verifySearchFunctionalityOfflineData.get("DeleteMailSubject"));
			
			//Move mail to test label
			inbox.openMail(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			navDrawer.moveToLabel(driver, verifySearchFunctionalityOfflineData.get("TestLabelPath"));

			commonFunction.turnOnWifi(driver);
			inbox.launchGmailApplication(driver);

			navDrawer.navigateToAllEmails(driver);
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("ArchiveMailSubject"));
			
			navDrawer.navigateToBin(driver, verifySearchFunctionalityOfflineData.get("BinOption"));
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("DeleteMailSubject"));
			
			navDrawer.navigateToLabel(driver, verifySearchFunctionalityOfflineData.get("TestLabelPath"));
			inbox.verifyMailIsPresent(driver, verifySearchFunctionalityOfflineData.get("MailSubject1"));
			
			softAssert.assertAll();
			log.info("*****************Completed testcase ::: verifySearchFunctionalityOffline*************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionalityOffline");
		}

	}

	/**
	* @TestCase  : Search functionality and perform actions
	* @Pre-condition : While online perform search operations for some search term for few minutes (GMail as well as IMAP Accouts)
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void verifySearchFunctionalityIMAP() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");

			log.info("*****************Started testcase ::: verifySearchFunctionality*****************************");
			
			//userAccount.switchMailAccount(driver, verifySendMailIMAPData.get("OutlookEmailAddress"));
			
			String  emailId = CommonFunctions.getNewUserAccount(verifySendMailIMAPData, "OutlookEmailAddress", "IMAP");

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			userAccount.verifySearchAndActionsIMAPInTL(driver);
			commonPage.navigateUntilHamburgerIsPresent(driver);
			userAccount.verifySearchAndActionsIMAPInCV(driver);
			
			
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifySearchFunctionalityOffline");
		}

	}
	/**
	* @TestCase  : Various operations from search
	* @Pre-condition : 
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void verifyVariousSearchOperations() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");

			log.info("*****************Started testcase ::: verifyVariousSearchOperations*****************************");
			String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert"+java.io.File.separator+"SearchAndActionsInTL.bat";
			 log.info("Executing the bat file to generate a mail for testing Swipe Down to refresh functionality "+command);
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed the bat file to generate a mail");
			 Thread.sleep(4000);
			userAccount.switchMailAccount(driver,verifyVariousSearchOperationsData.get("AccountOne"));
			userAccount.verifySearchAndActionsInGmailTL(driver);
//			commonPage.navigateUntilHamburgerIsPresent(driver);
			userAccount.verifySearchAndActionsInGmailCV(driver);
			
			
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyVariousSearchOperations");
		}

	}
	/**
	* @TestCase  : Various operations from search
	* @Pre-condition : 
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void verifySelectLableFromSearch() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");

			log.info("*****************Started testcase ::: verifyVariousSearchOperations*****************************");
			String  gigMailId = CommonFunctions.getNewUserAccount(verifyVariousSearchOperationsData, "AccountOne", null);
			userAccount.switchMailAccount(driver, gigMailId);
 // scenario 1: From TL view
			conversationView.selectLabelSearchTL(driver);
 // scenario 2: From CV view
			conversationView.selectLabelSearchCV(driver);
			
			log.info("*****************Ended testcase ::: verifyVariousSearchOperations*****************************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyVariousSearchOperations");
		}

	}
	/**
	* @TestCase  : Search by clearing and selecting any other recent searches
	* @Pre-condition : 
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void clearSearchAndSelectFromRecentSearch() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		
		try {
			Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
			String  gigMailId = CommonFunctions.getNewUserAccount(verifyVariousSearchOperationsData, "AccountOne", null);
			userAccount.switchMailAccount(driver, gigMailId);
			userAccount.launchGmailApp(driver);
			conversationView.clearSearchAndSelectRecent(driver);

		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "clearSearchAndSelectFromRecentSearch");
		}

	}
	/**
	* @TestCase  : search from Google & click on email from Personal
	* @Pre-condition : 
	* @throws: Exception
	*/
	@Test(groups = {"P1"}, enabled = true, priority = 97)
	public void verifyEmailFromPersonal() throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try {
			Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
			
			String[] homeButton = new String[]{"adb", "shell", "input", "keyevent 3"};
			ProcessBuilder pb;
			Process p1;
			pb = new ProcessBuilder(homeButton);
			p1 = pb.start(); 
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, verifyVariousSearchOperationsData.get("SearchAGSA"));
			CommonFunctions.searchAndSendKeysByXpath(driver, verifyVariousSearchOperationsData.get("SearchBoxTextFieldAGSA"), 
					verifyVariousSearchOperationsData.get("SearchAGSAText"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			inbox.waitForElementToBePresentByXpath(verifyVariousSearchOperationsData.get("AllFromAGSA"));
			driver.swipe(880, 400, 100, 400, 1000);
			if(CommonFunctions.getSizeOfElements(driver, verifyVariousSearchOperationsData.get("PersonalFromAGSA"))!=0){
				log.info("*********** Personal is loctaed and clicking on it ***********");
				CommonFunctions.searchAndClickByXpath(driver,verifyVariousSearchOperationsData.get("PersonalFromAGSA"));
			}
			else{
				log.info("!!!!!!!!!! Unable to locate Personal, so trying to click again !!!!!!!!!!");
				CommonFunctions.searchAndClickByXpath(driver,verifyVariousSearchOperationsData.get("ShoppingFromAGSA"));
				CommonFunctions.searchAndClickByXpath(driver,verifyVariousSearchOperationsData.get("PersonalFromAGSA"));
			}
			String mailSubjectAGSA = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifyVariousSearchOperationsData.get("MessageSubjectAGSA"));
			CommonFunctions.searchAndClickByXpath(driver, verifyVariousSearchOperationsData.get("MessageDetailsAGSA"));
			inbox.waitForElementToBePresentByXpath(verifyVariousSearchOperationsData.get("MailSubjectCV").replace("MailSubject", mailSubjectAGSA));
//			CommonFunctions.isElementByXpathDisplayed(driver, verifyVariousSearchOperationsData.get("VerifyText"));
			driver.navigate().back();
			if(CommonFunctions.getSizeOfElements(driver, verifyVariousSearchOperationsData.get("SearchTextXpath"))!=0){
				log.info("********** No ID is displayed, empty search box is displaye ********");
			}else{
				throw new Exception("!!!!!!! ID is displayed, search box is not empty !!!!!!!!");
			}
			
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyEmailFromPersonal");
		}

	}

}
