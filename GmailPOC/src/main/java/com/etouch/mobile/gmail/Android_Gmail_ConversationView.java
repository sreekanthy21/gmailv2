package com.etouch.mobile.gmail;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.MailManager;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.core.datamanager.excel.annotations.IExcelDataFiles;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

@Test(groups = "GmailApplication")
@IExcelDataFiles(excelDataFiles = { "GmailAppData=testData" })
public class Android_Gmail_ConversationView extends BaseTest{
	Date timeStamp = new Date();
	static Log log = LogUtil.getLog(GmailTest.class);
	AppiumDriver driver = null;
	HtmlEmailSender mail = new HtmlEmailSender();
	
	@Test
	public void ExecuteAllConversationView() throws Exception{
		verifyMailConversationView();
		verifySmartReplySuggestions();
		verifyReplyAllFunctionality();
		verifyForwardingMailFunctionality();
		verifyCalendarPromotionInConversation();
		verifySmartReplySuggestionsAfterReplying();
		verifySuperCollapseForMultiThreadedMail();
		verifyAttachments();
		verifyZoomingOutTablesForConversationView();
		verifySmartReplySuggestionsAfterDiscarding();
	}
	
	/**
	 * @TestCase 10 : Verify Verify Conversation view for mail / all thread
	 * @Pre-condition : Mail with subject "verifyForwardingMailFunctionality"
	 *                should be available under Account Two
	 * @throws: Exception
	 * GMAT 21
	 */
	@Test(groups= {"P0"}, enabled = true, priority = 36)
	public void verifyMailConversationView() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try{
    		Map < String, String > verifyMailConversationView = new HashMap < String, String > ();
            verifyMailConversationView = CommonPage.getTestData("verifyMailConversationView");
            
			String emailId = verifyMailConversationView.get("AccountOne");
			//emailId = CommonFunctions.getNewUserAccount(verifyMailConversationView, emailId);

            
    	log.info("*****************Started testcase ::: verifyMailConversationView***************");
    	//userAccount.switchMailAccount(driver,verifyMailConversationView.get("AccountOne"));
    	if(!CommonFunctions.isAccountAlreadySelected(emailId))
    	userAccount.switchMailAccount(driver,emailId);
    	inbox.openMailUsingSearch(driver,
    	    verifyMailConversationView.get("MailSubject"));
 	    Thread.sleep(4000);
	    
    	//commonFunction.scrollDown(driver, 1); //Commented by Venkat
    	//inbox.tapOnShowQuotedText(driver); //Commented by Venkat
 	    
 	    try {
 	    	inbox.tapOnShowQuotedText(driver);
		} catch (Exception e) {
			commonFunction.scrollDown(driver, 1);
			inbox.tapOnShowQuotedText(driver);		
		}
 	    
    	inbox.tapOnHideQuotedText(driver);
    	commonFunction.scrollUp(driver, 1);
    	inbox.verifySubjectOfMail(driver,
        	    verifyMailConversationView.get("MailSubject"));
        	
    	conversationView.verifyRecipientDetails(driver,
    	    verifyMailConversationView.get("alertTitle"));
    	conversationView.actionBarConversationView(driver);
    	commonFunction.navigateBack(driver);
    	/*attachment.openAndValidateAttachment(driver,
    	    verifyMailConversationView.get("AttachmentName"));
    	*/// Device get hang while opening chrome browser
    	
    	//Removed here as we are verify in chrome custom tabs feature
    	// inbox.openLinkInChrome(driver,
    	  //  verifyMailConversationView.get("LinkText"), verifyMailConversationView.get("InAppBrowserMoreOptionsOpenInChrome"));
    	// commonFunction.navigateBack(driver);
    	Thread.sleep(1500);
    	conversationView.tapOnRecipientSummary(driver);
    	conversationView.tapOnEmailSnippet(driver);
    	
    	//conversationView.actionBarConversationView(driver);
    	commonFunction.navigateBack(driver);
    	commonFunction.navigateBack(driver);
    	/*commonFunction.navigateBack(driver);
    	commonFunction.navigateBack(driver);
    	*/log.info("*****************Completed testcase ::: verifyMailConversationView********************");
    	}catch(Exception e){
			commonPage.catchBlock(e, driver, "verifyMailConversationView");
			log.info("Stop Time :" + System.currentTimeMillis());
		}
	}

	
	/**Testcase to verify the smart reply suggestions
	 * 
	 * @preCondition Mail should have smart reply suggestions
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 22
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 23)
	public void verifySmartReplySuggestions()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		inbox.verifyGmailAppInitState(driver);
		try{
			 Map <String, String> smartReplySuggestionsData = CommonPage.getTestData("smartReplySuggestions");
	   	        log.info("***************** Started testcase ::: verifySmartReplySuggestions ******************************");

				String emailId = CommonFunctions.getNewUserAccount(smartReplySuggestionsData, "Account2", null);
	   	        
	   	        //navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("UserAccount"));
				navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("UserAccount"));
	   	        userAccount.verifySmartReplyEnabled(driver, true);
	   	      //  userAccount.deleteMailExistPrimary(driver, smartReplySuggestionsData.get("EmailSubject"));
	   	     if(!CommonFunctions.isAccountAlreadySelected(emailId))
	   	        userAccount.switchMailAccount(driver,emailId);
	   	        //userAccount.switchMailAccountNew(driver,smartReplySuggestionsData.get("Account2") );// Commented by Venkat on 13/01/2019
	   	        inbox.composeAndSendMail(driver,
					  smartReplySuggestionsData.get("UserAccount") ,
					  smartReplySuggestionsData.get("") ,
					  smartReplySuggestionsData.get(""),
					  smartReplySuggestionsData.get("EmailSubject") ,
					  smartReplySuggestionsData.get("EmailSubject") );
			  
			  Thread.sleep(2000);
			  if(!CommonFunctions.isAccountAlreadySelected(smartReplySuggestionsData.get("UserAccount")))
			  userAccount.switchMailAccount(driver, smartReplySuggestionsData.get("UserAccount"));
	   	        inbox.verifyMailSmartReplySuggestions(driver, smartReplySuggestionsData.get("EmailSubject"));
	   	        log.info("*****************Completed testcase ::: verifySmartReplySuggestions*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySmartReplySuggestions");
		}
	}
	
	/**
	 * @TestCase 7 : Verify Reply/Reply All mail
	 * @Pre-condition : Mail with subject "verifyReplyAllFunctionality" should
	 *                be available under Account Two.
	 * @throws: Exception
	 * GMAT 23
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 77)
	public void verifyReplyAllFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		/*
		 String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ReplyAll.bat";
		 log.info("Executing the bat file to generate a mail for reply all functionality "+command);
		 Process p = new ProcessBuilder(command).start();
		 */
		
			
		
		 log.info("Executed the bat file to generate a mail");
		try {
			log.info("*****************Started testcase ::: verifyReplyAllFunctionality**********************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyReplyFunctionalityData = CommonPage.getTestData("verifyReplyAllFunctionality");
			Map<String, String> verifyForwardingMailFunctionalityData = CommonPage.getTestData("verifyForwardingMailFunctionality");
			Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
			String mailSubject = verifyReplyFunctionalityData.get("MailSubject");
			
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyReplyFunctionalityData, "AccountTwo", null);
			CommonFunctions.smtpMailGeneration(emailId,mailSubject, "ReplyAll.html", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			
			//userAccount.switchMailAccount(driver,verifyReplyFunctionalityData.get("AccountTwo"));

			/*inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
*/			userAccount.pullToReferesh(driver);
			Thread.sleep(5000);
			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			inbox.composeMailAsReply(
					driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"),
					verifyReplyFunctionalityData.get("AccountOne"));
			/*inbox.insertInLineText(driver,
					verifyReplyFunctionalityData.get("InLineData"));*/
			inbox.tapOnSend(driver);
//Code added to click on Undo while performing Reply by Bindu on 23/09			
			for(int i=1;i<=2;i++){
				log.info("Clicking on Undo while performing Reply button");
				inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
				CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
				inbox.tapOnSend(driver);
				}
				inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
				CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
				inbox.insertInLineText(driver,
						verifyForwardingMailFunctionalityData.get("InLineData"));
				Thread.sleep(2000);
				inbox.hideKeyboard();
				inbox.tapOnSend(driver);
//Code added to click on Undo while performing Reply by Bindu on 23/09				
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);
			//inbox.openMenuDrawer(driver);
			
			//Thread.sleep(7000);
			//inbox.scrollDown(1);
			log.info("Verifying Mail is sent");
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(2000);
			if(!CommonFunctions.isAccountAlreadySelected(verifyReplyFunctionalityData.get("AccountOne")))
			userAccount.switchMailAccount(driver,
					verifyReplyFunctionalityData.get("AccountOne"));
			/*inbox.searchForMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			*/
			userAccount.pullToReferesh(driver);
			Thread.sleep(1000);
			userAccount.pullToReferesh(driver);
			
			inbox.openMail(driver,
					verifyReplyFunctionalityData.get("MailSubject"));

		/*	inbox.verifyReplyInlineTextIsPresent(driver, verifyReplyFunctionalityData
					.get("ReplyAllComposeBodyData"));
		*/	inbox.composeMailAsReplyAll(driver,
					verifyReplyFunctionalityData.get("ReplyAllComposeBodyData"));
			/*inbox.insertInLineText(driver,
					verifyReplyFunctionalityData.get("InLineData"));
			*/
			inbox.tapOnSend(driver);
//Code added to click on Undo while performing Reply All by Bindu on 23/09			
			for(int i=1;i<=2;i++){
				log.info("Clicking on Undo while performing Reply All");
				inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
				CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
				inbox.tapOnSend(driver);
				}
				inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
				CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
				inbox.insertInLineText(driver,
						verifyForwardingMailFunctionalityData.get("InLineData"));
				Thread.sleep(2000);
				inbox.hideKeyboard();
				inbox.tapOnSend(driver);
//Code added to click on Undo while performing Reply All by Bindu on 23/09		
			
			
			commonFunction.navigateBack(driver);
			/*commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
*/
			inbox.scrollDown(1);
			Thread.sleep(7000);
			inbox.verifyMailInSentBox(driver,
					verifyReplyFunctionalityData.get("MailSubject"));
			Thread.sleep(3000);
			log.info("*****************Completed testcase ::: verifyReplyAllFunctionality**********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyReplyAllFunctionality");
		}
	}
	
	/**
	 * @TestCase 6 : Verify Forward mail functionality
	 * @Pre-condition : Mail with subject "verifyForwardingMailFunctionality"
	 *                should be available under Account Two.
	 * @throws: Exception
	 * GMAT 24
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 78)
	public void verifyForwardingMailFunctionality() throws Exception {
		driver = map.get(Thread.currentThread().getId());
/*		
		String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ForwardMail.bat";
		 log.info("Executing the bat file to generate a mail for Forward functionality "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed the bat file to generate a mail");
*/		
		try {
			log.info("*****************Started testcase ::: verifyForwardingMailFunctionality***************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyForwardingMailFunctionalityData = CommonPage.getTestData("verifyForwardingMailFunctionality");
			Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
			String  emailId = CommonFunctions.getNewUserAccount(verifyForwardingMailFunctionalityData, "AccountTwo", null);
			String mailSubject = verifyForwardingMailFunctionalityData.get("MailSubject");
			CommonFunctions.smtpMailGeneration(emailId,mailSubject, "forward.html", null);

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver, emailId);

			//userAccount.switchMailAccount(driver,verifyForwardingMailFunctionalityData.get("AccountTwo"));

		/*	inbox.searchForMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
*/			userAccount.pullToReferesh(driver);
			Thread.sleep(1000);
			userAccount.pullToReferesh(driver);
			inbox.openMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			inbox.composeMailAsFowarding(driver,
					verifyForwardingMailFunctionalityData.get("AccountThree"),
					"", "", verifyForwardingMailFunctionalityData
							.get("ComposeBodyData"));
			attachment
					.addAttachmentFromDrive(driver,
							verifyForwardingMailFunctionalityData
									.get("AttachmentName"));
			
			/*inbox.insertInLineText(driver,
					verifyForwardingMailFunctionalityData.get("InLineData"));*/
			inbox.tapOnSend(driver);
//Code added to click on Undo while performing Forward by Bindu on 23/09		
			for(int i=1;i<=2;i++){
			log.info("Clicking on Undo while performing forward button");
			inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
			CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
			inbox.tapOnSend(driver);
			}
			inbox.waitForElementToBePresentByXpath(undoData.get("undoIcon"));
			CommonFunctions.searchAndClickByXpath(driver,undoData.get("undoIcon"));
			inbox.insertInLineText(driver,
					verifyForwardingMailFunctionalityData.get("InLineData"));
			Thread.sleep(2000);
			inbox.hideKeyboard();
			inbox.tapOnSend(driver);
			
//Code added to click on Undo while performing Forward by Bindu on 23/09		
						
			commonFunction.navigateBack(driver);
			/*commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
*/
			inbox.verifyMailInSentBox(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			
			if(!CommonFunctions.isAccountAlreadySelected(verifyForwardingMailFunctionalityData.get("AccountThree")))
			userAccount.switchMailAccount(driver,
					verifyForwardingMailFunctionalityData.get("AccountThree"));

			/*inbox.searchForMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));
			*/
			inbox.openMail(driver,
					verifyForwardingMailFunctionalityData.get("MailSubject"));

			// Instead of testing the inline data, we should test the link
			// inbox.verifyInlineTextIsPresent(verifyForwardingMailFunctionalityData.get("InLineData"));
			// inbox.verifyReplyInlineTextIsPresent(verifyForwardingMailFunctionalityData.get("ComposeBodyData"));
			commonFunction.scrollUp(driver, 1);
			attachment
					.verifyAttachmentIsAddedSuccessfully(driver,
							verifyForwardingMailFunctionalityData
									.get("AttachmentName"));
			commonFunction.navigateBack(driver);
			/*commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			*/log.info("*****************Completed testcase ::: verifyForwardingMailFunctionality**********");
			//MailManager.DeleteAllMails();
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyForwardingMailFunctionality");
		}
	}
	
	/**
	 * @TestCase 17 : Verify Verify calendar promotion in the conversation if
	 *           the calendar app is not installed
	 * @Pre-condition : Calendar Invitation mail should be available with
	 *                account two
	 * @throws: Exception
	 * GMAT 25
	 * 
	 * Needs mails from smart mail generator like flight reservation to validate this use case. Instead sending everytime verify with existing mail
	 * Works for Non nexus devices only
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 81)
	public void verifyCalendarPromotionInConversation() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyCalendarPromotionInConversation******************");
				Process p;
			    ProcessBuilder pb; 
				String[] UninsCalendar = new String[]{"adb","uninstall", "com.google.android.calendar"};
			    p = new ProcessBuilder(UninsCalendar).start();
			    log.info("Uninstalling the google calender");
			    Thread.sleep(10000);
			
			
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyCalendarPromotionInConversationData = CommonPage.getTestData("verifyCalendarPromotionInConversation");
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyCalendarPromotionInConversationData, "AccountFour", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			//userAccount.switchMailAccount(driver,verifyCalendarPromotionInConversationData.get("AccountFour"));

			inbox.openMailUsingSearch(driver,verifyCalendarPromotionInConversationData.get("MailSubject"));
			/*inbox.openFirstMail(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));*/
			/*calendar.verifyCalendarDetails(driver,
					verifyCalendarPromotionInConversationData
							.get("CalendarMessage"),
					verifyCalendarPromotionInConversationData
							.get("DownloadCalendarText"));*/
			calendar.verifyCalendarDetailsFlight(driver,
					verifyCalendarPromotionInConversationData
							.get("NoThanks"),
					verifyCalendarPromotionInConversationData
							.get("GetCalendar"));
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyCalendarPromotionInConversation***********");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyCalendarPromotionInConversation");
		}
	}
	
	/**
	 * @TestCase 18 :Verify calendar promotion in the conversation if the calendar app is not used at all.
	 * @Pre-condition : Calendar Invitation mail should be available with
	 *                account two
	 * @throws: Exception
	 * GMAT 25
	 * 
	 * Needs mails from smart mail generator like flight reservation to validate this use case. Instead sending everytime verify with existing mail
	 * Works for Non nexus devices only
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 82)
	public void verifyCalendarPromotionInConversationAppNotUsed() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyCalendarPromotionInConversationAppNotUsed******************");
				
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyCalendarPromotionInConversationData = CommonPage.getTestData("verifyCalendarPromotionInConversation");
			
			String  emailId = CommonFunctions.getNewUserAccount(verifyCalendarPromotionInConversationData, "AccountFour", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			
			//userAccount.switchMailAccount(driver,verifyCalendarPromotionInConversationData.get("AccountFour"));

			inbox.openMailUsingSearch(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));
			/*inbox.openFirstMail(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));*/
			/*calendar.verifyCalendarDetails(driver,
					verifyCalendarPromotionInConversationData
							.get("CalendarMessage"),
					verifyCalendarPromotionInConversationData
							.get("DownloadCalendarText"));*/
			calendar.verifyCalendarDetailsFlight(driver,
					verifyCalendarPromotionInConversationData
							.get("NoThanks"),
					verifyCalendarPromotionInConversationData
							.get("GetCalendar"));
			calendar.verifyGetCalendar(driver,verifyCalendarPromotionInConversationData
					.get("NoThanks"),
			verifyCalendarPromotionInConversationData
					.get("GetCalendar"));
			inbox.openMailUsingSearch(driver,
					verifyCalendarPromotionInConversationData
							.get("MailSubject"));
			calendar.verifyCalendarDetailsFlight(driver,
					verifyCalendarPromotionInConversationData
							.get("NoThanks"),
					verifyCalendarPromotionInConversationData
							.get("OpenCalendar"));
			Thread.sleep(2500);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
		//	commonFunction.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyCalendarPromotionInConversationAppNotUsed***********");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyCalendarPromotionInConversationAppNotUsed");
		}
	}

	/**Testcase to verify the smart reply suggestions not displayed after replying
	 * 
	 * @preCondition Mail should have smart reply suggestions
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 26
	 */
	@Test(groups = {"P1"}, enabled = true, priority = 25)
	public void verifySmartReplySuggestionsAfterReplying()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			Map<String, String> smartReplySuggestionsData = CommonPage.getTestData("smartReplySuggestions");
			Thread.sleep(1500);
			log.info("***************** Started testcase ::: verifySmartReplySuggestionsAfterReplying ******************************");
			//verifySmartReplySuggestions();	
			inbox.openMail(driver,smartReplySuggestionsData.get("EmailSubject") ); 
			inbox.clickOnSmartReply(driver,smartReplySuggestionsData.get("EmailSubject") );
			
			
			log.info("Smart reply mail is replied with the available options");
			log.info("Verifying the smart reply suggestions is displayed after the mail is replied");
			userAccount.verifySmartReplySuggestionsNotDisplayed(driver);
			driver.navigate().back();
			log.info("*****************Completed testcase ::: verifySmartReplySuggestionsAfterReplying*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySmartReplySuggestionsAfterReplying");
		}
	}
	
	/**
	 * @TestCase 9 : Verify Super collpase for multiple thread mails
	 * @Pre-condition : Mail with subject
	 *                "verifyZoomingOutTablesForConversationView" should be
	 *                available under Account Two with upto 4 threads.
	 * @throws: Exception
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 44)
	public void verifySuperCollapseForMultiThreadedMail() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifySuperCollapseForMultiThreadedMail***************************");
			Map < String,
	    	String > verifySuperCollapse = commonPage.getTestData("verifySuperCollapseForMultiThreadedMail");
			if(!CommonFunctions.isAccountAlreadySelected(verifySuperCollapse.get("AccountOne")))
			userAccount.switchMailAccount(driver, verifySuperCollapse.get("AccountOne"));
			userAccount.SuperCollapse(driver, verifySuperCollapse.get("MailSubjectNew"));
			log.info("*****************Completed testcase ::: verifySuperCollapseForMultiThreadedMail*********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifySuperCollapseForMultiThreadedMail");
		}
	}

	/**
	 * @TestCase 11 : Verify Open various types of attachments and drive chips
	 *           for mail
	 * @Pre-condition : Mail with subject "Verifying Gmail Attachments Types"
	 *                should be available under Account One
	 * @throws: Exception
	 * GMAT 29
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 79)
	public void verifyAttachments() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyAttachments***************************");

			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyAttachments = CommonPage.getTestData("verifyAttachments");

			String  emailId = CommonFunctions.getNewUserAccount(verifyAttachments, "AccountTwo", null);
			String mailSubject = verifyAttachments.get("MailSubject");
			
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			//userAccount.switchMailAccount(driver,verifyAttachments.get("AccountTwo"));
			
			inbox.searchForMail(driver, mailSubject);
			inbox.openMail(driver, mailSubject);
			Thread.sleep(1500);
			inbox.scrollDown(4);

			/*attachment.openAndValidateAttachment1(driver,
					verifyAttachments.get("MyDriveGmailExcelFileName"));
			*/// attachment.openAndValidateAttachment1(verifyAttachments.get("MyDriveGmailSlidesFileName"));
			attachment.openAndValidateEMLFileAttachment(driver,
					verifyAttachments.get("MyDriveEMLFileName"));
			attachment.scrollDown(1);
			attachment.openAndValidateAttachment1(driver,
					verifyAttachments.get("MyDrivePDFFileName"));
			// attachment.openAndValidateAttachment1(verifyAttachments.get("MyDriveImageFileName"));
			attachment.navigateBack(driver);
			attachment.navigateBack(driver);
			attachment.navigateBack(driver);
			log.info("*****************Completed testcase ::: verifyAttachments*********************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver, "verifyAttachments");
		}
	}

	
	/**
	 * @TestCase 5 : Verify Zooming Out Tables for Conversation View
	 * @Pre-condition : Mail with subject
	 *                "verifyZoomingOutTablesForConversationView" should be
	 *                available under Account Two.
	 * @throws: Exception
	 * GMAT 30
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 80)
	public void verifyZoomingOutTablesForConversationView() throws Exception {
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifyZoomingOutTablesForConversationView********");
			SoftAssert softAssert = new SoftAssert();
			inbox.verifyGmailAppInitState(driver);
			Map<String, String> verifyZoomingOutTablesForConversationViewData = CommonPage.getTestData("verifyZoomingOutTablesForConversationView1");
			
			String mailSubject = verifyZoomingOutTablesForConversationViewData.get("MailSubject");


			String  emailId = CommonFunctions.getNewUserAccount(verifyZoomingOutTablesForConversationViewData, "AccountTwo", null);
			CommonFunctions.smtpMailGeneration(emailId,mailSubject, "ZoomOut.html", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			
			/*String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ZoomingOutTables.bat";
			 log.info("Executing the bat file to generate a mail for zooming out tables "+command);
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed the bat file to generate a mail");*/
			//userAccount.switchMailAccount(driver,verifyZoomingOutTablesForConversationViewData.get("AccountTwo"));
			inbox.openMailUsingSearch(driver,verifyZoomingOutTablesForConversationViewData.get("MailSubject"));
		/*	inbox.openMail(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsFowarding(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("AccountThree"), "", "",
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSendAndNavigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			userAccount.switchMailAccount(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("AccountThree"));
			inbox.openMailUsingSearch(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsReplyAll(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSend(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			userAccount.switchMailAccount(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("AccountTwo"));
			inbox.openMailUsingSearch(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("MailSubject"));
			inbox.composeMailAsReplyAll(driver,
					verifyZoomingOutTablesForConversationViewData
							.get("ComposeBodyData"));
			inbox.tapOnSend(driver);*/
			Thread.sleep(2000);
			inbox.scrollDown(2);
			inbox.tapOnShowQuotedText(driver);
			Thread.sleep(1200);
			commonFunction.scrollTo(driver, "Forward");
			softAssert
					.assertTrue(
							commonPage
									.getScreenshotAndCompareImage("verifyZoomingOutTablesForConversationView"),
							"Image comparison for verifyZoomingOutTablesForConversationView");
			commonFunction.navigateBack(driver);
			commonFunction.navigateBack(driver);
			//commonFunction.navigateBack(driver);
			softAssert.assertAll();
			log.info("Completion Time :" + System.currentTimeMillis());
			log.info("*****************Completed testcase ::: verifyZoomingOutTablesForConversationView************");
		} catch (Throwable e) {
			commonPage.catchBlock(e, driver,
					"verifyZoomingOutTablesForConversationView");
		}
	}

	
	/**Testcase to verify the smart reply suggestions displayed after discarding mail
	 * 
	 * @preCondition Mail should have smart reply suggestions
	 * @throws Exception
	 * @author Phaneendra
	 * GMAT 33
	 */
	@Test(groups = {"P2"}, enabled = true, priority = 24)
	public void verifySmartReplySuggestionsAfterDiscarding()throws Exception{
		driver = map.get(Thread.currentThread().getId());
		try{
			log.info("***************** Started testcase ::: verifySmartReplySuggestionsAfterDiscarding ******************************");
			Map<String, String> smartReplySuggestionsData = CommonPage.getTestData("smartReplySuggestions");
			//log.info("***************** Started testcase ::: verifySmartReplySuggestions ******************************");

			//userAccount.verifyAccountsExists(driver, smartReplySuggestionsData.get("Account"), smartReplySuggestionsData.get("AccountPassword"));
			//userAccount.switchMailAccount(driver, smartReplySuggestionsData.get("Account"));
			/*navDrawer.openPrimaryInbox(driver, smartReplySuggestionsData.get("Account"));
		    navDrawer.openUserAccountSetting(driver, smartReplySuggestionsData.get("Account"));
		    userAccount.verifySmartReplyEnabled(driver, true);
		    userAccount.deleteMailExistPrimary(driver, smartReplySuggestionsData.get("EmailSubject"));
		    */
			//inbox.verifySuggestions(driver, smartReplySuggestionsData.get("EmailSubject"));
			inbox.openMail(driver,smartReplySuggestionsData.get("EmailSubject") ); 
			inbox.saveMailAsDraftAndDiscard(driver);
		    userAccount.verifySmartReplySuggestionsDisplayed(driver);
			log.info("*****************Completed testcase ::: verifySmartReplySuggestionsAfterDiscarding*************************");
		}catch(Throwable e){
			commonPage.catchBlock(e, driver, "verifySmartReplySuggestionsAfterDiscarding");
		}
	}
	
	/**
	 * @TestCase 9 :Verify Safe Links V1 functionality /**
		 * @Pre-condition : Gmail build 6.0+
	 * @throws: Exception
	 */	 
	@Test(enabled = true, priority = 32)
   public void verifySafeLinksFunctionalityv1() throws Exception {
		
		driver = map.get(Thread.currentThread().getId());
		try {
			log.info("*****************Started testcase ::: verifySafeLinksV1Functionalityn*************************");
			inbox.verifyGmailAppInitState(driver);
			inbox.verifySafeLinksFunctionality(driver);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
		    log.info("*****************Completed testcase ::: verifySafeLinksV1Functionality******************************");	
		} catch (Throwable e) {
					}
	}
 	 
	 /**
		 * @TestCase 9 :Verify Safe Links V2 functionality /**
			 * @Pre-condition : Gmail build 6.0+
		 * @throws: Exception
		 */	 
		@Test(enabled = true, priority = 33)
	   public void verifySafeLinksFunctionalityv2() throws Exception {
			driver = map.get(Thread.currentThread().getId());
/*			
			 String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SafeLinks.bat";
			 log.info("Executing the bat file to generate and delete it "+command);
			 Runtime.getRuntime().exec(command);
			 log.info("Executed Bat file");
*/			 
			
			
			 Thread.sleep(4000);
			try {
				log.info("*****************Started testcase ::: verifySafeLinksV1Functionalityn*************************");
				inbox.verifyGmailAppInitState(driver);
				inbox.verifySafeLinksFunctionalityv2(driver);
			    log.info("*****************Completed testcase ::: verifySafeLinksV1Functionality******************************");	
			} catch (Throwable e) {
						}
		}
		
		/**
		 * @TestCase 9 :Test Expanded Style Support /**
			 * @Pre-condition : Gmail build 6.0+
		 * @throws: Exception
		 */	 
		@Test(enabled = true, priority = 83)
	   public void verifyStyleSupport() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			try {
				
				SoftAssert softAssert = new SoftAssert();

				Map<String, String> StyleSupport = CommonPage.getTestData("verifyComposeAndSendMail");
				log.info("*****************Started testcase ::: verifyStyleSupport*************************");
				inbox.verifyGmailAppInitState(driver);
				
				//String  emailId = CommonFunctions.getNewUserAccount(StyleSupport, "AccountOne");
				String  emailId = StyleSupport.get("AccountOne");
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);
				//userAccount.switchMailAccount(driver, StyleSupport.get("AccountOne"));
				
				userAccount.openMailUsingSearch(driver, StyleSupport.get("StyleSupportMail"));
				Thread.sleep(3000);
				softAssert
				.assertTrue(
						commonPage
								.getScreenshotAndCompareImage("verifyStyleSupport"),
						"Image comparison for StyleSupport");
				commonFunction.navigateBack(driver);
				commonFunction.navigateBack(driver);
				//commonFunction.navigateBack(driver);
				softAssert.assertAll();
			    log.info("*****************Completed testcase ::: verifyStyleSupport******************************");	
			} catch (Throwable e) {
						}
		}
		/**
		* @TestCase sanity IMAP : IMAP Accounts - Verify receive a new mail & notification
		* @Pre-condition :
		* GMAT 
		* @throws: Exception
		* @author batchi
		*/
		@Test(groups = {"P0"}, enabled = true, priority = 6)
		public void verifyEmailReceiveIMap() throws Exception {
			driver = map.get(Thread.currentThread().getId());
			Map < String, String > verifySendMailIMAPData = CommonPage.getTestData("IMAPAccountDetails");
			
			String  emailId = CommonFunctions.getNewUserAccount(verifySendMailIMAPData, "OutlookEmailAddress", "IMAP");

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);
			//userAccount.switchMailAccount(driver, verifySendMailIMAPData.get("OutlookEmailAddress"));

			CommonFunctions.smtpMailGeneration(emailId,"VerifyReceiveNotificationMail", "Test", null);
/*			
		    String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"IMAPNotification.bat";
		    log.info("Executing the bat file for imp and unimp mails "+command);
			// Process p = new ProcessBuilder(command).start();
			Runtime.getRuntime().exec(command);
			log.info("Executed Bat file");
*/			
			Thread.sleep(10000);
//			inbox.verifyEmailFromNotification(driver);
			inbox.verifyEmailReceivedCV(driver, verifySendMailIMAPData);
			
		   } 
		/**
		* @TestCase verifyPhotoViewerOption
		* @Pre-condition :
		* GMAT 
		* @throws: Exception
		* @author batchi
		*/
		@Test(groups = {"P0"}, enabled = true, priority = 6)
		public void verifyPhotoViewerOptions() throws Exception {
			log.info("******************* Start verifyPhotoViewerOptions Functionality*******************");			
			inbox.photoviewerSaveOption(driver);
			inbox.photoviewerSaveAllOption(driver);
			inbox.photoviewerShareOption(driver);
			inbox.photoviewerShareAllOption(driver);
			inbox.photoviewerPrintOption(driver);
			log.info("******************* End verifyPhotoViewerOptions Functionality*******************");
		}
		/**
		* @TestCase verifyPhotoViewerInlineImageOption
		* @Pre-condition :
		* GMAT 
		* @throws: Exception
		* @author batchi
		*/
		@Test(groups = {"P0"}, enabled = true, priority = 6)
		public void verifyPhotoViewerInlineImageOptions() throws Exception {
			log.info("******************* Start verifyPhotoViewerInlineImageOptions Functionality*******************");			
			inbox.photoviewerInlineImageSaveOption(driver);
			inbox.photoviewerInlineSaveAllOption(driver);
			inbox.photoviewerInlineShareOption(driver);
			inbox.photoviewerInlineShareAllOption(driver);
			inbox.photoviewerInlinePrintOption(driver);
			
			log.info("******************* End verifyPhotoViewerOptions Functionality*******************");
			
		}
		/**
		* @TestCase verify smart profile
		* @Pre-condition :
		* GMAT 
		* @throws: Exception
		* @author batchi
		*/
		@Test(groups = {"P0"}, enabled = true, priority = 6)
		public void verifySmartProfileOptions() throws Exception {
			Map < String, String > SmartProfileData = commonPage.getTestData("SmartProfile");
			log.info("******************* Start verifySmartProfileOptions Functionality*******************");
			String path = System.getProperty("user.dir");
			String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert"+java.io.File.separator+"verifySmartProfile.bat";
		    log.info("Executing the bat file for imp and unimp mails "+command);
			 Process p = new ProcessBuilder(command).start();
//			Runtime.getRuntime().exec(command);
			log.info("Executed Bat file");
			Thread.sleep(10000);
			
			userAccount.switchMailAccount(driver, SmartProfileData.get("Account1"));
//			inbox.openMail(driver, SmartProfileData.get("SmartProfileSubject"));
	//Verifying hangout option
			inbox.verifySmartProfileHangoutsOption(driver);
	// Verifying Schedule option
			inbox.verifySmartProfileScheduleOption(driver);
	// Verifying the contact info email id compose screen 		
			inbox.verifySmartProfileMailIDComposeOption(driver);
	//Verifying the Email option
			inbox.verifySmartProfileEMailOption(driver);	
			log.info("******************* End verifySmartProfileOptions Functionality*******************");
		}

}
