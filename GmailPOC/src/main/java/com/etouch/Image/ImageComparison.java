package com.etouch.Image;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.logging.Logger;

import org.testng.asserts.SoftAssert;

import com.etouch.Image.CustomImageLogging;
import com.etouch.Image.ImageCompare;
import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.GetDataInHashMap;
import com.etouch.gmail.common.TafExecutor;
import com.etouch.taf.util.ExcelUtil;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.util.logging.Logger;

import org.testng.Assert;
import com.etouch.taf.util.ExcelUtil;
import com.etouch.Image.CustomImageLogging;
import com.etouch.Image.ImageCompare;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.TafExecutor;

/**
 * @author krathod
 *
 */
public class ImageComparison {

	TafExecutor obj= new TafExecutor();
	private ImageCompare imgComp = null;
	ExcelUtil readExcel = new ExcelUtil();
	public Object[][] testData;
	protected BufferedImage imgc = null;
	Date timeStamp = new Date();
	boolean initialize = true;
	ImageCompare ic = null;
	boolean compareResult = true;
	/**
	 * Tests 2 images for differences 
	 * @param testData
	 * @param testName
	 * @param img1
	 * @param img2
	 */
	public boolean testImageComparison(Object[][] testData, String testName, String img1, String img2){
		String testBedName =BaseTest.testBedMap.get(Thread.currentThread().getId());
		String imageName = "";
		Logger logger = CustomImageLogging.startLogging(GetDataInHashMap.getPath().concat("GmailTestData/Output/"+testBedName+
				"/log/ImageComparison.log"));
		ImageCompare.setLogger(logger);
		ImageCompare ic = new ImageCompare(img1, img2);
		if(imgc==null){
			try{
				imgc = ic.imageToBufferedImage(ic.actualImage);
				if(imgc==null){
					compareResult = false;
					initialize = false;
				}
			}catch(Exception e){
				logger.warning("Initialization has failed - exiting Image comparison");
				compareResult = false;
				initialize = false;
			}
		}

		if(initialize){
			try{
				Graphics2D gc = imgc.createGraphics();
				long timeStart = System.currentTimeMillis();
				for(int i = 0; i< testData.length;i++){
					if(testData[i][10].toString().equals("N")){
						imageName = testData[i][0].toString();
						logger.info("******************************Starting Image Comparison for "
								+testData[i][0].toString()+"******************************");
						imgc = ic.compareImage(Integer.parseInt(testData[i][3].toString()),
								Integer.parseInt(testData[i][4].toString()),
								Integer.parseInt(testData[i][5].toString()),
								Integer.parseInt(testData[i][6].toString()),imgc, 
								Integer.parseInt(testData[i][2].toString()), 
								Integer.parseInt(testData[i][7].toString()), 
								Integer.parseInt(testData[i][8].toString()),
								Integer.parseInt(testData[i][9].toString()),true);
					}
					try{
						if(imgc!=null){
							System.out.println("Path::::::"+obj.getPath().concat("GmailTestData/Output/"
									+testBedName+"/ImageComparison/"+testName+".png"));
							ic.savePNG(imgc, obj.getPath().concat("GmailTestData/Output/"
									+testBedName+"/ImageComparison/"+testName+".png"));
						} else{
							ic.flag = false;
							logger.info("Image comparison failed");
							compareResult = false;
						}
					} catch(Exception e){
						logger.warning(e.getMessage());
						compareResult = false;
					}
					long timeEnd = System.currentTimeMillis();
					logger.info("Time taken for Image comparison : " + testData[i][0].toString() 
							+ " "+(timeEnd - timeStart) + " milliseconds");
					logger.info("******************************Ending Image Comparison for "
							+testData[i][0].toString()+"******************************");
				}
			}catch(NumberFormatException e){
				logger.warning("Check Datasheet and provide correct co-ordinates in numbers");
				compareResult = false;
			}catch(NullPointerException e){
				logger.warning("Image comparison failed " + e.getMessage());
				compareResult = false;
			}
			try{
				Assert.assertTrue(ic.match,"Image comparison failed");
				logger.info("Image Comparison for " + imageName + " Passed");
			}catch(NullPointerException e){
				logger.warning("Image comparison failed - " + e.getMessage());
				compareResult = false;
			}catch(AssertionError e){
				logger.info("Image Comparison for " + imageName + " failed");
				compareResult = false;
			}
		}else{
			try{
				Assert.assertTrue(false,"Image comparison failed due to null Image");
			}catch(AssertionError e){
				compareResult = false;
				logger.info("Image Comparison for " + imageName + " failed");
			}
		}
		return compareResult;
	}

	/**
	 * @param sheetName
	 * @param key
	 * @return
	 */
	public Object[][] getData(String sheetName, String key) {

		String testBedName =BaseTest.testBedMap.get(Thread.currentThread().getId());
		testData = readExcel.readExcelData(obj.getPath().concat("GmailTestData/Input/"
				+testBedName+"/excelData/imgCompare.xls"),
				sheetName, key);
		return testData;
	}
}
