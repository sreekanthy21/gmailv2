/**
 * @author krathod
 *
 */
package com.etouch.Image;

import java.util.LinkedList;

public class prepareLinkedList {
	private LinkedList<ImageCoOrdinates> compareLinkedList = new LinkedList<ImageCoOrdinates>();
	private LinkedList<ImageCoOrdinates> failLinkedList = new LinkedList<ImageCoOrdinates>();
	private LinkedList<ImageCoOrdinates> drawLinkedList = new LinkedList<ImageCoOrdinates>();
	private int blocks = 0;
	private int blockSize = 0;
	private ImageCoOrdinates imageCoOrdinates = null;

	/**
	 * Constructor call 
	 * @param imageCoOrdinates - Co-ordinates for which object is created
	 */
	public prepareLinkedList(ImageCoOrdinates imageCoOrdinates){
		this.blocks = imageCoOrdinates.getBlocks();
		this.blockSize = imageCoOrdinates.getBlockSize();
		this.imageCoOrdinates = imageCoOrdinates;
	}
	
	/**
	 * Prepares Linked list for comparison based on minimum block size and number of block criteria
	 * @param runFlag - boolean flag to determine if it is 1st run or a recursive run
	 * @return - Linked list for comparing
	 */
	public LinkedList<ImageCoOrdinates> prepareCompareLinkedList(boolean runFlag){
		if(((imageCoOrdinates.getHeight()/blocks)>blockSize)&&
				((imageCoOrdinates.getWidth()/blocks)>blockSize)){
			for(int i = 0; i < blocks; i++){
				for(int j = 0; j < blocks; j++){
					compareLinkedList.addLast(prepareData(j,i));
				}
			}
		} else if(runFlag){
			compareLinkedList.addFirst(prepareData(1,1));
		}
		return compareLinkedList;
	}
	
	/**
	 * Prepares linked list of failed co-ordinates
	 * @param imageCoOrdinates 
	 * @return 
	 */
	public LinkedList<ImageCoOrdinates> prepareFailLinkedList(ImageCoOrdinates imageCoOrdinates){
		failLinkedList.add(imageCoOrdinates);
		return failLinkedList;
	}
	
	/**
	 * Prepares linked list for co-ordinates which needs to be drawn
	 * @param imageCoOrdinates
	 * @return
	 */
	public LinkedList<ImageCoOrdinates> prepareDrawLinkedList(ImageCoOrdinates imageCoOrdinates){
		drawLinkedList.add(imageCoOrdinates);
		return drawLinkedList;
	}
	
	/**
	 * @return - size of compare linked list
	 */
	public int getCompareLinkedListSize(){
		return compareLinkedList.size();
	}
	
	/**
	 * @return - size of failed linked list
	 */
	public int getFailedLinkedListSize(){
		return 	failLinkedList.size();
	}
	
	/**
	 * @return - size of draw linked list
	 */
	public int getDrawLinkedListSize(){
		return drawLinkedList.size();
	}
	
	/**
	 * @return - Linked list for comparison
	 */ 
	public LinkedList<ImageCoOrdinates> getCompareLinkedList(){
		return compareLinkedList;
	}
	
	/**
	 * @return - linked list for failures
	 */
	public LinkedList<ImageCoOrdinates> getFailedLinkedList(){
		return failLinkedList;
	}
	
	/**
	 * @return - linked list for drawing
	 */
	public LinkedList<ImageCoOrdinates> getDrawLinkedList(){
		return drawLinkedList;
	}

	/**
	 * @return - Parent ImageCoOrdinate object 
	 */
	public ImageCoOrdinates getImageCoOrdinates(){
		return imageCoOrdinates;
	}
	
	/**
	 * New ImageCoOrdinate object 
	 * @param xMul - width multiplier
	 * @param yMul - height multiplier
	 * @return - New ImageCoOrdinate object 
	 */
	private ImageCoOrdinates prepareData(int xMul, int yMul){
		int newWidth = imageCoOrdinates.getWidth()/blocks;
		int newHeight = imageCoOrdinates.getHeight()/blocks;
		
		return new ImageCoOrdinates(imageCoOrdinates.getXStart() + xMul*newWidth, 
				imageCoOrdinates.getYStart()+ yMul*newHeight, imageCoOrdinates.getXStart() + (xMul+1)*newWidth, 
				imageCoOrdinates.getYStart() + (yMul+1)*newHeight, imageCoOrdinates.getPixelBuffer(), 
				blocks, blockSize);
	}
}
