/**
 * @author krathod
 *
 */

package com.etouch.Image;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.etouch.Image.ImageCoOrdinates;
import com.etouch.Image.CustomImageLogging;

public class ImageOperation {

	/**
	 * Finds edges in image and removes colour information.
	 * 3 Order of passes are performed for better detection of edges, Horizontal, Vertical and Diagonal
	 * Colour information is removed from input image and only edges are preserved
	 * @param img - Image which needs edge detection
	 * @return - BufferedImage which contains only edges and no colour
	 */
	public static BufferedImage findEdges(BufferedImage img){
		BufferedImage imgc = new BufferedImage(img.getWidth(null), img.getHeight(null), 
				BufferedImage.TYPE_INT_ARGB);
		imgc = findHorizontalEdges(img, imgc);
		imgc = findVerticalEdges(img, imgc);
		imgc = findDiagonalEdges(img, imgc);
		return imgc;
	}

	/**
	 * Image is traversed through x axis for detecting edges
	 * @param img - Input image for edge detection
	 * @param imgc - BufferedImage which contains output
	 * @return - Normalized image after horizontal pass
	 */
	private static BufferedImage findHorizontalEdges(BufferedImage img, BufferedImage imgc){
		for(int y = 0; y < img.getHeight(); y++){
			for(int x = 0; x <img.getWidth() - 1; x++){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x + 1 , y);
				if(currentPixel != nextPixel){
					imgc.setRGB(x + 1 , y , Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x + 1 , y , Color.WHITE.getRGB());
				}
			}
		}
		for(int y = img.getHeight() -1; y > 0; y--){
			for(int x = img.getWidth() - 1 ; x > 1; x--){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x - 1 , y);
				if(currentPixel != nextPixel){
					imgc.setRGB(x - 1 , y , Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x - 1 , y , Color.WHITE.getRGB());
				}
			}
		}
		return imgc;
	}

	/**
	 * Image is traversed through y axis for detecting edges
	 * @param img - Input image for edge detection
	 * @param imgc - BufferedImage which contains output
	 * @return - Normalized image after vertical pass
	 */
	private static BufferedImage findVerticalEdges(BufferedImage img, BufferedImage imgc){
		for(int y = 0; y < img.getHeight() - 1; y++){
			for(int x = 0; x <img.getWidth(); x++){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x , y + 1);
				if(currentPixel != nextPixel){
					imgc.setRGB(x , y + 1, Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x , y + 1, Color.WHITE.getRGB());
				}
			}
		}

		for(int y = img.getHeight() - 1; y > 1; y--){
			for(int x = img.getWidth() - 1; x > 0 ; x--){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x , y - 1);
				if(currentPixel != nextPixel){
					imgc.setRGB(x , y - 1, Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x , y - 1, Color.WHITE.getRGB());
				}
			}
		}
		return imgc;
	}

	/**
	 * Image is traversed diagonally for detecting edges
	 * @param img - Input image for edge detection
	 * @param imgc - BufferedImage which contains output
	 * @return - Normalized image after diagonal pass
	 */
	private static BufferedImage findDiagonalEdges(BufferedImage img, BufferedImage imgc){
		for(int y = 0; y < img.getHeight() - 1; y++){
			for(int x = 0; x <img.getWidth() - 1; x++){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x + 1, y + 1);
				if(currentPixel != nextPixel){
					imgc.setRGB(x + 1, y + 1, Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x + 1, y + 1, Color.WHITE.getRGB());
				}
			}
		}

		for(int y = img.getHeight() - 1; y > 1; y--){
			for(int x = img.getWidth() - 1; x > 1; x--){
				int currentPixel = img.getRGB(x, y);
				int nextPixel = img.getRGB(x - 1, y - 1);
				if(currentPixel != nextPixel){
					imgc.setRGB(x - 1, y - 1, Color.BLACK.getRGB());
				} else{
					imgc.setRGB(x - 1, y - 1, Color.WHITE.getRGB());
				}
			}
		}
		return imgc;
	}

	/**
	 * Creates luminance histogram for images for comparison and normalizes to 32 bins
	 * Histogram is created and normalized into 8 bands for faster processing
	 * @param img - Image whose histogram needs to be prepared
	 * @return - Histogram
	 */
	private static int[] createHistogram(BufferedImage img){
		int[] histogram = new int[256];
		int[] hist = new int[32];
		int colour = 0;
		int red = 0;
		int green = 0;
		int blue = 0;
		int luminance = 0;

		for (int y = 0; y < img.getHeight()-1; y++) {
			for (int x = 0; x < img.getWidth()-1; x++) {
				colour = img.getRGB(x, y);
				red = (colour >>> 16) & 0xFF;
				green = (colour >>> 8) & 0xFF;
				blue = (colour >>> 0) & 0xFF;
				luminance = (int)(red * 0.2126f + green * 0.7152f + blue * 0.0722f);
				histogram[luminance] = histogram[luminance] + 1;
			}
		}
		for(int x = 0; x < hist.length; x++){
			for(int i = x*8; i< x*8 + 8; i++){
				hist[x] = hist[x]+histogram[i];
			}
		}
		return hist;
	}

	/**
	 * @param hist - Histogram
	 * @return - Sum of elements of Histogram
	 */
	private static int sumOf(int[] hist){
		int total = 0;
		for(int i = 0; i < hist.length; i++){
			total += hist[i];
		}
		return total;
	}

	/**
	 * Returns statistical data after comparing 2 Histograms.
	 * A variation of Chi-Squared distance metric is used to find out differences between histograms
	 * @param img1 - Golden Image
	 * @param img2 - Screenshot
	 * @return - Chi-Squared distance metric calculated on the basis of 2 Histograms
	 */
	public static double getHistogramComparisonResult(BufferedImage img1, BufferedImage img2){
		int[] image1Histogram = createHistogram(img1);
		int[] image2Histogram = createHistogram(img2);
		double total = 0;
		int image1Total = sumOf(image1Histogram);
		int image2Total = sumOf(image2Histogram);
		for(int i = 0; i < image1Histogram.length; i++){
			if(image2Histogram[i]!=0&&image1Histogram[i]!=0){
				long diff = image2Total*image1Histogram[i]-image1Total*image2Histogram[i];
				total += (diff*diff)/(double)((image2Histogram[i]+ image1Histogram[i]));
			}
		}
		total = total/2;
		return total;
	}

	/**
	 * @param img - Image whose sub-image is needed
	 * @param coOrdinates - Co-Ordinates of sub image
	 * @return - Returns Sub-image
	 */
	public static BufferedImage getSubImage(BufferedImage img, 
			ImageCoOrdinates coOrdinates)throws RasterFormatException{
		BufferedImage subImage = img.getSubimage(coOrdinates.getXStart(), coOrdinates.getYStart(),
				coOrdinates.getWidth(), coOrdinates.getHeight());
		return subImage;

	}

	/**
	 * Converts Images to Buffered Image for comparison.
	 * @param img - Image to be buffered
	 * @return - BufferedImage
	 */
	public static BufferedImage imageToBufferedImage(Image img)throws NullPointerException {
		BufferedImage bi = null;
		Logger logger = CustomImageLogging.getLogger();
		try{
			bi = new BufferedImage(img.getWidth(null), img.getHeight(null), 
					BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2 = bi.createGraphics();
			if(!g2.drawImage(img, null, null)){
				logger.warning("Unable to draw Image - " );
			}
		}catch(NullPointerException e){
			logger.warning("Null Image - Unable to Initialise");
		}
		return bi;
	}

	/**
	 * Write a buffered image to a png file.
	 * @param img - Image to be saved
	 * @param filename - fileName
	 */
	public static void savePNG(Image img, String filename) {
		Logger logger = CustomImageLogging.getLogger();
		BufferedImage bi = imageToBufferedImage(img);
		FileOutputStream out = null;
		try { 
			out = new FileOutputStream(filename);
		} catch (java.io.FileNotFoundException io) { 
			logger.warning("File Not Found");
		}
		try {
			ImageIO.write(bi, "png", out);
		} catch (IOException e) {
			logger.warning("Unable to save file at specified  location");
			e.printStackTrace();
		}
		try { 
			out.close(); 
		} catch (java.io.IOException io) {
			logger.warning(io.getMessage());
		}
	}
}
