/**
 * @author krathod
 *
 */

package com.etouch.Image;

public class ImageCoOrdinates {
	private int xStart = 0;
	private int xEnd = 0;
	private int yStart = 0;
	private int yEnd = 0;
	private int pixelBuffer = 0;
	private int blocks = 0;
	private int blockSize = 0;
	
	/**
	 * @param xStart - Starting x co-ordinate for sub-image
	 * @param yStart - Starting y co-ordinate for sub-image
	 * @param xEnd - Ending x co-ordinate for sub-image
	 * @param yEnd - Ending y co-ordinate for sub-image
	 * @param pixelBuffer - number of buffer pixels
	 * @param blocks - number of blocks in which image is to be divided eg. blocks*blocks
	 * @param blockSize - minimum blocksSize
	 */
	public ImageCoOrdinates(int xStart, int yStart, int xEnd, int yEnd, int pixelBuffer, 
			int blocks, int blockSize){
		this.xStart = xStart;
		this.xEnd = xEnd;
		this.yStart = yStart;
		this.yEnd = yEnd;
		this.pixelBuffer = pixelBuffer;
		this.blocks = blocks;
		this.blockSize = blockSize;
	}

	/**
	 * @return - Height of sub image
	 */
	public int getHeight(){
		return(this.yEnd - this.yStart);
	}
	
	/**
	 * @return - Width of sub image
	 */
	public int getWidth(){
		return(this.xEnd - this.xStart);
	}

	/**
	 * @return - Starting co-ordinate for x
	 */
	public int getXStart(){
		return this.xStart;
	}

	/**
	 * @return - Ending co-ordinate for x
	 */
	public int getXEnd(){
		return this.xEnd ;
	}

	/**
	 * @return - Ending co-ordinate for y
	 */
	public int getYEnd(){
		return this.yEnd;
	}

	/**
	 * @return - Staring co-ordinate for y
	 */
	public int getYStart(){
		return this.yStart;
	}

	/**
	 * @return - buffer pixes
	 */
	public int getPixelBuffer(){
		return this.pixelBuffer;
	}
	
	/**
	 * @return - number of blocks
	 */
	public int getBlocks(){
		return this.blocks;
	}
	
	/**
	 * @return - minimum size of blocks in pixels
	 */
	public int getBlockSize(){
		return this.blockSize;
	}

	/**
	 * @return - true if co-ordinates are valid
	 */
	public boolean isValidCoOrdinates(int height, int width){
		return isValidXParam(width)&&isValidYParam(height);
	}
	/**
	 * @return - true if height is valid
	 */
	public boolean isValidHeight(){
		return (this.yEnd > this.yStart) ? true : false;
	}

	/**
	 * @return - true if width is valid
	 */
	public boolean isValidWidth(){
		return (this.xEnd > this.xStart) ? true : false;
	}
	
	/**
	 * @param width - width of image
	 * @param height - height of image
	 * @return - true if image has valid dimensions 
	 */
	public boolean isValidParam(int width, int height){
		return (isValidXParam(width) && isValidYParam(height))? true : false;
	}

	/**
	 * @param width - checks if width does not go out of image boundaries
	 * @return 
	 */
	public boolean isValidXParam(int width){
		boolean flag = true;
		flag = ((this.xStart + this.pixelBuffer)< 0 ? false : true )&&
				((this.xEnd + this.pixelBuffer) > width ? false : true );
		return flag;
	}

	/**
	 * @param height - checks if height does not go out of image boundaries
	 * @return
	 */
	public boolean isValidYParam(int height){
		boolean flag = true;
		flag = ((this.yStart + this.pixelBuffer)< 0 ? false : true )&&
				((this.yEnd + this.pixelBuffer) > height ? false : true );
		return flag;
	}
}
