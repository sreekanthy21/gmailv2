package com.etouch.Image;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

import com.etouch.Image.ImageCoOrdinates;
import com.etouch.Image.ImageCompare;
import com.etouch.Image.ImageOperation;
import com.etouch.Image.prepareLinkedList;

import java.util.logging.Logger;

public class ImageCompare extends ImageOperation{

	public BufferedImage expectedImage = null;
	public BufferedImage actualImage = null;
	private static BufferedImage edgeNormalisedExpectedImage = null;
	private static BufferedImage edgeNormalisedActualImage = null;
	private BufferedImage comparedImage = null;
	Graphics2D gc = null;
	public boolean match = true;
	public boolean flag = true;
	public boolean runFlag = true;
	private boolean draw = true;
	public boolean continueFlag = false;
	private static int executionCount = 1;
	private long startTime = 0;
	private long endTime = 0;
	private static Logger logger ;
	private boolean rasterFE = false;
	/**
	 * Initial constructor to be called with file paths.
	 * @param file1 - Path of Golden Image
	 * @param file2 - Path of Screenshot
	 */
	public ImageCompare(String file1, String file2){
		this(loadPNG(file1), loadPNG(file2));
	}

	/**
	 * Constructor 2 - Internally called with images.
	 * @param expectedImage - Golden image
	 * @param actualImage - Screenshot image
	 */
	public ImageCompare(Image expectedImage, Image actualImage)throws NullPointerException {
		this(imageToBufferedImage(expectedImage), imageToBufferedImage(actualImage));
	}

	/**
	 * Constructor 3 - Assigns BufferedImages to object variables.
	 * @param expectedImage - Golden image
	 * @param actualImage - Screenshot
	 */
	public ImageCompare(BufferedImage expectedImage, BufferedImage actualImage) {
		if(expectedImage==null||actualImage==null){
			rasterFE = true;
			logger.warning("Image initialization failed");
		}else{
			this.expectedImage = expectedImage;
			this.actualImage = actualImage;
			endTime = System.currentTimeMillis();
		}
	}

	/**
	 * Sets re-sized images as object variable
	 * @param expectedImage - Golden image
	 * @param actualImage - Screenshot
	 */
	public void setImages(BufferedImage expectedImage, BufferedImage actualImage) {
		this.expectedImage = expectedImage;
		this.actualImage = actualImage;
	}

	public static void setLogger(Logger logger2){
		logger = logger2;
	}

	/**
	 * imageCleanUp is performs in case if SoftKeys and Notification area is to be removed from image.
	 * @param img - Image to be cleaned
	 * @param top - Boolean flag to check if top/Notification need clean up
	 * @param bottom - Boolean flag to check if bottom/soft-keys need clean up
	 * @return - Returns Cleaned-up BufferedImage
	 */
	public BufferedImage imageCleanUp(BufferedImage img, boolean top, boolean bottom ){
		startTime = System.currentTimeMillis();
		logger.fine("Started Image cleanup");
		BufferedImage outputImage = null;
		BufferedImage croppedImage = null;
		int imageTop = (int) (expectedImage.getHeight()/24);
		int imageBottom = (int) (expectedImage.getHeight()/16.56);

		if(bottom){
			outputImage = new BufferedImage(expectedImage.getWidth(),expectedImage.getHeight()-imageBottom,
					expectedImage.getType());
		}else {
			outputImage = new BufferedImage(expectedImage.getWidth(),expectedImage.getHeight(), 
					expectedImage.getType());
		}

		Graphics2D g2d = outputImage.createGraphics();
		if(top){
			croppedImage = expectedImage.getSubimage(0, imageTop, expectedImage.getWidth(), 
					outputImage.getHeight()-imageTop);
		}else {
			croppedImage = expectedImage.getSubimage(0, 0, expectedImage.getWidth(), 
					outputImage.getHeight());
		}
		g2d.drawImage(croppedImage, 0, 0,expectedImage.getWidth(), expectedImage.getHeight(), null);
		g2d.dispose();
		endTime = System.currentTimeMillis();
		logger.fine("Image cleanup completed : Time taken - " + (endTime-startTime)+ " milliSeconds");
		return outputImage;
	}

	/**
	 * Resizing is performed in case the dimensions of Golden Image and Screenshot does not match.
	 * Note :- File location to be updated for relative location
	 * @param expectedImage - Golden Image
	 * @param actualImage - Screenshot
	 */
	public void ImageResize(BufferedImage expectedImage, BufferedImage actualImage){
		startTime = System.currentTimeMillis();
		logger.fine("Started Image resizing");
		String outputImagePath1 = "FilePath1"; // File location to be updated 
		String outputImagePath2 = "FilePath2"; // File location to be updated
		int imageWidth = 0;
		int imageHeight = 0;
		if(expectedImage.getHeight()>actualImage.getHeight()){
			imageHeight = actualImage.getHeight();
			imageWidth = actualImage.getWidth();
		}else{
			imageHeight = expectedImage.getHeight();
			imageWidth = expectedImage.getWidth();
		}

		BufferedImage outputImage1 = new BufferedImage(imageWidth,imageHeight, expectedImage.getType());
		Graphics2D g2d1 = outputImage1.createGraphics();
		g2d1.drawImage(expectedImage, 0, 0, imageWidth, imageHeight, null);
		g2d1.dispose();
		BufferedImage outputImage2 = new BufferedImage(imageWidth,imageHeight, actualImage.getType());
		Graphics2D g2d2 = outputImage2.createGraphics();
		g2d2.drawImage(actualImage, 0, 0, imageWidth, imageHeight, null);
		g2d2.dispose();
		String formatName1 = outputImagePath1.substring(outputImagePath1.lastIndexOf(".") + 1);
		String formatName2 = outputImagePath2.substring(outputImagePath2.lastIndexOf(".") + 1);
		try {
			ImageIO.write(outputImage1, formatName1, new File(outputImagePath1));
			ImageIO.write(outputImage2, formatName2, new File(outputImagePath2));
			setImages(outputImage1,outputImage2);
		} catch (IOException e) {
			logger.info("Unable to write Image to path specified" + outputImagePath1 + " and " + outputImagePath2);
			e.printStackTrace();
		}
		this.expectedImage = expectedImage;
		this.actualImage = actualImage;
		endTime = System.currentTimeMillis();
		logger.fine("Image resizing completed : Time taken - " + (endTime-startTime)+ " milliSeconds");
	}

	/**
	 * Creates ImageCoOrdinate object for image verification.
	 * Comparison process starts if following conditions are met.
	 * Co-Ordinates should  be valid.
	 * If end Condition is not met during recursion.
	 * 
	 * @param xStart - Starting x co-ordinate
	 * @param yStart - Starting y co-ordinate
	 * @param xEnd - Ending x co-ordinate
	 * @param yEnd - Ending y co-ordinate
	 * @param comparedImage - Buffered Image which is returned to be saved as output.
	 * @param pixelBuffer - Number of buffer pixels
	 * @param blocks - Number of sub-blocks for dividing image, eg. blocks*blocks
	 * @param minBlockSize - Minimum block size to which image will be divided into during recursion. 
	 * @param passCriteria - Percentage passing for image
	 * @param runFlag - Flag describing the run - True for first and false for recursion
	 * @return - Returns BufferedImage to be saved as output.
	 */
	public BufferedImage compareImage(int xStart, int yStart, int xEnd, int yEnd, BufferedImage comparedImage,
			int pixelBuffer, int blocks, int minBlockSize, int passCriteria, boolean runFlag) {

		if(runFlag){
			startTime = System.currentTimeMillis();
			logger.fine("Image Comparison started for : Level - " + executionCount);
		}
		this.comparedImage = comparedImage;
		this.runFlag = runFlag;
		ImageCoOrdinates imageCoOrdinates = new ImageCoOrdinates(xStart, yStart, xEnd, yEnd, pixelBuffer, 
				blocks, minBlockSize);

		if(!rasterFE){
			if(imageCoOrdinates.isValidCoOrdinates(actualImage.getHeight(), actualImage.getWidth())){
				if(!isEdgeImageReady()){
					createEdgeImages();
				}

				prepareLinkedList pLinkedList = new prepareLinkedList(imageCoOrdinates);
				pLinkedList.prepareCompareLinkedList(runFlag);

				if(pLinkedList.getCompareLinkedListSize()!=0){
					startCompare(pLinkedList, passCriteria);
					continueFlag = true;
				}
			} else {
				comparedImage = null;
				logger.info("Invalid Image co-ordinates provided, Check datasheet");
			}
			if(runFlag){
				endTime = System.currentTimeMillis();
				logger.fine("Image comparison completed for : Level - "+ executionCount 
						+" : Time taken - " + (endTime-startTime) + " milliSeconds");
			}
			if(rasterFE){
				return null;
			}
		}else{
			return null;
		}
		
		return comparedImage;
	}

	/**
	 * Performed startCompare using recursion for failed sub-images.
	 * @param prLinkedList - preparelinkedList object
	 * @param passCriteria - Passing criteria
	 */
	private void startCompare(prepareLinkedList prLinkedList, int passCriteria){
		ImageCoOrdinates coOrdinates ;
		float diffAC = 0;
		LinkedList<ImageCoOrdinates> cLinkedList = prLinkedList.getCompareLinkedList();
		startTime = System.currentTimeMillis();
		logger.fine("Image comparison at block level started : ");
		logger.fine("Number of blocks to be verified : " + prLinkedList.getCompareLinkedListSize());
		try{
			while(!prLinkedList.getCompareLinkedList().isEmpty()){
				coOrdinates = cLinkedList.poll();
				edgeNormalisedExpectedImage = imageToBufferedImage(edgeNormalisedExpectedImage);
				edgeNormalisedActualImage = imageToBufferedImage(edgeNormalisedActualImage);
				diffAC = getEdgeDifferencess(coOrdinates);
				if(diffAC > (100-passCriteria)&&!getHistogramDifferences(coOrdinates)){
					this.match = false;
					prLinkedList.prepareFailLinkedList(coOrdinates);
				}
			}
		}catch(RasterFormatException e){
			logger.warning("Co-Ordniates specified out of Image Raster - Check if screenshot and golden Image "
					+ " are taken for same mobile device");
			rasterFE = true;
		}
		endTime = System.currentTimeMillis();
		logger.fine("Image comparison at block level ended : Time taken - " + 
				(endTime-startTime)+ " milliSeconds");
		if(prLinkedList.getFailedLinkedListSize()!=0&&!rasterFE){
			logger.fine("***Image Comparison failed");
			logger.fine("***Comparing Images at sub-block level recursively");
			if(prLinkedList.getCompareLinkedListSize()==prLinkedList.getFailedLinkedListSize()){
				prLinkedList.prepareDrawLinkedList(prLinkedList.getImageCoOrdinates());
			} else{
				startTime = System.currentTimeMillis();
				executionCount++;
				logger.fine("***Recursion level " + executionCount +" started : ");
				while(!prLinkedList.getFailedLinkedList().isEmpty()){
					ImageCoOrdinates newCoOrdinates = prLinkedList.getFailedLinkedList().poll();
					ImageCompare ic = new ImageCompare(edgeNormalisedExpectedImage, edgeNormalisedActualImage);
					ic.compareImage(newCoOrdinates.getXStart(), newCoOrdinates.getYStart(),
							newCoOrdinates.getXEnd(), newCoOrdinates.getYEnd(), 
							comparedImage, newCoOrdinates.getPixelBuffer(), newCoOrdinates.getBlocks(),
							newCoOrdinates.getBlockSize(), passCriteria, false);
					if(!ic.continueFlag){
						prLinkedList.prepareDrawLinkedList(newCoOrdinates);
					}
				}
				endTime = System.currentTimeMillis();
				logger.fine("***Recursion level "+executionCount-- +" ended : "
						+ " Time taken - "+ (endTime-startTime) + " milliSeconds");
			}
		} 

		if(shouldPrint()){
			drawBoundaries(prLinkedList.getImageCoOrdinates());
		}else {
			while(!prLinkedList.getDrawLinkedList().isEmpty()){
				drawBoundaries(prLinkedList.getDrawLinkedList().poll());
			}
		}
	}

	/**
	 * @return - Boolean decision to print Pass or Fail blocks
	 */
	private boolean shouldPrint(){
		return runFlag&&this.match&&!rasterFE;
	}

	/**
	 * Draw Recangular boundraies around section of images which were verified .
	 * @param coOrdinates - Co-ordinates at which drawing is needed
	 */
	private void drawBoundaries(ImageCoOrdinates coOrdinates){
		gc = comparedImage.createGraphics();
		if(shouldPrint()&&this.match){
			gc.setColor(Color.GREEN);
			gc.drawRect(coOrdinates.getXStart(), coOrdinates.getYStart(), 
					coOrdinates.getWidth() , coOrdinates.getHeight() );
		} else{
			gc.setColor(Color.RED);
			gc.drawRect(coOrdinates.getXStart(), coOrdinates.getYStart(), 
					coOrdinates.getWidth() , coOrdinates.getHeight() );
		}
	}

	/**
	 * Difference in edges is computated for failed percentage.
	 * @param coOrdinates - Co-ordinate of sub image which needs to be tested
	 * @return - Returns percentage value describing match
	 */
	private float getEdgeDifferencess(ImageCoOrdinates coOrdinates)throws RasterFormatException{
		int b1 = 0;
		int b2 = 0;
		b2 = getAverageChanges(getSubImage(actualImage,coOrdinates));
		b1 = getAverageChanges(getSubImage(expectedImage,coOrdinates));

		float diff = Math.abs(b1 - b2)*100;
		if(b1!=0||b2!=0){
			diff = diff/(b1+b2);
		}
		logger.fine("******Edge difference percentage - "+ (100-diff));
		return diff;
	}

	/**
	 * Returns boolean decision based on Histogram mathching.
	 * A variation of chi-squared distance metric is used to find differences between 2 histograms.
	 * Critical value X = 42.557 is calculated based on following 
	 * c = 2(params) and k = 32(bins) 
	 * Degree of Freedom = k - (c + 1) 
	 * Significance level = 0.05(standard value) 
	 * @param coOrdinates - Co-ordinates of sub image which needs matching
	 * @return - boolean decision based on histogram matching
	 */
	private boolean getHistogramDifferences(ImageCoOrdinates coOrdinates){
		boolean flag = false;
		double chiSqaredTest = getHistogramComparisonResult(getSubImage(expectedImage, 
				coOrdinates), getSubImage(actualImage, coOrdinates));
		if(chiSqaredTest < 42.557){
			flag = true;
		}
		logger.fine("******Histogram mapping is " + flag);
		return flag;
	}

	/**
	 * @return - Return the image that indicates the regions where changes where detected.
	 */
	public BufferedImage getChangeIndicator() {
		return comparedImage;
	}

	/**
	 * @param img - Image in which change is to be detected
	 * @return - Returns avg changes in an edge normalised image
	 */
	private int getAverageChanges(BufferedImage img) {
		int xTotal = 0;
		int yTotal = 0;
		int xyTotal = 0;
		for (int y = 0; y < img.getHeight() - 1; y++) {
			for (int x = 0; x < img.getWidth() - 1 ; x++) {
				if(img.getRGB(x, y)!=img.getRGB(x + 1, y)){
					xTotal +=1;
				}
				if(img.getRGB(x, y)!=img.getRGB(x, y+1)){
					yTotal +=1;
				}
				if(img.getRGB(x, y)!=img.getRGB(x+1, y+1)){
					xyTotal +=1;
				}
			}
		}
		return (xTotal+yTotal+xyTotal);
	}

	/**
	 * @return - Returns status if Images are edge normalized/detected 
	 */
	private boolean isEdgeImageReady(){
		boolean ready = true;
		if(edgeNormalisedExpectedImage==null||edgeNormalisedActualImage==null){
			ready = false;
			logger.fine("******Images are not edgeNormalised");
		}
		return ready;
	}

	/**
	 * Normalizes images for edges
	 */
	private void createEdgeImages(){
		logger.fine("******Preparing Images for edgeNormalising");
		edgeNormalisedExpectedImage = findEdges(this.expectedImage);
		edgeNormalisedActualImage = findEdges(this.actualImage);
	}

	/**
	 * @return - returns true if image pair is considered a match
	 */
	public boolean isMatch() {
		return this.match;
	}

	/**
	 * read a PNG file into a buffered image
	 * @param filename - File path of image
	 * @return - Image
	 */
	public static Image loadPNG(String filename)throws NullPointerException {
		logger.fine("Loading image files");
		FileInputStream in = null;
		BufferedImage bo = null;
		BufferedImage bi = null;
		try { 
			in = new FileInputStream(filename);
			bo = ImageIO.read(in);
			bi = new BufferedImage(bo.getWidth(), bo.getHeight(),BufferedImage.TYPE_INT_ARGB);
			bi.createGraphics().drawImage( bo, 0, 0, Color.BLACK, null);
			in.close(); 
		} catch (java.io.FileNotFoundException io) {
			logger.warning("File Not Found - " + filename);
		} catch (IOException e) {
			logger.warning(e.getMessage()+ " - " + filename);
			logger.info("IO Exception encountered - " + e.getMessage());
			e.printStackTrace();
		}catch (IllegalArgumentException e){
			logger.warning("Null Input -  File not found - " + e.getMessage());
		}catch(NullPointerException e){
			logger.warning("Null Input -  File not found - " + e.getMessage());
		}
		return bi;
	}
}