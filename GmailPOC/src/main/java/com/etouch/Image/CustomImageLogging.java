/**
 * @author krathod
 *
 */
package com.etouch.Image;

import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.etouch.mobile.gmail.GmailTest;

public class CustomImageLogging {
	
	private static ThreadLocal<Logger> threadLocalLogger = new ThreadLocal<Logger>();
	
	/**Creates Java Util Logger for Image comparison
	 * @param fileName - Log file name to be saved
	 * @return - Returns logger instance
	 */
	public static Logger startLogging(String fileName){
		Logger logger = threadLocalLogger.get();
		if(logger==null){
			logger = Logger.getLogger(GmailTest.class.getName());
			logger.setLevel(Level.ALL);
			Formatter formatter = new CustomLogFormatter();
			Handler handler = new CustomImageLogHandler(fileName);
			handler.setFormatter(formatter);
			logger.addHandler(handler);
			threadLocalLogger.set(logger);
		}
		return logger;
	}
	
	/**
	 * @return - Returns current logger instance
	 */
	public static Logger getLogger(){
		return threadLocalLogger.get();
	}
}
