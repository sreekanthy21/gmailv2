/**
 * @author krathod
 *Creating custom handler for Java Util logger
 */
package com.etouch.Image;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class CustomImageLogHandler extends Handler {
	private int limit = 1024 * 1024;
	private String fileName = "ImageComaprison.log";
	private BufferedWriter bufferedWriter;
	private File file = null;

	/**
	 * @return - Returns File path
	 */
	private File getFile() {
		return file;
	}

	/**
	 * @return - Returns file name
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Default constructor 
	 */
	public CustomImageLogHandler() {
		open();
	}

	/**
	 * Constructor 
	 * @param fileName - FileName
	 */
	public CustomImageLogHandler(String fileName) {
		this.fileName = fileName;
		open();
	}

	/**
	 * Create new filename with timestamps
	 * @return
	 */
	protected String newFileName() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
		return  getFileName().replace(".log", "")+ "_" +sdf.format(cal.getTime()) + ".log";
	}

	/**
	 * Create File
	 */
	protected void open() {
		try {
			file = new File(getFileName());
			bufferedWriter = new BufferedWriter(new FileWriter(newFileName(), true));
		} catch (Exception e) {
			throw new RuntimeException("Exception occured while opening a logger.", e);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.logging.Handler#publish(java.util.logging.LogRecord)
	 */
	@Override
	public void publish(LogRecord record) {
		if (!isLoggable(record)) {
			return;
		}
		try {
			synchronized (bufferedWriter) {
				if (getFile().length() > limit) {
				}
				if (getFormatter() != null) {
					bufferedWriter.write(getFormatter().format(record));
				} else {
					bufferedWriter.write(Calendar.getInstance().getTime() + record.getMessage());
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception occured while publishing a log.", e);
		}
	}

	/* (non-Javadoc)
	 * @see java.util.logging.Handler#flush()
	 */
	@Override
	public void flush() {
		// TODO Auto-generated method stub
	}
	
	/* (non-Javadoc)
	 * @see java.util.logging.Handler#close()
	 */
	@Override
	public void close() throws SecurityException {
		try {
			if (bufferedWriter != null) {
				bufferedWriter.close();
			}
		} catch (Exception e) {
			throw new RuntimeException("Exception occured while closing a log.", e);
		}
	}


}