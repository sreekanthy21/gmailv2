package com.etouch.gmail.commonPages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;

public class NavigationDrawer extends InboxFunctions {

	/** The log. */
	private static Log log = LogUtil.getLog(NavigationDrawer.class);

	/**
	 * Method to verify Inbox Categories
	 * 
	 * @throws Exception
	 */
	public void verifyInboxCategories(AppiumDriver driver) throws Exception {
		log.info("******************* verifyInboxCategories*******************");
		try {

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxCategory"));
			String ExpectedInboxCategoriesList = commonElements.get("ExpectedInboxCategoriesList");

			List<WebElement> elemetList = driver.findElements(By.id(commonElements.get("InboxCategoriesList")));
			String actualList = new String();
			for (WebElement ele : elemetList) {
				actualList = actualList.concat(ele.getText()) + ", ";
			}
			Assert.assertTrue(actualList.contains(ExpectedInboxCategoriesList),
					"Inbox Categories are Expected:" + ExpectedInboxCategoriesList + "   Actual: " + actualList);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed verifyInboxCategories :::" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to Bin
	 * 
	 * @throws Exception
	 */
	public void navigateToBin(AppiumDriver driver, String BinXpath) throws Exception {
		try {

			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption"))!=0){
				CommonFunctions.scrollsToTitle(driver, "Trash");
			}else{

				waitForHambergerMenuToBePresent();
				openMenuDrawer(driver);
				log.info("From menu drawer, visit  'Trash' section");
				/*scrollDown(1);
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, BinXpath);*/
				CommonFunctions.scrollsToTitle(driver, "Trash");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to Navigate to Bin Due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to Snooze
	 * 
	 * @throws Exception
	 */
	public void navigateToSnooze(AppiumDriver driver) throws Exception {
		try {
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SnoozeTitle"))!=0){
				log.info("already in snooze folder");
			}else{

				waitForHambergerMenuToBePresent();
				openMenuDrawer(driver);
				log.info("From menu drawer, visit  'Snooze' section");
				/*scrollDown(1);
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, BinXpath);*/
				CommonFunctions.scrollsToTitle(driver, "Snoozed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to Navigate to Bin Due to : " + e.getLocalizedMessage());
		}
	}


	/**
	 * Method to switch to primary inbox
	 * 
	 * @throws Exception
	 */
	public void switchToPrimaryInbox(AppiumDriver driver) throws Exception {

		openMenuDrawer(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
	}

	/**
	 * Method to tap on Archive button in Mail Tool Bar and verify Snacker Bar
	 * is displayed
	 * 
	 * @throws Exception
	 */
	public void tapOnArchive(AppiumDriver driver) throws Exception {

		log.info("tapping on Tap on 'Archive' button");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		}
		if (!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnackerBar"))) {
			throw new Exception("Snacker bar was not seen after Archive operation");
		} else {
			log.info("Snacker Bar is displayed");
		}
	}

	/**
	 * Method to tap on Delete
	 * 
	 * @throws Exception
	 */
	public void tapOnDelete(AppiumDriver driver) throws Exception {

		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		}
		if (!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnackerBar"))) {
			throw new Exception("Snacker bar was not seen after Archive operation");
		} else {
			log.info("Snacker Bar is displayed");
		}
	}

	/**
	 * Method to verify Ad Forward Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void verifyDefaultInbox(AppiumDriver driver, String user) throws Exception {
		log.info("Selecting Default Inbox ");
		try {

			waitMap.get(Thread.currentThread().getId())
			.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("InboxType"))));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxType"));
			waitMap.get(Thread.currentThread().getId())
			.until(ExpectedConditions.presenceOfElementLocated((By.xpath(commonElements.get("DefaultInbox")))));
			driver.findElementByXPath(commonElements.get("DefaultInbox")).click();
			log.info("Default Inbox Selected");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed verify Default Inbox Check :::" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad Forward Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void enablePromotionAndSocialCategoriesfromInboxCategories(AppiumDriver driver,
			boolean isPromotionsInboxEnabled) throws Exception {
		log.info("Enabling Promotion And SocialCategories Functionality :"+commonElements.get("InboxCategory"));
		try {

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxCategory"));
			if (driver.findElement(By.xpath(commonElements.get("SocialCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("SocialCheckBox"))).click();
			}

			if (driver.findElement(By.xpath(commonElements.get("PromoCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("PromoCheckBox"))).click();
			}
			navigateBack(driver);
			navigateBack(driver);
			navigateBack(driver);
			if (isPromotionsInboxEnabled == false) {
				openMenuDrawer(driver);
				scrollUp(1);
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption"))) {
				} else {
					log.info("Opening APP Again:::->");
					launchGmailApp(driver);
					openMenuDrawer(driver);
					scrollUp(1);
				}
			} else {
				openMenuDrawer(driver);
				scrollUp(1);
			}
			Assert.assertTrue(
					CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption")),
					"Promotions inbox option NOT rendered");
			Assert.assertTrue(
					CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SocialInboxOption")),
					"Social inbox option NOT rendered");
			// CommonPage.navigateBack(driver);
			log.info("Promotion And SocialCategories enabled");

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed enablePromotionAndSocialCategories :::" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad Forward Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void enableAllInboxCategories(AppiumDriver driver, boolean isPrimaryInboxEnabled) throws Exception {
		log.info("Enabling all inbox Categorie");
		try {

			if (driver.findElement(By.xpath(commonElements.get("SocialCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("SocialCheckBox"))).click();
			}

			if (driver.findElement(By.xpath(commonElements.get("PromoCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("PromoCheckBox"))).click();
			}
			if (driver.findElement(By.xpath(commonElements.get("UpdatesCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("UpdatesCheckBox"))).click();
			}
			if (driver.findElement(By.xpath(commonElements.get("ForumsCheckBox"))).getAttribute("checked")
					.equalsIgnoreCase("false")) {
				driver.findElement(By.xpath(commonElements.get("ForumsCheckBox"))).click();
			}
			navigateBack(driver);
			navigateBack(driver);
			navigateBack(driver);
			if (isPrimaryInboxEnabled == false) {
				openMenuDrawer(driver);
				scrollUp(1);
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption"))) {
				} else {
					log.info("Opening APP Again:::-");
					launchGmailApp(driver);
					openMenuDrawer(driver);
					scrollUp(1);
				}
			} else {
				openMenuDrawer(driver);
				scrollUp(1);
			}
			Assert.assertTrue(
					CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption")),
					"Promotions Inbox tab not Rendered");
			Assert.assertTrue(driver.findElement(By.xpath(commonElements.get("SocialInboxOption"))).isDisplayed(),
					"Social Inbox tab not Rendered");
			Assert.assertTrue(driver.findElement(By.xpath(commonElements.get("UpdatesInboxOption"))).isDisplayed(),
					"Update Inbox tab not Rendered");
			Assert.assertTrue(driver.findElement(By.xpath(commonElements.get("ForumsInboxOption"))).isDisplayed(),
					"Forums  tab not Rendered");
			log.info("All inbox Categories enabled");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed enablePromotionAndSocialCategories :::" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad Forward Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void verifyPriorityInbox(AppiumDriver driver, String user) throws Exception {
		log.info("Enabling Priority Inbox");
		try {

			waitMap.get(Thread.currentThread().getId())
			.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("InboxType"))));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxType"));
			waitMap.get(Thread.currentThread().getId()).until(
					ExpectedConditions.presenceOfElementLocated((By.xpath(commonElements.get("PriorityInbox")))));
			driver.findElementByXPath(commonElements.get("PriorityInbox")).click();
			navigateBack(driver);
			navigateBack(driver);
			openMenuDrawer(driver);
			scrollUp(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AllIndoxesOption"));
			openMenuDrawer(driver);
			/*Assert.assertEquals(driver.findElement(By.xpath(commonElements.get("PriorityInboxMail"))).getText(),
					"Priority Inbox", "Priority Inbox text NOT matched ");
			 */
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PriorityInboxMail"));
			log.info("Priority Inbox enabled");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PriorityInboxMail"));
			int priorityInboxMailCount = driver.findElementsByXPath(commonElements.get("mailCount")).size();
			openMenuDrawer(driver);
			/*Assert.assertEquals(driver.findElement(By.xpath(commonElements.get("InboxOption"))).getText(), "Inbox",
					"Inbox option text NOT matched ");
			 */CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
			 /*navigateBack(driver);
			int inboxMailCount = driver.findElementsByXPath(commonElements.get("mailCount")).size();
			// Assert.assertEquals(inboxMailCount, priorityInboxMailCount,
			// "Email Count NOT matched ");
			navigateBack(driver);*/
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed verify Priority Inbox Check :::" + e.getLocalizedMessage());
		}
	}

	public void openUserAccountSetting(AppiumDriver driver, String user) throws Exception {
		log.info("Opening User Specific Setting");
		try {
			Thread.sleep(1500);
			
			// commented by Venkat on 01/12/2019
/*			
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SettingsOption"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			}else{
				openMenuDrawer(driver);
				log.info("openUserAccountSetting for user: " + user);
				CommonFunctions.scrollsToTitle(driver, "Settings");
				//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			}
*/			
			// commented by Venkat on 01/12/2019
			// added by Venkat on 01/12/2019: start
			userAccount.openMenuDrawerNew(driver);
			CommonFunctions.scrollsToTitle(driver, "Settings");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			//userAccount.goToAccountsListPage(driver);
			//CommonFunctions.clickOnAddAccountOrManageAccounts(driver, "", "");
			// added by Venkat on 01/12/2019: start
			
			//CommonFunctions.searchAndClickByXpath(driver,commonElements.get("AddAccountOption").replace("Add account", user)); //commented by Venkat on 14/12/2018
			//written by Venkat on 14/12/2018: start
			int count = 0;
			WebElement element = null;
			boolean isElementFound = false;
			while(count <=2){
				count ++;
				try{
					Thread.sleep(1000);
					element = driver.findElement(By.xpath(commonElements.get("AddAccountOption").replace("Add account", user)));
					if(element != null){
						element.click();
						isElementFound = true;
						break;
					}
				}catch(Exception e){
					scrollDown(1);
				}
			}
			count = 0;
			while(!isElementFound && count++ <=2){
				try{
					Thread.sleep(1000);
					driver.findElement(By.xpath(commonElements.get("AddAccountOption").replace("Add account", user))).click();
					break;
				}catch(Exception e){
					scrollUp(1);
				}
			}
			//written by Venkat on 14/12/2018: end
			
			Thread.sleep(1000);
			log.info("Account settings opened");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to openUserSpecificSetting:::" + e.getLocalizedMessage());
		}
	}


	/**
	 * Method to open Primary Inbox
	 * 
	 * @param user
	 * @throws Exception
	 */
	public void openPrimaryInbox(AppiumDriver driver, String user) throws Exception {
		log.info("******************* Start to open PrimaryInbox for user "+user+"*******************");
		try {
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryTitle"))!=0){
				log.info("Primary section is already opened");
			}else{
				openMenuDrawer(driver);
				boolean isPrimaryInboxEnabled = isPrimaryInboxDisplayed(driver);
				navigateBack(driver);
				if (!isPrimaryInboxEnabled) {
					log.info(isPrimaryInboxEnabled);
					openUserAccountSetting(driver, user);
					verifyDefaultInbox(driver, user);
					enablePromotionAndSocialCategoriesfromInboxCategories(driver, isPrimaryInboxEnabled);
					navigateBack(driver);
					navigateBack(driver);
					openMenuDrawer(driver);
					scrollUp(1);
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PrimaryInboxOption"))) {
					} else {
						launchGmailApp(driver);
						openMenuDrawer(driver);
					}
				} else {
					openMenuDrawer(driver);
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
				log.info("******************* Open PrimaryInbox displayed properly*******************");
			} }catch (Throwable e) {
				e.printStackTrace();
				throw new Exception("Failed to openPrimaryInbox:::" + e.getLocalizedMessage());
			}
	}

	public void EnablePrimaryInbox(AppiumDriver driver, String user) throws Exception {
		log.info("******************* Start to open PrimaryInbox for user "+user+"*******************");
		try {
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryTitle"))!=0){
				log.info("EnablePrimaryInbox Primary section is already opened");
			}else{
				openMenuDrawer(driver);
				boolean isPrimaryInboxEnabled = isPrimaryInboxDisplayed(driver);
				navigateBack(driver);
				if (!isPrimaryInboxEnabled) {
					log.info("EnablePrimaryInbox :"+isPrimaryInboxEnabled);
					openUserAccountSetting(driver, user);
					verifyDefaultInbox(driver, user);
					navigateBack(driver);
					navigateBack(driver);
					openMenuDrawer(driver);
					scrollUp(1);
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PrimaryInboxOption"))) {
					} else {
						launchGmailApp(driver);
						openMenuDrawer(driver);
					}
				} else {
					openMenuDrawer(driver);
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
				log.info("******************* Open PrimaryInbox displayed properly*******************");
			} }catch (Throwable e) {
				e.printStackTrace();
				throw new Exception("Failed to openPrimaryInbox:::" + e.getLocalizedMessage());
			}
	}
	/**
	 * Navigates to All inbox section displaying mail from all added accounts in
	 * a single inbox
	 * 
	 * @throws Exception
	 */
	public void navigateToAllInbox(AppiumDriver driver) throws Exception {

		openMenuDrawer(driver);
		log.info("Navigate to All Inbox");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AllIndoxesOption"));
		waitForHambergerMenuToBePresent();
	}

	/**
	 * Navigates to Manage accounts option
	 * @param driver
	 * @throws Exception
	 */
	public void navigateToManageAccountsNew(AppiumDriver driver, Map<String, String> data) throws Exception {
		log.info("ND navigateToManageAccounts starts..  ");
		try{
			CommonFunctions.searchAndClickByXpath(driver, data.get("Account_Switcher_Icon"));
			CommonFunctions.scrollDown(driver, 2);
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, data.get("ManageAccounts"));
			log.info("ND.navigateToManageAccounts. Clicked on Manage accounts on this device");
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("AccountSettings"));
			log.info("ND.navigateToManageAccounts Navigated to ManageAccounts");

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void navigateToManageAccounts(AppiumDriver driver) throws Exception {
		log.info("ND navigateToManageAccounts starts..  ");
		try{

			//if(CommonFunctions.getSizeOfElements(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']")!=0){
			if(BaseTest.isAccountSwitcherExist) {
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
				Thread.sleep(2000);
				WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
				org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
				int x = element.getLocation().x;
				int y = element.getLocation().y;
				log.info("In ND.navigateToManageAccounts X coordinates "+x+ " Y coordinates "+y);
				//driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
				//driver.swipe(x, y, ((windowSize.width)-20), y-50, 1000); // Commented by Venkat on 07/06/2019
				Thread.sleep(1000);
				CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
				//scrollTo("Manage accounts on this device");
				Thread.sleep(2000);
			//	CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Manage accounts on this device')] | //android.widget.TextView[@resource-id='com.google.android.gm:id/Text']"); // added by Venkat on 14/11/2018
				log.info("ND.navigateToManageAccounts. Clicked on Manage accounts on this device");
			}else{
				openMenuDrawer(driver);
				if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("FolderIcon"))){
					try{
						CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
					}catch(Exception e){
						log.info("ND.Seems no all inboxes and trying to click on account switcher");

					}
					CommonFunctions.searchAndClickById(driver, commonElements.get("AccountText"));
				}
				CommonFunctions.scrollsToTitle(driver, "Manage accounts");

				//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AllIndoxesOption"));
				//waitForHambergerMenuToBePresent();
			}
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("AccountSettings"));
			log.info("ND.navigateToManageAccounts Navigated to ManageAccounts");

		}catch(Exception e){
			e.printStackTrace();
		}

	}


	/**
	 * Method to verify Primar Inbox is rendered or not
	 * 
	 * @return
	 */
	public boolean isPrimaryInboxDisplayed(AppiumDriver driver) {
		try {

			boolean result = CommonFunctions.getSizeOfElements(driver,commonElements.get("PrimaryInboxOption"))!=0;

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			log.info(
					"Unable to find Element ::: Locator of element ::: " + commonElements.get("isPrimaryInboxEnabled"));
			return false;
		}
	}

	/**
	 * Method to navigate to sent box
	 * 
	 * @throws Exception
	 */
	public void navigateToDrafts(AppiumDriver driver) throws Exception{
		try{
			/*if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("DraftsOption"))){
				log.info("Drafts option is not found, hence scrolling down again");
				scrollDown(1);
			}

			--Check for the drafts messages if any--
			int priorityDraftsCount = driver.findElementsByXPath(commonElements.get("DraftsMessagesCount")).size();
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("NoDrafts")))
			log.info("Tapping on drats folder under menu drawer");
			Thread.sleep(2000);
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("DraftsOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DraftsOption"));*/
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DraftsOption"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DraftsOption"))!=0){
				log.info("already in drafts folder");
			}else{
				CommonFunctions.scrollsToTitle(driver, "Drafts");
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Failed to click on drafts option "+e.getLocalizedMessage());
		}
	}


	/**
	 * Method to go back to another screen page
	 * 
	 *  @throws Exception
	 */

	public void navigateBackTo(AppiumDriver driver) throws Exception{
		try{
			log.info("Navigate back to Drafts screen page");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));

		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Failed to navigate back: "+e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to sent box
	 * 
	 * @throws Exception
	 */
	public void navigateToSentBox(AppiumDriver driver) throws Exception {
		try {
			log.info("Navigating to sent folder");
			// Commented by Venkat on 06/05/2019: start
			/*
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BackButton"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
			}
			*/
			// Commented by Venkat on 06/05/2019: end
			waitForHambergerMenuToBePresent();
			openMenuDrawer(driver);
			/*Thread.sleep(7000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SentOption"))==0) {
				scrollDown(1);
			}
			log.info("Tapping on Sent folder under Menu drawer");
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SentOption"));*/
			CommonFunctions.scrollsToTitle(driver, "Sent");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to switch to Sent Box Due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to Primary box
	 * 
	 * @throws Exception
	 */
	public void navigateToPrimary(AppiumDriver driver) throws Exception {
		try {
			log.info("In ND.navigateToPrimary Navigating to Primary folder");
			/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BackButton"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
			}
			 */waitForHambergerMenuToBePresent();
			 openMenuDrawer(driver);
			 /*Thread.sleep(7000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SentOption"))==0) {
				scrollDown(1);
			}
			log.info("Tapping on Sent folder under Menu drawer");
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SentOption"));*/
			 CommonFunctions.scrollsToTitle(driver, "Primary");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to switch to Sent Box Due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to sent box
	 * 
	 * @throws Exception
	 */
	public void navigateToSentBox1(AppiumDriver driver) throws Exception {
		try {

			if (CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("SentOption"))) {
				scrollDown(1);
			}
			log.info("Tapping on Sent folder under Menu drawer");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SentOption"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to switch to Sent Box Due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate to All Emails
	 * 
	 * @throws Exception
	 */
	public void navigateToAllEmails(AppiumDriver driver) throws Exception {
		try {

			//waitForHambergerMenuToBePresent();

			openMenuDrawer(driver);
			//scrollDown(1);
			//  CommonFunctions.scrollTo(driver, "All mail");
			//Copied from Bindhu code on 30/01/2018: starts
			/* 
			if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AllEmailsOption"))){
				scrollDown(1);
			}
			*/
			//Copied from Bindhu code on 30/01/2018: end			
			CommonFunctions.scrollsToTitle(driver, "All mail");
			Thread.sleep(600);
			log.info("From menu drawer, selecting  'All mail' section");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AllEmailsOption"));

			commonPage.waitForElementToBePresentByXpath(commonElements.get("AllEmailsOption"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to switch to Sent Box Due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify whether promotions inbox is rendered
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean isPromotionsInboxDisplayed(AppiumDriver driver) throws Exception {

		boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption"));
		return value;
	}

	/**Method to navigate promotions options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void navigateToPromotions(AppiumDriver driver)throws Exception{
		try{
			if(isPromotionsInboxDisplayed(driver)){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PromotionsInboxOption"));
			}else{
				scrollDown(1);
				if(isPrimaryInboxDisplayed(driver)){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PromotionsInboxOption"));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to click on the promotion tab : "+e.getLocalizedMessage());
		}
	}

	/**Method to navigate social folder
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void navigateToSocial(AppiumDriver driver)throws Exception{
		try{
			openMenuDrawer(driver);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SocialInboxOption"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SocialInboxOption"));
			}else{
				/*scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SocialInboxOption"));*/
				CommonFunctions.scrollsToTitle(driver, "Social");
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to click on the Social tab : "+e.getLocalizedMessage());
		}
	}


	/**
	 * Method to navigate to Label
	 * 
	 * @throws Exception
	 */
	public void navigateToLabel(AppiumDriver driver, String LabelXpath) throws Exception {
		try {
//Copied from Bindhu code on 30/01/2018: starts
//			waitForHambergerMenuToBePresent();
//			openMenuDrawer(driver);
			log.info("From menu drawer, visit selected label");
			if(CommonFunctions.getSizeOfElements(driver, LabelXpath)==0){
				scrollDown(1);	
			}
			//Copied from Bindhu code on 30/01/2018: end
			CommonFunctions.searchAndClickByXpath(driver, LabelXpath);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to Navigate to label Due to : " + e.getLocalizedMessage());
		}
	}


	/**
	 * Method to move opened mail to label 
	 * 
	 * @return
	 * @throws Exception
	 */
	public void moveToLabel(AppiumDriver driver, String FolderName) throws Exception {
		try{
			Thread.sleep(2000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MoveToOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabel").replace("Label", FolderName));
		}catch(Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to move to label Due to : " + e.getLocalizedMessage());
		}
	}




	/**Method to verify the system label
	 * 
	 * @param driver
	 * @param SystemLabel
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToSystemFolders(AppiumDriver driver, String SystemLabel)throws Exception{
		try{
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AllIndoxesOption"))!=0){
				log.info("Account Drawer is displayed");
			}else{

				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				log.info("Switched to Account drawer");
			}
			//Verifying scrolling is happening 
			CommonFunctions.scrollToCellByTitleVerify(driver, "Settings");
			CommonFunctions.scrollToCellByTitleVerify(driver, "Primary");
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				log.info("Navigated to " +SystemLabel+ " label folder");
			}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel))!=0){
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				log.info("Navigated to " +SystemLabel+ " label folder");
				// throw new Exception("Unable to navigate to " +SystemLabel+ " folder");
			}
			else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel))!=0){
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				log.info("Navigated to " +SystemLabel+ " label folder");
				// throw new Exception("Unable to navigate to " +SystemLabel+ " folder");
			}
			else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel))!=0){
				scrollDown(2);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SystemLabelXpath").replace("Label", SystemLabel));
				log.info("Navigated to " +SystemLabel+ " label folder");
				// throw new Exception("Unable to navigate to " +SystemLabel+ " folder");
			}
			openMenuDrawer(driver);
		}catch(Exception e){
			throw new Exception("Unable to navigate to selected system folder "+e.getStackTrace());
		}
	}


	/**Method to verify the system label
	 * 
	 * @param driver
	 * @param SystemLabel
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToSystemFoldersFromSettings(AppiumDriver driver, String SystemLabel)throws Exception{
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));

			CommonFunctions.scrollsToTitle(driver, SystemLabel);
		}catch(Exception e){
			throw new Exception("Unable to navigate to selected system folder "+e.getStackTrace());
		}
	}


	/**Method to navigate Trash folder
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToTrashFolder(AppiumDriver driver)throws Exception{
		try{
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("NavigateUp"))!=0){
				driver.navigate().back();
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption"))!=0){
				log.info("Already exists in trash folder");
			}else{
				openMenuDrawer(driver);
				/* log.info("Looking for the Trash option to display");
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption"))!=0){
			 	 log.info("Trash option is displayed and clicking on it to navigate to trash folder");
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BinOption"));
			 }else{
				 log.info("Scrolling down for the Trash option to be display");
				 scrollDown(1);
				 Thread.sleep(1500);
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption"))!=0){
					 Thread.sleep(900);
					 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BinOption"));
				     log.info("Trash folder option is displayed after scrolling and clicked on it");
			 }
			}*/
				CommonFunctions.scrollsToTitle(driver, "Trash");
				Thread.sleep(1300);
			} }catch(Exception e){
				throw new Exception("Unable to navigate to trash folder "+e.getLocalizedMessage());
			}
	}

	/**Method to navigate spam folder
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToSpamFolder(AppiumDriver driver)throws Exception{
		try{
			//openMenuDrawer(driver);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("NavigateUp"))!=0){
				driver.navigate().back();
			}

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			log.info("Looking for the Spam option to display");
			/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SpamOption"))!=0){
			 	 log.info("Spam option is displayed and clicking on it to navigate to Spam folder");
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SpamOption"));
			 }else{
				 log.info("Scrolling down for the Spam option to be display");
				 scrollDown(1);
				 Thread.sleep(800);
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SpamOption"))!=0){
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SpamOption"));
				 log.info("Spam folder option is displayed after scrolling and clicked on it");
			 }
			}*/
			CommonFunctions.scrollsToTitle(driver, "Spam");
		}catch(Exception e){
			throw new Exception("Unable to navigate to Spam folder "+e.getLocalizedMessage());
		}
	}

	/**Method to navigate to Apps menu on phone
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToApps(AppiumDriver driver) throws Exception{
		try{
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppsMenu"));
			Thread.sleep(3000);
		}catch(NoSuchElementException e){
			throw new Exception("Unable to find Apps folder on screen "+e.getMessage());
		}
	}

	public void navigateToComposeShortCut(AppiumDriver driver)throws Exception{
		try{	
			log.info("Tapping on gmail app longpress");
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement gmail = driver.findElement(By.xpath(commonElements.get("Gmail")));
			TouchAction t = new TouchAction(driver);
			t.longPress(gmail).perform();
		}catch(Exception e){
			throw new Exception("Unable to find compose short cut "+e.getLocalizedMessage());
		}
	}

	public void createShortCutCompose(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			log.info("Creating shortcut");
			WebElement composesc = driver.findElement(By.xpath("//android.widget.TextView[@text='Compose']"));
			TouchAction t = new TouchAction(driver);
			t.longPress(composesc).waitAction(6000).perform();
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			log.info("Shortcut created");
		}catch(Exception e){
			throw new Exception("Unable to create short cut for compose "+e.getMessage());
		}
	}

	public void selectMultiWindow(AppiumDriver driver)throws Exception{
		try{
			TouchAction a2 = new TouchAction(driver);
			log.info("Device Selected "+deviceName +" driver.getOrientation():"+driver.getOrientation());
			if(deviceName.equalsIgnoreCase("Nexus 5X") || deviceName.equalsIgnoreCase("Nexus 6P")){
				a2.tap (117, 282).perform();
			}
		}catch(Exception e){
			throw new Exception("Coordinates are not in within display screen "+e.getMessage());
		}
	}

	public void selectGmailWindowCompose(AppiumDriver driver)throws Exception{
		try{
			TouchAction a2 = new TouchAction(driver);
			a2.tap (403, 117).perform();
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			Thread.sleep(2000);
			hideKeyboard();
		}catch(Exception e){
			throw new Exception("Coordinates are not in within display screen "+e.getMessage());
		}
	}

	public void selectDownloadsApp(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			TouchAction a2 = new TouchAction(driver);
			a2.tap (189, 978).perform();

		}catch(Exception e){
			throw new Exception("Coordinates are not in within display screen "+e.getMessage());
		}
	}

	/**
	 * Method to long  press on Gmail app
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyElementsOnLongPressOfGmail(AppiumDriver driver,  String compose)throws Exception{
		try{	
			log.info("In verify elements on log press of gmail");
			ArrayList<String> al1= new ArrayList<String>();
			List<WebElement> composesc = driver.findElements(By.className("android.widget.TextView"));
			log.info(composesc.size());
			Iterator<WebElement> iters = composesc.iterator();
			while (iters.hasNext()){
				WebElement row = iters.next();
				log.info(row.getText());
				String value= row.getText();
				al1.add(value);
				log.info(al1);
			}
			if(al1.containsAll(NavigationDrawer.gettingListFromMenuDrawer(driver, compose))){

				log.info("Both lists are matching");
			}		else{
				log.info("Not matching");

			}
		}catch(Exception e){
			throw new Exception("Unable to find "+e.getLocalizedMessage());
		}
	}

	/**
	 * Method to Create Shortcut Compose
	 * @author batchi
	 * @throws Exception
	 */
	public void createShortCutComposeMail(AppiumDriver driver)throws Exception{
		try{
			log.info("In create  shortcut for compose");
			Thread.sleep(5500);
			List<WebElement> composesc = driver.findElements(By.className("android.widget.TextView"));
			log.info(composesc.size());
			Iterator<WebElement> iters = composesc.iterator();
			while (iters.hasNext()){
				WebElement row = iters.next();

				String s= row.getText();
				log.info(row.getText());
				TouchAction t = new TouchAction(driver);
				t.longPress(row).waitAction(6000).perform();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				if(s.contains(commonFunction.searchAndGetTextOnElementByXpath(driver, commonElements.get("ShortCutIconOnHomeScreen").replace("Name", s)))){
					log.info("Verified shortcut is created");	
				}
				navigateToComposeShortCut(driver);
			} 	
		}catch(Exception e){
			throw new Exception("Unable to create short cut for compose "+e.getMessage());
		}
	}

	/**
	 * Method to verify After Data Is Cleared
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyAfterDataIsCleared(AppiumDriver driver)throws Exception{
		try{
			log.info("In verify after data is cleared");
			//navigateToComposeShortCut(driver);
			Process  p;
			List<WebElement> composesc = driver.findElements(By.className("android.widget.TextView"));
			log.info(composesc.size());

			Thread.sleep(2000);
			String[] homeButton = new String[]{"adb","shell", "input", "keyevent","3"};
			p =new ProcessBuilder(homeButton).start();
			String[] clearGmailCache = new String[]{"adb","shell","pm","clear","com.google.android.gm" };
			p = new ProcessBuilder(clearGmailCache).start();
			log.info("Cleared the Gmail Cache");
			Thread.sleep(2000);
			navigateToComposeShortCut(driver);
			List<WebElement> composesc1 = driver.findElements(By.className("android.widget.TextView"));
			log.info(composesc1.size());
			if(composesc.size()!=composesc1.size()){

				log.info("dynamic elements are not available ");
			}		else{
				log.info("Dynamic elements are available");

			}
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to find "+e.getLocalizedMessage());
		}
	}

	/**
	 * Method to get all accounts list
	 * @author batchi
	 *
	 * @throws Exception
	 */
	public static ArrayList gettingListFromMenuDrawer(AppiumDriver driver,String compose)throws Exception{
		// inbox.verifyGmailAppInitState(driver);
		inbox.launchGmailApplication(driver);
		openMenuDrawer(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		List<WebElement> list1= driver.findElements(By.xpath(commonElements.get("AccountListCommonXpath")));
		//String value = ((WebElement) list1).getText();
		ArrayList<String> al = new ArrayList<String>();
		for(WebElement e: list1){
			al.add(e.getText());
		}
		al.add(compose);
		log.info(al);
		driver.closeApp();
		return al;

	}

	/**
	 * Method to launch settings to remove  accounts
	 * @author batchi
	 * @throws Exception
	 */
	public void launchSettingsAndRemoveAccounts(AppiumDriver driver,  String accountType, String accountId, String searchApp,String searchFeature)throws Exception{
		try{
			log.info("In launch settings app to remove accounts");
			Thread.sleep(2000);
			Process p;
			String [] backButton = new String[]{"adb", "shell", "input", "keyevent", "4"};
			p = new ProcessBuilder(backButton).start();
			Thread.sleep(1000);
			commonFunction.navigateBack(driver);
			/*String[] launchSettingsApp = new String[]{"adb","shell","monkey","-p","com.android.settings","-c","com.android.settings.Settings"};
			 		 p = new ProcessBuilder(launchSettingsApp).start();
			 */ /*commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchApps"));
			 		  commonFunction.searchAndSendKeysByXpath(driver,commonElements.get("SearchApps") ,searchApp );
			 		  commonFunction.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));*/

			//navDrawer.navigateToApps(driver);
			//commonFunction.searchAndClickByXpath(driver, commonElements.get("AppMenu"));
			/*driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			 		   for(int i=1;i<=5;i++){
			 			   log.info("Verifying the settings option");
			 		     if(commonFunction.getSizeOfElements(driver, commonElements.get("SettingsOption"))==0){
			 		     	scrollDown(i);
			 		      }
			 		     }
			 */  Runtime.getRuntime().exec("adb shell am start -a android.settings.SETTINGS");
			 log.info("Launched Settings app");
			 /*	   commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchApps"));
			 		  commonFunction.searchAndSendKeysByXpath(driver,commonElements.get("SearchApps") ,searchApp );
			 		  commonFunction.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			  */ commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchInSettings"));
			  //commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchTextInSettings"));
			  commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("SearchTextInSettings"), searchFeature);
			  commonFunction.searchAndClickByXpath(driver, commonElements.get("AccountsInSettings"));
			  log.info("Accounts option  is found and clicked");	 
			  /*for(int i=1;i<=5;i++){
			 		    if(commonFunction.getSizeOfElements(driver, commonElements.get("AccountsInSettings"))==0){
			 		 		   scrollDown(i);
			 		 		   	if(commonFunction.getSizeOfElements(driver, commonElements.get("AccountsInSettings"))!=0){
			 		 		      	commonFunction.searchAndClickByXpath(driver, commonElements.get("AccountsInSettings")); 
			 		 		       	break;
			 		 		  	}
			 		 		  }
			 		 	 }*/ 
			  commonFunction.searchAndClickByXpath(driver, commonElements.get("OptionsInAccounts").replace("optionType", accountType));
			  commonFunction.searchAndClickByXpath(driver, commonElements.get("MailIdsInAccounts").replace("emailId", accountId));
			  log.info("clicked on Account ");
			  Thread.sleep(1000);
			  commonFunction.searchAndClickByXpath(driver, commonElements.get("InAppBrowserMoreOptions"));
			  commonFunction.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
			  commonFunction.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
			  String[] homeButton = new String[]{"adb", "shell", "input", "keyevent", "3"};
			  p = new ProcessBuilder(homeButton).start();
		}catch(Exception e){
			throw new Exception("Unable to launch Settings app to remove account "+e.getMessage());
		}
	}	
	/**
	 * Method to launch settings to remove  accounts
	 * @author batchi
	 * @throws Exception
	 */
	public void launchSettingsAndAddAccount(AppiumDriver driver,  String UId,String pwd,String searchApp,String searchFeature)throws Exception{
		try{
			log.info("In launch settings app to add accounts");
			Process p;
			/*  String [] backButton = new String[]{"adb", "shell", "input", "keyevent", "4"};
			 		 p = new ProcessBuilder(backButton).start();
			 		 Thread.sleep(10000);
			 		 commonFunction.navigateBack(driver);
			 		 String[] launchSettingsApp = new String[]{"adb","shell","monkey","-p","com.android.settings","-c","com.android.settings.Settings"};
			 	     p = new ProcessBuilder(launchSettingsApp).start();
			 	     log.info("Launched Settings app");
			 */  /*  driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			 	        for(int i=1;i<=5;i++){
			 	        if(commonFunction.getSizeOfElements(driver, commonElements.get("SettingsOption"))==0){
			 	        	scrollDown(i);
			 	         }
			 	        }*/
			Runtime.getRuntime().exec("adb shell am start -a android.settings.SETTINGS");
			// navDrawer.navigateToApps(driver);
			// commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchApps"));
			// commonFunction.searchAndSendKeysByXpath(driver,commonElements.get("SearchApps") ,searchApp );
			// commonFunction.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));

			commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchInSettings"));
			commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("SearchTextInSettings"), searchFeature);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("AccountsInSettings"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountGoogleOption"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountUsernameField"),
					UId);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountUsernameNextButton"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountPasswordField"),
					pwd);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountPasswordNextButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountAccpetButton"));
			Thread.sleep(3000);
			String [] homeButton = new String[]{"adb", "shell", "input", "keyevent", "3"};
			p = new ProcessBuilder(homeButton).start();

			Thread.sleep(3000);
		}catch(Exception e){
			throw new Exception("Unable to launch Settings app to remove account "+e.getMessage());
		}
	}

	/*
	* @author Venkat
	*/
	public void pressAndHoldForShortcut(AppiumDriver driver, String xPath)throws Exception{
		try{	
			if(xPath == null) {
				xPath = commonElements.get("Gmail");
			}
			log.info("Tapping on gmail app longpress:"+xPath);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement webElement = driver.findElement(By.xpath(xPath));
			TouchAction t = new TouchAction(driver);
			if(xPath.contains(".com")) {
				t.longPress(webElement).waitAction(2000).perform();
			}else {
				t.longPress(webElement).perform();
			}
		}catch(Exception e){
			throw new Exception("Unable to create short cut "+e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to verify Tasks app is installed or not
	 * @author batchi
	 * @throws Exception
	 */
	public void navigateTOTasksApp(AppiumDriver driver)throws Exception{
		try{
			log.info("Tapping on open navigation drawer");
			openMenuDrawer(driver);
			commonPage.scrollTo("Settings");
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("Tasks_LHP"))){
				CommonFunctions.searchAndClickByXpath(driver,commonElements.get("Tasks_LHP"));
				Thread.sleep(2000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PlayStoreInstallButton"))!=0){
					CommonFunctions.searchAndClickByXpath(driver,commonElements.get("PlayStoreInstallButton") );
					Thread.sleep(2000);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("PlayStoreOpenButton"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PlayStoreOpenButton"));
					Thread.sleep(2000);
					if(CommonFunctions.getSizeOfElements(driver, commonElements.get("GetStarted"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GetStarted"));
						driver.navigate().back();
						Thread.sleep(2000);
					}else{
						driver.navigate().back();
						Thread.sleep(2000);
					}
				}else{
				 	driver.navigate().back();
				 	
				}
			}else{
				log.info("Task  option is not available in DF now");
			}
			
			
		}catch(Exception e){
			throw new Exception("Unable to launch Settings app to remove account "+e.getMessage());
		}
	}
	
	/**
	 * Method to navigate to Scheduled label
	 * 
	 * @throws Exception
	 */
	public void navigateToScheduled(AppiumDriver driver) throws Exception {
		try {
			log.info("In Navigating to Scheduled folder");
			waitForHambergerMenuToBePresent();
			 openMenuDrawer(driver);
			 CommonFunctions.scrollsToTitle(driver, "Scheduled");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Failed to switch to Sent Box Due to : " + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to verify recent label
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyRecentLabel(AppiumDriver driver)throws Exception{
		try{
			Map < String, String > recentLabelData = commonPage.getTestData("RecentLabels");
			userAccount.switchMailAccount(driver, recentLabelData.get("account1"));

			log.info("Tapping on open navigation drawer");
			openMenuDrawer(driver);
			scrollTo("mtv1");
			commonFunction.searchAndClickByXpath(driver,recentLabelData.get("mtv1Label"));
			waitForElementToBePresentByXpath(recentLabelData.get("mtv1LabelinTLView"));
			if(commonFunction.isElementByXpathDisplayed(driver, recentLabelData.get("mtv1LabelinTLView"))){
				log.info("########### Navigated to TL view of MTV1 label #####################");
				driver.navigate().back();
				if(commonFunction.getSizeOfElements(driver, recentLabelData.get("primaryLabelinTLView"))!=0){
					log.info("Primary label is displayed");
				}else{
					CommonFunctions.isElementByXpathDisplayed(driver, recentLabelData.get("inboxLabelinTLView"));
					log.info("Inbox label is displayed");
				}
			}
			openMenuDrawer(driver);
			Thread.sleep(2000);
			if(commonFunction.isElementByXpathDisplayed(driver, recentLabelData.get("RecentLabels"))){
				log.info("############ Recent label option is available ###############");
				if(commonFunction.isElementByXpathDisplayed(driver,recentLabelData.get("mtv1Label"))){
					log.info("@@@@@@@@@@ MTV1 label is displayed under recent labels  @@@@@@@@@@@ ");
					commonFunction.searchAndClickByXpath(driver,recentLabelData.get("mtv1Label"));
					commonFunction.isElementByXpathDisplayed(driver, recentLabelData.get("mtv1LabelinTLView"));
				}
			}
			
		}catch(Exception e){
			throw new Exception("Unable to identify recent label option "+e.getMessage());
		}
	}
	
	/**
	 * @TestCase exploratory : verify Account switcher on boarding message
	 * @Pre-condition :
	 * GMAT 
	 * @throws: Exception
	 * @author batchi
	 */
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void onboardingMsg(AppiumDriver driver) throws Exception {
		log.info("Navigated to Welcome page");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NewGmailLaunchGotItMessage"));
		waitForElementToBePresentById(commonElements.get("AccountListSelectedIMAPAccountID"));
		Thread.sleep(2000);
		for(int i =0;i<=5;i++){
        List<WebElement> account=	driver.findElementsByXPath(commonElements.get("AccountListSelectedAccount"));
			int accListCount = account.size();
				if(accListCount >= 2){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NewGmailTakeMeToGmail"));
		}else{
			Thread.sleep(5000);
			
		}}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextButton"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		Thread.sleep(2000);
		driver.launchApp();
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ConfidentialAlert"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ConfidentialAlert"));
		}
		log.info("Pulling down to refresh the TL screen");
		commonPage.pullToReferesh(driver);
		Thread.sleep(2000);
		driver.launchApp();
		waitForElementToBePresentByXpath(commonElements.get("searchIDXapth"));
		
//		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		TouchAction forward = new TouchAction((MobileDriver)driver);
		forward.longPress(03,600).moveTo(900,600).release().perform();
		Thread.sleep(10000);
		waitForElementToBePresentByXpath(commonElements.get("AllIndoxesOption"));
		Thread.sleep(2000);
		TouchAction backward = new TouchAction((MobileDriver)driver);
		backward.longPress(900,600).moveTo(03,600).release().perform();
		 Thread.sleep(5000); 
		 
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OnboardingMessage"))!=0){
			log.info("********** OnBoarding message is displayed ***********");
		}  
		else{
			throw new Exception("!!!!!!!!!!! No onboarding  message is displayed !!!!!!!!!!!");
		}
	}

}
