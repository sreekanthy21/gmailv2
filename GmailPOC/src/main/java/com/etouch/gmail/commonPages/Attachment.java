package com.etouch.gmail.commonPages;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.testng.Assert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

public class Attachment extends CommonPage {

	/** The log. */
	private static Log log = LogUtil.getLog(Attachment.class);

	/**
	 * Instantiates an InboxFunctions.
	 */
	public Attachment() {

	}

	/**
	 * Method to add attach file from drive
	 * 
	 * @param AttachmentName
	 * @throws Exception
	 */
	public void addAttachmentFromDrive(AppiumDriver driver, String AttachmentName) throws Exception {
		String AttachmentXpath = null;
		try {
			
			log.info("adding attachement");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAttachmentIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InsertFromDrive"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverRefreshOption"));
			AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
			log.info(AttachmentXpath);
			Thread.sleep(2000);
			boolean isFileFound = false;
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DriveFile").replace("FileName", AttachmentName))!=0){
				CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
				isFileFound = true;
			}else{
				//for(int i=1; i<=3; i++){ // Commented by Venkat on 26/12/2018
				for(int i=1; i<=4; i++){ // added by Venkat on 26/12/2018
					scrollDown(i);
					Thread.sleep(1000);
					if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DriveFile").replace("FileName", AttachmentName))!=0){
						CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
						isFileFound = true;
						break;
					}
				}
			}
			//written by Venkat on 18/12/2018: start
			if(!isFileFound){
				for(int i=1; i<=4; i++){ 
					scrollUp(1);
					Thread.sleep(1000);
					if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DriveFile").replace("FileName", AttachmentName))!=0){
						CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
						isFileFound = true;
						break;
					}
				}
			}
			//written by Venkat on 18/12/2018: end
			/*if(AttachmentXpath==null){
				scrollDown(1);
			}*/
			CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSelect"));
			verifyAttachmentIsAddedSuccessfully(driver, AttachmentName);
		
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);
		}
	}

	/**
	 * Method to verify attchment is added successfully
	 * 
	 * @param AttachmentName
	 * @throws Exception
	 */
	public void verifyAttachmentIsAddedSuccessfully(AppiumDriver driver, String AttachmentName) throws Exception {
		String AttachmentXpath = null;
		try {
			log.info("Verifying the attachments");
			AttachmentXpath = commonElements.get("AttachedImage").replace("FileName", AttachmentName);
			scrollDown(1);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, AttachmentXpath),
					"Attachment file NOT displayed");
			log.info("Attachment file displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to find attachment in Mail : Attachment Name :" + AttachmentName);
		}
	}

	/**
	 * Method to open and validate attachment
	 * 
	 * @param attachmentName
	 * @throws Exception
	 */
	public void openAndValidateAttachment(AppiumDriver driver, String attachmentName) throws Exception {
		try {
			log.info("Opening attachment and validating them");
			
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("AttachedImage").replace("FileName", attachmentName));

			try {
				Thread.sleep(3000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentJustOnce"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentJustOnce"));
					if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentDriverOption"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentDriverOption"));

					}
				}
				
				/*CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OpenAttachmentJustOnce"));
				if (driver.findElement(By.xpath(commonElements.get("OpenAttachmentDriverOption"))).isDisplayed()) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentDriverOption"));
				}*/
			} catch (Exception e) {
			}
			/*try {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentJustOnce"));
			} catch (Exception e) {
			}*/

			/*waitForElementToBePresentByXpath(commonElements.get("OpenAttachmentPreviewImageName"));
			Thread.sleep(4000);

			if (!CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("OpenAttachmentPreviewImageName"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentImagePreview"));
				log.info("Successfully verified attachment");
			}
*/
		/*	Thread.sleep(4000);
			Assert.assertEquals(
					driver.findElementByXPath(commonElements.get("OpenAttachmentPreviewImageName")).getText(),
					attachmentName, "Open Attachment Preview Image Name NOT matched");
		*/	navigateBack(driver);
			log.info("Attachment validation completed");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to validate attachmend due to : " + e.getLocalizedMessage());
		}
	}

	public void openAndValidateAttachment1(AppiumDriver driver, String expectedAttachmentName) throws Exception {
		try {
			log.info("Opening attachment and validating them");
			
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("AttachedImage").replace("FileName", expectedAttachmentName));
			Thread.sleep(5000);
			String actualAttachmentName = null;
			try {
				if (CommonFunctions.getSizeOfElements(driver,
						commonElements.get("OpenAttachmentPreviewImageName"))!=0) {
					actualAttachmentName = driver
							.findElementByXPath(commonElements.get("OpenAttachmentPreviewImageName")).getText();
				}else{
					CommonFunctions.tapUsingCoordinates(driver, 528, 991);
					CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OpenAttachmentPreviewImageName"));
					actualAttachmentName = driver
							.findElementByXPath(commonElements.get("OpenAttachmentPreviewImageName")).getText();
				}
				
				
				/* else {
					actualAttachmentName = driver.findElementById(commonElements.get("AttachmentPreviewHeaderID"))
							.getText();
				}*/
			} catch (Exception e) {

			}
			/*
			 * if
			 * (!CommonFunctions.isElementByXpathDisplayed(driver,commonElements
			 * .get("OpenAttachmentPreviewImageName"))) {
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("OpenAttachmentImagePreview")); log.info(
			 * "Successfully verified attachment"); } else {
			 * CommonFunctions.searchAndClickById(driver,
			 * "com.android.chrome:id/title_bar"); log.info(
			 * "Successfully verified attachment"); }
			 */

			Assert.assertTrue(actualAttachmentName.contains(expectedAttachmentName),
					"Open Attachment Preview Image Name NOT matched Expected: " + expectedAttachmentName
							+ " ActualAttachment: " + actualAttachmentName);
			navigateBack(driver);
			log.info("Attachment validation completed");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to validate attachmend due to : " + e.getLocalizedMessage());
		}
	}
	
	///**Method to pass the google drive filename
	
	public void attachGoogleDriveFile()throws Exception{
		try{
			
		}catch(Throwable e){
			e.printStackTrace();
			throw new Exception("Unable to add the file name : "+e.getLocalizedMessage());
		}
	}
	
	
	
	/** Method to add file to the drive
	 * 
	 * 
	 * @throws Exception
	 */
	public void addFileToDrive(AppiumDriver driver, String fileName, String account)throws Exception{
		try{
			Thread.sleep(5000);
			log.info("Checking which account exists");
			//userAccount.switchMailAccount(driver, account);
			if(deviceName.equalsIgnoreCase("Nexus5x")){
				log.info("tapping on menu drawer");
				driver.tap(1, 21, 63, 1);
				Thread.sleep(2000);
				userAccount.switchMailAccountDrive(driver, account);
			}else if(deviceName.equalsIgnoreCase("")){
				
			}
			
			
			
			log.info("Clicking on the add file button on GDrive");
			Thread.sleep(5000);
			try{
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("AddFile"));
			log.info("Checking the device name and doing tap actions");
			log.info(deviceName);
			if(deviceName.equalsIgnoreCase("Nexus5x")){
			driver.tap(1, 891, 1605, 1);
			}else if(deviceName.equalsIgnoreCase("PixelXL")){
				driver.tap(1, 891, 1605, 1);	
			}
			//driver.findElement(By.xpath("//android.widget.ImageButton[contains(@content-desc,'Create')]")).click();
				//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddFile"));
			}catch(Exception e){
				e.printStackTrace();
				log.info("Tapping using coordinates");
				driver.tap(1, 891, 1605, 1);
			}
			//workaround as click is not working and throwing a unknown server side error
			Thread.sleep(2500);
			try{
			log.info("Clicking on google sheets");
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GoogleSheet"));
			//log.info("Google Sheet option is available in the popup layer and opened");
			if(deviceName.equalsIgnoreCase("Nexus5x")){
				log.info("tapping on google sheet");
				driver.tap(1, 436, 1667, 1);
			}else if(deviceName.equalsIgnoreCase("")){
				driver.tap(1, 891, 1605, 1);
			}
			
			}catch(Exception e){
				driver.tap(1, 477, 1520, 1);
				log.info("Google Sheet option is available in the popup layer and opened using coordinates");
			}
			log.info("Verifying more options is displayed");
//			commonPage.waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
//			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			Thread.sleep(3000);
			if(deviceName.equalsIgnoreCase("Nexus5x")){
				log.info("Tapping on more options");
				driver.tap(1, 954, 73, 1);
			}else if(deviceName.equalsIgnoreCase("")){
				
			}
			log.info("Click on the more options to rename the sheet");
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("TitleRename"));
			log.info("Clicked on the sheet default name to edit");
			
		/*	fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
			log.info(fileName);*/
			
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("EditTitle"), fileName);
			log.info("Edited the name of the sheet as : "+fileName);
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OkButton"));
			navigateBack(driver);
			
		}catch(Exception e){
			e.printStackTrace();
			//throw new Exception("Failed to add the file to drive : "+ e.getLocalizedMessage());
		}
	}
	
	
	

	/**
	 * Method to open and validate EML file attachment
	 * 
	 * @param attachmentName
	 * @throws Exception
	 */
	public void openAndValidateEMLFileAttachment(AppiumDriver driver, String attachmentName) throws Exception {
		try {
			
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("AttachedImage").replace("FileName", attachmentName));

			try {
				Thread.sleep(1200);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentJustOnce"))!=0){
					//waitForElementToBePresentByXpath(commonElements.get("OpenAttachmentJustOnce"));

					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OpenAttachmentDriverOption")))
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentDriverOption"));
				} }catch (Exception e) {
				}
			/*try {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentJustOnce"));
			} catch (Exception e) {
			}*/

			waitForElementToBePresentByXpath(commonElements.get("OpenEMLMessageViewerPreview"));
			Thread.sleep(3000);
			waitForElementToBePresentByXpath(commonElements.get("EMLMessageSubjectLine"));

			Assert.assertEquals(driver.findElementByXPath(commonElements.get("EMLMessageSubjectLine")).getText(),
					attachmentName, "EML Message Subject Line text NOT matched");
			//scrollDown(2);
			navigateBack(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to validate attached image due to : " + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to add attach file from drive
	 * @author batchi
	 * @param AttachmentName
	 * @throws Exception
	 */
	public void addAttachmentFromDriveMultiple(AppiumDriver driver, String AttachmentName) throws Exception {
		String AttachmentXpath = null;
		try {
			
			log.info("adding attachement");
			Thread.sleep(4000);
			log.info("Clicking on attach icon");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAttachmentIcon"));
			log.info("Clicking on insert from drive");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InsertFromDrive"));
			log.info("Clicking on drive more options");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			log.info("Clicking on refresh icon");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverRefreshOption"));
			AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)!=0){
				log.info("Attachment Found   "+ AttachmentName);
			}else{
			log.info("No attachement found  " +AttachmentName);
			Thread.sleep(2500);
			//commonFunction.scrollTo(driver,AttachmentName);
			log.info("Trying to scroll********************************************");
			commonFunction.scrollDown(driver,2);
			
			 }
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSelect"));
			log.info("Added Attachment ");
		Thread.sleep(2000);
			//verifyAttachmentIsAddedSuccessfully(driver, AttachmentName);
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);
		}
	}
	
	/**
	 * Method to add attach file from drive
	 * @author batchi
	 * @param AttachmentName
	 * @throws Exception
	 */
	public void addAttachmentFromDriveMultipleGmailGo(AppiumDriver driver, String AttachmentName) throws Exception {
		String AttachmentXpath = null;
		try {
			
			log.info("adding attachement");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAttachmentIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileGo"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get(""));
			Thread.sleep(1500);
			driver.findElement(By.xpath("//android.widget.TextView[@text='gm.gig1.auto@gmail.com'] | //android.widget.TextView[@text='gm.gig1.testing@gmail.com']")).click();
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverRefreshOption"));
			*/
			AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)!=0){
				log.info("Attachment Found   "+ AttachmentName);
			}else{
			log.info("No attachement found  " +AttachmentName);
			Thread.sleep(2500);
			//commonFunction.scrollTo(driver,AttachmentName);
			log.info("Trying to scroll********************************************");
			commonFunction.scrollDown(driver,1);
			
			 }
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSelect"));
			log.info("Added Attachment ");
		Thread.sleep(2000);
			//verifyAttachmentIsAddedSuccessfully(driver, AttachmentName);
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);
		}
	}
	
	/**
	 * Method to open an attachment 
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void openingAttachments(AppiumDriver driver, String attachmentName) throws Exception {
		try {
			Thread.sleep(3000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("AttachedImage").replace("FileName",attachmentName));
			log.info("Clicked on the attachment "+attachmentName);
			
			/*if(commonFunction.getSizeOfElements(driver,commonElements.get("OpenAttachmentDriverOption") )!=0){
			commonFunction.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentDriverOption"));
			commonFunction.searchAndClickByXpath(driver,commonElements.get("OpenAttachmentJustOnce"));
			} else if(commonFunction.getSizeOfElements(driver,commonElements.get("OpenAttachmentSheetsOption"))!=0){
	    		commonFunction.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentSheetsOption"));
	    		commonFunction.searchAndClickByXpath(driver,commonElements.get("OpenAttachmentJustOnce"));
	    	}*/
			/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentOpenWithText"))!=0){
				
			}*/
		}catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to open attached image in Google View due to : " + e.getLocalizedMessage());
		}
	}
	/**
	 * Method to verify OpenAttachment  In Pico Projector View
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void verifyOpenAttachmentInPicoView(AppiumDriver driver, String attachmentName) throws Exception {
	log.info("in Verify attachment in pico projector viewer or not ");
	Thread.sleep(3000);
	commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
	if(commonFunction.getSizeOfElements(driver, commonElements.get("PicoViewMoreIconDialog"))!=0){
		log.info("Document has opened in  Pico view "+attachmentName);
		}
	else {
		log.info("Document has opened in  Google "+attachmentName+" view");
		}
	 commonPage.navigateBack(driver);
	 commonFunction.searchAndClickByXpath(driver, commonElements.get("DocsViewCloseIcon"));
	}

	/**
	 * Method to verify OpenAttachment  In Respective apps
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void verifyOpenAttachmentInRespectiveApps(AppiumDriver driver, String attachmentName,String docType, String docOptionXpath) throws Exception {
	log.info("in Verify attachment in respective app or not ");
	Thread.sleep(4000);
	//commonFunction.searchAndClickByXpath(driver, docOptionXpath);
	if(commonFunction.getSizeOfElements(driver, commonElements.get("AddPeopleInDriveViewer"))==0){
		log.info("Document has opened in  Pico Projector Viewer "+attachmentName);
		
		}
	else {
		log.info("Document has opened in "+docType+ " App "+attachmentName);
		}
	// commonPage.navigateBack(driver);
	 commonFunction.searchAndClickByXpath(driver, commonElements.get("DocsViewCloseIcon"));
	}
	
	/**
	 * Method to verify attachments in uneditable mode  In Pico Projector View
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void verifyAttachmentInUneditableMode(AppiumDriver driver,String attachmentName) throws Exception {
	log.info("in Verify attachment is editable or not ");
	openingAttachments(driver, attachmentName);
	Thread.sleep(2000);
	if(commonFunction.isElementByXpathDisplayed(driver, commonElements.get("PicoViewerEditIcon"))==true){
		log.info("****************** Edit icon available ************************");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("PicoViewerEditIcon"));
		if(commonFunction.isElementByXpathDisplayed(driver, commonElements.get("PicoViewerInstallAppDialog"))==true){
			log.info("$$$$$$$$$$$$$$$$ Document "+attachmentName+" is uneditable $$$$$$$$$$$$$$$$$$$$$$$$");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("InstallAppDialogDismiss"));
		}else{
			
			throw new Exception("Document "+attachmentName+" is editable");
		}
		}
	else {
		throw new Exception("Edit icon is not available");
		}//commonPage.navigateBack(driver);
	 commonFunction.searchAndClickByXpath(driver, commonElements.get("DocsViewCloseIcon"));
	}
	
	/**
	 * Method to verify if add-to-drive option is working for attachments In Pico Projector View
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void verifyAddToDriveOption(AppiumDriver driver,String attachmentName) throws Exception {
	log.info("in verify Add-To-Drive Option ");
	openingAttachments(driver,attachmentName);
	Thread.sleep(3000);
	commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
	log.info("More options selected");
	Thread.sleep(2000);
	if(commonFunction.getSizeOfElements(driver, commonElements.get("PicoViewMoreIconDialog"))!=0){
		log.info("Document has opened in  Pico view "+attachmentName);
		if(commonFunction.isElementByXpathDisplayed(driver, commonElements.get("PicoViewerMoreIconOptionAddToDrive"))==false){
			commonFunction.scrollDown(driver,1);
			} 
		    commonFunction.searchAndClickByXpath(driver,commonElements.get("PicoViewerMoreIconOptionAddToDrive"));
			if(commonFunction.getSizeOfElements(driver, commonElements.get("MyDriveWindowButtonPanel"))!=0){
				log.info("Add-To-Drive option is working");
				
			}else{
				throw new Exception("Add-To-Drive option is not working");
				}
			commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
		}
	else {
		throw new Exception("Document "+attachmentName+" has Add-To-Drive option");
		}//commonPage.navigateBack(driver);
	commonFunction.searchAndClickByXpath(driver, commonElements.get("DocsViewCloseIcon"));
	}
	
	/**
	 * Method to verify if the attachment opened is scrollable In Pico Projector View
	 * @author batchi
	 * @param attachmentName
	 * @throws Exception
	 */
	public void verifyDocumentIsScrollable(AppiumDriver driver,String attachmentName) throws Exception {
		try{
	log.info("in verify doucument is scrollable or not function ");
	openingAttachments(driver, attachmentName);
	Thread.sleep(5000);
	/*int d= driver.findElements(By.xpath(commonElements.get("PicoViewerDocumentContentPortion"))).size();
	log.info(d);*/
	
	/*AndroidElement element= (AndroidElement) driver.findElement(By.xpath(commonElements.get("DocumentWithMultipleSlides")));
	doubleTapElement(driver, element);
	if(element.getAttribute("index").equals("2")){ 
		scrollDown(1);
	}else{
		
		log.info("No elements  to scroll down");
	}*/
	commonFunction.scrollDown(driver,1);
	}catch (Throwable e) {
		e.printStackTrace();
		throw new Exception("Failed to scroll due to : " + e.getLocalizedMessage());
	}
	}
	
	public void addAttachement(AppiumDriver driver, String AttachmentName)throws Exception{
		String AttachmentXpath = null;

		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAttachmentIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)!=0){
				log.info("Attachment Found   "+ AttachmentName);
			}else{
			log.info("No attachement found  " +AttachmentName);
			Thread.sleep(2500);
			//commonFunction.scrollTo(driver,AttachmentName);
			log.info("Trying to scroll********************************************");
			commonFunction.scrollDown(driver,2);
			//scrollTo(AttachmentName);
			 }
			Thread.sleep(1500);
			CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);

		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);

		}
	}
	
	public void addAttachementFromDriveAccount(AppiumDriver driver, String AttachmentName, String AccountDrive)throws Exception{
		String AttachmentXpath = null;

		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAttachmentIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			Thread.sleep(1200);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ShowDriveRoots"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
				Thread.sleep(800);
				//CommonFunctions.scrollsToTitle(driver, AccountDrive); // commented by Venkat on 08/01/2019
				// added by Venkat on 08/01/2019 :start
				try{
					CommonFunctions.scrollsToTitle(driver, AccountDrive);
				}catch(Exception e){
					CommonFunctions.scrollUp(driver, 1); // added by Venkat on 08/01/2019
					CommonFunctions.scrollsToTitle(driver, AccountDrive);
				}
				// added by Venkat on 08/01/2019 :end
				Thread.sleep(800);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentDriverOption"))!=0){
			CommonFunctions.scrollsToTitle(driver, AccountDrive);
			}
			Thread.sleep(1000);
			/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DriveTitle"))!=0){
				CommonFunctions.scrollsToTitle(driver, "My Drive");
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SelectAnAccount"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountGmGigAuto"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}*/
			AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)!=0){
				log.info("Attachment Found   "+ AttachmentName);
			}else{
			log.info("No attachement found  " +AttachmentName);
			Thread.sleep(2500);
			//commonFunction.scrollTo(driver,AttachmentName);
			log.info("Trying to scroll********************************************");
			//commonFunction.scrollDown(driver,2);
			CommonFunctions.scrollToCellByTitleVerify(driver, AttachmentName);
			}
			//CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath); // Commented by Venkat on 08/01/2019
			// added by Venkat on 08/01/2019 : start
			boolean isFileFound = false;
			Thread.sleep(1500);
			for(int i=1; i<=4; i++){
				Thread.sleep(1000);
				if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)!=0){
					CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
					isFileFound = true;
					break;
				}
				scrollDown(1);
			}

			if(!isFileFound){
				for(int i=1; i<=4; i++){ 
					scrollUp(1);
					Thread.sleep(1000);
					if(CommonFunctions.getSizeOfElements(driver,AttachmentXpath)!=0){
						CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
						isFileFound = true;
						break;
					}
				}
			}
			// added by Venkat on 08/01/2019 : end
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);

		}
	}
	public void addAttachementFromRoot(AppiumDriver driver, String AttachmentName)throws Exception{
		String AttachmentXpath = null;

		try{
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
				log.info("Show drive clicked");
				Thread.sleep(600);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
				log.info("etouchtestone drive cliced");
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
				System.out.println("Drive title is"+commonElements.get("DriveTitle"));
				Thread.sleep(2000);
				System.out.println("AttachmentsFolderInDrive is"+commonElements.get("AttachmentsFolderInDrive"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachmentsFolderInDrive"));
				AttachmentXpath = commonElements.get("DriveFile").replace("FileName", AttachmentName);
				CommonFunctions.searchAndClickByXpath(driver, AttachmentXpath);
				log.info("Attachment"+  AttachmentName +"is  clicked");
				if(CommonFunctions.getSizeOfElements(driver, AttachmentXpath)==0){
					log.info("Attachment Found   "+ AttachmentName);
					
				}else{
				log.info("No attachement found  " +AttachmentName);
				Thread.sleep(2500);
				}
		}catch(Exception e){
			e.printStackTrace();
			Assert.fail("Failed to add Attachment :" + AttachmentName + " Due to : " + e);

		}
	}
	/**
	 * Method to verify attachment is deleted successfully
	 * @author batchi
	 * @param AttachmentName
	 * @throws Exception
	 */
	public void verifyAttachmentIsDeletedSuccessfully(AppiumDriver driver, String AttachmentName) throws Exception {
		String AttachmentXpath = null;
		try {
			log.info("Verifying the attachments");
			AttachmentXpath = commonElements.get("AttachedImage").replace("FileName", AttachmentName);
			scrollDown(1);
			Assert.assertTrue(CommonFunctions.isElementByXpathNotDisplayed(driver, AttachmentXpath),
					"!!!!!!!!!!!!!! Attachment file "+AttachmentName+" is displayed !!!!!!!!!!!");
			log.info(" ########### Attachment file "+AttachmentName+" is not displayed ############");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to find attachment in Mail : Attachment Name :" + AttachmentName);
		}
	}
	
	}
