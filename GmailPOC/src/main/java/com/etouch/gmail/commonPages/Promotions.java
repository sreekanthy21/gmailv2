package com.etouch.gmail.commonPages;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.Assert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

public class Promotions extends InboxFunctions {

	/** The log. */
	private Log log = LogUtil.getLog(Promotions.class);

	/**
	 * Instantiates a new Promotion page.
	 */
	public Promotions() {

	}
	
	/**Method to verify the ad info icon functionality
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void adInfoIconFunctionality(AppiumDriver driver)throws Exception{
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InfoIcon"));
			log.info("Clicked on the info Icon");
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ControlAdsLikeThis"));
			log.info("Verified the control ads like this link is displayed");
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ControlAdsLikeThis"));
			log.info("Clicked on the control ads like this link");
			
			commonPage.waitForElementToBePresentByXpath(commonElements.get("WhyThisAd"));
			log.info("Control ads like this link navigated to browser");
			
			commonPage.waitForElementToBePresentByXpath(commonElements.get("BlockAd"));
			log.info("Block the advertiser link is displayed on the browser");
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to verify the ad Icon info functionality : "+e.getLocalizedMessage());
		}
	}
	
	
	
	/**Method for clicking on the ad info icon
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyAdInfoButton(AppiumDriver driver)throws Exception{
		try{
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AdText"))){
				adInfoIconFunctionality(driver);
			}else{
				pullToReferesh(driver);
				adInfoIconFunctionality(driver);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to find the Ad info : "+e.getLocalizedMessage());
		}
	}
	
	
	
	
	
	
	
	
	public void gotoSettings(){
		try
		{
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Settings']");
		
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='etouchqaone']");
		}catch(Throwable e){
			e.printStackTrace();
			
		}
		
	}

	/**
	 * Method to verify Ad header and badger icon on Promotion ad
	 * 
	 * @throws Exception
	 */
	public void validateAdHeaderAndBadgeIconOnPromotionsAd() throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PromotionsInboxOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderName"));
			Assert.assertTrue(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("AdInfoId")),
					"AD info NOT rendered");
			Assert.assertTrue(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("AdID")),
					"AD badge NOT rendered");
			Assert.assertTrue(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("AdInfoId")),
					"AD info NOT rendered");
			navigateBack(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to send and navigate back due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify promotion Ad is rendered
	 * 
	 * @throws Exception
	 */
	public void verifyPromotionsAdDisplayed(AppiumDriver driver) throws Exception {
		try {
			log.info("In Promotion.verifyPromotionsAdDisplayed Verifying for AD under promotion tab");
			//openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PromotionsInboxOption"));
           // syncInbox(driver);
			//pullToReferesh(driver);
			Thread.sleep(2000);
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SenderName"))) {
			} else {
				pullToReferesh(driver);
			}
			log.info("In Promotion.verifyPromotionsAdDisplayed Assertion");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SenderName")),
					"Ad not displayed under Promotion");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderName"));
			Thread.sleep(1500);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AdXpath")),
					" Ad badge NOT rendered  after clicking on Ad sender name");
			log.info("In Promotion.verifyPromotionsAdDisplayed AD verification completed");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Promotion Ad failure due to:" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad Forward Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void verifyAdForward(AppiumDriver driver,String user, String inboxCategory, String ToFieldData, String ComposeBodyData)
			throws Exception {
		log.info("******************* Start verify Ad Forward Functionality*******************");
		try {
			
			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get(inboxCategory));
			/*syncInbox(driver);
			Thread.sleep(2000);
			pullToReferesh(driver);
			*/Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InfoIcon")),
					" Failure Reason :AD InfoIcon NOT displayed  ");

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderName"));
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ForwardAd"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ToFieldXpath"), ToFieldData + " ");
			//CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), ComposeBodyData);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ForwardAd")),
					" Failure Reason :Forward icon NOT displayed after forwarding Ad email.  ");
			navigateBack(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(" verifyAdForward functionality failed for inbox category :: " + inboxCategory + "::::"
					+ e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad Delete Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void verifyAdDelete(AppiumDriver driver,String inboxCategory) throws Exception {
		log.info("******************* Start Ad  Delete Functionality *******************");
		try {
			
			pullToReferesh(driver);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InfoIcon")),
					"Failure Reason: AD InfoIcon NOT displayed  ");

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderName"));
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteAd"));
			Thread.sleep(1000);
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PromotionsInboxOption"));
			/**--Below code commented due to already existing bug when ad is deleted--**/
			/*	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Undo"));
			pullToReferesh(driver);

			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InfoIcon")),
					"AD InfoIcon NOT displayed after Undo refresh : ");
*/		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("verifyAdDelete functionality failed for inbox category :: " + inboxCategory + "::::"
					+ e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Ad StarAd Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public boolean verifyAd_StarAd(AppiumDriver driver, String inboxCategory, String methodName) throws Exception {
		boolean flag = false;
		log.info("******************* Start Ad  StarAd Functionality*******************");
		try {
			
			pullToReferesh(driver);
			pullToReferesh(driver);
			Thread.sleep(1000);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InfoIcon")),
					"Failure Reason: AD Info Icon NOT displayed ");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("StarIcon")),
					"Failure Reason: AD Star Icon NOT displayed ");

			String adName = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SenderName"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("StarIcon"));
			log.info("Clicked on star icon");
			Thread.sleep(4000);
			//Added By Phaneendra
			openMenuDrawer(driver);
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("StarredLabelOption"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("StarredLabelOption"));
			}else{
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("StarredLabelOption"));
			}
			Thread.sleep(2000);
			//flag = getScreenshotAndCompareImage(methodName);
			log.info("Pulling down to refresh");
			pullToReferesh(driver);
			Thread.sleep(2000);
			flag = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("EmailSubjectText").replace("Subject", adName));
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("StarredInfoIcon")),
					"AD one box is NOT changed to other email after clicking on Star icon for " + inboxCategory);
			return flag;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(
					"Failed verifyAd_StarAd for inbox category :: " + inboxCategory + "::::" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to Verify Able to expand Ad info and Control Ads
	 * 
	 * @param inboxCategory
	 * @throws Exception
	 */
	public void verifyControlAds(AppiumDriver driver,String inboxCategory) throws Exception {
		log.info("******************* Start verifyExpandAndControlAds Functionality*******************");
		try {
			Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage
					.getTestData("verifyTwoAdsDisplayInPromotionSocial");

			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get(inboxCategory));
			Thread.sleep(3000);
			//pullToReferesh(driver);
			log.info("Verifying the info icon");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InfoIcon")),
					"AD InfoIcon NOT displayed ");

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InfoIcon"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ControlAdsLikeThis"));
			Thread.sleep(5000);
			/*boolean isGmailLoginRendered = CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("NextButton"));
			log.info(isGmailLoginRendered);
			if (isGmailLoginRendered) {
				log.info("Gmail Next Button Displayed");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextButton"));
				hideKeyboard();
				CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("PasswordField"),
						commonElements.get("twoadspwd"));
				hideKeyboard();
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("NextButton"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextButton"));
				}
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SignInButton"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SignInButton"));
				}
				hideKeyboard();
			}
			*/
			if(CommonFunctions.getSizeOfElements(driver, verifyTwoAdsDisplayInPromotionSocial.get("GmailPwd"))!=0){
				CommonFunctions.searchAndSendKeysByXpath(driver, verifyTwoAdsDisplayInPromotionSocial.get("GmailPwd"), verifyTwoAdsDisplayInPromotionSocial.get("AcctPwd"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextButton"));
			}
			log.info("Verifying Why this Ad and Block Ad is rendered");
			Assert.assertTrue(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("WhyThisHeader")),
					" Failure Reason :Why this Ad Header  NOT displayed  ");
			log.info("Verified Why this ad header is displayed");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("BlockAd")),
					" Failure Reason : Block Ads element NOT displayed  ");
			log.info("Verified Block ad is displayed");
			navigateBack(driver);
			if (!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ControlAdsLikeThis"))) {
				navigateBack(driver);
				navigateBack(driver);
			}
			CommonFunctions.searchAndClickByXpath(driver, verifyTwoAdsDisplayInPromotionSocial.get("AdvName"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InfoIcon"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ControlAdsLikeThis"));
			Thread.sleep(5000);
			Assert.assertTrue(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("WhyThisHeader")),
					" Failure Reason :Why this Ad Header  NOT displayed  ");
			log.info("Verified Why this ad header is displayed");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("BlockAd")),
					" Failure Reason : Block Ads element NOT displayed  ");
			log.info("Verified Block ad is displayed");
			navigateBack(driver);
			

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed verifyExpandAndControlAds for inbox category :: " + inboxCategory + "::::"
					+ e.getLocalizedMessage());
		}
	}
}