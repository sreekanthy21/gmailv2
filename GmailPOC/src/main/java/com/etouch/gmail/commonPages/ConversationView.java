package com.etouch.gmail.commonPages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class ConversationView extends InboxFunctions {

	/** The log. */
	private Log log = LogUtil.getLog(ConversationView.class);

	/**
	 * Instantiates an InboxFunctions.
	 */
	public ConversationView() {

	}

	/**
	 * Method to tap on Recipient summary
	 * 
	 * @throws Exception
	 */
	public void tapOnRecipientSummary(AppiumDriver driver) throws Exception {
		try {
			log.info("Tapping on recipient summary");
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RecipientSummary"));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to Tap on RecipentSummary Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to tap on email snippet
	 * 
	 * @throws Exception
	 */
	public void tapOnEmailSnippet(AppiumDriver driver) throws Exception {
		try {
			log.info("Tapping on email snippet");
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSnippet"));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to Tap on RecipentSummary Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method for action bar conversation view
	 * 
	 * @throws Exception
	 */
	public void actionBarConversationView(AppiumDriver driver) throws Exception {
		try {
			log.info("tapping on 'Action bar' in CV");
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailToolBarArchiveIcon"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailToolBarDeleteIcon"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailToolBarUnReadIcon"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailMoreOptionsNavigation"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AttachmentGrid"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("actionBarMenuOptionXpath"));
			java.util.List<WebElement> actionBarMenuList = driver.findElementsByXPath(commonElements.get("actionBarMenuXpath"));
			for (WebElement webElement : actionBarMenuList) {
				log.info("actionBarMenuList are  ::" + webElement.getText());
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to Verify Action Bar :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Recipient Details
	 * 
	 * @param title
	 * @throws Exception
	 */
	public void verifyRecipientDetails(AppiumDriver driver,String title) throws Exception {
		try {
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ViewRecipientDetails"));
			waitForElementToBePresentByXpath(commonElements.get("DetailFromEmail"));
			log.debug("Verifying recipient details");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DetailFromEmail")),
					" DetailFromEmail NOT rendered");
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DetailToEmail")),
					" DetailToEmail NOT rendered");
		/*	try {
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DetailCCEmail")))
					Assert.assertTrue(
							driver.findElementByXPath(commonElements.get("DetailCCEmail")).getText().contains(".com"),
							"Detail CC email text not matched");

				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DetailBCCEmail")))
					Assert.assertTrue(
							driver.findElementByXPath(commonElements.get("DetailBCCEmail")).getText().contains(".com"),
							"Detail BCC email text not matched");
			} catch (Throwable e) {
				log.info("CC BCC field were not displayed");
			}
*/
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("Date")),
					"Date NOT rendered");
			log.info("Tapping on 'View security details'");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ViewSecurityDetails"));

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SecurityDetailsPresence"))) {
				Assert.assertEquals(driver.findElementByXPath(commonElements.get("SecurityDetailsPresence")).getText(),
						title, "Security Details Presence text NOT matched");
				Assert.assertTrue(
						CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailedByPresence")),
						" Mailed by element NOT rendered");
			}
			navigateBack(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HideDetails"));
			log.debug("Recipient details displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Verify Recipient Details due to : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify super collapse count of email
	 * 
	 * @throws Exception
	 */
	public void verifySuperCollapseCountOfMail(AppiumDriver driver) throws Exception {
		String Actual = null;
		List<WebElement> list = new ArrayList<WebElement>();
		List<WebElement> newList1 = new ArrayList<WebElement>();
		List<WebElement> newList2 = new ArrayList<WebElement>();
		int expandedMailCount =0;
		log.info("verifying mail count");
		
		try {
			list = driver.findElementsById(commonElements.get("MailContainer"));
			log.info("list size: "+list.size());
			Actual = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SuperCollapse"));
			log.info("Actual super collapse count:   "+Actual);
						
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SuperCollapse"));
			Thread.sleep(1000);
			newList1 = driver.findElementsById(commonElements.get("MailContainer"));
			log.info("New list1 size:  "+newList1.size());
			scrollDown(5);
			CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("DraftMail"));
			
			newList2 = driver.findElementsById(commonElements.get("MailContainer"));
			log.info("New list2 size:  "+newList2.size());
			expandedMailCount = newList1.size()+newList2.size();
			
			log.info("All mails opened count: " +(expandedMailCount-2));
			
			
			Assert.assertEquals(list.size() + Integer.parseInt(Actual), expandedMailCount-2,
					"Mailcontainer list count mismatched");
			log.info("Mail count displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Match Count : Actual :" + (expandedMailCount-2) + " Expected : "
					+ (list.size() + Integer.parseInt(Actual)));
		}
	}
	/**
	 * Method to verify tasks
	 * 
	 * @throws Exception
	 */
	public void verifyTasks(AppiumDriver driver) throws Exception {
	
		launchGmailApp(driver);
		log.info("Verifying Add to Task option from CV view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
		String subjtext = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
		log.info("Subject is "+subjtext);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddToTask"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ViewLink"));
    String taskTile = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("MyTasksTitle"));
	log.info("Title in MyTasks is "+taskTile);
    if(subjtext.contains(taskTile)){
    	log.info("Tasks app is launched and opened the mail which  is added to task ");
    }else{
    	
    	throw new Exception("Task app didn't launch  for correct email");
    }
    
	}
	/**
	 * Method to verify sent email in IMAP
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyEmailInSent(AppiumDriver driver) throws Exception {
		Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
		navDrawer.openMenuDrawer(driver);
		navDrawer.navigateToSentBox(driver);
		Thread.sleep(12000);
		inbox.pullToReferesh(driver);
		Thread.sleep(4000);
		inbox.pullToReferesh(driver);
		Thread.sleep(4000);
		//CommonFunctions.searchAndClickByXpath(driver,commonElements.get("FirstMailIndex") ); // Venkat on 12/12/2019
		CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[contains(@text,'VerifSendingEmailFromIMAPAccount')]");
	log.info("Verifying email in sent folder");
	
	//String emailSubjectInSent = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV")); // Venkat on 12/12/2019 //One more time(3) file reading
	//String emailSubjectInSent = CommonFunctions.searchAndGetTextOnElementByXpath(driver, "//android.view.View[@resource-id='m1001']//android.view.View[@index='0']"); // Venkat on 12/12/2019
	String emailSubjectInSent = CommonFunctions.searchAndGetTextOnElementByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.gm:id/subject_and_folder_view']");
	log.info("emailSubjectInSent:"+emailSubjectInSent);
	
	//if(emailSubjectInSent.contains(verifySendMailIMAPData.get("SubjectData"))== true){
	if(emailSubjectInSent.trim().equalsIgnoreCase(verifySendMailIMAPData.get("SubjectData"))){
		log.info("************** Sent email is displayed  in Sent folder ****************");
//		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("FirstMailIndex") );
	}else{
		throw new Exception("Email sent is not displayed in Sent folder");
	}
	
	
	}
	/**
	 * Method to verify select label from search a TL view
	 * @author batchi
	 * @throws Exception 
	 */
public void clearSearchAndSelectRecent(AppiumDriver driver) throws Exception{
	Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
	log.info("Searching with"+verifyVariousSearchOperationsData.get("MTV1LabelText") + "and verifying the results");
	labelSearchAndVerify(driver, verifyVariousSearchOperationsData.get("Mtv1LabelSearch"),verifyVariousSearchOperationsData.get("MTV1LabelText") );
	
	CommonFunctions.searchAndClickByXpath(driver,commonElements.get("ClearSearchButton"));
	log.info("Searching with"+verifyVariousSearchOperationsData.get("TestLabelText") + "and verifying the results");
	labelSearchAndVerify(driver, verifyVariousSearchOperationsData.get("TestLabelSearch"),verifyVariousSearchOperationsData.get("TestLabelText") );
	inbox.navigateUntilHamburgerIsPresent(driver);
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
	CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),verifyVariousSearchOperationsData.get("ScheduledLabelSearch") );
	((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
	waitForElementToBePresentByXpath(commonElements.get("SearchEmpty"));
	if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SearchEmpty"))!=0){
		log.info("******* No Search results displayed************");
	}else{
		throw new Exception("!!!!!! Search results are displayed !!!!!!!!");
	}
	CommonFunctions.searchAndClickByXpath(driver,commonElements.get("ClearSearchButton"));
	Thread.sleep(2000);
	driver.hideKeyboard();
	Thread.sleep(2000);
	log.info("Selecting recent search from suggestions");
	CommonFunctions.searchAndClickByXpath(driver, verifyVariousSearchOperationsData.get("RecentSearch"));
	Thread.sleep(5000);
	if(CommonFunctions.getSizeOfElements(driver, verifyVariousSearchOperationsData.get("ResultsText"))!=0){
		log.info("********* Results are displayed ************");
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("FirstMail123") );
		String subj3= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		if(subj3.contains(verifyVariousSearchOperationsData.get("TestLabelText"))){
			log.info("********** Searched label with" +verifyVariousSearchOperationsData.get("TestLabelText")+ " mail is displayed ***********");
		}
		else{
			throw new Exception("!!!!! Searched label with" +verifyVariousSearchOperationsData.get("TestLabelText")+ " mail is not displayed !!!!!!!!!");
		}
	}else{
		throw new Exception("!!!!!!!!!! No Results displayed !!!!!!!!!!");
	}
	
	
}
public void labelSearchAndVerify(AppiumDriver driver, String searchText, String Label) throws Exception{
	Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
	CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),searchText );
	((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
	Thread.sleep(5000);
	if(CommonFunctions.getSizeOfElements(driver, verifyVariousSearchOperationsData.get("ResultsText"))!=0){
		log.info("********* Results are displayed ************");
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("FirstMail123") );
//		String subj1= driver.findElement(By.xpath(commonElements.get("FirstMail"))).getAttribute("content-desc");
//		String subj1 =	driver.findElementByXPath(commonElements.get("FirstMail")).getAttribute("content-desc");
		String subj1= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		log.info("Subject of first mail is: "+subj1);
		if(subj1.contains(Label)){
			log.info("********** Searched label with " +Label+ " mail is displayed ***********");
		}
		else{
			throw new Exception("!!!!! Searched label with " +Label+ " mail is not displayed !!!!!!!!!");
		}
	}else{
		throw new Exception("!!!!!!!!!! No Results displayed !!!!!!!!!!");
	}
	driver.navigate().back();
}
	/**
	 * Method to verify select label from search a TL view
	 * @author batchi
	 * @throws Exception 
	 * @throws Exception
	 */
public void selectLabelSearchTL(AppiumDriver driver) throws Exception{
	Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");

	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
	CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),verifyVariousSearchOperationsData.get("SearchTextWithInbox") );
	((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SearchPageResultslable"));
	log.info(" ********* Search result label is selected ************** ");
	TouchAction action = new TouchAction((MobileDriver)driver);
	  action.longPress(03,600).moveTo(900,600).release().perform();
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("StarredLabelOption"));
	CommonFunctions.isElementByXpathDisplayed(driver,commonElements
			.get("StarredLabelOption"));
	log.info(" *********** Starred label is selected **************");
	driver.navigate().back();
}
/**
 * Method to verify select label from search a CV view
 * @author batchi
 * @throws Exception 
 * @throws Exception
 */
public void selectLabelSearchCV(AppiumDriver driver) throws Exception{
Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");

CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),
		verifyVariousSearchOperationsData.get("SearchTextWithInbox") );
((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
CommonFunctions.isElementByXpathDisplayed(driver,commonElements
		.get("SearchPageResultslable"));
log.info("*********** Search result label is selected ***************");
log.info("Clicking on the 1st email");
CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
inbox.waitForElementToBePresentByXpath(commonElements.get("MarkUnread"));
TouchAction action = new TouchAction((MobileDriver)driver);
  action.longPress(03,600).moveTo(900,600).release().perform();
CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ImportantLabelOption"));
CommonFunctions.isElementByXpathDisplayed(driver,commonElements
		.get("ImportantLabelOption"));
log.info(" **************** Important label is selected ****************");
}
/**
 * Method to verify Label Mutation TL
 * @author batchi
 * @throws Exception
 */
public void verifyLabelMutationTL(AppiumDriver driver) throws Exception {
	 Map < String, String > ChangeLabelMutationData = commonPage.getTestData("changeLabels");
	
	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
	 Thread.sleep(2000);
     CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickChangeLabel"));
	 Thread.sleep(1000);
	 String isLabelChecked=driver.findElement(By.xpath(ChangeLabelMutationData.get("InboxChangeLabel"))).getAttribute("checked");
	 if(isLabelChecked.equalsIgnoreCase("True"))
	 {
		 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("InboxChangeLabel"));				
	 }
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickOkChangeLabel"));
     Thread.sleep(1000);
     inbox.verifyMailNotPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
//     inbox.pullToReferesh(driver);
     commonPage.pullToReferesh(driver);
     Thread.sleep(1000);
     inbox.verifyMailNotPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
     navDrawer.navigateToAllEmails(driver);
     commonPage.pullToReferesh(driver);
    
     inbox.verifyMailPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
    
     Thread.sleep(2000);
     String label = CommonFunctions.searchAndGetTextOnElementByXpath(driver,commonElements.get("SubjectLinewithID"));
     if(label.contains("Inbox")){
		 throw new Exception(" !!!!!!!!! Inbox label is still displayed !!!!!!!!!!");
	 }else{
		 log.info("####### Inbox label is not displayed when changed label mutation from TL view #######");
	 }
//Setting the label back to inbox     
     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickChangeLabel"));
	 Thread.sleep(1000);
	 String isLabelChecked2=driver.findElement(By.xpath(ChangeLabelMutationData.get("InboxChangeLabel"))).getAttribute("checked");
	 if(isLabelChecked2.equalsIgnoreCase("False"))
	 {
		 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("InboxChangeLabel"));				
	 }
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickOkChangeLabel"));
     Thread.sleep(1000);
	 
    commonPage.navigateBack(driver);
    Thread.sleep(1000);
    commonPage.navigateBack(driver);
}
/**
 * Method to verify Label Mutation CV
 * @author batchi
 * @throws Exception
 */
public void verifyLabelMutationCV(AppiumDriver driver) throws Exception {
	 Map < String, String > ChangeLabelMutationData = commonPage.getTestData("changeLabels");
	 
//    CommonFunctions.searchAndClickByXpath(driver,commonElements.get("FirstMail"));
    openMail(driver, ChangeLabelMutationData.get("labelMutationSubject"));
//	 String mailText= CommonFunctions.searchAndGetTextOnElementByXpath(driver,commonElements.get("SubjectLinewithID"));
//	log.info("Mail Selected: "+mailText);
    
    CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickChangeLabel"));
	 Thread.sleep(1000);
	 String isLabelChecked=driver.findElement(By.xpath(ChangeLabelMutationData.get("InboxChangeLabel"))).getAttribute("checked");
	 if(isLabelChecked.equalsIgnoreCase("True"))
	 {
		 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("InboxChangeLabel"));				
	 }
	 CommonFunctions.searchAndClickByXpath(driver, ChangeLabelMutationData.get("ClickOkChangeLabel"));
     Thread.sleep(1000);
     inbox.verifyMailNotPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
//     inbox.pullToReferesh(driver);
     commonPage.pullToReferesh(driver);
     Thread.sleep(1000);
     inbox.verifyMailNotPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
     navDrawer.navigateToAllEmails(driver);
     inbox.pullToReferesh(driver);
     inbox.verifyMailPresent(driver, ChangeLabelMutationData.get("labelMutationSubject"));
     String label = CommonFunctions.searchAndGetTextOnElementByXpath(driver,commonElements.get("SubjectLinewithID"));
	 if(label.contains("Inbox")){
		 throw new Exception(" !!!!!!!!! Inbox label is still displayed !!!!!!!!!!");
	 }else{
		 log.info("####### Inbox label is not displayed when changed label mutation from CV view #######");
	 }
	 
    commonPage.navigateBack(driver);
}
public void saveToDriveFromDrafts(AppiumDriver driver) throws Exception{
	Map < String, String > verifyEditDraftAndSendMailData = CommonPage.getTestData("verifyEditDraftAndSendMail");
	log.info("Clicking on the 1st email");
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	   Thread.sleep(2000);
	   String mailSubject = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifyEditDraftAndSendMailData.get("SubjectField"));
	   log.info("Mail selected with subject:  "+mailSubject);
	   CommonFunctions.searchAndClickByXpath(driver, verifyEditDraftAndSendMailData.get("ReplyButton"));
	   attachment.addAttachementFromRoot(driver, verifyEditDraftAndSendMailData.get("SampleDocFile") );
	  commonPage.navigateUntilHamburgerIsPresent(driver);
	  CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	   CommonFunctions.searchAndClickByXpath(driver,verifyEditDraftAndSendMailData.get("SaveToDriveOption"));
	   log.info("Clicked on Save to Drive option");
	   inbox.waitForElementToBePresentByXpath(verifyEditDraftAndSendMailData.get("SavedToDriveSnackBar"));
	   if(CommonFunctions.getSizeOfElements(driver, verifyEditDraftAndSendMailData.get("OrganizeOptionFromSnackBar"))!=0){
		   Thread.sleep(2000);
		   log.info("*********** Organize option  is displayed and file is saved to Drive *************");
		   CommonFunctions.searchAndClickByXpath(driver,verifyEditDraftAndSendMailData.get("OrganizeOptionFromSnackBar") );
		   waitForElementToBePresentByXpath(commonElements.get("DriveSelect"));
		  if(CommonFunctions.getSizeOfElements(driver, verifyEditDraftAndSendMailData.get("MyDriveOpen"))!=0){
		   log.info("*********** Drive is opened *********");
		   CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSelect"));
		  }else{
			  throw new Exception("!!!!!!!! Drive is not opened !!!!!!!!!");
		  }
	   }else{
		   throw new Exception("!!!!!!! Organize option is not displayed and File is not saved to Drive !!!!!!!!!!!!");
	   } 
	   CommonFunctions.searchAndClickByXpath(driver,commonElements.get("EditViewIcon"));
		log.info("Clicked on EditViewIcon");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		log.info("Clicked on MoreOption");
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DiscardMail"));
		log.info("Clicked on DiscardMail");
}
}
