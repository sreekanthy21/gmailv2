package com.etouch.gmail.commonPages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.logging.Log;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.mobile.gmail.HtmlEmailSender;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

@SuppressWarnings(value = { "all" })
public class InboxFunctions extends CommonPage {

	/** The log. */
	private static Log log = LogUtil.getLog(InboxFunctions.class);
	String smartReply;
	HtmlEmailSender mail = new HtmlEmailSender();
	static String path = System.getProperty("user.dir");
	String command;

	/**
	 * Method to compose and send email
	 * 
	 * @param To
	 * @param CC
	 * @param BCC
	 * @param EmailSubject
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeAndSendMail(AppiumDriver driver,String To, String CC, String BCC, String EmailSubject, String ComposeData)
			throws Exception {
		composeNewMail(driver, To, CC, BCC, EmailSubject, ComposeData);
		tapOnSendForNewMail();
	}
	public String composeAndSendNewMail(AppiumDriver driver,String To, String CC, String BCC, String EmailSubject, String ComposeData)
			throws Exception {
		String fromUser=composeAsNewMail(driver, To, CC, BCC, EmailSubject, ComposeData);
		tapOnSendForNewMail();
		return fromUser;
	}
	/**
	 * Method to compose new email
	 * 
	 * @param To
	 * @param CC
	 * @param BCC
	 * @param EmailSubject
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeNewMail(AppiumDriver driver, String To, String CC, String BCC, String EmailSubject, String ComposeData)
			throws Exception {
		
		log.info("composeNewMail Selecting Compose button (FAB)");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
		composeOpenedMailBody(driver,To, CC, BCC, EmailSubject, ComposeData);
	}
	
	public void multiSelectMail(AppiumDriver driver) throws Exception{
		try{
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SenderImageIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailMoreOption"));
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public void multiSelectMailNew(AppiumDriver driver) throws Exception{
		try{
			String mailInTL = commonElements.get("SelectAMailInTLView");
			mailInTL = mailInTL.replaceAll("MailIndex", "1");
			CommonFunctions.searchAndClickByXpath(driver, mailInTL);
			Thread.sleep(1000);
			mailInTL = mailInTL.replaceAll("1", "2");
			CommonFunctions.searchAndClickByXpath(driver, mailInTL);
			Thread.sleep(1000);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void viewMoreOptionsCV(AppiumDriver driver) throws Exception{
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String 	composeMailWithOnlyTo(AppiumDriver driver, String To)
			throws Exception {
		
		log.info("composeMailWithOnlyTo Selecting Compose button (FAB)");
		Thread.sleep(1000);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			log.info("composeMailWithOnlyTo Clicked on compose button");
		}
		commonPage.waitForElementToBePresentByXpath(commonElements.get("FromUserXpath"));
		String fromUser=driver.findElement(By.xpath(commonElements.get("FromUserXpath"))).getText();
		log.info("composeMailWithOnlyTo From User is  "+fromUser);
		composeMailWithToAddressOnly(driver, To);
		//composeOpenedMailBody(driver,To, CC, BCC, EmailSubject, ComposeData);
		return fromUser;
	}
	
	
	
	
	
	public String composeAsNewMail(AppiumDriver driver, String To, String CC, String BCC, String EmailSubject, String ComposeData)
			throws Exception {
		
		log.info("Selecting Compose button (FAB)");
		Thread.sleep(8000);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			log.info("Clicked on compose button");
		}
		commonPage.waitForElementToBePresentByXpath(commonElements.get("FromUserXpath"));
		String fromUser=driver.findElement(By.xpath(commonElements.get("FromUserXpath"))).getText();
		log.info("From User is  "+fromUser);
		composeOpenedMailBody(driver,To, CC, BCC, EmailSubject, ComposeData);
		return fromUser;
	}
	
	public void tapOnSendForNewMail() throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			log.info("Tapping on Send mail");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("AcceptOk"));
			/*Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SharingOptionsTitle"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			}*/
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			if(deviceName.startsWith("Nexus 6P")){
				//driver.navigate().back();
			}else{
				waitForHambergerMenuToBePresent();
			}
		} catch (Throwable e) {
			throw new Exception("Unable to send and navigate back due to :" + e.getLocalizedMessage());
		}
	}
	
	public void clickEditDraft(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EditMail"));
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Edit button is not visible");
		}
	}
	
	
	/**
	 * Open a draft message and send the mail
	 * 
	 * @param driver
	 * @param subject
	 */
	public void openDraftMail(AppiumDriver driver, String subject)throws Exception{
		try{	
				Thread.sleep(2000);
				log.info("Finding the draft mail with the subject "+subject);
				//if(driver.findElements(By.xpath("//android.view.View[contains(@content-desc,'"+subject+"')]")).size()!=0){
				//	driver.findElement(By.xpath("//android.view.View[contains(@content-desc,'"+subject+"')]")).click();
				openMail(driver,subject );	
				Thread.sleep(1300);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EditMail"));
					CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ComposeTitle"));
		
					log.info("Verified the draft mail is opened");
				//}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	/**
	 * Method to compose opened mail body
	 * 
	 * @param To
	 * @param CC
	 * @param BCC
	 * @param EmailSubject
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeOpenedMailBody(AppiumDriver driver,String To, String CC, String BCC, String EmailSubject, String ComposeData)
			throws Exception {
		
		Thread.sleep(500);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("CcBccExpandButton"));
		log.info("Entering required email addreess in To, Cc and Bcc fields");
		if (To != null && !(To == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("ToFieldXpath"), To + " ");
			log.info("To address entered "+To);
		}
		Thread.sleep(100);

		if (CC != null && !(CC == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("CCFieldXpath"), CC + " ");
			log.info("CC address entered "+CC);

		}

		if (BCC != null && !(BCC == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("BCCFieldXpath"), BCC + " ");
			log.info("BCC address entered "+BCC);

		}
		scrollUp(1);
		log.info("Entering Subject and mail body");
		if (EmailSubject != null && !(EmailSubject == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("EmailSubjectXpath"), EmailSubject);
			log.info("EmailSubject is entered "+EmailSubject);

		}

		hideKeyboard();
		//--Added by Phaneendra--//
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeBody"));
		CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), ComposeData);
		log.info("ComposeBody is entered "+ComposeData);

		hideKeyboard();
		Thread.sleep(500);
	}
	
	public void composeMailWithToAddressOnly(AppiumDriver driver,String To)
			throws Exception {
		
		Thread.sleep(500);
		log.info("Entering required email addreess in To, Cc and Bcc fields");
		if (To != null && !(To == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("ToFieldXpath"), To);
			log.info("To address entered "+To);
		}
		Thread.sleep(100);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectXpath"));
	}
	
	
	
	public String composeAsNewMailWithOut(AppiumDriver driver, String EmailSubject)
			throws Exception {
		
		log.info("Selecting Compose button (FAB)");
		Thread.sleep(2000);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
		}
		Thread.sleep(2000);
		String fromUser=driver.findElement(By.xpath(commonElements.get("FromUserXpath"))).getText();
		log.info("From User is  "+fromUser);
		MailWithOutCompose(driver, EmailSubject);
		return fromUser;
	}
	

	public String composeAsNewMailWithOnlyTo(AppiumDriver driver, String To, String EmailSubject)
			throws Exception {
		
		log.info("composeAsNewMailWithOnlyTo Selecting Compose button (FAB)");
		commonPage.waitForElementToBePresentByXpath(commonElements.get("ComposeMailButton"));
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));

		}
		Thread.sleep(2000);
		String fromUser=driver.findElement(By.xpath(commonElements.get("FromUserXpath"))).getText();
		log.info("composeAsNewMailWithOnlyTo From User is  "+fromUser);
		MailWithOnlyTo(driver, To, EmailSubject);
		tapOnSend(driver);
		return fromUser;
	}

	public void MailWithOnlyTo(AppiumDriver driver, String To, String EmailSubject)
			throws Exception {
		
		Thread.sleep(500);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("CcBccExpandButton"));
		log.info("Entering required email addreess in To, Cc and Bcc fields");
		if (To != null && !(To == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("ToFieldXpath"), To + " ");
			log.info("To address entered");
		}
		
		hideKeyboard();		
		log.info("Entering Subject and mail body");
		if (EmailSubject != null && !(EmailSubject == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("EmailSubjectXpath"), EmailSubject);
			log.info("EmailSubject is entered");
		}else{
			throw new Exception(EmailSubject +" is null");
		}
		hideKeyboard();
	}
	
	
	
	public void MailWithOutCompose(AppiumDriver driver, String EmailSubject)
			throws Exception {
		
		Thread.sleep(500);
		log.info("Entering Subject and mail body");
		if (EmailSubject != null && !(EmailSubject == "")) {
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("EmailSubjectXpath"), EmailSubject);
			log.info("EmailSubject is entered");

		}

		hideKeyboard();
	}

	
	/**Save mail as draft using save as draft option from menu options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void saveMailAsDraft(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SaveDraft"));
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("mail is not saved as draft");
		}
	}

	/**
	 * Method to verify Gmail App init
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyGmailAppInitOld(AppiumDriver driver) throws Exception {
		try {
			waitMap.get(Thread.currentThread().getId())
			.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("HamburgerMenu"))));
		} catch (Exception e) {
			launchGmailApp(driver);
		}
	}

	
	public void verifyGmailAppInitState(AppiumDriver driver) throws Exception {
		try {
			//boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu")); // Commented by Venkat on 26/12/2018
			// added by Venkat on 26/12/2018 : start
			boolean value;
			try{
			value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"));
			} catch (Exception e) {
				value = false;
				System.out.println("verifyGmailAppInitState Unable to Open Gmail App, Please check Gmail App is available");
				//commonPage.catchBlock(e, driver, "verifyGmailAppInitState");
			}
			// added by Venkat on 26/12/2018 : end
			if(value==false){
				log.info("verifyGmailAppInitState Launching the gmail application");
				//launchGmailApp2(driver);
				launchGmailApp(driver);
					}
			else{
				log.info("verifyGmailAppInitState Gmail App already open");
			}
			//clickConfidential(driver);
		} catch (Exception e) {
			System.out.println("verifyGmailAppInitState Unable to Open Gmail App, Please check Gmail App is available");
			commonPage.catchBlock(e, driver, "verifyGmailAppInitState");
		}
	}
	
	 public static void SendMail(AppiumDriver driver) throws Exception {
	    	log.info("Sending mail");
	    	String host = "smtp.gmail.com";
	        String Password = "mobileautomation";
	        String from = "gm.gig2.auto@gmail.com";
	        String toAddress = "gm.gig1.auto@gmail.com";
	        
	        String filename = path+java.io.File.separator+"SuspiciousApk"+java.io.File.separator+"Suspicious.apk";
	        // Get system properties
	        Properties props = System.getProperties();
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        Session session = Session.getInstance(props, null);

	        MimeMessage message = new MimeMessage(session);

	        message.setFrom(new InternetAddress(from));

	        message.setRecipients(Message.RecipientType.TO, toAddress);

	        message.setSubject("ReportNotSuspicious");

	        BodyPart messageBodyPart = new MimeBodyPart();

	        messageBodyPart.setText("Here's the file");

	        Multipart multipart = new MimeMultipart();

	        multipart.addBodyPart(messageBodyPart);

	        messageBodyPart = new MimeBodyPart();

	        DataSource source = new FileDataSource(filename);

	        messageBodyPart.setDataHandler(new DataHandler(source));

	        messageBodyPart.setFileName(filename);

	        multipart.addBodyPart(messageBodyPart);

	        message.setContent(multipart);

	        try {
	            Transport tr = session.getTransport("smtps");
	            tr.connect(host, from, Password);
	            tr.sendMessage(message, message.getAllRecipients());
	            System.out.println("Mail Sent Successfully");
	            tr.close();

	        } catch (SendFailedException sfe) {
	        	
	            System.out.println(sfe);
	            throw new Exception(sfe);
	        }
	    }
	
	
	/** Method for to open the Gmail app
	 * 
	 * @param driver
	 */
	public void launchGmailApplicationLite(AppiumDriver driver)throws Exception{
		try{
			String appPackage = "com.google.android.gm.lite";
			String appActivity = "com.google.android.gm.ConversationListActivityGmail";
			log.info("Launching the gmail app after adding the google drive file");
			((AndroidDriver)driver).startActivity(appPackage, appActivity);
			boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"));
			
			if(value==false){
				//launchGmailApp2(driver);
				((AndroidDriver)driver).startActivity(appPackage, appActivity);
				Thread.sleep(4000);
					}
			else{
				log.info("Gmail App already open");
			}
		}catch(Throwable e){
			e.printStackTrace();
			throw new Exception("unable to launch the gmail app :" + e.getLocalizedMessage());
		}
	}
	
	
	
	/** Method for to open the Gmail app
	 * 
	 * @param driver
	 */
	public void launchGmailApplication(AppiumDriver driver)throws Exception{
		try{
			String appPackage = "com.google.android.gm";
			String appActivity = "com.google.android.gm.ConversationListActivityGmail";
			log.info("In IF.launchGmailApplication Launching the gmail app after adding the google drive file");
			((AndroidDriver)driver).startActivity(appPackage, appActivity);
			boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"));
			
			if(value==false){
				//launchGmailApp2(driver);
				((AndroidDriver)driver).startActivity(appPackage, appActivity);
				Thread.sleep(4000);
					}
			else{
				log.info("In IF.launchGmailApplication Gmail App already open");
			}
		}catch(Throwable e){
			e.printStackTrace();
			throw new Exception("unable to launch the gmail app :" + e.getLocalizedMessage());
		}
	}
	
	/** Method for to open the Gmail app
	 * 
	 * @param driver
	 */
	public void launchGmailGoApplication(AppiumDriver driver)throws Exception{
		try{
			String appPackage = "com.google.android.gm.lite";
			String appActivity = "com.google.android.gm.ConversationListActivityGmail";
			log.info("Launching the gmail app after adding the google drive file");
			((AndroidDriver)driver).startActivity(appPackage, appActivity);
			boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"));
			
			if(value==false){
				//launchGmailApp2(driver);
				((AndroidDriver)driver).startActivity(appPackage, appActivity);
				Thread.sleep(4000);
					}
			else{
				log.info("Gmail App already open");
			}
		}catch(Throwable e){
			e.printStackTrace();
			throw new Exception("unable to launch the gmail app :" + e.getLocalizedMessage());
		}
	}
	
public void syncInbox(AppiumDriver driver)
{
	try {
		waitMap.get(Thread.currentThread().getId())
		.until(ExpectedConditions.elementToBeClickable(By.id(commonElements.get("SyncSettingID"))));
		CommonFunctions.searchAndClickById(driver, commonElements.get("SyncSettingID"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SyncMessage"));
		//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SyncCheckbox"));
		Thread.sleep(2000);
		driver.navigate().back();
		driver.navigate().back();
		//Thread.sleep(2000);
		commonPage.navigateBack(driver);
		Thread.sleep(2000);
	} catch (Throwable e) {
		//e.printStackTrace();
	}
}
	/**
	 * Method to Open Menu drawer
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public static void openMenuDrawer(AppiumDriver driver) throws Exception {
		try {
			log.info("In IF.openMenuDrawer openMenuDrawer Tap on Menu drawer");
			String accountListButton = commonElements.get("ExpandAccountList");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
			//Thread.sleep(2500);
			// Commented by Venkat on 06/05/2019 : Start
			/*
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("CloseHamburger"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("CloseHamburger"));
			}
			*/
			// Commented by Venkat on 06/05/2019 : End
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			log.info("In IF.openMenuDrawer Tapped on Menu drawer");
			Thread.sleep(1200);
		/*	if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList"))==0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			}
		*/	
			//scrollDown(1);
			/*try{
				Thread.sleep(1500);
			if(CommonFunctions.getSizeOfElements(driver, accountListButton )==0){
				Thread.sleep(1500);
				log.info("Scrolling up");
				scrollUp(1);
			}
			}catch(Exception e){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			}*/
			if(driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.ListView[@resource-id='android:id/list']")).size()==0){
				log.info("In IF.openMenuDrawer Account drawer is not opened hence tapping it again");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				if(driver.findElements(By.xpath("//android.widget.FrameLayout/android.widget.ListView[@resource-id='android:id/list']")).size()==0){
					log.info("In IF.openMenuDrawer Account drawer is not opened hence tapping it again for second time");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				}
			}else{
				
				log.info("In IF.openMenuDrawer Scrolling up to find accountlistbutton");
				/*int count = 0;
			while (CommonFunctions.isElementByXpathNotDisplayed(driver, accountListButton)==true){
				scrollUp(3);
				count++;
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("FolderIcon"))){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				}
				if(count>10)
					break;
			}*/
				scrollUp(3);
				
			/*for(int i=1;i<=4;i++){
				if(CommonFunctions.getSizeOfElements(driver, accountListButton)!=0){
					scrollUp(3);
					count++;
					if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("FolderIcon"))){
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					}
					if(count>10)
						break;
				}
			}*/
			}		
		} catch (NoSuchElementException e) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			
		}
	}

	/**
	 * Method to verify Subject of email
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifySubjectOfMail(AppiumDriver driver,String MailSubject) throws Exception {
		String Actual = null;
		try {
			log.info("Verifying Subject of Mail");
			
			Actual = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
			Assert.assertTrue(Actual.contains(MailSubject), " Actual subject : " + Actual
					+ ":: doesn't contains expected subject text Expected:::" + MailSubject);
			log.info("Subject of Mail displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Match Mail Subject : Actual :" + Actual + " Expected : " + MailSubject);
		}
	}

	/**--Method to verify the Google Drive app is opened
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyGoogleDriveAppInitState(AppiumDriver driver) throws Exception {
		try {
			String appPackage = "com.google.android.apps.docs";
			String appActivity = ".app.NewMainProxyActivity";
			log.info("Launching the google drive");
			//((AndroidDriver)driver).startActivity(appPackage, appActivity);
			Runtime.getRuntime().exec("adb shell monkey -p com.google.android.apps.docs -c android.intent.category.LAUNCHER 1");
			log.info("Google Drive Launched");
			Thread.sleep(3000);
		/*	commonPage.waitForElementToBePresentByXpath(commonElements.get("DriveTitle"));
			boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DriveTitle"));
			if(value==false){
				//launchGmailApp2(driver);
				//((AndroidDriver)driver).startActivity(appPackage, appActivity);
				Runtime.getRuntime().exec("adb shell monkey -p com.google.android.apps.docs -c android.intent.category.LAUNCHER 1");
					}
			else{
				log.info("Google Drive App already open");
			}
		*/} catch (Exception e) {
			System.out.println("Unable to Open Google Drive App, Please check Google Drive App is available");
			commonPage.catchBlock(e, driver, "verifyGoogleDriveAppInitState");
		}
	}
	
	public void OpenDownloadsApp(AppiumDriver driver) throws Exception {
		try {
			String appPackage = "com.android.documentsui";
			String appActivity = ".app.NewMainProxyActivity";
			//TouchActions action = new TouchActions(driver);
			log.info("Launching the Downloads app");
			//((AndroidDriver)driver).startActivity(appPackage, appActivity);
			Runtime.getRuntime().exec("adb shell monkey -p com.android.documentsui -c android.intent.category.LAUNCHER 1");
			log.info("Downloads folder launched");
			Thread.sleep(3000);
			
			log.info("Download files from drive");
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Show roots']")).click();
			Thread.sleep(1500);
			driver.findElement(By.xpath("//android.widget.TextView[@text='gm.gig1.auto@gmail.com']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//android.widget.TextView[@text='My Drive']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//android.widget.TextView[@text='Attachments']")).click();
			Thread.sleep(1000);
			WebElement audio = driver.findElement(By.xpath("//android.widget.TextView[@text='SampleAudio.mp3']"));
			if(audio.isDisplayed()){
				
				driver.tap(1, audio, 3000);
				driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='More options']")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Copy to')]")).click();
				Thread.sleep(1500);
				/*log.info("Downloads click");
				WebElement dwl = driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'Downloads')]"));
				driver.tap(1, dwl, 1000);
				Thread.sleep(5000);*/
				log.info("Copy click");

				driver.findElement(By.xpath("//android.widget.Button[contains(@resource-id,'android:id/button1')]")).click();
				Thread.sleep(600);
				driver.findElement(By.xpath("//android.widget.Button[contains(@resource-id,'android:id/button1')]")).click();

				log.info("Done");

			}
			
			} catch (Exception e) {
			System.out.println("Unable to Open Google Drive App, Please check Google Drive App is available");
			commonPage.catchBlock(e, driver, "verifyGoogleDriveAppInitState");
		}
	}
	
	
	
	
	/**
	 * Method to verify mail in sent box
	 * 
	 * @param Subject
	 * @throws Exception
	 */
	public void verifyMailInSentBox(AppiumDriver driver,String Subject) throws Exception {
		try {
			log.info("Verifying mail with Subject: " + Subject + " is present or not");
			NavigationDrawer navDrawer = new NavigationDrawer();
			
			navDrawer.navigateToSentBox(driver);
			
			pullToReferesh(driver);
			openMail(driver,Subject);
			log.info("Mail text is :"
					+ CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine")));
			verifySubjectOfMail(driver,Subject);
			Thread.sleep(5000);
			log.info("Mail Successfully Matched");
			navigateBack(driver);
			navigateBack(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Find Mail in Sent Box with Subject Title : " + Subject + "Due to "
					+ e.getLocalizedMessage());
		}
	}

	/**
	 * Method to forward email
	 * 
	 * @param Subject
	 * @param To
	 * @param CC
	 * @param BCC
	 * @param ComposeData
	 * @throws Exception
	 */
	public void forwardMail(AppiumDriver driver,String Subject, String To, String CC, String BCC, String ComposeData) throws Exception {
		composeMailAsFowarding(driver,To, CC, BCC, ComposeData);
		tapOnSend(driver);
	}

	/**
	 * Method to compose mail as forwarding
	 * 
	 * @param To
	 * @param CC
	 * @param BCC
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeMailAsFowarding(AppiumDriver driver,String To, String CC, String BCC, String ComposeData) throws Exception {
	
		log.info("Scroll down and tap on 'Forward' button");
		Thread.sleep(4000);
		scrollTo("Forward");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ForwardButton"));
		commonPage.waitForElementToBePresentByXpath(commonElements.get("ForwatdTitle"));
		composeOpenedMailBody(driver,To, CC, BCC, null, ComposeData);
	}

	/**
	 * Method to open email
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void openMail(AppiumDriver driver,String MailSubject) throws Exception {
		log.info("In openMail Open Mail with subject: " + MailSubject);
		
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		log.info("openMail MailXpath:"+MailXpath);
		Thread.sleep(2000);
		userAccount.pullToReferesh(driver);
		Thread.sleep(2000);
		waitForElementToBePresentByXpath(MailXpath); 
		log.info("In openMail subject found to be clicked..");
		CommonFunctions.searchAndClickByXpath(driver, MailXpath);
		log.info("In openMail Mail opened");
		waitForElementToBePresentByXpath(commonElements.get("ConversationUnreadIcon"));
	}
	
	public void verifyForUnreadMail(AppiumDriver driver) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkUnread"));
		waitForElementToBePresentByXpath(commonElements.get("UnreadMail"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("UnreadMail")),"Unread Mail not present");	
	}
	/**
	 * Method to verify Ad StarAd Functionality
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public boolean verifyForStarredMail(AppiumDriver driver, String methodName) throws Exception {
		boolean flag = false;
		log.info("******************* Start Ad  StarAd Functionality*******************");
		try {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionAddStar"));
			Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("MailActionRemoveStar")),"Mail not present");	
			flag = getScreenshotAndCompareImage(methodName);
			return flag;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(
					"Failed verifyForStarredMail ::::::" + e.getLocalizedMessage());
		}
	}

	public void verifyChangeLabelForMail(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Change labels"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("SelectLabel"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
		navigateBack(driver);
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ChecKMailLabel")),"Mail not present with given Label");
		//navigateBack(driver);
		//navigateBack(driver);
		openMenuDrawer(driver);
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionLabel"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail not present");	
		log.info("");
	}
	public void verifyMovingMailtoOtherFolder(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoveToOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoveFolder"));
		//commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
		openMenuDrawer(driver);
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveFolder"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail not present");	
		log.info("");
	}
	public void verifyImportantAndUnImportantMail(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Mark important"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MoveToOption").replace("Move to","Mark not important")),"Mark not Important not present");
		navigateBack(driver);
		//navigateBack(driver);
		log.info("");
	}
	
	public void verifyPrintingOfMail(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Print"));
		Assert.assertEquals(CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("SaveAsPDF")),commonElements.get("SaveAsPDFText"),"PDF Text message");
		navigateBack(driver);
		log.info("");
	}
	public void verifyMuteConversation(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Mute"));
		Assert.assertEquals(CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("MutedID")),commonElements.get("MutedText"),"Muted ID message");
		Assert.assertEquals(CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("UndoID")),commonElements.get("UndoText"),"Undo message");
		Assert.assertEquals(false,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail present");
		
		openMenuDrawer(driver);
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AllEmailsOption"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail not present");
		log.info("");
	}
	public void verifyReportSpamMail(AppiumDriver driver) throws Exception {
		log.info("");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Report spam"));
		log.info("");
		openMenuDrawer(driver);
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SpamOption"));
		Assert.assertEquals(true,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail not present");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailXpath"));
		Assert.assertEquals(CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("WarningTextID")),commonElements.get("WarningText"),"Spam warning message");
		navigateBack(driver);
		}
	public void verifyEmptySpam(AppiumDriver driver) throws Exception {
		log.info("");
		CommonFunctions.searchAndClickById(driver, commonElements.get("EmptySpamnow"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteSpam"));
		Assert.assertEquals(false,CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MailXpath")),"Mail not present");
		log.info("");
	}
	
	public void openFirstSearchMail(AppiumDriver driver) throws Exception {
		log.info("Open First Search Mail ");
		String MailXpath = commonElements.get("mailCount");
		CommonFunctions.searchAndClickByXpath(driver, MailXpath);
	}
	/**
	 * Method to insert InLine text
	 * 
	 * @param inLineText
	 * @throws Exception
	 */
	public void insertInLineText(AppiumDriver driver,String inLineText) throws Exception {
		
		log.info("Inserting an in-line text in mail body ");
		Thread.sleep(2000);
		hideKeyboard();
		CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("RespondInLineOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RespondInLineOption"));
		/*scrollDown(2);
		Thread.sleep(2000);
		Dimension windowSize = driver.manage().window().getSize();
		driver.tap(1, (int) ((windowSize.width) * 0.10), (int) ((windowSize.height) * 0.65), 500);
		driver.getKeyboard().sendKeys("\n" + inLineText + "\n");
		Thread.sleep(2000);
		hideKeyboard();*/
	}

	/**
	 * Method to compose email as reply
	 * 
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeMailAsReply(AppiumDriver driver ,String ComposeData, String CC) throws Exception {
		
		log.debug("Clicking on reply button");
		driver.scrollTo("Forward");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyButton"));
		commonPage.waitForElementToBePresentByXpath(commonElements.get("ReplyText"));
		composeOpenedMailBody(driver,null, CC, null, null, ComposeData);

	}

	/**
	 * Method to compose email as reply all
	 * 
	 * @param ComposeData
	 * @throws Exception
	 */
	public void composeMailAsReplyAll(AppiumDriver driver,String ComposeData) throws Exception {
		
		log.info("Composing mail as 'Reply All' with appropriate body data");
		driver.scrollTo("Forward");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyAllButton"));
		commonPage.waitForElementToBePresentByXpath(commonElements.get("ReplyAllTitle"));
		composeOpenedMailBody(driver,null, null, null, null, ComposeData);
	}

	/**
	 * Method to tap on Show quoted text
	 * 
	 * @throws Exception
	 */
	public void tapOnShowQuotedText(AppiumDriver driver) throws Exception {
		try {
			
			log.info("Tapping on 'Show quoted text'");
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ShowQuotedText")))
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowQuotedText"));
		} catch (Throwable e) {
			e.printStackTrace();
			log.info("Show Quoted Text Is Not Displayed");
			throw new Exception("Show quoted tex is not displayed");
		}
	}

	/**
	 * Method to tap on hide quoted text
	 * 
	 * @throws Exception
	 */
	public void tapOnHideQuotedText(AppiumDriver driver) throws Exception {
		try {
			
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HideQuotedText")))
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HideQuotedText"));
		} catch (Throwable e) {
			e.printStackTrace();
			log.info("Hide Quoted Text Is Not Displayed");
		}
	}

	/**
	 * Method to verify Inline text
	 * 
	 * @param inlineText
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyInlineTextIsPresent(String inlineText) throws IOException, InterruptedException {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			driver.scrollTo("Forward");
			scrollDown(2);
			String inlineTextXpath = commonElements.get("InlineText").replace("InlineText", inlineText);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, inlineTextXpath),
					"Inline text NOT displayed");
			log.debug("Inline text displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to verify inline text due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify Inline text
	 * 
	 * @param inlineText
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void verifyReplyInlineTextIsPresent(AppiumDriver driver, String inlineText) throws IOException, InterruptedException {
		try {
			driver.scrollTo("Forward");
			scrollDown(2);
			String inlineTextXpath = commonElements.get("InlineText").replace("InlineText", inlineText);
			log.info("inlineTextXpath: "+inlineTextXpath);
			Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, inlineTextXpath),
					"Inline text NOT displayed");
			log.info("Inline text displayed properly");
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to verify inline text due to :" + e.getLocalizedMessage());
		}
	}

	



	/**
	 * Method to tap on reply icon
	 * 
	 * @throws Exception
	 */
	public void tapOnInMailReplyIcon(AppiumDriver driver) throws Exception {
		
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyIcon"));
	}

	/**
	 * Method to tap on First email
	 * 
	 * @throws Exception
	 */
	public void tapOnFirstMail(AppiumDriver driver) throws Exception {
		
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
	}

	/**
	 * Method to Select mail from inbox for Archive or Delete operation
	 * 
	 * @throws Exception
	 */
	public void selectMail(AppiumDriver driver,String MailSubject) throws Exception {
		try {
			log.info("Selecting mail with subject: "+MailSubject);
		//	pullToReferesh(driver);
			Thread.sleep(1500);
			String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
			if(CommonFunctions.getSizeOfElements(driver, MailXpath)!=0){
			//if (CommonFunctions.isElementByXpathDisplayed(driver, MailXpath)) {
				AndroidElement element = (AndroidElement) driver.findElement(By.xpath(MailXpath));
				// Point p = element.getLocation();
				driver.tap(1, element, 500);
				log.info("Selected Mail with subject: "+MailSubject);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Select Mail: Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to Compare number of Mail Selected with the Count displayed in
	 * Header
	 * 
	 * @throws Exception
	 */
	public void verifyMailSelectionCount(AppiumDriver driver,String expectedCount) throws Exception {
		try {
			log.info("Verifying mail selection count");
			
			//Thread.sleep(1000);
		//	CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SelectedMailSenderImage"));
			Thread.sleep(2000);
			
			
	/*		List<WebElement> elements = driver.findElements(By.xpath(commonElements.get("SelectedMailSenderImage")));
			
			List<WebElement> elements_index = driver.findElements(By.xpath("//android.view.View[@index='1']"));
			List<WebElement> elements_class_desc=driver.findElements(By.xpath("//*[@class = 'android.view.View' and contains(@content-desc,'Double tap to deselect this conversation')]"));
			List<WebElement> elements_index_desc=driver.findElements(By.xpath("//android.view.View[@index = '1' and contains(@content-desc,'Double tap to deselect this conversation')]"));
			
			
			log.info("Mail Selection count ***************"+elements.size());
			log.info("Mail Selection Direct count ***************"+elements_index.size()+ "Class : "+ elements_class_desc.size()+"Index : "+elements_index_desc.size() );
			
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SelectedMailActionBarTitle"));
			*/
			
			
			int HeaderCount = Integer
					.parseInt(driver.findElement(By.xpath(commonElements.get("SelectedMailActionBarTitle"))).getText());
			
			
			
			if (!(HeaderCount==Integer.parseInt(expectedCount))) {
				throw new Exception("Failed to match count : Header Count -> " + HeaderCount + "Expected Mail Selected count -> "
						+ expectedCount);

			} else {
				log.info("Selected Mail Count Successfully Matched");
			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Match Mail Selected Count : Due to :" + e.getLocalizedMessage());
		}
	}
	
	public void verifyMailSelectionCount1(int expectedCount) throws Exception {
		try {
			int count=0;
			log.debug("Verifying mail selection count");
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			Thread.sleep(2000);
			int HeaderCount = Integer
					.parseInt(driver.findElement(By.xpath(commonElements.get("SelectedMailActionBarTitle"))).getText());
			
			
			
			if (!(HeaderCount==expectedCount)) {
				throw new Exception("Failed to match count : Header Count -> " + HeaderCount + "Expected Mail Selected count -> "
						+ expectedCount);

			} else {
				log.debug("Selected Mail Count Successfully Matched");
			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to Match Mail Selected Count : Due to :" + e.getLocalizedMessage());
		}
	}

	public void verifyContentPresent(AppiumDriver driver)throws Exception{
		try{
			//Thread.sleep(5000);
			log.info("Verifying mails are synced");
			int Start =1, End = 5;
			while(Start<=End){
				Thread.sleep(6000);;
				int x = CommonFunctions.getSizeOfElements(driver, commonElements.get("SelectMailImage"));
				if(x>0)
					break;
				
				Start++;
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SelectMailImage"))!=0){
				log.info("Mails are synced");
			}else{
				throw new Exception("Mails are not synced");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to verify Mail is present
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifyMailIsPresent(AppiumDriver driver,String MailSubject) throws Exception {
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		if(CommonFunctions.isElementByXpathDisplayed(driver, MailXpath)){
			log.info("In IF.verifyMailIsPresent IMail is present");
		}else{
		
		pullToReferesh(driver);
		log.info("Verifying the mail is present");
		int count=1;
		while(CommonFunctions.isElementByXpathNotDisplayed(driver, MailXpath)==true)
		{
			scrollDown(1);
			count++;
			if(count>10)
				throw new Exception("Mail With Subject '" + MailSubject + "' was not found");
		}
		scrollUp(count);
	}}
	
	/**
	 * Method to verify Mail not present
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifyMailNotPresent(AppiumDriver driver,String MailSubject) throws Exception {
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		if(CommonFunctions.isElementByXpathNotDisplayed(driver, MailXpath)){
			log.info("Mail is present");
		}else{
		
		pullToReferesh(driver);
		log.info("Verifying the mail not present");
		int count=1;
		while(CommonFunctions.isElementByXpathDisplayed(driver, MailXpath)==true)
		{
			scrollDown(1);
			count++;
			if(count>10)
				throw new Exception("Mail With Subject '" + MailSubject + "' was found");
		}
		scrollUp(count);
	}}

	/**
	 * Method to verify whether inbox is rendered
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean isInboxDisplayed(AppiumDriver driver, String inboxType) throws Exception {
		
		boolean value = CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get(inboxType));
		return value;
	}

	/**
	 * Method to open Mail Using Search
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void openMailUsingSearch(AppiumDriver driver ,String MailSubject) throws Exception {
		try {
			log.info("Open mail with subject : " + MailSubject);
			try{
			
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchIconXpath"));
			}catch(Exception e){
				CommonFunctions.searchAndClickById(driver, commonElements.get("SearchIcon"));
			}
			
			Thread.sleep(1000);
			/*
			//added by Venkat on 7/2/2019 for Go Device : starts
			try {
				if(testBedName.contains("Go")){
					CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.gm.lite:id/search']");
				}
			} catch (Exception e) {
			}
			//added by Venkat on 7/2/2019 for Go Device : end
			 
			 */
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("searchIDXapth"), MailSubject);
			
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("InboxMail").replace("MailSubject", MailSubject));

			//Thread.sleep(10000);

			Thread.sleep(4000);

			commonPage.waitForElementToBePresentByXpath(commonElements.get("OutlookMail").replace("MailSubject", MailSubject));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OutlookMail").replace("MailSubject", MailSubject));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to openMail Using Search due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to open another mail using search
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void openAnotherMailUsingSearch(String MailSubject) throws Exception {
		try {
			log.info("Open another mail with subject: " + MailSubject);
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			driver.findElement(By.id(commonElements.get("searchID"))).clear();
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("searchID"), MailSubject);
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(5000);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to openMail Using Search due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to open First Mail
	 * 
	 * @param subject
	 * @throws Exception
	 */
	public void openFirstMail(AppiumDriver driver,String subject) throws Exception {
		try {
			
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("FirstMailOne").replace("FirstMail", subject));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("openFirstMail failed due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Undo Swipe Operation
	 * 
	 * @throws Exception
	 */
	public void searchForMail(AppiumDriver driver,String SearchKey) throws Exception {
		log.info("In IF.searchForMail Searching for a mail with the subject : "+SearchKey);
		pullToReferesh(driver);
		Thread.sleep(1000);
		log.info("In IF.searchForMail Looking for search button");
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("searchXpath"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchXpath"));
			driver.getKeyboard().sendKeys(SearchKey);
			log.info("In IF.searchForMail Clicking on search");
		}else if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SearchIconXpath"))) {
	//	if(driver.findElements(By.id("com.google.android.gm:id/search")).size()!=0){
			log.info("In IF.searchForMail Search Icon is displayed");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchIconXpath"));

			driver.getKeyboard().sendKeys(SearchKey);
		} else if (!CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("SearchtextBox"))) {
			driver.findElement(By.id(commonElements.get("SearchtextBox"))).sendKeys(SearchKey);
		}
		((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
		log.info("In IF.searchForMail Entered data to search for results");
		commonPage.waitForElementToBePresentByXpath(commonElements.get("SearchPageResultslable"));
	}

	/**
	 * Method to swipe To Next Mail
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void swipeToNextMail(AppiumDriver driver,String MailSubject) throws Exception {
		
		log.info("Swiping to Next Mail");
		Thread.sleep(5000);
		String CurrentSubjectLine = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("SubjectLine"));
		Dimension windowSize = driver.manage().window().getSize();
		driver.swipe((windowSize.width) - 20, (int) ((windowSize.height) * 0.50), 80,
				(int) ((windowSize.height) * 0.50), 1000);
		String SubjectLineAfterSwipe = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("SubjectLine"));
		if (CurrentSubjectLine.equalsIgnoreCase(SubjectLineAfterSwipe)) {
			throw new Error("Unable to swipr to Next Mail from current Mail with Subject :" + MailSubject);
		} else {
			log.debug("Swipe successful");
		}
	}

	/**
	 * Method to swipe To Delete Or Archive Mail
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void swipeToDeleteOrArchiveMail(AppiumDriver driver,String MailSubject) throws Exception {
		try {
			pullToReferesh(driver);
			log.info("Swipe right of left to archive the mail with subject" + MailSubject);
			Thread.sleep(2000);
			pullToReferesh(driver);
			Dimension windowSize = driver.manage().window().getSize();
			String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
			log.info(MailXpath);
			/*if(!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get(MailXpath))){
				log.info("Scrolling down to find the mail with the subject "+MailSubject);
				scrollDown(1);
				Thread.sleep(2000);
				if(!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get(MailXpath))){
					scrollUp(1);
					Thread.sleep(2000);
				}
				
			}*/
			//waitForElementToBePresentByXpath(MailXpath);
			
			WebElement ele = driver.findElement(By.xpath(MailXpath));
			int y = ele.getLocation().y;
			driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
			Thread.sleep(2000);
			log.info("mail is swiped");
			/*waitForElementToBePresentByXpath(commonElements.get("SwipeDeleteArchiveText"));
			waitForElementToBePresentByXpath(commonElements.get("SwipeUndo"));*/
			/*Assert.assertTrue(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SwipeUndo")),
					"Unable to locate Undo Button");
*/			String delete = "Deleted";
			String archive = "Archived";
			String Actual = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("SwipeDeleteArchiveText"));
			if (Actual.toLowerCase().contains(delete.toLowerCase())) {
				log.info("Swipe Delete Successful");
			} else if (Actual.toLowerCase().contains(archive.toLowerCase())) {
				log.info("Swipe Archive Successful");
			} else {
				throw new Exception("Unable to find Archive or Delete Tag after Swipe : " + MailSubject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("failed Due to :" + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to swipe To Delete Or Archive Mail
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public void swipeToDeleteOrArchiveMailNew(AppiumDriver driver,String MailSubject, String Action) throws Exception {
		try {
			log.info("In IF.swipeToDeleteOrArchiveMailNew Swipining " + MailSubject);
			Thread.sleep(2000);
			pullToReferesh(driver);
			Dimension windowSize = driver.manage().window().getSize();
			String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
			log.info("In IF.swipeToDeleteOrArchiveMailNew :"+MailXpath);
			
			WebElement ele = driver.findElement(By.xpath(MailXpath));
			int y = ele.getLocation().y;
			log.info("In IF.swipeToDeleteOrArchiveMailNew :"+y);
			if(Action.equalsIgnoreCase("Delete")){
				driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
				Thread.sleep(2000);
				log.info("In IF.swipeToDeleteOrArchiveMailNew mail is swiped to delete");
			}else if(Action.equalsIgnoreCase("Archive")){
				
				driver.swipe((y + 100), (y + 50),(windowSize.width) - 20,  80, 1000);
				Thread.sleep(2000);
				log.info("In IF.swipeToDeleteOrArchiveMailNew mail is swiped to archive");
				
			}
			
			String delete = "Deleted";
			String archive = "Archived";
			String Actual = CommonFunctions.searchAndGetTextOnElementByXpath(driver,commonElements.get("SwipeDeleteArchiveText"));
			if (Actual.toLowerCase().contains(delete.toLowerCase())) {
				log.info("In IF.swipeToDeleteOrArchiveMailNew Swipe Delete Successful");
			} else if (Actual.toLowerCase().contains(archive.toLowerCase())) {
				log.info("In IF.swipeToDeleteOrArchiveMailNew Swipe Archive Successful");
			} else {
				throw new Exception("Unable to find Archive or Delete Tag after Swipe : " + MailSubject);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("failed Due to :" + e.getLocalizedMessage());
		}
	}


	/**
	 * Method to undo Swipe Delete Archive Operation
	 * 
	 * @throws Exception
	 */
	public void undoSwipeDeleteArchiveOperation(AppiumDriver driver) throws Exception {
		log.info("Tapping on UNDO button to undo archiving");
	
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SwipeUndo"));
	}

	/**
	 * Method to check sender image check box
	 * 
	 * @throws Exception
	 */
	public void checkSenderImageCheckBox(AppiumDriver driver) throws Exception {
		try {
			
			log.info("Checking Sender image checkbox");
			if (CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("GeneralSettingsSenderImageCheckBox"))) {
				WebElement element = driver
						.findElement(By.xpath(commonElements.get("GeneralSettingsSenderImageCheckBox")));
				if (element.getAttribute("checked").equalsIgnoreCase("false")) {
					element.click();
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to navigate to General Settings : Due to :" + e.getLocalizedMessage());
		}
	}

	public String test() {
		return "AAAAAAAAAAAAa";

	}

	/**
	 * Method to tap on send button
	 * 
	 * @throws Exception
	 */
	public void tapOnSend(AppiumDriver driver) throws Exception {
		try {
			log.info("Tapping on 'Send' button ");
		//	AppiumDriver driver = map.get(Thread.currentThread().getId());
		//	commonPage.waitForElementToBePresentByXpath(commonElements.get("SendButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			log.info("Tapped on send button");
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			//verifySharingOptionPopUpIsPresent();
			//waitForElementToBePresentById(commonElements.get("OverFlowMenu"));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to send and navigate back due to :" + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to tap on send button sharing pop up
	 * 
	 * @throws Exception
	 */
	public void tapOnSendSharingAlert(AppiumDriver driver) throws Exception {
		try {
			
			log.info("Tapping on 'Send' button ");
		//	AppiumDriver driver = map.get(Thread.currentThread().getId());
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SharingOptionsSendButton"));
			log.info("Tapped on send button");
			//verifySharingOptionPopUpIsPresent();
			//waitForElementToBePresentById(commonElements.get("OverFlowMenu"));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to send and navigate back due to :" + e.getLocalizedMessage());
		}
	}
	

	/**
	 * Method to tap in send button and navigate back
	 * 
	 * @throws Exception
	 */
	public void tapOnSendAndNavigateBack(AppiumDriver driver) throws Exception {
		try {
			log.info("Tapping on Send and Navigate back");
			//AppiumDriver driver = map.get(Thread.currentThread().getId());
			Thread.sleep(2000); //// added by Venkat on 27/12/2018  for Pixel3
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			log.info("Clicked on Send button");
			Thread.sleep(3000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			navigateBack(driver);
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to send and navigate back due to :" + e.getLocalizedMessage());
		}
	}
	
	/**Method to verify the mail that is sent in draft folder
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyDraftMail(AppiumDriver driver, String subject)throws Exception{
		try{
			pullToReferesh(driver);
			log.info("Verifying mail is present in drafts folder after sending the mail");
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("DraftsOption"));
			log.info("Navigated to drafts folder");
			pullToReferesh(driver);
			if(driver.findElements(By.xpath("//android.view.View[contains(@content-desc,'"+subject+"')]")).size()!=0){
				throw new Exception("The mail is in draft folder and it is not sent");
			}
			log.info("********** Verified that the sent mail "+subject+" is not in draft folder *************");
		}catch(Throwable e){
			e.printStackTrace();
			throw new Exception("Unable to verify the draft mail");
		}
	}
	
	
	/**
	 * Method to verify subject in compose email
	 * 
	 * @param MailSubject
	 * @throws Exception
	 */
	public static void verifySubjectInComposeMail(String MailSubject) throws Exception {
		String Actual = null;
		try {
			String xpath = "//android.widget.EditText[contains(@text,'" + MailSubject + "')]";
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			Actual = CommonFunctions.searchAndGetTextOnElementByXpath(driver, xpath);
			Assert.assertTrue(Actual.contains(MailSubject),
					"Failed to match text in Mail : Actual :" + Actual + " Expected :" + MailSubject);
			// Thread.sleep(700);
		} catch (Throwable e) {
			e.printStackTrace();
			Assert.fail("Failed to match text in Mail : Actual :" + Actual + " Expected :" + MailSubject);
		}
	}
	
	public void navigateToPrimary(AppiumDriver driver)throws Exception{
		try{
			 log.info("Tapping on the hamburger");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			 log.info("Checking for the primary option");
			 Thread.sleep(2000);
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryInboxOption"))!=0){
				 log.info("Navigating to Primary folder");
				 scrollUp(1);
				 Thread.sleep(800);
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
			 }else{
				 log.info("Primary option is not available and hence scrolling up");
				 scrollTo("Primary");
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryInboxOption"))!=0){
					 Thread.sleep(2000);
					 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
					 log.info("Clicked on the primary option folder");
				 }
			 }
				 	 CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PrimaryTitle"));
					 log.info("Navigated to Primary folder");
					 pullToReferesh(driver);
					 Thread.sleep(2000);
		}catch(Exception e){
			throw new Exception("Unable to navigate to Primary folder");
		}
	}
	
	
	
	 /**Method to delete mails from primary inbox folder
	  * 
	  * @param driver
	  * @throws Exception
	  * @author Phaneendra
	  */
	 public void deleteMails(AppiumDriver driver) throws Exception{
		/* 
		 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"DeleteMail.bat";;
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed Bat file");
		*/ 
		 Thread.sleep(4000);
		 try{
			 	 pullToReferesh(driver);
			 	 navigateToPrimary(driver);
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SelectMailImage"))!=0){
					 log.info("Selecting few mails");
					 int count = 0;
					 List<WebElement> selectMailsToDelete = driver.findElements(By.xpath(commonElements.get("SelectMailImage")));
					 for(WebElement ele : selectMailsToDelete){
						 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
						 count++;
						 if(count==1)
							 break;
					 }
					 log.info("Deleting selected mails");
					 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
					Thread.sleep(800);
					 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
						 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					 }
					 log.info("Few mails are deleted");
				 }
			 		
	 }catch(Exception e){
		 throw new Exception("Unable to delete mails "+e.getLocalizedMessage());
	 }
	 }
	 
	 /**Method to spam mails from primary inbox folder
	  * 
	  * @param driver
	  * @throws Exception
	  * @author Phaneendra
	  */
	 public void spamMails(AppiumDriver driver, String emailId) throws Exception{
		 try{
			 log.info("Tapping on the hamburger");
		/*	 
			 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"DeleteMail.bat";
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file");
			 Thread.sleep(4000);
		*/	 
				CommonFunctions.smtpMailGeneration(emailId,"TestingDeleteMail", "ZoomOut.html", null);
				
			 pullToReferesh(driver);
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			 log.info("Checking for the primary option");
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryInboxOption"))!=0){
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
				 log.info("Navigated to Primary folder");
			 }else{
				 log.info("Primary option is not available and hence scrolling up");
				 scrollUp(1);
				 Thread.sleep(800);
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryInboxOption"))!=0){
					 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
					 log.info("Clicked on the primary option folder");
					 CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("PrimaryTitle"));
					 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SelectMailImage"))!=0){
						 log.info("Selecting few mails");
						 pullToReferesh(driver);
						 int count = 0;
						 List<WebElement> selectMailsToDelete = driver.findElements(By.xpath(commonElements.get("SelectMailImage")));
						 for(WebElement ele : selectMailsToDelete){
							 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
							 count++;
							 if(count==1)
								 break;
						 }
						 log.info("Marking as spam for selected mails");
						 //CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
						 reportSpamMail(driver);
						 log.info("Few mails are spamed");
					 }
				 }
			 }
		 }catch(Exception e){
			 throw new Exception("Unable to spam mails "+e.getLocalizedMessage());
		 }
		 }
	
	/**Method to delete the mail from primary folder
	 * 
	 * @param driver
	 * @param MailSubject
	 * @throws Exception
	 * @author Phaneendra
	 */
	 public void deleteMailPrimary(AppiumDriver driver, String MailSubject, String emailId)throws Exception{
		 
		CommonFunctions.smtpMailGeneration(emailId,MailSubject, "ZoomOut.html", null);
		 Thread.sleep(4000);
		 pullToReferesh(driver);
		 try{
			 Thread.sleep(2000);
			 pullToReferesh(driver);
			 log.info("Waiting for the mail to be found");
			 commonPage.waitForElementToBePresentByXpath(commonElements.get("DeleteEmail").replace("File", MailSubject));
			 log.info("Email with subject " +MailSubject+" is found");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteEmail").replace("File", MailSubject));
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
			 log.info("Clicked on Delete icon");
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			 }
			 log.info("Clicked on alert for deleting the conversation mail");
			
		 }catch(Exception e){
			 throw new Exception("Unable to delete mail from primary "+e.getStackTrace());
		 }
	 }
	 
	/**Method to verify the snack bar is displayed
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	 public void verifySnackBarDeleted(AppiumDriver driver)throws Exception{
		 try{
			 Thread.sleep(1000);
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SnackerBarDeleted"))!=0 &&
					 CommonFunctions.getSizeOfElements(driver, commonElements.get("SwipeUndo"))!=0){
				 log.info("Snack bar is displayed");
			 }else{
				 throw new Exception("Snack bar is not verified");
			 }
		 }catch(Exception e){
			 throw new Exception("Unable to verify the snack bar after deleting the mail "+e.getStackTrace());
		 }
	 }
	 
	 
	/**Method to verify the mails are deleted from Trash and Spam
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void emptyTrashOrSpamNow(AppiumDriver driver, String Bin)throws Exception{
		log.info("Deleting the mails from Trash/Spam folders");
		try{
			Thread.sleep(1000);
			if(Bin.equals("Trash") && (CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptyTrashNow"))!=0)){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmptyTrashNow"));
				log.info("Clicked on Empty Trash Now");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteSpam"));
				log.info("Clicked on Delete confirmation for Trash");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("NothingInTrash"));
			}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptySpamnow"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmptySpamnow"));
				log.info("Clicked on Empty Spam Now");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteSpam"));
				log.info("Clicked on Delete confirmation for Spam");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("NothingInSpam"));
		}
			/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptyTrashNow"))!=0 || 
					CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptySpamnow"))!=0){
				throw new Exception("Still mails are displayed in Trash/Spam folder after deleting the mails");
			}
			*/log.info("Trash/Spam folder is empty and all mails are deleted");
		}catch(Exception e){
			throw new Exception("Unable to delete the mails due to "+e.getLocalizedMessage());
		}
	}
	
	
	/**Method to report the mails as spam
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void reportSpamMail(AppiumDriver driver) throws Exception {
		try{
			log.info("Reporting mail as spam in Primary Inbox");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption").replace("Move to","Report spam"));
			log.info("Selected mails are reported as spam");
			
		}catch(Exception e){
			throw new Exception("Unable to report mail as spam due to "+e.getLocalizedMessage());
		}
		}
	
	/**Method to verify the signatute text in compose mail
	 * 
	 * @param driver
	 * @param AccountA
	 * @param AccountB
	 * @param AccountC
	 * @throws Exception
	 * 
	 * @author Phaneendra
	 */
	public void verifySignatureInCompose(AppiumDriver driver, String AccountA, String AccountB, String AccountC, String SignatureTextA, String SignatureTextC)throws Exception{
		try{
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
					commonPage.waitForElementToBePresentByXpath(commonElements.get("ComposeTitle"));
					Thread.sleep(1200);
					CommonFunctions.searchAndClickById(driver, commonElements.get("FromUserID"));
					List<WebElement> fromAccounts = driver.findElements(By.id(commonElements.get("FromUserID")));

					for(WebElement e : fromAccounts){

						String acctName = e.getText();
						log.info("Clicking on the account "+acctName);
						Thread.sleep(800);
						if(CommonFunctions.getSizeOfElements(driver, "//android.widget.TextView[@text='"+acctName+"']")!=0){
						driver.findElement(By.xpath("//android.widget.TextView[@text='"+acctName+"']")).click();
						}
					String fromAddress = driver.findElement(By.id(commonElements.get("FromUserID"))).getText();
					log.info("From Address displayed "+fromAddress);
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					if(fromAddress.equals(AccountA)){
						log.info("Verifying signature for "+AccountA);
						CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", SignatureTextA));
							log.info("Signature is appended for the account A "+AccountA);
					}else if(fromAddress.equals(AccountB)){
						log.info("Verifying signature for "+AccountB);
						CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", ""));
							log.info("Signature is not appended for the account C "+AccountB);
						
					}else if(fromAddress.equals(AccountC)){
						log.info("Verifying signature for "+AccountC);
						CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", SignatureTextC));
							log.info("Signature is appended for the account C "+AccountC);
					}
					CommonFunctions.searchAndClickById(driver, commonElements.get("FromUserID"));
				}
			}else{
				verifyGmailAppInitState(driver);
			}
		}catch(NoSuchElementException e){
			throw new Exception("Unable to verify the signature");
		}
	}
	
	public void sendEmail(AppiumDriver driver, String AccountA, String AccountC, String SubjectText, String ComposeText, String SignatureTextA)throws Exception{
		try{
			log.info("Entering the To, Subject and compose");
		
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSelect").replace("Email", AccountA));
			int cnt = 0;
			while(cnt++ <= 2){
				try{
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSelect").replace("Email", AccountA));
					break;
				}catch(Exception e){
					scrollDown(1);
				}
			}
			Thread.sleep(1500);// added by Venkat on 22-Nov-2018: end
			CommonFunctions.searchAndSendKeysByIDNoClear(driver, commonElements.get("ToField"), AccountC+" ");
			Thread.sleep(1500);// added by Venkat on 22-Nov-2018: end
			CommonFunctions.searchAndSendKeysByIDNoClear(driver, commonElements.get("EmailSubject"), SubjectText);
			
			log.info("Sending Mail");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			Thread.sleep(1500);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			log.info("Mail Sent");
			Thread.sleep(3000);
			//userAccount.switchMailAccount(driver, commonElements.get("EmailSelect").replace("Email", AccountC));
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList"))==0){
				scrollUp(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			}else{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			}
			log.info("Navigating to Account C");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSelect").replace("Email", AccountC));
			*///driver.navigate().back();
			if(!CommonFunctions.isAccountAlreadySelected(AccountC))
			userAccount.switchMailAccount(driver, AccountC);
			
			pullToReferesh(driver);
			log.info("Verifying for the Email with subject "+ SubjectText);
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("EmailSubjectText").replace("Subject", SubjectText))){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", SubjectText));
			}
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	
	
	/**Method to send Email and verify the signature
	 * 
	 * @param driver
	 * @param AccountA
	 * @param AccountC
	 * @param SubjectText
	 * @param ComposeText
	 * @param SignatureTextA
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void sendEmailAndVerify(AppiumDriver driver, String AccountA, String AccountC, String SubjectText, String ComposeText, String SignatureTextA)throws Exception{
		try{
			log.info("Sending email");
			sendEmail(driver, AccountA, AccountC, SubjectText, ComposeText, SignatureTextA);
		//	driver.findElement(By.xpath("//android.view.View[contains(@content-desc,'"+SubjectText+"')]")).click();
			
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", SignatureTextA));
			log.info("Verified the signature of the Account A at reciepent end Account C");
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception(e);
		}
	}
	
	/**Method to verify mail without No Signature
	 * 
	 * @author Phaneendra
	 */
	public void sendEmailAndVerifyNoSignature(AppiumDriver driver, String AccountA, String AccountC, String SubjectText, String ComposeText, String SignatureTextA)throws Exception{
		try{
			 log.info("Sending mail to Account C from AccountA with no Signature");

			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ComposeMailButton"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			}else{
				driver.navigate().back();
				Thread.sleep(1200);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			}
			CommonFunctions.searchAndClickById(driver, commonElements.get("FromUserID"));
			sendEmail(driver, AccountA, AccountC, SubjectText, ComposeText, SignatureTextA);
			log.info("Verifying the accountA has no signature");
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", ""));
			log.info("Verified the signature of the Account A at reciepent end Account C with no Signature");
		}catch(Exception e){
			throw new Exception("Failed to send email when signature is set to Off "+e.getMessage());
		}
	}
	
	/**Method to verify the deleted mail in trash folder
	 * 
	 * @param driver
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifyMailTrashFolder(AppiumDriver driver, String MailSubject)throws Exception{
		try{
			commonPage.waitForElementToBePresentByXpath(commonElements.get("EmptyTrashNow"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DeleteEmail").replace("File", MailSubject))!=0){
					log.info("Mail is deleted");
			}else{
				throw new Exception("Unable to find the deleted mail");
			}
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to verify the mail in trash folder");
		}
	}
	
	
	/**Method to verify the link is opened in Gmail app or browser after setting the option to checked/unchecked
	 * 
	 * @param driver
	 * @param ChromeCustomSubject
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyLinkOpenedInGmailApporBrowser(AppiumDriver driver, String ChromeCustomSubject, String View)throws Exception{
		/*
			 String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"ChromeCustom.bat";
			 log.info("Executing the bat file to generate and delete it "+command);
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file");
		*/
		
		
		
			 pullToReferesh(driver);
			 Thread.sleep(4000);
		try{
			 pullToReferesh(driver);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", ChromeCustomSubject));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", ChromeCustomSubject));
			log.info("Clicked on the custom mail");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SupportGoogleLink"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SupportGoogleLink"));
			log.info("Clicked on the support Google Link");
			
			if(View.equals("GmailApp")){
			log.info("Verifying whether the link is opened in Gmail App");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("BackButtonClose"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("GoogleHelpTitleBar"));
			log.info("Verified Back button and Google Help title bar is displayed and the Link is opened in Gmail App only");
			}else if(View.equals("Browser")){
				log.info("Verifying the link is opened in browser");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("ChromeTabSwitcher"));
				//commonPage.waitForElementToBePresentById(commonElements.get("BrowserTabSwitcher"));
				log.info("Verified the link is opened in browser");
			}
			driver.navigate().back();
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to verify the mail opened in Gmail app or not");
		}
	}
	
	/**Method to execute the adb shell command through process builder
	 * 
	 * @param openMultiWindow1
	 * @param openMultiWindow2
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void adbExecuteCommand(String[] openMultiWindow1, String[] openMultiWindow2)throws Exception{
		try{
			log.info("Navigating to");
			ProcessBuilder pb;
			Process p;
			pb = new ProcessBuilder(openMultiWindow1);
			pb.redirectErrorStream(true);
			p = pb.start(); 
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
			    log.info(line);
			    if(line.contains("does not exist")){
			    	pb = new ProcessBuilder(openMultiWindow2);
			    	p = pb.start();
			    }
			}
			p.waitFor();
			in.close();
			Thread.sleep(4000);
	/*		commonPage.waitForElementToBePresentByXpath(commonElements.get("SettingsTitlePhone"));
			log.info("Settings Screen is displayed");
	*/	}catch(Exception e){
			throw e;
		}
	}
	
	/**Method to call and exeucte the adb command while doing parallel execution
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void enableMultiWindow(AppiumDriver driver)throws Exception{
		try{
			String [] longpressOverview = new String[]{"adb", "shell", "input", "touchscreen", "swipe 844 1857 844 1857 2000"};
			String [] openMultiWindow1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.settings/com.android.settings.GridSettings"};
			String[] openMultiWindow2 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.settings/com.android.settings.Settings"};
			log.info("Executing adb commands");
			adbExecuteCommand(longpressOverview, openMultiWindow2);
		}catch(Exception e){
			throw e;
		}
	}
	
	/**Method to generate the bat file
	 *  
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void executeBatFile()throws Exception{
		try{
			 String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SwipeDownRefresh.bat";
			 log.info("Executing the bat file to generate a mail for testing Swipe Down to refresh functionality "+command);
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed the bat file to generate a mail");
			 Thread.sleep(4000);
		}catch(Exception e){
			throw new Exception("Unable to execute the bat file to generate instant mail "+e.getStackTrace());
		}
	}
	
	
	/**Method to verify the swipe down to refresh functionality
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifySwipeDownToRefresh(AppiumDriver driver, String EmailSubject)throws Exception{
		try{
			//executeBatFile(); // Enabled by Venkat on 14/11/2018 //Commented by Venkat on 18/04/2019
			//pullToReferesh(driver);
			log.info("Verifying the mail is present before PTR and after PTR");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))==0){
				log.info("Email with subject "+EmailSubject+" is not found");
				Thread.sleep(6000);
				pullToReferesh(driver);
				commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))!=0){
					log.info("Email is displayed after refreshing by swiping down");
				}else{
					throw new Exception("Mail is not displayed due to sync issue or network slow connectivity");
				}
			}
		}catch(Exception e){
			throw new Exception("Unable to verify the swipe down too refresh functionality "+e.getStackTrace());
		}
	}
	
	/**Method to move the mail to other folder
	 * 
	 * @param driver
	 * @param EmailSubject
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void moveMailToFolder(AppiumDriver driver, String EmailSubject, String FolderName, String emailId)throws Exception{
		/*
		 * String path = System.getProperty("user.dir"); String command =
		 * path+java.io.File.separator+"Mail"+java.io.File.separator+"MoveMailTo.bat";
		 * log.
		 * info("Executing the bat file to generate a mail and move to other folder "
		 * +command); Process p = new ProcessBuilder(command).start();
		 * log.info("Executed the bat file to generate a mail");
		 */		 
		
		CommonFunctions.smtpMailGeneration(emailId, EmailSubject, "MoveMailToFolderTest.html", null);

		 Thread.sleep(4000);
		 try{
			 pullToReferesh(driver);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))==0){
					log.info("Email with subject "+EmailSubject+" is not found");
					pullToReferesh(driver);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
					if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))!=0){
						log.info("Email is displayed after refreshing by swiping down");
					}else{
						throw new Exception("Mail is not displayed due to sync issue or network slow connectivity");
					}
					
				}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))!=0){
					log.info("Mail already exists");
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
				log.info("Clicked and opened the email");
				Thread.sleep(800);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
				Thread.sleep(1000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MoveToOption"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption"));
					commonPage.waitForElementToBePresentByXpath(commonElements.get("MoveToPopUp"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabel").replace("Label", FolderName));
					log.info("Clicked on folder label "+FolderName);
				}
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("NavigateUp"))!=0){
					 driver.navigate().back();
				 }
				
		 }catch(Exception e){
			 throw new Exception("Unable to move mail to selected folder "+e.getStackTrace());
		 }
	 }
	
	public void moveMailToPrimary(AppiumDriver driver, String EmailSubject,String FolderName)throws Exception{
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		Thread.sleep(800);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MoveToOption"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOption"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("MoveToPopUp"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SystemLabel").replace("Label", FolderName));
			log.info("Clicked on folder label "+FolderName);
		}
		driver.navigate().back();
	}
	
	
	
	public void verifyMailPresent(AppiumDriver driver, String EmailSubject)throws Exception{
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))==0){
			log.info("Email with subject "+EmailSubject+" is not found");
			Thread.sleep(3000);
			pullToReferesh(driver);
			Thread.sleep(2000);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))!=0){
				log.info("Email is displayed after refreshing by swiping down");
			}else{
				throw new Exception("Mail is not displayed due to sync issue or network slow connectivity");
			}
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			log.info("Clicked and opened the email");
		}else{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			log.info("Clicked and opened the email");
		}
	}
	
	/**Method to archive mail
	 * 
	 * @param driver
	 * @param EmailSubject
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void archiveMail(AppiumDriver driver, String EmailSubject, String emailId)throws Exception{
		
		/*
		 * String path = System.getProperty("user.dir"); String command =
		 * path+java.io.File.separator+"Mail"+java.io.File.separator+"ArchiveMail.bat";
		 * log.
		 * info("Executing the bat file to generate a mail and move to other folder "
		 * +command); // Process p = new ProcessBuilder(command).start();
		 * Runtime.getRuntime().exec(command);
		 * log.info("Executed the bat file to generate a mail");
		 */		 
		
		CommonFunctions.smtpMailGeneration(emailId,EmailSubject, "None.html", null);
		
		 Thread.sleep(4000);
		try{
			pullToReferesh(driver);
			verifyMailPresent(driver, EmailSubject);
			Thread.sleep(1200);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
			Thread.sleep(1200);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			Thread.sleep(800);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ArchivedSnackBar"))!=0){
				log.info("Archived snack bar is displayed and the mail is archived");
			}
		}
		catch(Exception e){
			throw new Exception("Unable to archive the mail "+e.getLocalizedMessage());
		}
	}
	
	/**Method verify mail is saved as pdf after clicking on print/print all option
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyMailSaveAsPDF(AppiumDriver driver, String fileName, String Kind)throws Exception{
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
			Thread.sleep(4000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			log.info("Clicked on more options");
			Thread.sleep(1500);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrintOption"))!=0){
				log.info("Print option is found");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrintOption"));
				log.info("Clicked on print option");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SaveAsPdf"));
				log.info("Save as PDF page is displayed");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrintButton"));
				log.info("Clicked on the button to save the file as pdf");
				Thread.sleep(2000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EditNameField"))!=0) {
					commonPage.waitForElementToBePresentByXpath(commonElements.get("EditNameField"));
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("EditNameField"), fileName);
					log.info("Save file name entered");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Save"));
					log.info("Edited name as given and saved");
				}else
				{
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Done"));
				}
				Thread.sleep(2500);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClickMoreOptionsCV"));
				Thread.sleep(800);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrintOption"));
				driver.navigate().back();
			//	openDriveAndVerify(driver, fileName, Kind);
			}
			//launchGmailApp(driver);
		}catch(Exception e){
			throw new Exception("Unable to save mail as pdf "+e.getLocalizedMessage());
		}
	}

	public void openDriveAndVerifyAttachments(AppiumDriver driver)throws Exception{
		try{
			verifyGoogleDriveAppInitState(driver);
			Thread.sleep(5000);
			//pullToReferesh(driver);
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchDriveIcon"));
			Thread.sleep(2000);
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("SearchTextDrive"), fileName);
			Thread.sleep(4000);*/
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveFile").replace("FileName", fileName));
			log.info("Verifying the attachments folder in google drive");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DriveFolderAttachments"))!=0){
				log.info("File found");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveFolderAttachments"));
				Thread.sleep(4000);
			}else {
				log.info("Scrolling down to find the folder");
				scrollDown(1);
			}
		}catch(Exception e){
			throw new Exception("Unable to open drive "+e.getLocalizedMessage());
		}
	}
	
	public void openDriveAndVerify(AppiumDriver driver, String fileName, String Type)throws Exception{
		try{
			verifyGoogleDriveAppInitState(driver);
			pullToReferesh(driver);
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchDriveIcon"));
			Thread.sleep(2000);
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("SearchTextDrive"), fileName);
			Thread.sleep(4000);*/
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveFile").replace("FileName", fileName));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("TestingPdfAttachment").replace("FileName", fileName))!=0){
				log.info("File found");
			}else {
				scrollDown(1);
			}
			commonPage.waitForElementToBePresentById(commonElements.get("DriveMoreActions"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("DriveMoreActions"));
			commonPage.waitForElementToBePresentById(commonElements.get("iIconActionMenu"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("iIconActionMenu"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("FileTypeKind").replace("Kind", Type));
			log.info("Verified file is saved as Pdf");
		}catch(Exception e){
			throw new Exception("Unable to open drive "+e.getLocalizedMessage());
		}
	}
	
	/**Method to verify the mail and mark it is as important
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public static void verifyMarkAsImportant(AppiumDriver driver, String subject) throws Exception{
		SoftAssert softAssert = new SoftAssert();
		try{
		log.info("In verify Mark As Important option method ");
		pullToReferesh(driver);
		Thread.sleep(3000);
		pullToReferesh(driver);
		//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
		commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", subject));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", subject));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MarkNotImportant"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkNotImportant"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkImportant"));
		Thread.sleep(2000);
		softAssert
		.assertTrue(
				commonPage
						.getScreenshotAndCompareImage("verifyMarkAsImportantInCV"),
				"Image comparison for verifyMarkAsImportantInCV");

		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	    CommonPage.waitForElementToBePresentByXpath(commonElements.get("MarkNotImportant"));
	    log.info("Mark not important is displayed");
	    driver.navigate().back();
	    driver.navigate().back();
	    log.info("Selecting multiple mails");
	    CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage2"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		Thread.sleep(800);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MarkNotImportant"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkNotImportant"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			Thread.sleep(800);
			
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkImportant"));

		}catch (Exception e) {
			e.printStackTrace();
			//throw new Exception("Unable to find Mark As Important ");
	}
	}

	/**Method to verify mail is starred 
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyStarredMail(AppiumDriver driver)throws Exception{
		try{
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("FirstMail"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MailActionAddStar"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionAddStar"));
				log.info("Mail is starred");
			}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MailActionRemoveStar"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionRemoveStar"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionAddStar"));
				log.info("Mail is already starred, so removed and added star again");
			}
			driver.navigate().back();
			commonPage.waitForElementToBePresentByXpath(commonElements.get("StarredMaile"));
			
			log.info("Verifying starring mail from more options");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			Thread.sleep(800);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveStarMoreOpt"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("NotStarredMaile"));
				log.info("Mail unstarred");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				Thread.sleep(800);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AddStarMoreOpt"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddStarMoreOpt"));
					commonPage.waitForElementToBePresentByXpath(commonElements.get("StarredMaile"));
					log.info("Mail Starred");
				}
			}
			
			
		}catch(Exception e){
			throw new Exception("Unable to click on star "+e.getLocalizedMessage());
		}
	}
	
	/**Method to verify mail is starred 
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyUnStarredMail(AppiumDriver driver)throws Exception{
		try{
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClickMoreOptionsCV"));
			log.info("Starring mail from CV more options");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveStarMoreOpt"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("MailActionAddStar"));
				
			}
			Thread.sleep(1000);
			driver.navigate().back();
			//driver.navigate().back();
			
			/*commonPage.waitForElementToBePresentByXpath(commonElements.get("FirstMail"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MailActionRemoveStar"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionRemoveStar"));
				//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailActionAddStar"));
				log.info("Mail is starred, so removed star");
			}*/
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage2"));
			log.info("Multiselect starring mails and unstar");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveStarMoreOpt"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("NotStarredMaile"));
				log.info("Mail unstarred");
			}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AddStarMoreOpt"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddStarMoreOpt"));
				commonPage.waitForElementToBePresentByXpath(commonElements.get("StarredMaile"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveStarMoreOpt"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
					commonPage.waitForElementToBePresentByXpath(commonElements.get("NotStarredMaile"));
					log.info("Mail unstarred");
				}
			}
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to click on star "+e.getLocalizedMessage());
		}
	}
	
	public void replyAllScreen(AppiumDriver driver, String smartReply, String EmailSubject)throws Exception{
		Thread.sleep(3000);
		try{
			log.info("Replying");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ReplyAllForward"))!=0){
				log.info("Reply All compose mail is opened and displayed");
				log.info("Looking for "+smartReply);
				Thread.sleep(4000);
//				driver.hideKeyboard();
				CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SmartReplyText").replace("ReplyText", smartReply) );
//				CommonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReplyText").replace("ReplyText", smartReply));
				log.info("Smart reply suggestion is displayed in the compose body");
				CommonFunctions.searchAndClickById(driver, commonElements.get("SendReply"));
				Thread.sleep(2000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
				CommonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReplyText").replace("ReplyText", smartReply));
				log.info("Mail is sent and received as reply mail");
			}else{
				throw new Exception("Reply All compose mail is not displayed");
			}
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReplyText").replace("ReplyText", smartReply));
		}catch(Exception e){
			throw new Exception("Unable to verify the reply all compose "+e.getLocalizedMessage());
		}
	}
	
	public void verifySuggestions(AppiumDriver driver, String EmailSubject)throws Exception{
		 /*String path = System.getProperty("user.dir");
		  String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SmartReplySuggestions.bat";
		  log.info("Executing the bat file to generate a mail and move to other folder "+command);
		  Process p = new ProcessBuilder(command).start();
		  log.info("Executed the bat file to generate a mail");
		 */ 
		
		
		//Thread.sleep(4000);
		try{
			//pullToReferesh(driver);
			verifyMailPresent(driver, EmailSubject);
			Thread.sleep(2000);
			scrollDown(1);
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReplyView"));
			log.info("Smart Reply view is displayed");
			/*smartReply = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SmartReplyButtonFirst"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SmartReplyButtonFirst"));
	*/	}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**Method to verify mail with smart reply suggestions
	 * 
	 * @param driver
	 * @param EmailSubject
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyMailSmartReplySuggestions(AppiumDriver driver, String EmailSubject)throws Exception{
			try{
			verifySuggestions(driver, EmailSubject);
			//replyAllScreen(driver, smartReply, EmailSubject);
		}catch(Exception e){
			throw new Exception("Unable to verfy smart reply suggestions "+e.getLocalizedMessage());
		}
	}
	
	/**Method to verify the Compose Screen language in Locale language
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyComposeLocale(AppiumDriver driver, String ComposeTitle, String FromAddress, String ToAddress,
			String SubjectAddress, String ComposeText)throws Exception{
		try{
			log.info("Tapping on the compose button to verify the preferred set Locale");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			commonPage.waitForElementToBePresentByXpath(ComposeTitle);
			log.info("Compose title is verified for preferred Locale");
			hideKeyboard();
			if(CommonFunctions.getSizeOfElements(driver, FromAddress)!=0){
				log.info("From translations is verified");
			}else{
				throw new Exception("Unable to verify the From text field in compose screen");
			}
			if(CommonFunctions.getSizeOfElements(driver, ToAddress)!=0){
				log.info("To translations is verified");
			}else{
				throw new Exception("Unable to verify the To text field in compose screen");
			}
			if(CommonFunctions.getSizeOfElements(driver, SubjectAddress)!=0){
				log.info("Subject translations is verified");
			}else{
				log.info("No subject");
				throw new Exception("Unable to verify the Subject text field in compose screen");
			}
			//hideKeyboard();
			Thread.sleep(1200);
			if(CommonFunctions.getSizeOfElements(driver, ComposeText)!=0){
				log.info("Compose body text translations is verified");
			}else{
				log.info("Compose text not displayed due to signature / or some other reason");
				//throw new Exception("Unable to verify the Compose text field in compose screen");
			}
			driver.navigate().back();
			//driver.navigate().back();
				}catch(Exception e){
					throw new Exception("Unable to verify the compose screen text in preferred locale");
		}
	}
	
	
	public String verifyComposeLocaleMail(AppiumDriver driver, String ComposeTitle, String FromAddress, String ToAddress,
			String SubjectAddress, String ComposeText)throws Exception{
		String subtxt;
		try{
			log.info("Tapping on the compose button to verify the preferred set Locale");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			commonPage.waitForElementToBePresentByXpath(ComposeTitle);
			log.info("Compose title is verified for preferred Locale");
			if(CommonFunctions.getSizeOfElements(driver, FromAddress)!=0){
				log.info("From translations is verified");
			}else{
				throw new Exception("Unable to verify the From text field in compose screen");
			}
			if(CommonFunctions.getSizeOfElements(driver, ToAddress)!=0){
				log.info("To translations is verified");
				CommonFunctions.searchAndClickByXpath(driver, ToAddress);
				CommonFunctions.searchAndSendKeysByXpath(driver, ToAddress, "gm.gig2.auto@gmail.com ");
			}else{
				throw new Exception("Unable to verify the To text field in compose screen");
			}
			if(CommonFunctions.getSizeOfElements(driver, SubjectAddress)!=0){
				log.info("Subject translations is verified");
				CommonFunctions.searchAndClickByXpath(driver, SubjectAddress);
				//CommonFunctions.searchAndSendKeysByXpath(driver, SubjectAddress, "בדיקות");
				
				/*--Not able to send non english language text using sendkeys or adb--*/
				Thread.sleep(2500);
				driver.tap(1, 374, 1409, 500);
				driver.tap(1, 704, 1567, 500);
				Thread.sleep(1200);
				subtxt = CommonFunctions.searchAndGetTextOnElementByXpath(driver, SubjectAddress);
			}else{
				log.info("No subject");
				throw new Exception("Unable to verify the Subject text field in compose screen");
			}
			hideKeyboard();
			Thread.sleep(1200);
			if(CommonFunctions.getSizeOfElements(driver, ComposeText)!=0){
				log.info("Compose body text translations is verified");
				CommonFunctions.searchAndClickByXpath(driver, ComposeText);
				CommonFunctions.searchAndSendKeysByXpath(driver, ComposeText, "בדיקה");
			}else{
				throw new Exception("Unable to verify the Compose text field in compose screen");
			}
			tapOnSend(driver);
				}catch(Exception e){
					throw new Exception("Unable to verify the compose screen text in preferred locale");
		}
		return subtxt;
	}
	
	
	/**Method to verify the search field text in Locale Language
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifySearchLocale(AppiumDriver driver, String SearchFieldText)throws Exception{
		try{
			log.info("Verifying the search field translations");
			//CommonFunctions.searchAndClickById(driver, commonElements.get("SearchIcon"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchLanguage"));
			
			if(CommonFunctions.getSizeOfElements(driver, SearchFieldText)!=0){
				log.info("Search field text with set language is verified");
			}else{
				throw new Exception("Unable to verify the search text");
			}
			driver.navigate().back();
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to verify the search translations "+e.getLocalizedMessage());
		}
	}
	
	public void verifyLeftNavigationLocale(AppiumDriver driver, String Primary, String Social, String Promotions, String Updates, String Forums)throws Exception{

		try{
			openMenuDrawer(driver);
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, Primary)!=0){
				log.info("Verified the Primary System Label");
			}else{
				throw new Exception("Unable to verify the Primary System Label");
			}
			if(CommonFunctions.getSizeOfElements(driver, Social)!=0){
				log.info("Verified the Social System Label");
			}else{
				throw new Exception("Unable to verify the Social System Label");
			}
			if(CommonFunctions.getSizeOfElements(driver, Promotions)!=0){
				log.info("Verified the Promotions System Label");
			}else{
				throw new Exception("Unable to verify the Promotions System Label");
			}
			if(CommonFunctions.getSizeOfElements(driver, Updates)!=0){
				log.info("Verified the Updates System Label");
			}else{
				throw new Exception("Unable to verify the Updates System Label");
			}
			if(CommonFunctions.getSizeOfElements(driver, Forums)!=0){
				log.info("Verified the Forums System Label");
			}else{
				throw new Exception("Unable to verify the Forums System Label");
			}
			driver.navigate().back();
		}catch(Exception e){
			throw new Exception("Unable to verify the Left Navigation Settings translations "+e.getMessage());
		}
	}
	
	/**Method to reuse for verifying the Over Flow options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void overFlowOptions(AppiumDriver driver, String MoveTo, String ChangeLabel, String MarkUnImportant, String Important,
			String Starred,String Ignore,String Print,String Spam)throws Exception{
		try{
			if(CommonFunctions.getSizeOfElements(driver, MoveTo)!=0){
				log.info("Verified Move To option");
			}
			if(CommonFunctions.getSizeOfElements(driver, MoveTo)!=0){
				log.info("Verified Move To option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, ChangeLabel)!=0){
				log.info("Verified Change Label option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, MarkUnImportant)!=0){
				log.info("Verified Mark as UnImportant option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, Starred)!=0){
				log.info("Verified Mail Starred option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, Important)!=0){
				log.info("Verified Mark as Important option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, Ignore)!=0){
				log.info("Verified Ignore option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver,Print)!=0){
				log.info("Verified Print option");
			}else{
				log.info("Not listed in the menu");
			}
			if(CommonFunctions.getSizeOfElements(driver, Spam)!=0){
				log.info("Verified As Spam option");
			}else{
				log.info("Not listed in the menu");
			}
			log.info("Verified all listed options for the mail in over flow menu");
		}catch(Exception e){
			throw new Exception("Unable to verify the over flow options "+e.getMessage());
		}
	}
	
	
	/**Method to verify the Over flow action options in CV view
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyMailOtherOptionsCV(AppiumDriver driver, String OverFlowBtn)throws Exception{
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
			commonPage.waitForElementToBePresentByXpath(OverFlowBtn);
			CommonFunctions.searchAndClickByXpath(driver, OverFlowBtn);
		//	overFlowOptions(driver);
			driver.navigate().back();
			driver.navigate().back();
			log.info("Verified CV view Over Flow options in preferred Language");
		}catch(Exception e){
			throw new Exception("Unable to verify the Mail other options like Move To, Delete, Archive "+e.getLocalizedMessage());
		}
	}
	
	/**Method to verify the Mail other options in TL view
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyMailOtherOptionsTL(AppiumDriver driver, String MailSenderImage, String OverFlowButton)throws Exception{

		try{
			commonPage.waitForElementToBePresentByXpath(MailSenderImage);
			CommonFunctions.searchAndClickByXpath(driver, MailSenderImage);
			CommonFunctions.searchAndClickByXpath(driver, OverFlowButton);
			//overFlowOptions(driver);
			driver.navigate().back();
			driver.navigate().back();
			log.info("Verified TL view Over Flow options in preferred Language");
		}catch(Exception e){
			throw new Exception("Unable to verfy the options in TL view "+e.getMessage());
		}
	}
	
	public void swipeMailToDeleteOrArchive(AppiumDriver driver, String MailSubject)throws Exception{
		Dimension windowSize = driver.manage().window().getSize();
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		log.info(MailXpath);
		//if(!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get(MailXpath))){
		if(!CommonFunctions.isElementByXpathDisplayed(driver, MailXpath)){
		log.info("Scrolling down to find the mail with the subject "+MailSubject);
			scrollDown(1);
			if(!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get(MailXpath))){
				scrollUp(1);
			}
		}
		WebElement ele = driver.findElement(By.xpath(MailXpath));
		int y = ele.getLocation().y;
		driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
		Thread.sleep(1000);
	}
	
	public void swipeText(AppiumDriver driver, String MailSubject)throws Exception{
		Dimension windowSize = driver.manage().window().getSize();
		String MailXpath = commonElements.get("SyncTipText").replace("SyncTip", MailSubject);
		log.info(MailXpath);
		WebElement ele = driver.findElement(By.xpath(MailXpath));
		int y = ele.getLocation().y;
		driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
		Thread.sleep(1000);
	}
	
	/**Method to verify Mail is Archived or Deleted
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyMailArchiveDelete(AppiumDriver driver, String ArchiveMail, String Undo, String DeleteMail)throws Exception{

		try{
			String MailSubject = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMail"));
			log.info(MailSubject);
			swipeMailToDeleteOrArchive(driver, MailSubject);
			if(CommonFunctions.getSizeOfElements(driver, ArchiveMail)!=0){
				log.info("Archived Mail is present");
			}else{
				log.info("Archived text is not displayed and instead it displayed as Deleted");
			}
			if(CommonFunctions.getSizeOfElements(driver,Undo)!=0){
				log.info("Undo Mail is present");
			}
			if(CommonFunctions.getSizeOfElements(driver, DeleteMail)!=0){
				log.info("Deleted Mail is present");
			}else{
				log.info("Deleted text is not displayed and instead it displayed as Archived");
			}
		}catch(Exception e){
			throw new Exception("Unable to verify the archive and delete options for the mail "+e.getMessage());
		}
	}
	
	/**Method to verify the settings translations for the preferred locale
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifySettings(AppiumDriver driver,String user, String Settings, String GSettings)throws Exception{

		try{
			openMenuDrawer(driver);
			if(CommonFunctions.getSizeOfElements(driver, Settings)!=0){
				CommonFunctions.searchAndClickByXpath(driver, Settings);
			}else{
				scrollDown(2);
				CommonFunctions.searchAndClickByXpath(driver, Settings);
			}
			log.info("Clicked on Settings Option");
			commonPage.waitForElementToBePresentByXpath(GSettings);
			log.info("Verified the General Settings option is displyed in preferred language");
			CommonFunctions.searchAndClickByXpath(driver, GSettings);
			commonPage.waitForElementToBePresentByXpath(GSettings);
			log.info("Navigated to General Settings Page and verified the translations");
			driver.navigate().back();
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("AddAccountOption").replace("Add account", user));
			commonPage.waitForElementToBePresentByXpath(Settings);
			log.info("Opened User Account Settings and verified the translations");
		}catch(Exception e){
			throw new Exception("Unable to verify the Settings option "+e.getMessage());
		}
	}
	
	public void tapOnComposeShortCut(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeShortCut"));
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("FromField"));
			log.info("Verified compose screen is opened after clicking on compose short cut");
		}catch(Exception e){
			throw new Exception("Unable to tap on compose short cut "+e.getMessage());
		}
	}
	
	public void openComposeShortCut(AppiumDriver driver, String account)throws Exception{
		try{
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeShortCut"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("FromField"));
			log.info("Verified compose screen is opened after clicking on compose short cut");
			String accountFrom = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FromUserXpath"));
			log.info(accountFrom);
			if(accountFrom.equalsIgnoreCase(account)){
				log.info("Last account used is displayed");
			}
			else{
				throw new Exception("Account used last for sent in before test case is not displayed");
			}
		}catch(Exception e){
			throw new Exception("Unable to open compose short cut "+e.getLocalizedMessage());
		}
	}
	
	/**Method to add signature
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Bindu
	 */
	public void addingSignature(AppiumDriver driver) throws Exception {
		try {
			Map<String, String> verifyAddingSignatureData = commonPage
					.getTestData("verifySettingsOption1");
			log.info("adding signature in compose body ");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			Thread.sleep(2000);
			/*scrollTo("Settings");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));*/
			CommonFunctions.scrollsToTitle(driver, "Settings");
			Thread.sleep(1200);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("AccountSpecificSystemLabelSetting").replace("accName",verifyAddingSignatureData.get("SwitchAccountId")));
			log.info("Entering Signature");
			Thread.sleep(1200);
			//commonFunction.searchAndClickByXpath(driver, commonElements.get("SettingSignature"));
			CommonFunctions.scrollsToTitle(driver, "Mobile Signature");
			Thread.sleep(800);
			commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("EnterSingature"),verifyAddingSignatureData.get("Signature"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
			commonPage.navigateUntilHamburgerIsPresent(driver);
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to add  signature ");
	}
	}
	/**Method to verify the signature 
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifySignature(AppiumDriver driver, String signature) throws Exception {
		try {
			Map<String, String> verifyAddingSignatureData = commonPage
					.getTestData("verifySettingsOption1");
			log.info("verify signature in compose body ");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			Thread.sleep(2000);
			commonPage.hideKeyboard();
			Thread.sleep(2000);
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SignatureMail").replace("sign", signature));
			
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify signature added ");
	}
	}
	
	/**Method to compose and send email with attachments
	 * 
	 * @param driver
	 * @author Rahul
	 * @throws Exception 
	 */
	public void composeAndSendMailForGmailAccountWithAttachments(AppiumDriver driver) throws Exception{
	    try{
	 	   Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

	 	   log.info("******************* Start composeAndSendMailForGmailAccountWithAttachments Functionality*******************");
	 	  /*Process p;
	 	   String[] pushingImageToDownloads = new String[]{"adb","push", "C:/Users/batchi/Desktop/NewImages/downloaded1.jpg /sdcard/Download"};
		    p = new ProcessBuilder(pushingImageToDownloads).start();
		    log.info("Pushed Image file to Downloads folder");*/
	   	 	Thread.sleep(5000);
	 	 	String fromUser=composeAsNewMail(driver,
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchAccountsTo"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestfour"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("BccFieldAttachments"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SubjectAttachments"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));			
		 	   
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			Thread.sleep(600);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			log.info("etouchtestone drive cliced");
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			Thread.sleep(2000);
//Added below code by Bindu on 09/08 as attachments are not visible to select
			if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImage"))){
				scrollDown(1);
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImage"))){
					scrollDown(1);
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			}else{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("Attachment1 clicked");	
			}
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSearchIcon"));
			Thread.sleep(2000);
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("DriveSearchTextField"),
					composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DriveAppiumJPGName") );
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);*/
// Ended code for search  by Bindu on 09/08			
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("Attachment1 clicked");*/		
		
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			Thread.sleep(2000);
			

			if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImageNew"))){
				scrollDown(1);
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImageNew"))){
					scrollDown(1);
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImageNew"));
			}else{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImageNew"));
			log.info("Attachment1 clicked");	
			}
			
//Added code to attach files from different places like recent, photos, Images on 04/09 --> On Hold
		/*	Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("Images"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Images"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ScreenshotsFolder"));
			
//			String screenshotName = 
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxTypeLayout"));
			
			}
			*/
			
// Code ended attach files from different places like recent, photos, Images  --> On Hold			
			Thread.sleep(5000);
			scrollDown(1);
			Thread.sleep(5000);
			
	// Added code to navigate to drafts by Bindu on 09/08
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			log.info("clicked on more options");
			CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SaveDraft"));
			log.info("verified save draft is dislayed");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SaveDraft"));
			log.info("clicked on saved draft");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
			log.info("clicked on navigate up");
			openMenuDrawer(driver);
			navDrawer.navigateToDrafts(driver);
			CommonFunctions.isElementByXpathDisplayed(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DraftWithSpecificSubject"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DraftWithSpecificSubject") );
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("editdraftIcon"));
			
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			Thread.sleep(600);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			log.info("etouchtestone drive cliced");
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveSearchIcon"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("DriveSearchTextField"),
					composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DriveImagePngName") );
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);*/
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachmentsFolderInDrive"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ImagePng"));
			driver.navigate().back();
			driver.navigate().back();
			
			CommonFunctions.isElementByXpathDisplayed(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DraftWithSpecificSubject"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DraftWithSpecificSubject") );
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("editdraftIcon"));
			
// Added code ended by Bindu on 08/09			
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
			Thread.sleep(800);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}		
			Thread.sleep(6000);
			driver.navigate().back();
			Thread.sleep(800);
			if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount")))
			userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
			log.info("Verifying the attachment");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("Verifyattachmentdraft1"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
			}else{
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
				log.info("Attachment found and clicked");
			}
			Thread.sleep(2500);
			
			try{
			/*CommonFunctions.scrollToCellByTitle(driver, "ImageJPG.jpg");
			CommonFunctions.scrollToCellByTitle(driver, "ImagePNG.png");
			*/scrollDown(1);
			CommonFunctions.scrollToCellByTitle(driver, "Appium.jpg");
			//CommonFunctions.scrollToCellByTitle(driver, "Getting started");
			CommonFunctions.scrollToCellByTitle(driver, "Appiumnew.png");
			log.info("All attachments found");
			}catch(Exception e){
				throw new Exception("Missing attachments");
			}
			Thread.sleep(2000);
		 	driver.navigate().back();
		 	if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestThreeAccount")))
		 		userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestThreeAccount"));
		 	Thread.sleep(2000);
		 	pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
			Thread.sleep(2000);
			scrollDown(1);
		 	log.info("scrolled to Getting Started attachment");
		 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
		 // Added code by Bindu to click and verify the image attachment is clicked on 20/09
	           
	           log.info("Clicking on the attachment");
	           CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"));
	           log.info("Verifying the attachment name after opening the image"); 	 	      
	           attachment.verifyAttachmentIsAddedSuccessfully(driver, "Appium.jpg");
	       	   driver.navigate().back();
	       	   if(CommonFunctions.getSizeOfElements(driver, commonElements.get("DiscardMailCVView") )==0){
	       		driver.navigate().back();  
	       	   }
//Added code by Bindu to click and verify the image attachment is clicked on 20/09
		 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImageNew"));
		 	driver.navigate().back();
		 	
		 	if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestOneAccount")))
		 	userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestOneAccount"));
			pullToReferesh(driver);
			Thread.sleep(3000);
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
			Thread.sleep(4000);
			scrollDown(1);
		 	log.info("scrolled to Geftting Started attachment");
		 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
		 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImageNew"));
		 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("ImagePng"));
		 	log.info("verified second attachments is displyaed");
		 	log.info("******************* Completed testcase composeMailWithMultipleAttachmentsAndAnsertedDriveChips Functionality*******************");
		 	
			}catch(Exception e){
				throw new Exception("Unable to attach file "+e.getLocalizedMessage());
			}
	}
	
	/**Method to verify the new RSVP displayed for calendar invites
	 * 
	 * @param driver
	 * @author Rahul
	 */
	public void verifyNewRSVPFormatDisplayedForCalendarInvites(AppiumDriver driver){

		try {
				Map<String, String> verifyNewRSVPFormatDisplayedForCalendarInvites = commonPage.getTestData("verifyNewRSVPFormatDisplayedForCalendarInvites");
				Thread.sleep(5000);
				Process p;
				log.info("Launching calender");
				String[] launchChromeCalendar = new String[]{"adb","shell","am", "start","-n","com.android.calendar/.LaunchActivity"};
				    p = new ProcessBuilder(launchChromeCalendar).start();
				
	 		    CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("CreateEventButton"));
	 		    Thread.sleep(1000);
	 		    if(driver.findElements(By.xpath("//android.widget.Button[@text='OK']")).size()!=0){
	 		    	driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
	 		    }
	 		    Thread.sleep(2000);
	 		    if(CommonFunctions.getSizeOfElements(driver,verifyNewRSVPFormatDisplayedForCalendarInvites.get("CalendarEmail"))==0)
	 		  {	
	 			  CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ClickCalendarEmailBox"));
	 			 CommonFunctions.scrollTo(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventCreatorEmail"));
	 			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);	
	 			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ClickOnEventCreatorEmail")); 	
	 		  }
	 		    log.info("Entering the Event Name");
				CommonFunctions.searchAndSendKeysByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventNametextbox"),verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventName"));
				String[] hidekeyboard = new String[]{"adb", "shell", "input", "keyevent 111"};
				ProcessBuilder pb;
				Process p1;
				pb = new ProcessBuilder(hidekeyboard);
				p1 = pb.start(); 
				Thread.sleep(1000);
				if(CommonFunctions.getSizeOfElements(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ViewMoreOptions"))!=0){
					log.info("Clicking on View More Options");
					CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ViewMoreOptions"));
				}else{
					p1 = pb.start(); 
					CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ViewMoreOptions"));
				}
				log.info("Scrolling down to enter participants");
				scrollDown(1);
				//CommonFunctions.scrollTo(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ScrollToAttendees"));
				Thread.sleep(2000);
				log.info("Entering required participants");
				CommonFunctions.searchAndSendKeysByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("RequiredAttendes"),verifyNewRSVPFormatDisplayedForCalendarInvites.get("RequiredAttendesEmail"));
				CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventSaveButton"));
			} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  }
	
	public void SendRsvpMail() throws Exception{
		 try {
        	 String host = "smtp.gmail.com";
             String port = "587";
             final String from = "gm.gig2.auto@gmail.com";
             final String password = "mobileautomation";
      
             // outgoing message information
             String to = "gm.gig1.auto@gmail.com";
           
             Properties properties = new Properties();
             properties.put("mail.smtp.host", host);
             properties.put("mail.smtp.port", port);
             properties.put("mail.smtp.auth", "true");
             properties.put("mail.smtp.starttls.enable", "true");

           // Session session = Session.getDefaultInstance(properties, null);
             Session session = Session.getDefaultInstance(properties, 
            		    new javax.mail.Authenticator(){
            		        protected PasswordAuthentication getPasswordAuthentication() {
            		            return new PasswordAuthentication(
            		                from, password);// Specify the Username and the PassWord
            		        }
            		});
             
             // Define message
            MimeMessage message = new MimeMessage(session);
            message.addHeaderLine("method=REQUEST");
            message.addHeaderLine("charset=UTF-8");
            message.addHeaderLine("component=VEVENT");

            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Meeting Request Using JavaMail");

            StringBuffer sb = new StringBuffer();
            StringBuffer buffer = sb.append("BEGIN:VCALENDAR\n" +
                    "PRODID:-//Google Inc//Google Calendar 70.9054//EN\n" +
                    "VERSION:2.0\n" +
                    "METHOD:REQUEST\n" +
                    "BEGIN:VEVENT\n" +
                    "UID:20120920T150350Z-70@http://localhost/www/\n"+
                    "CREATED:20140920T150350Z\n"+
                    "ATTENDEE;RSVP=TRUE:MAILTO:gm.gig1.auto@gmail.com\n" +
                    "ORGANIZER:MAILTO:gm.gig2.auto@gmail.com\n" +
                    "DTSTAMP:20051206T120102Z\n" +
                    "DTSTART:20181208T053000Z\n" +
                    "DTEND:20181208T060000Z\n" +
                    "LOCATION:Conference room\n" +
                    "TRANSP:OPAQUE\n" +
                    "SEQUENCE:0\n" +
                    "CATEGORIES:Meeting\n" +
                    "DESCRIPTION:This the description of the meeting.\n\n" +
                    "SUMMARY:Test meeting request\n" +
                    "PRIORITY:1\n" +
                    "CLASS:PUBLIC\n" +
                    "STATUS:CONFIRMED\n" +
                    "BEGIN:VALARM\n" +
                    "TRIGGER:PT1440M\n" +
                    "ACTION:DISPLAY\n" +
                    "DESCRIPTION:Reminder\n" +
                    "END:VALARM\n" +
                    "END:VEVENT\n" +
                    "END:VCALENDAR");
            
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setHeader("Content-Class", "urn:content-  classes:calendarmessage");
            messageBodyPart.setHeader("Content-ID", "calendar_message");
            messageBodyPart.setDataHandler(new DataHandler(
                    new ByteArrayDataSource(buffer.toString(), "text/calendar")));// very important

            // Create a Multipart
            Multipart multipart = new MimeMultipart();

            // Add part one
            multipart.addBodyPart(messageBodyPart);

            // Put parts in message
            message.setContent(multipart);

            // send message
            Transport.send(message);
        } catch (MessagingException me) {
            me.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

	/**Method to verify the new RSVP displayed for calendar invites in email
	 * 
	 * @param driver
	 * @author Rahul
	 */
	public void verifyNewRSVPFormatDisplayedForCalendarInvitesInEmail(AppiumDriver driver){

		try{
			Map<String, String> verifyNewRSVPFormatDisplayedForCalendarInvites = commonPage.getTestData("verifyNewRSVPFormatDisplayedForCalendarInvites");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			log.info("clicked on navigate up");
			if(CommonFunctions.isElementByXpathDisplayed(driver,verifyNewRSVPFormatDisplayedForCalendarInvites.get("etouchtesttwo")))
			  {	
			  driver.navigate().back();
			  }
			else
			  { 
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			    CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("etouchtesttwo"));
		      }
			Thread.sleep(2000);
			*/
			if(!CommonFunctions.isAccountAlreadySelected(verifyNewRSVPFormatDisplayedForCalendarInvites.get("MailAccount")))
			userAccount.switchMailAccount(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("MailAccount"));
			pullToReferesh(driver);
			log.info("Clicking on Event Mail");
			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("ClickEventMail"));
			Thread.sleep(5000);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("RespondYes"));
			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("RespondNo"));
			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("RespondMayBe"));
			/*Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("NavigateUp"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			 CommonFunctions.searchAndClickByXpath(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("etouchtestone"));
			 
			userAccount.switchMailAccount(driver, verifyNewRSVPFormatDisplayedForCalendarInvites.get("RequiredAttendesEmail"));
			pullToReferesh(driver);
			 CommonFunctions.isElementByXpathDisplayed(driver,verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventAcceptedMail"));
			 log.info("Event response Yes Recieved");
			 pullToReferesh(driver);
			 CommonFunctions.isElementByXpathDisplayed(driver,verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventTentativelyAcceptedMail"));
			 log.info("Event response MayBeRecieved");
			 pullToReferesh(driver);
			 CommonFunctions.isElementByXpathDisplayed(driver,verifyNewRSVPFormatDisplayedForCalendarInvites.get("EventDeclineMail"));
			 log.info("Event response Declined Recieved");
			*/
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
/**Method to verify the rich text formatting
 * 
 * @param driver
 * @author Rahul
 */
	public void richTextFormattingFeatures(AppiumDriver driver){
		try {
				Map<String, String> richTextFormattingFeatures = commonPage
						.getTestData("richTextFormattingFeatures");
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("ClickCompose"));
				CommonFunctions.searchAndSendKeysByXpath(driver, richTextFormattingFeatures.get("ComposeBody"),richTextFormattingFeatures.get("text"));
				Thread.sleep(2000);
				
				WebElement e = driver.findElement(By.xpath("//android.view.View[@content-desc='testing'] | //android.view.View[@text='testing']"));
				int x = e.getLocation().getX();
				log.info(x);
				int y = e.getLocation().getY();
				log.info(y);
				Point p = e.getLocation();
				Dimension d = e.getSize();
				new TouchAction(driver).longPress(p.getX()+ 44, p.getY() + (d.getHeight() / 2)).release().perform();
			//	CommonFunctions.tapUsingXYCordinates(driver, x, y);
				log.info("long press on text");

			//	Thread.sleep(2000);
				/*CommonFunctions.tapUsingXYCordinates(driver, richTextFormattingFeatures.get("textxcordinate"), richTextFormattingFeatures.get("textycordinate"));	
				CommonFunctions.tapUsingXYCordinates(driver, richTextFormattingFeatures.get("textxcordinate"), richTextFormattingFeatures.get("textycordinate"));	
				*/Thread.sleep(3000);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				log.info("Clicking on format button");
				log.info(deviceName);
				if(CommonFunctions.getSizeOfElements(driver, richTextFormattingFeatures.get("clickformat"))==0){
					if(deviceName.contains("Nexus")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+44, 609, 1);
					}else if(deviceName.contains("Samsung")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+70, 921,1);
					}else if(deviceName.contains("Pixel")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+63, 833,1);
					}
				}else{
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickformat"));
				}
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickbold"));
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickitalic"));
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickunderline"));
	            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				log.info("clicked on more options");
		    	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SaveDraft"));
				log.info("verified save draft is dislayed");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SaveDraft"));
				log.info("clicked on saved draft");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
				log.info("clicked on navigate up");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				log.info("clicked on navigate up");
				CommonFunctions.scrollsToTitle(driver,"Drafts");
				log.info("scroll to drafts");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DraftsOption"));
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickdraft"));
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickdraft"));
			    CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("editdraft"));
				 Thread.sleep(1500);
				 hideKeyboard();
				 Thread.sleep(2000);
					int x1 = e.getLocation().getX();
					log.info(x1);
					int y1 = e.getLocation().getY();
					log.info(y1);
					Point p1 = e.getLocation();
					Dimension d1 = e.getSize();
					new TouchAction(driver).longPress(p.getX()+ 44, p.getY() + (d.getHeight() / 2)).release().perform();
					//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					log.info("Clicking on format button");
				
					//CommonFunctions.tapUsingXYCordinates(driver, x, y);

				/* CommonFunctions.tapUsingXYCordinates(driver, richTextFormattingFeatures.get("textxcordinate"), richTextFormattingFeatures.get("textycordinate"));	
					CommonFunctions.tapUsingXYCordinates(driver, richTextFormattingFeatures.get("textxcordinate"), richTextFormattingFeatures.get("textycordinate"));	
				*/	if(CommonFunctions.getSizeOfElements(driver, richTextFormattingFeatures.get("clickformat"))==0){
					if(deviceName.contains("Nexus")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+44, 609, 1);
					}else if(deviceName.contains("Samsung")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+70, 921,1);
					}else if(deviceName.contains("Pixel")) {
						log.info(deviceName);
						driver.tap(1, p.getX()+63, 833,1);
					}
				}else{
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickformat"));
				}
				String actualIsTextBold =driver.findElement(By.xpath(richTextFormattingFeatures.get("clickbold"))).getAttribute("checked");
				if(actualIsTextBold.equalsIgnoreCase(richTextFormattingFeatures.get("boldtrue")))
				{
					log.info("text is bold");
				}
				else
				{
					log.info("text is not bold");
				}
				
			    String actualIsTextItalic =driver.findElement(By.xpath(richTextFormattingFeatures.get("clickitalic"))).getAttribute("checked");
				if(actualIsTextItalic.equalsIgnoreCase(richTextFormattingFeatures.get("italictrue")))
				{
					log.info("text is italic");
				}
				else
				{
					log.info("text is not italic");
				}
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				String actualIsTextUnderline =driver.findElement(By.xpath(richTextFormattingFeatures.get("clickunderline"))).getAttribute("checked");
				if(actualIsTextUnderline.equalsIgnoreCase(richTextFormattingFeatures.get("underlinetrue")))
				{
					log.info("text is underline");
				}
				else
				{
					log.info("text is not underline");
				}		
				
				/**--Commenting this below code because format options are increased and text clear format option is not visible.
				/*driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				if(CommonFunctions.getSizeOfElements(driver, richTextFormattingFeatures.get("clickclearformat"))!=0){
					driver.tap(1, 189, 712, 1);
				}else{
				CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickclearformat"));
				}
				String actualIsTextClearFormat =driver.findElement(By.xpath(richTextFormattingFeatures.get("clickclearformat"))).getAttribute("checked");
				if(actualIsTextUnderline.equalsIgnoreCase(richTextFormattingFeatures.get("underlinetrue")))
				{
					log.info("text is cleared fomat");
				}
				else
				{
					log.info("text is not cleared format");
				}*/
	} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  }
	
	/**
	 * @TestCase GAT16 : Compose mail and save as draft
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
	.    *Author Rahul kulkarni
	 * @throws: Exception
	 */
	public void composeMailAndSaveAsDraft(AppiumDriver driver){
		try{
			log.info("******************* Start composeMailAndSaveAsDraft Functionality*******************");
			Map<String, String>composeMailAndSaveAsDraft = commonPage.getTestData("composeMailAndSaveAsDraft");
			try{
				
// added log statement below by Bindu on  06/08
				log.info("Composing email tto create draft using Save-draft option");
			String fromUser=composeAsNewMail(driver,
					composeMailAndSaveAsDraft.get("CcFieldDatafdraft"),
					"", "",
				    composeMailAndSaveAsDraft.get("SubjectDatadraft"),
					composeMailAndSaveAsDraft.get("ComposeBodyDatadraft"));					
			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			log.info("clicked on more options");
			CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SaveDraft"));
			log.info("verified save draft is dislayed");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SaveDraft"));
			log.info("clicked on saved draft");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
			log.info("clicked on navigate up");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			log.info("clicked on hamburger");			
			CommonFunctions.scrollsToTitle(driver, "Drafts");
			Thread.sleep(2500);
			CommonFunctions.isElementByXpathDisplayed(driver,composeMailAndSaveAsDraft.get("ExpectedFirstDraft"));
			log.info("verified first draft is displayed");
		
			}catch(Exception e){
				throw new Exception("Issue while saving the mail as draft by clicking on save draft option");
			}
	// modified log content below by Bindu on 06/08		
			log.info("click on compose mail to create draft by hitting back button");
		try{	
			String fromUser2=composeAsNewMail(driver,
					composeMailAndSaveAsDraft.get("CcFieldDatafdraft"),
					"", "",
				    composeMailAndSaveAsDraft.get("SubjectDatadraft2"),
					composeMailAndSaveAsDraft.get("ComposeBodyDatadraft2"));
			//navDrawer.navigateToDrafts(driver);
			//driver.navigate().back();
	        driver.navigate().back();
	        Thread.sleep(2000);
	        CommonFunctions.isElementByXpathDisplayed(driver,composeMailAndSaveAsDraft.get("ExpectedSecondtDraft"));
		}catch(Exception e){
			throw new Exception("Issue while saving the mail as draft by navigating back");
		}
	        log.info("******************* End composeMailAndSaveAsDraft Functionality*******************");
	    	}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * @TestCase GAT17 : composeMailWithMultipleAttachmentsAndAnsertedDriveChips
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
	.    *Author Rahul kulkarni
	 * @throws: Exception
	 */
	public void composeMailWithMultipleAttachmentsAndAnsertedDriveChips(AppiumDriver driver){
	       try{
	    	   Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	    	   log.info("******************* Start composeMailWithMultipleAttachmentsAndAnsertedDriveChips Functionality*******************");
	    	   String fromUser=composeAsNewMail(driver,
	           composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"),
	           composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("CcFieldDatafdraft"), "",
	           composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SubjectAttachments"),
	           composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));			
	    	   
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
	 		log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			log.info("etouchtestone drive cliced");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("Attachment1 clicked");		
		
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			Thread.sleep(5000);
			log.info("etouchtestone drive clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			log.info("etouchtestone drive clicked");
			//CommonFunctions.scrollTo(driver, "Getting started");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MyDriveFileName2"));
			log.info("Attachment1 clicked");	
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			log.info("clicked on more options");
	    	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SaveDraft"));
			log.info("verified save draft is dislayed");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SaveDraft"));
			log.info("clicked on saved draft");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
			log.info("clicked on navigate up");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			log.info("clicked on navigate up");
			CommonFunctions.scrollTo(driver,"Drafts");
			log.info("scroll to drafts");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DraftsOption"));
			log.info("click on drafts");
			//CommonFunctions.scrollTo(driver,"verifyattachments");
			log.info("scroll to verifyattachments");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
	    	CommonFunctions.scrollTo(driver,"Getting started");
	    	log.info("scrolled to Getting Started attachment");
	    	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
	    	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("MyDriveFileName2"));
	    	//driver.findElement(By.xpath("//android.widget.TextView[@text='Getting started']")).isDisplayed();
	    	log.info("verified second attachments is displyaed");
	    	log.info("******************* End composeMailWithMultipleAttachmentsAndAnsertedDriveChips Functionality*******************");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void clickOnSmartReply(AppiumDriver driver,String EmailSubject) throws Exception{
		driver = map.get(Thread.currentThread().getId());

		smartReply = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SmartReplyButtonThird"));
		log.info("Clicking on smart reply suggestion");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SmartReplyButtonFirst"));
		log.info("Clicked on samrt reply");
		replyAllScreen(driver, smartReply, EmailSubject);
		}
	
	
	/**Method to edit mail and and discard mail
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void saveMailAsDraftAndDiscard(AppiumDriver driver)throws Exception{
		try{
			Map<String, String> smartReplySuggestionsData = CommonPage.getTestData("smartReplySuggestions");
			//commonFunction.searchAndClickByXpath(driver, commonElements.get("ReplyButton"));
			inbox.composeMailAsReply(driver,smartReplySuggestionsData.get("ComposeBody") ,smartReplySuggestionsData.get("") );

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));	
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EditMail"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EditMail"));
			}else{
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EditMail"));
			}
			commonPage.waitForElementToBePresentByXpath(commonElements.get("DriverMoreOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardMail"));
			log.info("Mail discard");
			Thread.sleep(1000);
			if(commonFunction.getSizeOfElements(driver, commonElements.get("DiscardPopUpOk") )!=0){
				Thread.sleep(850);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
				log.info("Clicked on Ok button in the pop up");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendMailWithSubject(AppiumDriver driver)throws Exception{
		try{
			 String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SearchFunctionality.bat";
			 log.info("Executing the bat file to generate and delete it "+command);
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file");
			 Thread.sleep(5000);
			 pullToReferesh(driver);
		}catch(Exception e){
			throw new Exception("Unable to send email from bat file");
		}
	}
	
	public void openMailWithSearchTerm(AppiumDriver driver,String MailSubject) throws Exception {
		log.info("Open Mail with subject: " + MailSubject);
		Thread.sleep(3000);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("InboxMail").replace("MailSubject", MailSubject))==0){
			sendMailWithSubject(driver);
		}
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		waitForElementToBePresentByXpath(MailXpath); 
		CommonFunctions.searchAndClickByXpath(driver, MailXpath);
		waitForElementToBePresentByXpath(commonElements.get("ConversationUnreadIcon"));
	}
	
	public void sendMailArchive(AppiumDriver driver) throws Exception{
		 String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SearchArchiveMail.bat";
		 log.info("Executing the bat file to generate and delete it "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed Bat file");
		 Thread.sleep(5000);
		 pullToReferesh(driver);
	}
	
	public void sendDeleteMail(AppiumDriver driver)throws Exception{
		String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"SearchDeleteMail.bat";
		 log.info("Executing the bat file to generate and delete it "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed Bat file");
		 Thread.sleep(5000);
		 pullToReferesh(driver);
	}
	
	/**Method to remove unsyncable label spam
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Rahul
	 */
	public void removeUnsyncableLablesSpam(AppiumDriver driver) throws Exception {
		try{
			Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		/*	CommonFunctions.scrollTo(driver, "Settings");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
		*/	CommonFunctions.scrollsToTitle(driver, "Settings");
			CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("etouchtestone"));
			/*CommonFunctions.scrollTo(driver, "Manage labels");
			CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("Managelables"));*/
			CommonFunctions.scrollsToTitle(driver, "Manage labels");
			Thread.sleep(3000);
			/*if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("spam"))==0){
				log.info("Spam is not displayed in Manage Labels");
		}else{
			throw new Exception("Spam is displayed in manage labels");
		}*/
			if(CommonFunctions.scrollToCellByTitleVerify(driver, "Spam") == false){
				log.info("Spam is not displayed in Manage Labels");
			}else{
				throw new Exception("Spam is displayed in manage labels");
			}
			
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			 /*CommonFunctions.scrollTo(driver, "Spam");
			 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("spam"));
			 */
			 CommonFunctions.scrollsToTitle(driver, "Spam");
			 Thread.sleep(1000);
			 pullToReferesh(driver);
			 log.info("Spam is synced and mails displayed");
			 driver.navigate().back();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	public void openDownloadsApp(AppiumDriver driver)throws Exception{
		try{
			String [] openMultiWindow1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.providers.downloads.ui/.DownloadList"};
			ProcessBuilder pb;
			Process p;
			pb = new ProcessBuilder(openMultiWindow1);
			pb.redirectErrorStream(true);
			p = pb.start(); 
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
			    log.info(line);
			    if(line.contains("does not exist")){
			    	driver.navigate().back();
			    	if(driver.findElements(By.xpath("//android.widget.TextView[@content-desc='Apps']")).size()!=0){
			    		driver.findElement(By.xpath("//android.widget.TextView[@content-desc='Apps']")).click();
			    	}else{
			    		driver.navigate().back();
			    		driver.findElement(By.xpath("//android.widget.TextView[@content-desc='Apps']")).click();
			    	}
			    	if(driver.findElements(By.xpath(commonElements.get("DownloadsApp"))).size()!=0){
			    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DownloadsApp"));
			    	}else{
			    		scrollDown(1);
			    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DownloadsApp"));
			    	}
			    }
			}
			p.waitFor();
			in.close();
			Thread.sleep(4000);
		}catch(Exception e){
			throw new Exception("Unable to open downloads app "+e.getLocalizedMessage());
		}
	}
	
	/**
	 * @TestCase  :verifyBlockAndUnblockSenderWorks
	 * @Pre-condition :-
	 * @throws: Exception
	 */
	public void verifyBlockAndUnblockSenderWorks(AppiumDriver driver) throws Exception {
		try{//Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		    Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	        Map<String, String>VerifyBlockAndUnblockSenderWorks = commonPage.getTestData("VerifyBlockAndUnblockSenderWorks");
	        
	        String  emailId = CommonFunctions.getNewUserAccount(composeMailWithMultipleAttachmentsAndAnsertedDriveChips, "etouchTestTwoAccount", null);
	        
	        if(!CommonFunctions.isAccountAlreadySelected(emailId))
	        	//userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
	        	userAccount.switchMailAccount(driver, emailId);
	        
	        Thread.sleep(2000);
	        String fromUser;
	        
	        /* fromUser=inbox.composeAndSendNewMail(driver,
	        		VerifyBlockAndUnblockSenderWorks.get("ToFieldData"),
	        		VerifyBlockAndUnblockSenderWorks.get("ToFieldData"), 
	        		VerifyBlockAndUnblockSenderWorks.get("BccFieldDat"),
	        		VerifyBlockAndUnblockSenderWorks.get("SubjectDatablocker"),
					VerifyBlockAndUnblockSenderWorks.get("ComposeBodyDat"));*/
	        
	        
	        /* fromUser=inbox.composeAndSendNewMail(driver,
	        		 VerifyBlockAndUnblockSenderWorks.get("ToFieldData"),
	        		"", 
	        		"",
	        		VerifyBlockAndUnblockSenderWorks.get("SubjectDatablocker"),""
	        		);	        
	        */
	        
	        //mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingBlockSender", " ");
	        CommonFunctions.smtpMailGeneration("gm.gig1.auto@gmail.com","TestingBlockSender", "verifyMailSnippets.html", null);
	        Thread.sleep(2000);
	        
	       /* fromUser = inbox.composeAsNewMailWithOnlyTo(driver, VerifyBlockAndUnblockSenderWorks.get("ToFieldData"), 
			 			 VerifyBlockAndUnblockSenderWorks.get("SubjectDatablocker"));
			*/        /*CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));	
			        if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					}*/
			        /*   
			 	  	String path = System.getProperty("user.dir");
			        String command = path + java.io.File.separator + "Mail" + java.io.File.separator + "TestingBlockSender.bat";
			       // String command1 = path + java.io.File.separator + "Mail" + java.io.File.separator + "TestingDefaultInBox.bat";
			        log.info("Executing the bat file " + command);
			        Process p = new ProcessBuilder(command).start();
			        log.info("Executed the bat file to generate a mail");
			 	  */
	        //CommonFunctions.smtpMailGeneration(emailId,"TestingBlockSender", "verifyMailSnippets.html", null);
	        if(!CommonFunctions.isAccountAlreadySelected(VerifyBlockAndUnblockSenderWorks.get("ToFieldData")))
			 	  userAccount.switchMailAccount(driver, VerifyBlockAndUnblockSenderWorks.get("ToFieldData"));
			      Thread.sleep(1000); 
			 	  pullToReferesh(driver);
			 	  Thread.sleep(2000);
			 	  pullToReferesh(driver);
			        CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("VerifyBlockerMail"));
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("ClickMoreOptions"));
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("BlockSender"));
					Thread.sleep(1000);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("BlockedMessage"));
					log.info("Blocked message displayed");
					driver.navigate().back();
					/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));				
					CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("Accounttwo"));
					*/
					Thread.sleep(1200);
					if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
			        //mail.sendHtmlEmail("gm.gig2.auto@gmail.com", "mobileautomation", "gm.gig1.auto@gmail.com", "TestingBlockSenderNew", " ");
					
					 CommonFunctions.smtpMailGeneration("gm.gig1.auto@gmail.com","TestingBlockSenderNew", "verifyMailSnippets.html", null);
					 Thread.sleep(1200);
					 userAccount.pullToReferesh(driver);
/*					fromUser = inbox.composeAsNewMailWithOnlyTo(driver, VerifyBlockAndUnblockSenderWorks.get("ToFieldData"), 
				 			 VerifyBlockAndUnblockSenderWorks.get("SubjectDataNewblocker"));
*/				      /*  CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));	
				        if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
						}
					*/
					
					/*String path1 = System.getProperty("user.dir");
			        String command1 = path1 + java.io.File.separator + "Mail" + java.io.File.separator + "TestingBlockSenderNew.bat";
			       // String command1 = path + java.io.File.separator + "Mail" + java.io.File.separator + "TestingDefaultInBox.bat";
			        log.info("Executing the bat file " + command1);
			        Process p1 = new ProcessBuilder(command1).start();
			        log.info("Executed the bat file to generate a mail");*/
				    Thread.sleep(1000);   
				    if(!CommonFunctions.isAccountAlreadySelected( VerifyBlockAndUnblockSenderWorks.get("ToFieldData")))
				    	userAccount.switchMailAccount(driver, VerifyBlockAndUnblockSenderWorks.get("ToFieldData"));
				       
						 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
						driver.scrollTo("Spam");	
						CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("ClickSpam"));
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						CommonFunctions.isElementByXpathDisplayed(driver, VerifyBlockAndUnblockSenderWorks.get("VerifyBlockerMailNew"));
						 log.info("Mail is sent to spam folder for blocked user");
						 CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("VerifyBlockerMailNew"));
						 log.info("Clicked on BlockerMailNew");
						 Thread.sleep(2000);
						 driver.navigate().back();
						 driver.navigate().back();
						 CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("VerifyBlockerMail"));
						 Thread.sleep(2000);
						 CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("ClickMoreOptions"));
						 log.info("Clicked on More Options");
						 CommonFunctions.searchAndClickByXpath(driver,VerifyBlockAndUnblockSenderWorks.get("UnblockUser"));
						 log.info("Clicked on Unblock User");
						 Thread.sleep(1000);
						 //commonPage.waitForElementToBePresentByXpath(commonElements.get("UnblockedMessage"));
						 log.info("Message unblocked and message is displayed ");
						 driver.navigate().back();
						 CommonFunctions.smtpMailGeneration("gm.gig1.auto@gmail.com","TestingBlockSenderNew", "verifyMailSnippets.html", null);
						 Thread.sleep(1200);
						 userAccount.pullToReferesh(driver);
						 Thread.sleep(2000);
						 CommonFunctions.isElementByXpathDisplayed(driver, VerifyBlockAndUnblockSenderWorks.get("VerifyBlockerMailNew"));
						 log.info("Mail is sent to primary folder for unblocked user");

				} 
		catch (Exception e) {
			// TODO Auto-generated catch block
	 		e.printStackTrace();
			throw new Exception("block unblock case failed");
		}

	}
	
	/**
	 * @throws Exception 
	 * @TestCase GAT29  : Verify two ads display in Promotion/Social
	 * @Pre-condition : Gmail App should be installed in testing device and 
	 * Gmail account is added to app
	.    *Author Phaneendra
	 * @throws: Exception
	 */	
	public void verifyTwoAdsDisplayInPromotionSocial(AppiumDriver driver) throws Exception{
		try {
			log.info("******************* Start verifyTwoAdsDisplayInPromotionSocial Functionality*******************");
			Map<String, String>verifyTwoAdsDisplayInPromotionSocial = commonPage.getTestData("verifyTwoAdsDisplayInPromotionSocial");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			//CommonFunctions.searchAndClickByXpath(driver, verifyTwoAdsDisplayInPromotionSocial.get("promotion"));
			CommonFunctions.scrollsToTitle(driver, "Promotions");
			log.info("Clicked on Promtotions");
			Thread.sleep(2000);
			List<WebElement> adsCount = new ArrayList<WebElement>();
			adsCount = driver.findElements(By.xpath(verifyTwoAdsDisplayInPromotionSocial.get("verifyad1")));
			log.info("Number of ads: "+adsCount.size());
			if(adsCount.size() == 2){
				log.info("Two Ads are displayed for promotions");
			}else{
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SocialInboxOption"));
				Thread.sleep(2000);
				List<WebElement> NumberOfAdsInSocial = driver.findElements(By.xpath(verifyTwoAdsDisplayInPromotionSocial.get("verifyad1")));    
				System.out.println(NumberOfAdsInSocial.size());
				if(NumberOfAdsInSocial.size() == 2)
				{
				 log.info("Two Ads  Displayed in Social");
				}else{
					log.info("Two ads are not displayed");
					throw new Exception("Number of ads two are not displayed for both Social and Promotions");
				}
			}
			/*java.util.Iterator<WebElement> i = adsCount.iterator();
			while(i.hasNext()) {
			    WebElement element = i.next();*/
			for(int i=0; i<2; i++){
			    	String adtxt = driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout/"
			    			+ "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[@index='1']/android.widget.TextView[contains(@text,'Ad')]")).getText();
			    	log.info(adtxt);
			    	//CommonFunctions.searchAndClickByXpath(driver, adtxt);		
					driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView/android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout/"
			    			+ "android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout[@index='1']/android.widget.TextView[contains(@text,'Ad')]")).click();
			    	CommonFunctions.isElementByXpathDisplayed(driver, verifyTwoAdsDisplayInPromotionSocial.get("Verifyad1displayed"));
					log.info("Ad is opened");
					driver.navigate().back();
					Thread.sleep(1500);
			} 
		}
			catch(Exception e){
			e.printStackTrace();
			throw new Exception("Ads did not clicked or opened");
		}
	}

	/**
	 * @TestCase  : reportSpam
	 * @Pre-condition :-

	 * @throws: Exception
	 */ 
	public void reportSpam(AppiumDriver driver) throws Exception {
		try{
			 Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			 Map<String, String> reportSpam = commonPage.getTestData("reportSpam");
			 
			 String  emailId = CommonFunctions.getNewUserAccount(reportSpam, "Account", null);
			 
			 if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver,emailId);
			 
			 CommonFunctions.smtpMailGeneration(emailId,"TestingReportSpam", "verifyMailSnippets.html", null);
				
			 //navDrawer.openPrimaryInbox(driver, reportSpam.get("Account"));
			 navDrawer.openPrimaryInbox(driver, emailId);

		 	 /* CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
			  {	
			  	
			  driver.navigate().back();
			  }
		 	  
		      else
		 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
		 	   }
		 	   
		 	Thread.sleep(5000);
		 	   String fromUser=composeAsNewMail(driver,
		 			  reportSpam.get("ToFieldData"),
		 			  reportSpam.get("CCFieldData"),
		 			  reportSpam.get("BccFieldData"),
		 			  reportSpam.get("SubjectData"),
		 			  reportSpam.get("ComposeBodyData"));	
		 	    CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
		 	    log.info("Mail Sent");
		 	    Thread.sleep(4000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				log.info("account list");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtesttwo"));*/
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ReportSpamEmail"));
				Thread.sleep(1000);
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
				 CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ReportSpamButton"));
				 Thread.sleep(800);
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("MuteInstead"))!=0){
					 CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ReportSpamButton"));
				 }
				 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("NavigateUp"))!=0){
					 driver.navigate().back();
				 }
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				 log.info("Clicking on Hamburger");
				 CommonFunctions.scrollTo(driver, "Spam");
				 log.info("Clicking on Spam folder");
				 CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ClickSpam"));
				 driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				 CommonFunctions.searchAndClickByXpath(driver,reportSpam.get("ReportSpamEmail"));
				 log.info("Email is displayed in Spam and clicked");
				 CommonFunctions.isElementByXpathDisplayed(driver,reportSpam.get("CVWarningMessage"));
				 log.info("CV Warning message is displayed");
				 
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Unable to spam the mail due to some issues");
		}
	}

	/**
	 * @TestCase  : validateReportSpamForIMAPAccounts
	 * @Pre-condition :-

	 * @throws: Exception
	 */ 

	public void validateReportSpamForIMAPAccounts(AppiumDriver driver) throws Exception {
		try{
			Map<String, String> ValidateReportSpamForIMAPAccounts= commonPage.getTestData("ValidateReportSpamForIMAPAccounts");

			String  emailId = CommonFunctions.getNewUserAccount(ValidateReportSpamForIMAPAccounts, "ImapAccount", "IMAP");
			//userAccount.switchMailAccount(driver, ValidateReportSpamForIMAPAccounts.get("ImapAccount"));

			if(!CommonFunctions.isAccountAlreadySelected(emailId))
				userAccount.switchMailAccount(driver,emailId);

			CommonFunctions.searchAndClickByXpath(driver, ValidateReportSpamForIMAPAccounts.get("FirstMail"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SubjectLine"));
			String subjectCV = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
			log.info("subjectCV :"+subjectCV);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, ValidateReportSpamForIMAPAccounts.get("ReportSpamButton"));
			log.info("Clicked on report spam button from mail options");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SearchIconXpath"));
			log.info("Back to TL");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
			/*		    String subjectTL = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SelectedMailSubject"));
			 */		    
			Thread.sleep(1500);
			 String subjectTL =driver.findElement(By.xpath("//android.view.View[@content-desc='Deselect this conversation']/preceding-sibling::android.view.View")).getAttribute("name");
			 log.info(subjectTL);
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
			 Thread.sleep(800);
			 CommonFunctions.searchAndClickByXpath(driver, ValidateReportSpamForIMAPAccounts.get("ReportSpamButton"));
			 log.info("Clicked on report spam button from mail options");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			 CommonFunctions.scrollsToTitle(driver, "Spam");
			 inbox.verifyMailIsPresent(driver, subjectCV);
			 inbox.verifyMailIsPresent(driver, subjectTL);
			 log.info("Mails moved to junk folder");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
			 log.info("Selecting mails to report not spam");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			 Thread.sleep(800);
			 CommonFunctions.searchAndClickByXpath(driver, ValidateReportSpamForIMAPAccounts.get("ReportNotSpamButton"));
			 log.info("Mails moved to inbox");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @TestCase  : changeLabels
	 * @Pre-condition :-

	 * @throws: Exception
	 */ 
	public void changeLabels(AppiumDriver driver) throws Exception {
		try{
			/* CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
			  {
		 		 driver.navigate().back();
			  }
		 	   else
		 	  { 
			 	 Thread.sleep(5000);
			 	 scrollUp(2);
			 	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
			  }
		 	 	Thread.sleep(5000);*/
		 	 	ChangeLabel(driver);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void ChangeLabel(AppiumDriver driver)throws Exception{
		try{
			 Map<String, String> changeLabels = commonPage.getTestData("changeLabels");
			 Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			 
			 String emailId = composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("Account");
			 emailId = CommonFunctions.getNewUserAccount(composeMailWithMultipleAttachmentsAndAnsertedDriveChips, emailId, null);
			 
			 String mailSubject = changeLabels.get("SubjectData");
			 CommonFunctions.smtpMailGeneration(emailId,mailSubject, "verifyMailSnippets.html", null);
			/* String command = "E:\\Gmail-POC-GmailTest_Automation\\GmailPOC\\Mail\\ChangeLabel.bat";
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file");*/
			 
			 /*
			 String fromUser=composeAsNewMail(driver,
		 			  changeLabels.get("ToFieldData"),"","",
		 			  changeLabels.get("SubjectData"),
		 			  changeLabels.get("ComposeBodyData"));	
		 	    CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
		 	    Thread.sleep(1000);
		 	    if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
		 	*/    
		 	    log.info("Mail sent");
		 	//    CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			//	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				log.info("account list");
			//	userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"));
			//	CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtesttwo"));
			   // pullToReferesh(driver);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Thread.sleep(4000);
			    pullToReferesh(driver);
					Thread.sleep(1000);
			    pullToReferesh(driver); // added by Venkat on 12/01/2019
					Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ChangeLableEmail"));
				 CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickChangeLabel"));
				 Thread.sleep(1000);
				 String isLabelChecked=driver.findElement(By.xpath(changeLabels.get("CheckChangeLabel"))).getAttribute("checked");
				 if(isLabelChecked.equalsIgnoreCase("false"))
				 {
					 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("CheckChangeLabel"));				
				 }
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickOkChangeLabel"));
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			     Thread.sleep(800);
			   //  CommonFunctions.scrollTo(driver,"TestLabel");
			     CommonFunctions.scrollsToTitle(driver, "TestLabel");
			   //  CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("TestLabel"));
			     CommonFunctions.isElementByXpathDisplayed(driver,changeLabels.get("ChangeLableEmail"));
			     log.info("message is displayed in Test Lablel");
			     CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("SelectMailChangeLabel"));
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickChangeLabel"));
				 Thread.sleep(1000);
				 String isLabelCheckedT=driver.findElement(By.xpath(changeLabels.get("CheckChangeLabel"))).getAttribute("checked");
				 if(isLabelCheckedT.equalsIgnoreCase("true"))
				 {
					 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("CheckChangeLabel"));				
				 }
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickOkChangeLabel"));
			     CommonFunctions.isElementByXpathNotDisplayed(driver,changeLabels.get("ChangeLableEmail"));

			     driver.navigate().back();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @TestCase  :verifyUserIsAbleToMuteAConversation
	 * @Pre-condition :-

	 * @throws: Exception
	 */
	public void verifyUserIsAbleToMuteAConversation(AppiumDriver driver){

	try {
		Map<String, String> verifyUserIsAbleToMuteAConversation = commonPage.getTestData("verifyUserIsAbleToMuteAConversation");
		Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	    Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
	    
			String emailId = CommonFunctions.getNewUserAccount(verifyUserIsAbleToMuteAConversation, "AccountOne", null);

	    
	    	//userAccount.switchMailAccount(driver, verifyUserIsAbleToMuteAConversation.get("AccountOne"));
			if(!CommonFunctions.isAccountAlreadySelected(emailId))
			userAccount.switchMailAccount(driver, emailId);
			
	    	Thread.sleep(2500);
		   String fromUser=composeAsNewMail(driver,
				   verifyUserIsAbleToMuteAConversation.get("ToFieldData"),
				   verifyUserIsAbleToMuteAConversation.get(""),
				   verifyUserIsAbleToMuteAConversation.get(""),
				   verifyUserIsAbleToMuteAConversation.get("SubjectData"),""
				   );	
		   CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("clicksend"));
		    if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("clickOk"))!=0){
		    	CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("clickOk"));
		    }
		   
		   		log.info("Mail sent");
			/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			//Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			log.info("account list");
			CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));
			*/
		   		if(!CommonFunctions.isAccountAlreadySelected(verifyUserIsAbleToMuteAConversation.get("etouchtesttwo")))
		   	userAccount.switchMailAccount(driver, verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));	
		   	pullToReferesh(driver);
			//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(3500);
			pullToReferesh(driver);
			//pullToReferesh(driver);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail")); 
			CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			 CommonFunctions.searchAndClickByXpath(driver,verifyUserIsAbleToMuteAConversation.get("ClickMute"));
			 CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("Verify1Muted"));
			 log.info("1 muted  is displayed");
			// CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("VerifyUNDO"));
			 log.info("UNDO  is displayed");
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ClickUndo"));
			 CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
			 log.info("Mail is not muted and is displayed in Primary Inbox");
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
			 CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			 CommonFunctions.searchAndClickByXpath(driver,verifyUserIsAbleToMuteAConversation.get("ClickMute"));
			 log.info("Clicked om Mute");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		     CommonFunctions.scrollTo(driver,"All mail");
		     CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ClickAllMail"));
		     log.info("Clicked on ALLMAIL");
		     CommonFunctions.isElementByXpathDisplayed(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
		     log.info("Mail is muted and is displayed in ALL MAIL");
		     /*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		     Thread.sleep(5000);
		     scrollUp(1);
			 scrollUp(1);
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("etouchtestthree"));
			 pullToReferesh(driver);
			 */
		     if(!CommonFunctions.isAccountAlreadySelected(verifyUserIsAbleToMuteAConversation.get("etouchtestthree")))
		     userAccount.switchMailAccount(driver, verifyUserIsAbleToMuteAConversation.get("etouchtestthree"));
		     CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
			 Thread.sleep(5000);
			// CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ClickMailMoreOptionReply"));
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ReplyAll"));
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("clicksend"));
			    if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("clickOk"))!=0){
			    	CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("clickOk"));
			    }
			 Thread.sleep(1500);
			 driver.navigate().back();
	         /*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				//Thread.sleep(5000);
		     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		     log.info("account list");
			 CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));
		    */ 
			 Thread.sleep(1000);
			 if(!CommonFunctions.isAccountAlreadySelected(verifyUserIsAbleToMuteAConversation.get("etouchtesttwo")))
			 userAccount.switchMailAccount(driver, verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));
			 pullToReferesh(driver);
		     CommonFunctions.isElementByXpathNotDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
		     log.info("After Reply Muted Mail is not displayed in Inbox");
		     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		     CommonFunctions.scrollTo(driver,"All mail");
		     CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ClickAllMail"));
		     log.info("Clicked on ALLMAIL");
		     CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
		     log.info("Verified Muted mail is displayed in AllMail");
		   /*  CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("CheckMuteMail"));
		     CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("MailMoreOption"));
		     CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("MovetoInbox"));*/
		     driver.navigate().back();
		     }
		    catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	/**
	 * @throws Exception 
	 * @TestCase  :markMailReadUnread
	 * @Pre-condition :-

	 * @throws: Exception
	 */

	public void markMailReadUnread(AppiumDriver driver) throws Exception{

		try {
			Map<String, String>markMailReadUnread = commonPage.getTestData("markMailReadUnread");

			navDrawer.openPrimaryInbox(driver, markMailReadUnread.get("Account"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage")); 

			if(CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("ReadIcon"))!=0){
				log.info("Mail is unread");
				CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("ReadIcon"));
				commonPage.waitForElementToBePresentByXpath(markMailReadUnread.get("UnReadIcon"));
				log.info("Mail is marked as read");
				//CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("ReadIcon"));
				driver.navigate().back();
			}else if(CommonFunctions.getSizeOfElements(driver, markMailReadUnread.get("UnReadIcon"))!=0){
				log.info("Mail is read");
				CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("UnReadIcon"));
				commonPage.waitForElementToBePresentByXpath(markMailReadUnread.get("ReadIcon"));
				log.info("Mail is marked as Unread");
				CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("ReadIcon"));
				driver.navigate().back();

			}
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMail"));
			commonPage.waitForElementToBePresentByXpath(markMailReadUnread.get("ConversationUnreadIcon"));
			CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("ConversationUnreadIcon"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("PrimaryTitle"));
			log.info("Navigated back to TL view after mail is marked as Unread");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new Exception("Unable to mark mail read/unread");

		}
	}

	/**
	 * @TestCase  :unStarMail
	 * @Pre-condition :-

	 * @throws: Exception
	 */
	public void unStarMail(AppiumDriver driver){

	try{
	    
	    //Added By Phaneendra
	    
	    
		  /*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		  Thread.sleep(1000);
		  scrollUp(1);
		 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
		  {	
		   driver.navigate().back();
		  }
		  else
		  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
	      }
		   
		Thread.sleep(10000);
		   String fromUser=composeAsNewMail(driver,
				reportSpam.get("ToFieldData"),"","",
				reportSpam.get("SubjectDataUnstarMail"),
				reportSpam.get("ComposeBodyDataUnstarMail"));	
		    CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
		     log.info("clicked on more options");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			log.info("account list");
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtesttwo"));
			
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("UnstarMail"));
			CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ClickStar"));
			driver.navigate().back();
			 CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("UnstarMail"));
			 log.info("mail is marked as Star");
			 CommonFunctions.searchAndClickByXpath(driver, markMailReadUnread.get("TapMail")); 
			CommonFunctions.searchAndClickByXpath(driver, reportSpam.get("ClickUnStar"));
			log.info("mail is marked as UnStar");
			driver.navigate().back();
			*/
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		 }
	}
	
	/**
	 * @throws Exception 
	 * @TestCase  :  turnOffAccountSync
	 * @Pre-condition :-

	 * @throws: Exception
	 */
	public void turnOffAccountSync(AppiumDriver driver) throws Exception{

		try {
			Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage.getTestData("testAccountSyncWorksForGoogleAccounts");
			Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			Map<String, String> ValidateReportSpamForIMAPAccounts= commonPage.getTestData("ValidateReportSpamForIMAPAccounts");

		 	   log.info("******************* Start composeAndSendMailForGmailAccountWithAttachments Functionality*******************");

				String emailId = CommonFunctions.getNewUserAccount(composeMailWithMultipleAttachmentsAndAnsertedDriveChips, "etouchTestOneAccount", null);

		 	  //userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestOneAccount"));
				if(!CommonFunctions.isAccountAlreadySelected(emailId))
					userAccount.switchMailAccount(driver, emailId);
		 	  pullToReferesh(driver);
		 	 
		 	  if(CommonFunctions.getSizeOfElements(driver, ValidateReportSpamForIMAPAccounts.get("AccountSyncOFF"))!=0){
		 		  log.info("turnOffAccountSync Sync Gmail option is On");
		 	  }else{
			 		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					 log.info("turnOffAccountSync clicked on navigate up");
					 Thread.sleep(2500);
					 CommonFunctions.scrollsToTitle(driver, "Settings");
					 Thread.sleep(800);
				 	 //CommonFunctions.scrollsToTitle(driver, "gm.gig1.auto@gmail.com");
				 	CommonFunctions.scrollsToTitle(driver, emailId);
				 	 Thread.sleep(800);
				 	 CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
				 	 driver.navigate().back();
					 driver.navigate().back();
					 pullToReferesh(driver);
					 commonPage.waitForElementToBePresentByXpath(ValidateReportSpamForIMAPAccounts.get("AccountSyncOFF"));
					 log.info("Account sync tip is displayed");
		 	  }
			/*
			 * String path = System.getProperty("user.dir"); String command =
			 * path+java.io.File.separator+"Mail"+java.io.File.separator+
			 * "TestingTurnOffSync.bat"; log.
			 * info("Executing the bat file to generate a mail and move to other folder "
			 * +command); Process p = new ProcessBuilder(command).start();
			 */			
		 	  
				//CommonFunctions.smtpMailGeneration(emailId, "TurnOffSyncOff", "TestingNotificationsOff");
		 	  CommonFunctions.smtpMailGeneration(emailId,"TurnOffSyncOff", "TestingNotificationsOff", null);

				Thread.sleep(15000); 
				
				CommonFunctions.isElementByXpathNotDisplayed(driver, ValidateReportSpamForIMAPAccounts.get("TestSyncOFFEmail"));
				
		 	    } catch (Exception e) {
					commonPage.catchBlock(e, driver, "turnOffAccountSync");
				}
		  }

	/**
  	 * @TestCase  : testAccountSyncWorksForGoogleAccounts
  	 * @Pre-condition :
  	 * @throws: Exception
  	 */ 
public void testAccountSyncWorksForGoogleAccounts(AppiumDriver driver)  {
	
	try {
		Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage
				.getTestData("testAccountSyncWorksForGoogleAccounts");
		            
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		log.info("clicked on navigate up");
		CommonFunctions.scrollTo(driver, "Settings");
		log.info("Scrolled to settings");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
		log.info("clicked on settings");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
		log.info("clicked on Add Account");
		Thread.sleep(5000);
		//userAccount.addNewGmailAccount(testAccountSyncWorksForGoogleAccounts.get("GmailSyncAccount"),testAccountSyncWorksForGoogleAccounts.get("GmailSyncAccountPassword"));
 } catch (Throwable e) {
		
	}
}

/**
 * @TestCase  : verifytestAccountSyncWorksForGoogleAccounts
 * @Pre-condition :
 * @throws: Exception
 */ 
public void verifytestAccountSyncWorksForGoogleAccounts(AppiumDriver driver)  {
try {
	Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage
		.getTestData("testAccountSyncWorksForGoogleAccounts");

		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		log.info("clicked on navigate up");
		Thread.sleep(5000);
		scrollUp(1);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		log.info("account list");
		System.out.println(testAccountSyncWorksForGoogleAccounts.get("EtouchSyncAccount"));
		CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("EtouchSyncAccount"));
		
		driver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);		
		System.out.println("checking for element");
		System.out.println(testAccountSyncWorksForGoogleAccounts.get("VerifySyncMailnbox"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("inbox"));
		
		pullToReferesh(driver);
		Thread.sleep(5000);
		try
		{
			CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("VerifySyncMailnbox"));
		}catch(Exception e)
		{
			System.out.println("not found");
		}
		log.info("Mail is Displayed after intitial sync after adding account");
		pullToReferesh(driver);
		CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("VerifySyncMailnbox"));
		log.info("Mail is Displayed after pull to refesh");	
} catch (Throwable e) {
	
}
}

/**
 * Method to verify inbound  CV
 * @author batchi
 * @throws Exception 
 */	
public static void inBound(AppiumDriver driver2) throws Exception{
		Thread.sleep(1000);
 		if(CommonFunctions.getSizeOfElements(driver2,commonElements.get("PadLockIcon"))!=0){
 		tapOnViewRecepientDetails(driver2);
 	}
 }

/**
* Method to tap on view details
* @author batchi
* @param MailSubject
* @throws Exception
*/
public static void tapOnViewRecepientDetails(AppiumDriver driver2) throws Exception{
	    CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ViewRecipientDetails"));
		CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("DetailFromEmail"));
		CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("DetailToEmail"));
		CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("Date"));
		CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("ViewSecurityDetails"));
		CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("ViewSecurityDetails"));
		CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("SecurityDetailsPresence"));
	} 	
 		
/**
* Method to verify Outbound  CV
* @author batchi
* @throws Exception 
*/	
public static void outBound(AppiumDriver driver2,String inputKey) throws Exception{
 	try{
	CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("PadLockInSubject"));
 	CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("PadLockInSubject"));
 	 }catch(Exception e){
 		throw new Exception("Red lock icon is not displayed may be due to sync error of people details in account settings"); 
 	 	}
 	CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("PadLockDialog"));
 	CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("ViewDetailsOnPadlock"));
 //	CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("StandardEncryptionSupported"));
 	CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("NoEncryptionSupported"));
 	CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("StandardEncryptionMailVerification").replace("inputKey",inputKey));			
 }


/**
* Method to verify if the checkbox is checked or not
* @author batchi
 * @throws Exception 
*/
public void verifyIfCheckboxIsChecked(AppiumDriver driver2,String checkboxName) throws Exception{
	log.info("In verifyIfCheckboxIsChecked method ");
	try{
	WebElement element = driver2.findElement(By.xpath(checkboxName));
	if (element.getAttribute("checked").equalsIgnoreCase("false")) {
		log.info("###################### Checkbox is not checked ########################33");
		commonFunction.searchAndClickByXpath(driver2,checkboxName);
		log.info("Checked the checkbox");
	 }else if(element.getAttribute("checked").equalsIgnoreCase("true")){
		log.info("$$$$$$$$$$$$$$$$$$$$$$$$$ Checkbox is checked $$$$$$$$$$$$$$$$$$$$$$$$$$");
	}
	}catch(Exception e){
		throw new Exception("Unable to find "+checkboxName);
	}
  }	

/**
* Method to verify if the checkbox is checked or not
* @author batchi
 * @throws Exception 
*/
public void verifyIfCheckboxIsUnChecked(AppiumDriver driver2,String checkboxName) throws Exception{
	log.info("In verifyIfCheckboxIsUnChecked method ");
	Thread.sleep(2000);
	WebElement element = driver2.findElement(By.xpath(checkboxName));
	if (element.getAttribute("checked").equalsIgnoreCase("false")) {
		log.info("Checkbox is not checked");
	 }else if(element.getAttribute("checked").equalsIgnoreCase("true")){
		log.info("Checkbox is checked");
		commonFunction.searchAndClickByXpath(driver2,checkboxName);
		log.info("Unchecked the checkbox");
		//commonFunction.searchAndClickByXpath(driver2,commonElements.get("NotificationTurnOffOption"));
	} 
  }	

/**
 * Method to swipe To Delete Or Archive Mail
 * @author batchi
 * @param MailSubject
 * @throws Exception
 */
public void verifySwipe(AppiumDriver driver,String MailSubject) throws Exception {
	try {
		log.info("*******************Verifying the Swipe functionality***********************************");
		log.info("Swipe right of left to archive the mail with subject  " + MailSubject);
		
		Dimension windowSize = driver.manage().window().getSize();
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		waitForElementToBePresentByXpath(MailXpath);
		WebElement ele = driver.findElement(By.xpath(MailXpath));
		int y = ele.getLocation().y;
		driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
		Thread.sleep(100);
		if(commonFunction.getSizeOfElements(driver,commonElements.get("SwipeDeleteArchiveText"))!=0){
			log.info("************************** Swipe action option enabled ************************");
		}else{
			log.info("######################### Swipe action is disabled #########################");
		}
 }catch (Exception e) {
		e.printStackTrace();
		throw new Exception("Unable to find Mark As Important ");
}
}

/**
 * Method to swipe To Delete Or Archive Mail
 * @author batchi
 * @param MailSubject
 * @throws Exception
 */
public void verifySwipeMail(AppiumDriver driver) throws Exception {
	try {
		log.info("*******************Verifying the Swipe functionality is happening/Not***********************************");
		log.info("Swipe left to right to verify swipe action  ");
		Thread.sleep(800);
		Dimension windowSize = driver.manage().window().getSize();
		/*String MailXpath = commonElements.get("InboxMail").replace("MailSubject", MailSubject);
		waitForElementToBePresentByXpath(MailXpath);
		WebElement ele = driver.findElement(By.xpath(MailXpath));
		*/
		/*WebElement ele = driver.findElement(By.xpath("//android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.view.View"));
		int y = ele.getLocation().y;
		*///driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
		int  startx = (int) (windowSize.width * 0.70);
		 int endx = (int) (windowSize.width * 0.30);
		 int starty = windowSize.height / 2;
		  System.out.println("Start swipe operation");
		  driver.swipe(endx, starty, startx, starty, 1000);
		Thread.sleep(800);
		if(commonFunction.getSizeOfElements(driver,commonElements.get("SwipeDeleteArchiveText"))!=0){
			log.info("************************** Swipe action option enabled ************************");
		}else{
			log.info("######################### Swipe action is disabled #########################");
		}
 }catch (Exception e) {
		e.printStackTrace();
		throw new Exception("Unable to find Mark As Important ");
}
}


public void verifyAccountSettingsDisplayed(AppiumDriver driver)throws Exception{
	Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");

	try{
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.scrollTo(driver, "Settings");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
		log.info("Navigated to settings option");
		CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
		log.info("Account user is selected");
		Thread.sleep(2000);
		/*if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SettingsOption"))!=0){
			log.info("Settings page is displayed");
		}else if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SettingsOption").replace("Settings", newChar))){
			throw new Exception("Settings page not displayed");
		}*/
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("InboxType"));
		log.info("Inbox Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("InboxType"));
		log.info("InboxType Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("InboxCatagories"));
		log.info("InboxCatagories Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("Notifications"));
		log.info("Notifications Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("InBoxSoundAndVibrate"));
		log.info("InBoxSoundAndVibrate Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("Signature"));
		log.info("Signature Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("SmartReply"));
		log.info("SmartReply Displayed");
		scrollDown(1);

		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("VacationResponder"));
		log.info("VacationResponder Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("SyncGmail"));
		log.info("SyncGmail Displayed");	
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("DaysOfMailsToSync"));
		log.info("DaysOfMailsToSync Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("ManageLabels"));
		log.info("ManageLabels Displayed");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("DownloadAttachment"));
		log.info("DownloadAttachment Displayed");
		scrollDown(1);
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("Images"));
		log.info("Images Displayed");
		
	}catch(Exception e){
		throw new Exception("Setting is not displayed");
	}
}



/**
 * @TestCase  : verifyGmailAccountSpecificSettings
 * @Pre-condition :-

 * @throws: Exception
 */
public void verifyGmailAccountSpecificSettings(AppiumDriver driver) throws Exception {
    try {
        Map < String, String > navigateToSettings = commonPage.getTestData("navigateToSettings");
        Map < String, String > composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
        Thread.sleep(2000);
        verifyAccountSettingsDisplayed(driver);
        scrollUp(1);
        /*log.info("Navigating back");
        driver.navigate().back();
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));*/
        Thread.sleep(1000);
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxType"));
        Thread.sleep(2000);
        String checkDefualtInbox = driver.findElement(By.xpath(navigateToSettings.get("DefautInbox"))).getAttribute("checked");
        if (checkDefualtInbox.equalsIgnoreCase("false")) {
            CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("DefautInbox"));

        }
        log.info("Navigating back 2 times");
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();

        // CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        /* Thread.sleep(5000);
		 scrollUp(2);
		 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
		  {	
		  	
		  driver.navigate().back();
		  }
	 	  
	     else
	 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
	      }
	 	   */
        Thread.sleep(5000);

        String path = System.getProperty("user.dir");
        String command = path + java.io.File.separator + "Mail" + java.io.File.separator + "TestingPriorityInBox.bat";
        String command1 = path + java.io.File.separator + "Mail" + java.io.File.separator + "TestingDefaultInBox.bat";
        log.info("Executing the bat file " + command);
        Process p = new ProcessBuilder(command).start();
        log.info("Executed the bat file to generate a mail");

        /* String fromUser=composeAsNewMail(driver,
	 			  navigateToSettings.get("ToFieldData"),
	 			  navigateToSettings.get("CCFieldData"),
	 			  navigateToSettings.get("BccFieldData"),
	 			  navigateToSettings.get("SubjectData"),
	 			  navigateToSettings.get("ComposeBodyData"));	*/
        //  CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
        log.info("Mail is sent");
        Thread.sleep(3000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        Thread.sleep(2000);
        CommonFunctions.scrollTo(driver, "Settings");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxType"));
        log.info("Inbox Displayed");
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("PriorityInbox"));
        log.info("Clicked Priority Inbox");
        driver.navigate().back();
        driver.navigate().back();
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("VerifyPriorityInboxMail"));


        /* String fromUser1=composeAsNewMail(driver,
		 			    navigateToSettings.get("ToFieldData"),
			 			navigateToSettings.get("CCFieldData"),
			 			navigateToSettings.get("BccFieldData"),
			 			navigateToSettings.get("SubjectDataPriorityInbox"),
			 			navigateToSettings.get("ComposeBodyDataPriorityInbox"));			
		        CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));*/

        Process p1 = new ProcessBuilder(command1).start();
        log.info("Executed the bat file to generate a mail");
        Thread.sleep(2000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        Thread.sleep(2000);
        scrollUp(2);
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("ClickInbox"));
        log.info("Clicked on Inbox");
        pullToReferesh(driver);
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("VerifyInboxMail"));
        log.info("Mail in Inbox Displayed");

    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

}

public void ManageLabels(AppiumDriver driver)throws Exception{
	try{
        Map < String, String > navigateToSettings = commonPage.getTestData("navigateToSettings");
        log.info("In manage labels");
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
        Thread.sleep(1500);
		CommonFunctions.scrollsToTitle(driver, "Manage labels");
		Thread.sleep(1500);
		List<WebElement> labels = driver.findElements(By.xpath(commonElements.get("ManageLabelsList")));
		int count = labels.size();
		log.info("Number of labels "+labels.size());
				
		for(WebElement e : labels){
			log.info("Clicking on labels");
			e.click();
			driver.navigate().back();
		}
		
	}catch(Exception e){
		e.printStackTrace();
		throw new Exception("Unable to open label ");
	}
}


/**
 * @TestCase  : verifyGmailAccountSpecificInboxCategoriesSettings
 * @Pre-condition :-

 * @throws: Exception
 */
public void verifyGmailAccountSpecificInboxCategoriesSettings(AppiumDriver driver) throws Exception {
    try {
        Map < String, String > navigateToSettings = commonPage.getTestData("navigateToSettings");
        Map < String, String > composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
        //CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("InboxType"));
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("PriorityInboxinMenu"));
        pullToReferesh(driver);
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("VerifyPriorityInboxMail"));
        log.info("Mail in PriorityInbox Displayed");
        Thread.sleep(2000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        CommonFunctions.scrollTo(driver, "Settings");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxType"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("DefautInbox"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxCatagories"));

        String checkSocial = driver.findElement(By.xpath(navigateToSettings.get("Social"))).getAttribute("checked");
        if (checkSocial.equalsIgnoreCase("false")) {
            CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Social"));
            log.info("Checkded Social");
        }
        String checkPromotions = driver.findElement(By.xpath(navigateToSettings.get("Promotions"))).getAttribute("checked");
        if (checkPromotions.equalsIgnoreCase("false")) {
            CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Promotions"));
            log.info("Checked Promotions");
        }
        String checkUpdates = driver.findElement(By.xpath(navigateToSettings.get("Updates"))).getAttribute("checked");
        if (checkUpdates.equalsIgnoreCase("false")) {
            CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Updates"));
            log.info("Checked Updates");
        }
        String checkForums = driver.findElement(By.xpath(navigateToSettings.get("Forums"))).getAttribute("checked");
        if (checkForums.equalsIgnoreCase("false")) {
            CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Forums"));
            log.info("Checked  Forums");
        }
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();
        Thread.sleep(5000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        scrollUp(2);
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Social")) != 0) {
            log.info("Checked Social is displayed");
        }
        // CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("Social"));
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Promotions")) != 0) {
            log.info("Checked Promotions is displayed");

        }
        //	 CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("Promotions"));
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Updates")) != 0) {
            log.info("Checked Updates is displayed");
        }
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Forums")) != 0) {
            log.info("Checked Forums  is displayed");
        }

        // CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("Updates"));

        // CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("Forums"));
        CommonFunctions.scrollTo(driver, "Settings");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxCatagories"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Social"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Promotions"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Updates"));
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Forums"));
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();
        pullToReferesh(driver);
        Thread.sleep(5000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        scrollUp(2);
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Social")) != 0) {
            log.info("Checked Social is not displayed");
        }
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Promotions")) != 0) {
            log.info("Checked Promotions is not displayed");
        }
        //CommonFunctions.isElementByXpathNotDisplayed(driver, navigateToSettings.get("Social"));
        // log.info("Checked Social is not displayed");
        //  CommonFunctions.isElementByXpathNotDisplayed(driver, navigateToSettings.get("Promotions"));
        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Updates")) != 0) {
            log.info("Checked Updates is not displayed");
        }

        if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("Forums")) != 0) {
            log.info("Checked Forums  is not displyed");
        }
        // CommonFunctions.isElementByXpathNotDisplayed(driver, navigateToSettings.get("Updates"));
        // log.info("Checked Updates is not displayed");
        // CommonFunctions.isElementByXpathNotDisplayed(driver, navigateToSettings.get("Forums"));
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

}

/**
 * @TestCase  : navigateToSettings
 * @Pre-condition :
 * @throws: Exception
 */
public void navigateToSettings(AppiumDriver driver) throws Exception {
    try {

        Map < String, String > navigateToSettings = commonPage.getTestData("navigateToSettings");
        Thread.sleep(2000);
        if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("GeneralSetting"))!=0){
        	log.info("Already driver in settings page");
        }else{
        	
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
            log.info("Clicked on Hambugermenu");
         //  CommonFunctions.scrollTo(driver, "Settings");
            CommonFunctions.scrollsToTitle(driver, "Settings");            
            log.info("Scrolled to settings");
         //   CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
            log.info("clicked on settings");
            
        }
        
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("GeneralSetting"));
        log.info("General Setting is Displayed");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
        Thread.sleep(800);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Manage Account"));
        CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AccountSettings"));
        driver.navigate().back();
        Thread.sleep(800);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
        Thread.sleep(800);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
        CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HelpSection"));
        driver.navigate().back();
        // added by Venkat on 22-Nov-2018 : start
		int cnt = 0;
        while(cnt++ <=2){
	        if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("AddAccount")) <= 0){
	        	scrollDown(1);
	        	continue;
	        }
	        break;
        }
        // added by Venkat on 22-Nov-2018: end
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("AddAccount"));
        log.info("Add Account is Displayed");
        CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("AddAccount"));
        log.info("Add Account is Displayed");
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("SetUpEmail"));
        driver.navigate().back();
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("etouchtestone"));
        log.info("Specific Account setting  is Displayed");
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
}


/**
 * @TestCase  : navigateToSettings
 * @Pre-condition :
 * @throws: Exception
 */
public void navigateSettingsPage(AppiumDriver driver) throws Exception {
    try {

        Map < String, String > navigateToSettings = commonPage.getTestData("navigateToSettings");
        Thread.sleep(2000);
        if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("GeneralSetting"))!=0){
        	log.info("Already driver in settings page");
        }else{
        	
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
            log.info("Clicked on Hambugermenu");
         //  CommonFunctions.scrollTo(driver, "Settings");
            CommonFunctions.scrollsToTitle(driver, "Settings");            
            log.info("Scrolled to settings");
         //   CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
            log.info("clicked on settings");
            
        }
        
        CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("GeneralSetting"));
        log.info("General Setting is Displayed");
       
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
}

/**
 * @TestCase  : Enable/Disable Vacation Responder
 * @Pre-condition :Enable Vacation responder from Settings -> Account
 *  ""A"" -> Vacation Responder with some message and subject for 
 *  some time period

 * @throws: Exception
 */
public void enableDisableVacationResponder(AppiumDriver driver) throws Exception {
	try {
		Map < String, String > enableDisableVacationResponder = commonPage.getTestData("enableDisableVacationResponder");
			
		userAccount.openUserAccountSetting(driver, enableDisableVacationResponder.get("ToFieldVR"));
		CommonFunctions.scrollsToTitle(driver, "Vacation responder");
		//CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VacationResponder"));
		log.info("Clicked on Vacation Responder");
		Thread.sleep(1500);
		if (CommonFunctions.getSizeOfElements(driver, enableDisableVacationResponder.get("VRSwitchoff")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRSwitchoff"));
		}
		Thread.sleep(1300);
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRSwitchon"));
		log.info("Turned on Vacation Respoder");
		CommonFunctions.searchAndSendKeysByXpath(driver, enableDisableVacationResponder.get("VRSubject"), enableDisableVacationResponder.get("VRSubjectData"));
		log.info("Entered Subject in Vacation Responder");
		CommonFunctions.searchAndSendKeysByXpath(driver, enableDisableVacationResponder.get("VRMessage"), enableDisableVacationResponder.get("VRMessageData"));
		log.info("Entered Message in Vacatoon Responder");
		hideKeyboard();
		Thread.sleep(1500);
		String checkcontact = driver.findElement(By.xpath(enableDisableVacationResponder.get("Contactscheck"))).getAttribute("checked");
		if (checkcontact.equalsIgnoreCase("false")) {
			CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("Contactscheck"));
			log.info("Checkded Send only to my contact");
		}
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRDone"));
		log.info("Clicked on Done");
		driver.navigate().back();
		driver.navigate().back();
		if(!CommonFunctions.isAccountAlreadySelected(enableDisableVacationResponder.get("etouchtesttwolocation")))
		userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("etouchtesttwolocation"));
		
		// Thread.sleep(5000);
		//driver.navigate().back(); 

	} catch (Exception e) {
		e.printStackTrace();
	}
}


/**
 * @TestCase  : verifyEnableDisableVacationResponderMailIsPresent
 * @Pre-condition :Enable Vacation responder from Settings -> Account
 *  ""A"" -> Vacation Responder with some message and subject for 
 *  some time period

 * @throws: Exception
 */

public void verifyEnableDisableVacationResponderMailIsPresent(AppiumDriver driver) {
	try {
		Map < String, String > enableDisableVacationResponder = commonPage.getTestData("enableDisableVacationResponder");

		/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
		pullToReferesh(driver);
		pullToReferesh(driver);			
		*/
		Thread.sleep(4000);
		pullToReferesh(driver);
		pullToReferesh(driver);
		pullToReferesh(driver);
		
		CommonFunctions.isElementByXpathDisplayed(driver, enableDisableVacationResponder.get("VerifyReceivedVResponse"));
		log.info("Verified Vacation Responder Message is Received to Account B");
		userAccount.openUserAccountSetting(driver, enableDisableVacationResponder.get("ToFieldVR"));
		CommonFunctions.scrollsToTitle(driver, "Vacation responder");
		//CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VacationResponder"));
		log.info("Clicked on Vacation Responder");
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRSwitchoff"));
		log.info("Disabled Vacation Responder   ");
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRDone"));
		log.info("Clicked on Done");
		driver.navigate().back();
		driver.navigate().back();
		if(!CommonFunctions.isAccountAlreadySelected(enableDisableVacationResponder.get("etouchtestthreelocation")))
		userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("etouchtestthreelocation"));
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**
 * @TestCase  : verifyEnableDisableVacationResponderMailIsNotPresent
 * @Pre-condition :Enable Vacation responder from Settings -> Account
 *  ""A"" -> Vacation Responder with some message and subject for 
 *  some time period

 * @throws: Exception
 */

public void verifyEnableDisableVacationResponderMailIsNotPresent(AppiumDriver driver) {
	Map < String, String > enableDisableVacationResponder;
	try {
		enableDisableVacationResponder = commonPage.getTestData("enableDisableVacationResponder");
		/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
		pullToReferesh(driver);
		*/
		Thread.sleep(2500);

		pullToReferesh(driver);	
		CommonFunctions.isElementByXpathNotDisplayed(driver, enableDisableVacationResponder.get("VerifyReceivedVResponse"));
		log.info("Verified Mail is not Sent from Vacation Respoder to Account C after disabling Vacation Respoder");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**
 * @TestCase  : enableVacationResponderWithSendToMyContactsOption
 * @Pre-condition :Enable Vacation responder from Settings -> Account
 *  ""A"" -> Vacation Responder with some message and subject for 
 *  some time period

 * @throws: Exception
 */
public void enableVacationResponderWithSendToMyContactsOption(AppiumDriver driver) throws Exception {
	try {
		Map < String, String > enableDisableVacationResponder = commonPage.getTestData("enableDisableVacationResponder");
		userAccount.openUserAccountSetting(driver, enableDisableVacationResponder.get("ToFieldVR"));
		CommonFunctions.scrollsToTitle(driver, "Vacation responder");
		//CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VacationResponder"));
		log.info("Clicked on Vacation Responder");
		Thread.sleep(2000);
		if (CommonFunctions.getSizeOfElements(driver, enableDisableVacationResponder.get("VRSwitchoff")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRSwitchoff"));
		}
		Thread.sleep(2500);
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRSwitchon"));
		log.info("Turned on Vacation Respoder");
		CommonFunctions.searchAndSendKeysByXpath(driver, enableDisableVacationResponder.get("VRSubject"), enableDisableVacationResponder.get("VRSubjectData"));
		log.info("Entered Subject in Vacation Responder");
		CommonFunctions.searchAndSendKeysByXpath(driver, enableDisableVacationResponder.get("VRMessage"), enableDisableVacationResponder.get("VRMessageData"));
		log.info("Entered Message in Vacatoon Responder");
		hideKeyboard();
		Thread.sleep(2000);
		String checkcontact = driver.findElement(By.xpath(enableDisableVacationResponder.get("Contactscheck"))).getAttribute("checked");
		if (checkcontact.equalsIgnoreCase("false")) {
			CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("Contactscheck"));
			log.info("Checkded Send only to my contact");
		}
		CommonFunctions.searchAndClickByXpath(driver, enableDisableVacationResponder.get("VRDone"));
		log.info("Clicked on Done");
		driver.navigate().back();
		driver.navigate().back();
		if(!CommonFunctions.isAccountAlreadySelected(enableDisableVacationResponder.get("etouchtesttwolocation")))
		userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("etouchtesttwolocation"));
		
		//  driver.navigate().back(); 

	} catch (Exception e) {
		e.printStackTrace();
	}
}

/**
 * @TestCase  : enableVacationResponderWithSendToMyContactsOptionMailIsPresent
 * @Pre-condition :
 * @throws: Exception
 */

public void enableVacationResponderWithSendToMyContactsOptionMailIsPresent(AppiumDriver driver) {

	Map < String, String > enableDisableVacationResponder;
	try {
		enableDisableVacationResponder = commonPage
				.getTestData("enableDisableVacationResponder");

		//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
		pullToReferesh(driver);
		Thread.sleep(5000);
		pullToReferesh(driver);
		CommonFunctions.isElementByXpathDisplayed(driver, enableDisableVacationResponder.get("VerifyReceivedVResponse"));
		log.info("Verified Vacation Responder Message is Received to Account B as it is in Contacts of Account A");
		if(!CommonFunctions.isAccountAlreadySelected(enableDisableVacationResponder.get("etouchtestfourlocation")))
			userAccount.switchMailAccount(driver, enableDisableVacationResponder.get("etouchtestfourlocation"));

		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**
 * @TestCase  : enableVacationResponderWithSendToMyContactsOptionMailIsNotPresent
 * @Pre-condition :
 * @throws: Exception
 */

public void enableVacationResponderWithSendToMyContactsOptionMailIsNotPresent(AppiumDriver driver) {
	Map < String, String > enableDisableVacationResponder;
	try {
		enableDisableVacationResponder = commonPage.getTestData("enableDisableVacationResponder");
		//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
		pullToReferesh(driver);
		Thread.sleep(5000);
		pullToReferesh(driver);
		Thread.sleep(5000);
		CommonFunctions.isElementByXpathNotDisplayed(driver, enableDisableVacationResponder.get("VerifyReceivedVResponse"));
		log.info("Verified Mail is not Sent from Vacation Respoder to Account C as it is not in contacts of Accoun A");

		            
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**
 * Method to tap on email received from notification
 * @author batchi
 * @param MailSubject
 * @throws Exception
 */
public void verifyLabelFromNotification(AppiumDriver driver2) throws Exception {
    try {
        Map < String, String > verifyLabelFromNotificationData = commonPage
            .getTestData("SystemLabels");
        driver2.navigate().back();
        ((AndroidDriver) driver2).openNotifications();
        int notificationSize = driver2.findElements(By.xpath(commonElements.get("NotificationDropdown"))).size();
        for (int i = 0; i <= notificationSize; i++) {
            String notificationLabelName = CommonFunctions.searchAndGetTextOnElementByXpath(driver2, commonElements.get("NotificationLabel"));
            if (notificationLabelName.equalsIgnoreCase(verifyLabelFromNotificationData.get("ForumsLabel"))) {
                log.info("****************** Mail triggered to Forums Label ********************* " + notificationLabelName);
            } else if (notificationLabelName.endsWith("com")) {

                log.info("************* Mail is triggered to Primary Label as the Manage lables are disabled ********************* " + notificationLabelName);
            }
            /*if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("LatestEventNotification"))==true){
  	    	 commonFunction.searchAndGetTextOnElementByXpath(driver2,commonElements.get("LatestEventNotificationLabel")); 
  	     }else{
  	    	 log.info("No more notifications");
  	     }*/
            CommonPage.swipeRightToLeft(driver2, verifyLabelFromNotificationData.get("NotificationViewID"));
            Thread.sleep(3000);

        }

    } catch (Throwable e) {
        e.printStackTrace();
        commonPage.catchBlock(e, driver2, "Unable to find notifications");
    }
}

/**
 * @TestCase  : testAccountSyncWorksFornonGoogleAccounts
 * @Pre-condition :-

 * @throws: Exception
 */
public void testAccountSyncWorksFornonGoogleAccounts(AppiumDriver driver) throws Exception {
    try {
        Map < String, String > reportSpam = commonPage.getTestData("reportSpam");
        Map < String, String > ValidateReportSpamForIMAPAccounts = commonPage.getTestData("ValidateReportSpamForIMAPAccounts");
        
		String  emailId = CommonFunctions.getNewUserAccount(ValidateReportSpamForIMAPAccounts, "ImapAccount", null);

		if(!CommonFunctions.isAccountAlreadySelected(emailId))
			userAccount.switchMailAccount(driver,emailId);

        CommonFunctions.smtpMailGeneration(emailId,"TestingReportSpam", "Test", null);
        //mail.sendHtmlEmail("gm.gig1.auto@gmail.com", "mobileautomation", "etouchtestone@outlook.com", "TestingReportSpam", "Test");
        //userAccount.switchMailAccount(driver, ValidateReportSpamForIMAPAccounts.get("ImapAccount"));
        Thread.sleep(5000);
        pullToReferesh(driver);
        Thread.sleep(1000);
        CommonFunctions.isElementByXpathDisplayed(driver, reportSpam.get("ReportSpamEmail"));
        log.info("Email is displayed after intitianl sync for non Google Account");
        pullToReferesh(driver);
        log.info("Email is displayed after Pull To Refresh for non Google Account");
        CommonFunctions.isElementByXpathDisplayed(driver, reportSpam.get("ReportSpamEmail"));
        
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
}


/**
 * @TestCase   : addAccountOODwarningForOutsideTheDomainReplies
 * @Pre-condition : Gmail App should be installed in testing device and 
 * Gmail account is added to app
.    *Author Rahul kulkarni
 * @throws: Exception
 */	
public void addAccountOODwarningForOutsideTheDomainReplies(AppiumDriver driver) throws Exception {
	try{
	    	
		Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		Map<String, String> OODwarningForOutsideTheDomainReplies = commonPage.getTestData("OODwarningForOutsideTheDomainReplies");
		Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		if(!CommonFunctions.isElementByXpathDisplayed(driver, OODwarningForOutsideTheDomainReplies.get("qa1@dynamitegroup.net")))
		  {	
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("addaccount"));
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("clickgoogle"));
			Thread.sleep(5000);
			CommonFunctions.searchAndSendKeysByXpath(driver, OODwarningForOutsideTheDomainReplies.get("enteremail"), OODwarningForOutsideTheDomainReplies.get("enteremailtext"));
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("clicknext"));
			CommonFunctions.searchAndSendKeysByXpath(driver, OODwarningForOutsideTheDomainReplies.get("enterepassword"), OODwarningForOutsideTheDomainReplies.get("enterpasswordtext"));
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("clicknext"));
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("clickaccept"));
			CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("qa1@dynamitegroup.net"));
			Thread.sleep(20000);
			
		  }
	 	   else 
	 	   { 
	 		  CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("qa1@dynamitegroup.net"));
	 		   //CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	 	    }
		
		if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExpandAccountList")))
		    {
			  driver.navigate().back();
		     }
		
} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


/**
 * @TestCase   : sendMailToaddAccountOODwarningForOutsideTheDomainReplies
 * @Pre-condition : Gmail App should be installed in testing device and 
 * Gmail account is added to app
.    *Author Rahul kulkarni
 * @throws: Exception
 */	
public void sendMailToaddAccountOODwarningForOutsideTheDomainReplies(AppiumDriver driver) throws Exception {
	try{
	    	
		Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		Map<String, String> OODwarningForOutsideTheDomainReplies = commonPage.getTestData("OODwarningForOutsideTheDomainReplies");
		Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
	 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
		  {	
	 		 driver.navigate().back();
		  }
	 	  else
	 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
	      }
	 	   
	 	Thread.sleep(5000);
	 	   String fromUser=composeAsNewMail(driver,
	 	   OODwarningForOutsideTheDomainReplies.get("ToField"),
	       OODwarningForOutsideTheDomainReplies.get("CcField"),
	       OODwarningForOutsideTheDomainReplies.get("Bcc"),
	       OODwarningForOutsideTheDomainReplies.get("Subject"),
	 	   OODwarningForOutsideTheDomainReplies.get("ComposeBody"));			
	       CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("SendMailButton"));
	       CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
	       CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	       CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("qa1@dynamitegroup.net"));
	       pullToReferesh(driver);

	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


/**
 * @TestCase   : verifyOODwarningForOutsideTheDomainReplies
 * @Pre-condition : Gmail App should be installed in testing device and 
 * Gmail account is added to app
.    *Author Rahul kulkarni
 * @throws: Exception
 */	
public void verifyOODwarningForOutsideTheDomainReplies(AppiumDriver driver) throws Exception {
	try{
	    	
		Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		Map<String, String> OODwarningForOutsideTheDomainReplies = commonPage.getTestData("OODwarningForOutsideTheDomainReplies");
		Map<String, String> composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);	
		CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("ClickOODmail"));
	    CommonFunctions.searchAndClickByXpath(driver, OODwarningForOutsideTheDomainReplies.get("clickreply"));
	    Thread.sleep(5000);
	   // CommonFunctions.tapUsingXYCordinates(driver, OODwarningForOutsideTheDomainReplies.get("Iconxcordinate"), OODwarningForOutsideTheDomainReplies.get("Iconycordinate"));
	    
	    Thread.sleep(5000);
	    CommonFunctions.isElementByXpathDisplayed(driver, OODwarningForOutsideTheDomainReplies.get("OODWarningMessage"));
		 
	 } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

/**- New method to verify OOD warning
 * 
 * @param driver
 * @throws Exception
 */
public void verifyOODwarning(AppiumDriver driver) throws Exception {
	Map<String, String> verifySmime = commonPage
			.getTestData("OODwarningForOutsideTheDomainReplies");

	try{
	    	
		driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);	
		CommonFunctions.searchAndClickByXpath(driver, verifySmime.get("clickreply"));
		Thread.sleep(1500);
		log.info(deviceName);
		if(deviceName.equalsIgnoreCase("Samsung S7")){
			CommonFunctions.tapUsingCoordinates(driver, 669, 498);
		}else if(deviceName.equalsIgnoreCase("Nexus 5X")){
			log.info("Device is Nexus 5X");
			try{
			CommonFunctions.tapUsingCoordinates(driver, 581, 438);
			}catch(Exception e){
				CommonFunctions.tapUsingCoordinates(driver, 580, 436);
			}
		}
		try{
	    CommonFunctions.isElementByXpathDisplayed(driver, verifySmime.get("OODWarningMessage"));
	    log.info("OOD warning is displayed");
	    driver.navigate().back();
		}catch(Exception e){
			throw new Exception("OOD warning is not verified, hence verify once whether the icon is not displayed r add device specific statement");
		}
		Thread.sleep(800);
		//CommonFunctions.searchAndClickByXpath(driver, verifySmime.get("ClickComposeMail"));
		CommonFunctions.searchAndSendKeysByXpath(driver, verifySmime.get("ClickComposeMail"), "Draft");
		driver.navigate().back();
		driver.navigate().back();
		Thread.sleep(800);
		CommonFunctions.searchAndClickByXpath(driver, verifySmime.get("EditDraft"));
		Thread.sleep(1000);
		if(deviceName.equalsIgnoreCase("Samsung S7")){
			CommonFunctions.tapUsingCoordinates(driver, 669, 498);
		}else if(deviceName.equalsIgnoreCase("Nexus 5X")){
			log.info("Device is Nexus 5X");
			CommonFunctions.tapUsingCoordinates(driver, 581, 438);
		}
		try{
	    CommonFunctions.isElementByXpathDisplayed(driver, verifySmime.get("OODWarningMessage"));
	    log.info("OOD warning is displayed");
	    driver.navigate().back();
		}catch(Exception e){
			throw new Exception("OOD warning is not verified, hence verify once whether the icon is not displayed r add device specific statement");
		}
		Thread.sleep(800);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		Thread.sleep(800);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardMail"));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


/**Method to compose and send email with attachments
 * 
 * @param driver
 * @author Rahul
 * @throws Exception 
 */
public void composeAndSendMailForGmailAccountWithAttachmentsGIG(AppiumDriver driver) throws Exception{
    try{
 	   Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

 	   log.info("******************* Start composeAndSendMailForGmailAccountWithAttachments Functionality*******************");
     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
	  {	
 		 driver.navigate().back();
	  }
 	  else
 	  { 
 		  CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
 	  	  CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
 	  	  log.info("Switched to other account etouchtestone || Switched to account gm.gig1");
 	  }
 	   
 	 	Thread.sleep(2000);
 	 	String fromUser=composeAsNewMail(driver,
				        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG2"),
				        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG1"),
				        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG3"),
				        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SubjectAttachments"),
				        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));			
	 	   
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
		log.info("Attach button clicked");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
		log.info("Attach button clicked");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
		log.info("Show drive clicked");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
		log.info("etouchtestone drive clicked || gm.gig1 is clicked");
		pullToReferesh(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
		log.info("Attachment1 clicked");		
	
		Thread.sleep(2500);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
		log.info("Attach button clicked");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
		log.info("Attach button clicked");
		Thread.sleep(2500);
		/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
		log.info("Show drive clicked");
		Thread.sleep(5000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
		Thread.sleep(5000);
		log.info("etouchtestone drive clicked");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
		log.info("etouchtestone drive clicked");
		scrollDown(1);
		*/CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MyDriveFileName2"));
		log.info("Attachment1 clicked");	
		CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
		Thread.sleep(800);
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		}
		
		Thread.sleep(2000);
		/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		log.info("verifying account list etouchtesttwo");*/
		if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG2")))
		userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG2"));
		
		//CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtesttwo"));
		Thread.sleep(2000);
		pullToReferesh(driver);
		Thread.sleep(3000);
		log.info("Verifying the attachment");
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("Verifyattachmentdraft1"))!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
		}else{
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
			log.info("Attachment found and clicked");
		}
		
		Thread.sleep(2500);
		scrollDown(1);
		log.info("scrolled to Getting Started attachment");
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("MyDriveFileName2"));
	 	Thread.sleep(2000);
	 	driver.navigate().back();
	 	/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		log.info("verifying account list etouchtestthree");
		CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestthree"));
		Thread.sleep(2000);
		*/
	 	if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG3")))
		userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG3"));

	 	//pullToReferesh(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
		Thread.sleep(2000);
		scrollDown(1);
	 	log.info("scrolled to Getting Started attachment");
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("MyDriveFileName2"));
	 	driver.navigate().back();
	 	/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		log.info("verifying account list etouchtestone");
		CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));*/
	 	if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG1")))
	 		userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToGIG1"));

		pullToReferesh(driver);
		Thread.sleep(3000);
		pullToReferesh(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
		Thread.sleep(4000);
		scrollDown(1);
	 	log.info("scrolled to Geftting Started attachment");
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("AppiumImage"));
	 	CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("MyDriveFileName2"));
	 	log.info("verified second attachments is displyaed");
	 	log.info("******************* Completed testcase composeMailWithMultipleAttachmentsAndAnsertedDriveChips Functionality*******************");
	 	
		}catch(Exception e){
			throw new Exception("Unable to attach file "+e.getLocalizedMessage());
		}
}

/**
 * Method to verify delete, archive  and Move To from TL view
 * @author batchi
 * @param MailSubject
 * @throws Exception
 */
public void verifyDeleteArchiveAndMoveToFromTL(AppiumDriver driver,String mailSubject) throws Exception {
	try {
		log.info("In verify Delete Archive And MoveTo From TL view");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
		Thread.sleep(1500);
		commonPage.verifyArchive(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
		Thread.sleep(1500);
		commonPage.verifyDelete(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
		Thread.sleep(1500);
		commonPage.verifyMoveTo(driver);
	}catch(Exception e){
		throw e;
	}
}
/**
 * Method to verify delete, archive  and Move To from TL view
 * @author batchi
 * @param MailSubject
 * @throws Exception
 */
public void verifyDeleteArchiveAndMoveToFromCV(AppiumDriver driver,String mailSubjectForArchive,String mailSubjectForDelete,String mailSubjectForMoveTo) throws Exception {
	try {
		log.info("In verify Delete Archive And MoveTo From CV view");
		inbox.openMail(driver,mailSubjectForArchive );
		//commonFunction.searchAndClickByXpath(driver, commonElements.get("OpenMail"));
		commonPage.verifyArchive(driver);
	    Thread.sleep(2000);
	    inbox.openMail(driver,mailSubjectForDelete );
	    //commonFunction.searchAndClickByXpath(driver, commonElements.get("OpenMail"));
	   	commonPage.verifyDelete(driver);
		Thread.sleep(2000);
		inbox.openMail(driver, mailSubjectForMoveTo);
		//commonFunction.searchAndClickByXpath(driver, commonElements.get("OpenMail"));
		commonPage.verifyMoveTo(driver);
		Thread.sleep(2000);
	}catch(Exception e){
		throw e;
	}
}

/**Method to  forwardAnHtmlEmail
 * 
 * @param driver
 * @throws Exception
 */
public void forwardAnHtmlEmail(AppiumDriver driver)throws Exception{
	try{
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		Map<String, String> richTextFormattingFeatures = commonPage
				.getTestData("richTextFormattingFeatures");	
     /*  log.info("******************* Start forwardAnHtmlEmail*******************");
       CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
       log.info("clicked on Hamburger");
	 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
		  {	
		   driver.navigate().back();
		  }
	 	   else
	 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
          }
	    Thread.sleep(10000);
	 	pullToReferesh(driver);
	 	Thread.sleep(5000);
	    CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("actionBarMenuOptionXpath"));
	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Forward"));
	 	CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ToFeild"),composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("BccFieldAttachments"));
	 	*/CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
	 	//driver.navigate().back();
	 	Thread.sleep(1000);
	 	commonPage.navigateBack(driver);
	 	pullToReferesh(driver);
	 	openMail(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	 	//CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ClickForwardAnHtmlMail"));
	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyButton"));
	    Thread.sleep(5000);
	 	//hideKeyboard();
	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RespondInLineOption"));
	 	hideKeyboard();
	 	Thread.sleep(5000);
	 	commonPage.scrollDown(1);
	 	log.info("Tapping on the text");
	 	driver.tap(1,190,997,1000);
	 	log.info("Tapped on the text");
	 	Thread.sleep(5000);
	 	CommonFunctions.searchAndClickByXpath(driver, richTextFormattingFeatures.get("clickformat"));
	 	String actualIsTextBold =driver.findElement(By.xpath(richTextFormattingFeatures.get("clickbold"))).getAttribute("checked");
		if(actualIsTextBold.equalsIgnoreCase(richTextFormattingFeatures.get("boldtrue")))
		{
			log.info("Mail is sent with Same HTML Content");
		}
		else
		{
			throw new Exception("Mail is not sent with Same HTML Content");
		}
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DiscardMail"));
	}catch(Exception e){
		throw new Exception("Unable to verify the archive and delete options for the mail "+e.getMessage());
	}
}

/**Method to verify the settings translations for the preferred locale
 * 
 * @param driver
 * @throws Exception
 */
 public void verifyTheLabelListForGmailifiedAccount(AppiumDriver driver)throws Exception{
	try{
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		/*scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));*/
		CommonFunctions.scrollsToTitle(driver, "Settings");
		Thread.sleep(1000);
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("LinkedAccount"));
		/*scrollDown(1);
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("Managelables"));*/
		CommonFunctions.scrollsToTitle(driver, "Manage labels");
		CommonFunctions.isElementByXpathNotDisplayed(driver,commonElements.get("spam"));
		log.info("Spam is not displayed in Manage Labels");
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		
	}catch(Exception e){
		throw new Exception("Unable to verify the Settings option "+e.getMessage());
	}
}

 /**
 * @TestCase  : certificateTransparency
 * @Pre-condition :-

 * @throws: Exception
 */ 
 public void certificateTransparency(AppiumDriver driver) throws Exception {


 try{
 	
 	Map<String, String> certificateTransparency= commonPage.getTestData("certificateTransparency");
 	userAccount.clickAddAccount(driver);
 	
 	/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
    log.info("Clicked Hamburger");
 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
 	log.info("Clicked Epanded List");
 	scrollDown(2);
 	CommonFunctions.scrollTo(driver, "Add account");
 	
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("AddAccount"));
 	log.info("Clicked Addaccount");*/
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Other"));
 	log.info("Clicked Other");
 	hideKeyboard();
 	CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("EmailidTextBox"), certificateTransparency.get("Emailid"));
 	log.info("Entered Username");
 	hideKeyboard();
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("IMAPAccount"));
 	log.info("Clicked IMAP Account");
 	
 	//commented the below code as it is required -- Bindu
 	/*CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");*/
 	hideKeyboard();
     CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("PasswordTextBox"), certificateTransparency.get("Password"));
    log.info("Entered Password");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("CertificateNotValid"));
 	log.info("Verified Certificate not valid message is displayed");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Advanced"));
 	log.info("Clicked Advenced");
 	scrollDown(3);
 	commonPage.waitForElementToBePresentByXpath(certificateTransparency.get("ProceedAnyway"));
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ProceedAnyway"));
 	log.info("Clicked ProceedAnyway");
 	Thread.sleep(5000);
 	CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("UsernameTextbox"), certificateTransparency.get("Username"));
 	log.info("Entered Username");
 	hideKeyboard();
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSecurityType"));
 	log.info("Clicked SecurityType");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSTARTTLSAcceptallcertificates"));
 	log.info("Clicked STARTLS Accept All Certificates");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickEditSettings"));
 	log.info("Clicked Edit Settings");
 	scrollDown(2);
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickEditSettings"));
 	log.info("Clicked Edit Settings");
 	scrollDown(2);
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSecurityType"));
 	log.info("Clicked Security Type");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSTARTTLSAcceptallcertificates"));
 	log.info("Clicked STARTLS Accept All Certificates");
 	CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("EmailSecuritynotguaranteed"));
 	log.info("Verified EmailSecuritynotguaranteed message is displayed");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("UsernameTextbox"), certificateTransparency.get("Username"));
 	log.info("Entered Username");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
 	log.info("Clicked Next");
 	CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("VerifyExchangeacccount"));
 	log.info("Exchange Account is Added Successfully");
 	
 	/*scrollDown(2); 	
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Manage Account"));
 	Thread.sleep(5000);
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("PersonalIMAP"));
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ExchangeAccount"));
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("OverFlow"));
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("RemoveAccount"));
 	CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("RemoveAccountConfirm"));
     log.info("Account is removed");*/
 	
 } catch (Exception e) {
 	// TODO Auto-generated catch block
 	e.printStackTrace();
 }
 }
 
 /**
	 * @TestCase  :  verifySafeLinksFunctionality
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
public void verifySafeLinksFunctionality(AppiumDriver driver) throws Exception {
		try{
			log.info("******************* Start verifySafeLinksV1Functionality*******************");
			Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			Map<String, String>verifySafeLinksFunctionality  = commonPage.getTestData("verifySafeLinksFunctionality ");
		 	log.info("******************* Start verifySafeLinksV1Functionality*******************");
		 	if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestOneAccount")))
		 		userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestOneAccount"));
		 	inbox.searchForMail(driver,
		 			verifySafeLinksFunctionality.get("MailSubject"));
		    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickSafeLinkMail"));
		    log.info("clicked on Safe links Mail");
		    Thread.sleep(4000);
		    if(CommonFunctions.getSizeOfElements(driver, verifySafeLinksFunctionality.get("MailSnippet"))!=0){
		    	CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("MailSnippet"));
		    	log.info("Mail expanded");
		    }
		    commonPage.waitForElementToBePresentByXpath(verifySafeLinksFunctionality.get("ClickGenericLink"));
		    
		    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickGenericLink"));
		    log.info("clicked on Generic link");
		    Thread.sleep(2000);
		    if(CommonFunctions.getSizeOfElements(driver, verifySafeLinksFunctionality.get("Clickjustonce"))!=0){
			   CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("Clickjustonce"));
			   log.info("clicked on just once");
		    }
		    CommonFunctions.isElementByXpathDisplayed(driver, verifySafeLinksFunctionality.get("VerifyRedirectorLinkExpires"));
	        log.info("verified  Redirector Link is expired");
	        driver.navigate().back();
	        CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickYoutubelink"));
            log.info("clicked on youtube link in Mail");
            Thread.sleep(5000);
            if(CommonFunctions.getSizeOfElements(driver, verifySafeLinksFunctionality.get("YoutubeHomepageUrlbar"))==0){
            	log.info("youtube App is opened");
            }
   		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

/**
	 * @TestCase  :  verifySafeLinksFunctionalityv2(
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
public void verifySafeLinksFunctionalityv2(AppiumDriver driver) throws Exception {
		try{
			log.info("******************* Start verifySafeLinksV1Functionality*******************");
			Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			Map<String, String> verifySafeLinksFunctionality  = commonPage.getTestData("verifySafeLinksFunctionality ");
			log.info("******************* Start verifySafeLinksV1Functionality*******************");
			
			String primaryAccountNew = CommonFunctions.getNewUserAccount(verifySafeLinksFunctionality, "Account", null);
			
			CommonFunctions.smtpMailGeneration(primaryAccountNew,"test", "SafeLinks.html", null);
			
			if(!CommonFunctions.isAccountAlreadySelected(primaryAccountNew))
				userAccount.switchMailAccount(driver, primaryAccountNew); 
			
		    pullToReferesh(driver);
		    log.info("Safe Links Mail is searched and displayed");
		    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickSafeLinksv2test"));
		    log.info("clicked on Safe links Mail");
		    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickPintoandroid"));
		    log.info("clicked on Generic link");
		    if(CommonFunctions.getSizeOfElements(driver, verifySafeLinksFunctionality.get("VerifySuspuciousDialogbox"))!=0){
     	        log.info("suspicious link dialog is opened");
			}
	  	    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("clickCancel"));
		    log.info("clicked on cancel button and verified dialog box is closed");
	        CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickPintoandroid"));
		    log.info("clicked on Generic link");
		    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("ClickProceed"));
	        log.info("clicked on proceedbutton");
		    if(CommonFunctions.getSizeOfElements(driver, verifySafeLinksFunctionality.get("Clickjustonce"))!=0){
			    CommonFunctions.searchAndClickByXpath(driver, verifySafeLinksFunctionality.get("Clickjustonce"));
			    log.info("clicked on just once");
		    }
		    CommonFunctions.isElementByXpathDisplayed(driver, verifySafeLinksFunctionality.get("VerifyRedirectorLinkExpires"));
	        log.info("verified Link is opened");
	        driver.navigate().back();
	        driver.navigate().back();
		    } catch (Exception e) {
			    e.printStackTrace();
			    throw new Exception("Suspicious dialog is not opened");
		}
	}

/**
	 * @TestCase  : verifyReportNotSuspiciousOption
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
 public void verifySuspiciousBannerDisplay(AppiumDriver driver) throws Exception {
		try{
			  Map<String, String>verifyReportNotSuspiciousOption = commonPage.getTestData("verifyReportNotSuspiciousOption");
		 	  log.info("******************* Start verifyReportNotSuspiciousOption*******************");
		 	//  userAccount.switchMailAccount(driver, verifyReportNotSuspiciousOption.get("Account"));
		 	  pullToReferesh(driver);
		 	  commonPage.waitForElementToBePresentByXpath(verifyReportNotSuspiciousOption.get("ClickSuspiciousMail"));
		 	  CommonFunctions.searchAndClickByXpath(driver, verifyReportNotSuspiciousOption.get("ClickSuspiciousMail"));
		      log.info("clicked on Suspicious Mail");
			  CommonFunctions.isElementByXpathDisplayed(driver, verifyReportNotSuspiciousOption.get("VerifySuspiciousBanner"));
			  log.info("verified Suspicious Banner is displayed");
			 // CommonFunctions.isElementByXpathDisplayed(driver, verifyReportNotSuspiciousOption.get("BannerText"));
			 /* CommonFunctions.searchAndClickByXpath(driver, verifyReportNotSuspiciousOption.get("clickMore"));
			  log.info("clicked on More Option");   		
			  CommonFunctions.isElementByXpathDisplayed(driver, verifyReportNotSuspiciousOption.get("VerifyReportNotSuspiciousOption"));
		      log.info("verirified Report Not Suspicious Option is displayed");
			*/} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Banner not verified");
		}
	}

 /**
	 * @TestCase  : verifyReportNotSuspiciousOption
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
public void verifyReportNotSuspiciousOption(AppiumDriver driver) throws Exception {
		try{
			   Map<String, String>verifyReportNotSuspiciousOption = commonPage.getTestData("verifyReportNotSuspiciousOption");
		 	   log.info("******************* Start verifyReportNotSuspiciousOption*******************");
			   CommonFunctions.searchAndClickByXpath(driver, verifyReportNotSuspiciousOption.get("clickMore"));
			   log.info("clicked on More Option");   		
			   CommonFunctions.isElementByXpathDisplayed(driver, verifyReportNotSuspiciousOption.get("VerifyReportNotSuspiciousOption"));
		       log.info("verirified Report Not Suspicious Option is displayed");
		       CommonFunctions.searchAndClickByXpath(driver, verifyReportNotSuspiciousOption.get("VerifyReportNotSuspiciousOption"));
		       driver.navigate().back();
		       pullToReferesh(driver);
		       CommonFunctions.searchAndClickByXpath(driver, verifyReportNotSuspiciousOption.get("ClickSuspiciousMail"));
			   log.info("clicked on Suspicious Mail");
			   CommonFunctions.isElementByXpathNotDisplayed(driver, verifyReportNotSuspiciousOption.get("BannerText"));
			   driver.navigate().back();   
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Report not suspicious is not verified");
		}
	}


 /**Method to verifyMailWithUnauthenticatedAvatarAndWarning
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyMailWithUnauthenticatedAvatarAndWarning(AppiumDriver driver)throws Exception{
		try{
		
		
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		Map<String, String> reportSpam = commonPage.getTestData("reportSpam");
		Map<String, String> certificateTransparency= commonPage.getTestData("certificateTransparency");
	    log.info("******************* Start verifyMailWithUnauthenticatedAvatarAndWarning*******************");
	    if(!CommonFunctions.isAccountAlreadySelected(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("eaitest101")))
	    userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("eaitest101"));
	    inbox.openMailUsingSearch(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("avatarSubject"));
	    Thread.sleep(4000);
	   // CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("avatarMail"));
	    commonPage.waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("UnauthBannerText"));
	    log.info("Verified unauth banner text");
	    
	    CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ContactBadgeInCV"));
	    log.info("Clicked on contact badge");
	    commonPage.waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ProfileHeaderAvatar"));
	    log.info("Smart profile is displayed after clicked on avatar");
	    commonPage.waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("UnauthBannerTextSmartProfile"));
	    log.info("Unauth banner text displayed for smart profile");
	    
	    /*
	    
	    
	    
	    
	    
	    
	    CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
	    log.info("Clicked Hamburger");
	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
	 	log.info("Clicked Epanded List");
	 	//CommonFunctions.scrollDown(driver, 2);
	 	CommonFunctions.scrollTo(driver, "Add account");
	 	
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("AddAccount"));
		log.info("Clicked Addaccount");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Other"));
		log.info("Clicked Other");
		hideKeyboard();
		CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("EmailidTextBox"), certificateTransparency.get("Emailid"));
		log.info("Entered Username");
		hideKeyboard();
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("IMAPAccount"));
		log.info("Clicked IMAP Account");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		hideKeyboard();
	    CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("PasswordTextBox"), certificateTransparency.get("Password"));
	   log.info("Entered Password");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("CertificateNotValid"));
		log.info("Verified Certificate not valid message is displayed");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Advanced"));
		log.info("Clicked Advenced");
		scrollDown(3);
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ProceedAnyway"));
		log.info("Clicked ProceedAnyway");
		Thread.sleep(5000);
		CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("UsernameTextbox"), certificateTransparency.get("Username"));
		log.info("Entered Username");
		hideKeyboard();
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSecurityType"));
		log.info("Clicked SecurityType");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSTARTTLSAcceptallcertificates"));
		log.info("Clicked STARTLS Accept All Certificates");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickEditSettings"));
		log.info("Clicked Edit Settings");
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickEditSettings"));
		log.info("Clicked Edit Settings");
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSecurityType"));
		log.info("Clicked Security Type");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ClickSTARTTLSAcceptallcertificates"));
		log.info("Clicked STARTLS Accept All Certificates");
		CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("EmailSecuritynotguaranteed"));
		log.info("Verified EmailSecuritynotguaranteed message is displayed");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndSendKeysByXpath(driver, certificateTransparency.get("UsernameTextbox"), certificateTransparency.get("Username"));
		log.info("Entered Username");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Next"));
		log.info("Clicked Next");
		CommonFunctions.isElementByXpathDisplayed(driver,certificateTransparency.get("VerifyExchangeacccount"));
		log.info("Exchange Account is Added Successfully");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[@text='Add account']");
		userAccount.addNewGmailAccount("eaitest101@gmail.com","eaitest123");
		driver.navigate().back();
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
      log.info("clicked on Hamburger");
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, certificateTransparency.get("emailid1policy")))
			  {	
			   driver.navigate().back();
			  }
		 	   else
		 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 	 CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("emailid1policy"));
	          }
		
		 	String fromUser=inbox.composeAndSendNewMail(driver,"eaitest101@gmail.com","","","Maybe","Maybe");
		 	
		   
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
        log.info("clicked on Hamburger");
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("eaitest101")))
			  {	
			   driver.navigate().back();
			  }
		 	   else
		 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("eaitest101"));
	          }
		 	 Thread.sleep(5000);
		 	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClickQASpamLabelMail"));
          Thread.sleep(5000);
		
          
         Assert.assertTrue(commonPage.getScreenshotAndCompareImage("verifyMailWithUnauthenticatedAvatarAndWarning"),
		 	"Image comparison for verifyMailWithUnauthenticatedAvatarAndWarning");
		 	
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("QASpamLabelWarningMessage"))!=0){
       	  log.info("UnAuthenticated Warning Message is displayed");
			}
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("QASpamLabelIcon"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("QASpamLabelWarningMessage"))!=0){
       	  log.info("UnAuthenticated Warning Message is displayed");
			}
			
		driver.navigate().back();
		driver.navigate().back();
		 
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu")); 
		scrollDown(2);
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("Manage Account"));
		Thread.sleep(5000);
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("PersonalIMAP"));
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("ExchangeAccount"));
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("OverFlow"));
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("RemoveAccount"));
		CommonFunctions.searchAndClickByXpath(driver, certificateTransparency.get("RemoveAccountConfirm"));
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().back();
		userAccount.removeAccount(driver,
					"eaitestone@gmail.com", composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("eaitest101"));
	*/		
}catch(Exception e){
			throw new Exception("Unable to verify the avatar and warning "+e.getMessage());
		}
	}
	
	public void changeLabelsWeb(AppiumDriver driver) throws Exception {
		try{
			 Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
			 Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
			 Map<String, String>testAccountSyncWorksForGoogleAccounts = commonPage.getTestData("testAccountSyncWorksForGoogleAccounts");	
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
			  {	driver.navigate().back();
			  }
		 	  else
		 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
		 	   }
		 	   Thread.sleep(10000);
		 	  String fromUser=composeAsNewMail(driver,
		 			  changeLabels.get("ToFieldData"),
		 			  changeLabels.get("CCFieldData"),
		 			  changeLabels.get("BccFieldData"),
		 			  changeLabels.get("SubjectData"),
		 			  changeLabels.get("ComposeBodyData"));	
		 	    CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
				log.info("clicked on more options");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				log.info("account list");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtesttwo"));
				pullToReferesh(driver);
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ChangeLableEmail"));
				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOption"));
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickChangeLabel"));
				 String isLabelChecked=driver.findElement(By.xpath(changeLabels.get("CheckChangeLabel"))).getAttribute("checked");
				 if(isLabelChecked.equalsIgnoreCase("false"))
				 {
					 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("CheckChangeLabel"));				
				 }
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("ClickOkChangeLabel"));
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NavigateUp"));
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			     CommonFunctions.scrollTo(driver,"TestLabel");
			     CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("TestLabel"));
			     CommonFunctions.isElementByXpathDisplayed(driver,changeLabels.get("ChangeLableEmail"));
			     log.info("message is displayed in Test Label");
				 
                 log.info("Launching Chrome");
                 Process p;
					String[] launchChromeBrowser1 = new String[]{"adb","shell", "monkey", "-p", "com.android.chrome", "-c", "android.intent.category.LAUNCHER 1"};
					    p = new ProcessBuilder(launchChromeBrowser1).start();
					    log.info("Launched Chrome Browser");
					    Thread.sleep(5000);
					    driver.findElement(By.xpath("//android.widget.ImageButton[@resource-id='com.android.chrome:id/tab_switcher_button']")).click();
					    Thread.sleep(5000);
					    driver.findElement(By.xpath("//android.widget.ImageButton[@resource-id='com.android.chrome:id/menu_button']")).click();
					    Thread.sleep(5000);
					    driver.findElement(By.xpath("//android.widget.TextView[@content-desc='New tab']")).click();
					    Thread.sleep(5000);
					    
					  if(driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("GmailTypeInSearchBox"))).isDisplayed())
					    {
					   driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("GmailTypeInSearchBox"))).sendKeys("http://gmail.com ");
					    Thread.sleep(5000);
					   ((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
					   Thread.sleep(5000);
		                }
					   else
					   { driver.findElement(By.xpath("//android.widget.EditText[@text='Search or type URL']")).sendKeys("http://gmail.com");
						   ((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
					  }
					   Thread.sleep(5000);
					   
					 if(CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail")))
							 { 
					   CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail"),testAccountSyncWorksForGoogleAccounts.get("etouchtesttwoemailid"));
					   log.info("Entered user name");
					   hideKeyboard();
					    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
					    Thread.sleep(5000);
					    //hideKeyboard();  
					    //driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enterpasssword"))).clear();
					    CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enterpasssword"),testAccountSyncWorksForGoogleAccounts.get("password"));
					    hideKeyboard();
					    log.info("Entered passwor");
					    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
					    //Thread.sleep(10000);
					  }
					 CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("ClickMenu"));
					 
					 log.info("clicked on menu");
					 Thread.sleep(10000);
					 driver.findElement(By.xpath("//android.widget.Button[contains(@content-desc,'gmail.com')]")).click();
					 
					 log.info("clicked on Email Account");
					 
					 //driver.findElement(By.xpath("//android.view.View[@index='0'] | //android.widget.Button[@index='0']")).click();
					Thread.sleep(10000);
					if(CommonFunctions.isElementByXpathDisplayed(driver,"//android.widget.Button[contains(@content-desc,'etouchtesttwo')]"))
					{
					 CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[contains(@content-desc,'etouchtesttwo')]");
					}
		            else{
		        	  CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[@content-desc='Add account']"); 
		        	  Thread.sleep(5000);
		        	  driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enteremail"))).clear();
		        	  CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail"),testAccountSyncWorksForGoogleAccounts.get("etouchtesttwoemailid"));
					   log.info("Entered user name");
					   hideKeyboard();
					    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
					    Thread.sleep(5000);
					    //hideKeyboard();  
					    driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enterpasssword"))).clear();
					    CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enterpasssword"),testAccountSyncWorksForGoogleAccounts.get("password"));
					    hideKeyboard();
					    log.info("Entered passwor");
					    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
		          }
						 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("clickgmail"));
					    Thread.sleep(5000);
					    CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("ClickMenu"));
					    Thread.sleep(5000);
					    log.info("Clicked Menu");
					    CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("TestLabelWeb"));
					    log.info("Clicked on Test Label");
					    Thread.sleep(5000);
					    CommonFunctions.isElementByXpathDisplayed(driver,changeLabels.get("CheckChangeLabelEmailWeb"));
					    log.info("verified on the mail is dislayed on web in Test Label");
		
	}catch (Exception e) {
			// TODO Auto-generated catch block/
		}
	}
	
	public void inputMethod_Arabic(AppiumDriver driver)throws Exception{
		try{
		    Map<String, String>inputMethodArabic= commonPage.getTestData("inputMethodArabic");
			 /*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 	 if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone")))
			  {	
			  	
			  driver.navigate().back();
			  }
		 	  
		      else
		 	  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 	 CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchtestone"));
		 	   }
		 	   */
		    if(!CommonFunctions.isAccountAlreadySelected(inputMethodArabic.get("ToFeilddata")))
		     userAccount.switchMailAccount(driver, inputMethodArabic.get("ToFeilddata"));	
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("compose"));
			 CommonFunctions.searchAndSendKeysByXpath(driver, inputMethodArabic.get("ToFeild"),inputMethodArabic.get("Email"));
			 Thread.sleep(15000);
			 driver.tap(1, 378, 1122, 1000);
			 Thread.sleep(10000);
			 String ArabicChecked =driver.findElement(By.xpath(inputMethodArabic.get("Arabicradio"))).getAttribute("checked");
			 if(ArabicChecked.equalsIgnoreCase("false")){
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("Arabicradio"));
			 }
			 else{
		 	 driver.navigate().back();
			 }
			 Thread.sleep(5000);
			 
			 driver.tap(1, 63, 553, 1000);
			 driver.tap(1, 40, 787, 1);
                             log.info("Input VerificationTarget language Arabic is Entered correctly"); 
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("send"));
			 
		}catch(Exception e){
			throw new Exception("Unable to verify the Settings option "+e.getMessage());
		}
	}
	

	/**Method to verify the settings translations for the preferred locale
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void inputMethod_Korean(AppiumDriver driver)throws Exception{
		try{
			
			 Map<String, String>inputMethodArabic= commonPage.getTestData("inputMethodArabic");
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("compose"));
			 CommonFunctions.searchAndSendKeysByXpath(driver, inputMethodArabic.get("ToFeild"),inputMethodArabic.get("Email"));
			 Thread.sleep(15000);
			 driver.tap(1, 378, 1122, 1000);
			 Thread.sleep(10000);
			 String KoreanChecked=driver.findElement(By.xpath(inputMethodArabic.get("KoreanRadio"))).getAttribute("checked");
			 if(KoreanChecked.equalsIgnoreCase("false")){
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("KoreanRadio"));
			 }
			 else{
		 	 driver.navigate().back();
			 }
			 Thread.sleep(5000);
			 
			 driver.tap(1, 63, 553, 1000);
			 driver.tap(1, 38, 811, 1);
                           log.info("Input VerificationTarget language Korean is Entered correctly");
			 CommonFunctions.searchAndClickByXpath(driver, inputMethodArabic.get("send"));
			 Thread.sleep(5000);
			 pullToReferesh(driver);
			 CommonFunctions.isElementByXpathDisplayed(driver, inputMethodArabic.get("VerifyArabicMail"));
                             log.info("Output VerificationTarget language Arabic  is displayed correctly");
			 CommonFunctions.isElementByXpathDisplayed(driver, inputMethodArabic.get("VerifyKoreanMail"));
                            log.info("Output VerificationTarget language Korean  is displayed correctly");
			 
			 
		}catch(Exception e){
			throw new Exception("Unable to verify the Settings option "+e.getMessage());
		}
	}
	
	/**
	 * @throws Exception 
	 * @TestCase  : verifytestAccountSyncWorksForGoogleAccounts
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
	public void verifyPeriodicSync(AppiumDriver driver) throws Exception  {
	try {
		log.info("Setting the periodic sync");
		Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage.getTestData("testAccountSyncWorksForGoogleAccounts");
		
		String primaryAccountNew = testAccountSyncWorksForGoogleAccounts.get("PrimaryAccountNew");
		String accountXPath = testAccountSyncWorksForGoogleAccounts.get("EtouchSyncAccount");
		
		if(null != primaryAccountNew && !primaryAccountNew.isEmpty()) {
			accountXPath = "//android.widget.TextView[@text='"+primaryAccountNew+"']";
		}
		
		Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
		driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Open navigation drawer'] | //android.widget.ImageButton[@content-desc='Navigate up']")).click();
		Thread.sleep(2000);
		CommonFunctions.scrollsToTitle(driver, "Settings");
		CommonFunctions.searchAndClickByXpath(driver,accountXPath);
		CommonFunctions.scrollsToTitle(driver, "Days of mail to sync");
		
		log.info("Clicked on days of mail to sync");
		Thread.sleep(2500);
		if(driver.findElements(By.xpath(testAccountSyncWorksForGoogleAccounts.get("SyncNoOfDays"))).size()!=0){
			log.info("Entering text for days of mail to sync");
			driver.findElementByXPath(testAccountSyncWorksForGoogleAccounts.get("SyncNoOfDays")).click();
			driver.findElementByXPath(testAccountSyncWorksForGoogleAccounts.get("SyncNoOfDays")).clear();
			Thread.sleep(2000);
			
			driver.findElementByXPath(testAccountSyncWorksForGoogleAccounts.get("SyncNoOfDays")).sendKeys("2");
		}else if(driver.findElements(By.xpath(testAccountSyncWorksForGoogleAccounts.get("DatePicker"))).size()!=0){
			log.info("Date picker displayed for mail to sync");

			driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("DatePicker"))).click();
			driver.findElementByXPath(testAccountSyncWorksForGoogleAccounts.get("DatePicker")).clear();

			driver.findElementByXPath(testAccountSyncWorksForGoogleAccounts.get("DatePicker")).sendKeys("2");
		}
		Thread.sleep(1500);
		CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("SyncOk"));
		driver.navigate().back();
		driver.navigate().back();
		
	} catch (Throwable e) {
		e.printStackTrace();
		throw new Exception("Unable to verify the days of mail to sync option");
	}
	}	
	
	/**
	 * @TestCase  : verifytestAccountSyncWorksForGoogleAccounts
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
	public void verifytestAccountSyncWorksForGoogleAccountsSendMailfromWeb(AppiumDriver driver)  {
	try {
		Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage
			.getTestData("testAccountSyncWorksForGoogleAccounts");
		Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
		
		
		log.info("Launching Chrome");
	    Process p;
			String[] launchChromeBrowser1 = new String[]{"adb","shell", "monkey", "-p", "com.android.chrome", "-c", "android.intent.category.LAUNCHER 1"};
			    p = new ProcessBuilder(launchChromeBrowser1).start();
			    log.info("Launched Chrome Browser");
			    Thread.sleep(5000);
			    driver.findElement(By.xpath("//android.widget.ImageButton[@resource-id='com.android.chrome:id/tab_switcher_button']")).click();
			    Thread.sleep(5000);
			    driver.findElement(By.xpath("//android.widget.ImageButton[@resource-id='com.android.chrome:id/menu_button']")).click();
			    Thread.sleep(5000);
			    driver.findElement(By.xpath("//android.widget.TextView[@content-desc='New tab']")).click();
			    Thread.sleep(5000);
			    
			  if(driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("GmailTypeInSearchBox"))).isDisplayed())
			    {
			   driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("GmailTypeInSearchBox"))).sendKeys("http://gmail.com ");
			    Thread.sleep(5000);
			   ((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			   Thread.sleep(5000);
	           }
			   else
			   { driver.findElement(By.xpath("//android.widget.EditText[@text='Search or type URL']")).sendKeys("http://gmail.com");
				   ((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			  }
			   Thread.sleep(5000);
			   
			 if(CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail")))
					 { 
			   CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail"),testAccountSyncWorksForGoogleAccounts.get("etouchtesttwoemailid"));
			   log.info("Entered user name");
			   hideKeyboard();
			    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
			    Thread.sleep(5000);
			    //hideKeyboard();  
			    //driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enterpasssword"))).clear();
			    CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enterpasssword"),testAccountSyncWorksForGoogleAccounts.get("password"));
			    hideKeyboard();
			    log.info("Entered passwor");
			    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
			    //Thread.sleep(10000);
			  }
			 CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("ClickMenu"));
			 
			 log.info("clicked on menu");
			 Thread.sleep(10000);
			 driver.findElement(By.xpath("//android.widget.Button[contains(@content-desc,'gmail.com')]")).click();
			 
			 log.info("clicked on Email Account");
			 
			 //driver.findElement(By.xpath("//android.view.View[@index='0'] | //android.widget.Button[@index='0']")).click();
			Thread.sleep(10000);
			if(CommonFunctions.isElementByXpathDisplayed(driver,"//android.widget.Button[contains(@content-desc,'etouchtesttwo')]"))
			{
			 CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[contains(@content-desc,'etouchtesttwo')]");
			}
	       else{
	   	  CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[@content-desc='Add account']"); 
	   	  Thread.sleep(5000);
	   	  driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enteremail"))).clear();
	   	  CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enteremail"),testAccountSyncWorksForGoogleAccounts.get("etouchtesttwoemailid"));
			   log.info("Entered user name");
			   hideKeyboard();
			    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
			    Thread.sleep(5000);
			    //hideKeyboard();  
			    driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("enterpasssword"))).clear();
			    CommonFunctions.searchAndSendKeysByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("enterpasssword"),testAccountSyncWorksForGoogleAccounts.get("password"));
			    hideKeyboard();
			    log.info("Entered passwor");
			    CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("Next"));
	     }
				 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("clickgmail"));
			    Thread.sleep(5000);
			    CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("ClickComposeWeb"));
			    CommonFunctions.searchAndSendKeysByXpath(driver, changeLabels.get("ToFieldWeb"),changeLabels.get("ToFieldWebEmailid"));
			    Thread.sleep(5000);
			    CommonFunctions.searchAndSendKeysByXpath(driver, changeLabels.get("SubjectWeb"),changeLabels.get("subjectWebtestsync"));
			    hideKeyboard();
	            Thread.sleep(5000);
			    CommonFunctions.searchAndSendKeysByXpath(driver, changeLabels.get("ComposeBodyWeb"),changeLabels.get("ComposeBodyTestSync"));
			    hideKeyboard();
			    scrollUp(1);
			   CommonFunctions.searchAndClickByXpath(driver,changeLabels.get("SendWeb"));
			    
		} catch (Throwable e) {
		
	}
	}

	/**
	 * @TestCase  : verifytestAccountSyncWorksForGoogleAccounts
	 * @Pre-condition :
	 * @throws: Exception
	 */ 
	public void verifytestAccountSyncWorksForGoogleAccountsFromWebisRecievedInGmailApp(AppiumDriver driver)  {
	try {
		Map<String, String> testAccountSyncWorksForGoogleAccounts = commonPage
			.getTestData("testAccountSyncWorksForGoogleAccounts");
		Map<String, String>changeLabels = commonPage.getTestData("changeLabels");

	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		if(CommonFunctions.isElementByXpathDisplayed(driver, testAccountSyncWorksForGoogleAccounts
	.get("EtouchSyncAccount")))
		  {	
		  	
		  driver.navigate().back();
		  }
		  
	   else
		  { CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		 CommonFunctions.searchAndClickByXpath(driver, testAccountSyncWorksForGoogleAccounts
	.get("EtouchSyncAccount"));
	    }
		
		CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("VerifySyncMailnboxWeb"));
		log.info("Mail is Displayed after intitial sync after adding account from web");
		pullToReferesh(driver);
		
		
	} catch (Throwable e) {
		
	}
	}	 
	
	public void verifySoundVibrateImagesDaysofmaailtosyncDowmloadAttachments(AppiumDriver driver) throws Exception {
		try{
			log.info("*********Verify Sound download attachments images**********************");
			Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
		   Map<String, String>testAccountSyncWorksForGoogleAccounts = commonPage.getTestData("testAccountSyncWorksForGoogleAccounts");
		  // Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
		/*	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			CommonFunctions.scrollTo(driver, "Settings");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
			Thread.sleep(3000);
		*/	scrollUp(2);
			Thread.sleep(2000);
		   	/*String checkNotifications=driver.findElement(By.xpath(navigateToSettings.get("NotificationsLabelcheckbox"))).getAttribute("checked");
			 if(checkNotifications.equalsIgnoreCase("false"))
			{
				 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("NotificationsLabelcheckbox"));
				 log.info("Checkded Notications");
		   }*/
			//added by Venkat on 28/12/2019: starts
			try {
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.RelativeLayout//android.widget.TextView[@text='Notifications']");
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.CheckedTextView[@text='All']");
			} catch (Exception e) {
			}
			//added by Venkat on 28/12/2019: end
			
			if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("InboxSoundVibrate"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxSoundVibrate"));
				Thread.sleep(1200);
			}else
			
			if(commonFunction.getSizeOfElements(driver,commonElements.get("Notifications"))!=0){
				  log.info("******************* Notifications Option is displayed ****************");
				 String text = "Notifications";
				 AndroidElement notify = null;
				  for(int i=6;i<=10;i++){
					String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
					  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
					if	(settingOption.equalsIgnoreCase(text)){
						notify = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout[@index='1']"
								+ "/android.widget.CheckBox[@index='0']"));
						if (notify.getAttribute("checked").equalsIgnoreCase("false")) {
							log.info("Checkbox is not checked");
							notify.click();
							log.info("Checkbox is now checked");
						}else{
							log.info("Checkbox already checked");
							notify.click();
							log.info("Checkbox is unchecked");
							notify.click();
							log.info("Checkbox checked again to verify to no crash");
						}
						log.info("Verified Notification option functionality");
						break;
					}  
				  }
			}
		log.info("*****************Clicking on sound and vibrate option*****************");
	   //CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("InboxSoundVibrate")); // Commented by Venkat on 31/01/2018
		// added by Venkat on 31/01/2018 : start
		try {
			CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("InboxSoundVibrate")); 
		} catch (Exception e) {
			scrollUp(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Notifications"));
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.CheckedTextView[contains(@text,'All')]");
			CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("InboxSoundVibrate")); 
		}
		// added by Venkat on 31/01/2018 : end
	   log.info("clicked on Inbox SoundVibrate"); 
	   Thread.sleep(2500);
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("SyncMessages"));
	   Thread.sleep(1500);
	   CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("SyncPrimary"));
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("All"));
		try{   
		 if(!CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("Sound")))
		{  
		   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("LabelNotifications"));
		   log.info("clicked on Label Notifications");
		   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("ClickokNotifications"));
		   log.info("clicked on OK Label Notifications");
		   
		}
	 Thread.sleep(1500);
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("Sound"));
	   log.info("clicked on Sound");
	   Thread.sleep(2500);
	   Random rd = new Random();
	   List<WebElement> sounds = driver.findElements(By.xpath("//android.widget.CheckedTextView[@resource-id='android:id/text1']"));
	   int index = rd.nextInt(sounds.size());
	   sounds.get(index).click();
	   
		}catch(Exception e){
			
		}
	   
	   /*
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("ClickBeeponce"));
	   log.info("clicked on Beep Once");
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("ClickBeeponce"));
	   log.info("clicked on Beep Once");
	   */
	   CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("ClickokNotifications"));
	   log.info("clicked on Ok and Sound Beep Once is enabled");
	   Thread.sleep(5000);
	   String selectedSoundValue = driver.findElement(By.xpath(navigateToSettings.get("SoundValue"))).getText();
	   log.info(selectedSoundValue);
	   
	   /*String checkVibrate=driver.findElement(By.xpath(navigateToSettings.get("Vibratecheckbox"))).getAttribute("checked");
		 if(checkVibrate.equalsIgnoreCase("false"))
		{
			 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("Vibratecheckbox"));
			 log.info("Vibrate is checked");
	    }
		 */
	   	
	   	log.info("Clicking on vibrate option");
	   	CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Vibratecheckbox"));
	   	CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Vibratecheckbox"));
	   	log.info("Checked on vibrate option");
	   	
	   	log.info("Clicking on notify for every message");
	   	CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("NotifyEveryMessage"));
	   	CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("NotifyEveryMessage"));
	   	log.info("Checked on vibrate option");
	   	
		 driver.navigate().back();
		 scrollTo("Days of mail to sync");
		 CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("DaysOfMailToSync"));
		 log.info("verified Days of Mails to sync is displayed");
		 CommonFunctions.scrollTo(driver,"Download attachments");
		 //scrollTo("Download attachmnents");
		CommonFunctions.isElementByXpathDisplayed(driver,navigateToSettings.get("VeriDownloadAttachments"));
		 log.info("verified DownloadAttachments");
		 /*String DownloadAttachments=driver.findElement(By.xpath(testAccountSyncWorksForGoogleAccounts.get("CheckDownloadAttachments"))).getAttribute("checked");
		 if(DownloadAttachments.equalsIgnoreCase("false"))
		{
			 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("CheckDownloadAttachments"));
			 log.info("Checkded Download Attachments");
	   }
*/
		 	log.info("Clicking on notify for every message");
		   	CommonFunctions.searchAndClickByXpath(driver, testAccountSyncWorksForGoogleAccounts.get("VeriDownloadAttachments"));
		   	CommonFunctions.searchAndClickByXpath(driver, testAccountSyncWorksForGoogleAccounts.get("VeriDownloadAttachments"));
		   	log.info("Checked on vibrate option");

		 CommonFunctions.scrollsToTitle(driver, "Images");  	
		 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("ClickAlwayShowImages"));
		 log.info("clicked on Always Show Images");
		 //CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("ClickImages"));
		 //log.info("clicked on Images");
		 //commonPage.navigateUntilHamburgerIsPresent(driver);
		 driver.navigate().back();
		 driver.navigate().back();
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 Thread.sleep(1500);
		 /*scrollUp(3);
		 CommonFunctions.scrollTo(driver,"TestLabel");
		 Thread.sleep(2500);
	     CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("TestLabel"));
	     */
		 CommonFunctions.scrollsToTitle(driver, "TestLabel");
		 userAccount.openMailUsingSearch(driver, testAccountSyncWorksForGoogleAccounts.get("CLickImagesMail"));
	    // CommonFunctions.isElementByXpathNotDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("showpictures"));
	     log.info("Verfied Images displayed");	 
	     driver.navigate().back();
	     driver.navigate().back();//added by Venkat on 15/11/2018
	     Thread.sleep(2000); //added by Venkat on 15/11/2018
	     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 Thread.sleep(1000);
	     CommonFunctions.scrollTo(driver, "Settings");
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));

		 CommonFunctions.scrollsToTitle(driver, "Settings");
		 //CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));  //commented by Venkat on 15/11/2018
		 //added by Venkat on 15/11/2018: starts
		 String emailID = navigateToSettings.get("etouchtestone");
		 String xPath = commonElements.get("AccountToSwitch").replace("Email", emailID);
		 boolean isElementFound = userAccount.scrollToFindElement(driver, emailID,"down",xPath);
		 if(!isElementFound){
			 isElementFound = userAccount.scrollToFindElement(driver, emailID,"up", xPath);						
		 }
		 if(!isElementFound){
			 throw new Exception("Mail id :"+emailID+" not configured. Please check..");
		 }

		 CommonFunctions.searchAndClickByXpath(driver, emailID);
		 //added by Venkat on 15/11/2018: end
		 scrollTo("Images");
		 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("ClickImages"));
		 log.info("clicked on Images");
		 
		 CommonFunctions.scrollsToTitle(driver, "Images");
		 CommonFunctions.searchAndClickByXpath(driver,testAccountSyncWorksForGoogleAccounts.get("ClickAskBeforeShowingImages"));
		 log.info("clicked on Ask Before Showing Images");
		 commonPage.navigateUntilHamburgerIsPresent(driver);
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		 CommonFunctions.scrollsToTitle(driver, "TestLabel");
		 Thread.sleep(5000);
	     
		 userAccount.openMailUsingSearch(driver, testAccountSyncWorksForGoogleAccounts.get("CLickImagesMail"));
	     Thread.sleep(5000);
	     CommonFunctions.isElementByXpathDisplayed(driver,testAccountSyncWorksForGoogleAccounts.get("showpictures"));
	     log.info("Verfied Images are not displayed");
		
	     CommonFunctions.searchAndClickByXpath(driver, testAccountSyncWorksForGoogleAccounts.get("showpictures"));

		 driver.navigate().back();

	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}  
	
	public void replyMessage(AppiumDriver driver, String ReplyText)throws Exception{
		try{
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SubjectLine"));
			log.info("In IF.replyMessage Getting text from subject ReplyText:"+ReplyText);
			String subject = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
			scrollDown(2);
			CommonFunctions.scrollsToTitle(driver, "Reply");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("ComposeBody"));
			//CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), ReplyText);
			CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("ComposeBody"), ReplyText);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("In IF Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("In IF.replyMessage Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("In IF Show drive clicked");
			Thread.sleep(600);
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive")); // commented by Venkat on 10/01/2019
			// added by Venkat on 10/01/2019 :start
				try{
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
				}catch(Exception e){
					CommonFunctions.scrollUp(driver, 1); // added by Venkat on 08/01/2019
					Thread.sleep(600);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
				}
			// added by Venkat on 10/01/2019 :end

			log.info("In IF etouchtestone drive cliced");
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("In IF Attachment1 clicked");		
			
			driver.navigate().back();
			Thread.sleep(6000);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("DraftMail"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("ReplyComposeText").replace("Text", ReplyText));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("AttachmentThumbNail"));
			log.info("In IF.replyMessage Verified replied text and attachment");
			driver.navigate().back();
			
		}catch(Exception e){
			throw new Exception("In IF.replyMessage replying message with attachment is failed");
		}
	}
	
	public void verifyAutoSyncTips(AppiumDriver driver)throws Exception{
		 Map<String, String> verifyAutoSync = commonPage
					.getTestData("AutoSync");
		try{
			Thread.sleep(600);
			inbox.openMenuDrawer(driver);
			Thread.sleep(1000);
			log.info("verifyAutoSyncTips Checking for primary folder. PrimaryTitle:"+commonElements.get("PrimaryTitle"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PrimaryTitle"))!=0){
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}else{
				navDrawer.navigateToPrimary(driver);
				Thread.sleep(1000);
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}
			log.info("Checking for Social folder");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SocialInboxOption"))!=0){
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}else{
				navDrawer.navigateToSocial(driver);
				Thread.sleep(1000);
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}
			log.info("Checking for Sent folder");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("SentOption"))!=0){
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}else{
				navDrawer.navigateToSentBox(driver);
				Thread.sleep(1000);
				commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncMsg"));
			}
			
			
		}catch(Exception e){
			throw new Exception("");
		}
	}
	
	public void verifyInboxTypes(AppiumDriver driver)throws Exception{
		try{
			commonPage.waitForElementToBePresentByXpath(commonElements.get("InboxType"));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("InboxTypes"))==5){
				log.info("Convergence account is opened");
				String inboxTypes = null;
				for(int i=0;i<=4;i++){
					inboxTypes = CommonFunctions.searchAndGetTextOnElementByXpath(driver, "//android.widget.CheckedTextView[@index='"+i+"']");
					log.info(inboxTypes);
					
				}
			}else{
				log.info("It is not a convergence account/please check if any issue exist");
				throw new Exception();
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	public void setNotificationLevel(AppiumDriver driver, String setnotification)throws Exception{
		try{
			CommonFunctions.scrollsToTitle(driver, "Notify for inbox sections");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, setnotification);
		}catch(Exception e){
			throw e;
		}
	}
	
	public void setNotificationMain(AppiumDriver driver, String setnotification)throws Exception{
		try{
			Thread.sleep(2000);
			//CommonFunctions.scrollsToTitle(driver, "Notifications");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NotificationsSettings"));
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, setnotification);
			Thread.sleep(1200);
			
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("TurnOffNotificationAlert"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("TurnOffNotificationAlert"));
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	public void verifyNotificationSection(AppiumDriver  driver, String NotificationSection, String Notification)throws Exception{
		try{
			CommonFunctions.scrollToCellByTitle(driver, NotificationSection);
			String text = NotificationSection;
			 AndroidElement cv = null;
			 Thread.sleep(1500);
			 /*String notificationIndex = driver.findElement(By.xpath("//android.widget.TextView[@text='Notifications']")).getText();
			 
			 int NotifyIndex = Integer.parseInt(notificationIndex);
			 for(int i=1;i<10;i+=2){
				String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
				  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
				if	(settingOption.equalsIgnoreCase(NotificationSection)){
				String NotificationValue = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
					  		+ "/android.widget.TextView[@resource-id='android:id/title']/android.widget.TextView[@resource-id='android:id/summary']")).getText();
				log.info(NotificationValue);
				}  
			  }
			*/
			 String notifyOption = driver.findElement(By.xpath("//android.widget.TextView[@text='Notify for inbox sections']/following-sibling::android.widget.TextView[@resource-id='android:id/summary']")).getAttribute("text");
			 log.info(notifyOption);
			 if(notifyOption.equalsIgnoreCase(Notification)){
				 log.info("Notification level is set good");
			 }else{
				 throw new Exception("Notification level has changed back to default");
			 }
		}catch(Exception e){
			throw e;
		}
	}
	
	public void verifyNotificationMain(AppiumDriver  driver, String NotificationSection, String Notification)throws Exception{
		try{
			CommonFunctions.scrollToCellByTitle(driver, NotificationSection);
			String text = NotificationSection;
			 AndroidElement cv = null;
			 Thread.sleep(1500);
			 /*String notificationIndex = driver.findElement(By.xpath("//android.widget.TextView[@text='Notifications']")).getText();
			 
			 int NotifyIndex = Integer.parseInt(notificationIndex);
			 for(int i=1;i<10;i+=2){
				String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
				  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
				if	(settingOption.equalsIgnoreCase(NotificationSection)){
				String NotificationValue = driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
					  		+ "/android.widget.TextView[@resource-id='android:id/title']/android.widget.TextView[@resource-id='android:id/summary']")).getText();
				log.info(NotificationValue);
				}  
			  }
			*/
			 String notifyOption = driver.findElement(By.xpath("//android.widget.TextView[@text='Notifications']/following-sibling::android.widget.TextView[@resource-id='android:id/summary']")).getAttribute("text");
			 log.info(notifyOption);
			 if(notifyOption.equalsIgnoreCase(Notification)){
				 log.info("Notification level is set good");
			 }else{
				 throw new Exception("Notification level has changed back to default");
			 }
		}catch(Exception e){
			throw e;
		}
	}
	
	
	public void verifyMailNotificationNone(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ImpMailSubject"))==0){
				log.info("Important mail notification is not displayed");
			}else{
				throw new Exception("Mail displayed");
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("UnImpMailSubject"))==0){
				log.info("Un important mail notification is not displayed");
			}else{
				throw new Exception("Mail displayed");
			}
		}catch(Exception e){
			throw new Exception();
		}
	}
	
	public void verifyMailNotificationAll(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(2000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ImpMailSubject"))!=0){
				log.info("Important mail notification is displayed");
			}else{
				throw new Exception("Mail is not displayed");
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("UnImpMailSubject"))!=0){
				log.info("Un important mail notification is displayed");
			}else{
				throw new Exception("Mail is not displayed");
			}
		}catch(Exception e){
			throw new Exception();
		}
	}
	
	public void verifyMailNotification(AppiumDriver driver)throws Exception{
		try{
			Thread.sleep(4000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ImpMailSubject"))!=0){
				log.info("In IF.verifyMailNotification Important mail notification is displayed");
			}else{
				throw new Exception("Mail is not received yet or imp mail is not received");
			}
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("UnImpMailSubject"))!=0){
				log.info("In IF.verifyMailNotification  Un important mail notification is displayed");
				throw new Exception("Un imp mail is displayed even the notification is set to important");
			}
		}catch(Exception e){
			throw new Exception();
		}
	}
	
	public void ClearNotification(AppiumDriver driver){
		try {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClearNotificationButton"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void selectInboxType(AppiumDriver driver, String InboxTypes)throws Exception{
		try{
			CommonFunctions.scrollsToTitle(driver, "Inbox type");
			Thread.sleep(800);
			CommonFunctions.scrollsToTitle(driver, InboxTypes);
			
		}catch(Exception e){
			throw e;
		}
	}
	
	public void mailSend(String batchFileName) throws Exception{
		 String path = System.getProperty("user.dir");
		 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+batchFileName;
		 log.info("Executing the bat file to generate a mail and move to other folder "+command);
		 Process p = new ProcessBuilder(command).start();
		 log.info("Executed the bat file to generate a mail");
		 Thread.sleep(4000);
	}
	
	public String findMailInPrimaryFolder(AppiumDriver driver, String emailSubject) throws Exception{
		String isMailFound = null;
		try {
			int count = 0;
			String emailSubjectXpath = commonElements.get("EmailSubjectText").replace("Subject", emailSubject);
			//emailSubjectXpath = "//android.view.View[starts-with(@content-desc,'"+emailSubject+"')] |"+emailSubjectXpath;
			emailSubjectXpath = "//android.view.View[contains(@content-desc,'"+emailSubject+"')] |"+emailSubjectXpath;
			System.out.println("emailSubjectXpath :"+emailSubjectXpath);
			outerLoop:
			while(count++ <= 2){
				Thread.sleep(2000);
				if(isMailFound == null){
					int innerCnt = 0;
					while(innerCnt++ <=1){
						if(CommonFunctions.getSizeOfElements(driver, emailSubjectXpath)<=0){
							Thread.sleep(1000);
							scrollDown(1);
							continue;
						}else{
							isMailFound = emailSubjectXpath;
							break outerLoop;
						}
					}
					if(innerCnt > 1) {
						Thread.sleep(1000);
						scrollUp(3);
					}
				}else{
					log.info("Mail exists in primary folder");
					isMailFound = emailSubjectXpath;
					break;
				}
			}
		} catch (Exception e) {
			isMailFound = null;
			throw new Exception("Exception while looking for mail with the subject "+emailSubject+"  :"+e.getMessage());
		}
		return isMailFound;
	}
	
	
	// ****************** Bindu code changes *******************//
	
	/**
	 * Method to tap on email received from notification
	 * @author batchi
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifyEmailFromNotification(AppiumDriver driver) throws Exception {
	    try {
	        Map < String, String > verifyReceiveMailData = commonPage
	            .getTestData("IMAPAccountDetails");

//			navDrawer.openMenuDrawer(driver);
//			CommonFunctions.scrollsToTitle(driver, "Settings");
//			Thread.sleep(2000);
//			CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("OutlookOptionInSettings"));
//			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SyncEmail"));
//			Thread.sleep(4000);
//			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SyncEmail"));
	        
	        log.info("Launching Settings app to sync IMAP account for notification to appear");
	        Process p;
			String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.settings/com.android.settings.Settings$AccountDashboardActivity"};
			    p = new ProcessBuilder(p1).start();
			    log.info("Trying to launch the app");
	        Thread.sleep(6000);
	        scrollDown(1);
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountSettings"));
	        Thread.sleep(2000);
	        CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("OutlookOptionInSettings"));
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsAccountSync"));
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsSyncToggle"));
	        Thread.sleep(4000);
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsSyncToggle"));
	        Thread.sleep(10000);
			((AndroidDriver)driver).openNotifications();
			
//	        driver.navigate().back();
//	        ((AndroidDriver) driver).openNotifications();
	        Thread.sleep(2000);
	        String title = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("NotificationTitleElement"));
	        
	        if(title.contains(verifyReceiveMailData.get("ReceiveSubjectData"))== true){
	        	log.info("Received notification for IMAP account");
	        	CommonFunctions.searchAndClickByXpath(driver,commonElements.get("NotificationTitleElement") );
	        	
	        }else{
	        	throw new Exception("Notification didn't appear");
	        }
	    
	        
	    }catch (Throwable e) {
	        e.printStackTrace();
	        commonPage.catchBlock(e, driver, "Unable to find notifications");
	    }

	}
	/**
	 * Method to tap on email received
	 * @author batchi
	 * @param MailSubject
	 * @throws Exception
	 */
	public void verifyEmailReceivedCV(AppiumDriver driver, Map <String, String> verifyReceiveMailData) throws Exception {
	    try {
	        //Map <String, String> verifyReceiveMailData = commonPage.getTestData("IMAPAccountDetails");
	    	boolean isMailFound = false;
	        	try {
	        		CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("ReceiveSubjectMailTL"));
	        		isMailFound = true;
				} catch (Exception e) {
					log.info("Mail not found Inbox. So going to Unread folder....");
				}
	        	if(!isMailFound) {
	        		userAccount.openMenuDrawerNew(driver);
	        		CommonFunctions.scrollsToTitle(driver, "Unread");
	        		Thread.sleep(2000);
	        		CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("ReceiveSubjectMailTL"));
	        	}
	        log.info("Verify Mark Unread from CV view");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkUnread"));
//	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
	        	if(CommonFunctions.getSizeOfElements(driver, commonElements.get("HamburgerMenu"))!=0){
	        	log.info("Mail marked as read");
	        }
	        	Thread.sleep(3000);
	        CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("ReceiveSubjectMailTL"));
	        Thread.sleep(3000);
	        log.info("Verify Delete from CV view");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DeleteActionCV"));
	        if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
				driver.navigate().back();
			}
			navDrawer.openMenuDrawer(driver);
	        CommonFunctions.scrollsToTitle(driver, "Trash");
	        //Commented by Venkat on 15/02/2019
/*	        
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
*/	        
	   

	        // added by Venkat on on 15/02/2019 : starts
	        CommonFunctions.searchAndClickByXpath(driver, verifyReceiveMailData.get("ReceiveSubjectMailTL"));
	        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifyReceiveMailData.get("ReceiveSubjectMailTL"));
	        // added by Venkat on on 15/02/2019 : end
	        	if(mailInTrash.contains(verifyReceiveMailData.get("ReceiveSubjectData"))){
	        	log.info(" *************** Mail is deleted and is in Trash ************");
	        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	        	Thread.sleep(1000);
	        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
	        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
	        	Thread.sleep(2000);
	        	navigateBack(driver);
	        	Thread.sleep(2000);
	        	if(mailInTrash.contains(verifyReceiveMailData.get("ReceiveSubjectData"))){
	        		log.info(" ************* Mail is moved to Primary *************");
	        	}else{
	        		throw new Exception(" !!!!!!!!!! Mail is not moved to PRimary !!!!!!!!!!");
	        	}
	        }else{
	        	throw new Exception("  !!!!!!!!!!! Deleted mail is not in Tash folder !!!!!!!!!!");
	        	
	        }
	        
	    }catch (Throwable e) {
	        e.printStackTrace();
	        commonPage.catchBlock(e, driver, "Unable to find notifications");
	    }

	}
	
	/**
	 * Method to verify Starred email sync
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyStarredIMAPSync(AppiumDriver driver) throws Exception {
	    try {
	    	Map < String, String > verifyIMAPLabelsData = commonPage.getTestData("IMAPAccountDetails");
	    	//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	CommonFunctions.searchAndClickByXpath(driver,"//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/thread_list_view']//android.view.ViewGroup[@index='1']");
	    	String SubLineText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject which is starred:  "+SubLineText);
//	    	CommonFunctions.searchAndClickByXpath(driver, verifyIMAPLabelsData.get("MoreOptionsCV"));
//	    	String StarRemoveStar = CommonFunctions.searchAndGetTextOnElementByXpath(driver,verifyIMAPLabelsData.get("MoreOptionRemoveStar"));
//	    	String StarRemoveStar = CommonFunctions.searchAndGetTextOnElementById(driver, "com.google.android.gm:id/title");
	    	
//	    	if(verifyIMAPLabelsData.get("RemoveStarText").equalsIgnoreCase(StarRemoveStar)){
//	    		
//	    		commonFunction.searchAndClickByXpath(driver,commonElements.get("RemoveStarMoreOpt"));
//	    		
//	    	}
//	    	driver.navigate().back();
	    	commonFunction.searchAndClickByXpath(driver,commonElements.get("StarIcon1"));
	    	//driver.navigate().back();
	    	commonFunction.searchAndClickByXpath(driver,"//android.widget.ImageButton[@content-desc='Navigate up']");
	    	log.info("Verifiying in Starred label");
	    	navDrawer.openMenuDrawer(driver);
	    	navDrawer.navigateToLabel(driver,verifyIMAPLabelsData.get("StarredLabel") );
	    	Thread.sleep(2000);
	    	//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	CommonFunctions.searchAndClickByXpath(driver,"//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/thread_list_view']//android.view.ViewGroup[@index='1']");
	    	String SubLineTextStarred = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject in starred label:  "+SubLineTextStarred);
	    	if(SubLineText.equalsIgnoreCase(SubLineTextStarred)){
	    		log.info(" ************** Starred email is displayed in Starred label ************");
	    	}
	    	else {
				throw new Exception("Starred email is not displayed in Starred label");
			}
	    	commonFunction.searchAndClickByXpath(driver,commonElements.get("StarIcon1"));
	    	Thread.sleep(1000);
	    	driver.navigate().back();
	    	driver.navigate().back();
	    	//commonFunction.searchAndClickByXpath(driver,"//android.widget.ImageButton[@content-desc='Navigate up']");
	    	//launchGmailApplication(driver);
	    	
	    }catch (Throwable e) {
	        e.printStackTrace();
	        commonPage.catchBlock(e, driver, "Unable to find Starred email");
	    }

	}

	/**
	 * Method to verify Starred email sync
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUnreadIMAPSync(AppiumDriver driver) throws Exception {
	    try {
	    	Map < String, String > verifyIMAPLabelsData = commonPage.getTestData("IMAPAccountDetails");
	    	//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	//CommonFunctions.searchAndClickByXpath(driver,"//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/thread_list_view]//android.view.ViewGroup[@resource-id='com.google.android.gm:id/viewified_conversation_item_view[@index='1']");
	    	CommonFunctions.searchAndClickByXpath(driver,"//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/thread_list_view']//android.view.ViewGroup[@index='1']");
	    	String SubLineText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject :  "+SubLineText);
	    	CommonFunctions.searchAndClickByXpath(driver, verifyIMAPLabelsData.get("UnReadIcon"));
	    	navDrawer.openMenuDrawer(driver);
	    	navDrawer.navigateToLabel(driver, verifyIMAPLabelsData.get("UnreadLabel"));
	    	//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	CommonFunctions.searchAndClickByXpath(driver,"//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/thread_list_view']//android.view.ViewGroup[@index='1']");
	    	String SubUnreadLabel = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject which is starred:  "+SubUnreadLabel);
	    	if(SubLineText.contains(SubUnreadLabel)){
	    		log.info(" ************** Unread mail is present under Unread Label ***************");
	    	}
	    	else{
	    		throw new Exception("Unread mail is not displayed in the Unread Label");
	    	}
	    	
	    	driver.navigate().back();
	    	launchGmailApplication(driver);
	    	
	    }catch (Throwable e) {
	        e.printStackTrace();
	        commonPage.catchBlock(e, driver, "Unable to find Unread mail");
	    }

	}
	/**
	 * Method to verify System label Archive email sync
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyArchiveSysLabelIMAPSync(AppiumDriver driver) throws Exception {
	    try {
	    	Map < String, String > verifyIMAPLabelsData = commonPage.getTestData("IMAPAccountDetails");
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	String SubLineText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject :  "+SubLineText);
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
//	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	    	commonFunction.searchAndClickByXpath(driver,commonElements.get("MoveToPopUp"));
	    	CommonFunctions.searchAndClickByXpath(driver, verifyIMAPLabelsData.get("ArchiveSysLabel"));
	    	navDrawer.openMenuDrawer(driver);
	    	if(CommonFunctions.isElementByXpathNotDisplayed(driver, verifyIMAPLabelsData.get("ArchiveSysLabel"))){
	    		scrollDown(1);
	    	}
	    	CommonFunctions.searchAndClickByXpath(driver, verifyIMAPLabelsData.get("ArchiveSysLabel"));
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    	String SubLineTextInSysLabel = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
	    	log.info("Email's subject :  "+SubLineTextInSysLabel);
	    	if(SubLineTextInSysLabel.contains(SubLineText)){
	    		log.info(" **************** Email is moved to User label successfully ***************");
	    		}
	    	else{
	    		throw new  Exception("Email moved to User label is not displayed");
	    	}
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
        	driver.navigate().back();
	    	

	    }catch (Throwable e) {
	        e.printStackTrace();
	        commonPage.catchBlock(e, driver, "Unable to find mail in User label");
	    }

	}
	/**
	* @TestCase sanity IMAP : IMAP Accounts - Verify receive a new mail & notification
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifymailInOutbox(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
	
		CommonFunctions.searchAndClickByXpath(driver, verifySendMailIMAPData.get("UnsentOutboxAlert"));
//		navDrawer.openMenuDrawer(driver);
//		Thread.sleep(1000);
//		navDrawer.navigateToLabel(driver,verifySendMailIMAPData.get("OutboxLabel") );
//		CommonFunctions.searchAndClickByXpath(driver, verifySendMailIMAPData.get("OutboxLabel"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
		String mailInOutbox = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
    	if(mailInOutbox.contains(verifySendMailIMAPData.get("SubjectData"))){
    	log.info(" ********************  Mail is in Outbox ********************");
    	}else{
    		throw new Exception("!!!!!!!!! Mail is not in Outbox !!!!!!!!!!!!");
    	}
    	driver.navigate().back();
	
	}
	/**
	* @TestCase sanity IMAP : IMAP Accounts - Verify move to & delete from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyIMAPMoveToDelTLview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
		
		log.info("Verifying delete option from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
        CommonFunctions.scrollsToTitle(driver, "Trash");
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifySendMailIMAPData.get("MailActionsTLSubject"))){
        	log.info(" *************** Mail is deleted and is in Trash ************");
        	}else{
        		//throw  new Exception("!!!!!! Mail is not displayed in Trash !!!!!!!!!! "); //Commented by Venkat on  15/02/2019 //Conflict in mail selection and deletion.
        	}
        	driver.navigate().back();
        	log.info("Verifying moveTo inbox from TL view");
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
        	driver.navigate().back();
        	Thread.sleep(5000);
    	}
	/**
	* @TestCase sanity IMAP : IMAP Accounts - Verify Mark Read/Unread & Add star from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyIMAPMarkReadAddStarTLview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
		log.info("Add star and removing star from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddStarMoreOpt"));
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		Thread.sleep(2000);
//		String  RemoveStarText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, verifySendMailIMAPData.get("RemoveStartextMoreOpt"));
//		if(verifySendMailIMAPData.get("RemoveStarText").equals(RemoveStarText)){
//			log.info("****************** Added star *************");
//		}else{
//			throw new Exception("!!!!!!!!! Add star didn't work !!!!!!!!!!!");
//		}
		if(CommonFunctions.isElementByXpathDisplayed(driver, verifySendMailIMAPData.get("MoreOptionRemoveStar"))){
			log.info("****************** Added star *************");
			}else{
				throw new Exception("!!!!!!!!! Add star didn't work !!!!!!!!!!!");
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
		log.info("Mark Read and Unread mail");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkUnread"));
		
		if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MarkRead"))){
			log.info("***********  Mail is Marked as Unread ***************");
		}else{
			throw  new Exception("!!!!!!!!! Mail is not marked as Unread !!!!!!!!!!!!!!");
		}
		
		
		
}
	/**
	 * Method to verify Archive undo operations TL & CV 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoArchive(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying Archive undo operations in TL view  ************");
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
		 Thread.sleep(2000);
		 if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is archived and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not archived and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(5000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying Archive undo operations in TL view  ************");

	}
	/**
	 * Method to verify Delete undo operations TL & CV 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoDelete(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying Delete undo operations in TL view  ************");
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		 Thread.sleep(2000);
		 if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is deleted and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not deleted and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying Delete undo operations in TL view  ************");

	}
	/**
	 * Method to verify Mute undo operations TL & CV
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoMute(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying Mute undo operations in TL view  ************");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		Thread.sleep(1000);
		if(CommonFunctions.getSizeOfElements(driver,undoData.get("ClickMute"))== 0){
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}
		commonFunction.searchAndClickByXpath(driver, undoData.get("ClickMute"));
		 Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is Muted and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not Muted and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying Mute undo operations in TL view  ************");

	}
	/**
	 * Method to verify Report Spam undo operations TL & CV
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoReportSpam(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying ReportSpam undo operations in TL view  ************");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		Thread.sleep(1000);
		if(CommonFunctions.getSizeOfElements(driver,commonElements.get("ReportSpamInMoreOption"))== 0){
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}
	   	commonFunction.searchAndClickByXpath(driver, commonElements.get("ReportSpamInMoreOption"));
	    Thread.sleep(2000);
		 if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is spammed and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not spammed and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying ReportSpam undo operations in TL view  ************");

	}
	/**
	 * Method to verify MoveTo undo operations TL & CV
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoMoveTo(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying MoveTo undo operations in TL view  ************");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		Thread.sleep(1000);
		if(CommonFunctions.getSizeOfElements(driver,commonElements.get("MoveToOptionInOverflowMenu"))== 0){
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}
	   	commonFunction.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
	   	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SocialInboxOption"));
	    Thread.sleep(2000);
		 if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is moved and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not moved and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying MoveTo undo operations in TL view  ************");

	}
	/**
	 * Method to verify Snooze undo operations TL
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyUndoSnooze(AppiumDriver driver) throws Exception {
		Map < String, String > undoData = commonPage.getTestData("VerifyUndo");
		log.info("*********** Started Verifying Snooze undo operations in TL view  ************");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		Thread.sleep(1000);
		if(CommonFunctions.getSizeOfElements(driver,commonElements.get("Snooze"))== 0){
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		}
		commonFunction.searchAndClickByXpath(driver, commonElements.get("Snooze"));
	   	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeTomorrow"));
	    Thread.sleep(2000);
		 if(CommonFunctions.isElementByXpathNotDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Mail is snoozed and is not present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Mail is not snoozed and is present in the TL view !!!!!!!!!!!");
		}
		 waitForElementToBePresentByXpath(undoData.get("undoIcon"));
		CommonFunctions.searchAndClickByXpath(driver, undoData.get("undoIcon"));
		Thread.sleep(2000);
		if(CommonFunctions.isElementByXpathDisplayed(driver, undoData.get("subjectMail"))){
			 log.info("########## Undo is performed and mail is present in the TL view ###############");
		 }else {
			throw new Exception(" !!!!!!!!!!! Undo is not performed and so mail is not present in the TL view !!!!!!!!!!!");
		}
		
		log.info("*********** Ended Verifying Snooze undo operations in TL view  ************");

	}
	/**Method to compose and send email with attachments
	 * 
	 * @param driver
	 * @author batchi
	 * @throws Exception 
	 */
	public void composeAndSendWithAttachments(AppiumDriver driver, String MailSubject) throws Exception{
	    try{
	 	   Map<String, String> offlineMailSendingData= commonPage.getTestData("OfflineMailSending");

	 	   log.info("******************* Start composeAndSendWithAttachments Functionality*******************");
	   	 	Thread.sleep(5000);
	 	 	String fromUser=composeAsNewMail(driver,
	 	 			offlineMailSendingData.get("account2"),"","",
					MailSubject,
					offlineMailSendingData.get("mailwithattchmentSubject"));			
	 	 	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			Thread.sleep(600);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			log.info("etouchtestone drive cliced");
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("Attachment1 clicked");		
		
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
	        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			Thread.sleep(2000);
			if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImageNew"))){
				scrollDown(1);
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("AppiumImageNew"))){
					scrollDown(1);
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImageNew"));
			}else{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImageNew"));
			log.info("Attachment1 clicked");	
			}
			Thread.sleep(5000);
			scrollDown(1);
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendMailButton"));
			Thread.sleep(800);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			log.info("******************* Completed testcase composeAndSendWithAttachments Functionality*******************");
		 	
		}catch(Exception e){
			throw new Exception("Unable to attach file "+e.getLocalizedMessage());
		}
	}

	/**Method to Verify offline toast & unsent tip is dispalyed
	 * 
	 * @param driver
	 * @author batchi
	 * @throws Exception 
	 */
	public void verifyOfflineToastAndOutboxTip(AppiumDriver driver) throws Exception{
	    try{
	 	   Map<String, String> offlineMailSendingData= commonPage.getTestData("OfflineMailSending");

	 	   log.info("******************* Start verifyOfflineToastAndOutboxTip Functionality*******************");
	   	 	waitForElementToBePresentByXpath(offlineMailSendingData.get("OfflineMessage"));
	   	 	if(CommonFunctions.getSizeOfElements(driver, offlineMailSendingData.get("OfflineMessage"))!=0){
	   	 		log.info(" ############### Offline toast is displayed ###############");
	   	 	}else{
	   	 		
	   	 		throw new Exception("!!!!!!!!! Offline toast is not displayed !!!!!!!!!!!!");
	   	 	}
	   	 	pullToReferesh(driver);
//	   	 	waitForElementToBePresentByXpath(offlineMailSendingData.get("OutboxTip"));
	   	 	String tipText = CommonFunctions.searchAndGetTextOnElementById(driver, offlineMailSendingData.get("OutboxTipID"));
	   	 	if(tipText.contains("2") && tipText.contains(" unsent in Outbox")){
	   	 		log.info("########### Outbox tip or teaser is displayed with number 2 #############");
	   	 	}else {
	   	 	throw new Exception("!!!!!!!!! Outbox tip or teaser is not displayed with number 2  !!!!!!!!!!!!");
	   	 	}
	   	 	
	   	 log.info("******************* Ended verifyOfflineToastAndOutboxTip Functionality*******************");
	    }catch(Exception e){
			throw new Exception("Unable to attach file "+e.getLocalizedMessage());
		}
	}
	/**Method to verify Mail Sent After Online
	 * 
	 * @param driver
	 * @author batchi
	 * @throws Exception 
	 */
	public void verifyMailSentAfterOnline(AppiumDriver driver) throws Exception{
	    try{
	 	   Map<String, String> offlineMailSendingData= commonPage.getTestData("OfflineMailSending");

	 	   log.info("******************* Start verifyOfflineToastAndOutboxTip Functionality*******************");
	 	  pullToReferesh(driver);
	 	 Thread.sleep(2000);
		  pullToReferesh(driver);
	 	   navDrawer.navigateToSentBox(driver);
	 	 Thread.sleep(2000);
	 	  pullToReferesh(driver);
	 	  verifyMailIsPresent(driver, offlineMailSendingData.get("mailwithoutattchmentSubject"));
	 	  Thread.sleep(2000);
	 	 verifyMailIsPresent(driver, offlineMailSendingData.get("mailwithattchmentSubject"));
	 	 openMail(driver, offlineMailSendingData.get("mailwithattchmentSubject"));
	 	 //verifying if mail sent has attachments or not
	 	 scrollDown(1);
	 	 attachment.verifyAttachmentIsAddedSuccessfully(driver, offlineMailSendingData.get("Attachment1") );
	 	 attachment.verifyAttachmentIsAddedSuccessfully(driver,offlineMailSendingData.get("Attachment2"));
	 	 navigateBack(driver);
	 	navigateBack(driver);
	 	   
	 	  log.info("******************* Ended verifyOfflineToastAndOutboxTip Functionality*******************");
	    }catch(Exception e){
			throw new Exception("Unable to attach file "+e.getLocalizedMessage());
		}
	}

	/**Method to verify Mail Sent After Online
	 * 
	 * @param driver
	 * @author batchi
	 * @throws Exception 
	 */
	public void addingMultipleFiles(AppiumDriver driver) throws Exception{
	    try{
	    	Map<String, String> attachMultipleFilesData = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	 	  CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
	 	// Adding attachmnet 5

	 		attachment.addAttachmentFromDrive(driver, attachMultipleFilesData.get("PasswordName"));
	 	// Adding attachmnet 6
	 		attachment.addAttachmentFromDrive(driver, attachMultipleFilesData.get("SampleTextName"));
	 	// Adding attachmnet 7
	 		attachment.addAttachmentFromDrive(driver, attachMultipleFilesData.get("ScreenshotName"));
	 	// Adding attachmnet 8
	 		attachment.addAttachmentFromDrive(driver, attachMultipleFilesData.get("SampleXLSFileName")); 	  
	 	  // Adding attachment 1 	  
	 	attachment.addAttachementFromRoot(driver, attachMultipleFilesData.get("ImageJPGName"));
	 // Adding attachment 2 
	 	attachment.addAttachementFromRoot(driver, attachMultipleFilesData.get("ImagePNGName"));
	// Adding attachment 3 		
		attachment.addAttachementFromRoot(driver, attachMultipleFilesData.get("SampleCSVFileName"));
	// Adding attachment 4				
		attachment.addAttachementFromRoot(driver, attachMultipleFilesData.get("SampleAudioName"));

	    }catch(Exception e){
			throw new Exception("Unable to attach file "+e.getLocalizedMessage());
		}
	}

	/**Method to verify Mail Sent After Online
	 * 
	 * @param driver
	 * @author batchi
	 * @throws Exception 
	 */
	public void deletingMultipleFiles(AppiumDriver driver) throws Exception{
	    try{
	    	Map<String, String> attachMultipleFilesData = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
	//Attachment 2 deleting
	    	
//	    	scrollUp(1);
	    	
//	    	scrollTo(" SampleText");
//	    	inbox.waitForElementToBePresentByXpath(attachMultipleFilesData.get("SampleTextComposeClose"));
	    	CommonFunctions.searchAndClickByXpath(driver, attachMultipleFilesData.get("SampleTextComposeClose"));
	    	inbox.hideKeyboard();
	    	Thread.sleep(1500);
	    	attachment.verifyAttachmentIsDeletedSuccessfully(driver, attachMultipleFilesData.get("SampleTextName"));
	 //Attachment 3 deleting 
	    	
	    	/*String[] backspace = new String[]{"adb", "shell", "input", "keyevent 8"};
			ProcessBuilder pb;
			Process p1;
			pb = new ProcessBuilder(backspace);
			p1 = pb.start(); */
			Thread.sleep(2000);
//	    	inbox.waitForElementToBePresentByXpath(attachMultipleFilesData.get("ScreenshotComposeClose"));
			inbox.scrollUp(1);
//			inbox.hideKeyboard();
			Thread.sleep(2000);
	    	CommonFunctions.searchAndClickByXpath(driver, attachMultipleFilesData.get("ScreenshotComposeClose"));
	  	
	    	Thread.sleep(1500);
	    	inbox.hideKeyboard();
	    	attachment.verifyAttachmentIsDeletedSuccessfully(driver, attachMultipleFilesData.get("ScreenshotName"));
	    	if(CommonFunctions.getSizeOfElements(driver,attachMultipleFilesData.get("SampleAudioCompose"))==0){
	    		scrollDown(1);
	    	}
	 //Attachment 5 deleting    	
	    	hideKeyboard();
	    	inbox.waitForElementToBePresentByXpath(attachMultipleFilesData.get("ImageJPGRemove"));
	    	CommonFunctions.searchAndClickByXpath(driver, attachMultipleFilesData.get("ImageJPGRemove"));
	    	Thread.sleep(2000);
	    	attachment.verifyAttachmentIsDeletedSuccessfully(driver, attachMultipleFilesData.get("ImageJPGName"));
	 //Attachment 7 deleting  
//	    	hideKeyboard();
	    	inbox.waitForElementToBePresentByXpath(attachMultipleFilesData.get("SampleCSVFileRemove"));
	    	CommonFunctions.searchAndClickByXpath(driver, attachMultipleFilesData.get("SampleCSVFileRemove"));
	    	Thread.sleep(2000);
	    	attachment.verifyAttachmentIsDeletedSuccessfully(driver, attachMultipleFilesData.get("SampleCSVFileName"));
	    	
	    }catch(Exception e){
			throw new Exception("Unable to delete file "+e.getLocalizedMessage());
		}
	}
	/**
	* @TestCase Exploratory: Verify move to & delete from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyMoveToDelInTLview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		
		log.info("Verifying delete option from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
	    CommonFunctions.scrollsToTitle(driver, "Trash");
	    
	    /*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
	    String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
	    	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("SearchTLGmailSubject"))){
	    	log.info(" *************** Mail is deleted and is in Trash ************");
	    	}else{
	    		throw  new Exception("!!!!!! Mail is not displayed in Trash !!!!!!!!!! ");
	    	}*/
	    if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InboxMail").replace("MailSubject",verifyVariousSearchOperationsData.get("SearchTLGmailSubject")))){
	    	log.info("*********** Mail is present in Trash folder  ************");
	    }
	    else{
	    	throw new Exception("!!!!!!!! Mail is not present in the Trash folder !!!!!!!!!!!! ");
	    }
	    
	    
//	    	driver.navigate().back();
	    	log.info("Verifying moveTo inbox from TL view");
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
	    	Thread.sleep(2000);
	    	if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
	    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
	    	}     	else{
	    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
	    	        	
	    	}
	    	driver.navigate().back();
	    	Thread.sleep(5000);
		}
	/**
	* @TestCase Exploratory: Verify move to & delete from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyMoveToDelInCVview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		
		log.info("Verifying delete option from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
	    CommonFunctions.scrollsToTitle(driver, "Trash");
	   /* if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InboxMail").replace("MailSubject",verifyVariousSearchOperationsData.get("SearchCVGmailSubject")))){
	    	log.info("*********** Mail is present in Trash folder  ************");
	    }
	    else{
	    	throw new Exception("!!!!!!!! Mail is not present in the Trash folder !!!!!!!!!!!! ");
	    }*/
	    openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
	    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
	    	Thread.sleep(2000);
	    	if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
	    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
	    	}     	else{
	    		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
	    	        	
	    	}
	    	driver.navigate().back();
	    	Thread.sleep(5000);
		}
	/**
	* @TestCase To search with specific subject
	* @throws: Exception
	* @author batchi
	*/
	public void searchMethodWithSubject(AppiumDriver driver, String MailSubject) throws  Exception{
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
		CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),MailSubject );
		((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
		Thread.sleep(2000);
		log.info("Searched for a mail and performing MoveTo and Delete actions in TL view");
	 }
	/**
	* @TestCase To search with specific subject
	* @throws: Exception
	* @author Santosh
	*/

	public void verifyReplyAndReplyAllFromSettings(AppiumDriver driver) throws Exception{
		try {

			 Map<String, String>replyandreplyallsettings = commonPage.getTestData("replyandreplyallsettings");
	          String command4 = path + java.io.File.separator + "Mail" + java.io.File.separator +"MailAlert"+ java.io.File.separator+ "ReplyAllSettings.bat";
	          Process p = new ProcessBuilder(command4).start();
	          log.info("Executed the bat file to generate a mail"+command4);
	          userAccount.switchMailAccount(driver, replyandreplyallsettings.get("Gig1Account"));
	          Thread.sleep(2000);
	          inbox.pullToReferesh(driver);
	          userAccount.navigateToSettings(driver);
	    // Start -- NavigateGeneralSettingsFromSettings -- Reply      
	          userAccount.NavigateGeneralSettingsFromSettings(driver);
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DefaultReplyAction"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyDefaultReplyAction"));
	          Thread.sleep(500);
	          driver.navigate().back();
	          driver.navigate().back();
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replymail"));
	          log.info("General settings reply button verification");
	          Thread.sleep(1000);
	          /*if(CommonFunctions.getSizeOfElements(driver, replyandreplyallsettings.get("replysubmail"))!=0){
	        	  CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replysubmail"));
				 }*/
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("ReplyButton"));
	          Thread.sleep(2000);
	          driver.hideKeyboard();
	          Thread.sleep(2000);
	   //verify with mailId as well --Bindu
	          String etouchDemoOne = replyandreplyallsettings.get("etouchdemoone");
	          String toFieldText = driver.findElement(By.xpath(replyandreplyallsettings.get("ToFeild"))).getText();
	          toFieldText = toFieldText.substring(1, toFieldText.length()-3);
				
	          if(!etouchDemoOne.equals(toFieldText)) {
	        	  throw new Exception("!!!!!!!! Reply verification Failed !!!!!!!!!!!! ");
	          }
	  //draft  discard, instead of having multiple backs.  to avoid draft being a probelm for next scenarios --Bindu
	          Thread.sleep(2000);
	          CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
			  CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));//DiscardMail
			  CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardMail"));
//	          driver.navigate().back();
//	          driver.navigate().back();
	          driver.navigate().back();
	 //End -- NavigateGeneralSettingsFromSettings -- Reply       
	 // Start -- openUserAccountSetting --  Reply All         
	          userAccount.openUserAccountSetting(driver, replyandreplyallsettings.get("Gig1Account"));
	          Thread.sleep(500);
	          scrollTo("Default reply action");
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DefaultReplyAction"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyAllDefaultReplyAction"));
	          Thread.sleep(500);
	          driver.navigate().back();
	          driver.navigate().back();
	       // verify Gig2 settings for default reply action --Bindu  start
	          userAccount.openUserAccountSetting(driver, replyandreplyallsettings.get("Gig2"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DefaultReplyAction"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyDefaultReplyAction"));
	          Thread.sleep(500);
	          driver.navigate().back();
	          driver.navigate().back();
	       // verify Gig2 settings for default reply action --Bindu end
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replymail"));
	          log.info("Account settings replyAll button verification");
	          Thread.sleep(1000);
	          
	          if(CommonFunctions.getSizeOfElements(driver, replyandreplyallsettings.get("ReplyAllButton"))!=0){
					 log.info("ReplyAll button is verified");
				 }else {
					 log.info("ReplyAll button is not verified");
				 }
	          Thread.sleep(1000);
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("ReplyAllButton"));
	 // verification missing for reply all accounts are  displayed or not --Bindu  start
	          String replyllToFieldText = driver.findElement(By.xpath(replyandreplyallsettings.get("ToFeild"))).getText();
	          replyllToFieldText = replyllToFieldText.substring(1, replyllToFieldText.length()-3);
	          String ccMailValue = replyandreplyallsettings.get("Gig1Account");
	          String ccFieldText = driver.findElement(By.xpath(replyandreplyallsettings.get("CCField"))).getText();
	          ccFieldText = toFieldText.substring(1, ccFieldText.length()-3);
	          if(!ccMailValue.equals(ccFieldText) && !replyllToFieldText.equals(etouchDemoOne) ) {
	        	  throw new Exception("!!!!!!!! ReplyALL verification Failed !!!!!!!!!!!! ");
	          }
	       // verification missing for reply all accounts are  displayed or not --Bindu  end
	          driver.navigate().back();
	          //driver.navigate().back();
	          Thread.sleep(5000);
	          driver.navigate().back();
	          Thread.sleep(5000);
	  
	          launchGmailApplication(driver);
	          userAccount.switchMailAccount(driver, replyandreplyallsettings.get("Gig2"));
	          Thread.sleep(2000);
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replymail"));
	          log.info("General settings reply button verification after account cahnging");
	          Thread.sleep(1000);
	          
	          if(CommonFunctions.getSizeOfElements(driver, replyandreplyallsettings.get("ReplyButton"))!=0){
					 log.info("General settings reply button verification after account cahnging is verified");
				 }else { // Add statement to throw an error. --Bindu
					 log.info("General settings reply button verification after account cahnging is not verified");
				 }
	          driver.navigate().back();
	 // End -- openUserAccountSetting --  Reply All         
	          userAccount.navigateToSettings(driver);
	  // start --         NavigateGeneralSettingsFromSettings -- Reply all ,  openUserAccountSetting -- Reply 
	          userAccount.NavigateGeneralSettingsFromSettings(driver);
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DefaultReplyAction"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyAllDefaultReplyAction"));
	          Thread.sleep(500);
	          driver.navigate().back();
	          driver.navigate().back();
	          
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replymail"));
	          log.info("Account settings replyAll button verification");
	          Thread.sleep(1000);
	          
	         
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("ReplyAllButton"));
	 
	          log.info(" replied to all senders verification");
	          Thread.sleep(1000);
	          driver.navigate().back();
	          driver.navigate().back();
	          Thread.sleep(2000);
	          driver.navigate().back();
	          userAccount.openUserAccountSetting(driver, replyandreplyallsettings.get("Gig1Account"));
	          Thread.sleep(500);
	          scrollTo("Default reply action");
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DefaultReplyAction"));
	          Thread.sleep(500);
	          CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReplyDefaultReplyAction"));
	          Thread.sleep(500);
	          driver.navigate().back();
	          driver.navigate().back();
	          userAccount.switchMailAccount(driver, replyandreplyallsettings.get("Gig1Account"));
	          Thread.sleep(2000);
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replymail"));
	          log.info("Account settings replyAll button verification");
	          Thread.sleep(1000);
	         /* 
	          if(CommonFunctions.getSizeOfElements(driver, replyandreplyallsettings.get("replysubmail"))!=0){
	        	  CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("replysubmail"));
				 }*/
	          Thread.sleep(1000);
	          if(CommonFunctions.getSizeOfElements(driver, replyandreplyallsettings.get("ReplyButton"))!=0){
					 log.info("Account level Reply button is verified");
				 }else { // Add statement to throw an error. --Bindu
					 log.info("Account level Reply button is not verified");
				 }
	          CommonFunctions.searchAndClickByXpath(driver, replyandreplyallsettings.get("ReplyButton"));
	          log.info(" replied to all senders verification");
	 // End --         NavigateGeneralSettingsFromSettings -- Reply all ,  openUserAccountSetting -- Reply  
	          Thread.sleep(1000);
	          driver.navigate().back();
	          log.info("***************Completed testcase :: Verify Reply/Reply All setting in settings***********");
	          driver.navigate().back();

		}catch (Throwable e) {
			  commonPage.catchBlock(e, driver, "verifyReplyAndReplyAllFromSettings");
	}
	}
	/**
	 * @TestCase 13 Verify able to edit IMAP account settings			 *
	 * @Pre-condition :  IMAP account settings
	 * @throws: Exception
	 * @author Santosh
	 * GMAT
	 */	 
	public void AbleToEditIMAPAccountSettings(AppiumDriver driver)throws Exception{
		try{
			 Map<String, String> IMAPAccountSettings = commonPage.getTestData("IMAPAccountSettings");
			 
			 userAccount.switchMailAccount(driver, IMAPAccountSettings.get("etouchTestAccount"));
			 Thread.sleep(600);
			 userAccount.navigateToSettings(driver);
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("eTouchOne"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("yourName"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("EditYourName"), "etouch test one");
			 Thread.sleep(2000);
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
			 
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("Signature"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("SignatureText"), IMAPAccountSettings.get("SignatureTextEntry"));
			 Thread.sleep(2000);
			 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("ImageCheck"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("AskBeforeDisplayingImages"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("syncfrequency"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("SyncSelection"));
			 Thread.sleep(600);
			 String syncVerify = driver.findElement(By.xpath(IMAPAccountSettings.get("syncfrequencyverify"))).getText();
			 boolean syncValue = syncVerify.equalsIgnoreCase(IMAPAccountSettings.get("synfrequencytext"));
			 if(syncValue == true){
				 log.info("Sync frequency verified");
				}else
				{
					//log.info("Sync frequency not verified");
					throw new Exception("!!!!!!!!Sync frequency not verified !!!!!!!!!!!! ");
					
				}
			 Thread.sleep(1000);
			 scrollDown(1);
			 Thread.sleep(2000);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("Incomingsettings"));
			 Thread.sleep(1000);
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("incomingport"), "995");
			 Thread.sleep(1000);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("DoneButton"));
			 Thread.sleep(2000);
			 driver.navigate().back();							 
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("incomingport"), IMAPAccountSettings.get("incomingporttext"));
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("DoneButton"));
			 log.info("incoming settings verification done");
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("outgoingsettings"));
			 Thread.sleep(600);
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("outgoingsmpt"), "smtp-mail.outlook.comss");
			 Thread.sleep(2000);
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("DoneButton"));
			 Thread.sleep(1000);
			 driver.navigate().back();
			 CommonFunctions.searchAndSendKeysByXpath(driver, IMAPAccountSettings.get("outgoingsmpt"), IMAPAccountSettings.get("outgoingsmpttext"));
			 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("DoneButton"));
			 log.info("outgoing settings verification done");
			 Thread.sleep(5000);
			 driver.navigate().back();
			 driver.navigate().back();
			 driver.navigate().back();
			 
			 userAccount.launchGmailApp(driver);
			 Thread.sleep(1000);
			 log.info("Selecting Compose button (FAB)");
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			 Thread.sleep(2000);
			 String signatureVerify = driver.findElement(By.xpath(IMAPAccountSettings.get("Signatureverify"))).getText().trim();
			 boolean signatureValue = signatureVerify.equalsIgnoreCase(IMAPAccountSettings.get("SignatureTextEntry"));
			 if(signatureValue == true){
				 log.info("Signature verified");
				
				}else
				{
					//log.info("Signature not verified");
					throw new Exception("!!!!!!!!Signature not verified !!!!!!!!!!!! ");
				}
			 Thread.sleep(1000);
			 driver.navigate().back();
			 driver.navigate().back();
			 Thread.sleep(1000);
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			 Thread.sleep(2000);
			 String yourNameVerify = driver.findElement(By.xpath(IMAPAccountSettings.get("yourNameVerify"))).getText();
			 boolean yourNameValue = yourNameVerify.equalsIgnoreCase(IMAPAccountSettings.get("yournametextverify"));
			 if(yourNameValue == true){
				 log.info("your name verified");
				}else
				{
					throw new Exception("!!!!!!!!your name not verified !!!!!!!!!!!! ");
				}
			 Thread.sleep(1000);
			 driver.navigate().back();
			 Thread.sleep(1000);
			 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchIconXpath"));
				Thread.sleep(1500);
				commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("SearchIconXpath"), "imap image check");
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				Thread.sleep(800);
				verifyMailPresent(driver, "imap image check");
				Thread.sleep(1200);
				CommonFunctions.isElementByXpathDisplayed(driver,IMAPAccountSettings.get("showpictures"));
			     log.info("Verfied Images are not displayed");
			     Thread.sleep(1200);
			     if(CommonFunctions.getSizeOfElements(driver, IMAPAccountSettings.get("showpictures"))!=0){
			    	 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("showpictures"));
			    	 log.info("Images are displayed");
					 }else {
						 log.info("Images are not displayed");
						 throw new Exception("!!!!!!!!Images are not displayed !!!!!!!!!!!! ");
					 }
			     Thread.sleep(500);
			     driver.navigate().back();
			     driver.navigate().back();
			     Thread.sleep(500);
			     userAccount.navigateToSettings(driver);
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("eTouchOne"));
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("ImageCheck"));
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, IMAPAccountSettings.get("AlwayShowExternalImages"));
				 Thread.sleep(500);
			     driver.navigate().back();
			     driver.navigate().back();
			     Thread.sleep(1200);
			     CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchIconXpath"));
					Thread.sleep(1500);
					commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("SearchIconXpath"), "imap image check");
					((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
					Thread.sleep(800);
					verifyMailPresent(driver, "imap image check");
					log.info("Verfied Images are displayed");
					Thread.sleep(1200);
					if(CommonFunctions.getSizeOfElements(driver, IMAPAccountSettings.get("testimage"))!=0){
				    	 log.info("Images are displayed");
						 }else {
							 throw new Exception("!!!!!!!!Images are not displayed !!!!!!!!!!!! ");
						 }
					 Thread.sleep(500);
					 driver.navigate().back();
				     driver.navigate().back();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/**
	 * @TestCase Verify label Notification Enabled And Inbox Sync Disabled
	 * @Pre-condition :  IMAP account settings
	 * @throws: Exception
	 * @author Santosh
	 * GMAT
	 */	 

	public void labelNotificationEnabledAndInboxSyncDisabled(AppiumDriver driver)throws Exception{
		try{
			 Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");

			 userAccount.navigateToSettings(driver);
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxType"));						 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("PriorityInbox"));
			 Thread.sleep(500);
			
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Managelables"));
			 Thread.sleep(500);
			 
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("ClickInbox"));
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSync"));
			 log.info("selected LabelSync Messages...");
			 Thread.sleep(500);
			 
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSyncNone"));
			 log.info("selected LabelSync None...");
			 driver.navigate().back();
			 Thread.sleep(100);
			 CommonFunctions.scrollsToTitle(driver, "LabelNotificationEnabled");
			 Thread.sleep(100);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelNotificationEnabled"));
			 log.info("clicked on LabelNotificationEnabled...");
			 Thread.sleep(500);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSync"));
			 log.info("selected LabelSync Messages...");
			 Thread.sleep(500);
			 String checkSyncLabel=driver.findElement(By.xpath(navigateToSettings.get("SyncTestLabel"))).getAttribute("checked");
			 if(checkSyncLabel.equalsIgnoreCase("true"))
			{
				 driver.navigate().back();
				 log.info("Notifications are enabled");
		    }else {
		    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("SyncTestLabel"));
		    	 driver.navigate().back();
		    	 log.info("clicked on SyncTestLabel...");
		    }
			 Thread.sleep(1000);
			 String checkLabelNoti=driver.findElement(By.xpath(navigateToSettings.get("LabelNotificationCheck"))).getAttribute("checked");
			 if(checkLabelNoti.equalsIgnoreCase("true"))
			{
				 log.info("Notifications are enabled");
		    }else {
		    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("LabelNotificationCheck"));
		    	 log.info("LabelNotification checked...");
		    }
			 

			 driver.navigate().back();
			 driver.navigate().back();
			 driver.navigate().back();
			 driver.navigate().back();
			  
			 userAccount.switchMailAccount(driver, navigateToSettings.get("AccountOne"));
			 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert" +java.io.File.separator +"LabelNotificationEnabled.bat";
			 Process p = new ProcessBuilder(command).start();
			 log.info("Executed Bat file"+command);
			 Thread.sleep(6000);

				    navDrawer.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
					Thread.sleep(800);
					CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
					Thread.sleep(1000);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					Thread.sleep(1000);
					CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
					Thread.sleep(5000);
					driver.navigate().back();
					Thread.sleep(2000);
					 ((AndroidDriver)driver).openNotifications();
					 Thread.sleep(3000);

	     	 commonPage.verifySubjectIsMatching(driver,navigateToSettings.get("VerifyLabelNotification1"));
			 Thread.sleep(2000);
			 if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("NotificationRemoveLabel"))!=0){
				 log.info("NotificationRemoveLabel is verified");
			 }else {
				 throw new Exception("!!!!!!!!NotificationRemoveLabel is not verified !!!!!!!!!!!! ");
			 }
			 Thread.sleep(800);
			//  Add Exception  points and  Merge "verifyManageLabelsAndEnableLabelNotifications" in this TC itself --Bindu
			 if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("NotificationReplyLabel"))!=0){
				 log.info("NotificationReplyLabel is verified");
			 }else {
				 throw new Exception("!!!!!!!!NotificationReplyLabel is not verified !!!!!!!!!!!! ");
			 }
			 // resettings to normal 
			 Thread.sleep(600);
			 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("VerifyLabelNotification1"));
			 Thread.sleep(1000);
			 driver.navigate().back();
			 Thread.sleep(600);
			 userAccount.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
				CommonFunctions.scrollsToTitle(driver, "Inbox type");
				inbox.verifyInboxTypes(driver);
				CommonFunctions.scrollsToTitle(driver, navigateToSettings.get("DefaultInbox"));
				Thread.sleep(800);
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Managelables"));
				 Thread.sleep(500);
				 
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("ClickInbox"));
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSync"));
				 log.info("selected LabelSync Messages...");
				 Thread.sleep(500);
				 
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("SyncTestLabel"));
				driver.navigate().back();
				driver.navigate().back();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		/**
		 * @TestCase Verify manage  Labels And Enable Label Notifications
		 * @Pre-condition :  IMAP account settings
		 * @throws: Exception
		 * @author Santosh
		 * GMAT
		 */	 
		public void manageLabelsAndEnableLabelNotifications(AppiumDriver driver)throws Exception{
			try{
				 Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");
				 Map<String, String>changeLabels = commonPage.getTestData("changeLabels");

				 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenuNew"));
		            log.info("Clicked on Hambugermenu");
		         //  CommonFunctions.scrollTo(driver, "Settings");
		            CommonFunctions.scrollsToTitle(driver, "Settings");            
		            log.info("Scrolled to settings");
		            log.info("clicked on settings");
//				 userAccount.navigateToSettings(driver);
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Managelables"));
				 CommonFunctions.scrollsToTitle(driver, "LabelNotification");
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, changeLabels.get("LabelNotification"));
				 log.info("clicked on LabelNotification...");
				 Thread.sleep(500);
				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("LabelSync"));
				 log.info("selected LabelSync Messages...");
				 Thread.sleep(500);
				 String checkSyncLabel=driver.findElement(By.xpath(navigateToSettings.get("SyncTestLabel"))).getAttribute("checked");
				 if(checkSyncLabel.equalsIgnoreCase("true"))
				{
					 driver.navigate().back();
					 log.info("Notifications are enabled");
			    }else {
			    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("SyncTestLabel"));
			    	 driver.navigate().back();
			    	 log.info("clicked on SyncTestLabel...");
			    }
				 Thread.sleep(1000);
				 String checkLabelNoti=driver.findElement(By.xpath(navigateToSettings.get("LabelNotificationCheck"))).getAttribute("checked");
				 if(checkLabelNoti.equalsIgnoreCase("true"))
				{
					// driver.navigate().back();
					 log.info("Notifications are enabled");
			    }else {
			    	 CommonFunctions.searchAndClickByXpath(driver,navigateToSettings.get("LabelNotificationCheck"));
			    	// driver.navigate().back();
			    	 log.info("LabelNotification checked...");
			    }
				 
//				 Thread.sleep(1000);
				 driver.navigate().back();
				 driver.navigate().back();
				 driver.navigate().back();
				 driver.navigate().back();
				 
				 userAccount.switchMailAccount(driver, navigateToSettings.get("AccountOne"));
				 command = path+java.io.File.separator+"Mail"+java.io.File.separator+"LabelNotification.bat";
				 Process p = new ProcessBuilder(command).start();
				 log.info("Executed Bat file"+command);
				 Thread.sleep(6000);
//				 ((AndroidDriver)driver).openNotifications();
//				 Thread.sleep(1000);
//				 if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("VerifyLabelNotification"))==0){
//					 log.info("Notification recived");
//				 }else {
					 log.info("Notification not recived");
//					 driver.navigate().back();
					 Thread.sleep(800);
					 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenuNew"));
			            log.info("Clicked on Hambugermenu");
			            Thread.sleep(800);
	 
					    navDrawer.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
						Thread.sleep(800);
						CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
						Thread.sleep(1000);

						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
						Thread.sleep(1000);
						CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
						Thread.sleep(5000);
						driver.navigate().back();
						Thread.sleep(2000);
						 ((AndroidDriver)driver).openNotifications();
						 Thread.sleep(3000);
//				 }
				
//				 userAccount.openUserAccountSetting(driver, navigateToSettings.get("ToFieldData"));
//				 Thread.sleep(1000);
//				 userAccount.EnableSyncOnOFf(driver);;
//				 Thread.sleep(4000);
	//
				 commonPage.verifySubjectIsMatching(driver,navigateToSettings.get("VerifyLabelNotification"));
				 Thread.sleep(2000);
	 CommonPage.waitForElementToBePresentByXpath(navigateToSettings.get("NotificationExpandButton"));
//				 Thread.sleep(1000);
	//
//				 CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("NotificationExpandButton"));
				 Thread.sleep(2000);
				 if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("NotificationRemoveLabel"))!=0){
					 log.info("NotificationRemoveLabel is verified");
				 }else {
					 log.info("NotificationRemoveLabel is not verified");
				 }
				 Thread.sleep(800);
				 if(CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("NotificationReplyLabel"))!=0){
					 log.info("NotificationReplyLabel is verified");
				 }else {
					 log.info("NotificationReplyLabel is not verified");
				 }

			}catch(Exception e){
				e.printStackTrace();
			}
		}
		/**Method to verify Prompt To Enter Password
		 *
		 * @param driver
		 * @author Santosh
		 * @throws Exception
		 */
		public void PromptToEnterPassword(AppiumDriver driver) throws Exception{
		    try{
		 	   Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");
		 	   Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		 	   log.info("******************* Start verifyPromptToEnterPassword Functionality*******************");

		   	   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("BccFieldAttachments"));
		   	   Thread.sleep(2500);//wrongPasswordVerify
		   		CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
				Thread.sleep(1500);
				commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), "Password  protection");
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				Thread.sleep(800);
				openMail(driver,"Fwd: Password protection" );
				Thread.sleep(2000);
				CommonFunctions.scrollToCellByTitle(driver, "pdf-example-password.original.pdf");
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("promptAttacmentSelect"));
				log.info("promptattacmentselect found and clicked");
				Thread.sleep(2500);
				CommonFunctions.searchAndSendKeysByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptEnterPassword"), "open");
				//driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptEnterPassword"))).sendKeys("test");
				log.info("pdf password entered");
				Thread.sleep(2500);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptOpen"));
				Thread.sleep(200);
				if(CommonFunctions.getSizeOfElements(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("wrongPasswordVerify"))!=0){
					log.info("pdf password wrong entered ");
				}else{
					throw new Exception("!!!!!!!! pdf password verification Failed !!!!!!!!");
				}
				CommonFunctions.searchAndSendKeysByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptEnterPassword"), "test");
				//driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptEnterPassword"))).sendKeys("test");
				log.info("pdf password entered");
				Thread.sleep(500);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PromptOpen"));
				
				Thread.sleep(2500);
				driver.navigate().back();
//				CommonFunctions.searchAndSendKeysByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("promptattacmentselect"), "open");
//				Thread.sleep(2000);

				

			 	log.info("******************* Completed testcase verifyPromptToEnterPassword Functionality*******************");
			 	
				}catch(Exception e){
					throw new Exception("Unable to open file "+e.getLocalizedMessage());
				}
		}
		/**Method to Verify Able To Archive And Mute From Search
		 *
		 * @param driver
		 * @param EmailSubject
		 * @throws Exception
		 * @author Santosh
		 */
		public void AbleToArchiveAndMuteFromSearch(AppiumDriver driver, String EmailSubject)throws Exception{
			Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
			Map<String, String> verifyUserIsAbleToMuteAConversation = commonPage.getTestData("verifyUserIsAbleToMuteAConversation");
		     String path = System.getProperty("user.dir");
			 String command = path+java.io.File.separator+"Mail"+java.io.File.separator+"MailAlert"+java.io.File.separator+"ArchiveMailFromSearch.bat";
			 log.info("Executing the bat file to generate a mail and move to other folder "+command);
			// Process p = new ProcessBuilder(command).start();
			 Runtime.getRuntime().exec(command);
			 log.info("Executed the bat file to generate a mail");
			 Thread.sleep(4000);
			try{
				userAccount.switchMailAccount(driver,  verifyUserIsAbleToMuteAConversation.get("AccountOne"));
				Thread.sleep(500);
				//navDrawer.openPrimaryInbox(driver, verifyUserIsAbleToMuteAConversation.get("AccountOne"));
				pullToReferesh(driver);
				Thread.sleep(2000);
				navDrawer.navigateToPrimary(driver);
				CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
				Thread.sleep(1500);
				commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), EmailSubject);
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				Thread.sleep(800);
				openMail(driver, EmailSubject);
				Thread.sleep(1200);
				CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
				if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("MovetoInbox"))==0){
				log.info("Verified Move to inbox is not available ");
			}else{
				throw new Exception("Verified Move to inbox is available ");
			}
			driver.navigate().back();
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
				Thread.sleep(1200);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
				Thread.sleep(800);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ArchivedSnackBar"))!=0){
					log.info("Archived snack bar is displayed and the mail is archived");
				}else{
					throw new Exception("Archived snack bar is not displayed");
				}
				// Have to verify if the mail is removed from Inbox or not, rather than verifying the Snackbar.--Bindu			
				verifyMailNotPresent(driver, EmailSubject); // checking mail is present or not
				Thread.sleep(800);
				driver.navigate().back();
				Thread.sleep(800);
				navDrawer.navigateToAllEmails(driver);
				Thread.sleep(1000);
				verifyMailPresent(driver, EmailSubject);
				Thread.sleep(1200);
				CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
				if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("MovetoInbox"))!=0){
				log.info("Verified Move to inbox is available ");
			  }else{
				  throw new Exception("Move to inbox is not available");
			  }
				driver.navigate().back();
				Thread.sleep(500);
				driver.navigate().back();
				navDrawer.openPrimaryInbox(driver, verifyUserIsAbleToMuteAConversation.get("AccountOne"));
			}
			catch(Exception e){
				throw new Exception("Unable to archive the mail "+e.getLocalizedMessage());
			}
		}


	public void verifyUserIsAbleToMuteFromSearch(AppiumDriver driver, String EmailSubject){

				try {
					Map<String, String> mailMoveToOtherFolderData = commonPage.getTestData("mailMoveToOtherFolder");
					Map<String, String> verifyUserIsAbleToMuteAConversation = commonPage.getTestData("verifyUserIsAbleToMuteAConversation");
				    Map<String, String>changeLabels = commonPage.getTestData("changeLabels");
				    Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
				   // userAccount.switchMailAccount(driver,  verifyUserIsAbleToMuteAConversation.get("AccountOne"));
				    
				    String  gigMailId = CommonFunctions.getNewUserAccount(verifyUserIsAbleToMuteAConversation, "AccountOne", null);
					userAccount.switchMailAccount(driver, gigMailId);
					
				    Thread.sleep(1000);
				    String fromUser=inbox.composeAndSendNewMail(driver,
				    		verifyUserIsAbleToMuteAConversation.get("etouchtestthree"),
				    		verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"), 
				    		verifyUserIsAbleToMuteAConversation.get("gig4"),
				    		EmailSubject,
				    		verifyUserIsAbleToMuteAConversation.get("ComposeBodyData"));

					 Thread.sleep(1000);
					 //userAccount.switchMailAccount(driver,  verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));
					 String  gigMailId2 = CommonFunctions.getNewUserAccount(verifyUserIsAbleToMuteAConversation, "etouchtesttwo", null);
						userAccount.switchMailAccount(driver, gigMailId2);
						pullToReferesh(driver);
						 Thread.sleep(2000);
						commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), mailMoveToOtherFolderData.get("SearchSubjectWithInbox"));
						((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
						Thread.sleep(600);
//						verifyMailPresent(driver, EmailSubject);
						openMail(driver,EmailSubject );
						Thread.sleep(1200);
//						CommonPage.waitForElementToBePresentByXpath(commonElements.get("MailMoreOptionsNavigation"));
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
						 CommonFunctions.searchAndClickByXpath(driver,verifyUserIsAbleToMuteAConversation.get("ClickMute"));
						 CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("Verify1Muted"));
						 log.info("1 muted  is displayed");
					     //Instead of verifying undo action its better to verify the mail is removed or not. --Bindu	 
						 CommonFunctions.isElementByXpathDisplayed(driver,verifyUserIsAbleToMuteAConversation.get("VerifyUNDO"));
						 log.info("UNDO  is displayed");
						 Thread.sleep(2000);
						driver.navigate().back();
						commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), mailMoveToOtherFolderData.get("SearchSubjectWithInbox"));
						((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER); 
						Thread.sleep(2000);
						 verifyMailNotPresent(driver, EmailSubject); // checking mail is present or not
						 Thread.sleep(800);
						 driver.navigate().back();
						 navDrawer.navigateToAllEmails(driver);
						 Thread.sleep(1000);
						 verifyMailPresent(driver, EmailSubject);
						 Thread.sleep(800);
					   //No verification point here, not verifying the mail is muted or not. Instead verifying in Allmail  whihc is not right --Bindu
							// Also there should be an update mail to muted mail in order to verify if it goes to inbox or All mail --Bindu  
					     if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("MuteLabel"))!=0){
								log.info("Verified Mute Label is available ");
							  }else{
								  throw new Exception("Mute Label is not available");
							  }
					     driver.navigate().back();
					   //  selecting gig3 account
					     Thread.sleep(600);
					     //userAccount.switchMailAccount(driver,  verifyUserIsAbleToMuteAConversation.get("etouchtestthree"));
					     String  gigMailId3 = CommonFunctions.getNewUserAccount(verifyUserIsAbleToMuteAConversation, "etouchtestthree", null);
							userAccount.switchMailAccount(driver, gigMailId3);
					     Thread.sleep(600);
					     commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), EmailSubject);
							((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
							Thread.sleep(800);
							verifyMailPresent(driver, EmailSubject);
							Thread.sleep(1200);
							CommonFunctions.searchAndClickByXpath(driver, verifyUserIsAbleToMuteAConversation.get("ReplyAllButton"));
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeBody"));
							CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), "verify mute ");
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
							 Thread.sleep(600);
							 driver.navigate().back();
							 driver.navigate().back();
							 userAccount.switchMailAccount(driver,  verifyUserIsAbleToMuteAConversation.get("etouchtesttwo"));
							 verifyMailNotPresent(driver, EmailSubject);
							 navDrawer.navigateToAllEmails(driver);
							 Thread.sleep(1000);
							 verifyMailPresent(driver, EmailSubject);
							 Thread.sleep(2000);
							 if(CommonFunctions.getSizeOfElements(driver, verifyUserIsAbleToMuteAConversation.get("BodyTextVeriy"))!=0){
									log.info("Mail body text is available ");
								  }else{
									  throw new Exception("Mail body text is  not verified");
								  }
						     driver.navigate().back();
					     }
					    catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}



	/**
	* @TestCase Verify schedule send  
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public String verifyScheduleSendAfterSomeMins(AppiumDriver driver,String MailSubject , int addMin) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");

		
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("ScheduleSendOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozePickDateTime"));
		String currentTime = CommonFunctions.searchAndGetTextOnElementByXpath(driver, composeAndSendMailData.get("CurrentTimeField"));
		log.info("Current Time  is: "+currentTime);
		
		log.info("Clicking on the Time field");
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("CurrentTimeField"));
		log.info("Clicking on the keyboard toggle");
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("SwitchToTextInputToggle"));
		
		calendar.updatingTimeWithFewMins(driver,addMin);
		
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeOKCalendarBtn"));
		String scheduledTime = CommonFunctions.searchAndGetTextOnElementByXpath(driver, composeAndSendMailData.get("CurrentTimeField"));
		log.info("Scheduled Time  is: "+scheduledTime);
		
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("ScheduleSendButton"));
		waitForElementToBePresentByXpath(commonElements.get("ClickUndo"));	
		
		return scheduledTime;
	}
	public void afterScheduledTime(AppiumDriver driver, String time, String accountId, String Subject) throws Exception{
		Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
		log.info("Verifying the scheduled email in Account B");
		DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("  ").toFormatter();
		DateFormat df = new SimpleDateFormat("hh:mm");
		log.info("Scheduled Time: "+time);
		
		for(int i=0;i<=5;i++){
			LocalTime currentTime = LocalTime.now();
			log.info("Current System Time before formatting: "+currentTime);		
			DateFormat dateFormat = new SimpleDateFormat("h:mm aa");
	    	String sysTime = dateFormat.format(new Date()).toString();
			log.info("Current System Time: "+sysTime);
			
		if(sysTime.toString().equals(time.toString())){
			pullToReferesh(driver);
			Thread.sleep(5000);
			navDrawer.openMenuDrawer(driver);
			navDrawer.navigateToDrafts(driver);
			Thread.sleep(5000);
			navDrawer.navigateToScheduled(driver);
			Thread.sleep(5000);
			pullToReferesh(driver);
			Thread.sleep(10000);
			pullToReferesh(driver);
			Thread.sleep(10000);
			verifyMailNotPresent(driver,Subject);
			userAccount.switchMailAccount(driver, accountId);
			Thread.sleep(2000);
			verifyMailPresentNoOpen(driver, Subject);
			
			log.info("************ Scheduled mail is sent to Account " + accountId+ "******************");
			break;
		}
		else{
			log.info("Waiting for schedule time to meet current time");
			Thread.sleep(55000);
			pullToReferesh(driver);
			Thread.sleep(1000);
		}
		}
	}
	public String selectScheduleSendOption(AppiumDriver driver, String action, String scheduleOption) throws Exception{
		Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("mailCount"));
		String mailSubject = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID")) ;
		CommonFunctions.searchAndClickByXpath(driver, action);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("ScheduleSendOption"));
		CommonFunctions.searchAndClickByXpath(driver, scheduleOption);
		waitForElementToBePresentByXpath(commonElements.get("ClickUndo"));	
		driver.navigate().back();
		
		return mailSubject;
		
	}

	public void verifyCancelScheduleSend(AppiumDriver driver, String mailSubject) throws Exception{
		Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
		log.info("Clicking on the Cancel send icon");
		waitForElementToBePresentByXpath(composeAndSendMailData.get("CancelSendIcon"));	
		CommonFunctions.searchAndClickByXpath(driver, composeAndSendMailData.get("CancelSendIcon"));
		waitForElementToBePresentByXpath(composeAndSendMailData.get("GoToDraftsSnackBarButton"));
		CommonFunctions.searchAndClickByXpath(driver,composeAndSendMailData.get("GoToDraftsSnackBarButton") );
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", mailSubject);
		if(CommonFunctions.isElementByXpathDisplayed(driver,MailXpath)){
			log.info("*************** Cancelled schedule mail is displayed in Drafts ************** ");
		}else{
			throw new Exception("!!!!!!!!!!! Cancelled schedule mail is not displayed in Drafts folder !!!!!!!!!!!");
		}
	}
	/** Verifying if mail is present or not and it will not open the same mail
	 * @author batchi
	 * @param driver
	 * @param EmailSubject
	 * @throws Exception
	 */
	public void verifyMailPresentNoOpen(AppiumDriver driver, String EmailSubject)throws Exception{
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))==0){
			log.info("####### Email with subject "+EmailSubject+" is not found #######");
			Thread.sleep(3000);
			pullToReferesh(driver);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("EmailSubjectText").replace("Subject", EmailSubject))!=0){
				log.info(" ####### Email is displayed after refreshing by swiping down ###########");
			}else{
				throw new Exception("!!!!!!!!! Mail is not displayed due to sync issue or network slow connectivity !!!!!!!");
			}
		}else{
			log.info("############# Mail is displayed #########");
		}
	}
	/**
	* @TestCase Save to photos 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author Santosh
	*/
	
	public void saveToPhotos(AppiumDriver driver) throws Exception{
	    try{
	 	   Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

	 	   log.info("******************* Start verifySaveToPhotos Functionality*******************");
	   	 	Thread.sleep(2000);
/*	 	 	String fromUser=composeAsNewMailPhoto(driver,
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SubjectAttachments"),
					        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));			
*/
	   	 String fromUser = inbox.composeAsNewMail(driver,
	   			composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"),
	   			"","",
		        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SubjectAttachments"),
		        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
			log.info("Attach button clicked");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
			log.info("Show drive clicked");
			Thread.sleep(600);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
			log.info("etouchtestone drive cliced");
			pullToReferesh(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
			Thread.sleep(2000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
			log.info("Attachment1 clicked");		
		
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
			Thread.sleep(800);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			Thread.sleep(6000);
			userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
			log.info("Verifying the attachment");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("Verifyattachmentdraft1"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
			}else{
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Verifyattachmentdraft1"));
				log.info("Attachment found and clicked");
			}
			Thread.sleep(2500);
			
			try{

			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoSaveButton"));
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoUploadButton"));
			Thread.sleep(3000);
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("imageSelect"));
			Thread.sleep(100);
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("headerPhotoSaveButton"));
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoUploadButton"));
			log.info("All attachments found");
			}catch(Exception e){
				throw new Exception("Missing attachments");
			}
			Thread.sleep(2000);
		 	driver.navigate().back();
		 	log.info("******************* Completed testcase composeMailWithMultipleAttachmentsAndAnsertedDriveChips Functionality*******************");
		 	
//			
		 	
			}catch(Exception e){
				throw new Exception("Unable to attach file "+e.getLocalizedMessage());
			}
	}
	/**
	 * Method to verify Smart profile Hangouts options
	 * @author batchi
	 * @throws Exception
	 */
	public void verifySmartProfileHangoutsOption(AppiumDriver driver) throws Exception {
		Map < String, String > SmartProfileData = commonPage.getTestData("SmartProfile");
		log.info("*********** Verifying Hangout option from Smart profile  ************");
		openMail(driver, SmartProfileData.get("SmartProfileSubject"));
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ContactBadgeInCV"));
	waitForElementToBePresentByXpath(SmartProfileData.get("HangoutOption"));
	CommonFunctions.searchAndClickByXpath(driver,SmartProfileData.get("HangoutOption") );
	waitForElementToBePresentByXpath(SmartProfileData.get("HangoutSignInID"));
	CommonFunctions.isElementByXpathDisplayed(driver, SmartProfileData.get("HangoutToolBar"));	
	navigateBack(driver);
	}
	/**
	 * Method to verify Smart profile Schedule options
	 * @author batchi
	 * @throws Exception
	 */
	public void verifySmartProfileScheduleOption(AppiumDriver driver) throws Exception {
		Map < String, String > SmartProfileData = commonPage.getTestData("SmartProfile");
		log.info("*********** Verifying Schedule option from Smart profile  ************");
		CommonFunctions.searchAndClickByXpath(driver, SmartProfileData.get("ScheduleOption"));
		driver.hideKeyboard();
		CommonFunctions.isElementByXpathDisplayed(driver, SmartProfileData.get("CalendarSaveButton"));
		navigateBack(driver);
		Thread.sleep(2000);
		navigateBack(driver);
	}

	/**
	 * Method to verify Smart profile Compose from contact info mail id options
	 * @author batchi
	 * @throws Exception
	 */
	public void verifySmartProfileMailIDComposeOption(AppiumDriver driver) throws Exception {
		Map < String, String > SmartProfileData = commonPage.getTestData("SmartProfile");
		log.info("*********** Verifying mail id compose option from Smart profile  ************");
		CommonFunctions.searchAndClickByXpath(driver, SmartProfileData.get("ContactInfo"));
		driver.hideKeyboard();
		commonFunction.isElementByXpathDisplayed(driver, SmartProfileData.get("ComposeTitle"));
		navigateBack(driver);	
	}
	/**
	 * Method to verify Smart profile Compose from email options
	 * @author batchi
	 * @throws Exception
	 */
	public void verifySmartProfileEMailOption(AppiumDriver driver) throws Exception {
		Map < String, String > SmartProfileData = commonPage.getTestData("SmartProfile");
		log.info("*********** Verifying Email option from Smart profile  ************");
	CommonFunctions.searchAndClickByXpath(driver, SmartProfileData.get("EmailActionOption"));
	driver.hideKeyboard();
	String tofieldId = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("ToFieldXpath"));
	CommonFunctions.searchAndSendKeysByXpath(driver, SmartProfileData.get("SubjectField"), SmartProfileData.get("SubjectForEmailOption"));
	CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), SmartProfileData.get("SubjectForEmailOption"));
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));

	driver.navigate().back();
	Thread.sleep(1000);
	driver.navigate().back();
	navDrawer.navigateToSentBox(driver);
	openMail(driver, SmartProfileData.get("SubjectForEmailOption"));
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ViewRecipientDetails"));
	String detailToId =	CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("DetailToEmail"));
	if(detailToId.contains(tofieldId)){
		log.info(" ^^^^^^^^^^^^ Email is successfully sent from Smart Profile ^^^^^^^^^^^^^");
	}else {
		throw new Exception("Email is not sent");
	}	
	navigateBack(driver);
	navigateBack(driver);
	}
	/**
	* @TestCase Verify Save option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerSaveOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Save option******************");
		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"));
		   String fromUser = inbox.composeAsNewMail(driver,
		   			composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"),
		   			"","",
			        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"),
			        composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ComposeBodyAttachments"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
				log.info("Show drive clicked");
				Thread.sleep(600);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
				log.info("etouchtestone drive cliced");
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"));
				log.info("Attachment Appium.jpg is  clicked");
				//Attaching AppiumNew.jpg 
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFile"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AttachFileText"));
				log.info("Attach button clicked");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShowDriveRoots"));
				log.info("Show drive clicked");
				Thread.sleep(600);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
				log.info("etouchtestone drive cliced");
				pullToReferesh(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriveTitle"));
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AppiumNewImage"));
				log.info("Attachment AppiumNew.jpg is  clicked");		
				
				Thread.sleep(5000);
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SendMailButton"));
				Thread.sleep(800);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
				Thread.sleep(6000);
				userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
				Thread.sleep(4000);
				pullToReferesh(driver);
				
				openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
				CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage") );
				waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
				CommonFunctions.searchAndClickByXpath(driver,commonElements.get("EventSaveButton"));
				log.info("Launching Downloads app");
				Process p;
				String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.documentsui/com.android.documentsui.files.FilesActivity"};
				    p = new ProcessBuilder(p1).start();
				    log.info("Trying to launch the app");
				Thread.sleep(6000);
				
				if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
					
					log.info("@@@@@@@@@@@@ Saved image is displayed in Downloads @@@@@@@@@@@");
					//Deleting the image from downloads
					log.info("Double tapping on the image");
					AndroidElement element = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage")));
					driver.tap(1, element, 500);
					log.info("Deleting the image");
					CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
						log.info("############ Appium.jpg is successfully deleted ####################");
					}else{
						throw new Exception("((((((((((((((((((((( Image is not deleted )))))))))))))))))))))");
					}
					
					
				}else{
					throw new Exception("%%%%%%%%%% Image is not saved %%%%%%%%%%%%%");
				}
	}
	/**
	* @TestCase Verify Save All option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerSaveAllOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Save All option******************");
		   launchGmailApp(driver);
//		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage") );
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerSaveAllOption"));
			
			log.info("Launching Downloads app");
			Process p;
			String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.documentsui/com.android.documentsui.files.FilesActivity"};
			    p = new ProcessBuilder(p1).start();
			    log.info("Trying to launch the app");
			Thread.sleep(6000);
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
				
				log.info("@@@@@@@@@@@@ Saved image 1 is displayed in Downloads @@@@@@@@@@@");
				//Deleting the image from downloads
				log.info("Double tapping on the image");
				AndroidElement element = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage")));
				driver.tap(1, element, 500);
				log.info("Deleting the image");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
					log.info("############ Appium.jpg is successfully deleted ####################");
				}else{
					throw new Exception("((((((((((((((((((((( Image 1 is not deleted )))))))))))))))))))))");
				}
				
				
			}else{
				throw new Exception("%%%%%%%%%% Image 1 is not saved %%%%%%%%%%%%%");
			}
	if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AppiumNewImage"))){
				
				log.info("@@@@@@@@@@@@ Saved image 2 is displayed in Downloads @@@@@@@@@@@");
				//Deleting the image from downloads
				log.info("Double tapping on the image");
				AndroidElement element = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AppiumNewImage")));
				driver.tap(1, element, 500);
				log.info("Deleting the image");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AppiumNewImage"))){
					log.info("############ AppiumNew.png is successfully deleted ####################");
				}else{
					throw new Exception("((((((((((((((((((((( Image 2 is not deleted )))))))))))))))))))))");
				}
			}else{
				throw new Exception("%%%%%%%%%% Image 2 is not saved %%%%%%%%%%%%%");
			}

	}
	/**
	* @TestCase Verify Photo viewer  Print option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerPrintOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Print option******************");
//		   launchGmailApp(driver);
//		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage") );
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerPrintOption"));
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerPrintPreview"))){
				
				log.info(" ^^^^^^^^^^^^^^^^ Print preview is displayed ^^^^^^^^^^^^^^^^^^^^^^^^^");
				
			}else {
				throw new Exception("!!!!!!!!!!!!!! Print  preview is not displayed !!!!!!!!!!!!!!!");
			}
			navDrawer.navigateUntilHamburgerIsPresent(driver);

	}
	/**
	* @TestCase Verify Photo viewer  Share option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerShareOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify Share option******************");
		   launchGmailApp(driver);
//		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage") );
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerShareOption") );
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("GmailProductName"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GmailProductName"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AlwaysButton"));
			}
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
				log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with single attachment ^^^^^^^^^^^^^^^^^^^^ ");
			}else{
				throw new Exception(" !!!!!!!!!!! Share option is not working as expected !!!!!!!!!!!!!!");
			}

			driver.navigate().back();

	}

	/**
	* @TestCase Verify Photo viewer  Share All option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerShareAllOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify Share All option******************");
//		   CommonFunctions.tapUsingCoordinates(driver, 500, 700);
		   waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerShareAllOption") );
			Thread.sleep(1000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("GmailProductName"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GmailProductName"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AlwaysButton"));
			}		
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage"))){
				log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with 1st attachment ^^^^^^^^^^^^^^^^^^^^ ");
				if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("AppiumNewImage"))){
					log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with 2nd attachment ^^^^^^^^^^^^^^^^^^^^ ");
				}
				else{
					throw new Exception(" !!!!!!!!!!! ShareAll option is not working as expected: 2nd attachment missing !!!!!!!!!!!!!!");
				}

			}else{
				throw new Exception(" !!!!!!!!!!! ShareAll option is not working as expected: 1st attachment is missing !!!!!!!!!!!!!!");
			}

			driver.navigate().back();

	}
	/**
	* @TestCase Verify Save option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerInlineImageSaveOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Inline image Save option******************");
		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ToFieldAttachments"));
		   CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
		   Thread.sleep(1000);
			commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(800);
			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
			log.info("Double tapping on the image");
			AndroidElement element = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImage")));
			driver.tap(1, element, 500);
			waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
			commonFunction.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver,commonElements.get("EventSaveButton"));
			log.info("Launching Downloads app");
			Process p;
			String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.documentsui/com.android.documentsui.files.FilesActivity"};
			    p = new ProcessBuilder(p1).start();
			    log.info("Trying to launch the app");
			Thread.sleep(6000);
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
				
				log.info("@@@@@@@@@@@@ Saved image is displayed in Downloads @@@@@@@@@@@");
				//Deleting the image from downloads
				log.info("Double tapping on the image");
				AndroidElement element1 = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp")));
				driver.tap(1, element1, 600);
				log.info("Deleting the image");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
					log.info("############ Appium.jpg is successfully deleted ####################");
				}else{
					throw new Exception("((((((((((((((((((((( Image is not deleted )))))))))))))))))))))");
				}
				
				
			}else{
				throw new Exception("%%%%%%%%%% Image is not saved %%%%%%%%%%%%%");
			}


	}

	/**
	* @TestCase Verify Save All option for inline image
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerInlineSaveAllOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Save All option******************");
		   launchGmailApp(driver);
		   CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
		   Thread.sleep(1000);
			commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(800);
			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
			log.info("Double tapping on the image");
			AndroidElement element4 = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImage")));
			driver.tap(1, element4, 500);
			waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
			commonFunction.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
//			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
//			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImage") );
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerSaveAllOption"));
			
			log.info("Launching Downloads app");
			Process p;
			String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.documentsui/com.android.documentsui.files.FilesActivity"};
			    p = new ProcessBuilder(p1).start();
			    log.info("Trying to launch the app");
			Thread.sleep(6000);
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
				
				log.info("@@@@@@@@@@@@ Saved image 1 is displayed in Downloads @@@@@@@@@@@");
				//Deleting the image from downloads
				log.info("Double tapping on the image");
				AndroidElement element1 = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp")));
				driver.tap(1, element1, 500);
				log.info("Deleting the image");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
					log.info("############ Appium.jpg is successfully deleted ####################");
				}else{
					throw new Exception("((((((((((((((((((((( Image 1 is not deleted )))))))))))))))))))))");
				}
				
				
			}else{
				throw new Exception("%%%%%%%%%% Image 1 is not saved %%%%%%%%%%%%%");
			}
	if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PythonImageDownloadApp"))){
				
				log.info("@@@@@@@@@@@@ Saved image 2 is displayed in Downloads @@@@@@@@@@@");
				//Deleting the image from downloads
				log.info("Double tapping on the image");
				AndroidElement element2 = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PythonImageDownloadApp")));
				driver.tap(1, element2, 500);
				log.info("Deleting the image");
				CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("DownloadsDeleteIcon"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				if(CommonFunctions.isElementByXpathNotDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PythonImageDownloadApp"))){
					log.info("############ AppiumNew.png is successfully deleted ####################");
				}else{
					throw new Exception("((((((((((((((((((((( Image 2 is not deleted )))))))))))))))))))))");
				}
			}else{
				throw new Exception("%%%%%%%%%% Image 2 is not saved %%%%%%%%%%%%%");
			}

	}

	/**
	* @TestCase Verify Photo viewer  Print option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerInlinePrintOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify photo viewer Print option******************");

			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerPrintOption"));
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerPrintPreview"))){
				
				log.info(" ^^^^^^^^^^^^^^^^ Print preview is displayed ^^^^^^^^^^^^^^^^^^^^^^^^^");
				
			}else {
				throw new Exception("!!!!!!!!!!!!!! Print  preview is not displayed !!!!!!!!!!!!!!!");
			}
			navDrawer.navigateUntilHamburgerIsPresent(driver);

	}
	/**
	* @TestCase Verify Photo viewer  Share option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerInlineShareOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify Share option******************");
		   launchGmailApp(driver);
//		   userAccount.switchMailAccount(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("etouchTestTwoAccount"));
			Thread.sleep(4000);
			pullToReferesh(driver);
//			openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("photoViewerSubject"));
//			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("ExpectedAppiumImage") );
			CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
			   Thread.sleep(1000);
				commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"), composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				Thread.sleep(800);
				openMail(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerImageInlineSubject"));
				log.info("Double tapping on the image");
				AndroidElement element6 = (AndroidElement) driver.findElement(By.xpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImage")));
				driver.tap(1, element6, 500);
				waitForElementToBePresentByXpath(composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
				commonFunction.searchAndClickByXpath(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("InlineImageViewOption"));
			
			
			waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerShareOption") );
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("GmailProductName"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GmailProductName"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AlwaysButton"));
			}
			
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
				log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with single attachment ^^^^^^^^^^^^^^^^^^^^ ");
			}else{
				throw new Exception(" !!!!!!!!!!! Share option is not working as expected !!!!!!!!!!!!!!");
			}

			driver.navigate().back();

	}

	/**
	* @TestCase Verify Photo viewer  Share All option 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void photoviewerInlineShareAllOption(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
		Map<String, String>composeMailWithMultipleAttachmentsAndAnsertedDriveChips = commonPage.getTestData("composeMailWithMultipleAttachmentsAndAnsertedDriveChips");

		   log.info("******************* Start Verify Share All option******************");
//		   CommonFunctions.tapUsingCoordinates(driver, 500, 700);

		   
		   waitForElementToBePresentByXpath(commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver,composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PhotoViewerShareAllOption") );
			Thread.sleep(1000);
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("GmailProductName"))!=0){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GmailProductName"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AlwaysButton"));
			}		
			if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("SeleniumImageDownloadApp"))){
				log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with 1st attachment ^^^^^^^^^^^^^^^^^^^^ ");
				if(CommonFunctions.isElementByXpathDisplayed(driver, composeMailWithMultipleAttachmentsAndAnsertedDriveChips.get("PythonImageDownloadApp"))){
					log.info(" ^^^^^^^^^^^^^^^^^^ Compose screen is displayed with 2nd attachment ^^^^^^^^^^^^^^^^^^^^ ");
				}
				else{
					throw new Exception(" !!!!!!!!!!! ShareAll option is not working as expected: 2nd attachment missing !!!!!!!!!!!!!!");
				}

			}else{
				throw new Exception(" !!!!!!!!!!! ShareAll option is not working as expected: 1st attachment is missing !!!!!!!!!!!!!!");
			}

			driver.navigate().back();

	}
	/**
	* @TestCase Exploratory:  Verify Mark Read/Unread & Add star from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyMarkReadAddStarCVview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Add star and removing star from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailCVMoreOptions"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddStarMoreOpt"));
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailCVMoreOptions"));
		Thread.sleep(2000);

		if(CommonFunctions.isElementByXpathDisplayed(driver, verifyVariousSearchOperationsData.get("MoreOptionRemoveStar"))){
			log.info("****************** Added star *************");
			}else{
				throw new Exception("!!!!!!!!! Add star didn't work !!!!!!!!!!!");
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
		log.info("Mark Read and Unread mail");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkUnread"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		
		if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MarkRead"))){
			log.info("***********  Mail is Marked as Unread ***************");
		}else{
			throw  new Exception("!!!!!!!!! Mail is not marked as Unread !!!!!!!!!!!!!!");
		}		
		
		navigateUntilHamburgerIsPresent(driver);
}
	/**
	* @TestCase Exploratory:  Verify Archive from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndArchiveTL(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Archive from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
        CommonFunctions.scrollsToTitle(driver, "All mail");
/*//        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
//        		driver.findElementByXPath("//android.view.View[@index='1']/android.view.View[@index='0']").getAttribute("content-desc");     		
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("SearchTLGmailSubject"))){
        	log.info(" *************** Mail is archived and is in All mail ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in All mail !!!!!!!!!! ");
        	}
        	driver.navigate().back();*/
        if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InboxMail").replace("MailSubject",verifyVariousSearchOperationsData.get("SearchTLGmailSubject")))){
        	log.info("*********** Mail is present in All mail folder  ************");
        }
        else{
        	throw new Exception("!!!!!!!! Mail is not present in the All mail folder !!!!!!!!!!!! ");
        }
        	log.info("Verifying moveTo inbox from TL view");
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MovetoInbox"));
        	driver. navigate().back();
		
	}
	/**
	* @TestCase Exploratory:  Verify Archive from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndArchiveCV(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Archive from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
        CommonFunctions.scrollsToTitle(driver, "All mail");
        Thread.sleep(2000);
        searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
        Thread.sleep(2000);
        /*if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("InboxMail").replace("MailSubject",verifyVariousSearchOperationsData.get("SearchTLGmailSubject")))){
        	log.info("*********** Mail is present in All mail folder  ************");
        }
        else{
        	throw new Exception("!!!!!!!! Mail is not present in the All mail folder !!!!!!!!!!!! ");
        }*/
        	log.info("Verifying moveTo inbox from CV view");
        	openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MovetoInbox"));
        	driver.navigate().back();
        	driver.navigate().back();
		
	}
	
	/**
	* @TestCase Exploratory:  Verify mute from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndMuteTL(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Muted from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("ClickMute"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
        CommonFunctions.scrollsToTitle(driver, "All mail");
        searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
        Thread.sleep(1000);
        CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("SearchTLGmailSubject"))){
        	log.info(" *************** Mail is Muted and is in All mail ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in All mail !!!!!!!!!! ");
        	}
        	driver.navigate().back();
        	log.info("Verifying moveTo inbox from TL view");
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MovetoInbox"));
        	driver.navigate().back();
        	Thread.sleep(5000);
	}		
	/**
	* @TestCase Exploratory:  Verify mute from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndMuteCV(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Muted from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("ClickMute"));
		navigateUntilHamburgerIsPresent(driver);
		navDrawer.openMenuDrawer(driver);
        CommonFunctions.scrollsToTitle(driver, "All mail");
        Thread.sleep(2000);
        searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
        Thread.sleep(1500);
        openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
        
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
    	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("MutedText"))){
    	log.info(" *************** Mail is Muted and is in All mail ************");
    	}else{
    		throw  new Exception("!!!!!! Mail is not muted !!!!!!!!!! ");
    	}
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MovetoInbox"));
        	driver.navigate().back();
        	Thread.sleep(5000);
	}		
	/**
	* @TestCase Exploratory:  Verify Change Label from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndChangeLabelTL(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Change Label from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClickChangeLabel"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ChangeLabelMtvOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OkButton"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
		CommonFunctions.scrollsToTitle(driver, "mtv1");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("MTV1LabelText"))){
        	log.info(" *************** Mail is in MTV1 label ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in MTV1 label !!!!!!!!!! ");
        	}
        	driver.navigate().back();
        	log.info("Verifying moveTo inbox from TL view");
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	Thread.sleep(2000);
        	if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
        		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
        	}     	else{
        		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
        	        	
        	}
        	driver.navigate().back();
        	Thread.sleep(5000);	
	}
	/**
	* @TestCase Exploratory:  Verify Change Label from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndChangeLabelCV(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Change Label from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClickChangeLabel"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ChangeLabelMtvOption"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OkButton"));
		/*if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}*/
		navigateUntilHamburgerIsPresent(driver);
		navDrawer.openMenuDrawer(driver);
		CommonFunctions.scrollsToTitle(driver, "mtv1");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );

        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("MTV1LabelText"))){
        	log.info(" *************** Mail is in MTV1 label ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in MTV1 label !!!!!!!!!! ");
        	}
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	Thread.sleep(2000);
        	if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
        		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
        	}     	else{
        		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
        	        	
        	}
        	driver.navigate().back();
        	Thread.sleep(5000);	
	}
	/**
	* @TestCase Exploratory:  Verify Report Spam from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndReportSpamTL(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Change Label from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReportSpamInMoreOption"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
		CommonFunctions.scrollsToTitle(driver, "Spam");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FirstMailIndex"));
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("SearchTLGmailSubject"))){
        	log.info(" *************** Mail is in Spam folder ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in Spam label !!!!!!!!!! ");
        	}
        	driver.navigate().back();
        	log.info("Verifying moveTo inbox from TL view");
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	Thread.sleep(2000);
if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
}     	else{
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
        	
}
        	driver.navigate().back();
        	Thread.sleep(5000);	
	}
	/**
	* @TestCase Exploratory:  Verify Report Spam from CV 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifySearchAndReportSpamCV(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Change Label from CV view");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ReportSpamInMoreOption"));
		if(commonFunction.isElementByXpathNotDisplayed(driver, commonElements.get("HamburgerMenu"))){
			driver.navigate().back();
		}
		navDrawer.openMenuDrawer(driver);
		CommonFunctions.scrollsToTitle(driver, "Spam");
		openMail(driver,verifyVariousSearchOperationsData.get("SearchCVGmailSubject") );
        String mailInTrash = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("FirstMailIndexCV"));
        	if(mailInTrash.contains(verifyVariousSearchOperationsData.get("SpamLabelText"))){
        	log.info(" *************** Mail is in Spam folder ************");
        	}else{
        		throw  new Exception("!!!!!! Mail is not displayed in Spam label !!!!!!!!!! ");
        	}
        	
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
        	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
        	Thread.sleep(2000);
if(CommonFunctions.getSizeOfElements(driver,commonElements.get("InboxOption"))!=0){ 
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InboxOption"));
}     	else{
	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PrimaryInboxOption"));
        	
}
        	driver.navigate().back();
        	Thread.sleep(5000);	
	}
	/**
	* @TestCase Exploratory:  Verify Mark Read/Unread & Add star from TL 
	* @Pre-condition :
	* GMAT 
	* @throws: Exception
	* @author batchi
	*/
	@Test(groups = {"P0"}, enabled = true, priority = 6)
	public void verifyMarkReadAddStarTLview(AppiumDriver driver) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		log.info("Add star and removing star from TL view");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SelectMailImage"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddStarMoreOpt"));
		Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
		Thread.sleep(2000);

		if(CommonFunctions.isElementByXpathDisplayed(driver, verifyVariousSearchOperationsData.get("MoreOptionRemoveStar"))){
			log.info("****************** Added star *************");
			}else{
				throw new Exception("!!!!!!!!! Add star didn't work !!!!!!!!!!!");
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveStarMoreOpt"));
		log.info("Mark Read and Unread mail");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MarkRead"));
		
		if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("MarkUnread"))){
			log.info("***********  Mail is Marked as read ***************");
		}else{
			throw  new Exception("!!!!!!!!! Mail is not marked as read !!!!!!!!!!!!!!");
		}		
		
		navigateUntilHamburgerIsPresent(driver);
}
	public void undoRestoresAutoAdvanceOlder(AppiumDriver driver, String operation, String action) throws Exception{
		Map < String, String > undoDeleteData = commonPage.getTestData("VerifyUndo");
		navDrawer.navigateSettingsPage(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettings"));
		CommonFunctions.scrollsToTitle(driver, "Auto-advance");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettingAutoAdvanceOlderOption"));
		navigateUntilHamburgerIsPresent(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ThirdMail123"));
		String subj1= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		log.info("1st mail's subject: "+subj1);
		navigateBack(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SecondMail123"));
		String subj2= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		log.info("2nd mail's subject is: "+subj2);
		CommonFunctions.searchAndClickByXpath(driver, operation);
		String subj3= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		log.info("After performing operations "+subj3);
		if(subj3.equals(subj1)){
			if(!subj3.equals(subj2)){
			log.info("***** Mail is "+action+ "  and navigated to the older mail ******");
		}else{
			throw new Exception("!!!!!!!!!! Mail is not "+action+ " subj2 is displayed !!!!!!!!!!");
		}
			}else{
			throw new Exception("!!!!!! Mail is not "+action+ " subj1 is displayed !!!!!!!!");
		}
		waitForElementToBePresentByXpath(commonElements.get("SwipeUndo"));
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("SwipeUndo"));
		String subj4= CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLinewithID"));
		log.info("After performing Undo "+subj4);
		if(subj2.equals(subj4)){
			log.info("********** Mail is restored ***********");
			}else{
				throw new Exception("!!!!!!!! Mail is not restored !!!!!!!!!");
			}
	navigateUntilHamburgerIsPresent(driver);
	}

}
