package com.etouch.gmail.commonPages;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.testng.Assert;

import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;

public class Calendar extends CommonPage {

	/** The log. */
	private Log log = LogUtil.getLog(Calendar.class);

	/**
	 * Instantiates an InboxFunctions.
	 */
	public Calendar() {

	}

	/**
	 * Method to verify Calendar Details
	 * 
	 * @param CalendarMessage
	 * @param DonloadCalendarText
	 * @throws Exception
	 */
	public void verifyCalendarDetails(AppiumDriver driver,String expectedCalendarMessage, String expectedDonloadCalendarText) throws Exception {
		try {
			log.info("Tapping on event time");
			CommonFunctions.searchAndClickById(driver, commonElements.get("EventTimeID"));
			
			String actualCalendarMessage =CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("CalendarMessageID"));
			log.info("actualCalendarMessage: "+actualCalendarMessage);
			Assert.assertEquals(actualCalendarMessage,expectedCalendarMessage, "Calendar Message: ");
           
			String actualDonloadCalendarText =CommonFunctions.searchAndGetTextOnElementById(driver, commonElements.get("DownloadCalendarID"));
		    log.debug("actualDonloadCalendarText: "+actualDonloadCalendarText);
		    Assert.assertEquals(actualDonloadCalendarText, expectedDonloadCalendarText, "Download Calendar: ");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("verifyCalendarDetails failed due to :" + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Method to verify Calendar Details
	 * 
	 * @param CalendarMessage
	 * @param DonloadCalendarText
	 * @throws Exception
	 */
	public void verifyCalendarDetailsFlight(AppiumDriver driver,String NoThanks, String GetCalendar) throws Exception {
		try {
			
			commonPage.waitForElementToBePresentByXpath(NoThanks);
			log.info("No Thanks button is displayed");
			commonPage.waitForElementToBePresentByXpath(GetCalendar);
			log.info("Get calendar option is displayed");
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("verifyCalendarDetails failed due to :" + e.getLocalizedMessage());
		}
	}
	
	public void verifyGetCalendar(AppiumDriver driver,String NoThanks, String GetCalendar) throws Exception {
		try {
			
			commonPage.waitForElementToBePresentByXpath(NoThanks);
			log.info("No Thanks button is displayed");
			commonPage.waitForElementToBePresentByXpath(GetCalendar);
			log.info("Get calendar option is displayed");
			CommonFunctions.searchAndClickByXpath(driver, GetCalendar);
			Thread.sleep(1200);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PlayStoreInstallButton"));
			Thread.sleep(10000);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("PlayStoreOpenButton"));
			driver.navigate().back();
			launchGmailApp(driver);
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("verifyCalendarDetails failed due to :" + e.getLocalizedMessage());
		}
	}
	public void updatingTimeWithFewMins(AppiumDriver driver,int addMins)throws Exception{
		Map<String, String>  composeAndSendMailData = commonPage.getTestData("verifyComposeAndSendMail");
		try {
			int subHour = 1, MinEnd=56 ;
			int editHour=0;
			String  HourEnd12 ="12";
			String hour = CommonFunctions.searchAndGetTextOnElementByXpath(driver,composeAndSendMailData.get("InputHour") );
			int hour1 = Integer.parseInt(hour);
			log.info("hour:" +hour);
			log.info("Sub hour:" +subHour);
			if(hour1== subHour){
				CommonFunctions.searchAndSendKeysByXpath(driver,composeAndSendMailData.get("InputHour") ,HourEnd12 );
			}
			
			else{
				if(hour.equals(HourEnd12) ){
					CommonFunctions.searchAndClickByXpath(driver,composeAndSendMailData.get("AmPmToggle") );
					CommonFunctions.searchAndClickByXpath(driver,composeAndSendMailData.get("AmOption") );	
				}
				
				editHour = hour1-subHour;
				String editedHour = String.format("%d", editHour);
			log.info("Hour field subtracted by an hour: "+editHour);		
			CommonFunctions.searchAndSendKeysByXpath(driver,composeAndSendMailData.get("InputHour"), editedHour);
			log.info("Updated the hour field");
			}
			
			String min = CommonFunctions.searchAndGetTextOnElementByXpath(driver, composeAndSendMailData.get("InputMin")) ;
			
			if(min.equals(MinEnd)){
				CommonFunctions.searchAndSendKeysByXpath(driver, composeAndSendMailData.get("InputMin"), "00");
				int increasedHour  = editHour+subHour;
				String incHour = String.format("%d", increasedHour);
				CommonFunctions.searchAndSendKeysByXpath(driver,composeAndSendMailData.get("InputHour"),incHour );
			}
			else{
			int min1 = Integer.parseInt(min);
			int editMin = min1+addMins;
			String editedMin = String.format("%d", editMin);
			log.info("Min field added by "+addMins +" mins: "+editMin);
			CommonFunctions.searchAndSendKeysByXpath(driver,composeAndSendMailData.get("InputMin"), editedMin);
			log.info("Updated the Min field");
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("getting Current Time failed due to :" + e.getLocalizedMessage());
		}
		}

	
}
