package com.etouch.gmail.commonPages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.CommonFunctions;
import com.etouch.gmail.common.CommonPage;
import com.etouch.gmail.common.MailManager;
import com.etouch.taf.util.LogUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

public class UserAccount extends InboxFunctions {
	static String[] val;
	String path = System.getProperty("user.dir");

	/** The log. */
	private static Log log = LogUtil.getLog(UserAccount.class);

	/**
	 * Method to add New Gmail Account
	 * 
	 * @param GmailID
	 * @param password
	 * @throws Exception
	 */
	public void addNewGmailAccount(String GmailID, String password) throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountGoogleOption"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountUsernameField"),
					GmailID);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountUsernameNextButton"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountPasswordField"),
					password);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountPasswordNextButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountAccpetButton"));
			waitForElementToBePresentByXpath(commonElements.get("ExpandAccountList"));
			navigateBack(driver);
			Assert.assertTrue(verifyGmailAccountIsPresent(GmailID), " Failed to find " + GmailID + " in Account List");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to add Gmail ID : " + GmailID + " Due to :" + e.getLocalizedMessage());
		}
	}

	public void changeToGigAccount(AppiumDriver driver) throws Exception {
		try {
			Map<String, String> addGigAccount = commonPage.getTestData("GIGAccount");
			if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("GigText")) != 0) {
				log.info("Account is already enabled with GIG");
			} else {
				openMenuDrawer(driver);
				Thread.sleep(2000);
				scrollDown(1);
				Thread.sleep(2000);
				if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("DataLayer")) != 0) {
					log.info("Data Layer option is found");
				} else {
					scrollDown(1);
					log.info("Scrolling down");
				}
				CommonFunctions.searchAndClickByXpath(driver, addGigAccount.get("DataLayer"));
				log.info("Clicked on Data Layer option");

				commonPage.waitForElementToBePresentByXpath(addGigAccount.get("SelectDataLayer"));
				try {
					if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("GIGButton")) != 0) {
						CommonFunctions.searchAndClickByXpath(driver, addGigAccount.get("GIGButton"));
					} else {
						log.info("GIG button is not found on alert/Check the app is already enabled with GIG");
						throw new Exception("GIG button is not shown on alert");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
				// if(CommonFunctions.getSizeOfElements(driver,
				// addGigAccount.get("GigText"))!=0){
				if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("bSync")) == 0) {
					log.info("b-sync not found");
					log.info("App is not yet migrated to b-sync");
					Thread.sleep(4000);
					pullToReferesh(driver);
					Thread.sleep(10000);
					pullToReferesh(driver);
					driver.closeApp();
					launchGmailApp(driver);
					commonPage.waitForElementToBePresentByXpath(addGigAccount.get("bSync"));
				} else {
					log.info("b-sync is found");
					pullToReferesh(driver);
					Thread.sleep(12000);
					pullToReferesh(driver);
					Thread.sleep(6000);
					driver.closeApp();
					launchGmailApp(driver);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
				}
				if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("m-ing")) == 0) {
					log.info("App is not yet migrated to m-ing");
					pullToReferesh(driver);
					Thread.sleep(10000);
					pullToReferesh(driver);
					Thread.sleep(5000);
					driver.closeApp();
					launchGmailApp(driver);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
					commonPage.waitForElementToBePresentByXpath(addGigAccount.get("m-ing"));
				} else {
					commonPage.waitForElementToBePresentByXpath(addGigAccount.get("m-ing"));
					log.info("App is migrated to m-ing");
					pullToReferesh(driver);
					Thread.sleep(10000);
					pullToReferesh(driver);
					driver.closeApp();
					launchGmailApp(driver);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
				}

				if (CommonFunctions.getSizeOfElements(driver, addGigAccount.get("GigText")) == 0) {
					log.info("App is not yet migrated to gig");
					pullToReferesh(driver);
					Thread.sleep(10000);
					driver.closeApp();
					launchGmailApp(driver);
					commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
					commonPage.waitForElementToBePresentByXpath(addGigAccount.get("GigText"));
				} else {
					log.info("App is migrated to GIG");
				}
			}
			// }
		} catch (Exception e) {

		}
	}

	public void clearNotification(AppiumDriver driver) throws Exception {
		try {
			((AndroidDriver) driver).openNotifications();
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ClearAllNotification")) != 0) {
				log.info("Clear notification is found");
			} else {
				driver.swipe(0, 1274, 0, 937, 1000);
			}
			Thread.sleep(3000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ClearAllNotification")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClearAllNotification"));
			} else {
				String[] closeNotifications = new String[] { "adb", "shell", "sevice", "call statusbar 2" };
				Process p;
				ProcessBuilder pb = new ProcessBuilder(closeNotifications);
				p = pb.start();
				log.info("Closed status bar");
			}
			log.info("Clicked on clear all button");
		} catch (Exception e) {
			throw new Exception("Unable to clear notifications " + e.getLocalizedMessage());
		}

	}

	public void switchMailAccount(AppiumDriver driver, String emailID) throws Exception {
		log.info("UA.switchMailAccountNew starts..");
		try{
			driver = map.get(Thread.currentThread().getId());
			if(BaseTest.isAccountSwitcherExist){
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
				Thread.sleep(1000);
				log.info("UA Scrolling up to find current account name. emailID:"+emailID);

				//goToManagerYourGoogleAccount(driver);
				
				Thread.sleep(1000);
				WebElement acctSelected;
				try {
					acctSelected = driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.TextView[contains(@text,'" + emailID.trim() +"')]"
							+ "| //android.view.ViewGroup//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));

				} catch (Exception e) {
					 acctSelected = driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name')]")); // Venkat
				}

				String selectedAccount = acctSelected.getText();
				log.info("UA Account selected input:"+emailID +", selectedAccount:"+ selectedAccount);

				//String pathToClick = "//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[contains(@text,'" + emailID + "')] | //android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[@text='" + emailID.trim() + "']";
				String pathToClick = "//android.widget.TextView[contains(@text,'" + emailID + "')] | //android.widget.TextView[@text='" + emailID.trim() + "']";
				if (emailID.trim().equalsIgnoreCase(selectedAccount.trim())) {
					log.info("UA Account is already selected");

					driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView")).click();
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
					log.info("Selecting the previous account again emailID:::"+emailID);

					byte cnt = -1;
					while(cnt++ <= 4){
						log.info("Selecting the previous account again [cnt]:"+cnt);
						try{
							Thread.sleep(1000);
								driver.findElement(By.xpath(pathToClick)).click();
							break;
						} catch (Exception e1) {
							Thread.sleep(1000);
							scrollDown(1);
							log.error("Unable to click on emailID. So scrolling down to::"+emailID);
							if( cnt >= 4){
								throw new Exception("Unable to click on given MailId::"+emailID);
							}
							continue;
						}
					}

				}else{
					String xPath = commonElements.get("AccountToSwitch").replace("Email", emailID);
					boolean isElementFound = scrollToFindElement(driver, emailID,"down",xPath);
					if(!isElementFound){
						isElementFound = scrollToFindElement(driver, emailID,"up", xPath);						
					}
					if(!isElementFound){
						throw new Exception("Mail id :"+emailID+" not configured. Please check..");
					}
				}
			}else{
				//goToAccountsListPage(driver);
				openMenuDrawerNew(driver);
				String accountListButton = commonElements.get("ExpandAccountList");
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				scrollUp(1);
				int count = 0;
				while(count <=2){
					count ++;
					try {
						WebElement accountElement = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_address']"));
						String selectedAccount = accountElement.getAttribute("text");
						//CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[@resource-id='com.google.android.gm:id/account_address']");
						if(emailID.equalsIgnoreCase(selectedAccount.trim())){
							driver.navigate().back();
							Thread.sleep(500);
							pullToReferesh(driver);
							waitForHambergerMenuToBePresent();
							break;
						}else{
							CommonFunctions.searchAndClickByXpath(driver, accountListButton);
							String xPath = commonElements.get("AccountToSwitch").replace("Email", emailID);
							boolean isElementFound = scrollToFindElement(driver, emailID,"down", xPath);
							if(!isElementFound){
								isElementFound = scrollToFindElement(driver, emailID,"up", xPath);						
							}else{
								break;
							}
							if(!isElementFound){
								throw new Exception("Mail id :"+emailID+" not configured. Please check..");
							}
						}
					} catch (Exception e) {
						log.error("Exception in findnig account name:"+e.getMessage());
						//throw new Exception("Exception in findnig account name:"+e.getMessage());
					}
				}
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to switch to account :" + emailID + " Due to :" + e.getLocalizedMessage());
		}
		log.info("UA.switchMailAccountNew End..");
	}

	public boolean scrollToFindElement(AppiumDriver driver, String emailID, String whichDirection, String xPath) throws Exception {
		log.info("scrollToFindElement whichDirection:"+whichDirection+", emailID:"+emailID+". starts");
		int count = 0;
		boolean isElementFound = false;
		while(count <=2){
			count ++;
			try{
				//System.out.println("ASwitcher scrollToFindElement looking for element :"+commonElements.get("AccountToSwitch")+" After Replace :"+commonElements.get("AccountToSwitch").replace("Email", emailID));
				Thread.sleep(1000);
				//element = driver.findElement(By.xpath(commonElements.get("AccountToSwitch").replace("Email", emailID)));
				WebElement element = driver.findElement(By.xpath(xPath));
					element.click();
					//CommonFunctions.searchAndClickByXpath(driver,xPath);
					System.out.println("ASwitcher scrollToFindElement clicked");
					isElementFound = true;
					break;
			}catch(Exception e){
				if("down".equals(whichDirection)){
					System.out.println("ASwitcher scrollToFindElement scrolling down");
					scrollDown(1);
				}else if("up".equals(whichDirection)){
					System.out.println("ASwitcher scrollToFindElement scrolling up");
					scrollUp(1);
				}
				Thread.sleep(1000);
			}
		}
		log.info("scrollToFindElement end. isElementFound:"+isElementFound);
		return isElementFound;
	}

	private void goToManagerYourGoogleAccount(AppiumDriver driver) throws Exception {
		int count = 0;
		while(count++ <= 2){
			if(CommonFunctions.getSizeOfElements(driver, "//android.support.design.chip.Chip[contains(@text,'Manage your Google Account')]") <= 0){
				CommonPage.scrollUp(1);
				continue;
			}
			break;
		}
		Thread.sleep(1000);
	}

	/**
	 * Method to switch email account
	 * 
	 * @param driver
	 * @param emailID
	 * @throws Exception
	 */
/*	
	public void switchMailAccount(AppiumDriver driver, String emailID) throws Exception {
		driver = map.get(Thread.currentThread().getId());
		String hamburgerButton = commonElements.get("HamburgerMenu");
		String accountListButton = commonElements.get("ExpandAccountList");
		String accountListXpath = commonElements.get("AccountListCommonXpath");
		String accountSelectedNow = commonElements.get("AccountSelected");
		String accountName = commonElements.get("AccountNameSelected");

		try {
			Thread.sleep(1200);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountIconNew")) != 0) {
				Thread.sleep(2000);

				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
				Thread.sleep(2500);
				//CommonPage.scrollUp(1); // added by Venkat on 3/12/2018
				// added by Venkat on 22-Nov-2018 : start
				int count = 0;
				log.info("UA Scrolling up to find current account name. emailID:"+emailID);
//				if(null != emailID && emailID.trim().endsWith("gmail.com")){
					while(count++ <= 2){
						if(CommonFunctions.getSizeOfElements(driver, "//android.support.design.chip.Chip[contains(@text,'Manage your Google Account')]") <= 0){
							CommonPage.scrollUp(1);
							//CommonFunctions.scrollsToTitle(driver, emailID);
							continue;
						}
						break;
					}
//				}
					Thread.sleep(1000);
				
				// added by Venkat on 22-Nov-2018: end

				Thread.sleep(1000);
				WebElement acctSelected;
				try {
					acctSelected = driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.TextView[contains(@text,'" + emailID.trim() +"')]"
							+ "| //android.view.ViewGroup//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));

				} catch (Exception e) {
					 //acctSelected = driver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name') and @index='0']")); // Venkat
					 acctSelected = driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name')]")); // Venkat
				}
				String selectedAccount = acctSelected.getText();
				log.info("UA Account selected input:"+emailID +", selectedAccount:"+ selectedAccount);
				if (emailID.trim().equalsIgnoreCase(selectedAccount.trim())) {
					log.info("UA Account is already selected");
					//driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView//android.widget.TextView[@index='2']")).click();
					driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView")).click();
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
					log.info("Selecting the previous account again emailID:::"+emailID);
					byte cnt = -1;
					String pathToClick = "//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[contains(@text,'" + emailID + "')] | //android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[@text='" + emailID.trim() + "']";
					while(true){
						try{
							Thread.sleep(2000);
							//driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + emailID.trim() + "') and @index='1']")).click();
							//driver.findElement(By.xpath("//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[contains(@text,'" + emailID.trim() + "')")).click();
							//CommonFunctions.searchAndClickByXpath(driver, "//android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[contains(@text,'" + emailID + "')] | //android.support.v7.widget.RecyclerView[@resource-id='com.google.android.gm:id/accounts_list']//android.widget.TextView[@text='" + emailID.trim() + "']");
							if(cnt <= -1){
								driver.findElement(By.xpath(pathToClick)).click();
							}else{
								CommonFunctions.searchAndClickByXpath(driver, pathToClick);
							}
							break;
						} catch (Exception e1) {
							cnt++;
							Thread.sleep(1000);
							scrollDown(1);
							//CommonFunctions.scrollsToTitle(driver, emailID);
							log.error("Unable to click on emailID. So scrolling down to::"+emailID);
							if( cnt++ >= 3){
								throw new Exception("Unable to click on given MailId::"+emailID);
							}
							continue;
						}
					}

					// Code written by Venkat on 15/11/2018: End
				} else {
					if (CommonFunctions.getSizeOfElements(driver,
							commonElements.get("AccountToSwitch").replace("Email", emailID)) != 0) {
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AccountToSwitch").replace("Email", emailID));

					} else {
						//WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']")); //// Commented by Venkat on 10/12/2018
						WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name'] | //android.widget.TextView[@resource-id='com.google.android.gm:id/account_display_name']")); //// added by Venkat on 10/12/2018
						org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
						int x = element.getLocation().x;
						int y = element.getLocation().y;
						log.info("UA.switchMailAccount X coordinates " + x + " Y coordinates " + y);
						// driver.swipe((windowSize.width) - 20, (y + 50), 80,
						// (y + 50), 1000);
						driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
						Thread.sleep(1300);
						scrollDown(1);
						if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAnotherAccount")) != 0) {
							log.info("UA.switchMailAccount Switcher expanded");
						} else {
							try {
								driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
								scrollDown(1);
							} catch (Exception e) {
								throw new Exception("Issue while expanding the switcher");
							}
						}
						log.info("UA.switchMailAccount Account is switching to " + emailID);
						Thread.sleep(800);
						try {
							CommonFunctions.searchAndClickByXpath(driver,
									commonElements.get("AccountToSwitch").replace("Email", emailID));
						} catch (Exception e) {
							scrollUp(1);
							CommonFunctions.searchAndClickByXpath(driver,
									commonElements.get("AccountToSwitch").replace("Email", emailID));
						}
					}

				}
			} else {

				openMenuDrawer(driver);
				log.info("UA.switchMailAccount Switch to Account -->" + emailID);
				Thread.sleep(1000);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				CommonFunctions.searchAndClickByXpath(driver, accountListButton);
				List<WebElement> acctSelected = driver.findElementsByXPath(accountSelectedNow);
				java.util.List<WebElement> accountList = driver.findElementsByXPath(accountListXpath);
				// accountList.add(accountSelectedNow);
				log.info("UA.switchMailAccount accountList.get(0):"+accountList.get(0).getText());
				log.info("UA.switchMailAccount acctSelected.get(0):"+acctSelected.get(0).getText());

				if (emailID.equalsIgnoreCase(accountList.get(0).getText())
						|| emailID.equalsIgnoreCase(acctSelected.get(0).getText())) {
					log.info("UA.switchMailAccount  Account " + emailID + " is Already Active");
					driver.navigate().back();
					Thread.sleep(500);
					pullToReferesh(driver);
					waitForHambergerMenuToBePresent();
				} else {
					log.info("UA.switchMailAccount  Account " + emailID + " is NOT Already Active.Switching account to " + emailID);
					for (int x = 1; x < accountList.size(); x++) {
						log.info(accountList.get(x).getText());

						if (emailID.equalsIgnoreCase(accountList.get(x).getText())) {
							log.info("UA.switchMailAccount  Selecting the account");
							accountList.get(x).click();
							Thread.sleep(2000);
							//CommonFunctions.searchAndClickByXpath(driver, hamburgerButton);
							log.info("UA.switchMailAccount Clicked on hamburger");
							Thread.sleep(1800);
							if (emailID.equalsIgnoreCase(accountList.get(0).getText())
									|| emailID.equalsIgnoreCase(acctSelected.get(0).getText())
									|| emailID.equalsIgnoreCase(accountList.get(1).getText())) {
								log.info("UA.switchMailAccount Account " + emailID + " is Selected");
								driver.navigate().back();
							} else {
								throw new Exception("Account is not selected when tapped on it");
							}
							waitForHambergerMenuToBePresent();
							Thread.sleep(700);
							pullToReferesh(driver);
							log.info("Successfully Switched the account to : " + emailID);
							break;
						} else {
							
							 * scrollDown(2); Thread.sleep(2000);
							 
							log.info("UA.switchMailAccount Selecting the account after scroll");
							CommonFunctions.scrollsToTitle(driver, emailID);
							// accountList.get(x).click();
							// Thread.sleep(2000);
							break;
						}
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to switch to account :" + emailID + " Due to :" + e.getLocalizedMessage());
		}

	}
*/
	public void openHelpAndFeedbackSection(AppiumDriver driver) throws Exception {
		try {
			/*
			 * Thread.sleep(1500); CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("HamburgerMenu"));
			 * if(CommonFunctions.getSizeOfElements(driver,
			 * commonElements.get("AvtarInMenuDrawer"))!=0){ Thread.sleep(1000);
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("ExpandAccountList")); }
			 */
			Thread.sleep(1000);
			/*
			 * scrollTo("Help & feedback");
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("HelpAndFeedBackOption"));
			 */
			CommonFunctions.scrollsToTitle(driver, "Help & feedback");
		} catch (Throwable e) {
			throw new Exception("Unable to open Help And Feedback Section Due to :" + e.getLocalizedMessage());
		}
	}

	public void verifyHelpDisplayed(AppiumDriver driver, String expText) throws Exception {
		Assert.assertEquals(CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("HelpSection")),
				expText, "Help Field not displayed");
		// CommonFunctions.searchAndClickByXpath(driver,
		// commonElements.get("HelpAndFeedbackCloseIcon"));
	}

	public void verifyAllFeedbackOptions(AppiumDriver driver) throws Exception {
		log.info("Feedback search ");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FeedbackSearch"));
		CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("FeedbackEnterSearch"), "reset");
		String[] EnterFeedbackSearch = new String[] { "adb", "shell", "input", "keyevent 66" };
		Process p;
		ProcessBuilder pb = new ProcessBuilder(EnterFeedbackSearch);
		p = pb.start();
		Thread.sleep(4000);
		log.info("Feedback search done");

		driver.navigate().back();

		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FeedbackSuggestion"));
		Thread.sleep(600);
		driver.navigate().back();
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		Thread.sleep(800);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("FeedbackVersionInfo"));
		driver.navigate().back();
		log.info("Tapped on feedback more options");
	}

	public void ClickToSendFeedback(AppiumDriver driver) throws Exception {
		try {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Sendfeedback"));
		} catch (Throwable e) {
			throw new Exception("Unable to click on SendFeedback :" + e.getLocalizedMessage());
		}
	}

	public static void verifyforEditingAndSentFeedback(AppiumDriver driver, String feedbackText) throws Exception {
		try {
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("EditText"), feedbackText);
			hideKeyboard();
			CommonFunctions.searchAndClickById(driver, commonElements.get("SendFeedbackButton"));
		} catch (Throwable e) {
			throw new Exception("Unable to verifyfor Editing And Sent Feedback Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify is Gmail WelCome Page Displayed
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean isGmailWelComePageDisplayed(AppiumDriver driver) throws Exception {
		Thread.sleep(6000);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("WelcomeMessage")) != 0) {
			log.info("In UA.isGmailWelComePageDisplayed Welcome page is displayed");
			return true;
		} else {
			log.info("In UA.isGmailWelComePageDisplayed Welcome page NOT displayed");
			return false;
		}

		// return CommonFunctions.isElementByIdDisplayed(driver,
		// commonElements.get("WelcomeMessage"));
	}

	/**
	 * Method to add New Account From Gmail HomePage
	 * 
	 * @throws Exception
	 */
	public void addNewAccountFromGmailHomePage(AppiumDriver driver) throws Exception {

		try {
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("WelcomePageSkipMessage")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("WelcomePageSkipMessage"));
			}
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddEmailAddress"));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to add New Gmail Account
	 * 
	 * @param gmailIDs
	 * @param password
	 * @param homePage
	 * @throws Exception
	 */

	public void clickOnAccountSwitcherIconNew(){
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		String accountSwitcherXpath = "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']";
		
		try{
			CommonFunctions.searchAndClickByXpath(driver,accountSwitcherXpath);
		}catch(Exception e){
			System.out.println(e);
		}
	}
	public Set<String> getListOfConfiguredMailIdsNew(boolean clickOnAccountSwitch){
		if(clickOnAccountSwitch){
			clickOnAccountSwitcherIconNew();
		}
		return getConfiguredMailIdsNew();
	}


	
	public Set<String> getConfiguredMailIdsNew(){
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		Set<String> configuredMailIdsList = new HashSet<>();
		try{
			int count = 0;
			while(count <= 2){
				if(CommonFunctions.getSizeOfElements(driver, "//android.support.design.chip.Chip[contains(@text,'Manage your Google Account')] | //android.widget.TextView[contains(@text,'General settings')]") <= 0){
					CommonPage.scrollUp(1);
					count ++;
					continue;
				}
				break;
			}
			count = 0;
			while(count <= 2){
				List<WebElement> emailIdsList = driver.findElements(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name'] | //android.widget.TextView[@resource-id='com.google.android.gm:id/title']"));
				
				for(WebElement e: emailIdsList){
					configuredMailIdsList.add(e.getAttribute("text"));
				}
				if(CommonFunctions.getSizeOfElements(driver, "//android.widget.TextView[contains(@text,'Manage accounts on this device')] | //android.widget.TextView[@text='Add another account'] | //android.widget.TextView[contains(@text,'Features')] | //android.widget.TextView[@text='Add account']") <= 0){
					CommonPage.scrollDown(1);
					Thread.sleep(600);
					count ++;
					continue;
				}
				break;
			}
		}catch(Exception e){
			System.out.println(e);
		}
		return configuredMailIdsList;
	}
	
	
	public void scrollAndClickNew(AndroidDriver driver, String visibleText) {
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+visibleText+"\").instance(0))").click();
	}
	
	public void navigateBackNTimesNew(AppiumDriver driver, int noOfTimes) throws InterruptedException{
		//AppiumDriver driver = map.get(Thread.currentThread().getId());
		for(int i=1; i<=noOfTimes; i++){
			Thread.sleep(1000);
			driver.navigate().back();
		}
	}
	
	public void addGmailAccount(AppiumDriver driver, List<String> mailIdsList, String password)throws Exception{
		CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[@text='Google']");
		boolean isAccountAddSuccess = false;
		int cnt = 0, emailLen = mailIdsList.size(); 
		for(String mailId: mailIdsList){
			cnt +=1;
			try {
				if(isAccountAddSuccess){
					Thread.sleep(3000);
					scrollAndClickNew((AndroidDriver)driver,"Add another account");
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver,"//android.widget.TextView[@text='Google']");
				}
				Thread.sleep(5000);
				//CommonFunctions.searchAndSendKeysByXpathNoClear(driver,commonElements.get("AddingGoogleAccountUsernameField"), mailId);
				CommonFunctions.searchAndSendKeysByXpathNoClear(driver,"//android.widget.EditText[@resource-id='identifierId']", mailId);
				Thread.sleep(1000);
				//CommonFunctions.searchAndClickByXpath(driver,commonElements.get("AddingGoogleAccountUsernameNextButton"));
				CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[@resource-id='identifierNext']");
				WebElement emailElement = null;
				boolean isWroingEmailEntered = false;
				try{
					Thread.sleep(4000);
					driver.findElement(By.xpath("//android.widget.Button[contains(@text,'Forgot email?')]"));
					isWroingEmailEntered = true;
					if(isWroingEmailEntered){
						log.error("Wrong/Already exists mailId:::"+mailId);
						emailElement = driver.findElement(By.xpath("//android.widget.EditText[@resource-id='identifierId']"));
						emailElement.clear();
						if(cnt == emailLen){
							navigateBackNTimesNew(driver,3);
							break;
						}else{
							continue;
						}
					}
				}catch(Exception e){
					log.info("Email doesn't exists mailId::::"+mailId);
				}
				boolean isPwdNotWrong = false;
				WebElement pwdWebElement = null;
				try{
					Thread.sleep(2000);
					CommonFunctions.searchAndSendKeysByXpathNoClear(driver,"//android.widget.EditText[@index='0']", password);
					isPwdNotWrong = true;
					Thread.sleep(2000);
					CommonFunctions.searchAndClickByXpath(driver,commonElements.get("AddingGoogleAccountPasswordNextButton"));
					if(isPwdNotWrong){
						driver.findElement(By.xpath("//android.widget.Button[@resource-id='forgotPassword']"));
						isPwdNotWrong = true;
						throw new Exception("Wrong Password Entered");
					}
				}catch(Exception e){
					log.error("Exception while Entering password for mailId:"+mailId);
					if(isPwdNotWrong){
						//e.printStackTrace();
						pwdWebElement = driver.findElement(By.xpath("//android.widget.EditText[@index='0']"));
						pwdWebElement.clear();
						navigateBackNTimesNew(driver,2);
						//emailElement = driver.findElement(By.xpath("//android.widget.EditText[@resource-id='identifierId']"));
						emailElement.clear();
						if(cnt == emailLen){
							navigateBackNTimesNew(driver, 4);
							break;
						}else{
							continue;
						}
					}					
				}
				CommonFunctions.searchAndClickByXpath(driver,"//android.widget.Button[@resource-id='signinconsentNext']");
				isAccountAddSuccess = true;
				Thread.sleep(4000);
			} catch (Exception e) {
				log.error("Exception while configuaring EmailId:"+mailId);
				isAccountAddSuccess = true;
			}
		} // email ids loop end;
		
	}
	
	public void openMenuDrawerNew(AppiumDriver driver, List<String> inputMailIds, String password) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		scrollAndClickNew(((AndroidDriver)driver), "Settings");
		Set<String> configuredMailIdsList = getListOfConfiguredMailIdsNew(true);
		System.out.println("configuredMailIdsList :"+configuredMailIdsList);
		inputMailIds.removeAll(configuredMailIdsList);
		if(inputMailIds.isEmpty() || inputMailIds.size()<=0){
			return;
		}
		Thread.sleep(1000);
		scrollAndClickNew(((AndroidDriver)driver), "Add account");
		addGmailAccount(driver,inputMailIds,password);
	}
	
	public void addNewMultipleGmailAccountNew(AppiumDriver driver, String gmailIDs, String password, boolean homePage,
			String expectedAccountTypeList) throws Exception {
		log.info("In UA.addNewMultipleGmailAccount Adding account");
		SoftAssert soft = new SoftAssert();
		if(null == gmailIDs || gmailIDs.isEmpty()){
			throw new Exception("Invalid email id is entered to configure..");
		}

		List<String> inputMailIds = new LinkedList<String>(Arrays.asList(gmailIDs.split(",")));
		try{
			//if (CommonFunctions.getSizeOfElements(driver,"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']") != 0) {
			if (BaseTest.isAccountSwitcherExist) {

				Set<String> configuredMailIdsList = getListOfConfiguredMailIdsNew(true);
				System.out.println("configuredMailIdsList :"+configuredMailIdsList);
				inputMailIds.removeAll(configuredMailIdsList);
				if(inputMailIds.isEmpty() || inputMailIds.size()<=0){
					log.info("all the accounts are already configured ..");
					return;
				}
				scrollAndClickNew((AndroidDriver)driver,"Add another account");
				System.out.println("Clicked on Add another account. inputMailIds:"+inputMailIds);
				
				addGmailAccount(driver,inputMailIds,password);
				System.out.println("Adding successful....... eachMailId:"+inputMailIds);
				navigateBackNTimesNew(driver, 2);
			}else{
				waitForHambergerMenuToBePresent();
				openMenuDrawerNew(driver,inputMailIds,password);
			}
			soft.assertAll();
		}catch(Exception e){
			log.error("Exception while configuring Email:"+inputMailIds);
		}
	}


	public void addNewMultipleGmailAccount(AppiumDriver driver, String gmailIDs, String password, boolean homePage,
			String expectedAccountTypeList) throws Exception {
		log.info("In UA.addNewMultipleGmailAccount Adding account");
		SoftAssert soft = new SoftAssert();
		String gmailUser = "";
		String[] userList = gmailIDs.split(",");
		try {
			Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018
			for (String gmailID : userList) {

				gmailUser = gmailID;
				if (homePage == false) {
					//if (verifyGmailAccountIsPresent(gmailID))// Commented by Venkat on 7/12/2018
					if(listOfConfiguredMailIds.contains(gmailID))// added by Venkat on 7/12/2018
						continue;
					else {
						navigateBack(driver);
						
						waitForHambergerMenuToBePresent();
						openMenuDrawer(driver);
						clickOnAccountSwitcher(driver);
					}
				}
				// String expectedAccountTypeList =
				// commonElements.get("ExpectedAccountTypeList");
				String actualAccountTypeList = verifyAllAccountTypesForMailSetup(driver);
				log.info("expectedAccountTypeList:-->" + expectedAccountTypeList);
				log.info("actualAccountTypeList:-->" + actualAccountTypeList);
				soft.assertTrue(actualAccountTypeList.contains(expectedAccountTypeList),
						"Account Type Verification text mismatched:::ActualText :" + actualAccountTypeList
								+ "::::ExpectedText :" + expectedAccountTypeList);
				log.info("Account Type List Verification Completed");
				log.info("Tap on 'Google' from the available options");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountGoogleOption"));

				// --Added and Modified by Phaneendra--//
				Thread.sleep(3000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Existing")) != 0) {
					log.info("Add a Google Account screen page is found with two options Existing and New");
					log.info("Clicking on Existing");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Existing"));
					log.info("Entering Email address");
					CommonFunctions.searchAndSendKeysByXpathNoClear(driver,
							commonElements.get("AddingGoogleAccountUsernameField"), gmailID);
					log.info("Entering Password");
					commonPage.waitForElementToBePresentByXpath(commonElements.get("PasswordTextField"));
					CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("PasswordTextField"),
							password);
					log.info("Tapping on 'NEXT' button");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ForwardArrow"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ForwardArrow"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ForwardArrow"));

				} else {

					log.info("Entering Email address");
					CommonFunctions.searchAndSendKeysByXpath(driver,
							commonElements.get("AddingGoogleAccountUsernameField"), gmailID);
					hideKeyboard();
					log.info("Tapping on 'NEXT' button");
					CommonFunctions.searchAndClickByXpath(driver,
							commonElements.get("AddingGoogleAccountUsernameNextButton"));
					Thread.sleep(5000);
					log.info("Entering Password");
					CommonFunctions.searchAndSendKeysByXpath(driver,
							commonElements.get("AddingGoogleAccountPasswordField"), password);
					hideKeyboard();
					log.info("Tapping on 'NEXT' button");
					CommonFunctions.searchAndClickByXpath(driver,
							commonElements.get("AddingGoogleAccountPasswordNextButton"));
					log.info("Tap on 'I AGREE' button");
					CommonFunctions.searchAndClickByXpath(driver,
							commonElements.get("AddingGoogleAccountAccpetButton"));
					Thread.sleep(6000);
					if (CommonFunctions.getSizeOfElements(driver,
							commonElements.get("AddingGoogleAccountAccpetButton")) != 0) {
						scrollDown(1);
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AddingGoogleAccountAccpetButton"));
						commonPage.waitForElementToBePresentById(commonElements.get("TakeMetoGmail"));
						log.info("In UA.addNewMultipleGmailAccount Clicking on Take me to Gmail button");
						if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountOwner")) != 0) {
							CommonFunctions.searchAndClickById(driver, commonElements.get("TakeMetoGmail"));
							log.info("In UA.addNewMultipleGmailAccount Clicked on Take me to Gmail button");

						} else {
							Thread.sleep(8000);
							CommonFunctions.searchAndClickById(driver, commonElements.get("TakeMetoGmail"));

						}
						Thread.sleep(5000);
						log.info("In UA.addNewMultipleGmailAccount Clicking on Confidential alert");
						if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ConfidentialAlert")) != 0) {
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ConfidentialAlert"));
						}
					}
				}

				Thread.sleep(10000);

				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("GoogleServicesSubTitle")) != 0) {
					CommonFunctions.searchAndClickById(driver, commonElements.get("subNavNext"));
/*					
					 * if (CommonFunctions.isElementByIdDisplayed(driver,
					 * commonElements.get("navbarMore")))
					 * CommonFunctions.searchAndClickById(driver,
					 * commonElements.get("navbarMore"));
					 * CommonFunctions.searchAndClickById(driver,
					 * commonElements.get("subNavNext")); Thread.sleep(3000);
*/					 
					CommonFunctions.searchAndClickById(driver, commonElements.get("TakeMetoGmail"));
					Thread.sleep(5000);
					log.info("In UA.addNewMultipleGmailAccount Clicking on Confidential alert");
					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ConfidentialAlert")) != 0) {
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ConfidentialAlert"));
					}
				}
				if (homePage) {
					waitForHambergerMenuToBePresent();
					homePage = false;
				} else {
					log.info("In UA.addNewMultipleGmailAccount Verifying for the account list button");
					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
						navigateBack(driver);
					}
					
					 //waitForElementToBePresentByXpath(commonElements.get("ExpandAccountList")); navigateBack(driver);
					  }
				log.info("In UA.addNewMultipleGmailAccount Verifying the account is added in list and also the add account options");
				Assert.assertTrue(verifyGmailAccountIsPresent(gmailID),
						" Failed to find " + gmailID + " in Account List");
				log.info("In UA.addNewMultipleGmailAccount Verification for new Account " + gmailID + " addition completed");
			}
			soft.assertAll();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed for Gmail ID : " + gmailUser + " Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the accounts to add in Gmail app
	 * 
	 * 
	 * @param driver
	 * @throws Exception
	 * 
	 */
	public void verifyAccountTypes(AppiumDriver driver, String ExpectedAccountTypes) throws Exception {
		SoftAssert soft = new SoftAssert();

		try {
			log.info("Fetching list of account types available in gmail app");
			String actualAccountTypeList = verifyAllAccountTypesForMailSetup(driver);
			log.info("expectedAccountTypeList:-->" + ExpectedAccountTypes);
			log.info("actualAccountTypeList:-->" + actualAccountTypeList);
			soft.assertTrue(actualAccountTypeList.contains(ExpectedAccountTypes),
					"Account Type Verification text mismatched:::ActualText :" + actualAccountTypeList
							+ "::::ExpectedText :" + ExpectedAccountTypes);
			log.info("Account Type List Verification Completed");

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to find the list of account types defined : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the accounts available in the gmail app
	 * 
	 * @param driver
	 * @param ExpectedAccountTypes
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyAccountsExists(AppiumDriver driver, String ExpectedAccountTypes, String password)
			throws Exception {
		SoftAssert soft = new SoftAssert();
		Thread.sleep(3000);
		/*
		 * if(CommonFunctions.getSizeOfElements(driver,
		 * commonElements.get("HamburgerMenu"))!=0){
		 * CommonFunctions.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu")); }
		 * 
		 */ try {

			log.info("Fetching list of account types available in gmail app");
			List<String> actualAccountTypeList = verifyAllAccountsAvailable(driver);
			log.info("actualAccountTypeList:----->" + actualAccountTypeList);
			log.info("expectedAccountTypeList:--->" + ExpectedAccountTypes);

			if (actualAccountTypeList.size() < 10) {
				log.info("Adding another account");
				// verifying email account if be add
				String gmailUser = "";
				String[] userList = ExpectedAccountTypes.split(",");
				Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018	

				for (String gmailID : userList) {
					gmailUser = gmailID;
					//if (verifyGmailAccountIsPresent(gmailID)) { // Commented by Venkat on 7/12/2018		
					if(listOfConfiguredMailIds.contains(gmailUser)){ // added by Venkat on 7/12/2018
						log.info("Verified " + gmailID);
						continue;
					} else {
						navigateBack(driver);
						waitForHambergerMenuToBePresent();
						log.info("Trying to add accounts " + gmailID);
						openMenuDrawer(driver);
						clickOnAccountSwitcher(driver);
						log.info("Tap on 'Google' from the available options");
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AddingGoogleAccountGoogleOption"));
						log.info("Entering email id");
						CommonFunctions.searchAndSendKeysByXpath(driver,
								commonElements.get("AddingGoogleAccountUsernameField"), gmailID);
						log.info("Tapping on 'NEXT' button");
						driver.hideKeyboard();
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AddingGoogleAccountUsernameNextButton"));
						log.info("Entering Password");
						CommonFunctions.searchAndSendKeysByXpath(driver,
								commonElements.get("AddingGoogleAccountPasswordField"), password);
						log.info("Tapping on 'NEXT' button");
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AddingGoogleAccountPasswordNextButton"));
						log.info("Tap on 'I AGREE' button");
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("AddingGoogleAccountAccpetButton"));
					}
				}
			}
			soft.assertTrue(actualAccountTypeList.contains(ExpectedAccountTypes),
					"Account Type Verification text mismatched:::ActualText :" + actualAccountTypeList
							+ "::::ExpectedText :" + ExpectedAccountTypes);

			log.info("Account Type List Verification Completed");

		} catch (Exception e) {
			throw new Exception("Unable to verify the accounts that are exists " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to set the signature for the existed accounts On or Off
	 * 
	 * @preCondition
	 * @param driver
	 * @param SignatureText
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void setSignatureOnOff(AppiumDriver driver, String SignatureTextA, String SignatureTextC,
			String generalSettings, String addAccount, String AccountB, String AccountA, String AccountC)
			throws Exception {
		try {
			// CommonFunctions.searchAndClickByXpath(driver,
			// commonElements.get("ExpandAccountList"));
			navigateToSettings(driver);
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SettingsOption"));
			// Getting list of accounts under Settings option
			List<WebElement> settingAccounts = driver.findElements(By.xpath(commonElements.get("SettingAccounts")));
			for (WebElement accounts : settingAccounts) {
				String accountName = accounts.getText();
				log.info("List of accounts displayed " + accountName);

				// If the account label name is General Settings or Add account,
				// the loop will not execute
				if (!accountName.equals(generalSettings) && !accountName.equals(addAccount)) {
					if (accountName.equals(AccountA) || accountName.equals(AccountB) || accountName.equals(AccountC)) {
						log.info("Setting the signature for the account " + accountName + " to On/Off");

						// CLick on the account selected
						WebElement selectAccount = driver
								.findElement(By.xpath("//android.widget.TextView[@text='" + accountName + "']"));
						selectAccount.click();
						log.info("Clicked on account " + accountName);
						CommonFunctions.scrollToCellByTitleVerify(driver, "Mobile Signature");
						// Setting the signature to Off for the second account B
						if (accountName.equals(AccountB)) {
							commonPage.waitForElementToBePresentByXpath(commonElements.get("Signature"));
							if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Signature")) != 0) {
								log.info("Signature label is found");
								if (CommonFunctions.getSizeOfElements(driver,
										commonElements.get("SignatureNotSet")) != 0) {
									log.info("Signture is Not Set for the second account as expected");
									CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
								} else {
									log.info("Signature is set for the account " + accountName
											+ " and clearing the signature to Off");
									CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Signature"));
									Thread.sleep(3000);
									((AndroidElement) driver.findElement(By.xpath(commonElements.get("SignatureText"))))
											.clear();
									CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
								}
							}
						} else {
							log.info("Verifying the signature for the account " + accountName);
							commonPage.waitForElementToBePresentByXpath(commonElements.get("Signature"));
							if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Signature")) != 0) {
								log.info("Signature label is found");
								if (CommonFunctions.getSizeOfElements(driver,
										commonElements.get("SignatureNotSet")) != 0) {
									log.info("Signture is Not Set for the " + accountName
											+ " selected and the signature is setting On");
									// CommonFunctions.searchAndClickByXpath(driver,
									// commonElements.get("BackButton"));
									CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Signature"));
									if (accountName.equals(AccountA)) {
										CommonFunctions.searchAndSendKeysByXpath(driver,
												commonElements.get("SignatureText"), SignatureTextA);
										CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
									} else if (accountName.equals(AccountC)) {
										CommonFunctions.searchAndSendKeysByXpath(driver,
												commonElements.get("SignatureText"), SignatureTextC);
										CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
									}
								} else {
									String signValue = driver
											.findElement(By.xpath(commonElements.get("SignatureValue"))).getText();
									log.info("Signature value is set as " + signValue);
								}
							}
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
						}
					}
				}
			}
			log.info("navigating back");
			navigateBack(driver);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to verify the other accounts are added to Gmail
	 * 
	 * 
	 * @throws Exception
	 */
	public void verifyExchangeAccountAdded(AppiumDriver driver, String EmailAddress) throws Exception {
		try {
			log.info("In UA.verifyExchangeAccountAdded Verifying the account " + EmailAddress + " is added to Gmail App");
			Thread.sleep(1500);
			verifyGmailAccountIsPresent(EmailAddress); 
			Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(false); // added by Venkat on 02/1/2019
			//listOfConfiguredMailIds.contains(EmailAddress) // commented by Venkat on 02/1/2019
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	public void tryGmailifySetUpIMAP(AppiumDriver driver) throws Exception {
		try {
			commonPage.waitForElementToBePresentByXpath(commonElements.get("LessSpamMoreControl"));
			log.info("Less Spam, more control page is displayed");
			log.info("Clicking on Try Gmailify option");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("TryGmailify"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("Next"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

			try {
				commonPage.waitForElementToBePresentByXpath(commonElements.get("LinkingAccounts"));
				log.info("Linking accounts");
				Thread.sleep(6000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AlmostDone")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			throw new Exception("Account not added properly " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to complete the setup for adding the accounts in gmail app
	 * 
	 * Reuse Method
	 * 
	 */
	public void completeSetupAccount(AppiumDriver driver) throws Exception {
		try {
			Thread.sleep(4000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
				log.info("Next button is found");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				Thread.sleep(2000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				// commonPage.waitForElementToBePresentByXpath(commonElements.get("Next"));
				Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("TakeMetoGmail")) != 0) {
					CommonFunctions.searchAndClickById(driver, commonElements.get("TakeMetoGmail"));
				}
			} else {
				log.info("Less Spam, more control page is displayed");
				log.info("Clicking on No Thanks option");
				Thread.sleep(8000);
				CommonPage.waitForElementToBePresentByXpath(commonElements.get("NoThanks"));
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("NoThanks")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NoThanks"));
				}
				log.info("Clicking on to navigate next page");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("Next"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

				log.info("Verifying the Account Options page screen is displayed or not");
				// CommonPage.waitForElementToBePresentByXpath(commonElements.get("AccountOptions"));
				Thread.sleep(5000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountOptions")) != 0) {
					log.info("Account Options screen is displayed");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
					log.info("Navigated forward from Account options page");

					log.info("Verifying for account setup confirmation screen");
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AccoutSetUp"))) {
						log.info("Account set up confirmation page is displayed and clicking on next button");
						Thread.sleep(4000);
						if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
						}
					}
				} else {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Setup is Interrupted due to some Issues");
		}
	}

	/**
	 * Method to setup the account
	 * 
	 * @param driver
	 * @param EmailAddMicrosoft
	 * @param EmailPwdMicrosoft
	 * @throws Exception
	 */
	public void setUpAccount(AppiumDriver driver, String EmailAddMicrosoft, String EmailPwdMicrosoft) throws Exception {
		try {

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SetUpEmail"))) {
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OutlookHotmailLive"))) {
					log.info("Tapping on the Outlook, Hotmail, and Live Option");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OutlookHotmailLive"));
					log.info("Waiting for the email account to load");
					commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailMicrosoft"));
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("EmailMicrosoft"))) {
						log.info("Entering the Outlook Email Address");
						CommonFunctions.searchAndSendKeysByXpathNoClear(driver, commonElements.get("EmailMicrosoft"),
								EmailAddMicrosoft);
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextPage"));
						if (CommonFunctions.isElementByXpathDisplayed(driver,
								commonElements.get("EmailMicrosoftPassword"))) {
							hideKeyboard();
							log.info("Entering the Outlook Email password");
							CommonFunctions.searchAndSendKeysByXpathNoClear(driver,
									commonElements.get("EmailMicrosoftPassword"), EmailPwdMicrosoft);
						}
					}
					hideKeyboard();
					log.info("Tap on signIn Outlook");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SignInButtonOutlook"));
				}
			} else {
				throw new Exception("Unable to find the requested page :");
			}
		} catch (Exception e) {
			throw new Exception("Unable to add account " + e.getMessage());
		}
	}

	/**
	 * Method to add the Outlook/Hotmail/Live email
	 * 
	 * @param driver,
	 *            EmailAddMicrosoft, EmailPwdMicrosoft
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void addOutlookHotmailLiveAcct(AppiumDriver driver, String EmailAddMicrosoft, String EmailPwdMicrosoft)
			throws Exception {
		try {
			setUpAccount(driver, EmailAddMicrosoft, EmailPwdMicrosoft);
			log.info("In UA.addOutlookHotmailLiveAcct Completing the setup");
			completeSetupAccount(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	public void addOutlookHotmailLiveAcctNew(AppiumDriver driver, String outlookEmailId, String outlookPwd)
			throws Exception {
		try {
			setUpAccount(driver, outlookEmailId, outlookPwd);
			log.info("In UA.addOutlookHotmailLiveAcct Completing the setup");
			completeSetupAccount(driver);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add Exchange and Office 365 Account
	 * 
	 * @throws Exception
	 * @Param
	 * @author Phaneendra
	 */
	public void addExchangeAccount(AppiumDriver driver, String ExchangeEmailAddress, String ExchangePassword)
			throws Exception {
		try {
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SetUpEmail"))) {
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExchangeOffice"))) {
					log.info("Tapping on the Exchange and Office 365 Option");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExchangeOffice"));
					log.info("Waiting for the email account to load");
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExchangeEmailField"))) {
						log.info("Entering the Exchange Email Address");
						CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangeEmailField"),
								ExchangeEmailAddress);
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
						if (CommonFunctions.isElementByXpathDisplayed(driver,
								commonElements.get("ExchangePasswordField"))) {
							log.info("Entering the Exchange Email password");
							CommonFunctions.searchAndSendKeysByXpath(driver,
									commonElements.get("ExchangePasswordField"), ExchangePassword);
						}
					}

					log.info("Tap on Next");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
					// commonPage.waitForElementToBePresentByXpath(commonElements.get("AccoutSetUp"));
					Thread.sleep(10000);
					CommonPage.waitForElementToBePresentByXpath(commonElements.get("RemoteScurityAdmin"));
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("RemoteScurityAdmin"))) {
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					}
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
			} else {
				throw new Exception("Unable to find the requested page :");
			}
			// completeSetupAccount(driver);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add Exchange and Office 365 Account
	 * 
	 * @throws Exception
	 * @Param
	 * @author Phaneendra
	 */
	public void addingOtherAccount(AppiumDriver driver, String ExchangeEmailAddress, String ExchangePassword)
			throws Exception {
		try {
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OtherAccount"))) {
				log.info("Tapping on the Other Option");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccount"));
				log.info("Waiting for the email account to load");
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExchangeEmailField"))) {
					log.info("Entering the Other Email Address");
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangeEmailField"),
							ExchangeEmailAddress);
				}
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				Thread.sleep(1200);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExchangePasswordField")) != 0) {
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangePasswordField"),
							ExchangePassword);
				}
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				Thread.sleep(5000); 
				//CommonPage.waitForElementToBePresentByXpath(commonElements.get("NoThanks")); // Commented by Venkat on 01/02/2019
				try {
					CommonPage.waitForElementToBePresentByXpath(commonElements.get("NoThanks"));
				} catch (Exception e) {
				}
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("NoThanks")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NoThanks"));
				}
				Thread.sleep(1200);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				Thread.sleep(2000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	public void addingOtherAccountForGmailify(AppiumDriver driver, String ExchangeEmailAddress, String ExchangePassword)
			throws Exception {
		try {
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OtherAccount"))) {
				log.info("Tapping on the Other Option");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccount"));
				log.info("Waiting for the email account to load");
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExchangeEmailField"))) {
					log.info("Entering the Other Email Address");
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangeEmailField"),
							ExchangeEmailAddress);
				}
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				Thread.sleep(1200);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExchangePasswordField")) != 0) {
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangePasswordField"),
							ExchangePassword);
				}
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				}
				Thread.sleep(5000);
				commonPage.waitForElementToBePresentByXpath(commonElements.get("NoThanks"));

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add Exchange and Office 365 Account IMAP settings
	 * 
	 * @throws Exception
	 * @Param
	 * @author Phaneendra
	 */
	public void addOtherAccount(AppiumDriver driver, String ServerType, String OtherEmailAddress, String OtherPassword)
			throws Exception {
		try {
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SetUpEmail"))) {
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OtherAccount"))) {
					log.info("Tapping on the Other Option");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccount"));
					log.info("Waiting for the email account to load");
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ExchangeEmailField"))) {
						log.info("Entering the Other Email Address");
						CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangeEmailField"),
								OtherEmailAddress);
					}
				}
				if (ServerType == "IMAP") {
					log.info("***Adding the Account with the IMAP Settings***");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooNextButton"));
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("YahooPasswordField"))) {
						log.info("Entering the Exchange Email password");
						CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("YahooPasswordField"),
								OtherPassword);
					}
					log.info("Tap on SignIn");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooNextButton"));
					commonPage.waitForElementToBePresentByXpath(commonElements.get("YahooAgreeButton"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooAgreeButton"));
				} else if (ServerType == "POP3") {
					log.info("***Account is adding with POP3 settings***");
					hideKeyboard();
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManualSetup"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PersonalPOP3"));
					log.info("Selected POP3 settings for " + OtherEmailAddress);
					Thread.sleep(2000);
					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Next")) != 0) {
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
					}
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ExchangePasswordField"),
							OtherPassword);
					log.info("Entered password for the email :" + OtherEmailAddress);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

					CommonPage.waitForElementToBePresentByXpath(commonElements.get("IncomingServerTitle"));
					log.info("Incoming Server Settings page is displayed");

					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("IncomingUserName"),
							"etouchtestthree@gmail.com");
					log.info("Entered the username");

					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ServerName"), "pop.gmail.com");
					log.info("Entered the server Name");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

					CommonPage.waitForElementToBePresentByXpath(commonElements.get("OutgoingServerTitle"));
					log.info("Entering details for Outgoing server");
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("OutgoingUserName"),
							"etouchtestthree@gmail.com");

					hideKeyboard();

					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("ServerName"),
							"smtp.gmail.com");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
					// CommonFunctions.searchAndClickByXpath(driver,
					// commonElements.get("Next"));

				}
			} else {
				throw new Exception("Unable to find the requested page SetupEmail:");
			}
			// completeSetupAccount(driver);
			log.info("Clicking on to navigate next page");
			// CommonFunctions.searchAndClickByXpath(driver,
			// commonElements.get("Next"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AccoutSetUp"))) {
				log.info("Account set up confirmation page is displayed and clicking on next button");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add Yahoo Account
	 * 
	 * @throws Exception
	 * @Param
	 * @author Phaneendra
	 */
	public void addYahooAccount(AppiumDriver driver, String YahooEmailAddress, String YahooPwd) throws Exception {
		try {
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SetUpEmail"))) {
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("Yahoo"))) {
					log.info("Tapping on the Yahoo Option");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Yahoo"));
					log.info("Waiting for the email account to load");
					if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("YahooEmailField"))) {
						log.info("Entering the Yahoo Email Address");
						CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("YahooEmailField"),
								YahooEmailAddress);
						hideKeyboard();
						// CommonFunctions.searchAndClickByXpath(driver,
						// commonElements.get("YahooNextButton"));
						if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("YahooNextButton"))) {
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooNextButton"));
						}
						if (CommonFunctions.isElementByXpathDisplayed(driver,
								commonElements.get("YahooPasswordField"))) {
							log.info("Entering the Yahoo Email password");
							CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("YahooPasswordField"),
									YahooPwd);
						}
					}
					log.info("Tap on signIn Yahoo");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooNextButton"));
					Thread.sleep(2500);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("YahooAgreeButton"));
				}
			} else {
				throw new Exception("Unable to find the requested page :");
			}
			completeSetupAccount(driver);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Account is not added : " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to click on Account switcher
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void clickOnAccountSwitcher(AppiumDriver driver) throws Exception {
		try {
			log.info("In UA.clickOnAccountSwitcher Tap on account selection drop down");
			Thread.sleep(3000);
			//if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
			if (BaseTest.isAccountSwitcherExist){
				log.info("In UA.accSwitch Inside IF after finding account switcher ICON");
				try {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
					Thread.sleep(5000);
				} catch (Exception e) {
					log.info("In UA.accSwitch Inside Exception. So openingMenuDrawer ");
					openMenuDrawer(driver);
				}
			} else {
				log.info("In UA.accSwitch Inside ELSE As NO account switcher ICON");
				scrollUp(1);
				Thread.sleep(800);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				} else if (CommonFunctions.getSizeOfElements(driver, commonElements.get("GmailProductName")) != 0) {
					log.info("In UA. Inside ELSE of GmailProductName:"+commonElements.get("GmailProductName"));
					driver.navigate().back();
					Thread.sleep(1000);
						if (CommonFunctions.getSizeOfElements(driver,"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']") != 0) {
						CommonFunctions.searchAndClickByXpath(driver,"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
						Thread.sleep(800);
						WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
						org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
						int x = element.getLocation().x;
						int y = element.getLocation().y;
						log.info("In UA.accSwitch X coordinates " + x + " Y coordinates " + y);
						// driver.swipe((windowSize.width) - 20, (y + 50), 80,
						// (y + 50), 1000);
						driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
						CommonFunctions.scrollToCellByTitleVerify(driver, "Add another account");
					}
				}
			}
			log.info("In us asw Tap on 'Add account' option AddAccountOption:"+commonElements.get("AddAccountOption"));
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAccountOption")) != 0) {
				log.info("UA if AddAccountOption:"+commonElements.get("AddAccountOption"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			} else {
				//scrollDown(1);
				//scrollDown(1); // added by Venkat on 27/12/2018  for Nexus
				CommonFunctions.scrollTo(driver, "Add another account"); 
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to click on the add Account option
	 * 
	 * @throws Exception
	 * 
	 * 
	 */
	public void clickAddAccount(AppiumDriver driver) throws Exception {
		try {

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				clickOnAccountSwitcher(driver);
			} else if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AddAccountOption"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));

			} else if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AddAccountOption"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to find the Add Account option");
		}
	}

	/**
	 * Method to click on the Manage Account option
	 * 
	 * @throws Exception
	 * 
	 * 
	 * @author Phaneendra
	 */
	public void manageAccount(AppiumDriver driver) throws Exception {
		try {

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HamburgerMenu"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				log.info("Tap on account selection drop down");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ManageAccountOption"))) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
				} else {
					log.info("Unable to find the Manage accounts option, hence scroll down to find it");
					scrollDown(1);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
					log.info("Manage Account option is found and clicked on it");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to find the Add Account option");
		}
	}

	/**
	 * Method to verify the added server accounts
	 * 
	 * @throws Exception
	 */
	public void verifyServerAccount(AppiumDriver driver) throws Exception {
		try {
			log.info("Verifying the server accounts added");
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AccountsTitle"))) {
				List<WebElement> accounts = driver.findElements(By.xpath(
						"//android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"));
				for (int i = 0; i <= accounts.size(); i++) {
					String accountName = driver
							.findElement(By.xpath("//android.widget.LinearLayout[" + i
									+ "]/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"))
							.getText();
					log.info(accountName);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("It seems one the account is not added successfully");
		}
	}

	/**
	 * Method to verify All Account Types For MailSetup
	 * 
	 * @param driver
	 * @return
	 * @throws Exception
	 */
	public String verifyAllAccountTypesForMailSetup(AppiumDriver driver) throws Exception {
		log.info("******************* Started verifying Account Types For Mail Setup *******************");
		Thread.sleep(1500);
		List<WebElement> elemetList = driver.findElements(By.xpath(commonElements.get("AccountTypeLabelXpath")));
		String actualList = new String();
		for (WebElement ele : elemetList) {
			actualList = actualList.concat(ele.getText()) + ", ";
		}
		return actualList;
	}

	public List<String> verifyAllAccountsAvailable(AppiumDriver driver) throws Exception {
		Thread.sleep(3000);
		log.info("***************Started verifying available account types that are setup already**************");
		// openMenuDrawer(driver);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		} else {
			log.info("account list not found and hence scrolling up");
			scrollUp(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		}
		Thread.sleep(2000);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AllIndoxesOption")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		}
		String acctDisplay = driver.findElement(By.xpath(commonElements.get("AccountSelected"))).getText();
		log.info("Account selected " + acctDisplay);
		List<WebElement> elemetList = driver.findElements(By.id(commonElements.get("AccountListCommonID")));
		List<String> currentOptions = new ArrayList<>();
		log.info("Getting list of accounts");
		for (WebElement ele : elemetList) {
			currentOptions.add(ele.getText());
		}
		currentOptions.add(acctDisplay);
		log.info(currentOptions);
		return currentOptions;
	}

	/**
	 * Method to verify Sharing Option PopUp Is Present
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifySharingOptionPopUpIsPresent() throws Exception {
		String ActualValue = "";
		String ExpectedValue = "";
		boolean OptionSelected;
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		log.info("Verify the share options");
		/*
		 * if (!CommonFunctions.isElementByIdNotDisplayed(driver,
		 * commonElements.get("SharingOptionsSendButton")))
		 * CommonFunctions.searchAndClickById(driver,
		 * commonElements.get("SharingOptionsSendButton"));
		 */
		boolean shareOptions = true;
		try {
			shareOptions = CommonFunctions.isElementByXpathDisplayed(driver,commonElements.get("SharingOptionsTitle"));
		} catch (Exception e) {
		}
		log.info("ShareOptions are available " + shareOptions);

		if (shareOptions == false) {
			throw new Exception("Sharing Options pop up is not displayed");

		} else {

			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SendOptionWithoutSharing"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ShareOptionWithLink"));
			// --Verifying the funcationality of share options--//
			ActualValue = CommonFunctions.searchAndGetAttributeOnElementByXpath(driver,
					commonElements.get("ShareOptionWithLink"), "checked");
			OptionSelected = Boolean.valueOf(ActualValue);
			log.info("Attribute Value retured and the option selected value is : " + ActualValue);
			if (OptionSelected == true) {
				log.info("First share option value is selected");
			}

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SharePermissions"));
			// java.util.List<String> permissions = new ArrayList<String>();
			int availablePermissions = driver
					.findElements(By.xpath("//android.widget.CheckedTextView[@resource-id='android:id/text1']")).size();
			log.info("Number of available permissions : " + availablePermissions);
			if (availablePermissions == 3) {
				for (int i = 0; i < availablePermissions; i++) {
					String permissions = driver.findElement(By.xpath(
							"//android.widget.CheckedTextView[@resource-id='android:id/text1' and @index='" + i + "']"))
							.getText();
					log.info(permissions);
				}
			} else {
				throw new Exception("Available sharing options are not displayed all 3");
			}
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SharePermissionsView"));

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendOptionWithoutSharing"));
			log.info("Second option is selected");
			log.info("Verifying the second option value is checked or not");
			ActualValue = CommonFunctions.searchAndGetAttributeOnElementByXpath(driver,
					commonElements.get("SendOptionWithoutSharing"), "checked");
			OptionSelected = Boolean.valueOf(ActualValue);
			log.info("Attribute Value retured and the second option selected value is : " + ActualValue);
			if (OptionSelected == true) {
				log.info("Second share option value is selected");
			}

			// --Verifying the cancel option--//
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ShareOptionsCancel"));
			log.info("Verified and Clicked cancel button on sharing options");

			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ComposeTitle"));
			log.info("Verified the sharing options screen is closed and is on Compose screen now");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			log.info("Clicked on send button");
			Thread.sleep(1200);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
			}
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SharingOptionsTitle"));
			log.info("Verified sharing Options pop up is displayed again");

			// --Verifying the i Icon--//
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("iIconShareOptions"));
			log.info("Clicked on i Icon");
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("HelpPage"));
			log.info("Verified and Navigated to learn more page");

			navigateBack(driver);
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SharingOptionsTitle"));
			log.info("Navigated back to sharing options pop up screen");
		}

	}

	/**
	 * Method to add new Gmail account
	 * 
	 * @param GmailID
	 * @param password
	 * @throws Exception
	 */
	public void addGmailAccount(String GmailID, String password) throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			waitForHambergerMenuToBePresent();
			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			Thread.sleep(2000);
			scrollDown(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountGoogleOption"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountUsernameField"),
					GmailID);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountUsernameNextButton"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountPasswordField"),
					password);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountPasswordNextButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountAccpetButton"));
			waitForElementToBePresentByXpath(commonElements.get("ExpandAccountList"));
			navigateBack(driver);
			Assert.assertTrue(verifyGmailAccountIsPresent(GmailID), " Failed to find " + GmailID + " in Account List");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to add Gmail ID : " + GmailID + " Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add New Gmail Account
	 * 
	 * @param GmailID
	 * @param password
	 * @param homePage
	 * @throws Exception
	 */
	public void addNewGmailAccount(String GmailID, String password, boolean homePage) throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountGoogleOption"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountUsernameField"),
					GmailID);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountUsernameNextButton"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("AddingGoogleAccountPasswordField"),
					password);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountPasswordNextButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingGoogleAccountAccpetButton"));
			if (homePage) {
				CommonFunctions.searchAndClickById(driver, commonElements.get("navbarMore"));
				CommonFunctions.searchAndClickById(driver, commonElements.get("navbarMore"));
				CommonFunctions.searchAndClickById(driver, commonElements.get("TakeMetoGmail"));
			}
			waitForElementToBePresentByXpath(commonElements.get("ExpandAccountList"));
			navigateBack(driver);
			Assert.assertTrue(verifyGmailAccountIsPresent(GmailID), " Failed to find " + GmailID + " in Account List");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to add Gmail ID : " + GmailID + " Due to :" + e.getLocalizedMessage());
		}
	}

	public boolean AccountExistsNewUI(String GmailID) throws Exception {
		log.info("Started verifying for presence of new account in List UI " + GmailID);
		AppiumDriver driver = map.get(Thread.currentThread().getId());

		CommonFunctions.searchAndClickByXpath(driver,
				"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
		Thread.sleep(800);
		WebElement element = driver.findElement(
				By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
		org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
		int x = element.getLocation().x;
		int y = element.getLocation().y;
		log.info("X coordinates " + x + " Y coordinates " + y);
		// driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50), 1000);
		driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
		Thread.sleep(1000);
		if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AddAnotherAccount"))) {
			log.info("Switcher expanded");
		} else {
			try {
				driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);

			} catch (Exception e) {
				throw new Exception("Issue while expanding the switcher");
			}
		}

		List<WebElement> myElements = driver.findElements(
				By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));

		for (WebElement e : myElements) {
			System.out.println(e.getText());
			if (GmailID.equalsIgnoreCase(e.getText()))
				log.info("Account exists");
			driver.navigate().back();
			return true;
		}

		return false;

	}

	/**
	 * Method to verify Gmail account is present or not
	 * 
	 * @param GmailID
	 * @return
	 * @throws Exception
	 */
	

	public boolean verifyGmailAccountIsPresent(String GmailID) throws Exception {
		log.info("In UA.verifyGmailAccountIsPresent Started verifying for presence of new account in List " + GmailID);
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		Set<String> accts = new LinkedHashSet<>();

		if (CommonFunctions.getSizeOfElements(driver,"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']") != 0) {

			
			// AccountExistsNewUI(GmailID);
			log.info("UA Started verifying for presence of new account in List UI " + GmailID);
			CommonFunctions.searchAndClickByXpath(driver,"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
			Thread.sleep(800);
			WebElement element = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name'] | //android.widget.TextView[@resource-id='com.google.android.gm:id/account_display_name']"));
			org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
			int x = element.getLocation().x;
			int y = element.getLocation().y;
			log.info("In UA.verifyGmailAccountIsPresent X coordinates " + x + " Y coordinates " + y);
			driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
			Thread.sleep(1300);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAnotherAccount")) != 0) {
				log.info("In UA.verifyGmailAccountIsPresent Switcher expanded");
			} else {
				try {
					driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);

				} catch (Exception e) {
					throw new Exception("Issue while expanding the switcher");
				}
			}

			List<WebElement> myElements = driver.findElements(
					By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
			List<WebElement> dotGmailAccts = driver.findElements(By
					.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name') and contains(@text,'@gmail.com')] "
							+ "| //android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name') and contains(@text,'@yahoo')] "
							+ "| //android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name') and contains(@text,'@outlook')] "
							+ "| //android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_display_name') and contains(@text,'@gmx')]"));
			for (WebElement e1 : dotGmailAccts) {
				log.info("In UA.verifyGmailAccountIsPresent e1.getText():"+e1.getText());
				accts.add(e1.getText());
			}
			for (WebElement e : myElements) {
				//System.out.println(e.getText());
				log.info("In UA.verifyGmailAccountIsPresent e.getText():"+e.getText());
				accts.add(e.getText());
			}
			if (accts.contains(GmailID)) {
				//log.info("Account exists");
				log.info("In UA.verifyGmailAccountIsPresent Account exists");
				driver.navigate().back();
				return true;
			}

		} else {
			
			String accountListButton = commonElements.get("ExpandAccountList");
			java.util.List<WebElement> accountSelected = driver.findElementsByXPath(commonElements.get("AccountSelected"));
			java.util.List<WebElement> accountList = driver
					.findElementsByXPath(commonElements.get("AccountListCommonXpath"));

			inbox.openMenuDrawer(driver);
			log.info("In UA.verifyGmailAccountIsPresent Veriyfing for account list");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
				log.info("In UA.verifyGmailAccountIsPresent Tapping on expand account list");
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				}
				// CommonFunctions.searchAndClickByXpath(driver,
				// commonElements.get("ExpandAccountList"));
			} else {
				int count = 0;
				log.info("In UA.verifyGmailAccountIsPresent Going to while loop");
				while (CommonFunctions.isElementByXpathNotDisplayed(driver, accountListButton) == true) {
					log.info("In UA.verifyGmailAccountIsPresent Scrolling up");
					scrollUp(1);
					count++;
					if (count > 5)
						break;
				}
			}
			log.info("In UA.verifyGmailAccountIsPresent Getting list of accounts");
			Thread.sleep(3000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountSelected")) == 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				Thread.sleep(1500);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			}
			// List accts = new ArrayList(new LinkedHashSet<>());
			accountList.addAll(accountSelected);
			for (int x = 0; x < accountList.size(); x++) {
				accts.add(accountList.get(x).getText());
				if (accts.contains(accountSelected))
					accts.add(accountSelected.get(0).getText());

			}
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAccountOption")) != 0) {
				log.info("In UA.verifyGmailAccountIsPresent All accounts are added to the list");
			} else {
				scrollDown(1);
				Thread.sleep(1000);
				for (int x = 0; x < accountList.size(); x++) {
					accts.add(accountList.get(x).getText());
					if (accts.contains(accountSelected))
						accts.add(accountSelected.get(0).getText());

				}

			}
			if (accts.contains(GmailID)) {
				log.info("In UA.verifyGmailAccountIsPresent Verifying account is present in list " + GmailID);
				Thread.sleep(400);
				log.info("In UA.verifyGmailAccountIsPresent Account ID " + GmailID + " is Present in Account List");
				// switchMailAccount(driver, GmailID);

				return true;
			}
		}
		return false;

	}

	
	/** Verify account name is present after addin the account
	 * 
	 * @throws Exception
	 */
	public void verifyAccountName(AppiumDriver driver) throws Exception {
		try {
			log.info("Verifying the account name is displayed");
			CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("AccountSelected"));
			String accountText = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("AccountSelected"));
			log.info(accountText);
			if (accountText.length() != 0) {
				log.info("Account Name is also displayed");
			} else {
				throw new Exception("Account Name is not displayed ");
			}

		} catch (Exception e) {
			throw new Exception("Account Name is not present " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove if the account is already exists
	 * 
	 * @param driver
	 * @param Email
	 * @throws Exception
	 */
	public void removeAccountIMAPIfExists(AppiumDriver driver, String Email) throws Exception {
		try {
			String emailXpath = commonElements.get("GoogleAccountListedOptions").replace("Emailid", Email);
			log.info("Removing IMAP account is already exists");
			Thread.sleep(2000);

			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountIconNew")) != 0) {
				log.info("Started verifying for presence of new account in List UI " + Email);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
				Thread.sleep(800);
				WebElement element = driver.findElement(
						By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
				org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
				int x = element.getLocation().x;
				int y = element.getLocation().y;
				log.info("X coordinates " + x + " Y coordinates " + y);
				// driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50),
				// 1000);
				driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
				Thread.sleep(1000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ManageAccountsOnDevice")) != 0) {
					CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
				} else {
					try {
						driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
						scrollDown(2);
						CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
					} catch (Exception e) {
						throw new Exception("Issue while expanding the switcher");
					}
				}
			} else {
				openMenuDrawer(driver);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AllIndoxesOption")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
					Thread.sleep(1000);
				}
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));

			}
			commonPage.waitForElementToBePresentByXpath(commonElements.get("OtherAccountsIMAP"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccountsIMAP"));
			Thread.sleep(3000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GoogleAccountListedOptions").replace("Emailid", Email)) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, emailXpath);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
				CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));

			} else {
				log.info("Account is not added before");
			}
		} catch (Exception e) {
			throw new Exception("Unable to remove account " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove if the account is already exists
	 * 
	 * @param driver
	 * @param Email
	 * @throws Exception
	 */
	public void removeAccountifExists(AppiumDriver driver, String Email) throws Exception {
		try {
			String emailXpath = commonElements.get("GoogleAccountListedOptions").replace("Emailid", Email);
			log.info("Removing account is already exists");
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AllIndoxesOption")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				Thread.sleep(1000);
			}
			scrollDown(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveGoogleAccountOption"));
			Thread.sleep(3000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GoogleAccountListedOptions").replace("Emailid", Email)) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, emailXpath);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
				CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
				driver.navigate().back();
				driver.navigate().back();
				driver.navigate().back();
				driver.navigate().back();
			} else {
				log.info("Account is not added before");
			}
		} catch (Exception e) {
			throw new Exception("Unable to remove account " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove account
	 * 
	 * @param emailID
	 * @throws Exception
	 */
	public void removeAccount(AppiumDriver driver, String emailID, String xpath) throws Exception {
		try {
			log.info("Removing account");
			String emailIDXpath = commonElements.get("GoogleAccountListedOptions").replace("Emailid", emailID);

			if (CommonFunctions.getSizeOfElements(driver,
					"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']") != 0) {
				Thread.sleep(1500);
				// AccountExistsNewUI(GmailID);
				log.info("Started verifying for presence of new account in List UI " + emailID);
				CommonFunctions.searchAndClickByXpath(driver,
						"//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
				Thread.sleep(1500);
				WebElement element = driver.findElement(
						By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
				org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
				int x = element.getLocation().x;
				int y = element.getLocation().y;
				log.info("X coordinates " + x + " Y coordinates " + y);
				// driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50),
				// 1000);
				driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
				Thread.sleep(1000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ManageAccountsOnDevice")) != 0) {
					CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
				} else {
					try {
						driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
						scrollDown(1);
						CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
					} catch (Exception e) {
						throw new Exception("Issue while expanding the switcher");
					}
				}
			}

			else {
				openMenuDrawer(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				Thread.sleep(2000);
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
			}

			Thread.sleep(3000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("UsersandAccounts")) != 0) {
				log.info("selecting the desired account to remove from the app");
			} else {

				//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveGoogleAccountOption"));
				//Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveGoogleAccountOption")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveGoogleAccountOption"));
					Thread.sleep(1500);
				}else{
					int cnt = 0;
					while(cnt++ <= 2){
						if(CommonFunctions.getSizeOfElements(driver, commonElements.get("RemoveGoogleAccountOption")) <= 0){
							CommonPage.scrollDown(1);
							continue;
						}
						break;
					}
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveGoogleAccountOption"));
					Thread.sleep(1000);
					CommonFunctions.searchAndClickByXpath(driver, "//android.widget.Button[@text='Remove account'] | //android.widget.Button[@resource-id='com.android.settings:id/button']");
					Thread.sleep(1000);
					CommonFunctions.searchAndClickByXpath(driver, "//android.widget.Button[@text='Remove account'] | //android.widget.Button[@resource-id='android:id/button1']");
					log.info("Account deleted successfully");
					
				}
			}
			log.info("Selecting the account to delete");
			//CommonFunctions.scrollsToTitle(driver, emailIDXpath); // added by Venkat on 18/12/2018
			if (CommonFunctions.getSizeOfElements(driver, emailIDXpath) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, emailIDXpath);
				log.info("Verifying for More options to click");
				if (CommonFunctions.isElementByXpathNotDisplayed(driver,
						commonElements.get("RemoveAccountMoreOptions"))) {
					if (CommonFunctions.isElementByXpathDisplayed(driver,
							commonElements.get("RemoveAccountPopUpListOption"))) {
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("RemoveAccountPopUpListOption"));
						log.info("Clicked on remove button");
						CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
						log.info("Clicked on confirm button to remove");
					} else {

						Runtime.getRuntime().exec("adb shell input keyevent 82");
						CommonFunctions.searchAndClickByXpath(driver,
								commonElements.get("RemoveAccountPopUpListOption"));
						CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
					}
				} else {
					log.info("Removing the account from selecting more options");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
					CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
				}
			} else {
				log.info("Account not exists in the list for deleting");
				throw new Exception("Account not exist in the list for deleting");
			}
			// inbox.navigateUntilHamburgerIsPresent(driver);
			driver.navigate().back();
			Thread.sleep(3000);
			driver.navigate().back();
			Thread.sleep(3000);
			driver.navigate().back();
			Thread.sleep(3000);
			//boolean acctExists = userAccount.verifyGmailAccountIsPresent(emailID); // Commented by Venkat on 7/12/2018
			Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018	
			boolean acctExists = (listOfConfiguredMailIds.isEmpty()) ? true : listOfConfiguredMailIds.contains(emailID);// added by Venkat on 7/12/2018	

			if (acctExists == false) {
				log.info("Account removed");
				driver.navigate().back();
			} else {
				throw new Exception("Account not removed yet");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to delete account " + emailID + " Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove All Accounts
	 * 
	 * @throws Exception
	 */
	public void removeAllAccounts() throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ManageAccountOption"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
			} else {
				scrollDown(1);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
			}

			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("OtherPOP"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherPOP"));
				CommonFunctions.searchAndClickById(driver, commonElements.get("AccountIcon"));
				removeAccountScreen();
			}

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveGoogleAccountOption"));

			boolean isMultipleAccounts = CommonFunctions.isElementByIdDisplayed(driver,
					commonElements.get("AccountIcon"));
			if (isMultipleAccounts) {
				List<WebElement> accountList = driver.findElements(By.id(commonElements.get("AccountIcon")));
				for (WebElement ele : accountList) {
					CommonFunctions.searchAndClickById(driver, commonElements.get("AccountIcon"));
					removeAccountScreen();
				}
				driver.navigate().back();
				Assert.assertTrue(isGmailWelComePageDisplayed(driver),
						"Welcome to Gmail NOT rendered after removing all accounts");
			} else {
				removeAccountScreen();
				driver.navigate().back();
				Assert.assertTrue(isGmailWelComePageDisplayed(driver),
						"Welcome to Gmail NOT rendered after removing all accounts");
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to delete account All Accounts" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove all accounts from Manage Accounts option
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void removeAllAccountsNew() throws Exception {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("WelcomeToGmail"))) {
				log.info("No accounts are added in Gmail. Please add atleast one account");
				addNewAccountFromGmailHomePage(driver);

			} else {
				openMenuDrawer(driver);
				log.info("Verifying the manage accts option");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));

				if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ManageAccountOption"))) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
					log.info("Clicked on Manage accounts option before scrolling");
				} else {
					scrollDown(1);
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
					log.info("Clicked on Manage accounts option after scrolling");
				}
				CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("AccountsTitle"));
				int accountscount = driver
						.findElements(By
								.xpath("//android.widget.ListView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"))
						.size() - 1;
				log.info("Number of accounts available are " + accountscount);
				if (accountscount > 1) {
					for (int i = 0; i < accountscount; i++) {
						String accountName = driver
								.findElement(
										By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index=" + i
												+ "]/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"))
								.getText();
						log.info("List of accounts available-----" + accountName);
					}
					for (int i = 0; i < accountscount; i++) {
						Thread.sleep(2000);
						String accountName = driver
								.findElement(
										By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index=" + i
												+ "]/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"))
								.getText();
						log.info("List of accounts available-----" + accountName);

						driver.findElement(
								By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index=" + i + "]"
										+ "/android.widget.RelativeLayout/android.widget.TextView[@resource-id='android:id/title']"))
								.click();
						Thread.sleep(3000);
						if (accountName.equals("Google") && (CommonFunctions.getSizeOfElements(driver,
								commonElements.get("SyncAppData")) != 0)) {
							log.info("Account have only one google");
							driver.navigate().back();
						} else {
							List<WebElement> accountList = driver
									.findElements(By.id(commonElements.get("AccountIcon")));
							for (WebElement ele : accountList) {
								log.info("Removing account " + ele.getText());
								CommonFunctions.searchAndClickById(driver, commonElements.get("AccountIcon"));
								removeAccountScreen();
							}
						}
					}
				}
				driver.navigate().back();
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			}

		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
		}
	}

	/**
	 * Method to remove account
	 * 
	 * @throws Exception
	 */
	public void removeAccountScreen() throws Exception {
		AppiumDriver driver = map.get(Thread.currentThread().getId());

		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
		CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
	}

	/**
	 * Method to open user account settings
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void openUserAccountSetting(AppiumDriver driver, String user) throws Exception {
		log.info("In openUserAccountSetting");
		try {

			openMenuDrawer(driver);
			/*
			 * scrollTo("Settings"); log.info(
			 * "Opening user account settings for user: " + user);
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SettingsOption")); Thread.sleep(5000);
			 */
			CommonFunctions.scrollsToTitle(driver, "Settings");
			CommonFunctions.scrollsToTitle(driver, user);
			/*
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("AddAccountOption").replace("Add account",
			 * user));
			 */} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to openUserSpecificSetting:::" + e.getLocalizedMessage());
		}
	}
    /**
	 * author Venkat
	 **/
	public void openUserAccountSettingNew(AppiumDriver driver, String user) throws Exception {
		log.info("In openUserAccountSettingNew");
		try {
			openMenuDrawerNew(driver);
			CommonFunctions.scrollsToTitle(driver, "Settings");
			CommonFunctions.scrollsToTitle(driver, user);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to openUserSpecificSetting:::" + e.getLocalizedMessage());
		}
	}

	public void NavigateGeneralSettingsFromSettings(AppiumDriver driver) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettings"));

	}

	/**
	 * Method to Navigate to Settings > General Settings
	 * 
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void navigateToGeneralSettings(AppiumDriver driver) throws Exception {
		try {
			log.info("Under menu drawer, tapping on Settings > General settings");
			Thread.sleep(1500);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("GeneralSettings")) != 0) {
				log.info("Already driver in settings page");
			} else if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HamburgerMenu")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				// CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("FolderIcon")) != 0) {
					Thread.sleep(800);
					CommonFunctions.scrollsToTitle(driver, "Settings");
					log.info("Settings option found");
				} else {
					log.info("Settings option not found hence finding it");
					openMenuDrawer(driver);
					Thread.sleep(2000);
					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AvtarInMenuDrawer")) != 0) {
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
						Thread.sleep(1500);
					}
					/*
					 * scrollDown(2); Thread.sleep(2000);
					 * CommonFunctions.scrollTo(driver, "Settings");
					 */
					CommonFunctions.scrollsToTitle(driver, "Settings");
					log.info("Scrolled to settings and clicked");
					Thread.sleep(1000);
				}

				log.info("Clicked on settings option");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
				log.info("Clicked on general settings option");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettings"));
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			openMenuDrawer(driver);
			scrollDown(2);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettings"));
			// throw new Exception("Failed to navigate to General Settings : Due
			// to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to add Rediff Account
	 * 
	 * @param RediffID
	 * @param password
	 * @throws Exception
	 */
	public void addRediffAccount(AppiumDriver driver, String RediffID, String password) throws Exception {
		try {

			waitForHambergerMenuToBePresent();
			openMenuDrawer(driver);
			log.info("Adding rediffmail account");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddingAccountOtherOption"));
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("OtherEmailAddressField"), RediffID);
			hideKeyboard();
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherPOP"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("OtherPassword"), password);
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("OtherIncomingServer"),
					"pop.rediffmail.com");

			if (!CommonFunctions.isElementByIdNotDisplayed(driver, commonElements.get("OtherSecurityTypeDropDown"))) {
				CommonFunctions.searchAndClickById(driver, commonElements.get("OtherSecurityTypeDropDown"));
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OtherSecurityTypeDropDownNoneOption"));
			}
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			if (!CommonFunctions.isElementByIdNotDisplayed(driver, commonElements.get("OtherSecurityTypeDropDown"))) {
				CommonFunctions.searchAndClickById(driver, commonElements.get("OtherPopUpProceedOption"));
			}
			CommonFunctions.searchAndSendKeysByID(driver, commonElements.get("OtherOutgoingSecuritySmtp"),
					"smtp.rediffmail.com");
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			Thread.sleep(2000);
			if (driver.findElements(By.xpath("//android.widget.TextView[@text='Username or password is incorrect.']"))
					.size() != 0) {
				driver.findElement(By.xpath("//android.widget.Switch[@text='Require signin ON']")).click();
				CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));

			}

			if (!CommonFunctions.isElementByIdNotDisplayed(driver, commonElements.get("OtherSecurityTypeDropDown"))) {
				CommonFunctions.searchAndClickById(driver, commonElements.get("OtherPopUpProceedOption"));
			}
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("OtherNext"));
			if (!CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("ExpandAccountList")))
				navigateBack(driver);
		} catch (Throwable e) {
			throw new Exception("Failed to add Gmail ID : " + RediffID + " Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to Gmail Default Action To Archive
	 * 
	 * @throws Exception
	 */
	public void setGmailDefaultActionToArchive(AppiumDriver driver) throws Exception {
		// Map < String, String > swipeActions =
		// commonPage.getTestData("SwipeActions");

		log.info("Tapping on Menu drawer > Settings > General settings");
		navigateToGeneralSettings(driver);
		Thread.sleep(1000);
		CommonFunctions.scrollsToTitle(driver, "Swipe actions");
		Thread.sleep(1000);
		CommonFunctions.scrollsToTitle(driver, "Right swipe");
		Thread.sleep(800);
		CommonFunctions.scrollsToTitle(driver, "Archive");
		Thread.sleep(800);
		CommonFunctions.scrollsToTitle(driver, "Left swipe");
		Thread.sleep(800);
		CommonFunctions.scrollsToTitle(driver, "Delete");
		Thread.sleep(800);

		/*
		 * log.info("Tapping on 'Gmail default action ");
		 * CommonFunctions.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingsGMailDefaultAction")); log.info(
		 * "Selecting Archive and navigating back");
		 * CommonFunctions.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingsGMailDefaultActionArchive"));
		 */navigateBack(driver);
		navigateBack(driver);
		navigateBack(driver);

	}

	/**
	 * Method to set Gmail Default Action To Delete
	 * 
	 * @throws Exception
	 */
	public void setGmailDefaultActionToDelete(AppiumDriver driver) throws Exception {

		log.info("Tapping on Menu drawer > Settings > General settings");
		navigateToGeneralSettings(driver);
		log.info("Tapping on 'Gmail default action ");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettingsGMailDefaultAction"));
		log.info("Selecting Delete and navigating back");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GeneralSettingsGMailDefaultDelete"));
		navigateBack(driver);
		navigateBack(driver);
	}

	/**
	 * Method verifies option available in settings page
	 * 
	 * @param driver
	 * @param Options
	 * @throws Exception
	 */

	public void verifySettingsOptions(AppiumDriver driver, HashSet<String> Options) throws Exception {
		List<String> accounts = new ArrayList<String>();
		int failureCount = 0;
		for (String option : Options) {
			try {
				String settingOptionsXpath = commonElements.get("SettingsMenuOptions");
				if (!(CommonFunctions.isElementByXpathNotDisplayed(driver,
						settingOptionsXpath.replace("SetingOptions", option)))) {
					log.info("Option " + option + " is displayed in Settings Page");
				}
			} catch (Throwable e) {
				failureCount++;
				e.printStackTrace();
				throw new Exception("Failed to find Option \"" + option + "\" in Settings page : Due to : "
						+ e.getLocalizedMessage());

			}
		}

	}

	/**
	 * Returns list of account added in app
	 * 
	 * @param driver
	 * @return
	 * @throws Exception
	 */
	public HashSet<String> getListOfAddedAccounts(AppiumDriver driver) throws Exception {
		HashSet<String> accounts = new HashSet<String>();
		try {
			String accountListButton = commonElements.get("ExpandAccountList");
			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, accountListButton);

			List<WebElement> accountList = driver.findElementsById(commonElements.get("AccountListCommonID"));
			try {
				accountList.add(driver.findElementById(commonElements.get("AccountListSelectedIMAPAccountID")));
			} catch (Exception e) {
			}
			log.info("List of Accounts added :");
			for (WebElement element : accountList) {
				String mailID = element.getText().trim();

				if (mailID.contains("@")) {

					log.info("Account added is : " + mailID);
					accounts.add(mailID);
					log.info(mailID);
				}
			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(
					"Failed to get List of Added accounts from account list : Due to :" + e.getLocalizedMessage());
		}
		return accounts;
	}

	/**
	 * To Navigate to settings page
	 * 
	 * @throws Exception
	 * 
	 *             Can be use to optimize other navigate to settings method
	 */
	public void navigateToSettings(AppiumDriver driver) throws Exception {
		try {
			log.info("Under menu drawer, tapping on Settings");
			Thread.sleep(1500);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("GeneralSettings")) != 0) {
				log.info("Already driver in settings page");
			} else if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HamburgerMenu")) != 0) {
				log.info("Clicking on hamburger icon");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				Thread.sleep(1300);

				if (driver
						.findElements(By
								.xpath("//android.widget.ImageView[@resource-id='com.google.android.gm.lite:id/avatar']"))
						.size() != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				}

				// scrollDown(2);
				// if (CommonFunctions.getSizeOfElements(driver,
				// commonElements.get("SettingsOption"))==0){
				// openMenuDrawer(driver);
				// scrollTo("Settings");
				CommonFunctions.scrollsToTitle(driver, "Settings");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("SettingsOption"));
				// }
				// scrollDown(1);
				// Thread.sleep(1200);
				// CommonFunctions.searchAndClickByXpath(driver,
				// commonElements.get("SettingsOption"));

			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to navigate to General Settings : Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate Trash folder
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToTrashFolder(AppiumDriver driver) throws Exception {
		try {
			openMenuDrawer(driver);
			log.info("Looking for the Trash option to display");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption")) != 0) {
				log.info("Trash option is displayed and clicking on it to navigate to trash folder");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BinOption"));
			} else {
				log.info("Scrolling down for the Trash option to be display");
				scrollDown(1);
				Thread.sleep(800);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("BinOption")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BinOption"));
					log.info("Trash folder option is displayed after scrolling and clicked on it");
				}
			}
		} catch (Exception e) {
			throw new Exception("Unable to navigate to trash folder " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the mails in trash folder present or not
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyTrashFolderForMails(AppiumDriver driver) throws Exception {
		try {
			log.info("Verifying the mails present in Trash folder");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptyTrashNow")) != 0) {
				log.info("EMPTY TRASH NOW button is displayed and there are few mails in the Trash folder");
			} else {
				log.info("No mails are present in Trash folder and hence few mails are deleting from inbox");
				deleteMails(driver);
				navigateToTrashFolder(driver);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to verify the Trash folder due to " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the mails in spam folder present or not
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifySpamFolderForMails(AppiumDriver driver, String emailId) throws Exception {
		try {
			log.info("Verifying the mails present in Spam folder");
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("EmptySpamnow")) != 0) {
				log.info("EMPTY SPAM NOW button is displayed and there are few mails in the Spam folder");
			} else {
				log.info("No mails are present in Spam folder and hence few mails are marking as spam from inbox");
				spamMails(driver, emailId);
				navigateToSpamFolder(driver);
			}
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to verify the Spam folder due to " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to navigate Trash folder
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void navigateToSpamFolder(AppiumDriver driver) throws Exception {
		try {
			openMenuDrawer(driver);
			/*
			 * log.info("Looking for the Spam option to display");
			 * if(CommonFunctions.getSizeOfElements(driver,
			 * commonElements.get("SpamOption"))!=0){ log.info(
			 * "Spam option is displayed and clicking on it to navigate to Spam folder"
			 * ); CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SpamOption")); }else{ log.info(
			 * "Scrolling down for the Spam option to be display");
			 * scrollDown(1); Thread.sleep(800);
			 * if(CommonFunctions.getSizeOfElements(driver,
			 * commonElements.get("SpamOption"))!=0){
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SpamOption")); log.info(
			 * "Spam folder option is displayed after scrolling and clicked on it"
			 * ); } }
			 */
			CommonFunctions.scrollsToTitle(driver, "Spam");
		} catch (Exception e) {
			throw new Exception("Unable to navigate to Spam folder " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the email with no signature
	 * 
	 * @param driver
	 * @param AccountA
	 * @param AccountC
	 * @param SubjectText
	 * @param ComposeText
	 * @param SignatureTextA
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyEmailNoSignature(AppiumDriver driver, String AccountA, String AccountC, String SubjectText,
			String ComposeText, String SignatureTextA) throws Exception {
		try {
			navigateToSettings(driver);
			log.info("Getting list of accounts");
			Thread.sleep(3000);
			List<WebElement> settingAccounts = driver.findElements(By.xpath(commonElements.get("SettingAccounts")));
			for (WebElement accounts : settingAccounts) {
				String accountName = accounts.getText();
				log.info("List of accounts displayed " + accountName);
				if (accountName.equals(AccountA)) {
					log.info("Setting the signature for the account " + accountName + " to Off");
					WebElement selectAccount = driver
							.findElement(By.xpath("//android.widget.TextView[@text='" + accountName + "']"));
					selectAccount.click();
					log.info("Clicked on account " + accountName);
					CommonFunctions.scrollToCellByTitleVerify(driver, "Mobile Signature");
					commonPage.waitForElementToBePresentByXpath(commonElements.get("Signature"));
					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("Signature")) != 0) {
						log.info("Signature label is found");
						if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SignatureNotSet")) != 0) {
							log.info("Signture is Not Set for the second account as expected");
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
						} else {
							log.info("Signature is set for the account " + accountName
									+ " and clearing the signature to Off");
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Signature"));
							Thread.sleep(1800);
							((AndroidElement) driver.findElement(By.xpath(commonElements.get("SignatureText"))))
									.clear();
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
						}
					}
				}
			}
			log.info("Signature is set to Off");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("BackButton"));
			Thread.sleep(4000);
			// inbox.sendEmailAndVerify(AccountA, AccountC, SubjectText,
			// ComposeText, SignatureTextA);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to verify the email with no signature " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verofy the account is linked in settings and unlink it
	 * 
	 * @param driver
	 * @param EmailAddress
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyLinkedAccountSettings(AppiumDriver driver, String EmailAddress) throws Exception {
		try {
			log.info("Verifying in settings page if any account is linked before");
			userAccount.navigateToSettings(driver);
			Thread.sleep(2500);
			log.info("Verifying accout is linked or not");
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("LinkedEmail").replace("Email", EmailAddress)) != 0) {
				log.info("Account is linked");
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("LinkedEmail").replace("Email", EmailAddress));
				Thread.sleep(2000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("UnlinkAccount")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("UnlinkAccount"));
					log.info("Clicked on the unlink account option");
				} else {
					// scrollDown(2);
					// CommonFunctions.searchAndClickByXpath(driver,
					// commonElements.get("UnlinkAccount"));
					CommonFunctions.scrollsToTitle(driver, "Unlink account");
					log.info("Clicked on the unlink account option");
				}
				log.info("Clicking on the radio button to delete the copied emails");
				CommonFunctions.searchAndClickById(driver, commonElements.get("DeleteCopiedEmails"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("UnlinkOption"));
				log.info("Clicked on the Unlink button");
				Thread.sleep(5000);
				driver.navigate().back();
				// CommonFunctions.searchAndClickByXpath(driver,
				// commonElements.get("BackButton"));
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("LinkedEmail").replace("Email", EmailAddress)) != 0) {

				throw new Exception("Account is not unlinked");
			} else {
				driver.navigate().back();
			}
			Thread.sleep(2000);
		} catch (Exception e) {
			throw new Exception("Unable to verify if any account is linked in settings " + e.getStackTrace());
		}
	}

	public void verifyTryGmailifyAccountAdded(AppiumDriver driver, String EmailAddress) throws Exception {
		try {
			verifyLinkedAccountSettings(driver, EmailAddress);
			log.info("Verifying the Imap account " + EmailAddress + " is added to Gmail App");
			// verifyGmailAccountIsPresent(EmailAddress);
			Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018
			//if (verifyGmailAccountIsPresent(EmailAddress) == false) { // Commented by Venkat on 7/12/2018
			if(!listOfConfiguredMailIds.contains(EmailAddress)){ // added by Venkat on 7/12/2018
				log.info("Imap account is not found and hence adding it");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
			} else {
				removeAccountIMAPIfExists(driver, EmailAddress);
				launchGmailApp(driver);
				// inbox.verifyGmailAppInitState(driver);
				userAccount.clickOnAccountSwitcher(driver);
			}
		} catch (Exception e) {
			throw new Exception("Unable to verify the account is added " + e.getLocalizedMessage());
		}
	}

	public void TryItOutGmailify(AppiumDriver driver) throws Exception {
		try {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("TryItNow"));
			Thread.sleep(1200);
		} catch (Exception e) {

		}
	}

	/**
	 * Method to verify the Imap account is added to Gmail app/if not account is
	 * added
	 * 
	 * @param driver
	 * @param EmailAddress
	 * @param EmailPwd
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyIMAPAccountAdded(AppiumDriver driver, String EmailAddress, String EmailPwd) throws Exception {
		driver = map.get(Thread.currentThread().getId());

		try {
			/*
			 * log.info("Verifying the imap account is linked / added");
			 * userAccount.verifyLinkedAccountSettings(driver, EmailAddress);
			 */ log.info("Verifying the Imap account " + EmailAddress + " is added to Gmail App");
			//boolean x = verifyGmailAccountIsPresent(EmailAddress); // Commented by Venkat on 7/12/2018
			Set<String> listOfConfiguredMailIds = getListOfConfiguredMailIdsNew(true); // added by Venkat on 7/12/2018	
			boolean x = (listOfConfiguredMailIds.isEmpty()) ? false : listOfConfiguredMailIds.contains(EmailAddress);// added by Venkat on 7/12/2018
			
			if (x == false) {
				log.info("Imap account is not found and hence adding it to app");
				Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAccountOption")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddAccountOption"));
				} else {

					if (driver
							.findElements(By
									.xpath("//android.widget.FrameLayout/android.widget.ListView[@resource-id='android:id/list']"))
							.size() == 0) {
						log.info("Account drawer is not opened hence tapping it again");
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
					}

					if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SwitchAccountDrawer")) != 0) {
						log.info("Now tap on add account");
					} else {
						CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
						CommonFunctions.searchAndClickById(driver, commonElements.get("AccountText"));
					}
					Thread.sleep(600);
					CommonFunctions.scrollsToTitle(driver, "Add account");
				}
				/*
				 * log.info("add outlook hotmail live account");
				 * addOutlookHotmailLiveAcct(driver, EmailAddress, EmailPwd);
				 */

				log.info("adding Other account using Other option");
				addingOtherAccount(driver, EmailAddress, EmailPwd);
				log.info("Verify account is present in the list");
				verifyGmailAccountIsPresent(EmailAddress);
				log.info("Imap email account is added");

			}else{ // else is added by Venkat
				Thread.sleep(1000);
				//driver.navigate().back();
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'"+EmailAddress+"')]");
			} // else is added by Venkat
		} catch (Exception e) {
			log.info(e.getLocalizedMessage());
			throw new Exception("unable to verify/add the account " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to select the account in settings page
	 * 
	 * @param driver
	 * @param EmailAccountAddress
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void selectAccountSettings(AppiumDriver driver, String EmailAccountAddress) throws Exception {
		try {
			Thread.sleep(3000);
			log.info("Selecting account to link");
			List<WebElement> settingAccounts = driver.findElements(By.xpath(commonElements.get("SettingAccounts")));
			for (WebElement accounts : settingAccounts) {
				String accountName = accounts.getText();
				log.info("List of accounts displayed " + accountName);

				if (accountName.equals(EmailAccountAddress)) {
					log.info("Clicking on the account " + accountName);
					CommonFunctions.searchAndClickByXpath(driver,
							commonElements.get("EmailSelect").replace("Email", EmailAccountAddress));
				}
			}
			Thread.sleep(2000);
		} catch (Exception e) {
			throw new Exception("Unable to select the account " + e.getLocalizedMessage());
		}
	}

	public void verifyLinkedAccount(AppiumDriver driver, String EmailAddress) throws Exception {
		try {
			Thread.sleep(3000);
			log.info("Verifying any account is linked to Imap account");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("LinkedEmail")) != 0) {
				log.info("Account is linked");
			} else {
				throw new Exception("Account is not linked");
			}
		} catch (Exception e) {
			throw new Exception("unble to verify the account is linked " + e.getLocalizedMessage());
		}
	}

	public void verifyAccountGmailified(AppiumDriver driver, String EmailAddress) throws Exception {
		try {
			Thread.sleep(3000);
			log.info("Verifying any account is linked to Imap account");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("LinkedEmail")) != 0) {
				log.info("Account is linked");
			} else {
				throw new Exception("Account is not linked");
			}
		} catch (Exception e) {
			throw new Exception("unble to verify the account is linked " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify account is linked
	 * 
	 * @param driver
	 * @param EmailAccountAddress
	 * @throws Exception
	 */
	public void verifyAccountIsLinked(AppiumDriver driver, String EmailAccountAddress) throws Exception {
		try {
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HamburgerMenu")) != 0) {
				commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				Thread.sleep(2000);
				scrollUp(2);
			}
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			}

			log.info("Clicking on Settings Option");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SettingsOption")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
				log.info("Clicked on Settings option");
			} else {
				
				 //scrollDown(1); Thread.sleep(1500);
				 //CommonFunctions.searchAndClickByXpath(driver,commonElements.get("SettingsOption"));
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HamburgerMenu")) != 0) {
					commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					Thread.sleep(2000);
					scrollUp(2);
				}
				 
				CommonFunctions.scrollsToTitle(driver, "Settings");
				log.info("Clicked on Settings option");
				Thread.sleep(800);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SettingsOption")) != 0) {
					log.info("General Setting is Displayed");

				} else {
					driver.navigate().back();
				}
			}
			verifyLinkedAccount(driver, EmailAccountAddress);
		} catch (Exception e) {
			throw new Exception("Unable to verify account is linked " + e.getStackTrace());
		}
	}

	/**
	 * Method to click on Link Account
	 * 
	 * @param driver
	 * @param EmailAccountAddress
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void clickLinkAccount(AppiumDriver driver, String EmailAccountAddress) throws Exception {
		try {
			selectAccountSettings(driver, EmailAccountAddress);
			log.info("Verifying for the Link Account option");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("LinkAccountLabel")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("LinkAccountLabel"));
				log.info("Clicked on Link Account option");
			}
		} catch (Exception e) {
			throw new Exception("Unable to link the account to Gmailify " + e.getStackTrace());
		}
	}

	/**
	 * Method to relogin the imap/pop3 account after changing the password
	 * 
	 * @param driver
	 * @param ImapPassword
	 * @throws Exception
	 */
	public void reLogin(AppiumDriver driver, String ImapPassword) throws Exception {
		try {
			log.info("Verifying the Log In page is displayed and enter the password");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("OutlookPassword"));
			commonPage.waitForElementToBePresentByXpath(commonElements.get("OutlookPassword"));
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OutlookPassword")) != 0) {
				log.info("Entering Password");
				Thread.sleep(2000);
				CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("OutlookPassword"), ImapPassword);
				log.info("Email Password entered");
			}

			driver.hideKeyboard();
			Thread.sleep(800);
			/*-- Before we have outlook account to gmailify, but that account is already linked to some other Account.class So the below is commented--*/
			/*
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SignInButtonOutlook"));
			 * 
			 * log.info("Clicked on SignIn Button"); Thread.sleep(3000);
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("AcceptOk"));
			 */

			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));

		} catch (Exception e) {
			throw new Exception("Unable to signIn " + e.getStackTrace());
		}
	}

	/**
	 * Method to link account for Gmailify
	 * 
	 * @param driver
	 * @param EmailAccount
	 * @param GmailAccount
	 * @param GmailPassword
	 * @throws Exception
	 */
	public void linkAccountGmailify(AppiumDriver driver, String EmailAccount, String GmailAccount, String GmailPassword,
			String ImapPassword) throws Exception {
		try {
			log.info("Linking account for Gmailify");
			//clickLinkAccount(driver, EmailAccount); //commented by Venkat on 14/12/2018

			//written by Venkat on 14/12/2018: start
			try{
				Thread.sleep(1000);
				clickLinkAccount(driver, EmailAccount);
				
			}catch(Exception e){
				scrollDown(1);
				clickLinkAccount(driver, EmailAccount);
			}
			//written by Venkat on 14/12/2018: end

			
			commonPage.waitForElementToBePresentByXpath(commonElements.get("GetStarted"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("GetStarted"));
			log.info("Clicked on Get Started");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("SetUpPage"));

			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OwnerAccount")) != 0) {
				log.info("Clicking on the Owner account to link the imap account");
				// CommonFunctions.searchAndClickByXpath(driver,
				// commonElements.get("OwnerAccount"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
			} else {
				log.info("Choosing a different account");
				commonPage.waitForElementToBePresentByXpath(commonElements.get("ChooseDiffGmailAddress"));
				log.info("Clicking on the choose a different Gmail address");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ChooseDiffGmailAddress"));
				Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver,
						"//android.widget.TextView[@text='gm.gig1.auto@gmail.com']") != 0) {
					CommonFunctions.searchAndClickByXpath(driver,
							"//android.widget.TextView[@text='gm.gig1.auto@gmail.com']");
				} else {

					commonPage.waitForElementToBePresentByXpath(commonElements.get("AddGmailMailbox"));
					log.info("Clicking on add another Gmail mailbox");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AddGmailMailbox"));

					commonPage.waitForElementToBePresentByXpath(commonElements.get("EmailorPhone"));
					log.info("Entering email for Gmailify");
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("EmailorPhone"), GmailAccount);
					log.info("Clicked on Next");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NextGmailify"));

					commonPage.waitForElementToBePresentByXpath(commonElements.get("PasswordGmailify"));
					log.info("Entering password");
					CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("PasswordGmailify"),
							GmailPassword);
					log.info("Clicked on Next after entering password");
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PasswordNext"));

					commonPage.waitForElementToBePresentByXpath(commonElements.get("AddingGoogleAccountAccpetButton"));
					log.info("Clicking on Accept button");
					CommonFunctions.searchAndClickByXpath(driver,
							commonElements.get("AddingGoogleAccountAccpetButton"));
				}
				// Google Service page
				commonPage.waitForElementToBePresentByXpath(commonElements.get("Next"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
			}
			log.info("Re Login into account");
			reLogin(driver, ImapPassword);

		} catch (Exception e) {
			throw new Exception("Unable to link account " + e.getStackTrace());
		}
	}

	/**
	 * Method to verify whether account is linked
	 * 
	 * @param driver
	 * @param EmailAccountAddress
	 * @throws Exception
	 */
	public void verifyGmailifyAccount(AppiumDriver driver, String EmailAccountAddress) throws Exception {
		try {
			openMenuDrawer(driver);
			log.info("Verifying the account is linked to Gmailify");
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			} else {
				scrollUp(1);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				}
			}
			verifyAccountIsLinked(driver, EmailAccountAddress);

			driver.navigate().back();
		} catch (Exception e) {
			throw new Exception("Unable to verify the account is linked " + e.getStackTrace());
		}
	}

	/**
	 * Method to change the linked Gmalify account password from notification
	 * 
	 * @param driver
	 * @param OutlookNewPassword
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void changePasswordFromNotification(AppiumDriver driver, String OutlookNewPassword) throws Exception {
		try {
			((AndroidDriver) driver).openNotifications();
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AuthError"));
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("OutlookPassword"));
			CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("OutlookPassword"), OutlookNewPassword);
			log.info("Entering the Outlook password");
			driver.hideKeyboard();
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SignInButtonOutlook"));
		} catch (Exception e) {
			throw new Exception("Unable to change password using the notification " + e.getStackTrace());
		}
	}

	/**
	 * Method to enable/disable open web links in Gmail option
	 * 
	 * @param driver
	 * @param check
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void openWebLinksGmailOption(AppiumDriver driver, boolean check) throws Exception {
		try {

			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenWebLinks")) != 0) {
				log.info("Open web links in Gmail Option is found");
			} else {
				scrollDown(1);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenWebLinks")) != 0) {
					log.info("Open web links in Gmail Option is found after scrolling");
				}
			}
			inbox.scrollDown(1);
			Thread.sleep(3000);
			String checkBox = CommonFunctions.searchAndGetAttributeOnElementByXpath(driver,
					commonElements.get("OpenWebLinksCheckboxValue"), "checked");
			log.info(checkBox);
			Boolean checkBoxValue = Boolean.valueOf(checkBox);
			if (check == true && checkBoxValue == true) {
				log.info("Open Web Links Check box value is checked");
			} else if (check == false && checkBoxValue == true) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenWebLinksCheckboxValue"));
				log.info("Option is Unchecked");
			} else if (check == true && checkBoxValue == false) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenWebLinksCheckboxValue"));
			}
			driver.navigate().back();
			log.info("Navigating to Primary folder page");
			driver.navigate().back();
		} catch (Exception e) {
			throw new Exception("Unable to verify enabling/disabling the option " + e.getStackTrace());
		}
	}

	/**
	 * Method to verify the add account and manage account options are available
	 * 
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifyAddAccountManageAccount(AppiumDriver driver) throws Exception {
		try {
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AllIndoxesOption")) != 0) {
				driver.navigate().back(); // written by Venkat on 20/11/2018
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				log.info("Account Drawer is displayed");
			} else {
				log.info("Switched to Account Switcher");
			}
			
			
			WebElement accountSelected;
			try {
				accountSelected = driver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_name') and @index='1']"));
			} catch (Exception e) {
				 accountSelected = driver.findElement(By.xpath("//android.widget.TextView[@index='0']")); // Venkat
			}
			String selectedAccount = accountSelected.getText();
			

			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAccountOption")) == 0) {
				scrollDown(1);
			}
			scrollDown(2);
			
			commonPage.waitForElementToBePresentByXpath(commonElements.get("AddAccountOption"));
			log.info("Verified Add account option in the account switcher");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("ManageAccountOption"));
			Thread.sleep(1000);
/*			
			WebElement accountSelected;
			try {
				accountSelected = driver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_name') and @index='1'] "));
			} catch (Exception e) {
				 accountSelected = driver.findElement(By.xpath("//android.widget.TextView[@index='0']")); // Venkat
			}
			String selectedAccount = accountSelected.getText();
			
*/			scrollUp(3);
			//WebElement secondAccount;
			try {
				//secondAccount = driver.findElement(By.xpath("//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_name') and @index='2'] "));
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@resource-id,'com.google.android.gm:id/account_name') and @index='1']");
			} catch (Exception e) {
				//secondAccount = driver.findElement(By.xpath("//android.widget.TextView[@index='2']")); // Venkat
				CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@index='1']");
			}
			//String secondAccountText = accountSelected.getText();
			Thread.sleep(1000);
			if(!CommonFunctions.isAccountAlreadySelected(selectedAccount))
			userAccount.switchMailAccount(driver, selectedAccount);
			//driver.navigate().back(); // written by Venkat on 20/11/2018
			log.info("Verified Manage account option in the account switcher");
		} catch (Exception e) {
			throw new Exception("Unable to verify the options" + e.getStackTrace());
		}
	}

	/**
	 * Method to verify the smart reply suggestions enabled/disabled
	 * 
	 * @param driver
	 * @param check
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void verifySmartReplyEnabled(AppiumDriver driver, boolean check) throws Exception {
//		try {

			CommonFunctions.scrollToCellByTitle(driver, "Smart Reply");
			String text = "Smart Reply";
			AndroidElement si = null;
			log.info("Checking for smart reply option enabled/disabled");
			Thread.sleep(2000);
			for (int i = 2; i <= 10; i++) {
				log.info("In loop");
				String settingOption = driver
						.findElement(By.xpath("//android.widget.LinearLayout[@index='" + i
								+ "']/android.widget.RelativeLayout[@index='0']"
								+ "/android.widget.TextView[@resource-id='android:id/title']"))
						.getText();
				log.info("Settings option's text: "+settingOption);
				if (settingOption.equalsIgnoreCase(text)) {
					si = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='" + i
							+ "']/android.widget.LinearLayout[@index='1']" + "/android.widget.CheckBox[@index='0']"));
					if (si.getAttribute("checked").equalsIgnoreCase("false")) {
						log.info("Checkbox is not checked");
						si.click();
						log.info("Checkbox is now checked");
					} else {
						log.info("Checkbox already checked");
						break;
					}

				}
			}
			// commonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReply"));

			// String checkBox =
			// driver.findElement(By.xpath("//android.widget.LinearLayout[@index='5']/android.widget.LinearLayout[@index='1']/android.widget.CheckBox[@index='0']")).getAttribute("checked");
			/*
			 * String checkBox =
			 * CommonFunctions.searchAndGetAttributeOnElementByXpath(driver,
			 * commonElements.get("SmartReplyCheckBox"), "checked");
			 * log.info(checkBox); Boolean checkBoxValue =
			 * Boolean.valueOf(checkBox); if(check == true && checkBoxValue ==
			 * true){ log.info("Smart Reply Check box value is checked"); }else
			 * if(check == false && checkBoxValue == true){
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SmartReplyCheckBox")); log.info(
			 * "Option is Unchecked"); }else if(check == true && checkBoxValue
			 * == false){ CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SmartReplyCheckBox")); }
			 */ log.info("Smart Reply checkbox is enabled");
			driver.navigate().back();
			driver.navigate().back();
		/*} catch (Exception e) {
			throw new Exception("Unable to verify the smart reply enabled/diabled");
		}*/
	}

	public void deleteMailExistPrimary(AppiumDriver driver, String EmailSubject) throws Exception {
		try {
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("EmailSubjectText").replace("Subject", EmailSubject)) != 0) {
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("EmailSubjectText").replace("Subject", EmailSubject));
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
				Thread.sleep(2000);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk")) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
				}
				MailManager.DeleteAllMailsTrash();
			}
			MailManager.DeleteAllMailsTrash();
		} catch (Exception e) {
			throw new Exception("Unable to delete mail with selected subject " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify the smart reply suggestions are not displayed after mail
	 * replied
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifySmartReplySuggestionsNotDisplayed(AppiumDriver driver) throws Exception {
		try {
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SmartReplyView")) == 0) {
				log.info("Smart Reply Suggestions are not displayed after reply");
			} else {
				throw new Exception("Smart reply suggestions are displayed after mail reply");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to verify the smart reply suggestions are displayed after mail
	 * discarded
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifySmartReplySuggestionsDisplayed(AppiumDriver driver) throws Exception {
		try {
			CommonPage.waitForElementToBePresentByXpath(commonElements.get("SmartReplyView"));
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SmartReplyView")) != 0) {
				log.info("Smart Reply Suggestions are displayed after mail discarded");
			}
		} catch (Exception e) {
			throw new Exception("Smart reply suggestions are not displayed after mail discarded");
		}
	}

	/**
	 * Method to call and exeucte the adb command while doing parallel execution
	 * 
	 * @param driver
	 * @throws Exception
	 * @author Phaneendra
	 */
	public void setLocale(AppiumDriver driver) throws Exception {
		try {
			String[] setLocaleLanguage = new String[] { "adb", "shell", "am", "start", "-n",
					"com.android.settings/.LanguageSettings" };
			String[] setLocaleLanguage1 = new String[] {};
			log.info("Executing adb commands");
			adbExecuteCommand(setLocaleLanguage, setLocaleLanguage1);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to set Locale Language to preferred one
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void setLanguageADB(AppiumDriver driver, String Language) throws Exception {
		try {
			Process p;
			ProcessBuilder pb;
			String[] setLocaleLanguage = new String[] { "adb", "shell", "am", "start", "-n",
					"com.android.settings/.LanguageSettings" };
			String[] setLocaleLanguage1 = new String[] {};
			log.info("Executing adb commands");
			adbExecuteCommand(setLocaleLanguage, setLocaleLanguage1);

			String[] installAdbLang = new String[] { "adb", "install", "-r", "-d", path + java.io.File.separator
					+ "AdbChangeLangApk" + java.io.File.separator + "ADBChangeLanguage.apk" };
			p = new ProcessBuilder(installAdbLang).start();
			Thread.sleep(15000);

			String[] perm = new String[] { "adb", "shell", "pm", "grant",
					"net.sanapeli.adbchangelanguage android.permission.CHANGE_CONFIGURATION" };
			p = new ProcessBuilder(perm).start();
			Thread.sleep(1000);

			if (Language.equalsIgnoreCase("Chinese")) {

				String[] chinese = new String[] { "adb", "shell", "am", "start", "-n",
						"net.sanapeli.adbchangelanguage/.AdbChangeLanguage", "-e language zh-rTW" };
				p = new ProcessBuilder(chinese).start();
			} else if (Language.equalsIgnoreCase("French")) {
				String[] French = new String[] { "adb", "shell", "am", "start", "-n",
						"net.sanapeli.adbchangelanguage/.AdbChangeLanguage", "-e language fr" };
				p = new ProcessBuilder(French).start();
			} else if (Language.equalsIgnoreCase("Hebrew")) {
				String[] Hebrew = new String[] { "adb", "shell", "am", "start", "-n",
						"net.sanapeli.adbchangelanguage/.AdbChangeLanguage", "-e language iw" };
				p = new ProcessBuilder(Hebrew).start();
			} else if (Language.equalsIgnoreCase("English")) {
				String[] English = new String[] { "adb", "shell", "am", "start", "-n",
						"net.sanapeli.adbchangelanguage/.AdbChangeLanguage", "-e language en" };
				p = new ProcessBuilder(English).start();
			}
			Thread.sleep(2000);
		} catch (NoSuchElementException e) {

		}
	}

	/**
	 * Method to set Locale Language to preferred one
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void setLanguageToLocale(AppiumDriver driver, String Locale) throws Exception {
		try {
			setLocale(driver);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("ChineseLanguageActionBar")) == 0) {
				log.info("Preferred language is not set, and hence setting it up");
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("LanguageOption"));
				Thread.sleep(4000);
				if (CommonFunctions.getSizeOfElements(driver, Locale) != 0) {
					CommonFunctions.searchAndClickByXpath(driver, Locale);
				} else {
					scrollDown(5);
					Thread.sleep(4000);
					CommonFunctions.searchAndClickByXpath(driver, Locale);
					log.info("Chinese language is set");
				}
			} /*
				 * else if(CommonFunctions.getSizeOfElements(driver,
				 * commonElements.get("EnglishLanguage"))==0){
				 * CommonFunctions.searchAndClickByXpath(driver,
				 * commonElements.get("LanguageOption")); Thread.sleep(4000);
				 * if(CommonFunctions.getSizeOfElements(driver, Locale)!=0){
				 * CommonFunctions.searchAndClickByXpath(driver, Locale); }else{
				 * scrollDown(1); Thread.sleep(4000);
				 * CommonFunctions.searchAndClickByXpath(driver, Locale);
				 * log.info("English language is set"); } }
				 */
		} catch (NoSuchElementException e) {
			// throw new Exception("Unable to set locale language
			// "+e.getLocalizedMessage());
			scrollDown(1);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ChineseLanguage"));
		}
	}

	public void startGmailApp(AppiumDriver driver) throws Exception {
		try {
			((AndroidDriver) driver).startActivity(appPackage, appActivity);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to upgrade the Gmail Go apk
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyGmailAppVersionUpgrade(AppiumDriver driver) throws Exception {
		log.info("In method for getting current gmail app version");
		Map<String, String> verifyUpgradeOfGmailAppData = commonPage.getTestData("verifySettingsOption1");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		Thread.sleep(2000);
		scrollDown(2);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		 //Added by Bindu on 26/07
		 waitForElementToBePresentByXpath(commonElements.get("HelpAndFeedbackCloseIcon"));
		
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		String versionBeforeUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionBeforeUpgrading);
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
		
		navigateToSettings(driver);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EtouchTestOneDrive"));
		
		// need further development
		
		
		
		/*
		 * commonPage.launchPlayStoreApp(driver);
		 * commonPage.verifyAppIsUpdated(driver,verifyUpgradeOfGmailAppData.get(
		 * "SearchKeyword") );
		 */
		Process p;
		ProcessBuilder pb;
		log.info("Upgrading the gmail apk");
		String[] launchOldApk = new String[] { "adb", "install", "-r", "-d",
				"path" + java.io.File.separator + "Gmail_Apk" + java.io.File.separator + "Gmail_fishfood_signed.apk" };
		p = new ProcessBuilder(launchOldApk).start();

		String[] clearGmailCache = new String[] { "adb", "shell", "pm", "clear", "com.google.android.gm" };
		Thread.sleep(30000);
		log.info("Launching Gmail app");
		String[] launchGmail = new String[] { "adb", "shell", "am", "start", "-n",
				"com.google.android.gm/.ConversationListActivityGmail" };
		pb = new ProcessBuilder(launchGmail);
		p = pb.start();
		Thread.sleep(2000);

		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		log.info("App launched");
		// commonFunction.searchAndClickById(driver,
		// commonElements.get("SearchIcon"));
		Thread.sleep(2000);
		scrollDown(2);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		String versionAfterUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionAfterUpgrading);
		if (versionAfterUpgrading.equals(versionBeforeUpgrading)) {
			throw new Exception("############ Upgrading the gmail app is not performed #############");
		} else {
			log.info("^^^^^^^^^^^^^^ Version got updated ^^^^^^^^^^^^^^^^");
		}
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
	}

	public void verifyGmailAppVersionUpgradeGmailGo(AppiumDriver driver) throws Exception {
		log.info("In method for getting current gmail go app version");
		Map<String, String> verifyUpgradeOfGmailAppData = commonPage.getTestData("verifySettingsOption1");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		Thread.sleep(2000);
		log.info("scrolling to help and feedback");
		scrollTo("Help & feedback");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		log.info("Clicked on help and feedback");
		   //Added below line by Bindu on 26/07
		   waitForElementToBePresentByXpath(commonElements.get("HelpAndFeedbackCloseIcon"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		log.info("Getting version info");
		String versionBeforeUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionBeforeUpgrading);
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
		
		
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		
		
		/*
		 * commonPage.launchPlayStoreApp(driver);
		 * commonPage.verifyAppIsUpdated(driver,verifyUpgradeOfGmailAppData.get(
		 * "SearchKeyword") );
		 */
		Process p;
		ProcessBuilder pb;
		log.info("Upgrading the gmail go apk");
		String[] launchOldApk = new String[] { "adb", "install", "-r", "-d",
				"E:/Gmail-POC-GmailTest_Automation/GmailPOC/Go_Apk/Gmail_go_releaseUpgrade.apk" };
		p = new ProcessBuilder(launchOldApk).start();

		String[] clearGmailCache = new String[] { "adb", "shell", "pm", "clear", "com.google.android.gm,lite" };
		Thread.sleep(30000);
		log.info("Launching Gmail app");
		String[] launchGmail = new String[] { "adb", "shell", "am", "start", "-n",
				"com.google.android.gm.lite/com.google.android.gm.ConversationListActivityGmail" };
		pb = new ProcessBuilder(launchGmail);
		p = pb.start();
		Thread.sleep(2000);
		log.info("Clicking on hamburger");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		log.info("App launched");
		// commonFunction.searchAndClickById(driver,
		// commonElements.get("SearchIcon"));
		Thread.sleep(2000);
		scrollTo("Help & feedback");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		String versionAfterUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionAfterUpgrading);
		if (versionAfterUpgrading.equals(versionBeforeUpgrading)) {
			throw new Exception("############ Upgrading the gmail app is not performed #############");
		} else {
			log.info("^^^^^^^^^^^^^^ Version got updated ^^^^^^^^^^^^^^^^");
		}
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
	}

	/**
	 * Method to verify version of Gmail app is downgraded
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyGmailAppVersionDowngradeGmailGo(AppiumDriver driver) throws Exception {
		try {
			log.info("In method for getting current gmail go version");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			Thread.sleep(2000);
			// commonPage.scrollDown(2);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HelpAndFeedBackOption")) == 0) {
				// CommonFunctions.searchAndClickByXpath(driver,
				// commonElements.get("ExpandAccountList"));
				log.info("Scrolling down to help and feedback");
				CommonFunctions.scrollTo(driver, "Help & feedback");
			}
			Thread.sleep(1000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
			Thread.sleep(2000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
			String versionBeforeDowngrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("VersionInfoDetails"));
			log.info(versionBeforeDowngrading);
			commonPage.navigateBack(driver);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
			log.info("Downgrading Gmail Apk from " + versionBeforeDowngrading + " to older version");
			Process p;
			ProcessBuilder pb;
			String[] launchOldApk = new String[] { "adb", "install", "-r", "-d",
					"E:/Gmail-POC-GmailTest_Automation/GmailPOC/Go_Apk/Gmail_go_release.apk" };
			p = new ProcessBuilder(launchOldApk).start();
			log.info("Downgraded the gmail apk");
			String[] clearGmailCache = new String[] { "adb", "shell", "pm", "clear", "com.google.android.gm.lite" };
			Thread.sleep(30000);
			log.info("Launching Gmail app");
			String[] launchGmail = new String[] { "adb", "shell", "am", "start", "-n",
					"com.google.android.gm.lite/com.google.android.gm.ConversationListActivityGmail" };
			pb = new ProcessBuilder(launchGmail);
			p = pb.start();
			Thread.sleep(2000);
			// inbox.openMenuDrawer(driver);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			Thread.sleep(2000);
			scrollTo("Help & feedback");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
			Thread.sleep(2000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
			String versionAfterDowngrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("VersionInfoDetails"));
			log.info(versionAfterDowngrading);
			if (versionAfterDowngrading.contains(versionBeforeDowngrading)) {
				throw new Exception(
						"############ Downgrade the gmail app is not performed check the app is already is downgraded/crashed#############");
			}
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
		} catch (Exception e) {

		}
	}

	/**
	 * Method to verify version of Gmail app is downgraded
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyGmailAppVersionDowngrade(AppiumDriver driver) throws Exception {
		try {
			log.info("In method for getting current gmail app version");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			Thread.sleep(2000);
			commonPage.scrollDown(2);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("HelpAndFeedBackOption")) == 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
				scrollDown(1);
			}
			Thread.sleep(1000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
			Thread.sleep(2000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
			String versionBeforeDowngrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("VersionInfoDetails"));
			log.info(versionBeforeDowngrading);
			commonPage.navigateBack(driver);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
			log.info("Downgrading Gmail Apk from " + versionBeforeDowngrading + " to older version");
			Process p;
			ProcessBuilder pb;
			String[] launchOldApk = new String[] { "adb", "install", "-r", "-d", path + java.io.File.separator
					+ "Gmail_Apk" + java.io.File.separator + "Gmail_fishfoodDowngrade.apk" };
			p = new ProcessBuilder(launchOldApk).start();
			log.info("Downgraded the gmail apk");
			String[] clearGmailCache = new String[] { "adb", "shell", "pm", "clear", "com.google.android.gm" };
			Thread.sleep(30000);
			log.info("Launching Gmail app");
			String[] launchGmail = new String[] { "adb", "shell", "am", "start", "-n",
					"com.google.android.gm/.ConversationListActivityGmail" };
			pb = new ProcessBuilder(launchGmail);
			p = pb.start();
			Thread.sleep(2000);
			// inbox.openMenuDrawer(driver);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			Thread.sleep(2000);
			scrollTo("Help & feedback");
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
			Thread.sleep(2000);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
			String versionAfterDowngrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("VersionInfoDetails"));
			log.info(versionAfterDowngrading);
			if (versionAfterDowngrading.equals(versionBeforeDowngrading)) {
				throw new Exception(
						"############ Downgrade the gmail app is not performed check the app is already is downgraded/crashed#############");
			}
			driver.navigate().back();
			commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
		} catch (Exception e) {

		}
	}

	public void selectFilesDownloads(AppiumDriver driver) throws Exception {
		try {
			Thread.sleep(2000);
			org.openqa.selenium.Dimension size = driver.manage().window().getSize();
			log.info(size);
			int ht = size.getHeight() / 2;
			int wt = size.getWidth() / 2;
			log.info(ht + " " + wt);

			WebElement e = driver.findElement(By.xpath(commonElements.get("DocFile")));
			TouchAction a = new TouchAction(driver);
			a.longPress(e).waitAction(2000).perform();
			a.longPress(e).waitAction(1000).moveTo(232, 709).release().perform();

		} catch (Exception e) {
			throw new Exception("Unable to select file " + e.getLocalizedMessage());
		}
	}

	public void adjustMultiWindow(AppiumDriver driver) throws Exception {
		try {
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("DownloadsApp")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DownloadsApp"));
			}
			Thread.sleep(3000);
		} catch (Exception e) {
			throw new Exception("Unable to adjust the multi window " + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to verify App is installed or not
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyAppIsInstalled(AppiumDriver driver, String docType) throws Exception {
		log.info("verify App Is Installed or not");
		if (commonFunction.getSizeOfElements(driver, commonElements.get("PlayStoreSearchField")) != 0) {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchField"));
		} else {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchIcon"));
		}
		commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("PlayStoreSeachFieldText"), docType);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchSuggetion"));

		if (commonFunction.getSizeOfElements(driver, commonElements.get("PlayStoreUnInstallButton")) != 0) {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreUnInstallButton"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
			waitForElementToBePresentByXpath(commonElements.get("PlayStoreInstallButton"));
			Thread.sleep(2000);
			// commonFunction.searchAndClickByXpath(driver,
			// commonElements.get("DocsViewCloseIcon"));
		}
		/*
		 * else if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("PlayStoreUnInstallButton"))==0){
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "PlayStoreInstallButton") );
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("PlayStoreAcceptButton")); Thread.sleep(5000);
		 * //waitForElementToBePresentByXpath(commonElements.get(
		 * "PlayStoreInstallingMessage"));
		 * waitForElementToBePresentByXpath(commonElements.get(
		 * "PlayStoreOpenButton"));
		 * 
		 * }
		 */
		driver.navigate().back();
		// commonFunction.searchAndClickByXpath(driver,
		// commonElements.get("NavigateBackButton"));
	}

	/**
	 * Method to verify App is installed or not
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyAppIsInstalledOrNot(AppiumDriver driver, String docType) throws Exception {
		log.info("verify App Is Installed or not");
		if (commonFunction.getSizeOfElements(driver, commonElements.get("PlayStoreSearchField")) != 0) {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchField"));
		} else {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchIcon"));
		}
		commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("PlayStoreSeachFieldText"), docType);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchSuggetion"));
		log.info("verify install button");
		if (commonFunction.getSizeOfElements(driver, commonElements.get("PlayStoreInstallButton")) != 0) {
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreInstallButton"));
			// commonFunction.searchAndClickByXpath(driver,
			// commonElements.get("PlayStoreAcceptButton"));
			Thread.sleep(30000);
			// waitForElementToBePresentByXpath(commonElements.get("PlayStoreInstallingMessage"));
			waitForElementToBePresentByXpath(commonElements.get("PlayStoreOpenButton"));

		}
		// commonFunction.searchAndClickByXpath(driver,
		// commonElements.get("NavigateBackButton"));
		driver.navigate().back();
	}

	/**
	 * Method to verify general settings options
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void verifyGeneralSettingsOptions(AppiumDriver driver) throws Exception {
		try {
			log.info("In verify General Settings Options method");
			SoftAssert softAssert = new SoftAssert();
			Map<String, String> verifyGeneralSettingsOptionsData = commonPage.getTestData("verifySettingsOption1");
			commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
			/*
			 * log.info(
			 * "********************Gmail Default Action***********************"
			 * ); commonPage.verifyGmailDefaultActionSettingsOption(driver);
			 */ log.info("********************Conversation View***************************");
			commonPage.verifyGeneralSettingsConversationViewOption(driver);
			/*
			 * log.info(
			 * "********************Swipe Actions***************************");
			 * commonPage.verifyGeneralSettingsSwipeActionOption(driver);
			 */ log.info("********************SenderImage Actions***************************");
			commonPage.verifyGeneralSettingsSenderImageOption(driver);
			log.info("********************ReplyAction Actions***************************");
			commonPage.verifyDefaultReplyActionOption(driver);
			// commonPage.verifyGeneralSettingsAutoFitOption(driver);
			log.info("********************AutoAdvance Actions***************************");
			commonPage.verifyGeneralSettingsAutoAdvanceOption(driver);
			if (commonFunction.isElementByXpathNotDisplayed(driver,
					commonElements.get("GeneralSettingsOpenWebLinkOption"))) {
				commonFunction.scrollDown(driver, 1);
			}
			/*
			 * navigateToGeneralSettings(driver);
			 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
			 * "GeneralSettings"));
			 */ // *Commented the below code as the Open web links checkbox is
				// not available now*//
			/*
			 * commonPage.verifyGeneralSettingsOpenWebLinkOption(driver);
			 * 
			 * inbox.navigateUntilHamburgerIsPresent(driver);
			 * navigateToGeneralSettings(driver);
			 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
			 * "GeneralSettings"));
			 * 
			 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
			 * "GeneralSettingOpenWebLinksCheckBox"));
			 */

			log.info("******Confirm Deleting, Archive, Send Option**********");
			commonPage.verifyGeneralSettingsDeleteArchiveSendOption(driver);
			driver.navigate().back();
			driver.navigate().back();
			/*
			 * commonPage.verifyGeneralSettingsConfirmDeleteOption(driver);
			 * log.info("******Confirm Archive Option**********");
			 * commonPage.verifyGeneralSettingsConfirmArchivingOption(driver);
			 * log.info("******Confirm Sending Option**********");
			 * commonPage.verifyGeneralSettingsConfirmSendingOption(driver);
			 */ } catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to verify general settings options
	 * 
	 * @author batchi
	 * @throws Exception
	 *//*
		 * public void verifyGeneralSettingsOptions(AppiumDriver driver)throws
		 * Exception{ log.info("In verify General Settings Options method");
		 * SoftAssert softAssert = new SoftAssert(); Map<String, String>
		 * verifyGeneralSettingsOptionsData = commonPage
		 * .getTestData("verifySettingsOption1");
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * if(CommonFunctions.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"))!=0){ log.info(
		 * "******************* Gmail Default Action Option is displayed ****************"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("GeneralSettingsGMailDefaultActionArchive"))!=0){
		 * log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Default Action Archive Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultActionArchive"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * 
		 * //
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("mailSubject")); inbox.swipeToDeleteOrArchiveMail(driver,
		 * verifyGeneralSettingsOptionsData.get("mailSubject")); log.info(
		 * "Swiped for Archive option");
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("SwipeUndo")); Thread.sleep(2000); }
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("GeneralSettingsGMailDefaultDelete"))!=0){
		 * log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Default Action Delete Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingsGMailDefaultDelete"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * 
		 * //
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("mailSubject")); inbox.swipeToDeleteOrArchiveMail(driver,
		 * verifyGeneralSettingsOptionsData.get("mailSubject")); log.info(
		 * "Swiped for Delete option");
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("SwipeUndo"));
		 * 
		 * } // commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("NotificationCancelOption")); }
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultActionArchive"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsConversationViewOption"))!=0){ log.info(
		 * "******************* Gmail Conversation View Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConversationViewCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingConversationViewCheckBox"));
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingConversationViewCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * verifyAccountsExists(driver,
		 * verifyGeneralSettingsOptionsData.get("RediffAccount"),
		 * verifyGeneralSettingsOptionsData.get("ImapPwd"));
		 * switchMailAccount(driver,
		 * verifyGeneralSettingsOptionsData.get("RediffAccount"));
		 * inbox.composeAndSendNewMail(driver,
		 * verifyGeneralSettingsOptionsData.get("SwitchAccountId"),
		 * verifyGeneralSettingsOptionsData.get("CC"),
		 * verifyGeneralSettingsOptionsData.get("BCC"),
		 * verifyGeneralSettingsOptionsData.get("MailLink"),
		 * verifyGeneralSettingsOptionsData.get("ComposeBody"));
		 * Thread.sleep(2000); switchMailAccount(driver,
		 * verifyGeneralSettingsOptionsData.get("SwitchAccountId"));
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("MailLink") ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("ReplyIcon"));
		 * commonFunction.searchAndSendKeysByXpath(driver,
		 * commonElements.get("ComposeBody"),
		 * verifyGeneralSettingsOptionsData.get("ReplyComposeBody"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("SendButton"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * switchMailAccount(driver,verifyGeneralSettingsOptionsData.get(
		 * "RediffAccount"));
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("MailLink") ); String replyText=
		 * commonFunction.searchAndGetTextOnElementById(driver,
		 * commonElements.get("SubjectInCV")); if(replyText.contains("Re")){
		 * log.info(
		 * "***************************** Individual Mails are displayed *****************************"
		 * ); commonPage.navigateUntilHamburgerIsPresent(driver);
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * navigateToGeneralSettings(driver);
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConversationViewCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver); }else{ throw new
		 * Exception("Conversation view checkbox is enabled" );
		 * 
		 * }
		 * 
		 * }
		 * 
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsSwipeActionOption"))!=0){ log.info(
		 * "******************* Gmail Swipe Action Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingSwipeActionsCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingSwipeActionsCheckBox"));
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingSwipeActionsCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.verifySwipe(driver,verifyGeneralSettingsOptionsData.get(
		 * "mailSubject"));
		 * 
		 * } navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsSenderImageOption"))!=0){ log.info(
		 * "******************* Gmail Sender Image Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingSenderImageCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingSenderImageCheckBox"));
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingSenderImageCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * 
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("SenderImageIcon"))==0){ throw new Exception(
		 * "Sender  Image icon is enabled" ); }else{ log.info(
		 * "******************* Sender Image Icon is diabled *********************"
		 * ); throw new Exception("Sender Image icon is enabled" ); }
		 * 
		 * } navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "DefaultReplyAction"))!=0){ log.info(
		 * "******************* Gmail Default Reply Action Option is displayed ****************"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "DefaultReplyAction")); if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("ReplyDefaultReplyAction"))!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Default Reply Action Reply Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("ReplyDefaultReplyAction"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("mailSubject") );
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "ReplyIcon"))!=0){
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "ReplyIcon") ); log.info("Clicked on Reply icon");
		 * if(commonFunction.searchAndGetTextOnElementByXpath(driver,
		 * commonElements.get("ReplyPage")).equalsIgnoreCase("Reply")){
		 * log.info("Reply Page is displayed");
		 * 
		 * }else{ throw new Exception("Reply Page should be available" ); }
		 * 
		 * }else{
		 * 
		 * throw new Exception("Reply icon is not available" ); }
		 * 
		 * String element=
		 * driver.findElement(By.xpath(commonElements.get("ReplyIcon"))).
		 * getAttribute("content-desc"); log.info(element);
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "ReplyIcon"))!=0){ log.info("GetSizeOff");
		 * if(element.equalsIgnoreCase("Reply")){ log.info(
		 * "Reply Icon is enabled");
		 * 
		 * }else{ throw new Exception("Reply action should be available" ); }
		 * if(element.equalsIgnoreCase("ReplyAll")){ log.info(
		 * "ReplyAll Icon is enabled");
		 * 
		 * }else{ throw new Exception("ReplyAll action should be available" ); }
		 * 
		 * }else{
		 * 
		 * throw new Exception("Reply icon is not available" ); }
		 * 
		 * }
		 * 
		 * commonPage.navigateUntilHamburgerIsPresent(driver); log.info(
		 * "Navigated to Hamburger");
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * navigateToGeneralSettings(driver);
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "DefaultReplyAction"));
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "ReplyAllDefaultReplyAction"))!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Default Reply Action ReplyAll Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("ReplyAllDefaultReplyAction"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("mailSubject") );
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "ReplyAllIconInCV"))!=0){
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "ReplyAllIconInCV") ); log.info("Clicked on ReplyAll icon");
		 * if(commonFunction.searchAndGetTextOnElementByXpath(driver,
		 * commonElements.get("ReplyAllPage")).equalsIgnoreCase("Reply all")){
		 * log.info("ReplyAll Page is displayed"); }else{ throw new Exception(
		 * "ReplyAll Page should be available" ); } }else{
		 * 
		 * throw new Exception("Reply icon is not available" ); } } //
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("NotificationCancelOption"));
		 * 
		 * }navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * 
		 * //used youtube mails in Social inbox option.
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsAutoFitOption"))!=0){ log.info(
		 * "******************* Gmail Auto Fit Messages Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingAutoFitMessageCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingAutoFitMessageCheckBox"));
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingAutoFitMessageCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * //commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * 
		 * switchMailAccount(driver,
		 * verifyGeneralSettingsOptionsData.get("ToMailId"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "SocialInboxOption"))!=0){ commonFunction.scrollUp(driver, 2); }
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("SocialInboxOption"));
		 * inbox.openMailUsingSearch(driver,
		 * verifyGeneralSettingsOptionsData.get("AutoFitSubject"));
		 * 
		 * softAssert.assertTrue(commonPage.getScreenshotAndCompareImage(
		 * "verifyGeneralSettingsOptions"),
		 * "Image comparison for verifyGeneralSettingsOptions");
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("PrimaryInboxOption"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("HamburgerMenu"));
		 * navigateToGeneralSettings(driver);
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingAutoFitMessageCheckBox")); }
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsAutoAdvanceOption"))!=0){ log.info(
		 * "******************* Gmail Auto Advance Option is displayed ****************"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsAutoAdvanceOption"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("GeneralSettingAutoAdvanceNewerOption"))!=0){
		 * log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Auto Advance Messages Newer Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingAutoAdvanceNewerOption") );
		 * inbox.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMail(driver,verifyGeneralSettingsOptionsData.get(
		 * "mailSubTwo") ); // inbox.openMailUsingSearch(driver,
		 * verifyGeneralSettingsOptionsData.get("mailSubTwo"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("MailToolBarDeleteIcon")); String strOne=
		 * commonFunction.searchAndGetTextOnElementByXpath(driver,commonElements
		 * .get("SubjectLine")); log.info(strOne);
		 * if(strOne.contains(verifyGeneralSettingsOptionsData.get("mailSubOne")
		 * )){ log.info("**********  After Deleting, CV of Newer Mail "
		 * +verifyGeneralSettingsOptionsData.get("mailSubOne") +
		 * " is displayed **********");
		 * 
		 * }else{ throw new Exception("Newer Mail is not available" );
		 * 
		 * }
		 * 
		 * } commonPage.navigateBack(driver); navigateToGeneralSettings(driver);
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsAutoAdvanceOption"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("GeneralSettingAutoAdvanceOlderOption"))!=0){
		 * log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Auto Fit Messages Older Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingAutoAdvanceOlderOption"));
		 * inbox.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMail(driver,verifyGeneralSettingsOptionsData.get(
		 * "mailSubOne") ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("MailToolBarDeleteIcon")); String strTwo=
		 * commonFunction.searchAndGetTextOnElementByXpath(driver,commonElements
		 * .get("SubjectLine")); log.info(strTwo);
		 * if(strTwo.contains(verifyGeneralSettingsOptionsData.get(
		 * "mailSubThree"))){ log.info(
		 * "**********  After Deleting, CV of Older Mail "
		 * +verifyGeneralSettingsOptionsData.get("mailSubThree") +
		 * " is displayed **********");
		 * 
		 * }else{ throw new Exception("Older Mail is not available" );
		 * 
		 * }
		 * 
		 * } commonPage.navigateBack(driver); navigateToGeneralSettings(driver);
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsAutoAdvanceOption"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("GeneralSettingAutoAdvanceConversationlistOption")
		 * )!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@ Gmail Auto Fit Messages Conversationlist Option is displayed @@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingAutoAdvanceConversationlistOption") );
		 * inbox.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMail(driver,verifyGeneralSettingsOptionsData.get(
		 * "mailSubFour") ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("MailToolBarDeleteIcon"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("PrimaryInboxOption"))!=0){ log.info(
		 * "**********  After Deleting Thread List view of  "
		 * +commonFunction.searchAndGetTextOnElementByXpath(driver,
		 * commonElements.get("PrimaryInboxOption")) +" is displayed **********"
		 * );
		 * 
		 * }else{ throw new Exception("Older Mail is not available" );
		 * 
		 * } }
		 * 
		 * } navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.isElementByXpathNotDisplayed(driver,
		 * commonElements.get("GeneralSettingsOpenWebLinkOption"))){
		 * commonFunction.scrollDown(driver,1);
		 * 
		 * } navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsOpenWebLinkOption"))!=0){ log.info(
		 * "******************* Gmail Open Web Links in Gmail Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingOpenWebLinksCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingOpenWebLinksCheckBox"));
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingOpenWebLinksCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("MailLink")); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("LinkInBodyText"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("ChromeNewTabButton"))==0){ log.info(
		 * "^^^^^^^^^^^^^^^^^^^^^^^ Link opened in Web Browser, Open Link in Gmail is disabled ^^^^^^^^^^^^^^^^^^^^^^^^^^"
		 * ); commonFunction.navigateBack(driver);
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * navigateToGeneralSettings(driver);
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingOpenWebLinksCheckBox")); } else{
		 * 
		 * throw new Exception("Open Link in Gmail is enabled" ); }
		 * 
		 * } else{
		 * 
		 * throw new Exception("Open Link in Gmail option is not displayed" ); }
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsConfirmDeleteOption"))!=0){ log.info(
		 * "******************* Gmail Confirm Delete Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get
		 * ("mailSubject") ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("MailToolBarDeleteIcon"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("DeleteConversationDialog"))!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@@ Delete Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("Cancel")); log.info("Clicked on Cancel button");
		 * 
		 * }else{ throw new Exception(
		 * "Delete Conversation Dialog is not available" );
		 * 
		 * }
		 * 
		 * }else{ throw new Exception("Confirm Delete Option is not available"
		 * );
		 * 
		 * }navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsConfirmArchivingOption"))!=0){ log.info(
		 * "******************* Gmail Confirm Archiving Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirArchivingCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingConfirArchivingCheckBox"));
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirArchivingCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.openMail(driver,verifyGeneralSettingsOptionsData.get(
		 * "mailSubject") ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("MailToolBarArchiveIcon"));
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("ArchiveConversationDialog"))!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@@ Archive Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("Cancel")); log.info("Clicked on Cancel button");
		 * 
		 * inbox.verifyIfCheckboxIsUnChecked(driver,
		 * commonElements.get("GeneralSettingConfirArchivingCheckBox"));
		 * 
		 * }else{ throw new Exception(
		 * "Archive Conversation Dialog is not available" );
		 * 
		 * }
		 * 
		 * }else{ throw new Exception("Confirm Archive Option is not available"
		 * );
		 * 
		 * }
		 * 
		 * navigateToGeneralSettings(driver);
		 * commonPage.waitForElementToBePresentByXpath(commonElements.get(
		 * "GeneralSettings"));
		 * commonFunction.searchAndClickByXpath(driver,commonElements.get(
		 * "GeneralSettingsGMailDefaultAction"));
		 * 
		 * if(commonFunction.getSizeOfElements(driver,commonElements.get(
		 * "GeneralSettingsConfirmSendingOption"))!=0){ log.info(
		 * "******************* Gmail Confirm Sending Option is displayed ****************"
		 * ); inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirmSendingCheckBox"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("GeneralSettingConfirmSendingCheckBox"));
		 * inbox.verifyIfCheckboxIsChecked(driver,
		 * commonElements.get("GeneralSettingConfirmSendingCheckBox"));
		 * commonPage.navigateUntilHamburgerIsPresent(driver);
		 * inbox.composeAsNewMail(driver,
		 * verifyGeneralSettingsOptionsData.get("SwitchAccountId"),
		 * verifyGeneralSettingsOptionsData.get("CC"),
		 * verifyGeneralSettingsOptionsData.get("BCC"),
		 * verifyGeneralSettingsOptionsData.get("mailSubject"),
		 * verifyGeneralSettingsOptionsData.get("ComposeBody"));
		 * commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("SendButton")); Thread.sleep(2000);
		 * if(commonFunction.getSizeOfElements(driver,
		 * commonElements.get("SendConversationDialog"))!=0){ log.info(
		 * "@@@@@@@@@@@@@@@@@@@@@ Send Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@"
		 * ); commonFunction.searchAndClickByXpath(driver,
		 * commonElements.get("Cancel")); log.info("Clicked on Cancel button");
		 * 
		 * }else{ throw new Exception(
		 * "Send Conversation Dialog is not available" );
		 * 
		 * }
		 * 
		 * }else{ throw new Exception("Confirm Sending Option is not available"
		 * );
		 * 
		 * } }
		 */
	/**
	 * Method to open user account settings
	 * 
	 * @author batchi
	 * @param verifyAdvertisment
	 * @throws Exception
	 */
	public void userSpecificSystemLabelSetting(AppiumDriver driver, String user, String label, String syncOption)
			throws Exception {
		log.info("Opening User Specific Setting for disabling Forums system label");
		try {

			openMenuDrawer(driver);
			scrollDown(2);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SettingsOption"));
			Thread.sleep(5000);
			log.info("Opening user account settings for user: " + user);
			commonFunction.searchAndClickByXpath(driver,
					commonElements.get("AccountSpecificSystemLabelSetting").replace("accName", user));
			// commonFunction.searchAndClickByXpath(driver,commonElements.get("InboxCategory"));
			// checkForumsLabelCheckbox(driver);
			if (commonFunction.isElementByXpathDisplayed(driver, commonElements.get("ManageLabelsOption")) == false) {
				log.info("Element is not displayed ");
				scrollDown(1);
				commonFunction.searchAndClickByXpath(driver, commonElements.get("ManageLabelsOption"));
				commonFunction.searchAndClickByXpath(driver, commonElements.get("ForumsInboxOption"));
				commonFunction.searchAndClickByXpath(driver, commonElements.get("SyncMessage"));
				log.info("Clicked on Sync Messages");
				if (commonFunction.searchAndGetTextOnElementByXpath(driver, commonElements.get("SyncForumsNoneOption"))
						.equalsIgnoreCase(syncOption)) {

					commonFunction.searchAndClickByXpath(driver, commonElements.get("SyncForumsNoneOption"));
					log.info("Clicked on None option");
				} else if (commonFunction
						.searchAndGetTextOnElementByXpath(driver, commonElements.get("SyncForumsLast30DaysOption"))
						.equalsIgnoreCase(syncOption)) {

					commonFunction.searchAndClickByXpath(driver, commonElements.get("SyncForumsLast30DaysOption"));
					log.info("Clicked on Last 30days option");
				} else {
					log.info("Element is displayed");
				}
				// commonPage.navigateUntilHamburgerIsPresent(driver);
			}

			log.info("Back to Home  page of Gmail app");
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to openUserSpecificSetting:::" + e.getLocalizedMessage());
		}

	}

	/**
	 * Method to set notifications Off
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void settingNotificationOff(AppiumDriver driver, String user) throws Exception {
		/*
		 * log.info("clicking Navigation Drawer");
		 * commonFunction.searchAndClickByXpath(driver2,commonElements.get(
		 * "HamburgerMenu"));
		 */
		navigateToSettings(driver);
		//commonFunction.searchAndClickByXpath(driver,commonElements.get("AccountSpecificSystemLabelSetting").replace("accName", user)); //commented by Venkat on 18/12/2018
		
		//written by Venkat on 18/12/2018: start
		try{
			Thread.sleep(2000);
			//commonFunction.searchAndClickByXpath(driver,commonElements.get("AccountSpecificSystemLabelSetting").replace("accName", user));
			driver.findElement(By.xpath(commonElements.get("AccountSpecificSystemLabelSetting").replace("accName", user))).click();
		}catch(Exception e){
			scrollDown(1);
			commonFunction.searchAndClickByXpath(driver,commonElements.get("AccountSpecificSystemLabelSetting").replace("accName", user));
		}
		//written by Venkat on 18/12/2018: end

		
		log.info("Opened user account settings for user: " + user);
		// driver2.findElement(By.xpath("//android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[@text='Notifications']"));
		// log.info("Notifications found");
		// inbox.verifyIfCheckboxIsUnChecked(driver2,
		// commonElements.get("NotificationCheckbox"));
		// commonPage.navigateUntilHamburgerIsPresent(driver2);
		Thread.sleep(1000);
		// CommonFunctions.scrollsToTitle(driver, "Notifications");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NotificationsSettings"));
		Thread.sleep(1000);
		CommonFunctions.scrollsToTitle(driver, "None");

		Thread.sleep(1000);
		driver.navigate().back();
		driver.navigate().back();
		Thread.sleep(1500);
		// commonFunction.searchAndClickByXpath(driver2,commonElements.get("HamburgerMenu"));

	}

	public void settingNotificationOn(AppiumDriver driver, String user) throws Exception {
		Thread.sleep(1000);
		CommonFunctions.scrollToCellByTitleVerify(driver, "Notifications");
		Thread.sleep(1000);
		try {
			driver.findElement(By.xpath("//android.widget.TextView[@text='Turn sync Gmail off?']"));
			Thread.sleep(1000);
			driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
		} catch (Exception e) {
			log.error("Exception :"+e);
		}
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NotificationsSettings"));
		Thread.sleep(1000);
		CommonFunctions.scrollsToTitle(driver, "All");

		Thread.sleep(1000);

	}

	/**
	 * Method to check Forums Label check box
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void checkForumsLabelNotificationCheckbox(AppiumDriver driver) throws Exception {
		try {

			log.info("Checking forums label notification checkbox");
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("LabelNotificationOption"))) {
				inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("LabelNotificationOption"));
				log.info("Navigating  back");
				// commonPage.navigateUntilHamburgerIsPresent(driver);
				log.info("Navigated to Home page");
			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Failed to check notification checkbox : Due to :" + e.getLocalizedMessage());
		}
	}

	public void removeAccountPOP3(AppiumDriver driver, String Email) throws Exception {
		try {
			String emailIDXpath = commonElements.get("GoogleAccountListedOptions").replace("Emailid", Email);

			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			CommonFunctions.scrollTo(driver, "Manage Accounts");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
			// commonPage.waitForElementToBePresentByXpath(commonElements.get(""));
			Thread.sleep(5000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccountPop3"));
			scrollDown(1);
			CommonFunctions.searchAndClickByXpath(driver, emailIDXpath);
			// CommonFunctions.searchAndClickByXpath(driver, xpath);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
			driver.navigate().back();
			driver.navigate().back();
			// driver.navigate().back();
			// driver.navigate().back();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to delete account " + Email + " Due to :" + e.getLocalizedMessage());
		}
	}

	/**
	 * Method to perform the actions to delete the account
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void deleteAccount(AppiumDriver driver) throws Exception {
		try {
			log.info("Performing action to delete the account");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
		} catch (Exception e) {
			throw new Exception("Unable to delete the account");
		}
	}

	/**
	 * Verifying the general settings options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyGeneralSettingsDisplayed(AppiumDriver driver) throws Exception {
		try {
			commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsGMailDefaultAction")) != 0) {
				log.info("Gmail default action");
			} else {
				throw new Exception("Unable to find Gmail default action");
			}

			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsConversationViewOption")) != 0) {
				log.info("Conversation view");
			} else {
				throw new Exception("Unable to find Conversation view");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsSwipeActionOption")) != 0) {
				log.info("Swipe actions");
			} else {
				throw new Exception("Unable to find Swipe actions");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsSenderImageOption")) != 0) {
				log.info("Sender Image option");
			} else {
				throw new Exception("Unable to find Sender image option");
			}
			scrollDown(1);
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsDefaultReplyAction")) != 0) {
				log.info("Default reply action");
			} else {
				throw new Exception("Unable to find Default reply action");
			}
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("GeneralSettingsAutoFitOption")) != 0) {
				log.info("Auto fit option");
			} else {
				throw new Exception("Unable to find Auto fit option");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsAutoAdvanceOption")) != 0) {
				log.info("auto advance option");
			} else {
				throw new Exception("Unable to find auto advance option");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsOpenWebLinkOption")) != 0) {
				log.info("Open web links option");
			} else {
				throw new Exception("Unable to find open web links option");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsConfirmDeleteOption")) != 0) {
				log.info("Confirm before deleting");
			} else {
				throw new Exception("Unable to find Confirm before deleting");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsConfirmArchivingOption")) != 0) {
				log.info("Confirm before archiving");
			} else {
				throw new Exception("Unable to find Confirm before archiving");
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("GeneralSettingsConfirmSendingOption")) != 0) {
				log.info("Confirm before sending");
			} else {
				throw new Exception("Unable to find Confirm before sending");
			}

		} catch (Exception e) {
			throw new Exception("General setting option is not displayed");
		}
	}

	/**
	 * Method to remove the IMAP and POP3 accounts
	 * 
	 * @param driver
	 * @param EmailOutlook
	 * @param EmailYahoo
	 * @param ExchangeEmail
	 * @param OtherEmailIMAP
	 * @param OtherEmailPop3
	 * @throws Exception
	 */
	
	public void removeAccountIMAPPOP3(AppiumDriver driver, String EmailOutlook, String EmailYahoo, String ExchangeEmail,String OtherEmailIMAP, String OtherEmailPop3) throws Exception {

		Thread.sleep(1200);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("AccountIconNew")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
			Thread.sleep(1200);
			WebElement element = driver.findElement(
					By.xpath("//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']"));
			org.openqa.selenium.Dimension windowSize = driver.manage().window().getSize();
			int x = element.getLocation().x;
			int y = element.getLocation().y;
			log.info("In UA.removeAccountIMAPPOP3 X coordinates " + x + " Y coordinates " + y);
			// driver.swipe((windowSize.width) - 20, (y + 50), 80, (y + 50),
			// 1000);
			driver.swipe(x, y, ((windowSize.width) - 20), y - 50, 1000);
			Thread.sleep(2000);
			scrollDown(1);
			Thread.sleep(1000);
			//Copied from Bindhu code on 30/01/2018: starts
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ManageAccountsOnDevice"))!=0){
			CommonFunctions.scrollsToTitle(driver, "Manage accounts on this device");
			}else{
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountsOnDevice"));
			}
			//Copied from Bindhu code on 30/01/2018: end
		} else {
			openMenuDrawer(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			CommonFunctions.scrollTo(driver, "Manage Accounts");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));
		}
		log.info("Verifying if account exists IMAP");
		Thread.sleep(3000);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OtherAccountsIMAP")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccountsIMAP"));
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("OutlookEmail").replace("Email", EmailOutlook)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OutlookEmail").replace("Email", EmailOutlook));
				deleteAccount(driver);
				log.info("Account deleted " + EmailOutlook);
				Thread.sleep(2000);
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("OutlookEmail").replace("Email", EmailYahoo)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OutlookEmail").replace("Email", EmailYahoo));
				deleteAccount(driver);
				log.info("Account deleted " + EmailYahoo);
				Thread.sleep(2000);
			}
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("OutlookEmail").replace("Email", OtherEmailIMAP)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OutlookEmail").replace("Email", OtherEmailIMAP));
				deleteAccount(driver);
				log.info("Account deleted " + OtherEmailIMAP);
				Thread.sleep(2000);
			}
			/*
			 * if(CommonFunctions.isElementByXpathDisplayed(driver,
			 * commonElements.get("OtherAccountsIMAP"))){
			 * driver.navigate().back(); driver.navigate().back(); }
			 */

		}
		log.info("No Imap account added");
		Thread.sleep(1000);
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OtherExchangeaccount")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherExchangeaccount"));
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("OutlookEmail").replace("Email", ExchangeEmail)) != 0) {
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OutlookEmail").replace("Email", ExchangeEmail));
				deleteAccount(driver);
				log.info("Account deleted " + ExchangeEmail);
				Thread.sleep(2000);
			}
		}
		log.info("No Exchange account added");
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OtherAccountPop3")) != 0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccountPop3"));
			Thread.sleep(1000);
			if (CommonFunctions.getSizeOfElements(driver,
					commonElements.get("OutlookEmail").replace("Email", OtherEmailPop3)) != 0) {
				CommonFunctions.searchAndClickByXpath(driver,
						commonElements.get("OutlookEmail").replace("Email", OtherEmailPop3));
				deleteAccount(driver);
				log.info("Account deleted " + OtherEmailPop3);
				Thread.sleep(2000);
			}
		}
		log.info("No Pop3 account added");
		driver.navigate().back();

	}

	public void removeAccountIMAPPOP3New(AppiumDriver driver, String outlookEmailId, String yahooEmailId, String exchangeEmailId, String otherEmailId, String otherEmailId1) throws Exception{
		if(BaseTest.isAccountSwitcherExist){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
			Thread.sleep(1000);
			CommonFunctions.scrollTo(driver, "Manage accounts on this device");  
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Manage accounts on this device')]");  
		}else{
			openMenuDrawerNew(driver);
			CommonFunctions.scrollTo(driver, "Settings");  
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Settings')]");
			Thread.sleep(500);
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@content-desc='More options'] | //android.widget.ImageView[@text='More options']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Manage accounts')]");
		}

		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OtherExchangeaccount")) != 0) {
			removeIMAPAccount(driver,outlookEmailId,yahooEmailId,exchangeEmailId,otherEmailId,otherEmailId1);
		}
	}
	
	public void goToAccountsListPage(AppiumDriver driver)throws Exception{
		if(BaseTest.isAccountSwitcherExist){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AccountIconNew"));
			Thread.sleep(1000);
		}else{
			openMenuDrawerNew(driver);
			CommonFunctions.scrollTo(driver, "Settings");  
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Settings')]");
			Thread.sleep(500);
		}
	}

	
	public void goToGenalSettings(AppiumDriver driver)throws Exception{
		//openMenuDrawerNew(driver);
		//CommonFunctions.scrollTo(driver, "Settings");  
		goToSpecificLable(driver, "Settings");
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Settings')]");
		Thread.sleep(1000);
		int count = 0;
		try {
			while(count++ <=3){
				driver.findElement(By.xpath("//android.widget.TextView[@text='General settings']")).click();
				break;
			}
		} catch (Exception e) {
			scrollUp(1);
		}
	}
	
	
	
	public void openMenuDrawerNew(AppiumDriver driver) throws Exception{
		commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		Thread.sleep(1000);
/*		
		int counter = 0;
		while(counter++ <= 10){
			try{
				Thread.sleep(500);
				CommonFunctions.scrollToCellByTitle(driver, "All inboxes");
				break;
			}catch(Exception e){
				scrollUp(1);
				continue;
			}
		}
*/		
	}
	

	private void addIMAPAccount(AppiumDriver driver) throws Exception{
		driver.findElement(By.xpath("//android.widget.TextView[@text='Add account']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//android.widget.TextView[@text='Personal (IMAP)']")).click();
		Thread.sleep(500);
	}
	private void removeIMAPAccount(AppiumDriver driver, String outlookEmailId, String yahooEmailId, String exchangeEmailId, String otherEmailId, String otherEmailId1) throws Exception {
		try{
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OtherAccountsIMAP"));
			if (CommonFunctions.getSizeOfElements(driver,commonElements.get("OutlookEmail").replace("Email", outlookEmailId)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,commonElements.get("OutlookEmail").replace("Email", outlookEmailId));
				deleteAccount(driver);
				log.info("Account deleted " + outlookEmailId);
				Thread.sleep(2000);
			}
			if (CommonFunctions.getSizeOfElements(driver,commonElements.get("OutlookEmail").replace("Email", yahooEmailId)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,commonElements.get("OutlookEmail").replace("Email", yahooEmailId));
				deleteAccount(driver);
				log.info("Account deleted " + yahooEmailId);
				Thread.sleep(2000);
			}
			if (CommonFunctions.getSizeOfElements(driver,commonElements.get("OutlookEmail").replace("Email", exchangeEmailId)) != 0) {
				Thread.sleep(2000);
				CommonFunctions.searchAndClickByXpath(driver,commonElements.get("OutlookEmail").replace("Email", exchangeEmailId));
				deleteAccount(driver);
				log.info("Account deleted " + exchangeEmailId);
				Thread.sleep(2000);
			}
		}catch(Exception e){
			
		}
	}

	/**
	 * Method to removeAccountNonGmail
	 * 
	 * @param emailID
	 * @throws Exception
	 */
	public void removeAccountNonGmail(AppiumDriver driver, String emailID, String xpath) throws Exception {
		try {
			String emailIDXpath = commonElements.get("GoogleAccountListedOptions").replace("Emailid", emailID);

			//openMenuDrawer(driver); // Commented by Venkat on 26/12/2018
			Thread.sleep(1000);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			//CommonFunctions.scrollTo(driver, "Manage Accounts"); // Commented by Venkat on 26/12/2018
			CommonFunctions.scrollTo(driver, "Manage accounts on this device"); // added by Venkat on 26/12/2018 
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ManageAccountOption"));  // Commented by Venkat on 26/12/2018
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Manage accounts on this device')]"); // added by Venkat on 26/12/2018 
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Personal (IMAP)')]"); // added by Venkat on 26/12/2018 
			//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveNonGoogleAccountOption")); // Commented by Venkat on 26/12/2018
			//scrollDown(1);
			CommonFunctions.searchAndClickByXpath(driver, emailIDXpath);
			//CommonFunctions.searchAndClickByXpath(driver, xpath); // Commented by Venkat on 26/12/2018
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountMoreOptions"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("RemoveAccountPopUpListOption"));
			CommonFunctions.searchAndClickById(driver, commonElements.get("ConfirmRemoveAccountButtonID"));
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to delete account " + emailID + " Due to :" + e.getLocalizedMessage());
		}
	}

	public void verifyAccountSettings(AppiumDriver driver) throws Exception {
		try {

			Map<String, String> navigateToSettings = commonPage.getTestData("navigateToSettings");

			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxType"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("DefautInbox"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxCatagories"));
			Thread.sleep(2000);
			String checkSocial = driver.findElement(By.xpath(navigateToSettings.get("Social"))).getAttribute("checked");
			if (checkSocial.equalsIgnoreCase("true")) {
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Social"));
				log.info("Checked Social");
			}
			String checkPromotions = driver.findElement(By.xpath(navigateToSettings.get("Promotions")))
					.getAttribute("checked");
			if (checkPromotions.equalsIgnoreCase("true")) {
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Promotions"));
				log.info("Checked Promotions");
			}
			String checkUpdates = driver.findElement(By.xpath(navigateToSettings.get("Updates")))
					.getAttribute("checked");
			if (checkUpdates.equalsIgnoreCase("true")) {
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Updates"));
				log.info("Checked Updates");
			}
			String checkForums = driver.findElement(By.xpath(navigateToSettings.get("Forums"))).getAttribute("checked");
			if (checkForums.equalsIgnoreCase("true")) {
				CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Forums"));
				log.info("Checked  Forums");
			}
			Thread.sleep(800);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			commonPage.waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			// openMenuDrawer(driver);
			log.info("Menu drawer is opened");
			Thread.sleep(1500);
			// if()
			// CommonFunctions.scrollUp(driver, 3);
			Thread.sleep(1000);
			scrollTo("All inboxes");
			Thread.sleep(1000);
			try {
				if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("SocialLabel")) == 0) {
					log.info("Checked Social is not displayed");

				}
				if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("PromotionsLabel")) == 0) {
					log.info("Checked Promotions is not  displayed");

				}
				if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("UpdatesLabel")) == 0) {
					log.info("Checked Updates is not displayed");

				}
				if (CommonFunctions.getSizeOfElements(driver, navigateToSettings.get("ForumsLabel")) == 0) {
					log.info("Checked Forums  is not displayed");

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			/*
			 * CommonFunctions.scrollTo(driver, "Settings");
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SettingsOption"));
			 */
			CommonFunctions.scrollsToTitle(driver, "Settings");
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("etouchtestone"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("InboxCatagories"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Social"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Promotions"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Updates"));
			CommonFunctions.searchAndClickByXpath(driver, navigateToSettings.get("Forums"));
			Thread.sleep(800);
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			// Thread.sleep(1500);
			pullToReferesh(driver);
			Thread.sleep(5000);
			pullToReferesh(driver);

			openMenuDrawer(driver);
			// CommonFunctions.searchAndClickByXpath(driver,
			// commonElements.get("HamburgerMenu"));
			Thread.sleep(1000);
			scrollTo("All inboxes");
			CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("SocialLabel"));
			log.info("Checked Social is displayed");
			CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("PromotionsLabel"));
			log.info("Checked Promotions is displayed");
			CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("UpdatesLabel"));
			log.info("Checked Updates isdisplayed");
			CommonFunctions.isElementByXpathDisplayed(driver, navigateToSettings.get("ForumsLabel"));
			log.info("Checked Forums  is  displyed");

			// driver.navigate().back();
			/*
			 * CommonFunctions.scrollTo(driver, "Settings");
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SettingsOption"));
			 */
			CommonFunctions.scrollsToTitle(driver, "Settings");
			inbox.ManageLabels(driver);
			driver.navigate().back();
			// inbox.navigateUntilHamburgerIsPresent(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to set default reply action to Reply All
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void setDefaultReplyActionToReplyAll(AppiumDriver driver2) throws Exception {
		log.info("Tapping on Menu drawer > Settings > General settings");
		navigateToGeneralSettings(driver2);
		log.info("Tapping on Default reply action ");
		CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("DefaultReplyAction"));
		log.info("Selecting Delete and navigating back");
		CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ReplyAllDefaultReplyAction"));
		navigateBack(driver2);
		navigateBack(driver2);

	}

	/**
	 * Method to set default reply action to Reply
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public void setDefaultReplyActionToReply(AppiumDriver driver2) throws Exception {
		log.info("Tapping on Menu drawer > Settings > General settings");
		navigateToGeneralSettings(driver2);
		log.info("Tapping on Default reply action ");
		CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("DefaultReplyAction"));
		log.info("Selecting Delete and navigating back");
		CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ReplyDefaultReplyAction"));
		navigateBack(driver2);
		navigateBack(driver2);

	}

	public void EnableSyncOnOFf(AppiumDriver driver) throws Exception {
		try {
			log.info("In UA.EnableSyncOnOFf Enabling sync gmail on and off");
			Thread.sleep(2000);
			scrollTo("Sync Gmail");

			CommonFunctions.scrollToCellByTitle(driver, "Sync Gmail");
			String text = "Sync Gmail";
			AndroidElement si = null;
			log.info("In UA.EnableSyncOnOFf Checking for Sync Gmail option enabled/disabled");
			for (int i = 1; i <= 16; i++) {
				if (i % 2 != 0) {
					String settingOption = driver
							.findElement(By.xpath("//android.widget.LinearLayout[@index='" + i
									+ "']/android.widget.RelativeLayout[@index='0']"
									+ "/android.widget.TextView[@resource-id='android:id/title']"))
							.getText();
					if (settingOption.equalsIgnoreCase(text)) {
						si = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='" + i
								+ "']/android.widget.LinearLayout[@index='1']"
								+ "/android.widget.CheckBox[@index='0']"));
						if (si.getAttribute("checked").equalsIgnoreCase("false")) {
							log.info("In UA.EnableSyncOnOFf Checkbox is not checked");
							si.click();
							log.info("In UA.EnableSyncOnOFf Checkbox is now checked");
						} else {
							log.info("In UA.EnableSyncOnOFf Checkbox already checked");
							CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
							Thread.sleep(1000);
		        			try {
		        				driver.findElement(By.xpath("//android.widget.TextView[@text='Turn sync Gmail off?']"));
		        				Thread.sleep(1000);
		        				driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
		        			} catch (Exception e) {
		        				log.error("Exception :"+e);
		        			}
							CommonFunctions.scrollsToTitle(driver, "Sync Gmail");
							break;
						}

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to switch account and reply to all members in the list
	 * 
	 * @author batchi
	 * @param MailSubject,
	 *            emailId, inputKeys
	 * @throws Exception
	 */
	public void switchAccountAndReplyAll(AppiumDriver driver, String emailId, String mailSubject, String inputKeys)
			throws Exception {
		try {
			log.info("in switch account and reply all");
			// Thread.sleep(3000);
			// waitForElementToBePresentByXpath(commonElements.get("HamburgerMenu"));
			log.info(emailId);
			inbox.openMailUsingSearch(driver, mailSubject);
			if (commonFunction.getSizeOfElements(driver, commonElements.get("ReplyAllButton")) == 0) {
				log.info("In  verify the ReplyAll  button");
				// commonPage.scrollTo(commonElements.get("ReplyAllButton"));
				commonFunction.scrollDown(driver, 4);
				Thread.sleep(2000);

			}
			commonFunction.searchAndClickByXpath(driver, commonElements.get("ReplyAllButton"));
			commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), inputKeys);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("SendButton"));
			commonPage.navigateUntilHamburgerIsPresent(driver);

		} catch (Exception e) {
			throw new Exception("Unable to switch account " + e.getLocalizedMessage());
		}
	}

	public void switchMailAccountDrive(AppiumDriver driver, String account) throws Exception {
		String accountListButton = commonElements.get("ExpandAccountList");

		try {
			/*
			 * int count = 0; while
			 * (CommonFunctions.isElementByXpathNotDisplayed(driver,
			 * accountListButton)==true){ scrollUp(2); count++; if(count>10)
			 * break; }
			 */
			log.info("Verifying account displayed");
			Thread.sleep(1000);
			String accountDisplayed = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("AccountListCommonXpath"));
			log.info("Account selected in drive " + accountDisplayed);
			if (accountDisplayed.equalsIgnoreCase(account)) {
				driver.navigate().back();
			} else {
				log.info("Desired account is not selected and hence selecting it");
				CommonFunctions.searchAndClickByXpath(driver, accountListButton);
				Thread.sleep(1500);
				if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SharedWithMeDrive")) != 0) {
					log.info("Accounts not displayed and hence clicking on account list again");
					CommonFunctions.searchAndClickByXpath(driver, accountListButton);
					driver.findElement(By.xpath("//android.widget.TextView[@text='" + account + "']")).click();
					Thread.sleep(2500);
				} else {
					log.info("Accounts displayed ");
					// CommonFunctions.searchAndClickByXpath(driver, account);
					driver.findElement(By.xpath("//android.widget.TextView[@text='" + account + "']")).click();
					Thread.sleep(2500);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to switch accounts in drive");
		}
	}

	/**
	 * Turning off/on the sync settings
	 * 
	 */
	public void verifyTurningOffOnSync(AppiumDriver driver, boolean value) throws Exception {
		log.info("UA verify Sync off/On starts");
		Map<String, String> verifyAutoSync = commonPage.getTestData("AutoSync");
		try {

/*			
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
			if (value == true) {
			 if (CommonFunctions.isElementByXpathDisplayed(driver,verifyAutoSync.get("AutoSyncData"))) {
			  CommonFunctions.searchAndClickByXpath(driver,
			   verifyAutoSync.get("AutoSyncData"));
			  Thread.sleep(600);
			  if (CommonFunctions.isElementByXpathDisplayed(driver,
			    verifyAutoSync.get("AutoSyncDataOff"))) {
			   log.info(
			    "Autosync is On and checking it Off");
			   CommonFunctions.searchAndClickByXpath(driver,
			    commonElements.get("AcceptOk"));
			  } else
			  if (CommonFunctions.isElementByXpathDisplayed(driver,
			    verifyAutoSync.get("AutoSyncDataOn"))) {
			   log.info(
			    "Autosync is Off and checking it On");
			   CommonFunctions.searchAndClickByXpath(driver,
			    commonElements.get("AcceptOk"));
			  }
			 }
			}
*/			
			if (CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("AutoSyncAlert")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("TurnOn"));
			} else {
				log.info("UA b4 Manage accounts on this device..");
				try{
					// added by Venkat on 21/11/2018 : starts
					scrollTo("Manage accounts on this device");
					CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Manage accounts on this device']");
					// added by Venkat on 21/11/2018 : end
				}catch(Exception e){
					log.error("Unable to scroll to Manage accounts on this device");
				}
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
				Thread.sleep(600);
				CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("AutoSyncData"));
				Thread.sleep(600);
				if (value == true) {
					log.info("Value is true and hence setting the auto sync to ON");
					if (CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("AutoSyncDataOn")) != 0) {
						log.info("Already the sync is OFF and turning to ON");
						Thread.sleep(600);
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					} else {
						log.info("Sync is already ON");
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Cancel"));
					}
				} else {
					log.info("Value is false and hence setting the auto sync to OFF");
					if (CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("AutoSyncDataOff")) != 0) {
						log.info("Already the sync is ON and turning to OFF");
						Thread.sleep(600);
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
					} else {
						log.info("Sync is already OFF");
						CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Cancel"));
					}
				}
				driver.navigate().back();
			}
			log.info("UA verify Sync off/On end..");
		} catch (Exception e) {
			throw new Exception("");
		}
	}

	public void verifyAutoSyncMsg(AppiumDriver driver) throws Exception {
		Map<String, String> verifyAutoSync = commonPage.getTestData("AutoSync");
//Copied from Bindhu code on 30/01/2018: starts
		userAccount.launchGmailApp(driver);
		Thread.sleep(1000);
		//Copied from Bindhu code on 30/01/2018: end
		try {
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsg"));
			if (CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("AutoSyncMsg")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("AutoSyncMsg"));
			}
			log.info("Verifying the auto sync message");
			if (CommonFunctions.getSizeOfElements(driver, verifyAutoSync.get("ChangeSyncSettings")) != 0) {
				CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("ChangeSyncSettings"));
				Thread.sleep(2000);
			}

		} catch (Exception e) {
			throw new Exception("Auto Sync Message");
		}
	}

	public void tapOnTurnOffSync(AppiumDriver driver) throws Exception {
		Map<String, String> verifyAutoSync = commonPage.getTestData("AutoSync");
		try {
			commonPage.waitForElementToBePresentByXpath(verifyAutoSync.get("AutoSyncAlert"));
			log.info("In UA.tapOnTurnOffSync Turn auto sync Alert displayed");
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("Cancel"));
			log.info("In UA.tapOnTurnOffSync Clicked on cancel button");
			CommonFunctions.isElementByXpathDisplayed(driver, verifyAutoSync.get("AutoSyncMsg"));
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("AutoSyncMsg"));
			Thread.sleep(800);
			CommonFunctions.searchAndClickByXpath(driver, verifyAutoSync.get("TurnOn"));
			log.info("In UA.tapOnTurnOffSync Clicked on Turn On button");
			Thread.sleep(3000);
			pullToReferesh(driver);
			Thread.sleep(3000);
			CommonFunctions.isElementByXpathNotDisplayed(driver, verifyAutoSync.get("AutoSyncMsg"));

		} catch (Exception e) {
			throw new Exception("");
		}
	}

	public static String[] getSubjectOfMail(AppiumDriver driver, String MailSubject, String emailSubject)
			throws Exception {
		try {

			val = new String[2];

			Thread.sleep(1500);
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SubjectLine"));
			MailSubject = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("SubjectLine"));
			val[0] = MailSubject;
			log.info("UA MailSubject:"+MailSubject);
			MailSubject = MailSubject.replaceAll("\\s+", " "); // added by Venkat on 07/06/2019
			MailSubject = MailSubject.substring(0, MailSubject.indexOf(" ")); // added by Venkat on 07/06/2019
			emailSubject = commonElements.get("EmailSubjectText").replace("Subject", MailSubject);
			val[1] = emailSubject;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;
	}

	/**
	 * Snooze Options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void SnoozeOptions(AppiumDriver driver) throws Exception {
		try {
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SnoozeLaterThisWeek")) != 0) {
				CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeLaterToday"));

			}
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeTomorrow"));
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SnoozeLaterThisWeek")) != 0) {
				CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeLaterThisWeek"));
			}
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeThisWeekend"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeNextWeek"));
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SnoozeSomeday")) != 0) {
				CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeSomeday"));
			}
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozePickDateTime"));
			log.info("All snooze options are displayed");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Verifying the snooze options
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void SnoozeGrid(AppiumDriver driver) throws Exception {
		try {
			// CommonFunctions.searchAndClickByXpath(driver,
			// commonElements.get("MailMoreOption"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("Snooze"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Snooze"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeGrid"));
			SnoozeOptions(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Snooze Mail
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void SnoozeMail(AppiumDriver driver, String emailSubject) throws Exception {
		try {
			log.info("Snooze a mail");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeLaterToday"));
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("SwipeUndo")) != 0) {
				log.info("Snooze Undo is displayed");
			} else {
				throw new Exception("Snooze undo toast bar is not displayed");

			}
			// CommonFunctions.isElementByXpathDisplayed(driver,
			// commonElements.get("SwipeUndo"));
			CommonFunctions.isElementByXpathNotDisplayed(driver, emailSubject);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Snooze Mail
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void UnSnoozeMail(AppiumDriver driver, String MailSubject) throws Exception {
		try {
			log.info("UnSnooze a mail from snooze folder");
			userAccount.verifyMailIsPresent(driver, MailSubject);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
			inbox.viewMoreOptionsCV(driver);
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Unsnooze"));
			Thread.sleep(2000);
			if (CommonFunctions.getSizeOfElements(driver, commonElements.get("UnsnoozeToastBar")) != 0) {
				log.info("Toast bar displayed");
			} else {
				throw new Exception("Unsnooze toast bar is not displayed");
			}
			CommonFunctions.isElementByXpathNotDisplayed(driver, MailSubject);

			/*
			 * if(CommonFunctions.getSizeOfElements(driver,
			 * commonElements.get("SenderImageIcon"))!=0){ log.info(
			 * "Mail present in snooze folder");
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("SenderImageIcon"));
			 * inbox.viewMoreOptionsCV(driver);
			 * CommonFunctions.searchAndClickByXpath(driver,
			 * commonElements.get("Unsnooze"));
			 * 
			 * }
			 */ } catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Custom snooze with Date and Time
	 * 
	 */
	public void SetCustomSnooze(AppiumDriver driver) throws Exception {
		try {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozePickDateTime"));
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("SnoozeCalendar"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeOKCalendarBtn"));
			// CommonFunctions.isElementByXpathDisplayed(driver,
			// commonElements.get("SnoozePickDateTimeAlert"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeTimeSelector"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeCustom"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeMinutes"));
			String minutes = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
					commonElements.get("SnoozeMinutes"));
			log.info(minutes);
			/*
			 * Thread.sleep(1000); WebElement clkmin =
			 * driver.findElement(By.className(
			 * "//android.widget.RadialTimePickerView$RadialPickerTouchHelper[@content-desc='30']"
			 * )); clkmin.click();
			 */
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SnoozeOKCalendarBtn"));
			WebElement SnoozeSave = null;
			try {
				SnoozeSave = driver.findElement(By.xpath("//android.widget.TextView[@text='Save']"));
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("EventSaveButton"));
			} catch (Exception e) {
				driver.tap(1, SnoozeSave, 500);
			}
			/*
			 * String ind = driver.findElement(By.className(
			 * "android.widget.RadialTimePickerView$RadialPickerTouchHelper[@content-desc='"
			 * +minutes+"'")).getAttribute("index"); log.info(ind);
			 */
			/*
			 * List <WebElement> allElements = driver.findElements(By.className(
			 * "android.widget.RadialTimePickerView$RadialPickerTouchHelper"));
			 * for(WebElement e : allElements){
			 * driver.findElementByAccessibilityId("").click(); // String c =
			 * e.getAttribute("content-desc"); }
			 */
			/*
			 * for(int i=0;i<=allElements.size();i++){
			 * allElements.get(i).findElement(By.className(
			 * "//android.widget.RadialTimePickerView$RadialPickerTouchHelper[contains(@content-desc,'"
			 * +i+"')]")); if(min.equals(minutes)){ WebElement m =
			 * driver.findElement(By.xpath(
			 * "//android.widget.RadialTimePickerView$RadialPickerTouchHelper[@content-desc='"
			 * +min+"']"));
			 * 
			 * }
			 * 
			 * }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Last option in snooze grid
	 * 
	 * @param driver
	 */
	public void verifyRememberLastSnooze(AppiumDriver driver) {
		try {
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("RememberSnoozeLast"));

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	/*
	 * public void RadialTimeNumber(AppiumDriver driver){ try{ List <WebElement>
	 * allElements = driver.findElements(By.className(
	 * "android.widget.RadialTimePickerView$RadialPickerTouchHelper"));
	 * 
	 * for(int i=0;i<=allElements.size();i++){
	 * 
	 * 
	 * 
	 * } }catch(Exception e){
	 * 
	 * } }
	 */

	public void ChangeSwipeAction(AppiumDriver driver, String SwipeAction) throws InterruptedException {
		Map<String, String> swipeActions = commonPage.getTestData("verifySwipeActions");

		try {
			CommonFunctions.searchAndClickByXpath(driver, swipeActions.get("SwipeChangeButton"));
			CommonFunctions.scrollsToTitle(driver, SwipeAction);
			Thread.sleep(800);
			String text = CommonFunctions.searchAndGetTextOnElementByXpath(driver,
					swipeActions.get("SwipeActionSummary"));
			log.info("Swipe actions selected" + text);
			navDrawer.navigateUntilHamburgerIsPresent(driver);
		} catch (Exception e) {

		}
	}

	public void verifyDevicePolicy(AppiumDriver driver) throws Exception {
		try {
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ComposeMailButton"));
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ComposeMailButton"));
			log.info("Verify the device policy setup");
			if (CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("GoogleAppsDevicePolicy"))) {
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("WorkProfile"));
				Thread.sleep(600);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				Thread.sleep(1000);
				CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("GoogleAppsDevicePolicy"));
				CommonFunctions.scrollsToTitle(driver, "ActivateDeviceAdministrator");
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Enforce"));
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Next"));
				Thread.sleep(2500);
			}
		} catch (Exception e) {

		}
	}

	public void SuperCollapse(AppiumDriver driver, String Subject) throws Exception {
		try {
			Map<String, String> verifySearchFunctionalityData = commonPage.getTestData("verifySearchFunctionality");
			SoftAssert softAssert = new SoftAssert();

			log.info("----------Started testcase SuperCollapse----------");
			CommonFunctions.searchAndClickByXpath(driver, verifySearchFunctionalityData.get("SearchIconXpath"));
			Thread.sleep(1500);
			commonFunction.searchAndSendKeysByXpath(driver, verifySearchFunctionalityData.get("SearchTextXpath"),
					Subject);
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(1200);
			CommonFunctions.searchAndClickByXpath(driver,
					commonElements.get("InboxMail").replace("MailSubject", Subject));
			Thread.sleep(2000);
			softAssert.assertTrue(commonPage.getScreenshotAndCompareImage("verifySuperCollapseForMultiThreadedMail"),
					"Image comparison for verifySuperCollapseForMultiThreadedMail");

			conversationView.verifySuperCollapseCountOfMail(driver);
			log.info("----------Completed testcase SuperCollapse----------");
			driver.navigate().back();
			driver.navigate().back();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifySwipeActionsPresent(AppiumDriver driver, String Archive, String Delete, String MarkRead,
			String Move, String Snooze, String None) throws Exception {
		try {
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", Archive));
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", Delete));
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", MarkRead));
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", Move));
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", Snooze));
			CommonFunctions.isElementByXpathDisplayed(driver,
					commonElements.get("SwipeActionXpath").replace("SwipeAction", None));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adding accounts through adb command externally
	 * 
	 * @throws Exception
	 */
	public void addAccountsInternal() throws Exception {
		try {
			log.info("Installing the google account manager apk internally to add other accounts");
			Process p;
			ProcessBuilder pb;
			String[] launchOldApk = new String[] { "adb", "install", "-r", "-d",
					"E:/Gmail-POC-GmailTest_Automation/GmailPOC/Gmail_Apk/GoogleAccountManagerApp.apk" };
			p = new ProcessBuilder(launchOldApk).start();
			log.info("Installing the google account manager apk");
			Thread.sleep(10000);

			/*
			 * String[] addAccountOne = new String[]{"adb", "shell", "am",
			 * "instrument", "-e", "account", "gm.gig1.auto@gmail.com", "-e",
			 * "password", "mobileautomation", "-e", "action", "add", "-e",
			 * "sync", "true", "-r", "-w",
			 * "com.google.android.tests.utilities/.GoogleAccountManager"}; p =
			 * new ProcessBuilder(addAccountOne).start(); log.info(
			 * "Installing account etouchtestone with google account manager apk"
			 * ); Thread.sleep(10000);
			 */
			String[] addAccountTwo = new String[] { "adb", "shell", "am", "instrument", "-e", "account",
					"gm.gig2.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync",
					"true", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager" };
			p = new ProcessBuilder(addAccountTwo).start();
			log.info("Installing account etouchtesttwo with google account manager apk");
			Thread.sleep(10000);

			String[] addAccountThree = new String[] { "adb", "shell", "am", "instrument", "-e", "account",
					"gm.gig3.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync",
					"true", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager" };
			p = new ProcessBuilder(addAccountThree).start();
			log.info("Installing account etouchtestthree with google account manager apk");
			Thread.sleep(10000);

			String[] addAccountFour = new String[] { "adb", "shell", "am", "instrument", "-e", "account",
					"gm.gig4.auto@gmail.com", "-e", "password", "mobileautomation", "-e", "action", "add", "-e", "sync",
					"true", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager" };
			p = new ProcessBuilder(addAccountFour).start();
			log.info("Installing account etouchtestfour with google account manager apk");
			Thread.sleep(10000);

			/*
			 * String[] addAccountSix = new String[]{"adb", "shell", "am",
			 * "instrument", "-e", "account", "etouchtestsix@gmail.com", "-e",
			 * "password", "eTouch@123", "-e", "action", "add", "-e", "sync",
			 * "true", "-r", "-w",
			 * "com.google.android.tests.utilities/.GoogleAccountManager"}; p =
			 * new ProcessBuilder(addAccountSix).start(); log.info(
			 * "Installing account etouchtestsix with google account manager apk"
			 * ); Thread.sleep(10000);
			 */
			String[] addeaitest = new String[] { "adb", "shell", "am", "instrument", "-e", "account",
					"eaitest101@gmail.com", "-e", "password", "eaitest123", "-e", "action", "add", "-e", "sync", "true",
					"-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager" };
			p = new ProcessBuilder(addeaitest).start();
			log.info("Installing account eaitest with google account manager apk");
			Thread.sleep(10000);

			String[] addcaribou = new String[] { "adb", "shell", "am", "instrument", "-e", "account",
					"caribou.dnis.receiver@gmail.com", "-e", "password", "BigBangInF()RK", "-e", "action", "add", "-e",
					"sync", "true", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager" };
			p = new ProcessBuilder(addcaribou).start();
			log.info("Installing account caribou with google account manager apk");
			Thread.sleep(10000);

			/*
			 * String[] addAdsAcct = new String[]{"adb", "shell", "am",
			 * "instrument", "-e", "account", "twoads.gig@gmail.com", "-e",
			 * "password", "mobiletesting", "-e", "action", "add", "-e", "sync",
			 * "true", "-r", "-w",
			 * "com.google.android.tests.utilities/.GoogleAccountManager"}; p =
			 * new ProcessBuilder(addAdsAcct).start(); log.info(
			 * "Installing account ads account with google account manager apk"
			 * );
			 */

			/*
			 * String[] addSmime = new String[]{"adb", "shell", "am",
			 * "instrument", "-e", "account", "smime.1qa1@gau.pintoqa.work",
			 * "-e", "password", "BigBangInF()RK", "-e", "action", "add", "-e",
			 * "sync", "true", "-r", "-w",
			 * "com.google.android.tests.utilities/.GoogleAccountManager"}; p =
			 * new ProcessBuilder(addSmime).start(); log.info(
			 * "Installing account mime account with google account manager apk"
			 * ); Thread.sleep(10000);
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void horizontalSwipe(AppiumDriver driver, String actionLableMailXpath, String swippingSide) throws InterruptedException {
		Dimension screenSize = driver.manage().window().getSize();
		int startx = (int) (screenSize.width * 0.70);
		int endx = (int) (screenSize.width * 0.30); 
		WebElement element = driver.findElement(By.xpath(actionLableMailXpath));
		int starty = element.getLocation().y;

		log.info("Swipping mail to "+swippingSide);
		if("Left".equalsIgnoreCase(swippingSide)){
			driver.swipe(startx, starty+70, endx-70, starty, 1000);
		}else{
			driver.swipe(endx-70, starty, startx, starty+70, 1000);
		}
		
	}

	public void navigeteNTimes(AppiumDriver driver, int numberOfTimes) throws InterruptedException{
		for (int i = 0; i < numberOfTimes; i++) {
			driver.navigate().back();
			Thread.sleep(500);
		}
	}
	
	
		
	public void goToSpecificLable(AppiumDriver driver, String toWhichLable) throws Exception {
		userAccount.openMenuDrawerNew(driver);
		CommonFunctions.scrollsToTitle(driver, toWhichLable);  
	}

	
	public void goToSwipteActions(AppiumDriver driver, String whichDirection, Map<String, String> swipeActions) throws Exception{
		goToSpecificLable(driver, "Settings");
		CommonFunctions.searchAndClickByXpath(driver, swipeActions.get("SettingsXPath"));
		try {
			CommonFunctions.searchAndClickByXpath(driver, swipeActions.get("GeneralSettingsXPath"));
		} catch (Exception e) {
			CommonFunctions.scrollTo(driver, "General settings"); 
			CommonFunctions.searchAndClickByXpath(driver, swipeActions.get("GeneralSettingsXPath"));
		}
		
		String swipeActionsXpath = swipeActions.get("SwipeActions");
		CommonFunctions.searchAndClickByXpath(driver, swipeActionsXpath);

		String swipeChangeXpath, xPathForSwipeForLable;
		if("Left".equals(whichDirection)){
			swipeChangeXpath = swipeActions.get("LeftSwipe");
			xPathForSwipeForLable = swipeActions.get("UseLeftSwipeFor");
		}else{
			swipeChangeXpath = swipeActions.get("RightSwipe");
			xPathForSwipeForLable = swipeActions.get("UseRightSwipeFor");
		}
		
		CommonFunctions.isElementByXpathDisplayed(driver, swipeChangeXpath);
		CommonFunctions.searchAndClickByXpath(driver, swipeChangeXpath);

		CommonFunctions.isElementByXpathDisplayed(driver, xPathForSwipeForLable);
	}

	public String findSwippedMail(AppiumDriver driver, String actionLableText, String actionLableMailXpath, String whichDirection) throws Exception {
		String folderLable = null, whereAmI = null;
		try {
			actionLableMailXpath = "//android.view.View[starts-with(@content-desc,'"+actionLableText+"')] | "+actionLableMailXpath; 
			switch (actionLableText) {
			case "Archive":
				folderLable = "All mail";
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				navigateBackNTimesNew(driver, 1);
				break;
			case "Delete":
				folderLable = "Trash";
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				navigateBackNTimesNew(driver, 1);
				break;
			case "Mark as read/unread":
				Thread.sleep(600);
				folderLable = "Primary";
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				userAccount.horizontalSwipe(driver, actionLableMailXpath, whichDirection);
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				break;
			case "Move to":
				folderLable = "Social";
				Thread.sleep(1000);
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				navigateBackNTimesNew(driver, 1);
				break;
			case "Snooze":
				folderLable = "Snoozed";
				findMailInSpecificFolder(driver, folderLable, actionLableMailXpath);
				navigateBackNTimesNew(driver, 1);
				break;
			default:
				System.out.println("In UA :"+actionLableMailXpath+" Not found..");
				break;
			}

		} catch (Exception e) {
			whereAmI = folderLable;
			throw new Exception("Unable to find mail with text "+actionLableMailXpath+" in "+folderLable);
		}
		return whereAmI;
	}
	
	private void findMailInSpecificFolder(AppiumDriver driver, String folderLable, String actionLableMailXpath) throws Exception{
		if(!"Primary".equals(folderLable)){
			userAccount.openMenuDrawerNew(driver);
			CommonFunctions.scrollToCellByTitle(driver, folderLable);
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='"+folderLable+"']");
		}
		CommonFunctions.isElementByXpathDisplayed(driver, actionLableMailXpath);
	}
	/**--
	*@author Venkat
	*/
	public void clickOrScrollAndClick(AppiumDriver driver, String scrollToTitle , String xpath) throws Exception {
		try {
			CommonFunctions.searchAndClickByXpath(driver, xpath);
		} catch (Exception e) {
			CommonFunctions.scrollTo(driver, scrollToTitle);
			CommonFunctions.searchAndClickByXpath(driver, xpath);
		}

	}

	/**--
	*@author Venkat
	*/
	public String getSelectedAccountName(AppiumDriver driver) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
		String currentAccName = "";
		try {
			CommonFunctions.scrollUp(driver, 2);
			currentAccName = CommonFunctions.searchAndGetTextOnElementByXpath(driver, "//android.widget.FrameLayout[@resource-id='com.google.android.gm:id/selected_account_header']//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']");
		} catch (Exception e) {
			throw new Exception("Exception while getting seclted account name..");
		}
		return currentAccName;
	}

	
	/**--Verifying actions in TL view  IMAP
	* @throws Exception
	*@author batchi
	*/
	@Test(priority = 98)
	public void verifySearchAndActionsIMAPInTL(AppiumDriver driver)throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
		try{
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
				CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),
						verifySendMailIMAPData.get("MailActionsTLSubject") );
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				log.info("Searched for a mail and performing MoveTo and Delete actions in TL view");
				inbox.verifyIMAPMoveToDelTLview(driver);
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
				CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),
						verifySendMailIMAPData.get("MailActionsTLSubject") );
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				log.info("Searched for a mail and performing Mark Read and Add Star actions in TL view");
				inbox.verifyIMAPMarkReadAddStarTLview(driver);
			
		}catch(Exception e){
	 		  commonPage.catchBlock(e, driver, "verifySearchAndActionsIMAPInTL");
	 	 }
	  }

	
	/**--Verifying actions in TL view  IMAP
	* @throws Exception
	*@author batchi
	*/
	@Test(priority = 98)
	public void verifySearchAndActionsIMAPInCV(AppiumDriver driver)throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Map <String, String> verifySendMailIMAPData = commonPage.getTestData("IMAPAccountDetails");
		try{
				CommonFunctions.searchAndClickByXpath(driver, commonElements.get("searchIDXapth"));
				CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("searchIDXapth"),
						verifySendMailIMAPData.get("ReceiveSubjectData") );
				((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
				log.info("Searched for a mail and performing MoveTo and Delete actions in CV view");
				Thread.sleep(4000);
				inbox.verifyEmailReceivedCV(driver, verifySendMailIMAPData);		
			
		}catch(Exception e){
	 		  commonPage.catchBlock(e, driver, "verifySearchAndActionsIMAPInCV");
	 	 }
	  }

	public void verifyTurningOffOnSyncNew(AppiumDriver driver) throws Exception {
		Thread.sleep(1500);
		String text = driver.findElement(By.xpath("////android.widget.Switch[@resource-id='android:id/switch_widget']")).getText();
	}

	public void addOrClearSignature(AppiumDriver driver, String accountA, String signature, String type) throws Exception {
		try {
			CommonFunctions.scrollsToTitle(driver, "Mobile Signature");
			Thread.sleep(2000);
			WebElement popUpElement = driver.findElement(By.xpath("//android.widget.EditText[@resource-id='android:id/edit']"));
			String signatureText = popUpElement.getText();
			if(signatureText != null && !signatureText.isEmpty()) {
				popUpElement.clear();
			}
			if("add".equals(type)) {
				popUpElement.sendKeys(signature);
				Thread.sleep(2000);
			}
			driver.findElement(By.xpath("//android.widget.Button[@resource-id='android:id/button1'] | //android.widget.Button[@resource-id='OK']")).click();
		}catch(Exception e) {
			new Exception("Exception while adding/clearing signature.:"+e.getMessage());
		}
	}

	public void signatureAddOrClear(AppiumDriver driver, String account,  String signatureText, String signToogle) throws Exception {
		userAccount.goToSpecificLable(driver, "Settings");
		Thread.sleep(2000);
		int count = 1;
		while(count++ <=3) {
			try {
				CommonFunctions.scrollsToTitle(driver, account);
			}catch(Exception e) {
				CommonFunctions.scrollUp(driver, 1);
			}
		}
		userAccount.addOrClearSignature(driver, account, signatureText, signToogle);
		userAccount.navigateBackNTimesNew(driver, 2);
	}
	
		/**--Exploratory case : account is in focus after switching accounts
	* @throws Exception
	*@author Santosh	*/
	@Test(priority =1)
	
	public void verifyAccountInFocus(AppiumDriver driver) throws Exception {
    	driver = map.get(Thread.currentThread().getId());
    	Map < String,
    	String > verifyAddingNewGmailAccountData = commonPage.getTestData("verifyAddingNewGmailAccount");

 		try {
 			log.info("verifyAccountInFocus");
 			log.info("*****************Started testcase ::: verifyAccountInFocus*****************************");
 			userAccount.launchGmailApp(driver);
			inbox.verifyGmailAppInitState(driver);
    		userAccount.switchMailAccount(driver, verifyAddingNewGmailAccountData.get("UserList"));
    		log.info("Account switched to gm.gig1.auto@gmail.com");
    		driver.navigate().back();
    		driver.navigate().back();
    		userAccount.launchGmailApp(driver);
    		driver.closeApp();
    		log.info("Soft killed app");
    		userAccount.launchGmailApp(driver);
			Thread.sleep(1000);
			log.info("Verifying the account name ");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
			//String accountText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, commonElements.get("GetAccountName"));
			String accountText = driver.findElement(By.xpath(commonElements.get("AccountsAdded"))).getAttribute("text");
			log.info(accountText);
			if(accountText.equalsIgnoreCase(verifyAddingNewGmailAccountData.get("UserList"))){
				log.info("Account Name is verified");
				driver.navigate().back();
			}else{
				throw new Exception("Account Name is not verified ");
				
			}
			
	    log.info("*****************Completed testcase ::: verifyAccountInFocus*****************************");   
	    } catch (Exception e) {
 			e.printStackTrace();
 		} 
 	}
	/**
	 * Method to upgrade the Gmail Go apk
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void AppUpgrade(AppiumDriver driver) throws Exception {
		log.info("In method for getting current gmail app version");
		
		Map<String, String> verifyUpgradeOfGmailAppData = commonPage.getTestData("verifySettingsOption1");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		Thread.sleep(2000);
		scrollDown(2);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		String versionBeforeUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionBeforeUpgrading);
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
		/*
		 * commonPage.launchPlayStoreApp(driver);
		 * commonPage.verifyAppIsUpdated(driver,verifyUpgradeOfGmailAppData.get(
		 * "SearchKeyword") );
		 */
		Process p;
		ProcessBuilder pb;
		log.info("Upgrading the gmail apk");
		String[] launchOldApk = new String[] { "adb", "install", "-r", "-d", "path" + java.io.File.separator + "Gmail_Apk" + java.io.File.separator + "Gmail_fishfood.apk" };
		p = new ProcessBuilder(launchOldApk).start();
		log.info("Upgraded the gmail apk");
		
				
//		Thread.sleep(2000);
		/*//Verifying if the apk upgrade/installation is successfull or not
		Scanner scanner = new Scanner(System.in);
		String apkInstallState = scanner.nextLine();
		if(apkInstallState.equalsIgnoreCase("Success")){
			log.info("Apk is successfully upgraded");
		}else{
			String[] signedAPK = new String[] { "adb", "install", "-r", "-d",
					"path" + java.io.File.separator + "Gmail_Apk" + java.io.File.separator + "Gmail_fishfood_signed.apk" };
			p = new ProcessBuilder(signedAPK).start();
			String signedApkInstallState = scanner.nextLine();
			if(signedApkInstallState.equalsIgnoreCase("Success")){
				log.info("Apk is successfully upgraded");
			}else {
				throw new Exception("Unable to upgrade apk eith with signed or unsigned apk");
			}
			
		}*/

//		String[] clearGmailCache = new String[] { "adb", "shell", "pm", "clear", "com.google.android.gm" };
		Thread.sleep(30000);
		log.info("Launching Gmail app");
		String[] launchGmail = new String[] { "adb", "shell", "am", "start", "-n",
				"com.google.android.gm/.ConversationListActivityGmail" };
		pb = new ProcessBuilder(launchGmail);
		
		p = pb.start();
		Thread.sleep(2000);

		commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
		log.info("App launched");
		// commonFunction.searchAndClickById(driver,
		// commonElements.get("SearchIcon"));
		Thread.sleep(2000);
		scrollDown(2);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedBackOption"));
		Thread.sleep(2000);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("DriverMoreOption"));
		commonFunction.searchAndClickByXpath(driver, commonElements.get("VersionInfo"));
		String versionAfterUpgrading = commonFunction.searchAndGetTextOnElementByXpath(driver,
				commonElements.get("VersionInfoDetails"));
		log.info(versionAfterUpgrading);
		if (versionAfterUpgrading.equals(versionBeforeUpgrading)) {
			throw new Exception("############ Upgrading the gmail app is not performed #############");
		} else {
			log.info("^^^^^^^^^^^^^^ Version got updated ^^^^^^^^^^^^^^^^");
		}
		commonPage.navigateBack(driver);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("HelpAndFeedbackCloseIcon"));
	}
	/**-Exploratory: -Verifying multiple accounts
	* @throws Exception
	*@author batchi
	*/
	@Test(priority = 100)
	public void verifyRemoveMultipleAccounts(AppiumDriver driver, String mailId)throws Exception{
		driver = map.get(Thread.currentThread().getId());
	
		try{
			log.info("Removing account");
			Process p;
			String[] remAcc = new String[]{"adb", "shell", "am", "instrument", "-e", "account", mailId , "-e", "action", "remove", "-e", "type", "com.google", "-r", "-w", "com.google.android.tests.utilities/.GoogleAccountManager"};
		    p = new ProcessBuilder(remAcc).start();
		    log.info("Removing account" + mailId +" from App");
		    if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk") )!=0){
		    	CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		    }
			   
	    } catch (Exception e) {
 			e.printStackTrace();
 		} 
 	}
	public void verifyAlertToAddAccount(AppiumDriver driver) throws Exception{
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("WelcomePageSkipMessage"));
		waitForElementToBePresentByXpath(commonElements.get("NewGmailTakeMeToGmail"));
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("NewGmailTakeMeToGmail"));
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AddAtleastOneEmail"))!=0){
			log.info("******* Alert to add an  account  is displayed **********");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		}else{
			throw new Exception("!!!!!!! Alert to add an account is not displayed !!!!!!!!!");
		}
	}
	/**--Verifying actions in TL view 
	* @throws Exception
	*@author batchi
	*/
	@Test(priority = 98)
	public void verifySearchAndActionsInGmailTL(AppiumDriver driver)throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		try{
			// Moveto  and Delete from TL 
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifyMoveToDelInTLview(driver);
			// Mark Read and Add Star	
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifyMarkReadAddStarTLview(driver);
			// Archive 	
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifySearchAndArchiveTL(driver);
			//Mute 
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifySearchAndMuteTL(driver);
			// Change Labels
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifySearchAndChangeLabelTL(driver);
			//Report Spam
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchTLGmailSubject"));
				inbox.verifySearchAndReportSpamTL(driver);
			
		}catch(Exception e){
	 		  commonPage.catchBlock(e, driver, "verifySearchAndActionsInTL");
	 	 }
	  }
	/**--Verifying actions in CV view 
	* @throws Exception
	*@author batchi
	*/
	@Test(priority = 98)
	public void verifySearchAndActionsInGmailCV(AppiumDriver driver)throws Exception{
		driver = map.get(Thread.currentThread().getId());
		Map < String, String > verifyVariousSearchOperationsData = commonPage.getTestData("verifySearchFunctionality");
		try{
			// Moveto and Delete from CV 
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifyMoveToDelInCVview(driver);
			
			// Mark Read and Add Star	
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifyMarkReadAddStarCVview(driver);
			// Archive 	
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifySearchAndArchiveCV(driver);
			//Mute 
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifySearchAndMuteCV(driver);
			// Change Labels
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifySearchAndChangeLabelCV(driver);
			//Report Spam
				inbox.searchMethodWithSubject(driver, verifyVariousSearchOperationsData.get("SearchCVGmailSubject"));
				inbox.verifySearchAndReportSpamCV(driver);
			
		}catch(Exception e){
	 		  commonPage.catchBlock(e, driver, "verifySearchAndActionsInGmailCV");
	 	 }
	  }
}
