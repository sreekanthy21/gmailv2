package com.etouch.gmail.common;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.Registry;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;

import com.etouch.taf.core.TestBedManager;

public class SpecializedScreenRecorder extends ScreenRecorder {
	private String name; 
	static Process child;
	static String AbsolutePath= TafExecutor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	static String  RequiredPath = AbsolutePath.substring(0,AbsolutePath.indexOf("target/classes/")).substring(1);
	static String droidAtScreenarJarLocation = RequiredPath.concat("src/test/resources/droidAtScreen.jar");
	
	private static ScreenRecorder screenRecorder;
	
	public SpecializedScreenRecorder(GraphicsConfiguration cfg,Rectangle captureArea, Format fileFormat, Format screenFormat,
	           Format mouseFormat, Format audioFormat, File movieFolder,
	           String name) throws IOException, AWTException
	{
		super(cfg, captureArea, fileFormat, screenFormat, mouseFormat,
                audioFormat, movieFolder);
		this.name = name;
	}
	
	
    protected File createMovieFile(Format fileFormat) throws IOException {
          if (!movieFolder.exists()) {
                movieFolder.mkdirs();
          } else if (!movieFolder.isDirectory()) {
                throw new IOException("\"" + movieFolder + "\" is not a directory.");
          }
                           
          SimpleDateFormat dateFormat = new SimpleDateFormat(
                   "yyyy-MM-dd HH.mm.ss");
                         
          return new File(movieFolder, name + "-" + dateFormat.format(new Date()) + "."
                  + Registry.getInstance().getExtension(fileFormat));
    }
    
    /**
	 * This method initiates DroidAtScreen to cast connected devices and uses monte jar to record activities on casted devices 
	 * 
	 * @param String Location
	 * @throws IOException, AWTException
	 */	
	
	public static void startVideoRecordingForMobile(String Location) throws IOException, AWTException
	{
		child = Runtime.getRuntime().exec("java -jar "+droidAtScreenarJarLocation);
		
		
		File file = new File(Location);
		Rectangle captureSize = new Rectangle(100,0,1200,800);
		//Set frame capture ratio
		GraphicsConfiguration gc = GraphicsEnvironment
	               .getLocalGraphicsEnvironment()
	               .getDefaultScreenDevice()
	               .getDefaultConfiguration();
		
		screenRecorder = new SpecializedScreenRecorder(gc, captureSize,
	               new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
	               new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
	                    CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
	                    DepthKey, 24, FrameRateKey, Rational.valueOf(15),
	                    QualityKey, 1.0f,
	                    KeyFrameIntervalKey, 15 * 60),
	               new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black",
	                    FrameRateKey, Rational.valueOf(30)),
	               null, file, "MyVideo");
	          screenRecorder.start();
		
		
	}
	
	public static void startVideoRecordingForDesktopBrowser(String Location) throws IOException, AWTException
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		File file = new File(Location);
		Rectangle captureSize = new Rectangle(0,0,width,height);
		//Set frame capture ratio
		GraphicsConfiguration gc = GraphicsEnvironment
	               .getLocalGraphicsEnvironment()
	               .getDefaultScreenDevice()
	               .getDefaultConfiguration();
		
		screenRecorder = new SpecializedScreenRecorder(gc, captureSize,
	               new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
	               new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
	                    CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
	                    DepthKey, 24, FrameRateKey, Rational.valueOf(15),
	                    QualityKey, 1.0f,
	                    KeyFrameIntervalKey, 15 * 60),
	               new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, "black",
	                    FrameRateKey, Rational.valueOf(30)),
	               null, file, "MyVideo");
	          screenRecorder.start();
		
		
	}
	
	/**
	 * Stops recording and stop DroidAtScreen process 
	 * 
	 * @param
	 * @throws IOException, AWTException
	 */	
	public static void stopVideoRecordingForMobile() throws IOException, AWTException
	{
		

		screenRecorder.stop();
		child.destroy();
		
		
	}
	
    
 }

