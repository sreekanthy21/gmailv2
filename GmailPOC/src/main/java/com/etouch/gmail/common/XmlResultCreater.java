package com.etouch.gmail.common;


import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class XmlResultCreater {

	
	public static void main(String argv[]) {

	    try {

		File fXmlFile = new File("C:/GmailPOC2602/GmailPOC/test-output/testng-results.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		File parentBody = fXmlFile;
		Document parentDoc = dBuilder.parse(parentBody);
		
	//	parentDoc.removeChild("test");
		
		doc.getDocumentElement().normalize();

		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		NodeList nList = doc.getElementsByTagName("test");
		
		
		
		//NodeList nList1 = doc.getElementsByTagName("staff");

		System.out.println("----------------------------");

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(1);

			System.out.println("\nCurrent Element :" + nNode.getNodeName());

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) nNode;

				System.out.println("Staff id : " + eElement.getAttribute("id"));
				System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
				

			}
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	  }
	
	
}
