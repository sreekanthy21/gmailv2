package com.etouch.gmail.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.etouch.taf.core.TestBedManager;
import com.etouch.taf.core.datamanager.excel.TestParameters;
import com.etouch.taf.tools.defect.IDefectManager;
import com.etouch.taf.util.CommonUtil;
import com.etouch.taf.util.LogUtil;
import com.etouch.taf.webui.selenium.WebPage;
import com.itextpdf.text.List;

import cucumber.api.java.en_old.Ac;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * The Class
 *
 * @author eTouch Systems Corporation
 */
@SuppressWarnings(value = {"all"})
public class CommonPage extends BaseTest {

	protected static Map<String, String> commonElements = new HashMap<String, String>();
	/** The page url. */
	protected String pageUrl;

	/** The web page. */
	protected WebPage webPage;

	/** The err message. */
	protected String errMessage;
	// required for screen recorder
	/** The screen recorder. */
	// private ScreenRecorder screenRecorder;

	/** The log. */
	private static Log log = LogUtil.getLog(CommonPage.class);
	//InboxFunctions inbox = new InboxFunctions();

	/**
	 * Instantiates a new common page.
	 */
	public CommonPage() {

	}

	/**
	 * Instantiates a new common page.
	 *
	 * @param sbPageUrl
	 *            the sb page url
	 * @param webPage
	 *            the web page
	 */
	public CommonPage(String sbPageUrl, WebPage webPage) {
		this.pageUrl = sbPageUrl;
		this.webPage = webPage;
	}

	/**
	 * Gets the err message.
	 *
	 * @return the err message
	 */
	public String getErrMessage() {
		CommonUtil.sop("Error Message + " + errMessage);
		return errMessage;
	}

	/**
	 * Sets the err message.
	 *
	 * @param errMessage
	 *            the new err message
	 */
	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	/**
	 * Sets the err message.
	 *
	 * @param errorType
	 *            the error type
	 * @param pageElement
	 *            the page element
	 * @param pageUrl
	 *            the page url
	 * @param expectedResult
	 *            the expected result
	 * @param actualResult
	 *            the actual result
	 * @param messageStr
	 *            the message str
	 */
	// TODO: ...taf.core.logging
	// commented for safari test
	public void setErrMessage(String errorType, String pageElement, String pageUrl, String expectedResult,
			String actualResult, String messageStr) {

		CommonUtil.sop("\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nPage Exception in ????????????????????????? ::"
				+ Thread.currentThread().getStackTrace()[2].getMethodName()
				+ "\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n");
		String message = "";
		try {
			if (errorType != null) {
				message += "\n # Error Type: " + errorType;
			}
			if (pageElement != null) {
				message += "\n # Page Element: " + pageElement;
			}
			if (pageUrl != null) {
				message += "\n # Page URL: " + pageUrl;
			}
			if (expectedResult != null) {
				message += "\n # Expected Result: " + expectedResult;
			}
			if (actualResult != null) {
				message += "\n # Actual Result: " + actualResult;
			}
			if (messageStr != null) {
				message += "\n # Message: " + messageStr;
			}
			message += "";
		} catch (Exception ex) {
			message = "An error occured while setting Error Message: " + ex.toString();
		} finally {
			this.errMessage = message;
		}
		CommonUtil.sop("Got Page Exception:-" + errMessage);

	}

	/**
	 * Load page.
	 */
	protected void loadPage() {
		webPage.loadPage(pageUrl);

	}

	/*
	 * Log error and create/add a defect
	 */
	/**
	 * Log and create a defect.
	 *
	 * @param defect
	 *            the defect
	 * @param fileName
	 *            the file name
	 * @param testcaseId
	 *            the testcase id
	 * @param workspaceId
	 *            the workspace id
	 * @param projId
	 *            the proj id
	 * @param storyId
	 *            the story id
	 * @param defectName
	 *            the defect name
	 * @param defectSeverity
	 *            the defect severity
	 * @param defectOwner
	 *            the defect owner
	 * @param defectNotes
	 *            the defect notes
	 * @param errorMsg
	 *            the error msg
	 */
	public void logAndCreateADefect(IDefectManager defect, String fileName, String testcaseId, String workspaceId,
			String projId, String storyId, String defectName, String defectSeverity, String defectOwner,
			String defectNotes, String errorMsg, String attachmentPath) {
		try {
			String defectNote = defectNotes + " : " + errorMsg;
			// to create defect and add attatchment
			defect.createDefectBuilder(defectName, testcaseId, workspaceId, projId, defectSeverity, defectOwner,
					defectNote, storyId, attachmentPath);

				// added to update testcase result

			defect.updateTestCaseResult(defectName, testcaseId, workspaceId, projId, defectSeverity, defectOwner,
					defectNotes, storyId);
		
		} catch (Exception e) {
	
		}
	}

	// log and defect method for Jira defect --added by sonam
	/**
	 * Log and create a defect.
	 *
	 * @param defect
	 *            the defect
	 * @param url
	 *            the url
	 * @param issueUrl
	 *            the issue url
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @param keys
	 *            the keys
	 */
	public void logAndCreateADefect(TestParameters inputs, String errMsg, IDefectManager defect, String url,
			String issueUrl, String username, String password, String keys) {

		if (defect != null) {

			defect.createAJiraDefectBuilder(inputs, errMsg, url, issueUrl, username, password, keys);

		}
	}

	public static void initializeCommonOperationsData() throws Exception {
		try {
			commonElements = getTestData("GmailCommonEmelents");

		} catch (Exception e) {

			throw new Exception("Unable to initialize Common Elements Data Due to  " + e.getLocalizedMessage());
		}
	}

	public static void logDefect(Throwable exp, String methodName) {
		// StackTraceElement[] elements = exp.getStackTrace();
		testFailureMap.put(Thread.currentThread().getId(), testFailureMap.get(Thread.currentThread().getId())
				+ methodName + " ----> " + exp.getLocalizedMessage() + "\n");

	}
	
	/**
	 * Method to launch Gmail App
	 * 
	 * @param driver2
	 * @throws Exception
	 *//*
	public void launchGmailApp(AppiumDriver driver2) throws Exception {
		// TestBedManager.INSTANCE.getCurrentTestBeds().get("testBedName").getApp().getAppActivity()
		log.info("Launching the gmail app using the appPackage and appActivity");
		((AndroidDriver) driver2).startActivity("com.google.android.gm","com.google.android.gm.ConversationListActivityGmail");
		Thread.sleep(3000);
		waitForHambergerMenuToBePresent();
	}*/
	
	/**
	 * Method to wait for Hamburger Menu using xpath until element is present
	 * and clickable
	 * 
	 * @throws Exception
	 */
	public void waitForHambergerMenuToBePresent() throws Exception {
		try {
			waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("HamburgerMenu"))));
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception("Unable to find Hamberger Menu");
		}
	}
	/**
	 * Method to launch Gmail App
	 * 
	 * @param driver2
	 * @throws Exception
	 */
	public static void launchGmailApp(AppiumDriver driver) throws Exception {
		// TestBedManager.INSTANCE.getCurrentTestBeds().get("testBedName").getApp().getAppActivity()
		try{
			log.info("Launching the gmail app using the appPackage and appActivity");
			log.info(appPackage+" and "+appActivity);
			((AndroidDriver) driver).startActivity(appPackage,appActivity);
			log.info("appPackage and appActivity picked up from the config file");
			Thread.sleep(6000);
	//	waitForHambergerMenuToBePresent();
	}catch(Exception e){
		log.info("App is not launched and hence trying again");
		 Process p;
		String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.google.android.gm/com.google.android.gm.ConversationListActivityGmail"};
		    p = new ProcessBuilder(p1).start();
		    log.info("Trying to launch the app");  
		Thread.sleep(6000);
			//e.printStackTrace();
		}
	}

	/**
	 * Method to get data from excel file
	 * 
	 * @param methodName
	 * @return
	 * @throws InterruptedException
	 */
	public static Map<String, String> getTestData(String methodName) throws InterruptedException {
		Map<String, String> testCaseTestData = new HashMap<String, String>();
		testCaseTestData = GetDataInHashMap.getData("GmailData", methodName);
		return testCaseTestData;
	}

	/**
	 * Method to refresh the screen
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public static void pullToReferesh(AppiumDriver driver) throws Exception {
		try {
			Thread.sleep(1000);
			Dimension windowSize = driver.manage().window().getSize();
			driver.swipe((int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.30),
					(int) ((windowSize.width) * 0.50), (int) ((int) (windowSize.height) * 0.85), 1000);
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to Pull and Referesh Page ");
		}
	}

	/**
	 * Method to wait for element using xpath until element is present and
	 * clickable
	 * 
	 * @param xpath
	 * @throws Exception
	 */
	public static void waitForElementToBePresentByXpath(String xpath) throws Exception {
		try {
			//Thread.sleep(5000);
			log.info("CP wait4 Element:"+xpath);
			waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			//log.info("Element found "+xpath);
		} catch (NoSuchElementException e) {
			
			scrollDown(1);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Unable to find element by Xpath :" + xpath);
		
		}
			
	}
	
	/**
	 * Method to verify the green padlock icon
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyGreenPadlock(AppiumDriver driver,
			String toFieldData) throws Exception {
		Map<String, String> verifyIconsAreUpdatedData = commonPage
				.getTestData("verifyIconsAreUpdated");
		
		try {
			log.info("In verify PadLock In Subject");
			SoftAssert softAssert = new SoftAssert();
			Thread.sleep(2000);
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ComposeMailButton"))){
				commonFunction.searchAndClickByXpath(driver,commonElements.get("ComposeMailButton"));
			}
			commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("ToFieldXpath"), toFieldData+" ");
		//	commonFunction.searchAndSendKeysByID(driver,commonElements.get("ToField"), toFieldData + "    ");
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(2000);
			commonFunction.searchAndClickById(driver,commonElements.get("EmailSubject"));
			softAssert.assertTrue(commonPage
					.getScreenshotAndCompareImage("verifyGreenPadlock"),
					"Image comparison for verifyGreenPadlock");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PadLockInSubject"));
			Thread.sleep(800);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyIconsAreUpdatedData.get("GreenPadlockToastText"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify Green Padlock ");
		}
	}

	/**
	 * Method to verify the gray padlock icon
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyGrayPadlock(AppiumDriver driver, String toFieldData)
			throws Exception {
		Map<String, String> verifyIconsAreUpdatedData = commonPage
				.getTestData("verifyIconsAreUpdated");
		
		try {
			log.info("In verify PadLock In Subject");
			SoftAssert softAssert = new SoftAssert();
			commonFunction.searchAndClickByXpath(driver,
					commonElements.get("ComposeMailButton"));
			commonFunction.searchAndSendKeysByID(driver,
					commonElements.get("ToField"), toFieldData+" ");
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(2000);
			softAssert.assertTrue(commonPage
					.getScreenshotAndCompareImage("verifyGrayPadlock"),
					"Image comparison for verifyGrayPadlock");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PadLockInSubject"));
			Thread.sleep(800);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyIconsAreUpdatedData.get("GrayPadlockToastText"));
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify Gray Padlock ");
		}
	}

	/**
	 * Method to verify the green padlock icon
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyRedPadlock(AppiumDriver driver, String toFieldData)
			throws Exception {
		Map<String, String> verifyIconsAreUpdatedData = commonPage
				.getTestData("verifyIconsAreUpdated");
		
		try {
			log.info("In verify PadLock In Subject");
			SoftAssert softAssert = new SoftAssert();
			commonFunction.searchAndClickByXpath(driver,
					commonElements.get("ComposeMailButton"));
			commonFunction.searchAndSendKeysByID(driver,
					commonElements.get("ToField"), toFieldData+" ");
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			Thread.sleep(2000);
			softAssert
					.assertTrue(commonPage
							.getScreenshotAndCompareImage("verifyRedPadlock"),
							"Image comparison for verifyRed Pad lock");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PadLockInSubject"));
			Thread.sleep(800);
			CommonFunctions.isElementByXpathDisplayed(driver, verifyIconsAreUpdatedData.get("RedPadlockToastText"));
		
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify Red Padlock ");
		}
	}

	/**
	 * Method to verify the content of the padlock icon
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyPadlockToast(AppiumDriver driver,
			String PadlockToastText) throws Exception {
		try {
			log.info("In verify PadLock toast");
			commonFunction.searchAndClickByXpath(driver,
					commonElements.get("SecurityIconInSubject"));
			if (commonFunction.getSizeOfElements(driver,
					commonElements.get("PadlockToast")) != 0) {
				log.info("$$$$$$$$$$$$$$$ Padlock Toast is avaialble $$$$$$$$$$$$$$$");
				Thread.sleep(2000);
				String toastText = driver.findElement(
						By.xpath(commonElements.get("PadlockToast"))).getText();
				log.info(toastText);
				if (toastText.equalsIgnoreCase(PadlockToastText)) {
					log.info("^^^^^^^^^^^ Padlock Toast is available ^^^^^^^^^^^ ");

				} else {
					throw new Exception(
							"Unable to verify PadLock toast message ");
				}
			} else {
				throw new Exception("Unable to verify PadLock toast ");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify Padlock ");
		}
	}
	
	/**
	 * Method to verify details in respective account
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyDetailsFromRespectiveAccount(AppiumDriver driver,
			String mailSubject, String encryptionMsg) throws Exception {
		try {
			log.info("In verify details from respective account");
			inbox.openMailUsingSearch(driver, mailSubject);
			commonFunction.searchAndClickByXpath(driver,
					commonElements.get("ViewRecipientDetails"));
			Thread.sleep(2000);
			String encryptionText = driver.findElement(
					By.xpath(commonElements.get("EncryptionMessage")))
					.getText();
			log.info(encryptionMsg);
			if (encryptionText.contains(encryptionMsg)) {
				log.info("!!!!!!!!!!!! Encryption message is as Expected !!!!!!!!!!!!!");

			} else {
				throw new Exception("Unable to verify details");

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify details ");
		}
	}


	/**
	 * Method to verify the content of the padlock icon
	 * 
	 * @author batchi
	 * @throws Exception
	 */
	public static void verifyViewDetailsInToast(AppiumDriver driver,
			String iconText, String tofield, String recipientInMessageSecurity)
			throws Exception {
		try {
			log.info("In verify View Details in toast");
			commonFunction.searchAndClickByXpath(driver,commonElements.get("ViewDetailsInPadlockToast"));
			commonFunction.searchAndClickByXpath(
					driver,
					commonElements.get("PadlockMessageSecurityText").replace(
							"msgSecurity", iconText));
			Thread.sleep(2000);
			/*String recipientText = driver.findElement(
					By.xpath(commonElements.get(recipientInMessageSecurity)))
					.getText();
			log.info(recipientText);
			if (recipientText.equalsIgnoreCase(tofield)) {
				log.info("########### Recipiants is same as that we have entered in To field of Compose ##########");
			} else {
				throw new Exception("Unable to verify View Deatils in Toast ");
			}
			*/
			CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("EncriptionRecipient").replace("Recipient", recipientInMessageSecurity));
			commonFunction.navigateBack(driver);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to verify Padlock ");
		}
	}
	
	/*public static void waitForElementToBePresentByXpath(String xpath) throws Exception {
		try {
			Thread.sleep(500);
			waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to find element by Xpath :" + xpath);
		}
	}*/
	

	/**
	 * Method to wait for element using if until element is present and
	 * clickable
	 * 
	 * @param id
	 * @throws InterruptedException
	 */
	public static void waitForElementToBePresentById(String id) throws InterruptedException {
		Thread.sleep(500);
		waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.elementToBeClickable(By.id(id)));
		Thread.sleep(1000);
	}

	/**
	 * Method to scroll down the screen
	 * 
	 * @param numberOfScroll
	 * @throws InterruptedException
	 */
	public static void scrollDown(int numberOfScroll) throws InterruptedException {
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		Dimension windowSize = driver.manage().window().getSize();
		for (int j = 1; j <= numberOfScroll; j++) {
			driver.swipe((int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.85),
					(int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.30), 1000);
			// Thread.sleep(500);
		}
	}

	/**
	 * Method o scroll Up the screen
	 * 
	 * @param numberOfScroll
	 * @throws InterruptedException
	 */
	public static void scrollUp(int numberOfScroll) throws InterruptedException {
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		Dimension windowSize = driver.manage().window().getSize();
		for (int j = 0; j < numberOfScroll; j++) {
			driver.swipe((int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.30),
					(int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.85), 1000);
			// Thread.sleep(100);
		}
	}
	/**
	 * Method to navigate back
	 * 
	 * @throws InterruptedException
	 */
	public static void navigateBack(AppiumDriver driver) throws InterruptedException {
	
		log.info("Navigating back");
		driver.navigate().back();
		Thread.sleep(700);
	}
	/**
	 * Method for catch block
	 * 
	 * @param e
	 * @param methodName
	 * @throws Exception
	 */
	public void catchBlock(Throwable e,AppiumDriver driver, String methodName) throws Exception {
		e.printStackTrace();
		log.info("Getting screenshot");
		GetDataInHashMap.getScreenShotForFailure(driver, methodName);
		//logDefect(e, methodName);
		Assert.fail("Test Case ::: " + methodName + " failed as :::" + e.getLocalizedMessage());
		driver.close();
	}
	
	/**
	 * Method for catch block
	 * 
	 * @param e
	 * @param methodName
	 * @throws Exception
	 */
	public void catchBlockDefect(Throwable e,AppiumDriver driver, String methodName) throws Exception {
		e.printStackTrace();
		log.info("Getting screenshot");
		GetDataInHashMap.getScreenShotForFailure(driver, methodName);
		//logDefect(e, methodName);
		Assert.fail("Test Case ::: " + methodName + " failed as :::" + e.getLocalizedMessage());
		driver.close();
	}
	/**
	 * Method to scroll to
	 * 
	 * @param elementText
	 * @throws InterruptedException
	 */
	public void scrollTo(String elementText) throws InterruptedException {
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		driver.scrollTo(elementText);
		// Thread.sleep(700);
	}
	/**
	 * Method to hide keyboard
	 * 
	 * @throws InterruptedException
	 */
	public static void hideKeyboard() throws InterruptedException {
		try{
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		driver.hideKeyboard();
		Thread.sleep(500);
		}
		catch(Exception e){
		log.info("Keyboard not found");
		}
	}

	/**
	 * Method to get screenshot and compare image
	 * @param methodName
	 * @throws IOException
	 */
	public static boolean getScreenshotAndCompareImage(String methodName) throws IOException {
		boolean compareFlag = true;
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		GetDataInHashMap.getScreenShot(driver,methodName);
		
		compareFlag = GetDataInHashMap.compareGoldenImage(methodName);
		return compareFlag;
	}
	/**
	 * Method to get screenshot and compare image
	 * @param methodName
	 * @throws IOException
	 */
	public static boolean getScreenshotAndCompareImage2(String methodName) throws IOException {
		boolean compareFlag = true;
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		/*GetDataInHashMap.getScreenShot(driver,methodName);*/
		compareFlag = GetDataInHashMap.compareGoldenImage(methodName);
		return compareFlag;
	}
	/**
	 * Method to open link in app browser
	 * 
	 * @param linkText
	 * @throws Exception
	 */
	public void openLinkInAppBrowser(AppiumDriver driver, String linkText) throws Exception {
		
		//Thread.sleep(2000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("Hyperlink").replace("LinkText", linkText));
		log.info("Link is opening in chrome browser");
		Thread.sleep(3000);
		}

	/**
	 * Method to open link in chrome
	 * 
	 * @param linkText
	 * @param ElementPresentInChrome
	 * @throws Exception
	 */
	public void openLinkInChrome(AppiumDriver driver, String linkText, String InAppBrowserMoreOptionsOpenInChromeXpath) throws Exception {
		openLinkInAppBrowser(driver, linkText);
		Thread.sleep(6000);
		/*CommonFunctions.searchAndClickByXpath(driver, commonElements.get("InAppBrowserMoreOptions"));
		Thread.sleep(3000);
		CommonFunctions.searchAndClickByXpath(driver, InAppBrowserMoreOptionsOpenInChromeXpath);
*/
		if (CommonFunctions.getSizeOfElements(driver, commonElements.get("OpenAttachmentJustOnce"))!=0) {
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("OpenAttachmentJustOnce"));
		}
		waitForElementToBePresentByXpath(commonElements.get("ElementXpathPresenceInChrome"));

		if (!CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("ElementXpathPresenceInChrome"))) {
			throw new Exception("Failed to Validate page opened in chrome : Failed to find Element with Xpath :"
					+ commonElements.get("ElementXpathPresenceInChrome"));
		}
		navigateBack(driver);
		if (CommonFunctions.isElementByXpathNotDisplayed(driver, commonElements.get("MailToolBarDeleteIcon"))) {
			navigateBack(driver);
		}

	}

	/**
	 * Method to verify Gmail App init
	 * 
	 * @param driver
	 * @throws Exception
	 */
	public void verifyGmailAppInit(AppiumDriver driver) throws Exception {
		try {
			waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("HamburgerMenu"))));
		} catch (Exception e) {
			launchGmailApp(driver);
		}
	}
	
	public void verifyThreadListLoaded(AppiumDriver driver)throws Exception{
		try{
			waitMap.get(Thread.currentThread().getId())
				.until(ExpectedConditions.elementToBeClickable(By.xpath(commonElements.get("SenderImageIcon"))));
			log.info("Thread list loaded");
		}catch(NoSuchElementException e){
			Thread.sleep(3000);
			pullToReferesh(driver);
		}
	}

	/**
	 * Method to tap On Navigate Back Button
	 * 
	 * @throws Exception
	 */
	public void tapOnNavigateBackButton() throws Exception {
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SearchPageNavigateBackButton"));
	}
	
	/**Method to navigate until hamburger is present
	 * 
	 * @param driver2
	 * @throws Exception
	 * @author Bindu
	 */
	public void navigateUntilHamburgerIsPresent(AppiumDriver driver2) throws Exception{
		log.info("clicking back button to navigate until hamburger icon is present");
		int count=0;
		for(int i=0;i<=count;i++){
			//navigateBack(driver2);
			//commonFunction.searchAndClickByXpath(driver2,commonElements.get("NavigateBackButton"));
			driver2.navigate().back();
			Thread.sleep(1300);
			
			if(commonFunction.getSizeOfElements(driver2,commonElements.get("HamburgerMenu"))!=0){
				break;
			}
			else{ 
				count++;
				if(count == 7){
					launchGmailApp(driver2);
					break;
				}
			}
		}
	}

	/**
	* Method to launch play store app
	* @author batchi
	 * @throws Exception 
	*/
	public void launchPlayStoreApp(AppiumDriver driver) throws Exception{
		log.info("Launching PlayStore");
		Process p;
		 String[] launchPlayStore = new String[]{"adb","shell", "monkey", "-p", "com.android.vending", "-c", "android.intent.category.LAUNCHER 1"};
	     p = new ProcessBuilder(launchPlayStore).start();
	     log.info("Launched PlayStore");
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
       // adb shell am start -n com.package.name/com.package.name.ActivityName
        
	}
	
	/**
	* Method to launch play store app and update the gmail app version
	* @author batchi
	 * @throws Exception 
	*/
	public void verifyAppIsUpdated(AppiumDriver driver, String keyword) throws Exception{
		log.info("verify App Is Updated or not");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchField"));
		commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("PlayStoreSeachFieldText"), keyword);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchSuggetion"));
		if(commonFunction.getSizeOfElements(driver, commonElements.get("PlayStoreUpdateButton"))!=0){
			commonFunction.searchAndClickByXpath(driver,commonElements.get("PlayStoreUpdateButton") );
			commonFunction.searchAndClickByXpath(driver, commonElements.get("PlayStoreAcceptButton"));
			waitForElementToBePresentByXpath(commonElements.get("PlayStoreOpenButton"));
			commonFunction.searchAndClickByXpath(driver,commonElements.get("PlayStoreOpenButton") );
		}
	}
	/**
	* Method to verify Subject Is Matching
	* @author batchi
	*/	
	public  void verifySubjectIsMatching(AppiumDriver driver2,String subject)throws Exception{
		/*NetworkConnectionSetting networkConnection = new NetworkConnectionSetting(false, true, false);
		networkConnection.setData(false);
		networkConnection.setWifi(false);
		((AndroidDriver)driver2).setNetworkConnection(networkConnection);*/
	      
		try{
			log.info("In Verify Subject Is matching Method");
			//int  x = driver2.findElements(By.xpath(commonElements.get("NotificationDropdown"))).size();
			//verifyAvailabilityOfNotifications(driver2);
			//log.info("Number of notifications "+x);
			int count=12;
			for (int i=1; i<count;i++){
				Thread.sleep(3000);

				if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("NotificationDropdown"))!=0){
					
					
				    break;
				}else{
					Runtime.getRuntime().exec("adb shell service call statusbar 2");
					Thread.sleep(15000);
					
					Runtime.getRuntime().exec("adb shell service call statusbar 1");

				}
				
			}
			log.info("verifying notifications");
			java.util.List<WebElement> x = driver2.findElements(By.xpath(commonElements.get("NotificationDropdown")));
		    for ( WebElement we: x) { 
		    	log.info(we.getText());
		       		if ( we.getText().contains(subject) ){
		       			log.info("Subject is matching");
		       			break;
		       		}
	    }
		    if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("ClickNowNotification"))!=0){
				log.info("Clicking on now from notifications");
		    	CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ClickNowNotification"));
				Thread.sleep(2000);
			}
			//commonPage.waitForElementToBePresentByXpath(commonElements.get("NotificationDropdown"));
			/*String title =commonFunction.searchAndGetTextOnElementByXpath(driver2,commonElements.get("NotificationSubjectLineWithId"));
		    log.info(title);
			log.info(subject);*/
			/*for(int i=0; i<=x;i++){
			
			if(title!=null){
				if(title.equalsIgnoreCase(subject)){
					log.info("Subject is matching");
					break;
				}
				else{
					log.info("&&&&&&&&&&&&&&&&&&&&&&& Subject Lines are  not matching &&&&&&&&&&&&&&&&&&&&&&& ");
					//throw new Exception("Notification is not received");
					}	
			}  else{
				log.info("No title available");
			       }
		}*/
		}catch (Throwable e) {
			  commonPage.catchBlock(e, driver2, "verifySubjectIsMatching");
		}
	}
	
	/**
	* Method to clicking on the reply button from notification
	* @author batchi
	*/	
	public  void verifyReplyButtonFromNotification(AppiumDriver driver2)throws Exception{
	try{
		log.info("Verifying reply button from notification");
		Thread.sleep(1500);
		String clickReply= commonPage.commonElements.get("ReplyButtonFromNotification");
		log.info("Verifying for reply button notification");
		if(commonFunction.getSizeOfElements(driver2, clickReply)==0){
			if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("ClickNowNotification"))!=0){
				log.info("Clicking on now from notifications");
		    	CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ClickNowNotification"));
				Thread.sleep(2000);
			}
		} else{
			
			log.info("Swiping down");
			driver2.swipe(328, 516, 328, 590, 1000);
		}
		commonFunction.searchAndClickByXpath(driver2, clickReply) ;
	       log.info("*****************************Clicked on the reply button from notification bar******************************");
		Thread.sleep(1500);
	       if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("ReplyPage"))==true){
	    	log.info("***************************** Compose page for reply has opened ******************************");
	 
	       }
	       }
	catch (Exception e) {
		e.printStackTrace();
		throw new Exception("Unable to find compose page for Reply ");
	}
	}
	
	/**
	 * Method to Open Drafts in Edit View
	 * @author batchi
	 * @throws Exception
	 */
	public static void openDraftsInEditView(AppiumDriver driver, String mailSubject) throws Exception{
		Thread.sleep(1500);
		log.info("Opening drafts Label");
		//CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DraftsOption"));
		log.info("Clicked on Drafts Option");
		//inbox.openMailUsingSearch(driver, mailSubject);
		Thread.sleep(2000);
		inbox.openMail(driver, mailSubject);
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("EditViewIcon"));
		log.info("Clicked on EditViewIcon");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("MoreOptions"));
		log.info("Clicked on MoreOption");
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DiscardMail"));
		log.info("Clicked on DiscardMail");
		Thread.sleep(1500);
		if(commonFunction.getSizeOfElements(driver, commonElements.get("DiscardPopUpOk") )!=0){
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
			log.info("Clicked on Ok button in the pop up");
		}
		Thread.sleep(1500);
		//inbox.verifyMailIsPresent(driver, MailSubject);
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", mailSubject);
		log.info("Verifying the mail is present after discard");
		int count=1;
		while(CommonFunctions.isElementByXpathNotDisplayed(driver, MailXpath)==false)
		{
			//scrollDown(1);
			count++;
			if(count>10)
				throw new Exception("Mail With Subject '" + mailSubject + "' was found");
		}
		//inbox.navigateUntilHamburgerIsPresent(driver);
		//Thread.sleep(3000);	
	}
	
	/**
	 * Method to Open Drafts in TL View
	 * @author batchi
	 * @throws Exception
	 */
	public static void openDraftsInCVView(AppiumDriver driver, String mailSubject) throws Exception{
		/*log.info("Opening drafts Label");
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DraftsOption"));
		log.info("Clicked on Drafts Option");
		*/		inbox.openMail(driver, mailSubject);

		Thread.sleep(1500);
		log.info("Clicking on delete button to discard mail");
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardMailCVView"));
		log.info("clicked on Discard button in CV view");
		Thread.sleep(2000);
if(commonFunction.getSizeOfElements(driver, commonElements.get("DiscardPopUpOk") )!=0){
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
		log.info("Clicked on Ok button in the pop up");
	}	//inbox.navigateUntilHamburgerIsPresent(driver);
	
String MailXpath = commonElements.get("InboxMail").replace("MailSubject", mailSubject);
log.info("Verifying the mail is present after discard");
int count=1;
while(CommonFunctions.isElementByXpathNotDisplayed(driver, MailXpath)==false)
{
	scrollDown(1);
	count++;
	if(count>10)
		throw new Exception("Mail With Subject '" + mailSubject + "' was found");
}
	}
	
	/**
	 * Method to Open Drafts in TL View
	 * @author batchi
	 * @throws Exception
	 */
	public static void openDraftsInTLView(AppiumDriver driver, String mailSubject) throws Exception{
		/*log.info("Opening drafts Label");
		CommonFunctions.searchAndClickByXpath(driver,commonElements.get("DraftsOption"));
		log.info("Clicked on Drafts Option");
		*/
		//inbox.searchForMail(driver, mailSubject);
		//Thread.sleep(1000);
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("SenderImageIcon"));
		log.info("clicked on Sender Image Icon in CV view");
		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
		Thread.sleep(2000);
		if(commonFunction.getSizeOfElements(driver, commonElements.get("DiscardPopUpOk") )!=0){
		CommonFunctions.searchAndClickByXpath(driver, commonElements.get("DiscardPopUpOk"));
		log.info("Clicked on Ok button in the pop up");
		}	
		//driver.navigate().back();
		//driver.navigate().back();
		//driver.navigate().back();
		pullToReferesh(driver);
		String MailXpath = commonElements.get("InboxMail").replace("MailSubject", mailSubject);
		log.info("Verifying the mail is present after discard");
		int count=1;
		while(CommonFunctions.isElementByXpathNotDisplayed(driver, MailXpath)==false)
		{
			scrollDown(1);
			count++;
			if(count>10)
				throw new Exception("Mail With Subject '" + mailSubject + "' was found");
		}
		//inbox.navigateUntilHamburgerIsPresent(driver);
	}
	
	/**
	* Method to Swipe top To bottom
	* @author batchi
	 * @throws Exception 
	*/
	public static void swipeDown(AppiumDriver driver) throws Exception{
		log.info("Swiping from Top  To Bottom");
		Thread.sleep(1000);
        driver.swipe(328, 516, 328, 590, 1000);
       log.info("Swiped down");
	}

	/**
	* Method to clicking on the delete option from notification to open
	* @author batchi
	*/	
	public  void verifyArchiveFromNotification(AppiumDriver driver2)throws Exception{
	try{
		Thread.sleep(1500);
		if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("NotificationArchiveButton"))==0){
			if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("ClickNowNotification"))!=0){
				log.info("Clicking on now from notifications");
		    	CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ClickNowNotification"));
				Thread.sleep(2000);
			}	
		}
		
		commonFunction.searchAndClickByXpath(driver2,commonElements.get("NotificationArchiveButton") ) ;
	       log.info("*****************************Clicked on the Archive button from notification bar******************************");
	       if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("NotificationDescriptionArchived"))==true){
	    	log.info("***************************** Archive description has shown up in Notification bar ******************************");
	       }
	}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Archived description in Notification ");
	       }     
	}
	/**
	* Method to clicking on the mail from notification to open
	* @author batchi
	*/	
	public  void verifyOpenFromNotification(AppiumDriver driver2, String subject)throws Exception{
	try{
		CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("OpenNotification").replace("Subject", subject));
		log.info("*****************************Clicked on the mail to open from notification bar******************************");
	       
	       if(commonFunction.isElementByIdDisplayed(driver2,commonElements.get("ToolBarId"))==true){
			log.info("***************************** Mail is opened as the Tool bar is available ******************************");
	       }
	}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find the ToolBar ");
			}
	}
	
	/**
	* Method to Swipe Right To Left
	* @author batchi
	*/
	public static void swipeRightToLeft(AppiumDriver driver2,String swipeElementName){
		try{
		log.info("Swiping from Right  To Left");
		//WebElement swipeElement1= driver2.findElement(By.xpath("//android.widget.ListView[@resource-id='PageId']".replace("PageId", swipeElementName)));
		//WebElement swipeElement=driver2.findElement(By.id(swipeElementName));
		
		WebElement swipeElement=driver2.findElement(By.xpath(swipeElementName));
		log.info(swipeElement);
	    int wide  = swipeElement.getSize().width;
	int hgt = swipeElement.getSize().height;

	        int startx = (int) (wide * (0.8));
	    int endx = (int)(wide *(0.12));
	int starty =  hgt /2 ;
	    int endy = hgt/2;
	   
	        driver2.swipe(startx, starty, endx, endy, 1000);	
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	/**
	* Method to Swipe Left To Right
	* @author batchi
	*/
	public static void swipeLeftToRight(AppiumDriver driver,String swipeElementName){
		try{
		log.info("Swiping from Left to right");
		WebElement swipeElement=driver.findElement(By.xpath(swipeElementName));
		log.info(swipeElement);
		Dimension size = driver.manage().window().getSize();
		  System.out.println(size);
		  
		  //Find swipe x points from screen's with and height.
		  //Find x1 point which is at right side of screen.
		  int x1 = (int) (size.width * 0.20);
		  //Find x2 point which is at left side of screen.
		  int x2 = (int) (size.width * 0.80);
		  
		  //Create object of TouchAction class.
		  TouchAction action = new TouchAction((MobileDriver)driver);
		  action.longPress(swipeElement).moveTo(x2,580).release().perform();

		
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	/**
	 * Method to verify Label view is open after closing the Hamburger Menu Drawer
	 * @author batchi
	 * @throws Exception
	 */
	public void hamburgerDrawerLableView(AppiumDriver driver2) throws Exception{
		
		boolean x=commonFunction.isElementByXpathDisplayed(driver2, commonElements.get("PrimaryInboxOption"));
		if(x==true){log.info("Hamburger menu  is opened in Label View  ");
		}
		else{log.info("Hamburger menu is not in Label View");}
		
	}
	/**
	 * Method to verify Account view is open after closing the Hamburger Menu Drawer
	 * @author batchi
	 * @throws Exception
	 */
	public void hamburgerDrawerAccountView(AppiumDriver driver) throws Exception{
		if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ExpandAccountList"))!=0){
			commonFunction.searchAndClickByXpath(driver, commonElements.get("ExpandAccountList"));
		    log.info("Expanded Account List");
		    Thread.sleep(3000);
		}
		Map<String, String> hamburgerDrawerAccountViewData = commonPage
				.getTestData("SystemLabels");

	  swipeRightToLeft(driver,hamburgerDrawerAccountViewData.get("LabelViewID"));
	   log.info("Swiping done");
	}
	
	public void verifyAccountView(AppiumDriver driver2) throws Exception{
	    boolean y=commonFunction.isElementByXpathDisplayed(driver2, commonElements.get("AddAccountOption"));
		if(y==false){
			log.info("Hamburger is in Label View");
		}else{
			log.info("Hamburger Menu is in Account  View ");
			}
		
	}
	/**
	* Method to launch chrome browser
	* @author batchi
	 * @throws Exception 
	*/
	public void launchChromeBrowser(AppiumDriver driver2) throws Exception{
		log.info("Launching Chrome");
		Process p;
		 String[] launchChromeBrowser = new String[]{"adb","shell", "monkey", "-p", "com.android.chrome", "-c", "android.intent.category.LAUNCHER 1"};
	     p = new ProcessBuilder(launchChromeBrowser).start();
	     log.info("Launched Chrome Browser");
        driver2.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        closeTabsInChromBrowser(driver2);
        CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("ChromeBrowserSearchURL"),commonElements.get("GoogleGroupsUrl"));
        CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("SearchSuggestion").replace("URL",commonElements.get("GoogleGroupsUrl")));
	}
	
	/**
	* Method to closing all tabs in chrome browser
	* @author batchi
	 * @throws Exception 
	*/
	public void closeTabsInChromBrowser(AppiumDriver driver) throws Exception{
		
		log.info(commonElements.get("URLField").contains(commonElements.get("GoogleGroupsUrl")));
		Thread.sleep(3000);
		if(commonFunction.getSizeOfElements(driver,commonElements.get("URLField"))!=0){
			Thread.sleep(2000);
		commonFunction.searchAndClickByXpath(driver, commonElements.get("ChromeTabSwitcher"));
		Thread.sleep(1000);
		commonFunction.searchAndClickByXpath(driver,commonElements.get("ChromeMenuButton"));
		Thread.sleep(1000);
		commonFunction.searchAndClickByXpath(driver,commonElements.get("ChromeCloseAllTabsOption"));
		Thread.sleep(1000);
		commonFunction.searchAndClickByXpath(driver,commonElements.get("ChromeNewTabButton"));
		//driver.close();
		}
		
	}
	/**
	* Method to post msgs in group 
	* @author batchi
	 * @throws Exception 
	*/
	
	public void exchangePosts(AppiumDriver driver2, String groupName,String subject, String details)throws Exception{
		Map<String, String> verifyDeleteAndUnsubscribeData = commonPage
				.getTestData("GoogleGroups");
		if(commonFunction.getSizeOfElements(driver2,commonElements.get("MyGroupsOption"))!=0){
			commonFunction.searchAndClickByXpath(driver2,commonElements.get("MyGroupsOption"));
			if(commonFunction.getSizeOfElements(driver2,commonElements.get("GroupNameSearch").replace("Gname", groupName))!=0){
			commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupNameSearch").replace("Gname",groupName));
			}if(commonFunction.getSizeOfElements(driver2,commonElements.get("GroupMsgComposeOptionInAppBrowser"))!=0){
				commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupMsgComposeOptionInAppBrowser"));
			}
			else if(commonFunction.getSizeOfElements(driver2,commonElements.get("GroupMsgComposeOptionInChromeBrowser"))!=0){
				commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupMsgComposeOptionInChromeBrowser"));
			}
			commonFunction.searchAndSendKeysByXpath(driver2,commonElements.get("GroupMsgSubject"),subject );
			hideKeyboard();
			commonFunction.searchAndSendKeysByXpath(driver2, commonElements.get("GroupMsgDetails"),details );
			hideKeyboard();
			commonFunction.searchAndClickByXpath(driver2,commonElements.get("PostButton"));		
		}
		
	}
	/**
	* Method to verify if the user is signed in  or not and also account logged in Gmail app should  not  be same in groups login
	* @author batchi
	 * @throws Exception 
	*/
	public void verifyLoggedIntoGroup(AppiumDriver driver2, String groupName,String subject, String details) throws Exception{
		log.info("**************** In verifyLoggedIntoGroup ***************** ");
		Map<String, String> verifyLoggedIntoGroupData = commonPage
				.getTestData("GoogleGroups");
		Thread.sleep(2000);
		if(commonFunction.getSizeOfElements(driver2,commonElements.get("GroupsSignIn"))!=0){
			log.info("Sign in available");
					//commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupsSignIn"));
			googleGroupsLogin(driver2, verifyLoggedIntoGroupData.get("UnameField"), verifyLoggedIntoGroupData.get("PwdField"));
			commonPage.createGoogleGroup(driver2,
					verifyLoggedIntoGroupData.get("gName1"),
					verifyLoggedIntoGroupData.get("gAddress"),
					verifyLoggedIntoGroupData.get("gDesc"),
					verifyLoggedIntoGroupData.get("inviteId"),
					verifyLoggedIntoGroupData.get("inviteMsg"),
					verifyLoggedIntoGroupData.get("XCoordinateforCreateGroup"),
					verifyLoggedIntoGroupData.get("YCoordinateforCreateGroup"));
			driver2.close();
			inbox.verifyGmailAppInitState(driver2);
			if(!CommonFunctions.isAccountAlreadySelected(verifyLoggedIntoGroupData.get("inviteId")))
				userAccount.switchMailAccount(driver2, verifyLoggedIntoGroupData.get("inviteId"));
			
			inbox.openMailUsingSearch(driver2, verifyLoggedIntoGroupData.get("mailSubject"));
			commonPage.scrollDown(2);
			commonPage.acceptInvitation(driver2);
			commonPage.unableToJoinGroup(driver2);
		commonPage.exchangePosts(driver2,groupName,subject,details);
		}
		else if(commonFunction.getSizeOfElements(driver2, commonElements.get("GroupSwitchAccount"))==0){
			log.info("verifying Group Switch account icon");
			googleGroupsLogin(driver2, verifyLoggedIntoGroupData.get("UnameField"), verifyLoggedIntoGroupData.get("PwdField"));
			commonPage.createGoogleGroup(driver2,
					verifyLoggedIntoGroupData.get("gName"),
					verifyLoggedIntoGroupData.get("gAddress"),
					verifyLoggedIntoGroupData.get("gDesc"),
					verifyLoggedIntoGroupData.get("inviteId"),
					verifyLoggedIntoGroupData.get("inviteMsg"),
					verifyLoggedIntoGroupData.get("XCoordinateforCreateGroup"),
					verifyLoggedIntoGroupData.get("YCoordinateforCreateGroup"));
			driver2.close();
			inbox.verifyGmailAppInitState(driver2);
			if(!CommonFunctions.isAccountAlreadySelected(verifyLoggedIntoGroupData.get("inviteId")))
			userAccount.switchMailAccount(driver2, verifyLoggedIntoGroupData.get("inviteId"));
			inbox.openMailUsingSearch(driver2, verifyLoggedIntoGroupData.get("mailSubject"));
			commonPage.scrollDown(2);
			commonPage.acceptInvitation(driver2);
			commonPage.unableToJoinGroup(driver2);
			commonPage.exchangePosts(driver2,groupName,subject,details);
		}
		
		else if(commonFunction.getSizeOfElements(driver2,commonElements.get("MyGroupsOption") )!=0){
			log.info("verifying <My group> option");
			Thread.sleep(2000);
			/*commonPage.createGoogleGroup(driver2,
					verifyLoggedIntoGroupData.get("gName"),
					verifyLoggedIntoGroupData.get("gAddress"),
					verifyLoggedIntoGroupData.get("gDesc"),
					verifyLoggedIntoGroupData.get("inviteId"),
					verifyLoggedIntoGroupData.get("inviteMsg"),
					verifyLoggedIntoGroupData.get("XCoordinateforCreateGroup"),
					verifyLoggedIntoGroupData.get("YCoordinateforCreateGroup"));
			driver2.close();
			inbox.verifyGmailAppInitState(driver2);
			userAccount.switchMailAccount(driver2, verifyLoggedIntoGroupData.get("inviteId"));
			inbox.openMailUsingSearch(driver2, verifyLoggedIntoGroupData.get("mailSubject"));
			commonPage.scrollDown(2);
			commonPage.acceptInvitation(driver2);
			commonPage.unableToJoinGroup(driver2);*/
			commonPage.exchangePosts(driver2,groupName,subject,details);
			
		}
		
		/*else if(commonFunction.getSizeOfElements(driver2, commonElements.get("GroupSwitchAccount"))!=0){
	     	googleGroupsLogin(driver2, verifyLoggedIntoGroupData.get("UnameField"), verifyLoggedIntoGroupData.get("PwdField"));
		}*/	//Switch Account option
				/*else if(!commonElements.get("GroupCurrentLoginId").equalsIgnoreCase(verifyLoggedIntoGroupData.get("inviteId"))){
					Thread.sleep(5000);
		
					log.info("In same group account");
					
								//WebElement e = driver2.findElement(By.xpath("//android.view.View[@content-desc='Switch accounts']"));
								//log.info("found index 11");
								//driver2.tap(364, e, 924);
								
								//driver2.findElement(By.xpath(""))
								
							    //WebView(driver2);
								commonFunction.searchAndClickByXpath(driver2,commonElements.get("MyGroupsOption"));
								log.info("Clicked on MyGroup option ");
					CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("GroupSwitchAccount"));
								String x = driver2.findElement(By.xpath(commonElements.get("GroupSwitchAccount"))).getAttribute("content-desc");
								log.info(x);
									Thread.sleep(5000);
								
					log.info("Clicked on switch account");
					userAccount.switchMailAccount(driver2,verifyLoggedIntoGroupData.get("UnameField"));
				}*/
				//For Desktop option
				/*if(CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("DesktopOption") )== true){
				//CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("DesktopOption"));
					 closeTabsInChromBrowser(driver2);	
				commonFunction.searchAndSendKeysByXpath(driver2,commonElements.get("ChromeBrowserSearchURL"), commonElements.get("DesktopOptionPageURL"));
				commonFunction.searchAndClickByXpath(driver2,commonElements.get("SearchSuggestion").replace("URL",commonElements.get("DesktopOptionPageURL")));
				
			}
		else{
			log.info("Unable to find the Desktop icon");
		}	*/
	}
	
	/**
	* Method to create a group 
	* @author batchi
	 * @throws Exception 
	*/	
		
	public void createGoogleGroup(AppiumDriver driver2, String gName,String gAddress, String gDesc, String inviteId,String inviteMsg,String x, String y) throws Exception{
	  // WebView(driver2);
	   /*driver2.findElement(By.xpath("//span[contains(@text,'Create group')]")).click();
	   log.info("Clicked");*/
	  //CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("CreateGoogleGroup"));
	Thread.sleep(2000);
		log.info("In create group method");
	 commonFunction.searchAndSendKeysByXpath(driver2,commonElements.get("URLField") , commonElements.get("DesktopOptionPageURL"));
     commonFunction.searchAndClickByXpath(driver2, commonElements.get("SearchSuggestion"));
	 Thread.sleep(2000);
	 //tapUsingXYCordinates(driver2, x, y,commonElements.get("CreateGoogleGroup"));
    //commonPage.zoomAnElementInChrome(driver2);
    /*WebElement e=driver2.findElement(By.className("F0XO1GC-x-b F0XO1GC-x-d"));
    e.click();*/
    //commonFunction.searchAndClickByXpath(driver2, commonElements.get("CreateGoogleGroup"));
	 commonFunction.searchAndSendKeysByXpath(driver2,commonElements.get("URLField") , commonElements.get("CreateGroupPageURL"));
     commonFunction.searchAndClickByXpath(driver2, commonElements.get("SearchSuggestion"));
     
	   CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("GroupNameField"),gName +getCurrentTimeStamp().replace(":","_").replace(".","_") );
	   CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("GroupEmailAddress"), gAddress);
	   CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("GroupDesc"), gDesc);
	   CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("CreateButton"));
	   verifyCaptcha(driver2);
		if(!CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("GroupCreatedDialog"))== false){
			CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("InviteToJoin"));
			CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("EnterIdToInvite"), inviteId);
			
			CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("EnterInvitationMsg"), inviteMsg);
			CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("SendInvitation"));
			verifyCaptcha(driver2);
			CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("InvitationsSent"));
		} else{
			
			log.info("Group is not created");
		}
	}
	/**
	* Method to verify Captcha dialog appears and performs action on it. 
	* @author batchi
	 * @throws Exception 
	*/	
	
	public static void verifyCaptcha(AppiumDriver driver2) throws Exception{
		if(!CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("CaptchaDialog"))== false){
			   CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("NotARobotCheckBox"));
			   CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("ContinueButton"));
		   }else{
			   log.info("No Captcha Dialog available");
		   }
		
	}
	
	/**
	 * Method to Open google groups and Login
	 * @author batchi
	 * @throws Exception
	 */
	public void googleGroupsLogin(AppiumDriver driver2,String uname, String pwd) throws Exception{
		//((AndroidDriver)driver2).startActivity("com.android.chrome", "org.chromium.chrome.browser.ChromeTabbedActivity");
        // if(CommonFunctions.isElementByXpathDisplayed(driver2,commonElements.get("GroupsSignIn"))== true){
        	 
        	 closeTabsInChromBrowser(driver2);
		        	 /*commonFunction.searchAndClickByXpath(driver2, commonElements.get("URLField"));
		        	 log.info("Clicked on url to delete");
		        	 commonFunction.searchAndClickByXpath(driver2,commonElements.get("ChromeDeleteButton"));
		        	 */
        	 commonFunction.searchAndSendKeysByXpath(driver2,commonElements.get("ChromeBrowserSearchURL"), commonElements.get("SigninPageURL"));
        	 Thread.sleep(2000);
        	 log.info(commonElements.get("SigninPageURL"));
        	 commonFunction.searchAndClickByXpath(driver2,commonElements.get("SearchSuggestion").replace("URL",commonElements.get("SigninPageURL")));
        	 		//CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("GroupsSignIn"));
        	//commonFunction.searchAndClickByXpath(driver2,commonElements.get("UidOfTestAccount1")); 
        	//log.info("Clicked on etouchmotogonea@gmail.com account");
        	
        	 /* WebElement imagesopt = driver2.findElement(By.xpath(commonElements.get("UseAnotherAccount"))); 
        	  ((JavascriptExecutor)driver2).executeScript("au.getElement('" + ((RemoteWebElement) imagesopt).getId() + "').tapWithOptions({tapCount:2});");
        	*/
        	 /*driver2.context("NATIVE_APP");
        	 Thread.sleep(2000);
        	PreciseTap(driver2, 0.0, 1004.0);*/
        	
        	if(commonFunction.getSizeOfElements(driver2, commonElements.get("GroupSwitchAccount"))!=0){
        	commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupSwitchAccount"));
        	log.info("Clicked on Group switch icon");
		    CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("UseAnotherAccount"));
			log.info("Clicked on the Use another account option");
        	}
            	
        	CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("MailIdField"),uname );
			hideKeyboard();
			CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("AddingGoogleAccountUsernameNextButton"));
			CommonFunctions.searchAndSendKeysByXpath(driver2,commonElements.get("AddingGoogleAccountPasswordField"), pwd);
			hideKeyboard();
			CommonFunctions.searchAndClickByXpath(driver2,commonElements.get("AddingGoogleAccountPasswordNextButton"));
		//}
		
		/*else{
			log.info("Unable to find the Sign in option");
		}		
		*/
	}
/**
 * Method to get current timestamp
 * @author batchi
 * @throws Exception
 */
	 public static String getCurrentTimeStamp() {
	        SimpleDateFormat sdfDate = new SimpleDateFormat(
	                "yyyy-MM-dd HH:mm:ss.SSS");// dd/MM/yyyy
	        Date now = new Date();
	        String strDate = sdfDate.format(now);
	        return strDate;
	    }
	 
	 /**
		* Method to accept invitation
		* @author batchi
		 * @throws Exception 
		*/	
		public void acceptInvitation(AppiumDriver driver2) throws Exception{
			commonFunction.searchAndClickByXpath(driver2,commonElements.get("AcceptInvitationFromMail"));
			
		}
		/**
		* Method to verify if the user is unable to join group and sign in 
		* @author batchi
		 * @throws Exception 
		*/
		public void unableToJoinGroup(AppiumDriver driver2) throws Exception{
			Map<String, String> verifyDeleteAndUnsubscribeData = commonPage
					.getTestData("GoogleGroups");
			if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("CannotSignIn"))==true){
				commonFunction.searchAndClickByXpath(driver2,commonElements.get("CannotSignIn"));
				googleGroupsLogin(driver2, verifyDeleteAndUnsubscribeData.get("inviteId"), verifyDeleteAndUnsubscribeData.get("PwdField"));
			}
			else if(commonElements.get("GroupCurrentLoginId").equalsIgnoreCase(verifyDeleteAndUnsubscribeData.get("inviteId"))){
				log.info("In same group account");
			}
			else{
				commonFunction.searchAndClickByXpath(driver2,commonElements.get("GroupSwitchAccount"));
				if(!CommonFunctions.isAccountAlreadySelected(verifyDeleteAndUnsubscribeData.get("inviteId")))
				userAccount.switchMailAccount(driver2,verifyDeleteAndUnsubscribeData.get("inviteId"));
			}
			
		}
		
		public void clickConfidential(AppiumDriver driver)throws Exception{
			try{
				Thread.sleep(5000);
				if(CommonFunctions.getSizeOfElements(driver, commonElements.get("ConfidentialAlert"))!=0){
					CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ConfidentialAlert"));
					log.info("Clicked on Ok, Got it");
				}
			}catch(Exception e){
				throw new Exception("Unable to find the confidential alert "+e.getLocalizedMessage());
			}
		}
		
		/**
		* Method to verifying Sender name in notification dropdown
		* @author batchi
		 * @throws Exception 
		*/

		public static void verifyNotificationSenderName(AppiumDriver driver) throws Exception{
			try{
			log.info("In verify Notification Sender Name ");
			if(CommonFunctions.getSizeOfElements(driver, commonElements.get("InboxCategoriesList"))!=0){
				log.info("*************************** Sender name is displayed **************************************");
				
			}else{
				
				log.info("Sender Name is not displayed");
			}
			}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Sender Name ");
			} 
		 }
		/**
		* Method to verifying Time in notification dropdown
		* @author batchi
		 * @throws Exception 
		*/

		public static void verifyNotificationTime(AppiumDriver driver) throws Exception{
			try{
			log.info("In verify Notification Time element ");
			if(CommonFunctions.isElementByIdDisplayed(driver, commonElements.get("NotificationTimeElement"))){
				log.info("*************************** Time is displayed **************************************");
				
			}else{
				
				log.info("Time is not displayed");
			}
			}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Time element ");
			} 
		 }
		
		/**
		* Method to verifying Mail Subject in notification dropdown
		* @author batchi
		 * @throws Exception 
		*/

		public static void verifyNotificationMailSubject(AppiumDriver driver) throws Exception{
			try{
			log.info("In verify Notification Mail Subject ");
			if(CommonFunctions.isElementByXpathDisplayed(driver, commonElements.get("NotificationSubjectLineWithId"))){
				log.info("*************************** Subject line is displayed **************************************");
				
			}else{
				
				log.info("Subject line is not displayed");
			}
			}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Subject Line in Notification ");
			} 
		 }
	
		/**
		* Method to click  on the clear all notifications icon
		* @author batchi
		 * @throws Exception 
		*/
		public static void clearAllNotifications(AppiumDriver driver) throws Exception{
			try{
			log.info("In clear All Notifications method ");
			CommonFunctions.searchAndClickByXpath(driver, commonElements.get("ClearNotificationButton"));

			}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Clear icon in Notification ");
			} 
		}
		/**
		* Method to click  on the clear all notifications icon
		* @author batchi
		 * @throws Exception 
		*/
		public static void verifyMuteOption(AppiumDriver driver,String mailSubject) throws Exception{
			try{
			log.info("verifyMuteOption In verify report spam and mute method ");
			
			inbox.openMail(driver, mailSubject);
			commonFunction.searchAndClickByXpath(driver, commonElements.get("MailMoreOptionsNavigation"));
			commonFunction.searchAndClickByXpath(driver, commonElements.get("ReportSpamInMoreOption"));
			Thread.sleep(4000);
			if(commonFunction.getSizeOfElements(driver, commonElements.get("ReportSpamInToast"))!=0){
				log.info("################# Report Spam option is available in Toast ################# ");
			}else{
				throw new Exception("Unable to find ReportSpam Option in toast ");
			}
			if(commonFunction.getSizeOfElements(driver, commonElements.get("MuteOptionInToast"))!=0){
				log.info("@@@@@@@@@@@@@@@@@ Mute option is available in Toast @@@@@@@@@@@@@@@@@");
			}else{
				throw new Exception("Unable to find Mute option in Toast ");
			}
			if(commonFunction.getSizeOfElements(driver, commonElements.get("MuteInstead"))!=0){
				log.info("@@@@@@@@@@@@@@@@@ Mute Instead Text in Toast @@@@@@@@@@@@@@@@@");
			}else{
				throw new Exception("Unable to find Mute Instead ");
			}
			if(commonFunction.getSizeOfElements(driver, commonElements.get("MuteText"))!=0){
				log.info("@@@@@@@@@@@@@@@@@ Mute Text in Toast @@@@@@@@@@@@@@@@@");
			}else{
				throw new Exception("Unable to find Mute Text ");
			}
			}catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Report mute alert"); 
			} 
		}
		
		/**
		* Method to verify availability of Notifications  
		* @author batchi
		 * @throws Exception 
		*/
		public void verifyAvailabilityOfNotifications(AppiumDriver driver2) throws Exception{
		  commonFunction.switchOnAirPlaneMode();
	  	Thread.sleep(5000);		
	      commonFunction.switchOffAirPlaneMode();
	      log.info("Clicked on Air Plan mode");
		int  x = driver2.findElements(By.xpath(commonElements.get("NotificationDropdown"))).size();
		if(x==2){
			log.info("No Notification available");
			
		}else{
			log.info("Notifications Avaialable");
		}
		log.info(x);
		}
		
		/**
		* Method to verify availability of Notifications  
		* @author batchi
		 * @throws Exception 
		*/
		public void verifyNoNotifications(AppiumDriver driver2, String Subject) throws Exception{
		  try{
			  Thread.sleep(1000);
			  CommonFunctions.isElementByXpathNotDisplayed(driver2, commonElements.get("NotificationSubject").replace("EmailSubject", Subject));
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		}
		
		
		
		
		/**
		* Method to clicking on the delete option from notification to open
		* @author batchi
		*/	
		public  void verifyDeleteFromNotification(AppiumDriver driver2)throws Exception{
			  
			try{
				Thread.sleep(1500);
				if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("NotificationDeleteButton"))==0){
					if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("ClickNowNotification"))!=0){
						log.info("Clicking on now from notifications");
				    	CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ClickNowNotification"));
						Thread.sleep(2000);
					}	
				}
				
			   commonFunction.searchAndClickByXpath(driver2,commonElements.get("NotificationDeleteButton") ) ;
		       log.info("*****************************Clicked on the Delete button from notification bar******************************");
		       if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("NotificationDescriptionDeleted"))==true){
		    	log.info("***************************** Deleted description has shown up in Notification bar ******************************");
		       }
		       else{
		    	   
		    	   log.info("&&&&&&&&&&&&&&&&&&&&&&&&&& Deleted description is not displayed in Notification bar &&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		       }
			} 
			catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Unable to find Deleted description ");
			} 
		      
		}
		
		/**
		* Method to clicking on the Reply All from notification to open
		* @author batchi
		*/	
		public  void verifyReplyAllFromNotification(AppiumDriver driver2)throws Exception{
			try{  
				Thread.sleep(1500);
				if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("NotificationArchiveButton"))==0){
					if(CommonFunctions.getSizeOfElements(driver2, commonElements.get("ClickNowNotification"))!=0){
						log.info("Clicking on now from notifications");
				    	CommonFunctions.searchAndClickByXpath(driver2, commonElements.get("ClickNowNotification"));
						Thread.sleep(2000);
					}	
				}
			commonFunction.searchAndClickByXpath(driver2,commonElements.get("NotificationReplyAllButton") ) ;
		       log.info("*****************************Clicked on the Reply All button from notification bar******************************");
		       if(commonFunction.isElementByXpathDisplayed(driver2,commonElements.get("ReplyAllPage"))==true){
		    	log.info("***************************** Compose page for Reply All has opened ******************************");
		       }
			}
		       catch (Exception e) {
					e.printStackTrace();
					throw new Exception("Unable to find Compose page for Reply All ");
				}
		}
		
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGmailDefaultActionSettingsOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				if(CommonFunctions.getSizeOfElements(driver,commonElements.get("GeneralSettingsGMailDefaultAction"))!=0){
					  log.info("******************* Gmail Default Action Option is displayed ****************");
					  commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsGMailDefaultAction"));
					  Thread.sleep(1500);
					 if(commonFunction.getSizeOfElements(driver, commonElements.get("GeneralSettingsGMailDefaultActionArchive"))!=0){
						 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Default Action Archive Option is displayed @@@@@@@@@@@@@@"); 
						 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsGMailDefaultActionArchive"));
						 commonPage.navigateUntilHamburgerIsPresent(driver);
						// inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject"));
						 inbox.swipeToDeleteOrArchiveMail(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
						 log.info("Swiped for Archive option");
						 commonFunction.searchAndClickByXpath(driver, commonElements.get("SwipeUndo"));
						 Thread.sleep(1000);
						 inbox.verifyMailIsPresent(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
					 }else{
						 throw new Exception("Unable to find GeneralSettingsGMailDefaultActionArchive");
					 }
						 
					 }else{
						 throw new Exception("Unable to find GeneralSettingsGMailDefaultAction");
					 }
				userAccount.navigateToGeneralSettings(driver);
				 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
				 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsGMailDefaultAction"));
				 Thread.sleep(1500);
				if(commonFunction.getSizeOfElements(driver, commonElements.get("GeneralSettingsGMailDefaultDelete"))!=0){
					 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Default Action Delete Option is displayed @@@@@@@@@@@@@@"); 
					 commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingsGMailDefaultDelete"));
					 commonPage.navigateUntilHamburgerIsPresent(driver);
					 
						// inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject"));
						 inbox.swipeToDeleteOrArchiveMail(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
						 log.info("Swiped for Delete option");
						 commonFunction.searchAndClickByXpath(driver, commonElements.get("SwipeUndo"));
						 
				 }
				else{
					 throw new Exception("Unable to find GeneralSettingsGMailDefaultDelete ");
				 }
					
				 // commonFunction.searchAndClickByXpath(driver, commonElements.get("NotificationCancelOption"));
			     userAccount.navigateToGeneralSettings(driver);
				 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
				 /*commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsGMailDefaultAction"));
				 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsGMailDefaultActionArchive"));
				*/log.info("**************Testing Default action is completed*******************");
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGmailDefaultActionSettingsOption "
								+ e.getMessage());
			}
		}
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		/*public void verifyGeneralSettingsConversationViewOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
		
				 if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConversationViewOption"))!=0){
					  log.info("******************* Gmail Conversation View Option is displayed ****************");
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					  inbox.verifyIfCheckboxIsUnChecked(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					  commonPage.navigateUntilHamburgerIsPresent(driver);
					  userAccount.verifyIMAPAccountAdded(driver, verifyGeneralSettingsOptionsData.get("RediffAccount"), verifyGeneralSettingsOptionsData.get("ImapPwd"));
					  userAccount.switchMailAccount(driver, verifyGeneralSettingsOptionsData.get("RediffAccount"));
					  inbox.composeAndSendNewMail(driver, 
							  verifyGeneralSettingsOptionsData.get("SwitchAccountId"), 
							  verifyGeneralSettingsOptionsData.get("CC"),
							  verifyGeneralSettingsOptionsData.get("BCC"),
							  verifyGeneralSettingsOptionsData.get("MailLink"),
							  verifyGeneralSettingsOptionsData.get("ComposeBody"));
					  Thread.sleep(2000);
					  userAccount.switchMailAccount(driver, verifyGeneralSettingsOptionsData.get("SwitchAccountId"));
					  inbox.pullToReferesh(driver);
					  Thread.sleep(3500);
					  inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("MailLink") );
					  //inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("MailLink"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("ReplyButton"));
					  commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("ComposeBody"), verifyGeneralSettingsOptionsData.get("ComposeBody"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("SendButton"));
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("AcceptLabel"))!=0){
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
					  }
					  Thread.sleep(2000);
					  commonPage.navigateUntilHamburgerIsPresent(driver);
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					  userAccount.switchMailAccount(driver,verifyGeneralSettingsOptionsData.get("RediffAccount"));
					  inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("MailLink") );
					  //inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("MailLink"));
					  String replyText= commonFunction.searchAndGetTextOnElementById(driver, commonElements.get("SubjectInCV"));
					  if(replyText.contains("Re")){
						  log.info("***************************** Individual Mails are displayed *****************************");
						  commonPage.navigateUntilHamburgerIsPresent(driver);
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
						  }else{
						  throw new Exception("Conversation view checkbox is enabled" ); 
						  
					  }
					  
				  } 
				   userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					 commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					  userAccount.switchMailAccount(driver, verifyGeneralSettingsOptionsData.get("SwitchAccountId"));
					
				log.info("***************Testing gmail conversation is done******************");
			
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsConversationViewOption "
								+ e.getMessage());
			}
		}
		*/
		public void verifyGeneralSettingsConversationViewOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
		
				 if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConversationViewOption"))!=0){
					  log.info("******************* Gmail Conversation View Option is displayed ****************");
					 String text = "Conversation view";
					 AndroidElement cv = null;
					  for(int i=0;i<=10;i++){
						String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
						  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
						if	(settingOption.equalsIgnoreCase(text)){
							 cv = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout[@index='1']"
									+ "/android.widget.CheckBox[@index='0']"));
							if (cv.getAttribute("checked").equalsIgnoreCase("false")) {
								log.info("Checkbox is not checked");
								cv.click();
								log.info("Checkbox is now checked");
							}else{
								log.info("Checkbox already checked");
								cv.click();
								log.info("Checkbox is unchecked");
								cv.click();
								log.info("Checkbox checked again to verify to no crash");
							}
							cv.click();
							log.info("Unchecking to verify Conversation view option functionality");
							break;
						}  
					  }
					//  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					//  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					//  inbox.verifyIfCheckboxIsUnChecked(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					  commonPage.navigateUntilHamburgerIsPresent(driver);
					  
					  if(!CommonFunctions.isAccountAlreadySelected(verifyGeneralSettingsOptionsData.get("RediffAccount")))
					  userAccount.switchMailAccount(driver,verifyGeneralSettingsOptionsData.get("RediffAccount"));
					  inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("MailLink") );
					 log.info("Opening searched mail");
					//  inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("MailCoversation"));
					  Thread.sleep(2500);
					  int count;
					  log.info("Veriyfing mail is opened without conversation view off");
					  count = driver.findElements(By.xpath("//android.widget.ImageView[@resource-id='com.google.android.gm:id/contact_badge'] |"
					  		+ "//android.widget.ImageView[@resource-id='com.google.android.gm.lite:id/contact_badge'] | //android.view.View")).size();
					  log.info("count :"+count);
					  // if(count == 1){ //Commmented by Venkat on 23/11/2018
					  if(count > 1){
						  //log.info("Mail is not opened with conversation view option");
						  log.info("Mail is opened with conversation view option");
					  }else{
						  throw new Exception("Mails are opened in a single list");
					  }
					  driver.navigate().back();
					  driver.navigate().back();
					//  driver.navigate().back();
					  
					  userAccount.navigateToGeneralSettings(driver);
						 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
						// commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
						 cv.click();
						 navigateUntilHamburgerIsPresent(driver);
						  inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("MailLink") );

						 // inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("MailCoversation"));
						  Thread.sleep(2500);
						  log.info("Veriyfing mail is opened without conversation view on");
						  count = driver.findElements(By.xpath("//android.widget.ImageView[@resource-id='com.google.android.gm:id/contact_badge'] |"
					  		+ "//android.widget.ImageView[@resource-id='com.google.android.gm.lite:id/contact_badge']")).size();
						  if(count > 1){
							  log.info("Mail is opened with conversation view option");
						  }else{
							  throw new Exception("Mails are not opened in a single list");
						  }
					  //String replyText= commonFunction.searchAndGetTextOnElementById(driver, commonElements.get("SubjectInCV"));
					  /*if(replyText.contains("Re")){
						  log.info("***************************** Individual Mails are displayed *****************************");
						  commonPage.navigateUntilHamburgerIsPresent(driver);
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
						  }else{
						  throw new Exception("Conversation view checkbox is enabled" ); 
						  
					  }*/
					  
				  } 
				   /*userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					 commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConversationViewCheckBox"));
					*/ 
				 driver.navigate().back();
				  driver.navigate().back();
				 //navigateUntilHamburgerIsPresent(driver);
				  if(!CommonFunctions.isAccountAlreadySelected(verifyGeneralSettingsOptionsData.get("SwitchAccountId")))
					  userAccount.switchMailAccount(driver, verifyGeneralSettingsOptionsData.get("SwitchAccountId"));
					  userAccount.navigateToGeneralSettings(driver);
						 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsConversationViewOption "
								+ e.getMessage());
			}
		}
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsSwipeActionOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsSwipeActionOption"))!=0){
					  log.info("******************* Gmail Swipe Action Option is displayed ****************");
				}else{
					CommonFunctions.scrollToCellByTitle(driver, "Swipe actions");
				}
					  String text = "Swipe actions";
						 AndroidElement sa = null;
						  for(int i=0;i<=10;i++){
							String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
							  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
							if	(settingOption.equalsIgnoreCase(text)){
								 sa = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout[@index='1']"
										+ "/android.widget.CheckBox[@index='0']"));
								if (sa.getAttribute("checked").equalsIgnoreCase("false")) {
									log.info("Checkbox is not checked");
									sa.click();
									log.info("Checkbox is now checked");
								}else{
									log.info("Checkbox already checked");
									sa.click();
									log.info("Checkbox is unchecked");
									sa.click();
									log.info("Checkbox checked again to verify to no crash");
								}
								sa.click();
								log.info("Unchecking to verify Conversation view option functionality");
								break;
							}  
						  }
					  
					/*  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingSwipeActionsCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingSwipeActionsCheckBox"));
					  inbox.verifyIfCheckboxIsUnChecked(driver, commonElements.get("GeneralSettingSwipeActionsCheckBox"));
					*/  
					commonPage.navigateUntilHamburgerIsPresent(driver);
					 // userAccount.switchMailAccount(driver,verifyGeneralSettingsOptionsData.get("ToMailId"));
					  inbox.verifySwipe(driver,verifyGeneralSettingsOptionsData.get("mailSubject"));
				  
				 userAccount.navigateToGeneralSettings(driver);
				 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
				// commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingSwipeActionsCheckBox"));
				 sa.click(); 
				 log.info("******************* Completed Gmail Swipe Action Option is displayed ****************");
				
			
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsSwipeActionOption "
								+ e.getMessage());
			}
		}
		
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsSenderImageOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsSenderImageOption"))!=0){
					  log.info("******************* Gmail Sender Image Option is displayed ****************");
				}else{
					CommonFunctions.scrollToCellByTitle(driver, "Sender image");
				}
					  
					  
					  String text = "Sender image";
						 AndroidElement si = null;
						  for(int i=0;i<=10;i++){
							String settingOption =  driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.RelativeLayout[@index='0']"
							  		+ "/android.widget.TextView[@resource-id='android:id/title']")).getText();
							if	(settingOption.equalsIgnoreCase(text)){
								si = (AndroidElement) driver.findElement(By.xpath("//android.widget.LinearLayout[@index='"+i+"']/android.widget.LinearLayout[@index='1']"
										+ "/android.widget.CheckBox[@index='0']"));
								if (si.getAttribute("checked").equalsIgnoreCase("false")) {
									log.info("Checkbox is not checked");
									si.click();
									log.info("Checkbox is now checked");
								}else{
									log.info("Checkbox already checked");
									si.click();
									log.info("Checkbox is unchecked");
									si.click();
									log.info("Checkbox checked again to verify to no crash");
								}
								si.click();
								log.info("Unchecking to verify Sender image option functionality");
								break;
							}  
						  }
					  /* inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingSenderImageCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingSenderImageCheckBox"));
					  inbox.verifyIfCheckboxIsUnChecked(driver, commonElements.get("GeneralSettingSenderImageCheckBox"));
					 */ commonPage.navigateUntilHamburgerIsPresent(driver);
					  log.info("*************Verifying the sender image*****************");
					  pullToReferesh(driver);
					  Thread.sleep(3000);

					  /*if(commonFunction.getSizeOfElements(driver, commonElements.get("SenderImageIcon"))==0){
						  log.info("******************* Sender Image Icon is diabled *********************"); 
					  }else{
						  log.info("Sender images are still displaying");
						  throw new Exception("Sender Image icon is enabled" ); 
					  }*/
					  
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("TouchAndHoldText"))!=0){
						  log.info("*********Sender Image icon is enabled******************");
					  }else{
						  log.info("Sender images are still displaying");
						  throw new Exception("Sender Image icon is enabled" ); 
					  }
					  
				  
				 	 userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					// commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingSenderImageCheckBox"));
					 si.click();
			}catch (Exception e) {
				 userAccount.navigateToGeneralSettings(driver);
				 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
				 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingSenderImageCheckBox"));
				e.printStackTrace();
			}
		}
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyDefaultReplyActionOption(AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				 if(commonFunction.getSizeOfElements(driver,commonElements.get("DefaultReplyAction"))!=0){
					  log.info("******************* Gmail Default Reply Action Option is displayed ****************");
					  commonFunction.searchAndClickByXpath(driver,commonElements.get("DefaultReplyAction"));
					  Thread.sleep(1500);
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("ReplyDefaultReplyAction"))!=0){
							 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Default Reply Action Reply Option is displayed @@@@@@@@@@@@@@"); 
							 commonFunction.searchAndClickByXpath(driver, commonElements.get("ReplyDefaultReplyAction"));
							 commonPage.navigateUntilHamburgerIsPresent(driver);
					          inbox.pullToReferesh(driver);

					         // driver.findElement(By.xpath("//android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout[0]/android.view.View[1]")).click();
							 inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject") );
							 Thread.sleep(2000);
						 if(commonFunction.getSizeOfElements(driver,commonElements.get("ReplyImageSide"))!=0){
							 log.info("Clicking on Reply icon");
							 commonFunction.searchAndClickByXpath(driver,commonElements.get("ReplyImageSide") );
							 log.info("Clicked on Reply icon");
							  if(commonFunction.searchAndGetTextOnElementByXpath(driver, commonElements.get("ReplyPage")).equalsIgnoreCase("Reply")){
								  log.info("Reply Page is displayed");
								 
							  }else{
									 throw new Exception("Reply Page should be available" ); 
								 }
							  
							 }
							  else{
								 
								 throw new Exception("Reply icon is not available" ); 
							 } 
					  commonPage.navigateUntilHamburgerIsPresent(driver);
					  log.info("Navigated to Hamburger");
					 // commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					  userAccount.navigateToGeneralSettings(driver);
					  commonFunction.searchAndClickByXpath(driver,commonElements.get("DefaultReplyAction"));
					  Thread.sleep(1500);
						 if(commonFunction.getSizeOfElements(driver,commonElements.get("ReplyAllDefaultReplyAction"))!=0){
							 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Default Reply Action ReplyAll Option is displayed @@@@@@@@@@@@@@"); 
							 commonFunction.searchAndClickByXpath(driver, commonElements.get("ReplyAllDefaultReplyAction"));
							 commonPage.navigateUntilHamburgerIsPresent(driver);
					          inbox.pullToReferesh(driver);
							 inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
							 //inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject") );
							 Thread.sleep(2000);
							 if(commonFunction.getSizeOfElements(driver,commonElements.get("ReplyAllImageSide"))!=0){
								 commonFunction.searchAndClickByXpath(driver,commonElements.get("ReplyAllImageSide") );
							 log.info("Clicked on ReplyAll icon");
							  if(commonFunction.searchAndGetTextOnElementByXpath(driver, commonElements.get("ReplyAllPage")).equalsIgnoreCase("Reply all")){
								  log.info("ReplyAll Page is displayed");
							  }else{
									 throw new Exception("ReplyAll Page should be available" ); 
								 }
							 }else{
								 log.info("Reply all image is not available");
								 throw new Exception("Reply All icon is not available in the compose page");
							 }
						 }
						 else{
							 throw new Exception("Reply All default action is not available" );
						 }
						 }
						 // commonFunction.searchAndClickByXpath(driver, commonElements.get("NotificationCancelOption"));
					 inbox.navigateUntilHamburgerIsPresent(driver);
				  }
				 	 userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					 /*commonFunction.searchAndClickByXpath(driver,commonElements.get("DefaultReplyAction"));
					 commonFunction.searchAndClickByXpath(driver,commonElements.get("ReplyDefaultReplyAction"));
					*/}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyDefaultReplyActionOption "
								+ e.getMessage());
			}
		}
		
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsAutoAdvanceOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				log.info("*********Auto Advance Option ***************");
				if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsAutoAdvanceOption"))!=0){
					  log.info("******************* Gmail Auto Advance Option is displayed ****************");
					  commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsAutoAdvanceOption"));
					  Thread.sleep(1500);
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("GeneralSettingAutoAdvanceNewerOption"))!=0){
							 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Auto Advance Messages Newer Option is displayed @@@@@@@@@@@@@@"); 
							 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingAutoAdvanceNewerOption") );
							 inbox.navigateUntilHamburgerIsPresent(driver);
							 inbox.openMail(driver,verifyGeneralSettingsOptionsData.get("mailSubTwo") );
							//inbox.openMailUsingSearch(driver, verifyGeneralSettingsOptionsData.get("mailSubTwo"));
							commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
							if(commonFunction.getSizeOfElements(driver,commonElements.get("AcceptLabel"))!=0){
							commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
							}String strOne= commonFunction.searchAndGetTextOnElementByXpath(driver,commonElements.get("SubjectLine"));
							log.info("strOne :"+strOne);
						if(strOne.contains(verifyGeneralSettingsOptionsData.get("mailSubOne"))){
							log.info("**********  After Deleting, CV of Newer Mail "+verifyGeneralSettingsOptionsData.get("mailSubOne") +" is displayed **********");
								
						}else{
							throw new Exception("Newer Mail is not available" ); 
								
							}
							 
						 }
					  	//driver.navigate().back();
					  	driver.navigate().back();
					  	userAccount.navigateToGeneralSettings(driver);
					    commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsAutoAdvanceOption"));
					    Thread.sleep(1500);
						if(commonFunction.getSizeOfElements(driver, commonElements.get("GeneralSettingAutoAdvanceOlderOption"))!=0){
							 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Auto Advance Messages Older Option is displayed @@@@@@@@@@@@@@");
							 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingAutoAdvanceOlderOption"));
							 inbox.navigateUntilHamburgerIsPresent(driver);
							 inbox.openMail(driver,verifyGeneralSettingsOptionsData.get("mailSubOne") );
							 commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
							 if(commonFunction.getSizeOfElements(driver,commonElements.get("AcceptLabel"))!=0){
									commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
									}
							 String strTwo= commonFunction.searchAndGetTextOnElementByXpath(driver,commonElements.get("SubjectLine"));
								log.info(strTwo);
							 if(strTwo.contains(verifyGeneralSettingsOptionsData.get("mailSubThree"))){
								log.info("**********  After Deleting, CV of Older Mail "+verifyGeneralSettingsOptionsData.get("mailSubThree") +" is displayed **********");
								
							}else{
								throw new Exception("Older Mail is not available" ); 
							}
						 }
						 commonPage.navigateBack(driver);
						 Thread.sleep(5000);
					 	   userAccount.navigateToGeneralSettings(driver);
						    commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsAutoAdvanceOption"));
						    Thread.sleep(1500);
						if(commonFunction.getSizeOfElements(driver, commonElements.get("GeneralSettingAutoAdvanceConversationlistOption"))!=0){
							 log.info("@@@@@@@@@@@@@@@@@@@@ Gmail Auto Advance Messages Conversationlist Option is displayed @@@@@@@@@@@@@@"); 
							 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingAutoAdvanceConversationlistOption") );
							 inbox.navigateUntilHamburgerIsPresent(driver);
							 inbox.openMail(driver,verifyGeneralSettingsOptionsData.get("mailSubFour") );
							 commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
							 if(commonFunction.getSizeOfElements(driver,commonElements.get("AcceptLabel"))!=0){
									commonFunction.searchAndClickByXpath(driver, commonElements.get("AcceptLabel"));
									}
							 //Thread.sleep(800);
							 Thread.sleep(4800);// added by Venkat on 23/11/2018
							  if(commonFunction.getSizeOfElements(driver, commonElements.get("PrimaryInboxOption"))!=0 || commonFunction.getSizeOfElements(driver, commonElements.get("InboxTitle"))!=0){
								log.info("********** TL is displayed **********");
								
							}else{
								throw new Exception("Not navigated to TL view" ); 
								
							}
						 }	  
				  }
				  userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					 /*commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingsAutoAdvanceOption"));
					 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingAutoAdvanceConversationlistOption") );
			*/}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsAutoAdvanceOption "
								+ e.getMessage());
			}
		}
		
		
		public void verifyGeneralSettingsDeleteArchiveSendOption(AppiumDriver driver) throws Exception{
			try{
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
					log.info("*****Confirm Delete confirmation option function********************");
					scrollDown(1);
					Thread.sleep(1000);
					
					if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConfirmDeleteOption"))!=0){
						  log.info("******************* Gmail Confirm Delete Option is displayed ****************");
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
						  
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
						  
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
						  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
						  
						  commonPage.navigateUntilHamburgerIsPresent(driver);
						  //inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject") );
						  inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
						  if(commonFunction.getSizeOfElements(driver, commonElements.get("DeleteConversationDialog"))!=0){
							  log.info("@@@@@@@@@@@@@@@@@@@@@ Delete Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
							  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
							  log.info("Clicked on Cancel button");
						  }else{
							  throw new Exception("Delete Conversation Dialog is not available" ); 
						  }
						  
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
						  if(commonFunction.getSizeOfElements(driver, commonElements.get("ArchiveConversationDialog"))!=0){
							  log.info("@@@@@@@@@@@@@@@@@@@@@ Archive Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
							  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
							  log.info("Clicked on Cancel button");
						  }  
					  }
					  
					  inbox.navigateUntilHamburgerIsPresent(driver);
					 // commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
					  inbox.composeAsNewMailWithOnlyTo(driver, verifyGeneralSettingsOptionsData.get("SwitchAccountId"), 
							  "Test");
					  
					 /* commonFunction.searchAndClickByXpath(driver, commonElements.get("SendButton"));
					  Thread.sleep(3000);*/
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("SendConversationDialog"))!=0){
						  log.info("@@@@@@@@@@@@@@@@@@@@@ Send Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
						  log.info("Clicked on Cancel button");
					  }  	
			}catch(Exception e){
				throw new Exception(
						"Unable to verify verifyGeneralSettingsDeleteArchiveSendOption "
								+ e.getMessage());
			}
		}
		
		
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsConfirmDeleteOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
					log.info("*****Confirm Delete confirmation option function********************");
					scrollDown(1);
					Thread.sleep(1000);
				if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConfirmDeleteOption"))!=0){
					  log.info("******************* Gmail Confirm Delete Option is displayed ****************");
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
					  
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
					  
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					  
					  commonPage.navigateUntilHamburgerIsPresent(driver);
					  //inbox.openMailUsingSearch(driver,verifyGeneralSettingsOptionsData.get("mailSubject") );
					  inbox.openMail(driver, verifyGeneralSettingsOptionsData.get("mailSubject"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarDeleteIcon"));
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("DeleteConversationDialog"))!=0){
						  log.info("@@@@@@@@@@@@@@@@@@@@@ Delete Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
						  log.info("Clicked on Cancel button");
						  
					  }else{
						  
						  throw new Exception("Delete Conversation Dialog is not available" ); 
						  
					  }  
				  }else{
					  throw new Exception("Confirm Delete Option is not available" ); 
					  
				  }
				  
				  inbox.navigateUntilHamburgerIsPresent(driver);
				 // commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
				  userAccount.navigateToGeneralSettings(driver);
					 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
					 scrollDown(1);
					 Thread.sleep(1500);
					 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingConfirmDeletingCheckBox"));
						
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsConversationViewOption "
								+ e.getMessage());
			}
		}
		
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsConfirmArchivingOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				scrollDown(1);
				Thread.sleep(1000);
				log.info("*********Archive confirmation function*********");
				if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConfirmArchivingOption"))!=0){
					  log.info("******************* Gmail Confirm Archiving Option is displayed ****************");
					  /*inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));
					  inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirArchivingCheckBox"));*/
					 commonPage.navigateUntilHamburgerIsPresent(driver);
					  inbox.openMail(driver,verifyGeneralSettingsOptionsData.get("mailSubject") );
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("MailToolBarArchiveIcon"));
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("ArchiveConversationDialog"))!=0){
						  log.info("@@@@@@@@@@@@@@@@@@@@@ Archive Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
						  log.info("Clicked on Cancel button");
						 // inbox.navigateUntilHamburgerIsPresent(driver);	  
						 // commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
							driver.navigate().back();
						  log.info("Navigating to settings");
						  	 userAccount.navigateToGeneralSettings(driver);
							 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
							 Thread.sleep(1500);
							 scrollDown(1);
							 Thread.sleep(1500);
							 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingConfirArchivingCheckBox"));
						   }else{
						  throw new Exception("Archive Conversation Dialog is not available" );   
					  }
					  
				  }else{
					  throw new Exception("Confirm Archive Option is not available" ); 
					  
				  }
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsConversationViewOption "
								+ e.getMessage());
			}
		}
		/**
		 * Method to verifyUnsubscribeForOneClickHttp
		 * 
		 * @author batchi
		 * @throws Exception
		 */
		public void verifyGeneralSettingsConfirmSendingOption(
				AppiumDriver driver) throws Exception {
			try {
				Map<String, String> verifyGeneralSettingsOptionsData = commonPage
						.getTestData("verifySettingsOption1");
				scrollDown(1);
				Thread.sleep(1000);
				log.info("*********Sending confirmation function*********");
				 if(commonFunction.getSizeOfElements(driver,commonElements.get("GeneralSettingsConfirmSendingOption"))!=0){
					  log.info("******************* Gmail Confirm Sending Option is displayed ****************");
					  /*inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					  commonFunction.searchAndClickByXpath(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					 inbox.verifyIfCheckboxIsChecked(driver, commonElements.get("GeneralSettingConfirmSendingCheckBox"));
					  */commonPage.navigateUntilHamburgerIsPresent(driver);
					  inbox.composeAsNewMailWithOnlyTo(driver, verifyGeneralSettingsOptionsData.get("SwitchAccountId"), 
							  verifyGeneralSettingsOptionsData.get(""));
					  
					  
					  /*inbox.composeAsNewMail(driver, 
							  verifyGeneralSettingsOptionsData.get("SwitchAccountId"), 
							  verifyGeneralSettingsOptionsData.get("CC"),
							  verifyGeneralSettingsOptionsData.get("BCC"),
							  verifyGeneralSettingsOptionsData.get(""),
							  verifyGeneralSettingsOptionsData.get(""));
					  */commonFunction.searchAndClickByXpath(driver, commonElements.get("SendButton"));
					  Thread.sleep(3000);
					  if(commonFunction.getSizeOfElements(driver, commonElements.get("SendConversationDialog"))!=0){
						  log.info("@@@@@@@@@@@@@@@@@@@@@ Send Conversation Dialog appeared @@@@@@@@@@@@@@@@@@@@@@@@@");
						  commonFunction.searchAndClickByXpath(driver, commonElements.get("Cancel"));
						  log.info("Clicked on Cancel button");
						  
					  }else{
						  throw new Exception("Send Conversation Dialog is not available" ); 
						  
					  }
					  
				  }else{
					  throw new Exception("Confirm Sending Option is not available" ); 
					  
				  } 
			  inbox.navigateUntilHamburgerIsPresent(driver);
			//  commonFunction.searchAndClickByXpath(driver, commonElements.get("HamburgerMenu"));
			  userAccount.navigateToGeneralSettings(driver);
				 commonPage.waitForElementToBePresentByXpath(commonElements.get("GeneralSettings"));
				 Thread.sleep(1500);
				 scrollDown(1);
				 Thread.sleep(1500);
				 commonFunction.searchAndClickByXpath(driver,commonElements.get("GeneralSettingConfirmSendingCheckBox"));
			
		
			}catch (Exception e) {
				throw new Exception(
						"Unable to verify verifyGeneralSettingsConfirmSendingOption "
								+ e.getMessage());
			}
		}
		
		/**
		* Method to syncNowFromSettingsAppToGetNotifications
		*@author batchi
		* @throws Exception
		*/
				public void syncNowFromSettingsAppToGetNotifications(AppiumDriver driver,String searchFeature, String accountType, String accountId) throws Exception {
					try {
						log.info("In Sync Now  method");
						Runtime.getRuntime().exec("adb shell am start -a android.settings.SETTINGS");
						commonFunction.searchAndClickByXpath(driver, commonElements.get("SearchInSettings"));
			 			commonFunction.searchAndSendKeysByXpath(driver, commonElements.get("SearchTextInSettings"), searchFeature);
			 			commonFunction.searchAndClickByXpath(driver, commonElements.get("AccountsInSettings"));
			 			commonFunction.searchAndClickByXpath(driver, commonElements.get("OptionsInAccounts").replace("optionType", accountType));
				 		commonFunction.searchAndClickByXpath(driver, commonElements.get("MailIdsInAccounts").replace("emailId", accountId));
				 		log.info("clicked on Account ");
				 		Thread.sleep(1000);
				 		commonFunction.searchAndClickByXpath(driver, commonElements.get("InAppBrowserMoreOptions"));
				 		commonFunction.searchAndClickByXpath(driver, commonElements.get("SyncNow"));
				 		Runtime.getRuntime().exec("adb shell input keyevent 3");
				 		
				 		
					} catch (Exception e) {
						throw new Exception(
								"changed settings to verify after backup and restore "
										+ e.getMessage());
					}
				}
				/**
				* Method to verify Archive button
				* @author batchi
				 * @throws Exception 
				*/
				public static void verifyArchive(AppiumDriver driver) throws Exception{
					try{
					log.info("In verify Archive In TL View");
					if(commonFunction.getSizeOfElements(driver, commonElements.get("MailToolBarArchiveIcon"))!=0){
						commonFunction.searchAndClickByXpath(driver,commonElements.get("MailToolBarArchiveIcon"));
						Thread.sleep(1500);
						if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
						}
						Thread.sleep(1200);
						if(commonFunction.getSizeOfElements(driver,commonElements.get("SwipeUndo"))!=0){
							log.info("********** Successfully mail has been marked as Archive *************");
						}else{
							throw new Exception("Unable to find Undo option");
						}
						
					}else{
						throw new Exception("Unable to find Archive button");
					}
					
					}catch (Exception e) {
						e.printStackTrace();
						throw new Exception("Unable to verify Archive button functionality");
					} 
				}
		/**
		* Method to verify Delete button
		* @author batchi
		* @throws Exception 
		*/
				public static void verifyDelete(AppiumDriver driver) throws Exception{
					try{
					log.info("In verify Delete Button");
					if(commonFunction.getSizeOfElements(driver, commonElements.get("MailToolBarDeleteIcon"))!=0){
						commonFunction.searchAndClickByXpath(driver,commonElements.get("MailToolBarDeleteIcon"));
						Thread.sleep(1500);
						if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
							CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
						}
						Thread.sleep(1200);
						if(commonFunction.getSizeOfElements(driver,commonElements.get("SwipeUndo"))!=0){
							log.info("********** Successfully mail has been Deleted *************");
						}else{
							throw new Exception("Unable to find Undo option");
						}
					}else{
						throw new Exception("Unable to find Delete button");
					}
					
					}catch (Exception e) {
						e.printStackTrace();
						throw new Exception("Unable to verify Delete button  functionality ");
					} 
				}
				
				/**
				* Method to verify MoveTo button
				* @author batchi
				* @throws Exception 
				*/
				public static void verifyMoveTo(AppiumDriver driver) throws Exception{
							try{
							log.info("In verify MoveTo Button");
							if(commonFunction.getSizeOfElements(driver, commonElements.get("MailMoreOptionsNavigation"))!=0){
								Thread.sleep(2000);
								commonFunction.searchAndClickByXpath(driver,commonElements.get("MailMoreOptionsNavigation"));
								Thread.sleep(800);
								commonFunction.searchAndClickByXpath(driver, commonElements.get("MoveToOptionInOverflowMenu"));
								if(commonFunction.getSizeOfElements(driver,commonElements.get("MoveToOptionInOverflowMenu"))!=0){
									log.info("MoveTo option toast has appeared");
									commonFunction.searchAndClickByXpath(driver, commonElements.get("SocialInboxOption"));
									Thread.sleep(2000);
									if(commonFunction.getSizeOfElements(driver,commonElements.get("SwipeUndo"))!=0){
										log.info("********** Successfully mail has been moved *************");
									}else{
										throw new Exception("Unable to find Undo option");
									}
								}else{
									throw new Exception("Unable to find MoveTo option toast ");
								}
							}else{
								throw new Exception("Unable to find MoveTo option in overflow menu ");
							}
							
							}catch (Exception e) {
								e.printStackTrace();
								throw new Exception("Unable to verify MoveTo Button functionality");
							} 
						}		
					
				/**
				 * Method to verify removed accounts
				 * 
				 * @author batchi
				 * @throws Exception
				 */
				public static void verifyDeleteShortcutBeforeDisabled(AppiumDriver driver,
						String accountId) throws Exception {
					try {
						log.info("In verify delete  launcher shortcut before disabled");
						Thread.sleep(3000);
						commonFunction.searchAndClickByXpath(driver,commonElements.get("MailIdsInAccounts").replace("emailId",accountId));

						/*
						 * Process p; String[] homeButton = new String[]{"adb", "shell",
						 * "input", "keyevent", "3"}; p = new
						 * ProcessBuilder(homeButton).start();
						 * inbox.verifyGmailAppInitState(driver); Thread.sleep(2000);
						 */
						String parentPanelText = driver
								.findElement(
										By.xpath(commonElements
												.get("MessageOnAccountRemoveToast")))
								.getText();
						log.info(parentPanelText);
						if (parentPanelText.contains(accountId)) {
							log.info("&&&&&&&&&&&& Account " + accountId
									+ " removed toast is displayed  &&&&&&&&&&&& ");
						} else {
							throw new Exception("Unable to find remove  account toast");
						}
						commonFunction.searchAndClickByXpath(driver,
								commonElements.get("Cancel"));
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("Unable to verify accounts removed ");
					}
					Process p;
					String[] homeButton = new String[] { "adb", "shell", "input",
							"keyevent", "3" };
					p = new ProcessBuilder(homeButton).start();
				}

				/**
				 * Method to verify removed accounts
				 * 
				 * @author batchi
				 * @throws Exception
				 */
				public static void verifyDeleteShortcutAfterDisabled(AppiumDriver driver,
						String accountId) throws Exception {
					try {
						log.info("In verify delete  launcher shortcut  after disabled");
						Process p;
						String[] homeButton = new String[] { "adb", "shell", "input",
								"keyevent", "3" };
						p = new ProcessBuilder(homeButton).start();
						SoftAssert softAssert = new SoftAssert();
						Thread.sleep(2000);
						softAssert
								.assertTrue(
										commonPage
												.getScreenshotAndCompareImage("verifyDeleteShortcutAfterDisabled"),
										"Image comparison for verifyDeleteShortcutAfterDisabled");
						commonFunction.searchAndClickByXpath(
								driver,
								commonElements.get("MailIdsInAccounts").replace("emailId",
										accountId));
						log.info("Clicked on the " + accountId + " account");
						boolean value = CommonFunctions.isElementByXpathDisplayed(driver,
								commonElements.get("HamburgerMenu"));
						if (value == false) {
							log.info("@@@@@@@@@@@@ Gmail App is not launched after pressing on the disabled shortcut @@@@@@@@@@");
						} else {
							throw new Exception("Gmail App is launched");
						}
					} catch (Exception e) {
						e.printStackTrace();
						throw new Exception("Unable to verify accounts removed ");
					}
				}
				/**
				 * Method to verify reenabled shortcut icon
				 * 
				 * @author batchi
				 * @throws Exception
				 */
				public void verifyReenabledShortcutIcon(AppiumDriver driver, String account)
						throws Exception {
					try {
						Process p;
						log.info("verifying the disabled shortcut icon");
						inbox.launchGmailApp(driver);
						String[] homeButton = new String[] { "adb", "shell", "input",
								"keyevent", "3" };
						p = new ProcessBuilder(homeButton).start();

						SoftAssert softAssert = new SoftAssert();
						Thread.sleep(2000);
						softAssert
								.assertTrue(
										commonPage
												.getScreenshotAndCompareImage("verifyReenabledShortcutIcon"),
										"Image comparison for verifyReenabledShortcutIcon");
						commonFunction.searchAndClickByXpath(
								driver,
								commonElements.get("MailIdsInAccounts").replace("emailId",
										account));
						log.info("Clicked on the " + account + " account");
						boolean value = CommonFunctions.isElementByXpathDisplayed(driver,
								commonElements.get("HamburgerMenu"));
						if (value == true) {
							log.info("@@@@@@@@@@@@ Gmail App is launched after pressing on the re-enabled shortcut @@@@@@@@@@");
						} else {
							throw new Exception("Gmail App is not launched");
						}

					} catch (Exception e) {
						throw new Exception(
								"changed settings to verify after backup and restore "
										+ e.getMessage());
					}
				}
				
				/**
				* Method to perform orientation(Rotation) of screen
				* @author batchi
				 * @throws Exception 
				*/	
				public static void orientationOfScreen(AppiumDriver driver2) throws Exception{
					for(int i=0;i<=3;i++){
				 		driver2.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
						  Thread.sleep(3000);
						  driver2.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
							  log.info("Orientation is done" +i+ "times");
							  Thread.sleep(3000);
						  			  }
					}	
/**
* Method to launch playstore app and search for any app
* @author batchi
 * @throws Exception 
*/		
public static void launchPlaystoreAppAndSearch(AppiumDriver driver, String searchKeyword) throws Exception{
	 Map < String, String > appsInDrawerData = commonPage.getTestData("AppsInDrawer");
	 Process p;
	 String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.vending/com.google.android.finsky.activities.MainActivity"};
	 p = new ProcessBuilder(p1).start();
	
	 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PlayStoreSearchField"));
	 CommonFunctions.searchAndSendKeysByXpath(driver, commonElements.get("PlayStoreSearchIcon"), searchKeyword);
	 ((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
	 CommonFunctions.searchAndClickByXpath(driver, appsInDrawerData.get("AppSelector").replace("AppName", searchKeyword));
	 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("PlayStoreUnInstallButton"))!=0){
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("PlayStoreUnInstallButton"));
		 CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AcceptOk"));
		 waitForElementToBePresentByXpath(commonElements.get("PlayStoreInstallButton"));
	 }else{
		
	 }


}
/**
* Method to launch settings app and disable the app
* @author batchi
 * @throws Exception 
*/		
public static void launchSettingsAppAndDisableApp(AppiumDriver driver, String appName) throws Exception{
	 Map < String, String > appsInDrawerData = commonPage.getTestData("AppsInDrawer");
	 Process p;
	 String[] p1 = new String[]{"adb", "shell", "am", "start", "-n", "com.android.settings/com.android.settings.applications.ManageApplications"};
	 p = new ProcessBuilder(p1).start();
	 waitForElementToBePresentByXpath(appsInDrawerData.get("AllAppsInSettings"));
	 log.info("Disabling the  "+appName + " app ");
	 CommonFunctions.scrollsToTitle(driver, appName);
	 Thread.sleep(2000);
	 if(CommonFunctions.getSizeOfElements(driver,appsInDrawerData.get("DisableButton") )!=0){
	 String disabledstatus = CommonFunctions.searchAndGetAttributeOnElementByXpath(driver,appsInDrawerData.get("DisableButton") , "enabled");
	 if(disabledstatus.equalsIgnoreCase("True")){
		 CommonFunctions.searchAndClickByXpath(driver, appsInDrawerData.get("DisableButton"));
		 CommonFunctions.searchAndClickByXpath(driver, appsInDrawerData.get("DisableAppButton"));
		 if(CommonFunctions.getSizeOfElements(driver, commonElements.get("AcceptOk"))!=0){
			 CommonFunctions.searchAndClickByXpath(driver,commonElements.get("AcceptOk") );
		 }
		 waitForElementToBePresentByXpath(appsInDrawerData.get("EnableButton"));
		 	
	 }else{
		 launchPlaystoreAppAndSearch(driver, appName);
	 }
	 }
	 driver.navigate().back();
}
}