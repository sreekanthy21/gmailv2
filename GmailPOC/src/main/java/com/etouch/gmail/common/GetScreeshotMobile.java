package com.etouch.gmail.common;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import com.etouch.taf.webui.selenium.MobileView;

import io.appium.java_client.AppiumDriver;

public class GetScreeshotMobile {
	static File scrFile,targetFile;
	static DateFormat dateFormat,dateFormat2;
	static String DateForFolderName,DateForFileName;
	static String FileLocationScreenShot;
	
	 public void getScreenShot(String Method,AppiumDriver driver) throws IOException
		{
		 //FileLocationScreenShot = System.getProperty("user.dir");
		scrFile = driver.getScreenshotAs(OutputType.FILE);
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh");
		dateFormat2 = new SimpleDateFormat("dd-MMM-yyyy__hh_mm");
		DateForFolderName= dateFormat.format(new Date());
		DateForFileName= dateFormat2.format(new Date());
		targetFile=new File(FileLocationScreenShot+"src/test/resources/testdata/Screenshot/"+DateForFolderName+"/" + Method +DateForFileName+".jpg");
		System.out.println("Location of screenshot is"+FileLocationScreenShot+"src/test/resources/testdata/Screenshot/"+DateForFolderName+"/" + Method +DateForFileName+".jpg");
		FileUtils.copyFile(scrFile,targetFile);
		
		}
	 public static void initializePath(String RequiredPath)
	 {
		 FileLocationScreenShot=RequiredPath;
	 }

}
