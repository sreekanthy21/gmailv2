package com.etouch.gmail.common;

import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;

import com.etouch.taf.util.ExcelUtil;

public class MailDeletion {

	public static void DeleteAllMails() throws InterruptedException {

		String DataFilePath = GetDataInHashMap.getPath()
				.concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
		String testBedName = GetDataInHashMap.testBedMap.get(Thread.currentThread().getId());
		DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
		String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, "GmailData", "GmailAccounts");
		Properties props = new Properties();

		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.port", "465");

		for (int i = 0; i < testDataArray.length; i++) {

			try {
				System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
				Session session = Session.getDefaultInstance(props, null);
				Store store = session.getStore("imaps");
				store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);
				Folder AllMail = store.getFolder("[Gmail]/All Mail");
				Folder Inbox = store.getFolder("inbox");
				AllMail.open(Folder.READ_WRITE);
				Message[] AllMailmessages = AllMail.getMessages();
				AllMail.copyMessages(AllMailmessages, Inbox);
				AllMail.close(true);
				Inbox.open(Folder.READ_WRITE);
				Message[] Inboxmessages = Inbox.getMessages();
				// int messageCount = AllMail.getMessageCount();
				for (Message msg : Inboxmessages) {
					System.out.println("Mail Subject:- " + msg.getSubject());
					if (msg.getSubject().contains("Invitation")
							|| msg.getSubject().contains("Gmail Attachments Types")
							|| msg.getSubject().contains("verifyMailConversation")) {
						continue;
					} else {
						msg.setFlag(Flags.Flag.DELETED, true);
					}
				}
				Inbox.close(true);
				store.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/*for (int i = 0; i < testDataArray.length; i++) {

			try {
				System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
				Session session = Session.getDefaultInstance(props, null);
				Store store = session.getStore("imaps");
				store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);
				Folder AllMail = store.getFolder("[Gmail]/All Mail");
				Folder Inbox = store.getFolder("inbox");
				AllMail.open(Folder.READ_WRITE);
				Message[] AllMailmessages = AllMail.getMessages();
				AllMail.copyMessages(AllMailmessages, Inbox);
				AllMail.close(true);
				Inbox.open(Folder.READ_WRITE);
				Message[] Inboxmessages = Inbox.getMessages();
				// int messageCount = AllMail.getMessageCount();
				for (Message msg : Inboxmessages) {
					System.out.println("Mail Subject:- " + msg.getSubject());
					if (msg.getSubject().contains("Invitation")
							|| msg.getSubject().contains("Gmail Attachments Types")) {
						continue;
					} else {
						msg.setFlag(Flags.Flag.DELETED, true);
					}
				}
				Inbox.close(true);
				store.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/

	}

}
