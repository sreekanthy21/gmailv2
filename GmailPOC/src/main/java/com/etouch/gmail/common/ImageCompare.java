package com.etouch.gmail.common;


import javax.swing.*;

import java.io.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

public class ImageCompare {

	public BufferedImage img1 = null;
	public BufferedImage img2 = null;
	protected BufferedImage imgc = null;
	Graphics2D gc = null;
	protected int comparex = 0;
	protected int comparey = 0;
	protected int factorA = 2;
	protected int factorD = 20;
	protected boolean match = false;
	protected int debugMode = 2; // 1: textual indication of change, 2: difference of factors
	protected List<String> passData = new ArrayList<String>();
	protected List<String> failData = new ArrayList<String>();
	public boolean flag = true;
	protected boolean draw = true;
	protected LinkedList failLinkedList = new LinkedList();
	protected LinkedList compareLinkedList = new LinkedList();
	protected LinkedList drawLinkedList = new LinkedList();

	// constructor 1. use filenames
	public ImageCompare(String file1, String file2) {
		this(loadJPG(file1), loadJPG(file2));
	}

	// constructor 2. use awt images.
	public ImageCompare(Image img1, Image img2) {
		this(imageToBufferedImage(img1), imageToBufferedImage(img2));
	}

	// constructor 3. use buffered images. all roads lead to the same place. this place.
	public ImageCompare(BufferedImage img1, BufferedImage img2) {
		this.img1 = img1;
		this.img2 = img2;
	}

	public void setImages(BufferedImage img1, BufferedImage img2) {
		this.img1 = img1;
		this.img2 = img2;
	}

	//Image clean Up cleans image for Notification and softKeys
	/*	public BufferedImage ImageCleanUp(BufferedImage img, boolean top, boolean bottom ){
		BufferedImage outputImage = null;
		BufferedImage croppedImage = null;
		int imageTop = (int) (img1.getHeight()/24);
		int imageBottom = (int) (img1.getHeight()/16.56);

		if(bottom){
			outputImage = new BufferedImage(img1.getWidth(),img1.getHeight()-imageBottom,img1.getType());
		}else {
			outputImage = new BufferedImage(img1.getWidth(),img1.getHeight(), img1.getType());
		}

		Graphics2D g2d = outputImage.createGraphics();
		if(top){
			croppedImage = img1.getSubimage(0, imageTop, img1.getWidth(), outputImage.getHeight()-imageTop);
		}else {
			croppedImage = img1.getSubimage(0, 0, img1.getWidth(), outputImage.getHeight());
		}
		g2d.drawImage(croppedImage, 0, 0,img1.getWidth(), img1.getHeight(), null);
		g2d.dispose();
		return outputImage;
	}*/

	public void ImageResize(BufferedImage img1, BufferedImage img2){
		String outputImagePath1 = "d:\\testA.png";
		String outputImagePath2 = "d:\\testB.png";
		int imageWidth = 0;
		int imageHeight = 0;
		//	img1 = ImageCleanUp(img1, top, bottom);
		if(img1.getHeight()>img2.getHeight()){
			imageHeight = img2.getHeight();
			imageWidth = img2.getWidth();
		}else{
			imageHeight = img1.getHeight();
			imageWidth = img1.getWidth();
		}

		BufferedImage outputImage1 = new BufferedImage(imageWidth,imageHeight, img1.getType());
		Graphics2D g2d1 = outputImage1.createGraphics();
		g2d1.drawImage(img1, 0, 0, imageWidth, imageHeight, null);
		g2d1.dispose();
		BufferedImage outputImage2 = new BufferedImage(imageWidth,imageHeight, img2.getType());
		Graphics2D g2d2 = outputImage2.createGraphics();
		g2d2.drawImage(img2, 0, 0, imageWidth, imageHeight, null);
		g2d2.dispose();
		String formatName1 = outputImagePath1.substring(outputImagePath1.lastIndexOf(".") + 1);
		String formatName2 = outputImagePath2.substring(outputImagePath2.lastIndexOf(".") + 1);
		try {
			ImageIO.write(outputImage1, formatName1, new File(outputImagePath1));
			ImageIO.write(outputImage2, formatName2, new File(outputImagePath2));
			setImages(outputImage1,outputImage2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.img1 = img1;
		this.img2 = img2;
	}

	// like this to perhaps be upgraded to something more heuristic in the future.
	protected void autoSetParameters() {
		comparex = 10;
		comparey = 10;
		factorA = 2;
		factorD = 30;
	}

	// set the parameters for use during change detection.
	public void setParameters(int x, int y, int factorA, int factorD) {
		this.comparex = x;
		this.comparey = y;
		this.factorA = factorA;
		this.factorD = factorD;
	}

	// want to see some stuff in the console as the comparison is happening?
	public void setDebugMode(int m) {
		this.debugMode = m;
	}

	// compare the two images in this object.
	public void compare() {
		// setup change display image
		gc.setColor(Color.RED);
		// convert to gray images.
		img1 = imageToBufferedImage(GrayFilter.createDisabledImage(img1));
		img2 = imageToBufferedImage(GrayFilter.createDisabledImage(img2));
		// how big are each section
		int blocksx = (int)(img1.getWidth() / 2);
		int blocksy = (int)(img1.getHeight() / 2);

		if(blocksx < 50){
			blocksx = 50;
		} 
		if(blocksy < 50){
			blocksy = 50;
		}
		// set to a match by default, if a change is found then flag non-match
		this.match = true;
		// loop through whole image and compare individual blocks of images
		for (int y = 1; y < comparey; y++) {
			if (debugMode > 0) System.out.print("|");
			for (int x = 1; x < comparex; x++) {
				int b1 = getAverageBrightness(img1.getSubimage(x*blocksx, y*blocksy, blocksx - 1, blocksy - 1));
				int b2 = getAverageBrightness(img2.getSubimage(x*blocksx, y*blocksy, blocksx - 1, blocksy - 1));
				int diff = Math.abs(b1 - b2);
				if (diff > factorA) { // the difference in a certain region has passed the threshold value of factorA
					// draw an indicator on the change image to show where change was detected.
					gc.setColor(Color.RED);
					gc.drawRect(x*blocksx, y*blocksy, blocksx - 1, blocksy - 1);

					this.match = false;
				}else{
					gc.setColor(Color.WHITE);
					gc.fillRect(x*blocksx, y*blocksy, blocksx , blocksy);
					gc.setColor(Color.WHITE);
				}
				if (debugMode == 1) System.out.print((diff > factorA ? "X" : " "));
				if (debugMode == 2) System.out.print(diff + (x < comparex - 1 ? "," : ""));
			}
			if (debugMode > 0) System.out.println("|");
		}
		for(int y = 0; y < 1; y++ ){
			for(int x = 0; x < 1; x++ ){
				gc.setColor(Color.WHITE);
				gc.fillRect(x*blocksx, y*blocksy, blocksx , blocksy);
			}
		}
	}

	// compare the Block of 1 image with entire 2nd image.
	public BufferedImage compareBlock(int xStart, int yStart, int xEnd, int yEnd, BufferedImage imgc,
			int pixelBuffer, int passCriteria) {
		// setup change display image
		gc = imgc.createGraphics();
		//initialize internal params
		int avgX = 0;
		int avgY = 0;
		// how big is component
		int componentWidth = xEnd - xStart;
		int componentHeight = yEnd - yStart;

		// convert to gray images.
		img1 = imageToBufferedImage(GrayFilter.createDisabledImage(img1));
		img2 = imageToBufferedImage(GrayFilter.createDisabledImage(img2));

		int b1 = 0;
		int b2 = getAverageBrightness(img2.getSubimage(xStart, yStart, componentWidth, componentHeight));
		System.out.println("b2 "+b2);
		// set to no-match by default, if a match is found then flag match
		this.match = false;
		// loop through whole image and compare individual blocks of images
		for (int y = yStart - pixelBuffer ; y < yStart + pixelBuffer; y++) {
			//	if (debugMode > 0) System.out.print("|");
			for (int x = xStart - pixelBuffer; x < xStart + pixelBuffer; x++) {
				b1 = getAverageBrightness(img1.getSubimage(x, y, 
						componentWidth , componentHeight ));
				double diff = Math.abs(b1 - b2);
				diff = ((diff/(b1 + b2))*100);
				try{
					if(diff<(100-passCriteria)){
						this.match = true;
						passData.add(x+","+y);
					}else {
						failData.add(x+","+y);
					}
				}catch(Exception e){
					gc.drawImage(img2.getSubimage(xStart, yStart, componentWidth, componentHeight), 
							300, 500, null);
					gc.setColor(Color.RED);
					System.out.println("in Exception");
				} 
			}
		}

		if(this.match){
			for(String data : passData){
				avgX = avgX + Integer.parseInt(data.split(",")[0]);
				avgY = avgY + Integer.parseInt(data.split(",")[1]);
			}
			avgX = avgX/passData.size();
			avgY = avgY/passData.size();
		}else{
			for(String data : failData){
				avgX = avgX + Integer.parseInt(data.split(",")[0]);
				avgY = avgY + Integer.parseInt(data.split(",")[1]);
			}
			avgX = avgX/failData.size();
			avgY = avgY/failData.size();

		}
		if(this.match){
			gc.setColor(Color.GREEN);
			gc.drawRect(avgX, avgY, componentWidth - 1, componentHeight - 1);
			System.out.println(avgX + "  -  " + avgY);
			System.out.println(passData.size());
			System.out.println("Image Comparison Succesful");
		}else{
			gc.setColor(Color.RED);
			gc.drawRect(avgX, avgY, componentWidth - 1, componentHeight - 1);
			System.out.println(avgX + "  -  " + avgY);
			System.out.println(failData.size());
			System.out.println("Image Comparison fail");
		}
		return imgc;
	}

	// compare the Block of 1 image with entire 2nd image.
	public BufferedImage compare(int xStart, int yStart, int xEnd, int yEnd, BufferedImage imgc,
			int pixelBuffer, int passCriteria) {
		// setup change display image
		if(pixelBuffer!=0){
			xStart = ((xStart - pixelBuffer)<0) ? (xStart - pixelBuffer) :xStart;
			yStart = ((yStart - pixelBuffer)<0) ? (yStart - pixelBuffer) :yStart;
			xEnd = ((xEnd + pixelBuffer)>img1.getWidth()) ? (xEnd + pixelBuffer) : xEnd;
			yEnd = ((yEnd + pixelBuffer)>img1.getHeight()) ? (yEnd + pixelBuffer) : yEnd;
		}
		// how big is component
		int componentWidth = xEnd - xStart;
		int componentHeight = yEnd - yStart;
		int quadrant = 1;
		//Create sub block of images
		int xRange = ((componentWidth / 2)<50) ? componentWidth : (componentWidth / 2);
		int yRange = ((componentHeight / 2)<50) ? componentHeight : (componentHeight / 2);

		// convert to gray images.
		/*img1 = imageToBufferedImage(GrayFilter.createDisabledImage(img1));
		img2 = imageToBufferedImage(GrayFilter.createDisabledImage(img2));*/
		
		img1 = imageToBufferedImage(img1);
		img2 = imageToBufferedImage(img2);
		//Initialize internal variables

		int b1 = 0;
		int b2 = 0;
		int b4 = 0;
		int b3 = 0;
		
		// set to no-match by default, if a match is found then flag match
		this.match = false;
		// loop through whole image and compare individual blocks of images
		for (int y = 0  ; y < componentHeight/yRange ; y++) {
			//	if (debugMode > 0) System.out.print("|");
			for (int x = 0 ; x < componentWidth/xRange; x++) {
				b2 = getAverageChange(img2.getSubimage(xStart + xRange*x, yStart +yRange*y, 
						xRange , yRange));
				b1 = getAverageChange(img1.getSubimage(xStart + xRange*x, yStart +yRange*y, 
						xRange , yRange ));
				
				b4 = getAverageBrightness(img2.getSubimage(xStart + xRange*x, yStart +yRange*y, 
						xRange , yRange));
				b3 = getAverageBrightness(img1.getSubimage(xStart + xRange*x, yStart +yRange*y, 
						xRange , yRange));
				double diffAC = Math.abs(b1 - b2);
				double diffAB = Math.abs(b3 - b4);
				if(b1==0.0&&b2==0.0){
					diffAC = 0;
				}else{
					diffAC = (((double)diffAC/(double)(b1 + b2))*100);
				}
				
				/*if(b3==0.0&&b4==0.0){
					diffAB = 0;
				}else{
					diffAB = (((double)diffAB/(double)(b3 + b4))*100);
				}*/
				try{
					if((diffAC<(100-passCriteria))&&(diffAB<factorA)){
						if(!(b1==0.0&&b2==0.0)){
							this.match = true;
							draw = false;
						}
					}else {
						this.match = false;
						flag = false;
						draw = true;
						failData.add(xStart + ";" + yStart + ";" + xEnd + ";" + yEnd);
						int newXStart = 0;
						int newYStart = 0;
						int newXEnd = 0;
						int newYEnd = 0;
						int newComponentWidth = 0;
						int newComponentHeight = 0;
						if(x==0&&y==0){ 
							newXStart = xStart;
							newYStart = yStart;
							newXEnd = xStart + xRange;
							newYEnd = yStart + yRange;
						} else if(x==0&&y==1){ 
							newXStart = xStart;
							newYStart = yStart + yRange;
							newXEnd = xStart + xRange;
							newYEnd = yStart + componentHeight;
						}else if(x==1&&y==0){ 
							newXStart = xStart + xRange;
							newYStart = yStart;
							newXEnd = xStart + componentWidth;
							newYEnd = yStart + yRange;
						}else { 
							newXStart = xStart + xRange;
							newYStart = yStart + yRange;
							newXEnd = xStart + componentWidth;
							newYEnd = yStart + componentHeight;
						}
						newComponentWidth = newXEnd - newXStart;
						newComponentHeight = newYEnd - newYStart;
						if(newComponentWidth/2>50||newComponentHeight/2>50){
							ImageCompare ic = new ImageCompare(img1,img2);
							imgc = ic.compare(newXStart, newYStart, newXEnd, newYEnd, 
									imgc, pixelBuffer, passCriteria);
							draw = false;
							failData.remove(failData.size()-1);
						}
					}
				}catch(Exception e){
					System.out.println("in Exception");
					System.out.println("CompH " + componentHeight + " compW " + componentWidth);
					System.out.println("xRange - " + xRange + " yRange - " + yRange);
					System.out.println(" x - " + xStart + " y - " + yStart + " xe - " + xEnd + 
							" ye - " + yEnd);
					System.out.println(e);
				} 
			}
		}
		gc = imgc.createGraphics();
		if(this.match&&flag){
			gc.setColor(Color.GREEN);
			gc.drawRect(xStart, yStart, componentWidth - 1, componentHeight - 1);
			//	System.out.println("Image Comparison Succesful");
		}else {
			for(String data : failData){
				String[] dataArray = data.split(";");
				gc.setColor(Color.RED);
				gc.drawRect(Integer.parseInt(dataArray[0]), Integer.parseInt(dataArray[1]), 
						(Integer.parseInt(dataArray[2]) - Integer.parseInt(dataArray[0])) + 1, 
						(Integer.parseInt(dataArray[3]) - Integer.parseInt(dataArray[1])) + 1);
			}
			/*gc.setColor(Color.RED);
			gc.drawRect(xStart, yStart, componentWidth + 1, componentHeight + 1);*/
			//	System.out.println("Image Comparison fail");
		}
		return imgc;
	}


	public BufferedImage compareImage(int xStart, int yStart, int xEnd, int yEnd, BufferedImage imgc,
			int pixelBuffer, int blocks, int minBlockSize, int passCriteria) {


		return imgc;
	}

	//set Block Size for testing 
	protected int setBlockSize(int xStart, int yStart, int xEnd, int yEnd, int pixelBuffer, 
			int blocks, int minBlockSize) {

		int width = getHW(xStart, xEnd);
		int height = getHW(yStart, yEnd);


		return 1;
	}

	//Get Height and Width of element 
	protected int getHW(int start, int end) {
		return (end-start);
	}

	//Get Height and Width of element 
	protected int createCompareLinkedList(int width, int height, int blocks, int minBlockSize) {
		return 1;
	}


	// return the image that indicates the regions where changes where detected.
	/*	public BufferedImage getChangeIndicator() {
		return imgc;
	}*/

	// returns a value specifying some kind of average brightness in the image.
	protected int getAverageBrightness(BufferedImage img) {
		Raster r = img.getData();
		int total = 0;
		for (int y = 0; y < r.getHeight(); y++) {
			for (int x = 0; x < r.getWidth(); x++) {
				total += r.getSample(r.getMinX() + x, r.getMinY() + y, 0);
			}
		}
		return (int)(total / ((r.getWidth()/factorD)*(r.getHeight()/factorD)));
	}

	// returns a total of avg change in pixels.
	protected int getAverageChange(BufferedImage img) {
		int xTotal = 0;
		int yTotal = 0;
		for (int y = 0; y < img.getHeight() - 1; y++) {
			for (int x = 0; x < img.getWidth() - 1 ; x++) {
				if(img.getRGB(x, y)!=img.getRGB(x + 1, y)){
					xTotal +=1;
				}
				if(img.getRGB(x, y)!=img.getRGB(x, y+1)){
					yTotal +=1;
				}
			}
		}
		return (xTotal+yTotal);
	}

	// returns true if image pair is considered a match
	public boolean match() {
		return this.match;
	}

	// buffered images are just better.
	public static BufferedImage imageToBufferedImage(Image img) {
		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), 
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bi.createGraphics();
		g2.drawImage(img, null, null);
		return bi;
	}

	// write a buffered image to a jpeg file.
	public static void saveJPG(Image img, String filename) {
		BufferedImage bi = imageToBufferedImage(img);
		FileOutputStream out = null;
		try { 
			out = new FileOutputStream(filename);
		} catch (java.io.FileNotFoundException io) { 
			System.out.println("File Not Found"); 
		}
		try {
			ImageIO.write(bi, "png", out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try { 
			/*encoder.encode(bi); */
			out.close(); 
		} catch (java.io.IOException io) {
			System.out.println("IOException"); 
		}
	}

	// read a jpeg file into a buffered image
	public static Image loadJPG(String filename) {
		FileInputStream in = null;
		BufferedImage bo = null;
		BufferedImage bi = null;
		try { 
			in = new FileInputStream(filename);
		} catch (java.io.FileNotFoundException io) { 
			System.out.println("File Not Found"); 
		}
		try {
			bo = ImageIO.read(in);
			bi = new BufferedImage(bo.getWidth(), bo.getHeight(),BufferedImage.TYPE_INT_ARGB);
			bi.createGraphics().drawImage( bo, 0, 0, Color.BLACK, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//JPEGImageDecoder decoder = JPEGCodec.createJPEGDecoder(in);
		try { 
			//	bi = decoder.decodeAsBufferedImage(); 
			in.close(); 
		} catch (java.io.IOException io) {
			System.out.println("IOException");
		}
		return bi;
	}

}
