package com.etouch.gmail.common;


import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ModifyXMLFile {
	protected int testngResutlsPassed = 0;
	protected int testngResutlsTotal = 0;
	protected int testngResutlsFailed = 0;
	protected int testngResutlsSkipped = 0;
	protected List<Node> numberOfNodes = new ArrayList<Node>();
	protected Set<String> testNames = new HashSet<String>();
	protected Set<String> methodNames = new HashSet<String>();
	protected String filePath = "";
	protected String outPutFilePath = "";
	protected String parentNode = "";
	protected int length = 0;

		public void prepareTestNGReport(String file1, String file2){
		 filePath = file1;
		 outPutFilePath=file2;
		 try {
			 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			 DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			 Document doc = docBuilder.parse(filePath);
			 // Get the root element 
			 NodeList listOfTestNodes = doc.getElementsByTagName("test");

			 //Create Individual TestNG Reports
			 for(int i = 0; i < listOfTestNodes.getLength(); i++){
				 String name = listOfTestNodes.item(i).getAttributes().getNamedItem("name").toString()
						 .replace(" Test", "").replace("name=", "").replaceAll("\"", "");
				 String location = outPutFilePath + name + ".xml";
				 createTestNGReport(doc,location);
				 testNames.add(name);
			 }
			 for(String name : testNames){
				 cleanTests(name);
			 }

			 //Clean TestNG Repors


		 } catch (ParserConfigurationException pce) {
			 pce.printStackTrace();
		 } catch (IOException ioe) {
			 ioe.printStackTrace();
		 } catch (SAXException sae) {
			 sae.printStackTrace();
		 }
	 }

	 private void cleanTests(String name){
		 NodeList listOfTestNodes = null;
		 Document document = null;
		 boolean flag = true;
		 int beforeFlag = 0;
		 try{
			// System.out.println(name);
			 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			 DocumentBuilder docBuilderTarget = docFactory.newDocumentBuilder();
			 String location = outPutFilePath + name + ".xml";
			 document = docBuilderTarget.parse(location);
			 document.getDocumentElement().normalize();
			 listOfTestNodes = document.getElementsByTagName("test");
		 }catch(Exception e){
			// System.out.println(e);
		 }
		 try {

			 while(flag){
				 beforeFlag = listOfTestNodes.getLength();
				 for(int i = 0; i < listOfTestNodes.getLength(); i++){
					 String testName = listOfTestNodes.item(i).getAttributes().getNamedItem("name").toString()
							 .replace(" Test", "").replace("name=", "").replaceAll("\"", "");
					// System.out.println("Test Name - " + testName);
					 try{
						 if(!testName.equals(name)){
							// System.out.println("Inside if " + testName);
							 listOfTestNodes.item(i).getParentNode().removeChild(listOfTestNodes.item(i));
						 }
					 }catch(Exception e){
					 }

					 //createTestNGReport(document,outPutFilePath + name + ".xml");
				 }
				 if(beforeFlag==listOfTestNodes.getLength()){
					 flag = false;
				 }

			 }
			 cleanMethods(document, name);
		 } catch (Exception e) {
			// System.out.println("In Exception");
			 e.printStackTrace();
		 }
	 }

	 private void cleanMethods(Document document, String name){
		 boolean flag = true;
		 int beforeFlag = 0;
		 NodeList listOfMethodNodes = document.getElementsByTagName("method");
		 try {
			 while(flag){
				 beforeFlag = listOfMethodNodes.getLength();
				 String methodSignature = document.getElementsByTagName("test-method").item(0)
						 .getAttributes().getNamedItem("signature").toString().split("@")[1].split("]\"")[0];

				 for(int i = 0; i < listOfMethodNodes.getLength(); i++){
					 String methodSignatures = listOfMethodNodes.item(i).getAttributes()
							 .getNamedItem("signature").toString().split("@")[1].split("]\"")[0];
					 if(!methodSignatures.equals(methodSignature)){
						 listOfMethodNodes.item(i).getParentNode().removeChild(listOfMethodNodes.item(i));
					 }

					 //	createTestNGReport(document,outPutFilePath + name + ".xml");
				 }
				 if(beforeFlag==listOfMethodNodes.getLength()){
					 flag = false;
				 }
			 }
			 updateSuite(document, name);

		 } catch (Exception e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
	 }

	 private void updateSuite(Document document, String name){
		 String suiteTime = "";
		 String testTime = "";
		 try {
			 suiteTime = document.getElementsByTagName("suite").item(0).getAttributes()
					 .getNamedItem("duration-ms").toString();
			 testTime = document.getElementsByTagName("test").item(0).getAttributes()
					 .getNamedItem("duration-ms").toString();

			 if(!suiteTime.equals(testTime)){
				 suiteTime = testTime;
				 document.getElementsByTagName("suite").item(0).getAttributes()
				 .getNamedItem("duration-ms").setTextContent(suiteTime);
			 }
			 updateTestNgReport(document, name);
			 //	createTestNGReport(document,outPutFilePath + name + ".xml");
		 } catch (Exception e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }
	 }

	 private void updateTestNgReport(Document document, String name){
		 String methodName = "";
		 String testMethodName = "";
		 String testMethodStatus = "";
		 NodeList methodList = null;
		 NodeList testMethodList = null;
		 int passCount = 0;
		 int failCount = 0;
		 int skippedCount = 0;
		 int total = 0;
		 try {
			 methodList = document.getElementsByTagName("method");
			 testMethodList = document.getElementsByTagName("test-method");

			 for(int i = 0 ; i < methodList.getLength(); i++){
				 methodName = methodList.item(i).getAttributes().getNamedItem("name").toString();
				 for(int j = 0; j < testMethodList.getLength(); j++){
					 testMethodName = testMethodList.item(j).getAttributes().getNamedItem("name").toString();
					 if(testMethodName.equals(methodName)){
						 testMethodStatus = testMethodList.item(j).getAttributes().getNamedItem("status")
								 .toString().split("status=\"")[1].split("\"")[0];
						 if(testMethodStatus.equalsIgnoreCase("pass")){
							 passCount = passCount + 1;
						 } else if(testMethodStatus.equalsIgnoreCase("fail")){
							 failCount = failCount + 1;
						 } else {
							 skippedCount = skippedCount + 1;
						 }
					//	 System.out.println("testMethodStatus " + testMethodStatus);
					 }
				 }
			 }
			 total = passCount + failCount + skippedCount;
			 document.getElementsByTagName("testng-results").item(0).getAttributes()
			 .getNamedItem("total").setTextContent(Integer.toString(total));

			 document.getElementsByTagName("testng-results").item(0).getAttributes()
			 .getNamedItem("skipped").setTextContent(Integer.toString(skippedCount));

			 document.getElementsByTagName("testng-results").item(0).getAttributes()
			 .getNamedItem("passed").setTextContent(Integer.toString(passCount));

			 document.getElementsByTagName("testng-results").item(0).getAttributes()
			 .getNamedItem("failed").setTextContent(Integer.toString(failCount));
			 createTestNGReport(document,outPutFilePath + name + ".xml");
		 } catch (Exception e) {
			// System.out.println(e);
			 e.printStackTrace();
		 }
	 }

	 private void createTestNGReport(Document doc, String outPutFilePath){
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 Transformer transformer;
		 try {
			 transformer = transformerFactory.newTransformer();
			 DOMSource source = new DOMSource(doc);
			 StreamResult result = new StreamResult(new File(outPutFilePath));
			 transformer.transform(source, result);
		 } catch (TransformerConfigurationException e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
		 }catch (TransformerException tfe) {
			 tfe.printStackTrace();
		 } 

	 }
}