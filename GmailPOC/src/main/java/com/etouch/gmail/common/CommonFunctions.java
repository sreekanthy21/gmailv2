package com.etouch.gmail.common;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.ScrollsTo;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import mx4j.log.Logger;
import io.appium.java_client.android.AndroidKeyCode;

public class CommonFunctions extends BaseTest {

	/**--
	*@author Venkat
	*/
	public static void clickOrScrollAndClick(AppiumDriver driver, String scrollToTitle , String xpath) throws Exception {

		int cnt = 0;
		while(cnt++ <= 2) {
			try {
				Thread.sleep(1000);
				CommonFunctions.searchAndClickByXpath(driver, xpath);
				break;
			} catch (Exception e) {
				log.info("Scrolling Down to find the Mail: "+xpath+".");
				CommonFunctions.scrollDown(driver, 1);
				Thread.sleep(2000);
			}
		}
		if(cnt >= 2) {
			throw new Exception("No Mail :"+xpath+" Found in "+scrollToTitle+". Please check...");
		}
	}

	
	public static void findMailInSpecificFolder(AppiumDriver driver, String folderLable, String mailXpath) throws Exception{
		userAccount.openMenuDrawerNew(driver);
		CommonFunctions.scrollsToTitle(driver, folderLable);
		clickOrScrollAndClick(driver, folderLable, mailXpath);
	}
	
	
	public static void searchForFileInDrive(AppiumDriver driver, String imageXPath, String fileName) throws Exception{
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.documentsui:id/option_menu_search'] | //android.widget.TextView[@resource-id='com.android.documentsui:id/menu_search']");  
		CommonFunctions.searchAndSendKeysByXpath(driver, "//android.widget.EditText[@resource-id='android:id/search_src_text'] | //android.widget.EditText[@text='Search�']", fileName);

		Thread.sleep(1000);

		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.EditText[@resource-id='android:id/search_src_text'] | //android.widget.EditText[@resource-id='com.google.android.documentsui:id/search_src_text']");  
		Thread.sleep(1000);
		((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
		Thread.sleep(1000);
		
		//CommonFunctions.searchAndClickByXpath(driver, commonElements.get("AppiumImage"));
		CommonFunctions.searchAndClickByXpath(driver, imageXPath+" | //android.widget.TextView[@resource-id='android:id/title']");
		log.info("In IF Attachment1 clicked");		
		Thread.sleep(1000);
	}
	
	public static void clickOnAddAccountOrManageAccounts(AppiumDriver driver, String scrollTo, String xPath) throws Exception{
		int counter = 3;
		while(counter <=2){
			try{
				CommonFunctions.scrollTo(driver, scrollTo);  
				CommonFunctions.searchAndClickByXpath(driver, xPath);
			}catch(Exception e){
				throw new Exception("Unable to click on Add account/Manage Account");
			}
		}
		
		if(BaseTest.isAccountSwitcherExist){
		}else{
			Thread.sleep(500);
			CommonFunctions.scrollTo(driver, "Add account");  
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'Add account')]");  
		}
	}
	
	/**
	 * Method o scroll Up the screen
	 * 
	 * @param numberOfScroll
	 * @throws InterruptedException
	 */
	public static void scrollUp(AppiumDriver driver, int numberOfScroll) throws InterruptedException {
		log.info("Scrolling up");

		Dimension windowSize = driver.manage().window().getSize();
		for (int j = 0; j < numberOfScroll; j++) {
			driver.swipe((int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.30),
					(int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.85), 1000);
			// Thread.sleep(100);
			
		}
	}

	/**
	 * Method to scroll down the screen
	 * 
	 * @param numberOfScroll
	 * @throws InterruptedException
	 */
	public static void scrollDown(AppiumDriver driver, int numberOfScroll) throws InterruptedException {
		log.info("Scrolling Down");

		Dimension windowSize = driver.manage().window().getSize();
		for (int j = 1; j <= numberOfScroll; j++) {
			driver.swipe((int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.85),
					(int) ((windowSize.width) * 0.50), (int) ((windowSize.height) * 0.30), 1000);
			// Thread.sleep(500);
		}
	}

	/**
	 * Method to navigate back
	 * 
	 * @throws InterruptedException
	 */
	public void navigateBack(AppiumDriver driver) throws InterruptedException {
		log.info("Navigating Back");
		Thread.sleep(3000);
		/*if(deviceSpecific.equalsIgnoreCase("AndroidSamsung")){
			log.info("Performing navigate back operation on "+deviceSpecific);
			driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Close search'] | //android.widget.ImageButton[@content-desc='Navigate up']")).click();
			driver.findElement(By.xpath("//android.widget.ImageButton[@content-desc='Open navigation drawer'] | //android.widget.ImageButton[@content-desc='Navigate up']")).click();
		}else{*/
		driver.navigate().back();
	//	}
	}
	
	
	/**
	 * Method to scroll to
	 * 
	 * @param elementText
	 * @throws InterruptedException
	 */
	public static void scrollTo(AppiumDriver driver,String elementText) throws InterruptedException {
		//AppiumDriver driver = map.get(Thread.currentThread().getId());
		((ScrollsTo)driver).scrollTo(elementText);
		// Thread.sleep(700);
	}
	
	/** -- Scroll to element and tap on it
	 * 
	 * @param driver
	 * @param Title
	 * @throws Exception 
	 */
	public static void scrollsToTitle(AppiumDriver driver, String Title) throws Exception{
		
        System.out.println("CF tapCellByTitle(): " + Title);
        List<WebElement> elementList = driver.findElements(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().resourceIdMatches(\".*id/list\")).setMaxSearchSwipes(60).scrollIntoView("
                        + "new UiSelector().text(\"" + Title + "\"))"));
        if (elementList.isEmpty()){
        	log.info("CF Empty Title:"+Title+" found" );
        	throw new Exception("Desired locator not found");
        }else {
            try {
                new TouchAction((MobileDriver) driver).press(elementList.get(0)).waitAction(70).release().perform();
           		if("Sync Gmail".equalsIgnoreCase(Title)) {
        			Thread.sleep(2000);
        			try {
        				driver.findElement(By.xpath("//android.widget.TextView[@text='Turn sync Gmail off?']"));
        				Thread.sleep(1000);
        				driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
        			} catch (Exception e) {
        				log.error("Exception :"+e);
        			}
        		}

               } catch (Exception e) {
                 e.printStackTrace();
               }
		}
	}
	
	public static void verifyPopUp(AppiumDriver driver, String xPath){
		
		try {
			driver.findElement(By.xpath("//android.widget.TextView[@text='Turn sync Gmail off?']"));
			Thread.sleep(1000);
			driver.findElement(By.xpath("//android.widget.Button[@text='OK']")).click();
		} catch (Exception e) {
			log.error("Exception :"+e);
		}

	}
	
	
	
	/** -- Scroll to element
	 * 
	 * @param driver
	 * @param Title
	 * @throws Exception 
	 */
	public static boolean scrollToCellByTitle(AppiumDriver driver, String title) throws Exception {
        System.out.println("  scrollToCellByTitle(): " + title);
        List<WebElement> elementList = driver.findElements(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().resourceIdMatches(\".*id/list\")).setMaxSearchSwipes(10).scrollIntoView("
                        + "new UiSelector().text(\"" + title + "\"))"));
        if (elementList.isEmpty()){
        	log.info("Empty file found "+elementList);
        	throw new Exception("");
        }
            
        else {
          return true;
        }
    }
	
	
	/** -- Scroll to element
	 * 
	 * @param driver
	 * @param Title
	 * @throws Exception 
	 */
	public static boolean scrollToCellByTitleVerify(AppiumDriver driver, String title) throws Exception {
        System.out.println("  scrollToCellByTitle(): " + title);
        List<WebElement> elementList = driver.findElements(MobileBy.AndroidUIAutomator(
                "new UiScrollable(new UiSelector().resourceIdMatches(\".*id/list\")).setMaxSearchSwipes(5).scrollIntoView("
                        + "new UiSelector().text(\"" + title + "\"))"));
        if (elementList.isEmpty()){
        	log.info("Empty file found "+elementList);
        	return false;
        }
            
        else {
          return true;
        }
    }
	
	
	/**
	* Method to Navigate back to Menu Drawer
	* 
	*/
	public static void navigateBackToMenuDrawer(AppiumDriver driver) throws InterruptedException  {
		try {
			
			driver.navigate().back();
			driver.navigate().back();
			driver.navigate().back();
			} catch (Exception e) {
			log.info("Unable to switch off AirPlane mode");
			
		}
	}



	/**
	 * Method to hide keyboard
	 * 
	 * @throws InterruptedException
	 */
	public static void hideKeyboard() throws InterruptedException {
		try {
			AppiumDriver driver = map.get(Thread.currentThread().getId());
			driver.hideKeyboard();
			Thread.sleep(500);
		} catch (Exception e) {
			log.info("Keyboard not found");
		}
	}

	/**
	 * Method to get screenshot and compare image
	 * 
	 * @param methodName
	 * @throws IOException
	 */
	public void getScreenshotAndCompareImage(String methodName) throws IOException {
		AppiumDriver driver = map.get(Thread.currentThread().getId());
		GetDataInHashMap.getScreenShot(driver, methodName);
		GetDataInHashMap.compareGoldenImage(methodName);
	}

	/**
	 * Method to search and click on element using ID
	 * 
	 * @param driver2
	 * @param ID
	 * @throws Exception
	 */
	public static void searchAndClickById(AppiumDriver driver2, String ID) throws Exception {
		try {
			// Thread.sleep(200);
			waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.elementToBeClickable(By.id(ID)));
			driver2.findElementById(ID).click();
			// Thread.sleep(1000);
		} catch (Exception e) {
			log.info("Unable to click on element using ID: " + ID);
			throw new Exception("Unable to click on element using ID: " + ID);
		}
	}

	/**
	 * Method to search and click on element using Xpath
	 * 
	 * @param driver2
	 * @param xpath
	 * @throws Exception
	 */
	public static void searchAndClickByXpath(AppiumDriver driver2, String xpath) throws Exception {
		try {
			log.info("CF wait4 element:"+xpath);
			waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			((AndroidElement)driver2.findElement(By.xpath((xpath)))).click();
			
		} catch (Exception e) {
			log.info("CF Unable to click element using X-Path: " + xpath);
			throw new Exception("Unable to click element using X-Path: " + xpath);
		}
	}

	/**
	 * Method to search and send keys on element using ID
	 * 
	 * @param driver2
	 * @param locater
	 * @param inputKeys
	 * @throws Exception
	 */
	public static void searchAndSendKeysByID(AppiumDriver driver2, String locater, String inputKeys) throws Exception {
		try {
			/*WebElement element = waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.id(locater)));*/
			commonPage.waitForElementToBePresentById(locater);
			WebElement element = driver2.findElement(By.id(locater));
			//((AndroidElement)element).clear();
			((AndroidElement)element).sendKeys(inputKeys);
			
			/*driver2.findElement(By.id(locater)).click();
			driver2.getKeyboard().sendKeys(inputKeys);*/
			/*
			try {
				Thread.sleep(1000);

				driver2.findElement(By.id(locater)).click(); 
				Thread.sleep(1000);
			     new ProcessBuilder(new String[]{"adb", "-s", "ENU7N15A20005381", "shell", "input", "text", inputKeys})
			       .redirectErrorStream(true)
			       .start();
			} catch (IOException e) {
			   e.printStackTrace();
			}
*/

		} catch (Exception e) {
			log.info("Unable to send keys: " + inputKeys);
			throw new Exception("Unable to send keys pn element by ID " + locater + " Data :" + inputKeys);
		}
	}

	/**
	 * Method to search and send keys on element using Xpath
	 * 
	 * @param driver2
	 * @param locater
	 * @param inputKeys
	 * @throws Exception
	 */
	public static void searchAndSendKeysByXpath(AppiumDriver driver2, String locater, String inputKeys)
			throws Exception {
		try {
			/*WebElement element = waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(locater)));*/
			CommonPage.waitForElementToBePresentByXpath(locater);
			AndroidElement element = (AndroidElement) driver2.findElement(By.xpath(locater));
			((AndroidElement)element).click();
			((AndroidElement)element).clear();
			((AndroidElement)element).sendKeys(inputKeys);
			
			/*driver2.findElement(By.xpath(locater)).click();
			driver2.getKeyboard().sendKeys(inputKeys);*/

		} catch (Exception e) {
			log.info("Unable to send keys: " + inputKeys);
			throw new Exception("Unable to send keys on element by Xpath " + locater + " Data :" + inputKeys);
		}
	}

	/**
	 * Method to search and send keys on element using Xpath
	 * 
	 * @param driver2
	 * @param locater
	 * @param inputKeys
	 * @throws Exception
	 */
	public static void searchAndSendKeysByXpathNoClear(AppiumDriver driver2, String locater, String inputKeys)
			throws Exception {
		AndroidElement element = null;
		try {
			/*WebElement element = waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(locater)));*/
			CommonPage.waitForElementToBePresentByXpath(locater);
			element = (AndroidElement) driver2.findElement(By.xpath(locater));
			//element.clear();
			((AndroidElement)element).click();
			((AndroidElement)element).sendKeys(inputKeys);
			
			/*driver2.findElement(By.xpath(locater)).click();
			driver2.getKeyboard().sendKeys(inputKeys);*/

		} catch (Exception e) {
			log.info("Unable to send keys: " + inputKeys);
			log.info("Hence trying adb input");
			try{
				Process p;
				 String[] adbInput = new String[]{"adb", "-s", deviceudid, "shell", "input", "text ", "'",inputKeys,"'"};
					//((AndroidElement)element).click();
				 p = new ProcessBuilder(adbInput).start();
				    log.info("Sent keys using adb input");
				    Thread.sleep(1000);
			}catch(Exception ex){
				ex.printStackTrace();
				throw new Exception("Unable to send keys on element by Xpath " + locater + " Data :" + inputKeys);
			}
		}
	}

	/**
	 * Method to search and Get keys of element using Xpath
	 * 
	 * @param driver2
	 * @param elementgetTexLocater
	 * @return
	 * @throws Exception
	 */
	public static String searchAndGetTextOnElementByXpath(AppiumDriver driver2, String elementgetTexLocater)
			throws Exception {
		String textOnElement = "";
		try {
			waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(elementgetTexLocater)));
			textOnElement = driver2.findElement(By.xpath(elementgetTexLocater)).getText();
			log.info("In CF. Text displayed "+textOnElement);
		} catch (Exception e) {
			log.info("In CF.searchAndGetTextOnElementByXpath Unable to find text on element");
			throw new Exception("Unable to find text on element " + elementgetTexLocater);
		}
		return textOnElement;
	}

	/**
	 * Method to search and get text of element using ID
	 * 
	 * @param driver2
	 * @param elementgetTexLocater
	 * @return
	 * @throws Exception
	 */
	public static String searchAndGetTextOnElementById(AppiumDriver driver2, String elementgetTexLocater)
			throws Exception {
		String textOnElement = "";
		try {
			waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.id(elementgetTexLocater)));
			textOnElement = driver2.findElement(By.id(elementgetTexLocater)).getText();
		} catch (Exception e) {
			log.info("Unable to find text on element");
			throw new Exception("Unable to find text on element by ID :" + elementgetTexLocater);
		}
		return textOnElement;
	}
	
	public static String searchAndGetAttributeOnElementByXpath(AppiumDriver driver2, String ElementLocator, String AttributeValue) throws Exception{
		
		String AttributeOnElement = "";
		try{
			waitMap.get(Thread.currentThread().getId()).until(ExpectedConditions.presenceOfElementLocated(By.xpath(ElementLocator)));
			AttributeOnElement = driver2.findElement(By.xpath(ElementLocator)).getAttribute(AttributeValue);
			
		}catch(Exception e){
			log.info("Unable to get the attribute value"+ElementLocator);
			throw new Exception("Unable to get the attribute value or it may not present : "+AttributeValue);
		}
		return AttributeOnElement;
	}
	

	/**
	 * Method to verify whether element is rendered or not USING explicit wait
	 * Note : Explicit wait is used for finding the element
	 * 
	 * @param driver2
	 * @param xpath
	 * @return True if element is rendered and returns False if element is NOT
	 *         rendered
	 * @throws Exception 
	 */
	public static boolean isElementByXpathDisplayed(AppiumDriver driver2, String xpath) throws Exception {
		try {
			log.info("CF Wait4 Element:" + xpath);
			WebElement element = waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
			log.info("Element is displayed");
			return element.isDisplayed();
		} catch (Exception e) {
			log.info("CF Unable to find Element ::: Locator of element ::: " + xpath);
			throw new Exception("Unable to find the "+xpath);
		}
	}

	/**
	 * Method to verify whether element is rendered or not WITHOUT using
	 * explicit wait
	 * 
	 * @param driver2
	 * @param xpath
	 * @return True if element is NOT rendered and returns false if element is
	 *         rendered
	 */
	public static boolean isElementByXpathNotDisplayed(AppiumDriver driver2, String xpath) {
		int count = 0;
		boolean isElementFound = false;
		while (count < 6) {
			try {
				Thread.sleep(500);
				log.info("In CF.isElementByXpathNotDisplayed findElement, count:"+count+", xpath:" + xpath);
				driver2.findElement(By.xpath(xpath));
				return false;
			} catch (Exception e) {
				count++;
				log.info("In CF.isElementByXpathNotDisplayed Unable to find Element ::: Locator of element ::: " + xpath);
			}
		}
		return true;
	}

	/**
	 * Method to verify whether element is rendered or not USING explicit wait
	 * Note : Explicit wait is used for finding the element
	 * 
	 * @param driver2
	 * @param xpath
	 * @return True if element is rendered and returns False if element is NOT
	 *         rendered
	 */
	public static boolean isElementByIdDisplayed(AppiumDriver driver2, String id) {
		try {
			WebElement element = waitMap.get(Thread.currentThread().getId())
					.until(ExpectedConditions.elementToBeClickable(By.id(id)));
			return element.isDisplayed();
		} catch (Exception e) {
			log.info("Unable to find Element ::: Locator of element ::: " + id);
			return false;
		}
	}
	
	
	/**
	 * Method to verify whether element is rendered or not by id or not WITHOUT
	 * using explicit wait
	 * 
	 * @param driver2
	 * @param id
	 * @return True if element is NOT rendered and returns false if element is
	 *         rendered
	 */
	public static boolean isElementByIdNotDisplayed(AppiumDriver driver2, String id) {
		try {
			Thread.sleep(3000);
			driver2.findElement(By.id(id));
			return false;
		} catch (Exception e) {
			log.info("Unable to find Element ::: Locator of element ::: " + id);
			return true;
		}
	}
	
	/**
	* Method to switch on AirPlane mode in device
	* 
	*/
	public static void switchOnAirPlaneMode() {
		try {
			
			Runtime.getRuntime().exec("adb shell settings put global airplane_mode_on 1");
			Runtime.getRuntime().exec("adb shell am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true");
			Thread.sleep(3000);
		} catch (Exception e) {
			log.info("Unable to switch on AirPlane mode");
			
		}
	}
	
	public void turnOffWifi(AppiumDriver driver){
		try{
			Runtime.getRuntime().exec("adb shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//android.widget.Switch[@text='ON']")).click();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void turnOnWifi(AppiumDriver driver){
		try{
			Runtime.getRuntime().exec("adb shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings");
			Thread.sleep(3000);
			driver.findElement(By.xpath("//android.widget.Switch[@text='OFF']")).click();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


/**
	* Method to switch off AirPlane mode in device
	* 
	*/
	public static void switchOffAirPlaneMode() {
		try {
			
			Runtime.getRuntime().exec("adb shell settings put global airplane_mode_on 0");
			Runtime.getRuntime().exec("adb shell am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false");
			Thread.sleep(20000);
		} catch (Exception e) {
			log.info("Unable to switch off AirPlane mode");
			
		}
	}
	
	/**Bindu**//*
	
	*//**Method for comparing the mail compose body
	 * 
	 * @param driver2
	 * @throws Exception
	 *//*
	public void verifyBodyIsMatching(AppiumDriver driver2) throws Exception{

		Map<String, String> verifyBodyIsMAtchingData = commonPage.getTestData("verifyReplyFromNotification");

		int y = driver2.findElements(By.xpath("//android.view.View[@resource-id='com.android.systemui:id/backgroundNormal']")).size();
		log.info(y);

		for(int i=0; i<=y;i++){
		String body =driver2.findElement(By.xpath("//android.webkit.WebView[@index='0']")).getText();
		String expectedCompose = verifyBodyIsMAtchingData.get("ComposeBodyData1");
		if(body.contains(expectedCompose)){
			log.info("*********************** Verified the body of the mail and data is intact******************************");
		}else{log.info("&&&&&&&&&&&&&&&&&&&&&&&Actual And Expected values are not matching &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");}
			}
		}

		
	
	*//**Method for comparing subject line of a mail
	 * 
	 * @param driver2
	 * @throws Exception
	 *//*
		public void verifySubjectIsMatching(AppiumDriver driver2)throws Exception{

		Map<String, String> verifySubjectIsMatchingData = commonPage.getTestData("verifyReplyFromNotification");
		int x = driver2.findElements(By.xpath("//android.view.View[@resource-id='com.android.systemui:id/backgroundNormal']")).size();
		log.info(x);
		for(int i=0; i<=x;i++){

		String title =	driver2.findElement(By.xpath("//android.widget.TextView[@resource-id='android:id/big_text']")).getText();
		log.info(title+" xyz");
		String title1 = verifySubjectIsMatchingData.get("SubjectData");
		log.info(title1+" abc");
		if(title.contains(title1)){

		log.info("enetred");
		commonPage.commonElements.get("ReplyButtonFromNotification");
		//driver2.findElement(By.xpath("//android.widget.Button[@text='Reply']")).click();
		Thread.sleep(3000);
		log.info("*****************************Clicked on the reply button from notificationbar******************************");
		break;

		}else{log.info("&&&&&&&&&&&&&&&&&&&&&&&Subject Lines are not matching &&&&&&&&&&&&&&&&&&&&&&& ");}
			}
		}
	
*/
	
	
	/**Method to get the count of number of elements present in a page
	 * 
	 * @param driver
	 * @param xpath
	 * @return
	 * @throws Exception
	 * @author Phaneendra
	 */
	public static int getSizeOfElements(AppiumDriver driver, String xpath)throws Exception{
		log.info("Verifying the count size of elements for "+xpath);
		int tcount;
		try{
			List<WebElement> count = driver.findElements(By.xpath(xpath));
			tcount = count.size();
		}catch(Exception e){
			throw new Exception(e.getLocalizedMessage());
		}
		log.info("Returning the count size as "+tcount);
		return tcount;
	}
	
	
	/**Method to get the count of number of elements present in a page
	 * 
	 * @param driver
	 * @param xpath
	 * @return
	 * @throws Exception
	 * @author Phaneendra
	 */
	public static int getSizeOfElementsId(AppiumDriver driver, By id)throws Exception{
		log.info("Verifying the count size of elements");
		int tcount;
		try{
			List<WebElement> count = driver.findElements(id);
			tcount = count.size();
		}catch(Exception e){
			throw new Exception(e.getLocalizedMessage());
		}
		log.info("Returning the count size as "+tcount);
		return tcount;
	}
	/**
	* Method to perform orientation(Rotation) of screen
	* @author batchi
	 * @throws Exception 
	*/	
	public static void orientationOfScreen(AppiumDriver driver2) throws Exception{
		for(int i=0;i<=3;i++){
	 		driver2.rotate(org.openqa.selenium.ScreenOrientation.LANDSCAPE);
			  Thread.sleep(3000);
			  driver2.rotate(org.openqa.selenium.ScreenOrientation.PORTRAIT);
			  Thread.sleep(3000);
				  log.info("Orientation is done" +i+ "times");
			  			  }
		}
	
	/**
	 * Method to Tap on device using x and y cordinates
	 * 
	 * @throws InterruptedException
	 */
	public static void tapUsingXYCordinates(AppiumDriver driver,int x,int y) throws InterruptedException {
	       driver.tap(1,x+68,y+30,2);
	       
     }
	
	public static void tapUsingCoordinates(AppiumDriver driver,int x,int y) throws InterruptedException {
	       driver.tap(1,x,y,1);
	       
  }
	
	/**
	 * Method to search and send keys on element using ID
	 * 
	 * @param driver2
	 * @param locater
	 * @param inputKeys
	 * @throws Exception
	 */
	public static void searchAndSendKeysByIDNoClear(AppiumDriver driver2, String locater, String inputKeys) throws Exception {
		try {
			commonPage.waitForElementToBePresentById(locater);
			WebElement element = driver2.findElement(By.id(locater));
			//element.clear();
			element.sendKeys(inputKeys);

		} catch (Exception e) {
			log.info("Unable to send keys: " + inputKeys);
			throw new Exception("Unable to send keys pn element by ID " + locater + " Data :" + inputKeys);
		}
	}

	
	public static void searchAndSendKeysByIDNoClearVal(AppiumDriver driver2, String locater, String inputKeys) throws Exception {
		try {
			commonPage.waitForElementToBePresentById(locater);
			WebElement element = driver2.findElement(By.id(locater));
			((AndroidElement)element).sendKeys(inputKeys);
			//element.clear();
			//element.sendKeys(inputKeys);
		} catch (Exception e) {
			log.info("Unable to send keys: " + inputKeys);
			throw new Exception("Unable to send keys pn element by ID " + locater + " Data :" + inputKeys);
		}
	}

	public static void performAction(AppiumDriver driver, String primayAcc) throws Exception {
		priorityInboxChange(driver, primayAcc, null);
		userAccount.navigateBackNTimesNew(driver, 2);
		Thread.sleep(1000);
		userAccount.openMenuDrawerNew(driver);
		userAccount.clickOrScrollAndClick(driver, "Unread","//android.widget.TextView[@text='Unread']");
		Thread.sleep(1000);
	}
	
	public static void verifyLable(AppiumDriver driver, String actualLable, String lableXPath) throws Exception {
		String unreadText = CommonFunctions.searchAndGetTextOnElementByXpath(driver, lableXPath);
		if(!actualLable.equalsIgnoreCase(unreadText)) {
			throw new Exception(actualLable+" section not displayed..");
		}
	}

	public static void priorityInboxChange(AppiumDriver driver, String primayAcc, String xPath)  throws Exception {
		if(null == xPath) {
			xPath = "//android.widget.CheckedTextView[@text='Unread first']";
		}
		inbox.verifyGmailAppInitState(driver);
		if(!CommonFunctions.isAccountAlreadySelected(primayAcc))
			userAccount.switchMailAccount(driver, primayAcc);
		userAccount.openUserAccountSettingNew(driver, primayAcc);

		userAccount.clickOrScrollAndClick(driver, "Inbox type","//android.widget.TextView[@text='Inbox type']");
		Thread.sleep(200);

		CommonFunctions.searchAndClickByXpath(driver, xPath);
	}
/*	
	public static void smtpMailGeneration(String... commands) throws Exception{
		log.info("Executing the bat file for "+commands[2]);
		new ProcessBuilder(commands).start();
		Thread.sleep(4000);
		log.info("Executed the bat file to generate a mail");
	}
*/
/*	
	public static void smtpMailGeneration(String mailIds, String mailSubjects, String mailBody) throws Exception{
		String[] args = new String[5];
		args[0]=  BaseTest.batchFilesPath+"\\SMTPMailGeneration.bat";
		args[1] = BaseTest.batchFilesPath;
		if(mailBody.endsWith(".html")) {
			mailBody = "@"+BaseTest.batchFilesPath+"\\"+mailBody;
		}
		Process process = null;
		for(String mailId: mailIds.split(",")) {
			args[2] = mailId;
			for(String mailSubject: mailSubjects.split(",")) {
				args[3] = mailSubject; args[4] = mailBody;
				log.info("Executing the bat file for mail: "+mailId+", "+mailSubject);
				process = new ProcessBuilder(args).start();
				Thread.sleep(1000);
				log.info("Executed the bat file to generate a mail..");
				process.destroy();
			}
		}
	}
*/
	
	
public static void smtpMailGeneration(String mailIds, String mailSubjects, String mailBody, String flag){
	String[] processArguments = new String[6];
	processArguments[0]=  BaseTest.batchFilesPath+"SMTPMailGeneration.bat";
	processArguments[1] = BaseTest.batchFilesPath;
	
	if(mailBody.endsWith(".html")) {
		mailBody = "@"+BaseTest.batchFilesPath+mailBody;
	}
	if(flag == null) {
		flag = "-b";
	}else {
		mailBody = "High";
	}
	
	Process process = null;
	
	for(String mailId: mailIds.split(",")) {
		processArguments[2] = mailId;
		
		for(String mailSubject: mailSubjects.split(",")) {
			processArguments[3] = mailSubject; processArguments[4] = flag; processArguments[5] = mailBody;
			
			log.info("Executing bat file for: "+mailId+", "+mailSubject);
			try {
				process = new ProcessBuilder(processArguments).start();
				Thread.sleep(1000);
			} catch (Exception e) {
				log.error("Excpetion while generating mail for:"+mailId+", "+mailSubject);
			}finally {
				process.destroy();
			}
			log.info("Execution of batch file is success...");
		}
	}
}



	
	public static void navigateBackNTimesNew(AppiumDriver driver, int noOfTimes) throws InterruptedException{
		for(int i=1; i<=noOfTimes; i++){
			Thread.sleep(1000);
			driver.navigate().back();
		}
	}

/*
	public static String getNewUserAccount(Map<String, String> excelData, String inputMailId) {
		String primaryAccountNew;
		
			primaryAccountNew = excelData.get("PrimaryAccountNew");

		if(null == primaryAccountNew || primaryAccountNew.isEmpty()) {
			primaryAccountNew = excelData.get(inputMailId);;
		}
		return primaryAccountNew;
	}
*/
	
	
	public static String getNewUserAccount(Map<String, String> excelData, String inputMailId, String mailType) {
		String primaryAccountNew;
		
		if(null == mailType){
			primaryAccountNew = excelData.get("PrimaryAccountNew");			
		}else if("exchange".equals(mailType)){
			primaryAccountNew = excelData.get("ExchangeAccountNew");		
		}else{
			primaryAccountNew = excelData.get("PrimaryAccountNewIMAP");
		}

		if(null == primaryAccountNew || primaryAccountNew.isEmpty()) {
			primaryAccountNew = excelData.get(inputMailId);
		}
		log.info("Primary account to return:"+primaryAccountNew);
		return primaryAccountNew;
	}
	
	
/*
	public static String getNewUserAccountIMAP(Map<String, String> excelData, String inputMailId) {
		String primaryAccountNew = excelData.get("PrimaryAccountNewIMAP");

		if(null == primaryAccountNew || primaryAccountNew.isEmpty()) {
			primaryAccountNew = excelData.get(inputMailId);;
		}
		return primaryAccountNew;
	}
*/
	/**
	 * @author Venkat
	 */
	public static boolean isAccountAlreadySelected(String inputAccount) {
		log.info("CF.isAccountAlreadySelected :inputAccount:"+inputAccount+", BaseTest.selectedAccount:"+BaseTest.selectedAccount);
		if(BaseTest.selectedAccount == null || inputAccount == null) {
			return false;
		}
		if(BaseTest.selectedAccount.trim().equalsIgnoreCase(inputAccount.trim())) {
			return true;
		}
		BaseTest.selectedAccount = inputAccount;
		return false;
	}
	
	/**
	 * @author Venkat
	 */
	public static void changeCustomFrom(AppiumDriver driver, String accountSpinner, String customFromAccount) throws Exception {
		
		driver.findElement(By.xpath(accountSpinner)).click();
		boolean isElementFound = userAccount.scrollToFindElement(driver, null, "up", customFromAccount);
		
		if(!isElementFound){
			isElementFound = userAccount.scrollToFindElement(driver, null,"down", customFromAccount);						
		}
		if(!isElementFound){
			throw new Exception("Mail id :"+null+" not configured. Please check..");
		}

	}
		
	public static void verifyAttachmentExistance(AppiumDriver driver, String mailSubject, String fileName) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, "//android.view.View[contains(@content-desc,'"+mailSubject+"')]");
		Thread.sleep(2000);
		if(CommonFunctions.getSizeOfElements(driver, "//android.widget.ImageView[contains(@content-desc,'"+fileName+"')]")<=0){
			throw new Exception("Attachment not found in sent mail..!!");
		}
	}
	
	public static void addAttachmentFromDriver(AppiumDriver driver, Map<String, String> verifyComposeAndSendMailData, String fileName) throws Exception {
		CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("AttachmentIcon"));
		CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("InsertFromDriver"));
		CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("DriveMenuSearch"));
		
		CommonFunctions.searchAndSendKeysByXpath(driver, verifyComposeAndSendMailData.get("SearchSrcText"), fileName);
		((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
		Thread.sleep(5000);
		String fileLocator = verifyComposeAndSendMailData.get("DriveFileListItemTitle");
		fileLocator = fileLocator.replace("FileName", fileName);
		CommonFunctions.searchAndClickByXpath(driver, fileLocator);
		CommonFunctions.searchAndClickByXpath(driver, verifyComposeAndSendMailData.get("DriveButtonBarButtonRight"));
	}
	
	public static void launchActivity(AppiumDriver driver, String adbCommandActivity) throws Exception {
		try{
			String activity[] = adbCommandActivity.substring(adbCommandActivity.indexOf("com")).split("/");
			((AndroidDriver) driver).startActivity(activity[0],activity[1]);
			log.info("The Activity launched successfully. -->: "+adbCommandActivity);
			Thread.sleep(3000);
		}catch(Exception e) {
			log.error("Unable to start the app. Trying with ADB commands..", e);
			sendKeysThroughADB(adbCommandActivity);
		}
	}
	
	public static void sendKeysThroughADB(String command) {
		createAndStartProcess(command);
	}
	public static void createAndStartProcess(String activityCommand) {
		Process process = null;
		try {
			process = new ProcessBuilder(activityCommand.split("\\s+")).start();
			Thread.sleep(3000);
		} catch (IOException e) {
			e.printStackTrace();
			log.error("Unable to create the OS Process",e);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Unable to create the OS Process",e);
		}finally {
			if(null != process) {
				process.destroy();
			}
		}
	}
	
	
	public static void discardDraftFromPage(AppiumDriver driver, String text, String pageType) throws Exception {
		try {
			CommonFunctions.isElementByXpathDisplayed(driver, "//android.view.ViewGroup[contains(@text,'"+text+"')]");
		} catch (Exception e) {
			log.info("Scrolling down for mail :"+text);
			CommonFunctions.scrollDown(driver,1);
		}
		if("TL".equalsIgnoreCase(pageType)) {
			CommonFunctions.searchAndClickByXpath(driver, "//android.view.ViewGroup[contains(@text,'"+text+"')]//android.widget.ImageView[@index='0']");
		}else {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[contains(@text,'"+text+"')]");
		}

		if("EditView".equalsIgnoreCase(pageType)) {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/edit_draft'] | //android.widget.ImageView[@content-desc='Edit']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@content-desc='More options']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@text='Discard']");
		}else {
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.TextView[@resource-id='com.google.android.gm:id/discard_drafts'] | //android.widget.TextView[@content-desc='Discard drafts']");
			CommonFunctions.searchAndClickByXpath(driver, "//android.widget.Button[@text='OK']");
		}
		Thread.sleep(2000);
		
		CommonFunctions.scrollUp(driver,1);
		

	}
	
	public static void enterDataForDraftMail(AppiumDriver driver, String text) throws Exception {

		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageButton[@content-desc='Compose'] | //android.widget.ImageButton[@resource-id='com.google.android.gm:id/compose_button']");

		CommonFunctions.searchAndSendKeysByXpath(driver,"//android.widget.EditText[@text='Subject']",text);
		try {
			CommonFunctions.searchAndSendKeysByXpathAndADB(driver,"//android.widget.EditText[@text='Compose email']",text);
		} catch (Exception e) {
			((AndroidDriver) driver).sendKeyEvent(AndroidKeyCode.ENTER);
			
			try{
				Process p;
				 String[] adbInput = new String[]{"adb", "-s", deviceudid, "shell", "input", "text ", "'",text,"'"};
				 p = new ProcessBuilder(adbInput).start();
				    log.info("Sent keys using adb input");
				    Thread.sleep(2000);
				    p.destroy();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageButton[@content-desc='Navigate up']");

	}

}