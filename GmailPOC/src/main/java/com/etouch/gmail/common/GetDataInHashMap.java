package com.etouch.gmail.common;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.etouch.Image.ImageComparison;
import com.etouch.taf.util.ExcelUtil;

import io.appium.java_client.AppiumDriver;

public class GetDataInHashMap extends BaseTest {

	static File scrFile, targetFile;
	static DateFormat dateFormat, dateFormat2;
	static String DateForFolderName, DateForFileName;
	static Process child;
	static String AbsolutePath = GetDataInHashMap.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	static String RequiredPath = AbsolutePath.substring(0, AbsolutePath.indexOf("target/classes/")).substring(1);
	static String configFilePath = RequiredPath.concat("src/test/resources");
	static String videoFileLocation = RequiredPath.concat("src/test/resources/testdata/videos");
	static String droidAtScreenarJarLocation = RequiredPath.concat("src/test/resources/droidAtScreen.jar");
	static String FileLocationScreenShot = RequiredPath;

	// static Path path;
	// static String DataFilePath;

	/**
	 * 
	 * @param sheetName
	 * @param dataKey
	 * @return
	 */
	public static LinkedHashMap<String, String> getData(String sheetName, String dataKey) {
		String testBedName = testBedMap.get(Thread.currentThread().getId());
		//String testBedName = "AndroidSamsung";
		log.info("Current testBedName is---------->>>" + testBedName);

		String DataFilePath = getPath().concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
		DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
		log.info("DataFilePath for CurrentTestBed--------->>>" + DataFilePath);

		LinkedHashMap<String, String> testData = new LinkedHashMap<String, String>();
		String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, sheetName, "NewAccountGenericData");
		for (int i = 0; i < testDataArray.length; i++) {
			testData.put(testDataArray[i][0], testDataArray[i][1]);
			log.info(testDataArray[i][0] + " " + testDataArray[i][1]);
		}
		testDataArray = null;
		testDataArray = ExcelUtil.readExcelData(DataFilePath, sheetName, dataKey);

		for (int i = 0; i < testDataArray.length; i++) {
			testData.put(testDataArray[i][0], testDataArray[i][1]);
			log.info(testDataArray[i][0] + " " + testDataArray[i][1]);
		}
		return testData;

	}
	public static String getCurrentTestBedBatchFile() {
		String testBedName = testBedMap.get(Thread.currentThread().getId());
		return getPath().concat("Mail/" + testBedName+".bat");
	}
	/**
	 * 
	 * @param driver
	 * @param methodName
	 * @throws IOException
	 */
	public static void getScreenShot(AppiumDriver driver, String methodName) throws IOException {

	//	scrFile = driver.getScreenshotAs(OutputType.FILE);
		scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		/*
		 * dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh"); dateFormat2 =
		 * new SimpleDateFormat("dd-MMM-yyyy__hh_mm"); DateForFolderName=
		 * dateFormat.format(new Date()); DateForFileName=
		 * dateFormat2.format(new Date());
		 */

		String testBedName = testBedMap.get(Thread.currentThread().getId());
		log.info("Current testBedName---------->>>" + testBedName);
		targetFile = new File(
				FileLocationScreenShot + "GmailTestData/Output/" + testBedName + "/Image/" + methodName + ".png");
		log.info("ScreenShotPath for CurrentTestBed--------->>>" + targetFile);

		FileUtils.copyFile(scrFile, targetFile);
	}

	/**
	 * 
	 * @param driver
	 * @param methodName
	 * @throws IOException
	 */
	public static void getScreenShotForFailure(AppiumDriver driver, String methodName) throws IOException {

		scrFile = driver.getScreenshotAs(OutputType.FILE);
		/*
		 * dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh"); dateFormat2 =
		 * new SimpleDateFormat("dd-MMM-yyyy__hh_mm"); DateForFolderName=
		 * dateFormat.format(new Date()); DateForFileName=
		 * dateFormat2.format(new Date());
		 */

		String testBedName = testBedMap.get(Thread.currentThread().getId());
		log.info("Current testBedName---------->>>" + testBedName);
		targetFile = new File(
				FileLocationScreenShot + "GmailTestData/Output/" + testBedName + "/Failure/" + methodName + ".png");
		log.info("ScreenShotPath for CurrentTestBed--------->>>" + targetFile);

		FileUtils.copyFile(scrFile, targetFile);
	}

	/**
	 * 
	 * @return
	 */
	public static String getPath() {
		return FileLocationScreenShot;
	}

	/**
	 * 
	 * @param methodName
	 */
	public static boolean compareGoldenImage(String methodName)
	{
		log.info("Comparing the Image");
		boolean compareResult = true;
		String testBedName =testBedMap.get(Thread.currentThread().getId());
		ImageComparison imgCmp=new ImageComparison();
		String img1 = getPath().concat("GmailTestData/Input/"+testBedName+"/GoldenImage/"+methodName+"goldenImage.png");
		String img2 = getPath().concat("GmailTestData/Output/"+testBedName+"/Image/" +methodName+".png");
		Object[][] testData = imgCmp.getData("ImageCompare", methodName);
		log.info(img1);
		log.info(img2);
		compareResult = imgCmp.testImageComparison(testData,methodName, img1, img2);
		log.info("Comparing Image done. compareResult:"+compareResult);
		return compareResult;
	}

}
