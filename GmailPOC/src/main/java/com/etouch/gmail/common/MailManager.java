package com.etouch.gmail.common;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.commons.logging.Log;

import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.util.ExcelUtil;
import com.etouch.taf.util.LogUtil;

public class MailManager {
	private static Log log = LogUtil.getLog(UserAccount.class);

	public static void DeleteAllMails() throws InterruptedException {

		String DataFilePath = GetDataInHashMap.getPath()
				.concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
		String testBedName = GetDataInHashMap.testBedMap.get(Thread.currentThread().getId());
		DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
		String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, "GmailData", "GmailAccounts");
		Properties props = new Properties();

		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.port", "465");

		for (int i = 0; i < testDataArray.length; i++) {
		try {
				System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
				Session session = Session.getDefaultInstance(props, null);
				Store store = session.getStore("imaps");
				store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);
				
				Folder inboxFolder=store.getFolder("inbox");
				deleteMessages(inboxFolder);
				
				Folder sentFolder=store.getFolder("[Gmail]/Sent Mail");
				deleteMessages(sentFolder);
				
				Folder draftFolder=store.getFolder("[Gmail]/Drafts");
				deleteMessages(draftFolder);
				
				Folder allMailFolder=store.getFolder("[Gmail]/All Mail");
				Folder trashFolder;
				try{
				 trashFolder=store.getFolder("[Gmail]/Bin");
				 trashFolder.open(Folder.READ_WRITE);
				 trashFolder.close(true);
				}catch(Exception e){
					trashFolder=store.getFolder("[Gmail]/Trash");
				}
				allMailFolder.open(Folder.READ_WRITE);
				allMailFolder.copyMessages(allMailFolder.getMessages(), trashFolder);
				allMailFolder.close(true);
				deleteMessages(trashFolder);
				
				store.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	
	 private  static  void deleteMessages(Folder folder){
		 try {
			folder.open(Folder.READ_WRITE);
			
			Message[] messages = folder.getMessages();
			for (Message msg : messages) {
				if(msg!=null && msg.getSubject()!=null){
				if (msg.getSubject().contains("Invitation")
						|| msg.getSubject().contains("Gmail Attachments Types")
						|| msg.getSubject().contains("VerifyMailConversation")
						|| msg.getSubject().contains("Promotion mail")
						|| msg.getSubject().contains("VerifyMailSnippet")){
					continue;
				}
					else{
					msg.setFlag(Flags.Flag.DELETED,true);
					}
				}
			}
			folder.close(true);
		 	} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	 
	 
	 static void sendMessagesOld(){
		 Map<String, String> testCaseTestData = new HashMap<String, String>();
			testCaseTestData = GetDataInHashMap.getData("GmailData", "BatchfileName");
			
			 try{    
				 	String command ="cmd /c start " + testCaseTestData.get("fileName");
				 	System.out.println("command1:   "+command);
				    Process p = Runtime.getRuntime().exec(command);
				    p.waitFor();

				}catch( IOException ex ){
				    //Validate the case the file can't be accesed (not enought permissions)

				}catch( InterruptedException ex ){
				    //Validate the case the process is being stopped by some external situation     

				}
	 }
	 
	 static void sendMessages(){
			 try{    
				 	String filePath=GetDataInHashMap.getCurrentTestBedBatchFile();
				 	String command ="cmd /c start " + filePath;
				 	
				 	System.out.println("command2:   "+command);
				    Process p = Runtime.getRuntime().exec(command);
				    p.waitFor();

				}catch( IOException ex ){
				    //Validate the case the file can't be accesed (not enought permissions)

				}catch( InterruptedException ex ){
				    //Validate the case the process is being stopped by some external situation     

				}
	 }
	 
	 public static void DeleteAllMailsTrash() throws InterruptedException {

			String DataFilePath = GetDataInHashMap.getPath()
					.concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
			String testBedName = GetDataInHashMap.testBedMap.get(Thread.currentThread().getId());
			DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
			String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, "GmailData", "GmailTrashAccount");
			Properties props = new Properties();

			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.socketFactory.port", "465");
			props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail.smtp.auth", "true");
			props.setProperty("mail.smtp.port", "465");

			for (int i = 0; i < testDataArray.length; i++) {
			try {
					log.info("Getting list of accounts "+testDataArray[i][0] + " " + testDataArray[i][1]);
					System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
					Session session = Session.getDefaultInstance(props, null);
					Store store = session.getStore("imaps");
					store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);

					Folder trashFolder;
					try{
					 trashFolder=store.getFolder("[Gmail]/Trash");
					 trashFolder.open(Folder.READ_WRITE);
					 trashFolder.close(true);
					}catch(Exception e){
						trashFolder=store.getFolder("[Gmail]/Bin");
					}

					deleteMessages(trashFolder);
					
					store.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	 
	 public static void DeleteAllMailsDrafts() throws InterruptedException {

			String DataFilePath = GetDataInHashMap.getPath()
					.concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
			String testBedName = GetDataInHashMap.testBedMap.get(Thread.currentThread().getId());
			DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
			String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, "GmailData", "GmailTrashAccount");
			Properties props = new Properties();

			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.socketFactory.port", "465");
			props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail.smtp.auth", "true");
			props.setProperty("mail.smtp.port", "465");

			for (int i = 0; i < testDataArray.length; i++) {
			try {
					log.info("Getting list of accounts "+testDataArray[i][0] + " " + testDataArray[i][1]);
					System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
					Session session = Session.getDefaultInstance(props, null);
					Store store = session.getStore("imaps");
					store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);

					Folder DraftsFolder;
					
					DraftsFolder=store.getFolder("[Gmail]/Drafts");
					DraftsFolder.open(Folder.READ_WRITE);
					DraftsFolder.close(true);
					
					deleteMessagesdraft(DraftsFolder);
					
					store.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	 
	 public static void DeleteAllMailsInbox() throws InterruptedException {

			String DataFilePath = GetDataInHashMap.getPath()
					.concat("GmailTestData/Input/CurrentTestBedName/excelData/GmailDataSheet.xls");
			String testBedName = GetDataInHashMap.testBedMap.get(Thread.currentThread().getId());
			DataFilePath = DataFilePath.replaceFirst("CurrentTestBedName", testBedName);
			String[][] testDataArray = ExcelUtil.readExcelData(DataFilePath, "GmailData", "GmailInbox");
			Properties props = new Properties();

			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.socketFactory.port", "465");
			props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.setProperty("mail.smtp.auth", "true");
			props.setProperty("mail.smtp.port", "465");

			for (int i = 0; i < testDataArray.length; i++) {
			try {
					log.info("Getting list of accounts "+testDataArray[i][0] + " " + testDataArray[i][1]);
					System.out.println(testDataArray[i][0] + " " + testDataArray[i][1]);
					Session session = Session.getDefaultInstance(props, null);
					Store store = session.getStore("imaps");
					store.connect("smtp.gmail.com", testDataArray[i][0], testDataArray[i][1]);

					Folder allMailFolder=store.getFolder("[Gmail]/All Mail");
					Folder trashFolder;
					try{
						trashFolder=store.getFolder("[Gmail]/Trash");
						 trashFolder.open(Folder.READ_WRITE);
						 trashFolder.close(true);
						}catch(Exception e){
							trashFolder=store.getFolder("[Gmail]/Bin");
						}
						allMailFolder.open(Folder.READ_WRITE);
						allMailFolder.copyMessages(allMailFolder.getMessages(), trashFolder);
						allMailFolder.close(true);
					//	deleteMessages(trashFolder);
					
					deleteMessagesInbox(trashFolder);
					
					store.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
	 
	
		
		 private  static  void deleteMessagesInbox(Folder folder){
			 try {
				folder.open(Folder.READ_WRITE);
				
				Message[] messages = folder.getMessages();
				for (Message msg : messages) {
					 if(msg!=null && msg.getSubject()!=null){
							if (msg.getSubject().contains("Testing Gmail Mail Composing Functionality")){
								continue;
							}
								else{
								msg.setFlag(Flags.Flag.DELETED,true);
								}
							}
						}
						folder.close(true);
			 	} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
	 
	 private  static  void deleteMessagesdraft(Folder folder){
		 try {
			folder.open(Folder.READ_WRITE);
			
			Message[] messages = folder.getMessages();
			for (Message msg : messages) {
				
					msg.setFlag(Flags.Flag.DELETED,true);
					}
				
			
			folder.close(true);
		 	} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	 
	
}
