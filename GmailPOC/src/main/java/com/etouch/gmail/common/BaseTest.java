/*
 * 
 */
package com.etouch.gmail.common;

import java.awt.AWTException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.etouch.gmail.commonPages.Attachment;
import com.etouch.gmail.commonPages.Calendar;
import com.etouch.gmail.commonPages.ConversationView;
import com.etouch.gmail.commonPages.InboxFunctions;
import com.etouch.gmail.commonPages.NavigationDrawer;
import com.etouch.gmail.commonPages.Promotions;
import com.etouch.gmail.commonPages.UserAccount;
import com.etouch.taf.core.TestBed;
import com.etouch.taf.core.TestBedManager;
import com.etouch.taf.core.exception.DefectException;
import com.etouch.taf.core.exception.DriverException;
import com.etouch.taf.core.resources.TestTypes;
import com.etouch.taf.util.CommonUtil;
import com.etouch.taf.util.LogUtil;
import com.etouch.taf.webui.selenium.MobileView;

import io.appium.java_client.AppiumDriver;

/**
 * Base test that all the test classes should extend. This class initializes
 * {@link TestBedManager} based on configuration file input
 *
 * @author eTouch Systems Corporation
 *
 */

public class BaseTest {

	/** The log. */
	static Log log = LogUtil.getLog(BaseTest.class);

	/** The test bed manager. */
	protected TestBedManager testBedManager = TestBedManager.INSTANCE;

	/** The defect_properties_file. */
	protected static String defect_properties_file = null;

	/** The base url. */
	public String BASE_URL = null;

	/** The BAS e_ ur l2. */
	public String BASE_URL2 = null;

	/** The start time. */
	public long startTime = 0;
	public static String batchFilesPath;
	public static String selectedAccount;
	/** The method num. */
	public int methodNum = 0;
	Properties props;
	public static String deviceName;
	public static String version;
	public static String testBedName;
	public static String appPackage;
	public static String appActivity;
	public static String deviceSpecific;
	public static String deviceudid;
	public static String buildType;
	public static boolean isAccountSwitcherExist;
	public static TestBed testBed = null;
	public static WebDriverWait wait, AppInstallwait;
	public static HashMap<Long, AppiumDriver> map = new HashMap<Long, AppiumDriver>();
	public static HashMap<Long, WebDriverWait> waitMap = new HashMap<Long, WebDriverWait>();
	public static HashMap<Long, WebDriverWait> AppInstallwaitMap = new HashMap<Long, WebDriverWait>();
	public static HashMap<Long, String> testBedMap = new HashMap<Long, String>();
	public static HashMap<Long, String> testFailureMap = new HashMap<Long, String>();
	public static HashMap<Long, String> testBedTypeMap = new HashMap<Long, String>();

	public static CommonPage commonPage;
	public static Attachment attachment;
	public static Calendar calendar = null;
	public static InboxFunctions inbox = null;
	public static NavigationDrawer navDrawer = null;
	public static Promotions promotions = null;
	public static UserAccount userAccount = null;
	public static CommonFunctions commonFunction;
	public static ConversationView conversationView;

	@BeforeTest
	@Parameters("testBedName")
	// Initializing Drivers and Verifying WI-FI availability
	public void settingup(ITestContext context, String testBedName)
			throws InterruptedException, FileNotFoundException, IOException {
		
		try {
			testFailureMap.put(Thread.currentThread().getId(), "Failure Details---->\n");
			testBedName = context.getCurrentXmlTest().getAllParameters().get("testBedName");
		//	testBedName = "AndroidSamsung";
			log.info("Test bed Name is " + testBedName);
			testBed = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName);

			deviceudid = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName).getDevice().getUdid();
			buildType = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName).getDevice().getBuildType();
			
			if("fishfood".equalsIgnoreCase(buildType) || "dogfood".equalsIgnoreCase(buildType) || "release".equalsIgnoreCase(buildType)){
				isAccountSwitcherExist = true;
			}else{
				isAccountSwitcherExist = false;
			}
			String clearadblogscommand = "adb "+"-s "+deviceudid+" logcat -c";
			log.info("Executing adb clear log command :::"+clearadblogscommand);
			Process p =Runtime.getRuntime().exec(clearadblogscommand);
			
			deviceSpecific = testBedName;
			log.info("Device mentioned is : "+deviceSpecific);
			appPackage = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName).getApp().getAppPackage();
			appActivity = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName).getApp().getAppActivity();
			
			// Initializing Android Driver
			testBedMap.put(Thread.currentThread().getId(), testBedName);
			testBedTypeMap.put(Thread.currentThread().getId(), testBed.getDevice().getType());
			synchronized (this) {

				MobileView mobileView = null;
				mobileView = new MobileView(context);
				map.put(Thread.currentThread().getId(), mobileView.getDriver());
				deviceName = testBed.getDevice().getName();
				version = testBed.getPlatform().getVersion();
				log.info(map.get(Thread.currentThread().getId()));
				attachment = new Attachment();
				calendar = new Calendar();
				inbox = new InboxFunctions();
				navDrawer = new NavigationDrawer();
				promotions = new Promotions();
				userAccount = new UserAccount();
				commonFunction = new CommonFunctions();
				commonPage = new CommonPage();
				conversationView = new ConversationView();
			}
			
			log.info("DEVICE NAME IS :" + deviceName);
			AppInstallwait = new WebDriverWait(map.get(Thread.currentThread().getId()), 60);
			wait = new WebDriverWait(map.get(Thread.currentThread().getId()), 30);

			waitMap.put(Thread.currentThread().getId(), wait);
			AppInstallwaitMap.put(Thread.currentThread().getId(), AppInstallwait);
			CommonPage.initializeCommonOperationsData();
			
			/*try {
				MailManager.DeleteAllMails();
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			MailManager.sendMessages();*/

		} catch (Exception e) {

			e.printStackTrace();
			Assert.fail("Error in method " + new Exception().getStackTrace()[0].getMethodName() + "for TestBed "
					+ testBedName + " ----------->" + e.getMessage());
			StackTraceElement[] elements = e.getStackTrace();
			testFailureMap.put(Thread.currentThread().getId(),
					testFailureMap.get(Thread.currentThread().getId()) + ":" + elements[0]);
			log.error("Map Contains:  -->" + testFailureMap.get(Thread.currentThread().getId()));
		}
	}

	/**
	 * Prints the time start.
	 *
	 * @param method
	 *            the method
	 */
	@BeforeMethod()
	public void printTimeStart(Method method) {
		methodNum = methodNum + 1;
		log.info("=================================== Starting method " + methodNum + "::" + method.getName()
				+ "===================================");
		startTime = System.currentTimeMillis();
	}

	/**
	 * Prints the time taken.
	 *
	 * @param method
	 *            the method
	 */
	@AfterMethod()
	public void printTimeTaken(Method method) {
		log.info("=================================== Ending method " + methodNum + "::" + method.getName()
				+ "===================================Time Taken ::" + (System.currentTimeMillis() - startTime) / 1000
				+ " Seconds");
	}

	/**
	 * Setup config.
	 *
	 * @param configParameters
	 *            the config parameters
	 * @param browserName
	 *            the browser name
	 * @throws DriverException
	 *             the driver exception
	 * @throws DefectException
	 *             the defect exception
	 */
	@BeforeSuite()

	private static void setupConfig(ITestContext configParameters, @Optional("FF") String browserName)
			throws DriverException, DefectException {

	}

	/**
	 * Convert file to input stream.
	 *
	 * @param fileName
	 *            the file name
	 * @return the input stream
	 * @throws DriverException
	 *             the driver exception
	 */
	private static InputStream convertFileToInputStream(String fileName) throws DriverException {
		InputStream ipStream = null;
		if (fileName != null) {

			try {
				ipStream = new FileInputStream(new File(fileName));
			} catch (FileNotFoundException e) {

				log.error(e.getMessage());
				e.printStackTrace();
			}
		} else {
			log.error(" File name is null - " + fileName);
			throw new DriverException("failed to read profile configuration/TestNG, file name is missing");
		}
		return ipStream;
	}

	/**
	 * Load config.
	 *
	 * @throws DriverException
	 *             the driver exception
	 * @throws DefectException
	 *             the defect exception
	 * @throws IOException
	 * @throws AWTException
	 */
	@BeforeClass(alwaysRun = true)
	public void loadConfig() throws DriverException, DefectException, IOException, AWTException, Exception {
		batchFilesPath = System.getProperty("user.dir")+File.separator+"Mail"+File.separator;

		AppiumDriver driver = map.get(Thread.currentThread().getId());
		UserAccount.launchGmailApp(driver);
		CommonFunctions.searchAndClickByXpath(driver, "//android.widget.ImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view'] | //android.support.v7.widget.AppCompatImageView[@resource-id='com.google.android.gm:id/og_apd_internal_image_view']");
		selectedAccount = driver.findElement(By.xpath("//android.view.ViewGroup//android.widget.TextView[@resource-id='com.google.android.gm:id/account_name']")).getText();
	}

	/**
	 * Tear down.
	 * 
	 * @throws IOException
	 */
	@AfterClass(alwaysRun = true)
	public void tearDown(ITestContext context) throws IOException {

		String testBedName = context.getCurrentXmlTest().getAllParameters().get("testBedName");
		TestBed testBed = TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName);
		WebDriver driver = (WebDriver) testBed.getDriver();
		if (testBed.getTestType() == TestTypes.WEB.getTestType()) {
			driver.close();
			driver.quit();
		} else {
			map.get(Thread.currentThread().getId()).quit();
			// driver.quit();
		}
		try {
			driver.wait(150000);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		// driver.close();
		// driver.quit();

	}

	@AfterTest
	public void enterLogDetails() throws IOException {
		try {

			testBedName = testBedMap.get(Thread.currentThread().getId());
			
			log.info("Capture crash logs:::::");
			String deviceudid= TestBedManager.INSTANCE.getCurrentTestBeds().get(testBedName).getDevice().getUdid();
			String crashLogCommmand = "adb "+"-s "+deviceudid+" logcat -d AndroidRuntime:E *:S";
			captureCrashLogs(crashLogCommmand);
			
			log.info("Map Contains: -->" + testFailureMap.get(Thread.currentThread().getId()));
			CommonUtil.sop("enterLogDetails Test bed Name is " + testBedName);
			String AbsolutePath = GetDataInHashMap.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String RequiredPath = AbsolutePath.substring(0, AbsolutePath.indexOf("target/classes/")).substring(1);
			String FILENAME = RequiredPath + "GmailTestData/Output/" + testBedName + "/Log/log.txt";
			log.info("Log File Path is" + RequiredPath + "GmailTestData/Output/" + testBedName + "/Log/log.txt");
			BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));
			String content = testFailureMap.get(Thread.currentThread().getId());
			bw.write(content);
			bw.close();

			MailManager.DeleteAllMails();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Method for command line execution  to capture  and save adb crash logs
	 * @param command
	 */
	public void captureCrashLogs(String command) {
		String s = null;
		StringBuffer sbuffer = new StringBuffer();

		try {
			log.info("Executing adb crash log command :::"+command);
			Process p =Runtime.getRuntime().exec(command);
			
			log.debug("Inside executeCommand****************"+command);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			log.debug("Inside executeCommand  Input****************"+stdInput);
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			log.debug("Inside executeCommand error****************"+stdError);
			// read the output from the command
			log.debug("Standard output of the command:\n");

			while ((s = stdInput.readLine()) != null) {
				sbuffer.append(s+"\n");
				log.debug("stdout::"+s);
			}
			// read any errors from the attempted command
			log.debug("Standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				log.debug("error::"+s);
			}
			String currentTime = new SimpleDateFormat("ddMMM_hhmmss").format(new Date());
			log.info("Current testBedName for crash logs---------->>>" + testBedName);

			String file_crashLog = GetDataInHashMap.FileLocationScreenShot + "GmailTestData/Output/" + testBedName + "/Log/" + "crashLogs_"+currentTime + ".txt";
			BufferedWriter bw = new BufferedWriter(new FileWriter(file_crashLog));
			bw.write(sbuffer.toString());
			bw.close();
			log.info("Successfully saved adb crash log file under directory :::"+file_crashLog);

		} catch (Throwable e) {
			log.debug("exception happened: ");
			log.debug("IOException", e);
			e.printStackTrace();
		}
	}

	/**
	 * Construct url.
	 *
	 * @param inputURL
	 *            the input url
	 * @return the string
	 */
	public String constructURL(String inputURL) {
		if (inputURL.contains("<baseurl>"))
			inputURL = inputURL.replace("<baseurl>", BASE_URL);
		else if (inputURL.contains("<baseurl2>"))
			inputURL = inputURL.replace("<baseurl2>", BASE_URL2);

		return inputURL;
	}

}