

/*
 * 
 */
package com.etouch.gmail.listener;

import io.appium.java_client.AppiumDriver;

import java.awt.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.etouch.gmail.common.BaseTest;
import com.etouch.gmail.common.GetDataInHashMap;
import com.etouch.gmail.common.ModifyXMLFile;
import com.etouch.mobile.gmail.RegressionLegacyDF;
import com.etouch.taf.util.LogUtil;
import com.gargoylesoftware.htmlunit.attachment.Attachment;
import com.github.fge.jsonschema.format.draftv3.TimeAttribute;


/**
 * The listener interface for receiving suite events.
 * The class that is interested in processing a suite
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addSuiteListener<code> method. When
 * the suite event occurs, that object's appropriate
 * method is invoked.
 *
 * @author eTouch Systems Corporation
 */

public class SuiteListener extends RegressionLegacyDF implements ISuiteListener {
	
	/** The log. */
	static Log log = LogUtil.getLog(SuiteListener.class);
	
	/** The page ur ls. */
	static public Properties pageURLs = null;
	
	/** The rally property file. */
	static public Properties rallyPropertyFile = null;
	
	/** The is initialize. */
	static boolean isInitialize=false;
	 public static  String line;
	
    /* (non-Javadoc)
     * @see org.testng.ISuiteListener#onStart(org.testng.ISuite)
     */
    public void onStart(ISuite arg0) {
    	log.info("Suite Name :"+ arg0.getName() + " - Start");
    	
    }
    public static HashMap<Long, AppiumDriver> map = new HashMap<Long, AppiumDriver>();
    /* (non-Javadoc)
     * @see org.testng.ISuiteListener#onFinish(org.testng.ISuite)
     */
    public void onFinish(ISuite arg0) {
    	log.info("Suite Name :"+ arg0.getName() + " - End");
    	log.info("********Results*******");    
    	GetDataInHashMap getDataObj = new GetDataInHashMap();
    	ModifyXMLFile mx = new ModifyXMLFile();
		mx.prepareTestNGReport(getDataObj.getPath().concat("test-output/testng-results.xml"),getDataObj.getPath().concat("test-output/testng-results-"));
		 try {
			 /*reportMail();*/
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }
        
    static class EmailAuth extends Authenticator {

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {

            return new PasswordAuthentication("etouchtesttwo@gmail.com", "eTouch@123");

        }
    }
    
   
        
    public static void reportMail() throws Exception{  
   	 try{
   	  String[] ver= null;
   	  
   	//  String to="etouchtestone@gmail.com"; 
   	  
   	String[] to = { "etouchtestone@gmail.com"}; 
  
   	
   	/*String[] to = { "wenchen@google.com" }; 
    String[] cc={ "siddharthach@google.com@gmail.com", "arunmadan@google.com" };
   	*/  
   	  
   	  
   	  final String user="etouchtestone@gmail.com";  
   	  final String password="eTouch@123";  
   	  String bedname = BaseTest.testBedName;
   	  String osVersion = RegressionLegacyDF.version;
   	  log.info("OS version: "+osVersion);
   	  Properties properties = System.getProperties();  
   	  properties.setProperty("mail.smtp.host", "smtp.gmail.com");  
   	  properties.put("mail.smtp.auth", "true");  
   	  properties.put("mail.debug", "false");
         properties.put("mail.smtp.ssl.enable", "true");
//         String prop = properties.getProperty(osVersion);
//         log.info("Version property"+prop);
   	  Session session = Session.getInstance(properties, new EmailAuth()); 	          
   	    
   	    MimeMessage message = new MimeMessage(session);  
   	    message.setFrom(new InternetAddress(user));  
   	   
        InternetAddress[] toAddress = new InternetAddress[to.length];
     //   InternetAddress[] ccAddress = new InternetAddress[cc.length];

        for( int i = 0; i < to.length; i++ ) {
            toAddress[i] = new InternetAddress(to[i]);
        }
        for( int i = 0; i < toAddress.length; i++) {
            message.addRecipient(Message.RecipientType.TO, toAddress[i]);
        }
        
      /*  for( int i = 0; i < cc.length; i++ ) {
            ccAddress[i] = new InternetAddress(cc[i]);
        }
        for( int i = 0; i < ccAddress.length; i++) {
            message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
        } */
   	   // message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
   	    Process p;
 		  ProcessBuilder pb; 
 		  String[] appVersionCommand = new String[]{"adb","shell", "dumpsys", "package", "com.google.android.gm" ,"|"," grep", "versionName"};
 		  p = new ProcessBuilder(appVersionCommand).start();
 		  
 		  String v="";
 		  BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
 		  while ((line=input.readLine()) != null) { 
 		//	System.out.println("App version DogFood : "+line);
 			  //printing in the console
 			  if(line.contains("dogfood")){
 				  System.out.println("App version DogFood : "+line);
 	  		  }else if(line.contains("release")){
 				System.out.println("App version Release: " +line);
 			  }else if(line.contains("fishfood")){
 	 				System.out.println("App version Fishfood: " +line);
 			  }
 			v=line;
  			//ver= v.split("\n");
  			//log.info("Build Version :"+ver[0]);
  			//v= ver[0];
 			log.info(v);
 			break;
 		  }
     	      
   	    message.setSubject("Test Case Report generated for " +bedname +" "+"on " +osVersion +" "+"on"+"	"+v);  
   	          
   	    BodyPart messageBodyPart1 = new MimeBodyPart();  
   	    messageBodyPart1.setText("Test Execution report.");  
   	            
   	    MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
   	    String path = System.getProperty("user.dir");
   	    String filename = path + java.io.File.separator + "test-output"+java.io.File.separator +"Default Suite"+ java.io.File.separator  +bedname+ " "+"Test.html";  
   	    DataSource source = new FileDataSource(filename);  
   	    messageBodyPart2.setDataHandler(new DataHandler(source));  
   	    messageBodyPart2.setFileName(filename);  
   	           
   	    Multipart multipart = new MimeMultipart();  
   	    multipart.addBodyPart(messageBodyPart1);  
   	    multipart.addBodyPart(messageBodyPart2);  
   	  
   	    message.setContent(multipart );  
   	    Thread.sleep(2000); 
   	    Transport.send(message);  
   	   
   	   System.out.println("message sent....");  
   	   input.close();
   	   }catch (MessagingException ex) {ex.printStackTrace();}  
   	 }  
   	}   

